param($noGen = $false, $commonParamFile = "commonparams.txt", $repeatCount = 40)

<# Usage notes

To run this, you need m4m on your path (See the M4M readme for help with that)
It will produce some directories, each containing a spread of experiments and files to run and 'post' them.

$noGen can be set to true so that the run/post files are created, but not the experiment files
$commonParamFile sets the common parameter file
$repeatCount is the number of repeats; 40 is traditional for final runs

#>

# ranges
# NOTE NOTE: seed/r coresponance is good here; not on IRIDIS runs (must consult logs as necessary)
function repeats($n) { 0..($n-1) | ForEach-Object { ,@($_.ToString("00"), $_.ToString("00"))} }
# funky ordering is to match the funky ordering of the runs that are used in the paper (for seed generation) (sorry)
$twentyHalf = (("0.00", "000"), ("0.10", "010"), ("0.20", "020"), ("0.30", "030"), ("0.40", "040"), ("0.50", "050"), ("0.60", "060"), ("0.70", "070"), ("0.80", "080"), ("0.90", "090"), ("1.00", "100"), ("0.05", "005"), ("0.15", "015"), ("0.25", "025"), ("0.35", "035"), ("0.45", "045"), ("0.55", "055"), ("0.65", "065"), ("0.75", "075"), ("0.85", "085"), ("0.95", "095"))
$one = (,("0", "0"))

$lambdas = (("0.00", "000"), ("0.02", "002"), ("0.04", "004"), ("0.06", "006"), ("0.08", "008"), ("0.10", "010"), ("0.12", "012"), ("0.14", "014"), ("0.16", "016"), ("0.18", "018"), ("0.20", "020"))

$runName = "One"
$repeats = repeats($repeatCount) # full batches were all 40 (CPUs per node on IRIDIS)

# standard Z
. .\genSpread.ps1 "Standard" "Standard" 400000 Z r Z r $twentyHalf $repeats {param($x, $y) "Z=$x"} $runName 100 $commonParamFile $noGen

# perfect G
. .\genSpread.ps1 "PerfectG" "PerfectG" 400000 Z r Z r $twentyHalf $repeats {param($x, $y) "Z=$x gresetprob=1 resetoperation=perfect"} $runName 100 $commonParamFile $noGen

# fixed high
. .\genSpread.ps1 "FixHigh" "FixHigh" 400000 Z r Z r $one $repeats {param($x, $y) "Z=$x targetpackage=ivmc:custom targetstring=++++++++++++++++ modulesstring=4*4 ivmcvmpos=hhhh ivmcvmmos=0000 mg=0 g0string=++++++++++++++++"} $runName 100 $commonParamFile $noGen

# L2 (common super-additive; Quadratic)
. .\genSpread.ps1 "LTwo" "LTwo" 400000 Z r Z r $twentyHalf $repeats {param($x, $y) "Z=$x"} $runName 100 $commonParamFile $noGen "regfunc=l2"

# MMSO (somewhat arbitrary strictly sub-additive)
. .\genSpread.ps1 "MMSO" "MMSO" 400000 Z r Z r $twentyHalf $repeats {param($x, $y) "Z=$x"} $runName 100 $commonParamFile $noGen "regfunc=mmsolinAQ1;0.8"

# E204
. .\genSpread.ps1 "E204" "E204" 400000 Z r Z r $twentyHalf $repeats {param($x, $y) "Z=$x targetpackage=ivmc:custom targetstring=80*+ modulesstring=20*4 K=2000 mb=2E-4"} $runName 100 $commonParamFile $noGen

# VaryLambda
. .\genSpread.ps1 "VaryL" "VaryL" 400000 Lambda r L r $lambdas $repeats {param($x, $y) "Lambda=$x"}$runName 100 $commonParamFile $noGen "Z=0.95"