# IvmcL010Bex
. ./seqIvmc.ps1 Standard\Standard\StandardZ000runsOne\r00 40000 "040k" "IvmcL010BExZ000"
. ./seqIvmc.ps1 Standard\Standard\StandardZ050runsOne\r00 40000 "040k" "IvmcL010BExZ050"
. ./seqIvmc.ps1 Standard\Standard\StandardZ095runsOne\r00 120000 "120k" "IvmcL010BExZ095"
. ./seqIvmc.ps1 Standard\Standard\StandardZ100runsOne\r00 400000 "400k" "IvmcL010BExZ100" "400000" "1*16"

# IvmcL010Bex Fixed G
. ./seqIvmc.ps1 FixHigh\FixHigh\FixHighZ0runsOne\r00 10000 "010K" "IvmcL010BExZ000FixedG" "400000" "1*16"

# IvmcL010Bex Perfect G
. ./seqIvmc.ps1 PerfectG\PerfectG\PerfectGZ000runsOne\r00 10000 "010K" "IvmcL010BExZ000PerfectG" "400000" "1*16"

# 204 - NoWs, non-IRIDIS run (unknown seed)
. ./seqIvmc \E204\E204\E204Z050runsOne\r00 500000 "500K" "Ivmc204L800BExZ050" "500000" "20*4"