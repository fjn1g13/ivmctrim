# M4MStuff

These are the powershell scripts that generate the experiments discussed in the paper.

Running `. gens.ps1` will generate all the experiments, each in their own directory. Within that directory will be methods to run the experiments and to perform post-processing.

Having run any experiments of interest, you can run `. paperSequences.ps1` to generate figures as per the paper: the figures appear in the directory along with the data-files.

You need `m4m` in path (or as a function) to use these scripts. Assuming you have powershell and .NET Core (2.1, or 3.1, or Net 5) SDK installed, you need only run `. m4minit.ps1 netcoreapp3.1` in the M4MCode/M4M_Mk1 directory to make the `m4m` command available. If you have not already done so, you will need to compile M4M by running `netstateb` and `m4mb` in that order. You can choose which version of .NET to use with the parameter to `m4minit.ps1`; it can be any of `netcoreapp2.1`, `netcoreapp3.1`, and `net5`.

There is another readme in the M4MCode/M4M_Mk1 directory that describes many more things, but the above commands are all that suffice to reproduce the sub-figures from the paper.

Misc notes:
 - If you run an experiment, and are horrified by how many files it produces, you can use the `m4mclean` command to clean up a bit. The `m4mdeepclean` may be more aggressive.
 - You can download the latest .NET SDK for Windows/Linux/MaxOS from https://dotnet.microsoft.com/download
 - You can install powershell (pwsh) with .NET by running `dotnet tool install -g powershell` (this works on windows or linux); if you can't install tools globally, then consult https://docs.microsoft.com/en-us/dotnet/core/tools/local-tools-how-to-use or install powershell some other way
 - Starter scripts are generated for the IRIDIS5 computing cluser, but you'll need to modify `genSpread` (Ctrl-F for "# IRIDIS group runner")