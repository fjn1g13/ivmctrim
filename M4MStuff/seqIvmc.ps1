# NOTE NOTE: this has diverged from that in the paper dir; the export exps have an IVMCFeedback resolution of 100
param($dir, $end = 120000, $postfix = '120K', $prefix = "IvmcL010Z095", $simend = 400000, $huskyModules="4*4", $ivmcDownsample=5, $rebase=$true, $z2traceeStart = 18000, $z2traceeEnd = 22000, $traceeSeed = 3, $denseEpoch=15000, $ivmcScaleFactor = 0.01)



$baseSize = 0.3
$baseIntervalSize = 0.5 * 0.4 / $baseSize
$ybaseIntervalSize = $baseIntervalSize * 0.5

# (used in figform export code)
# $subSize = "" + [int](55.0 * 0.4 / $baseSize) + "%"

$wsFile = "$dir\wholesamplese$($simend).dat";
$terminal = "$dir\epoch$($simend)saveterminal.dat"


if ($rebase) {
# 1 sample per 500 epochs

$commonPad = 0.04

# four-fig
m4m -plot $wsFile fitness= regcoefs=l end=$end size=$baseSize intervalsizeh=$baseIntervalSize intervalsizev=$ybaseIntervalSize format:generations=0.0E+0 legend= title= out="$dir/rcs$postfix" targetlines=none downsample=50 fig=A padding="NaN;4;NaN;NaN" minpad=$commonPad maxpad=$commonPad format:regcoefs=0.0 title:generations="Episodes"
m4m -plot $wsFile fitness= huskyness=l end=$end size=$baseSize intervalsizeh=$baseIntervalSize intervalsizev=$ybaseIntervalSize format:generations=0.0E+0 title:huskyness="Module Hierarchy" legend= title= out="$dir/husky$postfix" targetlines=none downsample=50 fig=B padding="NaN;4;NaN;NaN" minpad=$commonPad maxpad=$commonPad modulesstring=$huskyModules title:generations="Episodes"
m4m -plot $wsFile fitness=ls end=$end size=$baseSize intervalsizeh=$baseIntervalSize intervalsizev=$ybaseIntervalSize format:generations=0.0E+0 legend= title= out="$dir/fitness$postfix" targetlines=none downsample=50 start=0 fig=C padding="NaN;4;NaN;NaN" minpad=$commonPad maxpad=$commonPad title:generations="Episodes"
m4m -plot "$dir\ivmcswitches_t.dat" end=$end size=$baseSize intervalsizeh=$baseIntervalSize intervalsizev=$ybaseIntervalSize format:x=0.0E+0 legend= title= out="$dir/ivmcswitchness$postfix" meandownsample=$ivmcDownsample title:y="Module Switches" keep=4 plottype=scatter fig=D padding="NaN;4;NaN;NaN" minpad=$commonPad maxpad=$commonPad scalefactor=$ivmcScaleFactor title:x="Episodes"
m4m -plot "$dir\ivmcsolves_t.dat" end=$end size=$baseSize intervalsizeh=$baseIntervalSize intervalsizev=$ybaseIntervalSize format:x=0.0E+0 legend= title= out="$dir/ivmcsolves$postfix" meandownsample=$ivmcDownsample title:y="Freq. Max-Fit. Ph." keep=4 plottype=scatter fig=C padding="NaN;4;NaN;NaN" minpad=$commonPad maxpad=$commonPad scalefactor=$ivmcScaleFactor title:x="Episodes"

# orange line of loathing (win-freq) special case
m4m -plot "$dir\fitnessSat_t(best).dat" end=$end size=$baseSize intervalsizeh=$baseIntervalSize intervalsizev=$ybaseIntervalSize format:x=0.0E+0 legend= title= out="$dir/winfreq$postfix" meandownsample=$ivmcDownsample samplescale=100 title:y="Success Frequency" keep=1 samplethreshold=0.99 plottype=scatter fig=C padding="NaN;4;NaN;NaN" minpad=$commonPad maxpad=$commonPad title:x="Episodes"

m4m -plot $wsFile epoch=$end size=$baseSize intervalsize=$baseIntervalSize legend= title= out="$dir/g$postfix" forceaspect title:generations="Episodes"

# tracees (new)
$traceeCentre = 24000
$traceeStart = $traceeCentre - 100
$traceeEnd = $traceeCentre + 200

m4m -extract $wsFile epoch=$denseEpoch outdir=$dir expfile="$dir/epoch0savestart.dat" postfix=dense appendGeneration=false appendEpoch=false
$dense = "$dir\epoch0savedense.dat"
m4m -tracee $dense epochs=50 seed=$traceeSeed postfix=Dense
m4m -plot "$dir\traceeDense.dat" size=$baseSize intervalsizeh=0.8 legend= title= out="$dir/traceeDense" targetlines=none fitness=l padding="NaN;4;NaN;NaN" fig=A min:fitness=0.2 max:fitness=1.0 format:generations=0.0E+0 minpad:generations=$commonPad maxpad:generations=$commonPad dropmode=droplast title:generations="Evolutionary Steps"
m4m -plot "$dir\traceeDense.dat" start=$traceeStart end=$traceeEnd size=$baseSize intervalsizeh=0.8 legend= title= out="$dir/traceeDenseZ" targetlines=none fitness=l padding="NaN;4;NaN;NaN" fig=C min:fitness=0.2 max:fitness=1.0 annotations="lx $traceeCentre" minpad:generations=$commonPad maxpad:generations=$commonPad dropmode=droplast title:generations="Evolutionary Steps"
m4m -plot "$dir\traceeDense.dat" start=$traceeStart end=$traceeEnd retime=-100 size=$baseSize intervalsizeh=0.8 legend= title=Dense out="$dir/traceeDenseZA" targetlines=none fitness=l padding="NaN;4;NaN;NaN" fig=A min:fitness=0.0 max:fitness=1.0 annotations="lx 0" minpad:generations=$commonPad maxpad:generations=$commonPad dropmode=droplast title:generations="Evolutionary Steps" titlefontsize=15
m4m -plot "$dir\traceeDense.dat" out="$dir/traceeDenseZ2A" title=Dense fig=A fitness=lo downsample=100 start=$z2traceeStart end=$z2traceeEnd retime=0 majorgridlinestyle:generations=solid targetlines=none legend= min:fitness=0.0 max:fitness=1.0 width=504 Height=179 dropmode=droplast title:generations="Evolutionary Steps" titlefontsize=15

m4m -tracee $terminal epochs=50 seed=$traceeSeed postfix=Husky
m4m -plot "$dir\traceeHusky.dat" size=$baseSize intervalsizeh=0.8 legend= title= out="$dir/traceeHusky" targetlines=none fitness=l padding="NaN;4;NaN;NaN" fig=B min:fitness=0.2 max:fitness=1.0 format:generations=0.0E+0 minpad:generations=$commonPad maxpad:generations=$commonPad dropmode=droplast title:generations="Evolutionary Steps"
m4m -plot "$dir\traceeHusky.dat" start=$traceeStart end=$traceeEnd size=$baseSize intervalsizeh=0.8 legend= title= out="$dir/traceeHuskyZ" targetlines=none fitness=l padding="NaN;4;NaN;NaN" fig=D min:fitness=0.2 max:fitness=1.0 annotations="lx $traceeCentre" minpad:generations=$commonPad maxpad:generations=$commonPad dropmode=droplast title:generations="Evolutionary Steps"
m4m -plot "$dir\traceeHusky.dat" start=$traceeStart end=$traceeEnd retime=-100 size=$baseSize intervalsizeh=0.8 legend= title=Hierarchical out="$dir/traceeHuskyZB" targetlines=none fitness=l padding="NaN;4;NaN;NaN" fig=B min:fitness=0.0 max:fitness=1.0 annotations="lx 0" minpad:generations=$commonPad maxpad:generations=$commonPad dropmode=droplast title:generations="Evolutionary Steps" titlefontsize=15
m4m -plot "$dir\traceeHusky.dat" out="$dir/traceeHuskyZ2B" title=Hierarchical fig=B fitness=lo downsample=100 start=$z2traceeStart end=$z2traceeEnd retime=0 majorgridlinestyle:generations=solid targetlines=none legend= min:fitness=0.0 max:fitness=1.0 width=504 Height=179 dropmode=droplast title:generations="Evolutionary Steps" titlefontsize=15


# big RCS/composites

m4m -plot $wsFile fitness= regcoefs=l end=$end size=0.8 intervalsize=0.8 format:generations=0.0E+0 legend= title= out="$dir/rcs$($postfix)_big" targetlines=none downsample=50

}