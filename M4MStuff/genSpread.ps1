param(
  $topDir,
  $name,
  $epochs,
  $xtitle,
  $ytitle,
  $xprefix,
  $yprefix,
  $xs,
  $ys,
  $paramStrFunc,
  $runName = "One",
  $wsperiod = 100,
  $paramFile,
  $nogen=$false,
  $moreParams=""
)

<# Generates experiments and scripts to run, start, and post-process them.

$topdir the directory in which to generate everything
$name the name of the spread
$epochs the number of epochs to run
$xtitle the outer title of the spread
$ytitle the inner title of the spread
$xprefix the outer prefix of the spread
$yprefix the inner prefix pf the spread
$xs the list of x values
$ys the list of y values
$paramStrFunc a function taking the x, y parameters that returns the params string
$runName the name of the run to prepare in the starter file
$wsperiod the wholesample sample period
$paramFile the file from which to take the basic parameters
$nogen set to $true so that experiments are not generate, only starter files etc.
$moreParams an additional params string

#>

mkdir $topDir

# saves a file as utf8 with no BOM
function save($fname)
{
  $Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding($False);
  $fname = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($fname);
  [System.IO.File]::WriteAllText($fname, $input -join "`n", $Utf8NoBomEncoding);
}

# removes the last character from a string (used to strip trailing delimiters)
function clip($s)
{
  return $s.Substring(0, $s.Length - 1)
}

$grpName = "grp$($name).sh";
$postName = "post$($name).sh";
$psGrpName = "psGrp$($name).ps1";
$psPostName = "psPost$($name).ps1";
$setname = $name;

mkdir "$topDir/logs"
mkdir "$topDir/logs/$name"
$starter = "";
$psStarter = "";

$postFitnesses = ""
$postSatFitnesses = ""
$postSubGroups = ""

$expFileName = "epoch0savestarter.dat"

$seed = 42;
$xAliases = "";
foreach ($xxa in $xs) { # x
  $x = $xxa[0]
  $xa = $xxa[1]
  
  $lepochs = $epochs;
  $lplotperiod = $epochs # [math]::Round($lepochs / 10);

  $groupName = "$($name)$($xprefix)"
  $dirname = "$($groupName)$($xa)"
  $dirstarter = "sbatch $grpName $dirname $runName $seed $expFileName $setname $lplotperiod $lepochs";
  $starter += $dirstarter + "`n"
  $dirpsstarter = ". ./$psGrpName $name/$dirname $runName $seed";
  $psStarter += $dirpsstarter + "`r`n"
  
  $xAliases += "$($dirname)runs$($runName) $x`r`n";

  $postFitnesses += "trajTracesFitness$($xa).dat;"
  $postSatFitnesses += "trajTracesSatFitness$($xa).dat;"
  $postSubGroups += "$($xa);"

  $yAliases = "";
  foreach ($yya in $ys) { # y
    $y = $yya[0]
    $ya = $yya[1]

    $paramStr = $paramStrFunc.invoke($x, $y)

    Write-Output "$xtitle = $x ($xprefix = $xa); $ytitle = $y ($yprefix = $ya)"
    Write-Output "  $paramStr"

    $subRunName = "$($yprefix)$($ya)";
    $subRunDir = "$($dirname)/$($subRunName)";

    $yAliases += "$($subRunName) $ya`r`n"

    if (!$nogen) {
      m4m gen=composedense topdir="$topDir/$name" name=$($subRunDir) epochs=$epochs paramfile="$paramFile" params=$paramStr params=$moreParams
    }
  }

  $seed += 100;
}


# aliases
$xAliases | save "$topDir/xAliases.txt"
$yAliases | save "$topDir/yAliases.txt"


# IRIDIS (batch) starter
$starter | save "$topDir/starter$name.sh"

# PS start (sequential starter)
$psStarter | save "$topDir/psStarter$name.ps1"


# Posts
$postFitnesses = clip($postFitnesses)
$postSatFitnesses = clip($postSatFitnesses)
$postSubGroups = clip($postSubGroups)

$post = @"
m4m bestiary dir=$($name) min=-3 max=3 out=bg_tl.png gname=genome_t.dat
m4m bestiary dir=$($name) min=-3 max=3 out=bg_tl2.png gname=genome_t.dat font="DejaVu Sans" axes=true cw=2 ch=2 xTitle=$($xtitle) yTitle=$($ytitle)
m4m bestiary dir=$($name) min=-3 max=3 out=bg_t.png gname=genome_t.dat labels=false
m4m -stackgroup . groupname=$($groupName) TrajFilename="fitness_t(best).dat" prefix=trajTracesFitness
m4m -stackgroup . groupname=$($groupName) TrajFilename="fitnessSat_t(best).dat" prefix=trajTracesSatFitness

~$~files="$postFitnesses"
~$~satfiles="$postSatFitnesses"
~$~labels="$postSubGroups"

m4m -plot trajtraces -files `$files -label `$labels out=trajs title=$($name) title:y=Fitness
m4m -plot trajtraces -files `$satfiles -label `$labels out=trajsSat title="$($name) SAT" title:y=Fitness
m4m -plot trajtraces -files `$files -label `$labels out=trajBoxplot_t epoch=$($epochs-1) color=black title=$($name) title:y=Fitness
m4m -plot trajtraces -files `$satfiles -label `$labels out=trajBoxplot_t epoch=$($epochs-1) color=black title="$($name) SAT" title:y=Fitness

m4m -plot GroupPostWinFreq dir=. -groupname $($name) expfilename=epoch$($epochs)saveterminal.dat title:x="$($xtitle)" title:y="Frequency" size=0.5 title="Mean frequency global optimum is found" padding="NaN;4;NaN;NaN" -out RunonWinFreq
m4m -plot GroupTimeToHierarchy dir=. -groupname $($name) rcsfilename="rcs_t(best).dat" title:x="$($xtitle)" title:y="Epochs" size=0.5 title="Mean time to hierarchy" padding="NaN;4;NaN;NaN" -out TTH modules=4*4 boxplot
"@

$post.Replace("~$~", "").Replace("`r", "") | save "$topDir/$postName"
$post.Replace("~$~", "$") | save "$topDir/$psPostName"

$nodeCount = [Math]::Min(40, ($ys | Measure-Object).Count)
$pwd = (pwd).Path

# IRIDIS group runner
$grp = "`#!/bin/bash`n`n#SBATCH --ntasks-per-node=$($nodeCount)     `# Tasks per node" + @"

#SBATCH --nodes=1                # Number of nodes requested
#SBATCH --time=48:00:00          # walltime

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=ALL

# send mail to this address
#SBATCH --mail-user=fjn1g13@soton.ac.uk

# usage: . ./ivmcgrp.sh directory postfix seed expfilename setname plotperiod totalepochs wholesampleperiod

setname=`$5
plotperiod=`$6
epochs=`$7
wsperiod=10
~/tools/dotnet/dotnet ~/raw/Code/Other/M4M_MkI/M4M.CoreRunner/bin/Release/netcoreapp2.1/M4M.CoreRunner.dll run=$pwd/`$setname/`$setname/`$1 postfix=`$2 traceperiod=0 wholesampleperiod=`$wsperiod wholesamplewriteperiod=`$epochs plotperiod=`$plotperiod saveperiod=`$plotperiod saveperiodepochs=100000 saveperiodseconds=7200 seed=`$3 expfilename=`$4 configurators="IvmcDenseFeedback;SatFitnessFeedback" ivmcswitchplotperiod=`$epochs ivmcswitchresolution=100 ivmcrecordpropers=false > $pwd/`$setname/logs/`$setname/`$1runs`$2log.txt
"@.Replace("`r", "")

$grp | save "$topDir/$grpName"


# PS quick and dirty group runner
$psGrp = @"
param(`$setDir, `$postfix, `$seed)
`$plotperiod=$([Int]($epochs/10))
`$epochs=$epochs
`$wsperiod=10
m4m run=`$setDir postfix=`$postfix traceperiod=0 wholesampleperiod=`$wsperiod wholesamplewriteperiod=`$epochs plotperiod=`$plotperiod saveperiod=`$plotperiod saveperiodepochs=`$plotperiod saveperiodseconds=7200 seed=`$seed expfilename=$expFileName configurators="IvmcDenseFeedback;SatFitnessFeedback" ivmcswitchplotperiod=`$epochs ivmcswitchresolution=100 ivmcrecordpropers=false
m4m -stackgroup . groupname=$($groupName) TrajFilename="fitness_t(best).dat" prefix=trajTracesFitness
m4m -stackgroup . groupname=$($groupName) TrajFilename="fitnessSat_t(best).dat" prefix=trajTracesSatFitness
"@.Replace("`r", "")

$psGrp | save "$topDir/$psGrpName"