# IVMCTrim

This is a (somewhat) minimal implementation of a HillClimber/IVMC experiment from M4M. It runs exactly the same experiments given the same parameters (which requires a couple of weird implementations details, but nothing too painful).

If you want to reproduce the exact figures from the paper, then look at the readme in the M4MStuff directory.

If you want the joy of using M4M for something novel, then you must face the readme in the M4MCode/M4M_Mk1 directory.

## Dependencies

### Runtime

Everything depends on .NET Core 2.1, .NET Core 3.1, or .NET 5.0. You need an SDK for one of these in order to run it from the source code (which is what is supplied in this repository): a Runtime alone will not let you build the program.

You can download the latest .NET SDK for Windows/Linux/MaxOS from https://dotnet.microsoft.com/download

It will probably run on .NET Framework 4.something with minor changes, but not tested.

### Libraries

IVMCTrim depends on the [MathNET.Numerics Version 4.9.1](https://www.nuget.org/packages/MathNet.Numerics/4.9.1) and [OxyPlot.Core Version 2.0.0](https://www.nuget.org/packages/OxyPlot.Core/2.0.0) nuget packages. Exact versions probably don't matter, but not tested.

## Running

Run the command

    dotnet run --configuration Release --framework <framework>

where `<framework>` is an appropriate selection from `netcoreapp2.1`/`netcoreapp3.1`/`net5.0` in the `IVMCTrim` directory (the directory containing `IVMCTrim.csproj`). Run

    dotnet --list-sdks

to determine which SDKS you have installed. For example, if you have `5.0.201` installed, then you could use the framework moniker `net5.0`.

This will run each of the experiments called in the `Program.Main` method. Each experiment has an output filename, a set of parameters, and a seed. The seeds are those that were used to generate the figures in the paper. Sequential seeds were used for the replicates (e.g. seeds 42-81 were used for the Z=0.00 experiments).

## Notes

The idea behind IVMCTrim is that it is possible to understand/mess with the model quickly, instead of having to trawl through M4M looking for the right place to plug in an `IComposer` or whatever, only to find that your code doesn't serialise properly. If you want to understand the model or just want to reproduce qualatitive, IVMCTrim is the place to look. If you want to perform extensive analysis, then you will want M4M.

In order to ensure the simulations are exactly the same as those run under M4M, the order and method by which random numbers are generated must be exactly the same. This ordering is fairly easy to see in IVMCTrim (less so in M4M).

.NET does not enforce a consistent floating point implementation across platforms, so it is possible that the same seed will produce different outputs on different machines; however, this has not been observed on a cross section of modern and not-so-modern computers.

## Help / More / Contact

If you run into trouble or have questions then contact fjn1g13@soton.ac.uk.

## FAQ

Q: What is an IVMC? A: It's the code name (the name used in code) for environments with the mix of single/dual peaked environmental instances. I think it stood for Independentaly Varying Modular Constraints originally, but I can't remember.

Q: What is M4M? A: It's the name of the software IVMCTrim minimcs. I think it stood for Modular, 4 Modules, but again, I can't remember.