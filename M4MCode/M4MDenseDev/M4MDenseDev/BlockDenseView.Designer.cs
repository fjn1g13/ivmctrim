﻿namespace M4MDenseDev
{
    partial class BlockDenseView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.NetworkCheck = new System.Windows.Forms.CheckBox();
            this.MultiCheck = new System.Windows.Forms.CheckBox();
            this.DeclutterCheck = new System.Windows.Forms.CheckBox();
            this.AddCheck = new System.Windows.Forms.CheckBox();
            this.DeltaCheck = new System.Windows.Forms.CheckBox();
            this.AdditiveBox = new System.Windows.Forms.NumericUpDown();
            this.TargetDropdown = new System.Windows.Forms.ComboBox();
            this.DeltaModeDropdown = new System.Windows.Forms.ComboBox();
            this.DeltaResultModeDropdown = new System.Windows.Forms.ComboBox();
            this.FitnessLabel = new System.Windows.Forms.Label();
            this.MouseLabel = new System.Windows.Forms.Label();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.DtmLabel = new System.Windows.Forms.Label();
            this.SpringyCheck = new System.Windows.Forms.CheckBox();
            this.InnerPanel = new M4MDenseDev.DoubleBufferedPanel();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AdditiveBox)).BeginInit();
            this.SuspendLayout();
            // 
            // SaveFileDialog
            // 
            this.SaveFileDialog.DefaultExt = "dat";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.SpringyCheck);
            this.flowLayoutPanel1.Controls.Add(this.NetworkCheck);
            this.flowLayoutPanel1.Controls.Add(this.MultiCheck);
            this.flowLayoutPanel1.Controls.Add(this.DeclutterCheck);
            this.flowLayoutPanel1.Controls.Add(this.AddCheck);
            this.flowLayoutPanel1.Controls.Add(this.DeltaCheck);
            this.flowLayoutPanel1.Controls.Add(this.AdditiveBox);
            this.flowLayoutPanel1.Controls.Add(this.TargetDropdown);
            this.flowLayoutPanel1.Controls.Add(this.DeltaModeDropdown);
            this.flowLayoutPanel1.Controls.Add(this.DeltaResultModeDropdown);
            this.flowLayoutPanel1.Controls.Add(this.FitnessLabel);
            this.flowLayoutPanel1.Controls.Add(this.MouseLabel);
            this.flowLayoutPanel1.Controls.Add(this.SaveBtn);
            this.flowLayoutPanel1.Controls.Add(this.DtmLabel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(124, 209);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // NetworkCheck
            // 
            this.NetworkCheck.AutoSize = true;
            this.NetworkCheck.Location = new System.Drawing.Point(3, 26);
            this.NetworkCheck.Name = "NetworkCheck";
            this.NetworkCheck.Size = new System.Drawing.Size(92, 17);
            this.NetworkCheck.TabIndex = 4;
            this.NetworkCheck.Text = "Network View";
            this.NetworkCheck.UseVisualStyleBackColor = true;
            this.NetworkCheck.CheckedChanged += new System.EventHandler(this.NetworkCheck_CheckedChanged);
            // 
            // MultiCheck
            // 
            this.MultiCheck.AutoSize = true;
            this.MultiCheck.Enabled = false;
            this.MultiCheck.Location = new System.Drawing.Point(3, 49);
            this.MultiCheck.Name = "MultiCheck";
            this.MultiCheck.Size = new System.Drawing.Size(88, 17);
            this.MultiCheck.TabIndex = 5;
            this.MultiCheck.Text = "Multi-Arrange";
            this.MultiCheck.UseVisualStyleBackColor = true;
            this.MultiCheck.CheckedChanged += new System.EventHandler(this.MultiCheck_CheckedChanged);
            // 
            // DeclutterCheck
            // 
            this.DeclutterCheck.AutoSize = true;
            this.DeclutterCheck.Enabled = false;
            this.DeclutterCheck.Location = new System.Drawing.Point(3, 72);
            this.DeclutterCheck.Name = "DeclutterCheck";
            this.DeclutterCheck.Size = new System.Drawing.Size(69, 17);
            this.DeclutterCheck.TabIndex = 6;
            this.DeclutterCheck.Text = "Declutter";
            this.DeclutterCheck.UseVisualStyleBackColor = true;
            this.DeclutterCheck.CheckedChanged += new System.EventHandler(this.DeclutterCheck_CheckedChanged);
            // 
            // AddCheck
            // 
            this.AddCheck.AutoSize = true;
            this.AddCheck.Location = new System.Drawing.Point(3, 95);
            this.AddCheck.Name = "AddCheck";
            this.AddCheck.Size = new System.Drawing.Size(99, 17);
            this.AddCheck.TabIndex = 0;
            this.AddCheck.Text = "Additive Mouse";
            this.AddCheck.UseVisualStyleBackColor = true;
            this.AddCheck.CheckedChanged += new System.EventHandler(this.AddCheck_CheckedChanged);
            // 
            // DeltaCheck
            // 
            this.DeltaCheck.AutoSize = true;
            this.DeltaCheck.Enabled = false;
            this.DeltaCheck.Location = new System.Drawing.Point(3, 118);
            this.DeltaCheck.Name = "DeltaCheck";
            this.DeltaCheck.Size = new System.Drawing.Size(86, 17);
            this.DeltaCheck.TabIndex = 7;
            this.DeltaCheck.Text = "Delta Mouse";
            this.DeltaCheck.UseVisualStyleBackColor = true;
            // 
            // AdditiveBox
            // 
            this.AdditiveBox.DecimalPlaces = 3;
            this.AdditiveBox.Enabled = false;
            this.AdditiveBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.AdditiveBox.Location = new System.Drawing.Point(3, 141);
            this.AdditiveBox.Name = "AdditiveBox";
            this.AdditiveBox.Size = new System.Drawing.Size(115, 20);
            this.AdditiveBox.TabIndex = 0;
            this.AdditiveBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // TargetDropdown
            // 
            this.TargetDropdown.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.TargetDropdown.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.TargetDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TargetDropdown.DropDownWidth = 300;
            this.TargetDropdown.FormattingEnabled = true;
            this.TargetDropdown.Location = new System.Drawing.Point(3, 167);
            this.TargetDropdown.Name = "TargetDropdown";
            this.TargetDropdown.Size = new System.Drawing.Size(115, 21);
            this.TargetDropdown.TabIndex = 0;
            this.TargetDropdown.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.TargetDropdown_Format);
            this.TargetDropdown.SelectedValueChanged += new System.EventHandler(this.TargetDropdown_SelectedValueChanged);
            // 
            // DeltaModeDropdown
            // 
            this.DeltaModeDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DeltaModeDropdown.FormattingEnabled = true;
            this.DeltaModeDropdown.Location = new System.Drawing.Point(3, 194);
            this.DeltaModeDropdown.Name = "DeltaModeDropdown";
            this.DeltaModeDropdown.Size = new System.Drawing.Size(115, 21);
            this.DeltaModeDropdown.TabIndex = 0;
            this.DeltaModeDropdown.SelectedIndexChanged += new System.EventHandler(this.DeltaModeDropdown_SelectedIndexChanged);
            this.DeltaModeDropdown.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.DeltaModeDropdown_Format);
            // 
            // DeltaResultModeDropdown
            // 
            this.DeltaResultModeDropdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DeltaResultModeDropdown.FormattingEnabled = true;
            this.DeltaResultModeDropdown.Location = new System.Drawing.Point(3, 221);
            this.DeltaResultModeDropdown.Name = "DeltaResultModeDropdown";
            this.DeltaResultModeDropdown.Size = new System.Drawing.Size(115, 21);
            this.DeltaResultModeDropdown.TabIndex = 8;
            this.DeltaResultModeDropdown.SelectedIndexChanged += new System.EventHandler(this.DeltaResultModeDropdown_SelectedIndexChanged);
            // 
            // FitnessLabel
            // 
            this.FitnessLabel.AutoSize = true;
            this.FitnessLabel.Location = new System.Drawing.Point(3, 245);
            this.FitnessLabel.Name = "FitnessLabel";
            this.FitnessLabel.Size = new System.Drawing.Size(0, 13);
            this.FitnessLabel.TabIndex = 0;
            // 
            // MouseLabel
            // 
            this.MouseLabel.AutoSize = true;
            this.MouseLabel.Location = new System.Drawing.Point(3, 258);
            this.MouseLabel.Name = "MouseLabel";
            this.MouseLabel.Size = new System.Drawing.Size(0, 13);
            this.MouseLabel.TabIndex = 0;
            // 
            // SaveBtn
            // 
            this.SaveBtn.Location = new System.Drawing.Point(3, 274);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(115, 34);
            this.SaveBtn.TabIndex = 2;
            this.SaveBtn.Text = "Save Genome";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // DtmLabel
            // 
            this.DtmLabel.AutoSize = true;
            this.DtmLabel.Location = new System.Drawing.Point(3, 311);
            this.DtmLabel.Name = "DtmLabel";
            this.DtmLabel.Size = new System.Drawing.Size(0, 13);
            this.DtmLabel.TabIndex = 3;
            // 
            // SpringyCheck
            // 
            this.SpringyCheck.AutoSize = true;
            this.SpringyCheck.Location = new System.Drawing.Point(3, 3);
            this.SpringyCheck.Name = "SpringyCheck";
            this.SpringyCheck.Size = new System.Drawing.Size(61, 17);
            this.SpringyCheck.TabIndex = 9;
            this.SpringyCheck.Text = "Springy";
            this.SpringyCheck.UseVisualStyleBackColor = true;
            this.SpringyCheck.CheckedChanged += new System.EventHandler(this.SpringyCheck_CheckedChanged);
            // 
            // InnerPanel
            // 
            this.InnerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InnerPanel.DoubleBuffered = false;
            this.InnerPanel.Location = new System.Drawing.Point(124, 0);
            this.InnerPanel.Name = "InnerPanel";
            this.InnerPanel.Size = new System.Drawing.Size(213, 209);
            this.InnerPanel.TabIndex = 0;
            this.InnerPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.InnerPanel_Paint);
            this.InnerPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.InnerPanel_MouseDown);
            this.InnerPanel.MouseLeave += new System.EventHandler(this.InnerPanel_MouseLeave);
            this.InnerPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.InnerPanel_MouseMove);
            this.InnerPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.InnerPanel_MouseUp);
            this.InnerPanel.Resize += new System.EventHandler(this.InnerPanel_Resize);
            // 
            // BlockDenseView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.InnerPanel);
            this.Controls.Add(this.flowLayoutPanel1);
            this.DoubleBuffered = true;
            this.Name = "BlockDenseView";
            this.Size = new System.Drawing.Size(337, 209);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AdditiveBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DoubleBufferedPanel InnerPanel;
        private System.Windows.Forms.CheckBox AddCheck;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.SaveFileDialog SaveFileDialog;
        private System.Windows.Forms.Label FitnessLabel;
        private System.Windows.Forms.Label MouseLabel;
        private System.Windows.Forms.Label DtmLabel;
        private System.Windows.Forms.CheckBox NetworkCheck;
        private System.Windows.Forms.CheckBox MultiCheck;
        private System.Windows.Forms.CheckBox DeclutterCheck;
        private System.Windows.Forms.ComboBox TargetDropdown;
        private System.Windows.Forms.NumericUpDown AdditiveBox;
        private System.Windows.Forms.ComboBox DeltaModeDropdown;
        private System.Windows.Forms.CheckBox DeltaCheck;
        private System.Windows.Forms.ComboBox DeltaResultModeDropdown;
        private System.Windows.Forms.CheckBox SpringyCheck;
    }
}
