﻿namespace M4MDenseDev
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.BlockView = new M4MDenseDev.BlockDenseView();
            this.TrajectoryView = new M4MDenseDev.TrajectoryView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.BlockView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TrajectoryView);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 413;
            this.splitContainer1.TabIndex = 1;
            // 
            // BlockView
            // 
            this.BlockView.DenseContext = null;
            this.BlockView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BlockView.Location = new System.Drawing.Point(0, 0);
            this.BlockView.Name = "BlockView";
            this.BlockView.Size = new System.Drawing.Size(413, 450);
            this.BlockView.TabIndex = 0;
            // 
            // TrajectoryView
            // 
            this.TrajectoryView.DenseContext = null;
            this.TrajectoryView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TrajectoryView.Location = new System.Drawing.Point(0, 0);
            this.TrajectoryView.Name = "TrajectoryView";
            this.TrajectoryView.Size = new System.Drawing.Size(383, 450);
            this.TrajectoryView.TabIndex = 0;
            // 
            // TestForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Name = "TestForm";
            this.Text = "TestForm";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.TestForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestForm_DragEnter);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BlockDenseView BlockView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private TrajectoryView TrajectoryView;
    }
}