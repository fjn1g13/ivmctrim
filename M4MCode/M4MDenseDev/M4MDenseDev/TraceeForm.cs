﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M4MDenseDev
{
    public partial class TraceeForm : Form
    {
        public TraceeForm()
        {
            InitializeComponent();
            TraceeView.Moused += TraceeView_Moused;
        }

        private DenseContext CurrentContext = null;
        private TestForm ContextForm = null;

        private void TraceeView_Moused(long time, TraceeSample closestSample)
        {
            DisplayGenome(time, closestSample);
        }

        private void DisplayGenome(long time, TraceeSample closestSample)
        {
            //this.Text = $"Time: {time}; {closestSample.Time}";

            int size = DenseTraceeInfo.Size;

            this.Text = closestSample.Time + ";" + closestSample.DevelopmentalTransformationMatrix.Length;

            if (CurrentContext == null || ContextForm.IsDisposed)
            {
                CurrentContext = Examples.CreateDefaultContext(size);
                ContextForm = Forms.ShowDenseContext("Default of Size " + size, CurrentContext);
            }
            
            if (TraceeView.ShowRegCoefs && closestSample.DevelopmentalTransformationMatrix != null)
            {
                var dtm = MathNet.Numerics.LinearAlgebra.CreateMatrix.DenseOfMatrix(MathNet.Numerics.LinearAlgebra.CreateMatrix.SparseOfRowMajor(size, size, closestSample.DevelopmentalTransformationMatrix));
                CurrentContext.Genome.CopyOverTransMat(dtm);
            }

            if (TraceeView.ShowInitialStates && closestSample.InitialState != null)
            {
                var isv = MathNet.Numerics.LinearAlgebra.CreateVector.DenseOfArray(closestSample.InitialState);
                CurrentContext.Genome.CopyOverInitialState(isv);
            }
            
            CurrentContext.NotifyChange(DenseContextThing.Genome);
        }

        private void TraceeForm_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files)
            {
                try
                {
                    if (System.IO.File.Exists(file))
                    {
                        var fileName = System.IO.Path.GetFileName(file);

                        if (fileName.StartsWith("ist"))
                        {
                            continue; // ignore
                        }
                        if (fileName.StartsWith("rcs"))
                        {
                            continue; // ignore
                        }
                        else
                        {
                            var data = M4M.State.GraphSerialisation.Read<object>(file);

                            if (data is M4M.PopulationExperiment<M4M.DenseIndividual> denseExp)
                            {
                                if (CurrentContext == null)
                                    DisplayGenome(TraceeView.DenseTraceeInfo.Samples[0].Time, TraceeView.DenseTraceeInfo.Samples[0]);

                                var oldContext = CurrentContext;
                                CurrentContext = DenseContext.FromPopulationExperiment(denseExp);
                                CurrentContext.Genome = oldContext.Genome;
                                ContextForm.DenseContext = CurrentContext;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
        }

        private void TraceeForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        public DenseTraceeInfo DenseTraceeInfo
        {
            get => TraceeView.DenseTraceeInfo;
            set
            {
                TraceeView.DenseTraceeInfo = value;
            }
        }
    }
}
