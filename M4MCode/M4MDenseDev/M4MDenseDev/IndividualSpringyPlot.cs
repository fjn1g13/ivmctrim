﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using M4M;
using MathNet.Numerics.LinearAlgebra;

namespace M4MDenseDev
{
    public struct Vec2
    {
        public Vec2(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; }
        public double Y { get; }

        public static Vec2 operator +(Vec2 l, Vec2 r)
        {
            return new Vec2(l.X + r.X, l.Y + r.Y);
        }

        public static Vec2 operator -(Vec2 l, Vec2 r)
        {
            return new Vec2(l.X - r.X, l.Y - r.Y);
        }

        public static Vec2 operator *(Vec2 v, double c)
        {
            return new Vec2(v.X * c, v.Y * c);
        }

        public static Vec2 operator /(Vec2 v, double c)
        {
            return new Vec2(v.X / c, v.Y / c);
        }

        public double LengthSqr => X * X + Y * Y;
        public double Length => Math.Sqrt(LengthSqr);
        public Vec2 Normalise() => this / Length;
        public Vec2 Rot90() => new Vec2(-Y, X);

        public Vec2 Rotate(double radians)
        {
            var c = Math.Cos(radians);
            var s = Math.Sin(radians);
            return new Vec2(c * X - s * Y, s * X + c * Y);
        }

        public static Vec2 Polar(double radians, double length)
        {
            return new Vec2(Math.Cos(radians), Math.Sin(radians)) * length;
        }

        public double Dot(Vec2 other)
        {
            return X * other.X + Y * other.Y;
        }
    }

    public class Connection
    {
        public Connection(SpringyNode other, double strength)
        {
            Other = other ?? throw new ArgumentNullException(nameof(other));
            Strength = strength;
        }

        public SpringyNode Other { get; }
        public double Strength { get; set; }
    }

    public class RotatrySubNode
    {
        public RotatrySubNode(double weight, double distance, double angle)
        {
            Weight = weight;
            Distance = distance;
            Angle = angle;
            AngularVelocity = 0;
        }

        public double Weight { get; set; }
        public double Distance { get; set; }
        public double AngularVelocity { get; set; }
        public double Angle { get; set; }

        public Vec2 Offset => Vec2.Polar(Angle, Distance);
        public Vec2 DirectionOfTravel => Vec2.Polar(Angle + Math.PI / 2, 1);
    }

    public class SpringyNode
    {
        public SpringyNode(int id, double weight, double selfConnectionNodeOffset, double selfConnectionNodeWeight)
        {
            Id = id;
            Weight = weight;

            Connections = new List<Connection>();
            SelfConnection = new RotatrySubNode(selfConnectionNodeWeight, selfConnectionNodeOffset, 0);
        }

        public List<Connection> Connections { get; }
        public int Id { get; }
        public double Weight { get; set; }
        public Vec2 Location { get; set; }
        public Vec2 Velocity { get; set; }
        public double InitialExpression { get; set; }
        public double PhenotypicExpression { get; set; }

        public RotatrySubNode SelfConnection { get; set; }
    }

    public class Springy
    {
        public bool FancyArrows { get; set; } = true;

        public float NodeRadius { get; set; } = 15;
        public float ArrowSize { get; set; } = 5;
        public IColours InitialStateColours { get; set; } = LerpColours.WHot(-1.0, 1.0);
        public IColours DtmColours { get; set; } = LerpColours.WHot(-5.0, 5.0);
        public IColours PhenotypeColours { get; set; } = LerpColours.WHot(-1.0, 1.0);

        public List<SpringyNode> Nodes { get; } = new List<SpringyNode>();
        public int Size => Nodes.Count;

        public double RepulsionStrength { get; set; } = 800;
        public double AttrationStrength { get; set; } = 0.1;

        public bool Springness { get; set; } = true;
        public bool Momentum { get; set; } = true;
        public double Damping { get; set; } = 0.001;
        public double RsnDamping { get; set; } = 0.002;
        public double CentralAttraction = 0.1;

        public List<int> Highlights { get; set; } = new List<int>();
        public Vector<double> Target { get; set; }
        public bool ShowG { get; set; } = true;
        public bool Shapes { get; set; } = true;

        public Springy(int size)
        {
            for (int i = 0; i < size; i++)
            {
                Nodes.Add(new SpringyNode(i, 1, NodeRadius * 2, 1));
            }

            for (int i = 0; i < size; i++)
            {
                var n = Nodes[i];

                for (int j = 0; j < size; j++)
                {
                    n.Connections.Add(new Connection(Nodes[j], 1));
                }
            }
        }

        public Springy(Matrix<double> transMat, Vector<double> g, Vector<double> p)
            : this(g.Count)
        {
            Update(transMat, g, p);
        }

        public Springy(DenseIndividual individual)
            : this(individual.Genome.Size)
        {
            Update(individual);
        }

        public void Update(DenseIndividual individual, double strengthRescale = 1.0)
        {
            Update(individual.Genome.TransMat, individual.Genome.InitialState, individual.Phenotype.Vector, strengthRescale);
        }

        public void Update(Matrix<double> transMat, Vector<double> g, Vector<double> p, double strengthRescale = 1.0)
        {
            foreach (var n in Nodes)
            {
                n.InitialExpression = g[n.Id];
                n.PhenotypicExpression = p[n.Id];

                foreach (var c in n.Connections)
                {
                    c.Strength = transMat[c.Other.Id, n.Id] * strengthRescale;
                }
            }
        }

        public void Step(double dt)
        {
            var force = new Vec2[Nodes.Count];
            var rsnforce = new Vec2[Nodes.Count];
            foreach (var n in Nodes)
            {
                // attraction to centre
                force[n.Id] -= n.Location * CentralAttraction;

                // attration to other nodes
                foreach (var c in n.Connections)
                {
                    if (c.Other == n)
                        continue;

                    var mag = AttrationStrength * Math.Abs(c.Strength);
                    var dir = Springness
                        ? (c.Other.Location - n.Location)
                        : (c.Other.Location - n.Location).Normalise();
                    force[n.Id] += dir * mag;
                    force[c.Other.Id] -= dir * mag;
                }

                // repulsion from other nodes
                foreach (var o in Nodes)
                {
                    if (o == n)
                        continue;

                    var dir = o.Location - n.Location;
                    var l2 = dir.LengthSqr / RepulsionStrength;
                    force[n.Id] -= dir / l2;

                    // rsn
                    var rsndir = o.Location - (n.Location + n.SelfConnection.Offset);
                    l2 = rsndir.LengthSqr / RepulsionStrength;
                    rsnforce[n.Id] -= dir / l2;
                }
            }

            Random rnd = null;
            foreach (var n in Nodes)
            {
                if (double.IsNaN(force[n.Id].X) || double.IsInfinity(force[n.Id].X) || double.IsNaN(n.Location.X) || double.IsInfinity(n.Location.X)
                    || double.IsNaN(force[n.Id].Y) || double.IsInfinity(force[n.Id].Y) || double.IsNaN(n.Location.Y) || double.IsInfinity(n.Location.Y))
                {
                    rnd = rnd ?? new Random();
                    n.Location = new Vec2(rnd.NextDouble() - 0.5, rnd.NextDouble() - 0.5) * 100;
                    n.Velocity = new Vec2(0, 0);
                    n.SelfConnection.AngularVelocity = 0;
                    n.SelfConnection.Angle = rnd.NextDouble() * Math.PI * 2;
                    continue;
                }

                if (double.IsNaN(n.SelfConnection.AngularVelocity))
                {
                    n.SelfConnection.AngularVelocity = 0;
                    continue;
                }

                if (Momentum)
                {
                    n.Velocity = n.Velocity * (1 - Damping) + force[n.Id] / n.Weight * dt;
                    n.Location += n.Velocity * dt;
                }
                else
                {
                    n.Location += force[n.Id] / n.Weight * dt;
                }

                // rsn
                var angularAcc = (rsnforce[n.Id] - force[n.Id]).Dot(n.SelfConnection.DirectionOfTravel) / n.SelfConnection.Weight / n.SelfConnection.Distance;
                if (Momentum)
                {
                    n.SelfConnection.AngularVelocity = (1 - RsnDamping) * n.SelfConnection.AngularVelocity + angularAcc * dt;
                    n.SelfConnection.Angle += n.SelfConnection.AngularVelocity * dt;
                }
                else
                {
                    n.SelfConnection.Angle += angularAcc * dt;
                }
            }
        }

        public void Draw(Graphics g, Point origin, double scale)
        {
            PointF map(Vec2 loc) => new PointF((float)(loc.X * scale + origin.X), (float)(loc.Y * scale + origin.Y));
            int alpha(double x) => Math.Max(0, Math.Min(255, (int)(Math.Abs(x) * 255)));

            var ah = new ArrowHelper() { NodeRadius = NodeRadius };

            // highlights
            var highlighter = Brushes.Yellow;
            foreach (var hn in Highlights)
            {
                var n = Nodes[hn];
                var centre = map(n.Location);
                var rect = new RectangleF(centre.X - NodeRadius * 1.5f, centre.Y - NodeRadius * 1.5f, NodeRadius * 3, NodeRadius * 3);
                g.FillEllipse(highlighter, rect);
            }

            // arrows
            using (var penCacheA = new DictionaryCache<double, Pen>(c => new Pen(Color.FromArgb(Math.Abs(c) < 1 ? 0 : 255, (c < 0 ? Color.Red : Color.Blue)), (float)Math.Floor(c))))
            using (var penCacheB = new DictionaryCache<double, Pen>(c => new Pen(Color.FromArgb(alpha(Math.Abs(c) - Math.Truncate(Math.Abs(c))), (c < 0 ? Color.Red : Color.Blue)), (float)Math.Ceiling(c))))
                foreach (var n in Nodes)
                {
                    foreach (var c in n.Connections)
                    {
                        var penA = penCacheA.Grab(c.Strength);
                        var penB = penCacheB.Grab(c.Strength);

                        if (FancyArrows)
                        {
                            ah.DrawArrow(g, map(n.Location), map(c.Other.Location), penA, n.SelfConnection.Angle);
                            ah.DrawArrow(g, map(n.Location), map(c.Other.Location), penB, n.SelfConnection.Angle);
                        }
                        else
                        {
                            if (c.Other == n)
                                continue;

                            var dir = (c.Other.Location - n.Location).Normalise();
                            var s = n.Location + dir * NodeRadius;
                            var e = c.Other.Location - dir * NodeRadius;
                            var avec = dir * ArrowSize;
                            var a0 = e - avec - avec.Rot90();
                            var a1 = e - avec + avec.Rot90();

                            g.DrawLine(penA, map(s), map(e));
                            g.DrawLine(penB, map(s), map(e));
                            g.DrawLines(penA, new PointF[] { map(a0), map(e), map(a1) });
                            g.DrawLines(penB, new PointF[] { map(a0), map(e), map(a1) });
                        }
                    }
                }

            // nodes
            using (var npen = new Pen(Color.Black, 2))
            using (var brushCache = new DictionaryCache<Color, Brush>(c => new SolidBrush(c)))
            using (var penCache = new DictionaryCache<Color, Pen>(c => new Pen(c, 2)))
                foreach (var n in Nodes)
                {
                    var centre = map(n.Location);

                    var rect = new RectangleF(centre.X - NodeRadius, centre.Y - NodeRadius, NodeRadius * 2, NodeRadius * 2);
                    
                    if (Shapes)
                    {
                        if (Target != null)
                        {
                            var tup = Target[n.Id] > 0;
                            var ts = Math.Abs(Target[n.Id]);

                            var pup = n.PhenotypicExpression > 0;
                            var ps = Math.Abs(n.PhenotypicExpression);

                            var pen = tup == pup
                                ? penCache.Grab(Color.Green)
                                : penCache.Grab(Color.Red);

                            DrawPolygon(g, centre, 3, NodeRadius * (float)ts, tup ? 0f : 0.5f, pen);
                            DrawPolygon(g, centre, 3, NodeRadius * (float)ps, pup ? 0f : 0.5f, pen);
                        }
                        else
                        {
                            var pup = n.PhenotypicExpression > 0;
                            var ps = Math.Abs(n.PhenotypicExpression) * NodeRadius;

                            var pen = penCache.Grab(Color.Black);

                            DrawPolygon(g, centre, 3, NodeRadius * (float)ps, pup ? 0f : 0.5f, pen);
                        }
                    }
                    else
                    {
                        g.DrawEllipse(npen, rect);
                        if (Target != null)
                        {
                            var tBrush = brushCache.Grab(InitialStateColours.GetColour(Target[n.Id]));
                            g.FillEllipse(tBrush, rect.X, rect.Y, rect.Width, rect.Height);
                            rect.Inflate(-NodeRadius / 4, -NodeRadius / 4);
                        }

                        var pBrush = brushCache.Grab(PhenotypeColours.GetColour(n.PhenotypicExpression));
                        if (ShowG)
                        {
                            var gBrush = brushCache.Grab(InitialStateColours.GetColour(n.InitialExpression));
                            g.FillPie(gBrush, rect.X, rect.Y, rect.Width, rect.Height, 90, 180);
                            g.FillPie(pBrush, rect.X, rect.Y, rect.Width, rect.Height, 270, 180);
                        }
                        else
                        {
                            g.FillEllipse(pBrush, rect.X, rect.Y, rect.Width, rect.Height);
                        }

                        if (Target != null)
                        {
                            var tAntiPen = penCache.Grab(InitialStateColours.GetColour(-Math.Sign(Target[n.Id])));
                            g.DrawEllipse(tAntiPen, rect);
                        }
                    }
                }
        }

        private static void DrawPolygon(Graphics g, PointF loc, int n, float size, float phase, Pen pen)
        {
            if (n < 3)
                throw new ArgumentOutOfRangeException(nameof(n), n, "n must be no less than 3");

            var points = new PointF[n];
            for (int i = 0; i < n; i++)
            {
                var t = Math.PI * 2.0 * (phase + (double)i / n);
                var dx = Math.Sin(t) * size;
                var dy = Math.Cos(t) * size;
                points[i] = new PointF(loc.X + (float)dx, loc.Y + (float)dy);
            }
            g.DrawPolygon(pen, points);
        }
    }

    public class ArrowHelper
    {
        public float ArrowSplit { get; set; }  = 3;
        public float ArrowForkLength { get; set; }  = 4;
        public float ArrowForkTightness { get; set; }  = 1;
        public float ArrowSpread { get; set; }  = 6;
        public float ArrowOutset { get; set; } = 1.2f;
        public float ArrowSelfDim { get; set; } = 10f;
        public float NodeRadius { get; set; } = 15f;

        public void DrawArrow(Graphics g, PointF from, PointF to, Pen pen, double selfAngle)
        {
            var arrowPoints = Arrow(from, to, selfAngle);
            g.DrawCurve(pen, arrowPoints);

            int li = arrowPoints.Length - 1;
            var l = arrowPoints[li];
            var m = arrowPoints[li - 1];

            l.Fork(m, ArrowForkLength, ArrowForkTightness, out var ka, out var kb);
            g.DrawLine(pen, ka, l);
            g.DrawLine(pen, kb, l);
        }

        public PointF[] Arrow(PointF ci, PointF cj, double selfAngle)
        {
            if (ci == cj)
            {
                return ArrowSelf(ci, new PointF(ci.X + NodeRadius * (float)Math.Cos(selfAngle), ci.Y + NodeRadius * (float)Math.Sin(selfAngle)));
            }
            else
            {
                return ArrowBetween(ci, cj);
            }
        }

        public PointF[] ArrowSelf(PointF ci, PointF fi)
        {
            ci.Fork(fi, NodeRadius * ArrowOutset, 5f, out var ka1, out var kb1);
            ci.Fork(fi, NodeRadius * ArrowOutset + ArrowSelfDim, 5f, out var ka2, out var kb2);

            return new[] { ka1, ka2, kb2, kb1 };
        }

        public PointF[] ArrowBetween(PointF ci, PointF cj)
        {
            var dist = ci.Dist(cj);

            var mid = ci.Mid(cj);
            var dx = ci.X - cj.X;
            var dy = ci.Y - cj.Y;

            ci = ci.Offset((-dx / dist) * NodeRadius * 1.2f, (-dy / dist) * NodeRadius * ArrowOutset);
            cj = cj.Offset((dx / dist) * NodeRadius * 1.2f, (dy / dist) * NodeRadius * ArrowOutset);

            mid = mid.Offset((dy / dist) * ArrowSpread, (-dx / dist) * ArrowSpread);
            ci = ci.Offset((dy / dist) * ArrowSplit, (-dx / dist) * ArrowSplit);
            cj = cj.Offset((dy / dist) * ArrowSplit, (-dx / dist) * ArrowSplit);

            return new PointF[] { ci, mid, cj };
        }

        public static void Fork(PointF l, PointF m, float length, float tighness, out PointF ka, out PointF kb)
        {
            var dx = l.X - m.X;
            var dy = l.Y - m.Y;
            var dist = m.Dist(l);

            ka = l.Offset(((-dx - dy / tighness) / dist) * length, ((-dy + dx / tighness) / dist) * length);
            kb = l.Offset(((-dx + dy / tighness) / dist) * length, ((-dy - dx / tighness) / dist) * length);
        }
    }

    public class IndividualSpringyPlot : IIndividualPlot
    {
        public DeltaResultMode DeltaResultMode { get; set; }

        public Springy Springy { get; set; }

        void IIndividualPlot.Draw(Graphics g, Rectangle b, DenseIndividual individual, DeltaTest optionalDeltaTest)
        {
            UpdateStepAndDraw(g, b, individual, 500);
        }

        public void UpdateStepAndDraw(Graphics g, Rectangle b, DenseIndividual individual, int steps, double strengthRescale = 1.0, Action<Springy> configurator = null)
        {
            Update(individual, strengthRescale, configurator);
            Step(steps);
            Draw(g, b);
        }

        public void Update(DenseIndividual individual, double strengthRescale = 1.0, Action<Springy> configurator = null)
        {
            Update(individual.Genome.TransMat, individual.Genome.InitialState, individual.Phenotype.Vector, strengthRescale, configurator);
        }

        public void Update(Matrix<double> transMat, Vector<double> g, Vector<double> p, double strengthRescale = 1.0, Action<Springy> configurator = null)
        {
            if (Springy == null || g.Count != Springy.Size)
            {
                Springy = new Springy(transMat, g, p);
                configurator?.Invoke(Springy); // well this is hideous
            }

            Springy.Update(transMat, g, p, strengthRescale);
        }

        public void Step(int steps)
        {
            for (int i = 0; i < steps; i++)
            {
                Springy.Step(0.001);
            }
        }

        public void Draw(Graphics g, Rectangle b)
        {
            Springy.Draw(g, new Point(b.X + b.Width / 2, b.Y + b.Height / 2), 1);
        }

        public ContextElementInfo HitTest(PointF p)
        {
            // TODO: implement
            return ContextElementInfo.None;
        }
    }
}
