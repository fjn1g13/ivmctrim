﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using M4M;

namespace M4MDenseDev
{
    public class IndividualBlockPlot : IIndividualPlot
    {
        public float FontSize { get; set; } = 10;
        public float Gap { get; set; } = 0.5f;
        public float BlockDim { get; set; }
        public int GenomeSize { get; private set; } = -1;
        public Rectangle Bounds { get; set; }
        public IColours InitialStateColours { get; set; } = LerpColours.WHot(-1.0, 1.0);
        public IColours DtmColours { get; set; } = LerpColours.WHot(-5.0, 5.0);
        public IColours PhenotypeColours { get; set; } = LerpColours.WHot(-1.0, 1.0);
        public DeltaResultMode DeltaResultMode { get; set; } = DeltaResultMode.Beneficial;

        private PointF GTopLeft;
        private PointF BTopLeft;
        private PointF PTopLeft;

        public void PerformLayout(Rectangle b, int genomeSize)
        {
            Bounds = b;
            GenomeSize = genomeSize;

            float padLeft = 0.0f;
            float padTop = FontSize;
            float PadRight = 15f;
            float padBottom = 0.0f;

            // square blocks
            float blocksX = genomeSize + 2 + Gap * 2;
            float blocksY = genomeSize;

            float dim = Math.Min((b.Width - padLeft - PadRight) / blocksX, (b.Height - padTop - padBottom) / blocksY);
            BlockDim = dim;

            // position
            var topLeft = new PointF(b.X + padLeft + (b.Width - padLeft - PadRight - (blocksX * dim)) / 2, b.Y + padTop + (b.Height - padTop - padBottom - (blocksY * dim)) / 2);
            
            GTopLeft = new PointF(topLeft.X, topLeft.Y);
            BTopLeft = new PointF(topLeft.X + (1.0f + Gap) * BlockDim, topLeft.Y);
            PTopLeft = new PointF(topLeft.X + (GenomeSize + 1.0f + Gap * 2f) * BlockDim, topLeft.Y);
        }
        
        public RectangleF InitialStateBlock(int i)
        {
            return new RectangleF(
                GTopLeft.X,
                GTopLeft.Y + i * BlockDim,
                BlockDim,
                BlockDim);
        }

        public RectangleF DtmBlock(int i, int j)
        {
            return new RectangleF(
                BTopLeft.X + j * BlockDim,
                BTopLeft.Y + i * BlockDim,
                BlockDim,
                BlockDim);
        }

        public RectangleF PhenotypeBlock(int i)
        {
            return new RectangleF(
                PTopLeft.X,
                PTopLeft.Y + i * BlockDim,
                BlockDim,
                BlockDim);
        }

        public void Draw(Graphics g, Rectangle b, M4M.DenseIndividual individual, DeltaTest optionalDeltaTest = null)
        {
            PerformLayout(b, individual.Genome.Size);
            Draw(g, individual, optionalDeltaTest);
        }

        public void Draw(Graphics g, M4M.DenseIndividual individual, DeltaTest optionalDeltaTest = null)
        {
            if (individual.Genome.Size != GenomeSize)
                throw new ArgumentException("Individual is not of the correct size; perform layout first, or use a different overload for Draw", nameof(individual));

            using (var brushCache = new DictionaryCache<Color, Brush>(c => new SolidBrush(c)))
            {
                // draw headings
                using (var font = CreateFont())
                {
                    int N = individual.Genome.Size;
                    g.DrawString("G", font, Brushes.Black, new RectangleF(GTopLeft.X, GTopLeft.Y - FontSize, BlockDim, FontSize));
                    g.DrawString("B", font, Brushes.Black, new RectangleF(BTopLeft.X, PTopLeft.Y - FontSize, BlockDim, FontSize));
                    g.DrawString("P", font, Brushes.Black, new RectangleF(PTopLeft.X, BTopLeft.Y - FontSize, BlockDim, FontSize));

                    // draw blocks
                    for (int i = 0; i < GenomeSize; i++)
                    {
                        g.FillRectangle(brushCache.Grab(InitialStateColours.GetColour(individual.Genome.InitialState[i])), InitialStateBlock(i));

                        for (int j = 0; j < GenomeSize; j++)
                        {
                            var bBlock = DtmBlock(i, j);
                            g.FillRectangle(brushCache.Grab(DtmColours.GetColour(individual.Genome.TransMat[i, j])), bBlock);

                            if (optionalDeltaTest != null)
                            {
                                var deltaResult = DeltaResultMode == DeltaResultMode.Beneficial
                                    ? optionalDeltaTest.GetBeneficial(i, j)
                                    : optionalDeltaTest.GetBest(i, j);
                                string deltaStr = deltaResult.ToSymbol();

                                g.DrawString(deltaStr, font, Brushes.Purple, bBlock.Left, bBlock.Top);
                            }
                        }

                        g.FillRectangle(brushCache.Grab(PhenotypeColours.GetColour(individual.Phenotype[i])), PhenotypeBlock(i));
                    }

                    // draw colour axis
                    float caTop = 5;
                    float caBottom = Bounds.Y + Bounds.Height - 5;
                    float caLeft = Bounds.X + Bounds.Width - 5;

                    float min = -6.0f;
                    float max = 6.0f;
                    int c = 200;
                    for (int di = 0; di <= c; di++)
                    {
                        float dl = (float)di / c;
                        float d = max + dl * (min - max);
                        g.FillRectangle(brushCache.Grab(DtmColours.GetColour(d)), new RectangleF(caLeft, caTop + (caBottom - caTop) * dl, 5, (float)(caBottom - caTop) / c));
                    }
                }
            }
        }

        private Font CreateFont()
        {
            return new Font(FontFamily.GenericMonospace, FontSize, FontStyle.Regular, GraphicsUnit.Pixel);
        }

        public ContextElementInfo HitTest(PointF p)
        {
            if (GenomeSize <= 0)
                return ContextElementInfo.None;

            // naive & inefficient implementation for now
            for (int i = 0; i < GenomeSize; i++)
            {
                if (InitialStateBlock(i).Contains(p))
                    return new ContextElementInfo(ContextElement.InitialState, i, -1);

                for (int j = 0; j < GenomeSize; j++)
                {
                    if (DtmBlock(i, j).Contains(p))
                        return new ContextElementInfo(ContextElement.DevelopmentalTransformationMatrix, i, j);
                }
                
                if (PhenotypeBlock(i).Contains(p))
                    return new ContextElementInfo(ContextElement.Phenotype, i, -1);
            }

            return ContextElementInfo.None;
        }
    }

    public interface IColours
    {
        System.Drawing.Color GetColour(double value);
    }

    public delegate TOut Creator<TIn, TOut>(TIn input);

    public class DictionaryCache<TIn, TOut> : IDisposable where TOut : IDisposable
    {
        private Dictionary<TIn, TOut> Table = new Dictionary<TIn, TOut>();
        public Creator<TIn, TOut> Creator { get; }

        public DictionaryCache(Creator<TIn, TOut> creator)
        {
            Creator = creator;
        }

        public TOut Grab(TIn input)
        {
            if (Table.TryGetValue(input, out var b))
                return b;
            else
            {
                return Table[input] = Creator(input);
            }
        }

        public void Dispose()
        {
            if (Table == null)
                return;

            foreach (var b in Table.Values)
            {
                b.Dispose();
            }

            Table = null;
        }
    }

    public class LerpColours : IColours
    {
        public static LerpColours WHot(double min, double max) => new LerpColours(min, max, Color.Black, Color.White, Color.Blue, Color.Red);
        public static LerpColours BHot(double min, double max) => new LerpColours(min, max, Color.White, Color.Black, Color.Blue, Color.Red);

        public LerpColours(double min, double max, Color minColour, Color maxColour, Color underMinColour, Color overMaxColour)
        {
            if (max <= min)
                throw new ArgumentException("max must be strictly greater than min");

            Min = min;
            Max = max;
            MinColour = minColour;
            MaxColour = maxColour;
            UnderMinColour = underMinColour;
            OverMaxColour = overMaxColour;
        }

        public double Min { get; }
        public double Max { get; }
        public Color MinColour { get; }
        public Color MaxColour { get; }
        public Color UnderMinColour { get; }
        public Color OverMaxColour { get; }

        public Color GetColour(double value)
        {
            if (value > Max)
                return OverMaxColour;
            if (value < Min)
                return UnderMinColour;

            double x = (value - Min) / (Max - Min);
            
            return Color.FromArgb(LerpInt(MinColour.R, MaxColour.R, x), LerpInt(MinColour.G, MaxColour.G, x), LerpInt(MinColour.B, MaxColour.B, x));
        }

        private int LerpInt(int a, int b, double f)
        {
            return (int)(a * (1-f) + b * f);
        }
    }
}
