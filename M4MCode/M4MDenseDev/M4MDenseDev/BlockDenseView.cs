﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using M4M;
using MathNet.Numerics.Random;

namespace M4MDenseDev
{
    public partial class BlockDenseView : UserControl
    {
        private class NullDeltaMode : IDeltaMode
        {
            private NullDeltaMode()
            {
                // nix
            }

            public static readonly NullDeltaMode Instance = new NullDeltaMode();

            public string Name => "(none)";

            public void Apply(DenseGenome genome, int r, int c, double delta)
            {
                throw new InvalidOperationException();
            }

            public double Judge(ModelExecutionContext context, DenseGenome genome, DevelopmentRules drules, JudgementRules jrules, ITarget target, int r, int c, double delta)
            {
                throw new InvalidOperationException();
            }
        }

        private class NullTarget : M4M.ITarget
        {
            private NullTarget()
            {
                // nix
            }

            public static readonly NullTarget Instance = new NullTarget();

            public int Size => throw new InvalidOperationException();

            public string FriendlyName => "";

            public string FullName => "(none)";

            public string Description => throw new InvalidOperationException();

            public double Judge(Phenotype p)
            {
                throw new InvalidOperationException();
            }

            public bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
            {
                throw new InvalidOperationException();
            }

            public bool NextGeneration(RandomSource rand, JudgementRules jrules)
            {
                throw new InvalidOperationException();
            }
        }

        private DenseContext _denseContext;
        public DenseContext DenseContext
        {
            get => _denseContext;
            set
            {
                if (_denseContext != value)
                {
                    if (_denseContext != null)
                        _denseContext.DenseContextUpdated -= DenseContextChanged;

                    _denseContext = value;

                    if (_denseContext != null)
                        _denseContext.DenseContextUpdated += DenseContextChanged;

                    ContextChanged();
                }
            }
        }

        public DeltaTest DeltaTest { get; private set; } = null;

        private IIndividualPlot _individualPlot = new IndividualBlockPlot();
        public IIndividualPlot IndividualPlot
        {
            get => _individualPlot;
            set
            {
                if (value != _individualPlot)
                {
                    _individualPlot = value;
                    InnerPanel.Invalidate();
                }
            }
        }

        private M4M.ITarget _selectedTarget = null;
        public M4M.ITarget SelectedTarget
        {
            get => _selectedTarget;
            set
            {
                value = value == NullTarget.Instance ? null : value;
                if (_selectedTarget != value)
                {
                    _selectedTarget = value;
                    TargetDropdown.SelectedItem = _selectedTarget ?? NullTarget.Instance;

                    RefreshFitness();
                    InnerPanel.Invalidate();
                }
            }
        }

        private IDeltaMode _deltaMode = null;
        public IDeltaMode DeltaMode
        {
            get => _deltaMode;
            set
            {
                value = value == NullDeltaMode.Instance ? null : value;
                if (value != _deltaMode)
                {
                    _deltaMode = value;
                    DeltaModeDropdown.SelectedItem = _deltaMode ?? NullDeltaMode.Instance;

                    RefreshFitness();
                    InnerPanel.Invalidate();
                }
            }
        }

        private DeltaResultMode _deltaResultMode = DeltaResultMode.Beneficial;
        public DeltaResultMode DeltaResultMode
        {
            get => _deltaResultMode;
            set
            {
                if (value != _deltaResultMode)
                {
                    _deltaResultMode = value;
                    DeltaResultModeDropdown.SelectedItem = _deltaResultMode;
                    IndividualPlot.DeltaResultMode = _deltaResultMode;

                    RefreshFitness();
                    InnerPanel.Invalidate();
                }
            }
        }

        public BlockDenseView()
        {
            this.InitializeComponent();
            InnerPanel.DoubleBuffered = true;
            InnerPanel.MouseWheel += InnerPanel_MouseWheel;

            DeltaModeDropdown.Items.Add(NullDeltaMode.Instance);
            DeltaModeDropdown.Items.Add(CellDeltaMode.Instance);
            DeltaModeDropdown.Items.Add(ColumnDeltaMode.Instance);
            DeltaModeDropdown.SelectedItem = NullDeltaMode.Instance;

            DeltaResultModeDropdown.Items.Add(DeltaResultMode.Beneficial);
            DeltaResultModeDropdown.Items.Add(DeltaResultMode.Best);
            DeltaResultModeDropdown.SelectedItem = DeltaResultMode.Beneficial;

            RefreshTargets();
        }

        private void DenseContextChanged(DenseContextThing things)
        {
            ContextChanged();
        }

        private void RefreshFitness()
        {
            if (DenseContext?.Individual?.Phenotype != null)
            {
                try
                {
                    if (SelectedTarget != null && DeltaMode != null)
                    {
                        DeltaTest = DeltaTestHelpers.Test(DenseContext, SelectedTarget, DenseContext.ExperimentConfiguration.ReproductionRules.DevelopmentalTransformationMatrixMutationSize, DeltaMode);
                    }
                    else
                    {
                        DeltaTest = null;
                    }
                }
                catch
                {
                    DeltaTest = null;
                }

                try
                {
                    var g = DenseContext.Individual.Genome;
                    var p = DenseContext.Individual.Phenotype;

                    StringBuilder sb = new StringBuilder();
                    int i = 0;
                    foreach (var t in DenseContext.ExperimentConfiguration.Targets)
                    {
                        if (SelectedTarget == t)
                            sb.Append("> ");

                        var j = M4M.MultiMeasureJudgement.Judge(g, p, DenseContext.ExperimentConfiguration.JudgementRules, t);
                        sb.AppendLine($"f_{i} = {j.CombinedFitness:G5}");
                        i++;
                    }

                    FitnessLabel.Text = sb.ToString().Trim();
                }
                catch
                {
                    FitnessLabel.Text = "(cannot measure fitness)";
                }
            }
            else
            {
                FitnessLabel.Text = "";
                DeltaTest = null;
            }
        }

        private void RefreshClassification()
        {
            try
            {
                int N = DenseContext.ExperimentConfiguration.Size;
                var dtm = DenseContext.Genome.TransMat;
                var threshold = M4M.DtmClassification.ComputeAutoThreshold(dtm, 10);

                // classify
                var dtmInfo = new M4M.DtmInfo(dtm, M4M.DtmClassification.DiscerneTrueModules(dtm, threshold));
                var desc = M4M.Module.DescribeUnsortedCompact(M4M.Module.OrderByTraits(dtmInfo.Modules));

                try
                {
                    // we don't trust this
                    var clas = M4M.DtmClassification.Type1Classify(dtmInfo, threshold);
                    DtmLabel.Text = desc + "\n" + clas;
                }
                catch
                {
                    DtmLabel.Text = desc;
                }
            }
            catch
            {
                DtmLabel.Text = "(cannot classify dtm)";
            }
        }

        private void RefreshTargets()
        {
            TargetDropdown.Items.Clear();
            if (DenseContext == null)
                return;

            try
            {
                TargetDropdown.Items.Add(NullTarget.Instance);
                foreach (var t in DenseContext.ExperimentConfiguration.Targets)
                    TargetDropdown.Items.Add(t);

                if (SelectedTarget != null && DenseContext.ExperimentConfiguration.Targets.Contains(SelectedTarget))
                {
                    TargetDropdown.SelectedItem = SelectedTarget;
                }
                else
                {
                    SelectedTarget = null;
                    TargetDropdown.SelectedItem = NullTarget.Instance;
                }
            }
            catch
            {
            }
        }

        private void ContextChanged()
        {
            RefreshFitness();
            RefreshClassification();
            RefreshTargets();

            InnerPanel.Invalidate();
        }

        private void InnerPanel_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            var b = InnerPanel.ClientRectangle;
            g.Clear(Color.FromArgb(130, 130, 90));

            if (DenseContext == null)
            {
                DrawNullContext(g, b);
            }
            else
            {
                DrawBlockContext(g, b);
            }
        }

        private void DrawNullContext(Graphics g, Rectangle b)
        {
            using (var font = new Font(FontFamily.GenericMonospace, 10))
            {
                g.DrawString("No Dense Context associated with BlockDenseView.", font, Brushes.Black, InnerPanel.ClientRectangle);
            }
        }

        private void DrawBlockContext(Graphics g, Rectangle b)
        {
            IndividualPlot.Draw(g, b, DenseContext.Individual, DeltaTest);
        }

        private void InnerPanel_Resize(object sender, EventArgs e)
        {
            InnerPanel.Invalidate();
        }
        
        private bool IsMouseDown = false;
        public double LeftDrawValue = 1.0;
        public double RightDrawValue = -1.0;
        public double MiddleDrawValue = 0.0;
        private ContextElementInfo MouseLast = ContextElementInfo.None;
        private void InnerPanel_MouseDown(object sender, MouseEventArgs e)
        {
            IsMouseDown = true;
            MouseLast = ContextElementInfo.None;
            MouseApply(e);
            InnerPanel.Focus();
        }

        private void InnerPanel_MouseWheel(object sender, MouseEventArgs e)
        {
            //IndividualBlockPlot.DtmColours = LerpColours.WHot(-5.0, 5.0);
            //InnerPanel.Invalidate();
        }

        private void InnerPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsMouseDown)
                MouseApply(e);
            else
                MouseOver(e);
        }

        private void MouseOver(MouseEventArgs e)
        {
            var element = IndividualPlot.HitTest(new PointF((float)e.Location.X, (float)e.Location.Y));
            MouseOver(element);
        }

        private void MouseOver(ContextElementInfo element)
        {
            if (element.ContextElement == ContextElement.InitialState)
            {
                MouseLabel.Text = $"G[{element.IndexI}] = {DenseContext.Genome.InitialState[element.IndexI]:G5}";
            }
            else if (element.ContextElement == ContextElement.DevelopmentalTransformationMatrix)
            {
                MouseLabel.Text = $"B[{element.IndexI}, {element.IndexJ}] = {DenseContext.Genome.TransMat[element.IndexI, element.IndexJ]:G5}";
            }
            else if (element.ContextElement == ContextElement.Phenotype)
            {
                MouseLabel.Text = $"P[{element.IndexI}] = {DenseContext.Individual.Phenotype[element.IndexI]:G5}";
            }
            else
            {
                MouseLabel.Text = "";
            }
        }

        private void MouseApply(MouseEventArgs e)
        {
            var element = IndividualPlot.HitTest(new PointF((float)e.Location.X, (float)e.Location.Y));
            if (element == MouseLast || (e.Button != MouseButtons.Left && e.Button != MouseButtons.Right && e.Button != MouseButtons.Middle))
                return;

            bool addMode = AddCheck.Checked;
            bool deltaMode = AddCheck.Checked && DeltaCheck.Checked;
            double addModifier = (double)AdditiveBox.Value;

            var DrawValue = e.Button == MouseButtons.Left ? LeftDrawValue
                : e.Button == MouseButtons.Right ? RightDrawValue
                : MiddleDrawValue;

            if (element.ContextElement == ContextElement.InitialState)
            {
                //DenseContext.Genome.InitialState[element.IndexI] = DrawValue;
                
                if (addMode && DrawValue != 0.0)
                    DenseContext.Genome.InitialState[element.IndexI] += DrawValue * addModifier;
                else
                    DenseContext.Genome.InitialState[element.IndexI] = DrawValue;
                DenseContext.NotifyChange(DenseContextThing.Genome);
            }

            if (element.ContextElement == ContextElement.DevelopmentalTransformationMatrix)
            {
                if (deltaMode && DrawValue != 0.0 && DeltaMode != null)
                    DeltaMode.Apply(DenseContext.Genome, element.IndexI, element.IndexJ, DrawValue * addModifier);
                else if (addMode && DrawValue != 0.0)
                    DenseContext.Genome.TransMat[element.IndexI, element.IndexJ] += DrawValue * addModifier;
                else
                    DenseContext.Genome.TransMat[element.IndexI, element.IndexJ] = DrawValue;
                DenseContext.NotifyChange(DenseContextThing.Genome);
            }

            MouseLast = element;
            MouseOver(element);
        }

        private void InnerPanel_MouseUp(object sender, MouseEventArgs e)
        {
            IsMouseDown = false;
        }

        private void InnerPanel_MouseLeave(object sender, EventArgs e)
        {
            IsMouseDown = false;
            MouseLabel.Text = "";
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            var res = SaveFileDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                var filename = SaveFileDialog.FileName;
                M4M.Analysis.SaveGenome(filename, DenseContext.Genome);
            }
            //M4M.Analysis.SaveGenome(filename, DenseContext.Genome);
            //DenseContext.ExperimentConfiguration;
        }

        private void PreparePlot()
        {
            if (SpringyCheck.Checked)
            {
                IndividualPlot = PrepareIndividualSpringyPlot();
            }
            else if (NetworkCheck.Checked)
            {
                IndividualPlot = PrepareIndividualNetworkPlot();
            }
            else
            {
                IndividualPlot = PrepareIndividualBlockPlot();
            }
        }

        private IndividualSpringyPlot PrepareIndividualSpringyPlot()
        {
            return new IndividualSpringyPlot();
        }

        private IndividualNetworkPlot PrepareIndividualNetworkPlot()
        {
            var plot = new IndividualNetworkPlot()
            {
                NodeArranger = MultiCheck.Checked
                    ? new MultiArranger(new CircleArranger() { ZeroAngle = (float)Math.PI / -4f })
                    : (INodeArranger)new CircleArranger(),
                DeltaResultMode = DeltaResultMode
            };

            plot.DeclutterThresholdFactor = DeclutterCheck.Checked ? 10 : -1;

            return plot;
        }

        private IndividualBlockPlot PrepareIndividualBlockPlot()
        {
            var plot = new IndividualBlockPlot()
            {
                DeltaResultMode = DeltaResultMode
            };

            return plot;
        }

        private void NetworkCheck_CheckedChanged(object sender, EventArgs e)
        {
            PreparePlot();

            MultiCheck.Enabled = NetworkCheck.Checked;
            DeclutterCheck.Enabled = NetworkCheck.Checked;
        }

        private void MultiCheck_CheckedChanged(object sender, EventArgs e)
        {
            PreparePlot();
        }

        private void DeclutterCheck_CheckedChanged(object sender, EventArgs e)
        {
            PreparePlot();
        }

        private void AddCheck_CheckedChanged(object sender, EventArgs e)
        {
            AdditiveBox.Enabled = AddCheck.Checked;
            DeltaCheck.Enabled = AddCheck.Checked;
        }

        private void TargetDropdown_SelectedValueChanged(object sender, EventArgs e)
        {
            SelectedTarget = (M4M.ITarget)TargetDropdown.SelectedItem;
        }

        private void TargetDropdown_Format(object sender, ListControlConvertEventArgs e)
        {
            var t = (M4M.ITarget)e.ListItem;
            e.Value = t.FriendlyName + ": " + t.FullName;
        }

        private void DeltaModeDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            DeltaMode = (IDeltaMode)DeltaModeDropdown.SelectedItem;
        }

        private void DeltaModeDropdown_Format(object sender, ListControlConvertEventArgs e)
        {
            var t = (IDeltaMode)e.ListItem;
            e.Value = t.Name;
        }

        private void DeltaResultModeDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            DeltaResultMode = (DeltaResultMode)DeltaResultModeDropdown.SelectedItem;
        }

        private void SpringyCheck_CheckedChanged(object sender, EventArgs e)
        {
            PreparePlot();
        }
    }

    public enum ContextElement
    {
        None,
        InitialState,
        DevelopmentalTransformationMatrix,
        Phenotype,
    }

    public struct ContextElementInfo
    {
        public static readonly ContextElementInfo None = new ContextElementInfo(ContextElement.None, -1, -1);

        public ContextElementInfo(ContextElement contextElement, int indexI, int indexJ) : this()
        {
            ContextElement = contextElement;
            IndexI = indexI;
            IndexJ = indexJ;
        }

        public ContextElement ContextElement { get; }
        public int IndexI { get; }
        public int IndexJ { get; } // only applicable to Dtm

        public static bool operator ==(ContextElementInfo l, ContextElementInfo r)
        {
            return l.ContextElement == r.ContextElement &&
                   l.IndexI == r.IndexI &&
                   l.IndexJ == r.IndexJ;
        }

        public static bool operator !=(ContextElementInfo l, ContextElementInfo r)
        {
            return !(l == r);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ContextElementInfo))
            {
                return false;
            }

            var info = (ContextElementInfo)obj;
            return ContextElement == info.ContextElement &&
                   IndexI == info.IndexI &&
                   IndexJ == info.IndexJ;
        }

        public override int GetHashCode()
        {
            var hashCode = 1638630285;
            hashCode = hashCode * -1521134295 + ContextElement.GetHashCode();
            hashCode = hashCode * -1521134295 + IndexI.GetHashCode();
            hashCode = hashCode * -1521134295 + IndexJ.GetHashCode();
            return hashCode;
        }
    }

    public interface IIndividualPlot
    {
        ContextElementInfo HitTest(PointF p);
        DeltaResultMode DeltaResultMode { get; set; }
        void Draw(Graphics g, Rectangle b, M4M.DenseIndividual individual, DeltaTest optionalDeltaTest = null);
    }
}
