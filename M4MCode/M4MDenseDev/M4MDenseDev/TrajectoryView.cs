﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M4MDenseDev
{
    public partial class TrajectoryView : UserControl
    {
        private DenseContext _denseContext;
        public DenseContext DenseContext
        {
            get => _denseContext;
            set
            {
                if (_denseContext != value)
                {
                    if (_denseContext != null)
                        _denseContext.DenseContextUpdated -= DenseContextChanged;

                    _denseContext = value;
                    
                    if (_denseContext != null)
                        _denseContext.DenseContextUpdated += DenseContextChanged;

                    ContextChanged();
                }
            }
        }
        
        public OxyPlot.WindowsForms.PlotView PlotView { get; private set; }
        
        public TrajectoryView()
        {
            InitializeComponent();
        }

        private void TrajectoryView_Load(object sender, EventArgs e)
        {
            DoLoad();
        }

        private void DoLoad()
        {
            if (PlotView == null)
            {
                PlotView = new OxyPlot.WindowsForms.PlotView();
                PlotView.Dock = DockStyle.Fill;
                this.Controls.Add(PlotView);
            }
        }

        private void DenseContextChanged(DenseContextThing things)
        {
            ContextChanged();
        }

        private void ContextChanged()
        {
            DoLoad();

            if (DenseContext.Trajectories == null)
            {
                PlotView.Model = null;
            }
            else
            {
                PlotView.Model = null; // important
                var plot = M4M.TrajectoryPlotting.PrepareTrajectoriesPlot("", "Developmental Time Steps", "Trait Expression", null, null, null, "x", "y", null, false, false, false);
                M4M.TrajectoryPlotting.PlotTrajectories(plot, "Developmental Trajectories", DenseContext.Trajectories, 1, 0, (OxyPlot.OxyColor?)null, null, 2, 1, M4M.TrajectoryPlotType.Line, "x", "y");
                foreach (var s in plot.Series)
                    s.TrackerFormatString = "Trait " + s.Tag.ToString() + Environment.NewLine + s.TrackerFormatString;
                PlotView.Model = plot;
            }
            this.Invalidate(true);
        }
    }
}
