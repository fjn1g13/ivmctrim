﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M4MDenseDev
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
        }
        
        public DenseContext DenseContext
        {
            get => BlockView.DenseContext;
            set
            {
                BlockView.DenseContext = value;
                TrajectoryView.DenseContext = value;
            }
        }

        private void TestForm_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files)
            {
                try
                {
                    if (System.IO.File.Exists(file))
                    {
                        var fileName = System.IO.Path.GetFileName(file);

                        if (fileName.StartsWith("ist"))
                        {
                            continue; // ignore
                        }
                        else if (fileName.StartsWith("rcs"))
                        {
                            continue; // ignore
                        }
                        else if (fileName.StartsWith("genome"))
                        {
                            var genome = M4M.Analysis.LoadGenome(file);
                            DenseContext.Genome = genome;
                        }
                        else
                        {
                            var data = M4M.State.GraphSerialisation.Read<object>(file);

                            if (data is M4M.PopulationExperiment<M4M.DenseIndividual> denseExp)
                            {
                                var oldContext = DenseContext;
                                DenseContext = DenseContext.FromPopulationExperiment(denseExp);

                                if (oldContext != null)
                                {
                                    DenseContext.Genome = oldContext.Genome;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
        }

        private void TestForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }
    }
}
