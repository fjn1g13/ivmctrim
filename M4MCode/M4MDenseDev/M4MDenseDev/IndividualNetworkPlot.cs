﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using M4M;

namespace M4MDenseDev
{
    public struct NodeArrangement
    {
        public NodeArrangement(PointF position, PointF focus) : this()
        {
            Position = position;
            Focus = focus;
        }

        public PointF Position { get; }
        public PointF Focus { get; }
    }

    public interface INodeArranger
    {
        NodeArrangement[] Arrange(int genomeSize, float arrowOutset, float arrowPadding, RectangleF b, out float nodeRadius);
    }

    public class CircleArranger : INodeArranger
    {
        public float ZeroAngle { get; set; } = 0.0f;

        public NodeArrangement[] Arrange(int genomeSize, float arrowOutset, float arrowPadding, RectangleF b, out float nodeRadius)
        {
            var centre = new PointF(b.X + b.Width / 2f, b.Y + b.Height / 2f);
            var outerRadius = Math.Min(b.Width, b.Height) / 2f;
            nodeRadius = outerRadius / genomeSize;
            outerRadius -= nodeRadius * arrowOutset + arrowPadding; // so we don't go over the edges
            
            NodeArrangement node(int i)
            {
                float radians = (float)(((float)i / genomeSize) * Math.PI * 2) + ZeroAngle;
                float ox = (float)Math.Sin(radians) * outerRadius;
                float oy = -(float)Math.Cos(radians) * outerRadius;
                return new NodeArrangement(new PointF(centre.X + ox, centre.Y + oy), centre);
            }

            return Enumerable.Range(0, genomeSize).Select(node).ToArray();
        }
    }

    public class MultiArranger : INodeArranger
    {
        public MultiArranger(INodeArranger arranger)
        {
            Arranger = arranger ?? throw new ArgumentNullException(nameof(arranger));
        }

        public INodeArranger Arranger { get; } = new CircleArranger();

        public NodeArrangement[] Arrange(int genomeSize, float arrowOutset, float arrowPadding, RectangleF b, out float nodeRadius)
        {
            var sqrt2 = (int)Math.Sqrt(Math.Sqrt(genomeSize));
            if (sqrt2 * sqrt2 * sqrt2 * sqrt2 == genomeSize)
            {
                nodeRadius = -1; // hopefully this will tell us if we mess up
                
                var tall = sqrt2;
                var wide = sqrt2;
                int subSize = genomeSize / (tall * wide);

                List<NodeArrangement> res = new List<NodeArrangement>();

                for (int i = 0; i < tall; i++)
                {
                    for (int j = 0; j < wide; j++)
                    {
                        var subb = new RectangleF(b.X + b.Width / wide * j, b.Y + b.Height / tall * i, b.Width / wide, b.Height / tall);
                        res.AddRange(Arranger.Arrange(subSize, arrowOutset, arrowPadding, subb, out nodeRadius));
                    }
                }

                return res.ToArray();
            }
            else
            {
                return Arranger.Arrange(genomeSize, arrowOutset, arrowPadding, b, out nodeRadius);
            }
        }
    }

    public class IndividualNetworkPlot : IIndividualPlot
    {
        public float FontSize { get; set; } = 10;
        public double DeclutterThresholdFactor { get; set; } = -1;

        public int GenomeSize { get; private set; } = -1;
        public Rectangle Bounds { get; set; }
        public IColours InitialStateColours { get; set; } = LerpColours.WHot(-1.0, 1.0);
        public IColours DtmColours { get; set; } = LerpColours.WHot(-5.0, 5.0);
        public IColours PhenotypeColours { get; set; } = LerpColours.WHot(-1.0, 1.0);
        public DeltaResultMode DeltaResultMode { get; set; } = DeltaResultMode.Beneficial;

        private PointF Centre;
        //private float OuterRadius;
        private float NodeRadius;

        public INodeArranger NodeArranger { get; set; } = new MultiArranger(new CircleArranger());
        private NodeArrangement[] NodeArrangements { get; set; }

        public void PerformLayout(Rectangle b, int genomeSize)
        {
            Bounds = b;
            GenomeSize = genomeSize;

            float padLeft = 0.0f;
            float padTop = 0.0f;
            float PadRight = 15f;
            float padBottom = 0.0f;

            // need to fit n circles in a big space
            float width = b.Width - padLeft - PadRight;
            float height = b.Height - padTop - padBottom;

            Centre = new PointF(b.X + padLeft + width / 2f, b.Y + padTop + height / 2f);

            NodeArrangements = NodeArranger.Arrange(GenomeSize, ArrowOutset, ArrowSelfDim + ArrowForkLength, new RectangleF(b.X + padLeft, b.Y + padTop, width, height), out NodeRadius);
        }

        public PointF NodePosition(int i)
        {
            return NodeArrangements[i].Position;
        }

        public PointF NodeFocus(int i)
        {
            return NodeArrangements[i].Focus;
        }
        
        private const float ArrowSplit = 3;
        private const float ArrowForkLength = 4;
        private const float ArrowForkTightness = 1;
        private const float ArrowSpread = 6;
        private const float ArrowOutset = 1.2f;
        private const float ArrowSelfDim = 10f;
        public PointF[] ArrowBetween(int i, int j)
        {
            if (i == j)
            {
                var ci = NodePosition(i);
                var fi = NodeFocus(i);
                return ArrowSelf(ci, fi);
            }
            else
            {
                var ci = NodePosition(i);
                var cj = NodePosition(j);
                return ArrowBetween(ci, cj);
            }
        }

        public PointF[] ArrowSelf(PointF ci, PointF fi)
        {
            ci.Fork(fi, -NodeRadius * ArrowOutset, 5f, out var ka1, out var kb1);
            ci.Fork(fi, -NodeRadius * ArrowOutset - ArrowSelfDim, 5f, out var ka2, out var kb2);

            return new[] { ka1, ka2, kb2, kb1 };
        }

        public PointF[] ArrowBetween(PointF ci, PointF cj)
        {
            var dist = ci.Dist(cj);

            var mid = ci.Mid(cj);
            var dx = ci.X - cj.X;
            var dy = ci.Y - cj.Y;
            
            ci = ci.Offset((-dx / dist) * NodeRadius * 1.2f, (-dy / dist) * NodeRadius * ArrowOutset);
            cj = cj.Offset((dx / dist) * NodeRadius * 1.2f, (dy / dist) * NodeRadius * ArrowOutset);
            
            mid = mid.Offset((dy / dist) * ArrowSpread, (-dx / dist) * ArrowSpread);
            ci = ci.Offset((dy / dist) * ArrowSplit, (-dx / dist) * ArrowSplit);
            cj = cj.Offset((dy / dist) * ArrowSplit, (-dx / dist) * ArrowSplit);

            return new PointF[] { ci, mid, cj };
        }

        public RectangleF CircleAround(PointF centre, float radius)
        {
            return new RectangleF(
                centre.X - radius,
                centre.Y - radius,
                radius * 2,
                radius * 2);
        }

        public RectangleF EllipseAround(PointF centre, float halfWidth, float halfHeight)
        {
            return new RectangleF(
                centre.X - halfWidth,
                centre.Y - halfHeight,
                halfWidth * 2,
                halfHeight * 2);
        }

        public void Draw(Graphics g, Rectangle b, M4M.DenseIndividual individual, DeltaTest optionalDeltaTest = null)
        {
            PerformLayout(b, individual.Genome.Size);
            Draw(g, individual, optionalDeltaTest);
        }

        public void Draw(Graphics g, M4M.DenseIndividual individual, DeltaTest optionalDeltaTest = null)
        {
            if (individual.Genome.Size != GenomeSize)
                throw new ArgumentException("Individual is not of the correct size; perform layout first, or use a different overload for Draw", nameof(individual));
            
            using (var brushCache = new DictionaryCache<Color, Brush>(c => new SolidBrush(c)))
            using (var penCache = new DictionaryCache<Color, Pen>(c => new Pen(c, 1)))
            using (var font = CreateFont())
            {
                // draw nodes
                for (int i = 0; i < GenomeSize; i++)
                {
                    var ci = NodePosition(i);
                    var rect = CircleAround(ci, NodeRadius);
                    g.DrawEllipse(Pens.Black, rect);
                    var gBrush = brushCache.Grab(InitialStateColours.GetColour(individual.Genome.InitialState[i]));
                    var pBrush = brushCache.Grab(PhenotypeColours.GetColour(individual.Phenotype[i]));
                    g.FillPie(gBrush, rect.X, rect.Y, rect.Width, rect.Height, 90, 180);
                    g.FillPie(pBrush, rect.X, rect.Y, rect.Width, rect.Height, 270, 180);

                    var idStr = "" + i;
                    var idSize = g.MeasureString(idStr, font);
                    var idRect = EllipseAround(ci, idSize.Width / 2, idSize.Height / 2);
                    g.DrawString(idStr, font, Brushes.Purple, idRect);
                }

                var declutterThreshold = DeclutterThresholdFactor < 0
                    ? -1
                    : M4M.DtmClassification.ComputeAutoThreshold(individual.Genome.TransMat, DeclutterThresholdFactor);

                // draw connections
                for (int i = 0; i < GenomeSize; i++)
                {
                    for (int j = 0; j < GenomeSize; j++)
                    {
                        var arrowPoints = ArrowBetween(j, i);
                        var weight = individual.Genome.TransMat[i, j];

                        if (Math.Abs(weight) < declutterThreshold)
                            continue;
                        
                        var bPen = penCache.Grab(DtmColours.GetColour(weight));
                        g.DrawCurve(bPen, arrowPoints);

                        int li = arrowPoints.Length - 1;
                        var l = arrowPoints[li];
                        var m = arrowPoints[li - 1];
                        
                        l.Fork(m, ArrowForkLength, ArrowForkTightness, out var ka, out var kb);
                        g.DrawLine(bPen, ka, l);
                        g.DrawLine(bPen, kb, l);
                        
                        if (optionalDeltaTest != null)
                        {
                            var deltaResult = DeltaResultMode == DeltaResultMode.Beneficial
                                ? optionalDeltaTest.GetBeneficial(i, j)
                                : optionalDeltaTest.GetBest(i, j);
                            string deltaStr = deltaResult.ToSymbol();

                            var deltaSize = g.MeasureString(deltaStr, font);
                            var deltaRect = EllipseAround(m, deltaSize.Width / 2, deltaSize.Height / 2);
                            g.DrawString(deltaStr, font, Brushes.Purple, deltaRect);
                        }
                    }
                }

                // draw colour axis
                float caTop = 5;
                float caBottom = Bounds.Y + Bounds.Height - 5;
                float caLeft = Bounds.X + Bounds.Width - 5;

                float min = -6.0f;
                float max = 6.0f;
                int c = 200;
                for (int di = 0; di <= c; di++)
                {
                    float dl = (float)di / c;
                    float d = max + dl * (min - max);
                    g.FillRectangle(brushCache.Grab(DtmColours.GetColour(d)), new RectangleF(caLeft, caTop + (caBottom - caTop) * dl, 5, (float)(caBottom - caTop) / c));
                }
            }
        }

        private Font CreateFont()
        {
            return new Font(FontFamily.GenericMonospace, FontSize, FontStyle.Regular, GraphicsUnit.Pixel);
        }

        public ContextElementInfo HitTest(PointF p)
        {
            if (GenomeSize <= 0)
                return ContextElementInfo.None;

            // naive & inefficient implementation for now
            for (int i = 0; i < GenomeSize; i++)
            {
                var centre = NodePosition(i);

                if (centre.Dist(p) < NodeRadius)
                {
                    if (p.X < centre.X)
                        return new ContextElementInfo(ContextElement.InitialState, i, -1);
                    else
                        return new ContextElementInfo(ContextElement.Phenotype, i, -1);
                }

                int bestArrow = -1;
                float bestArrowDist = float.MaxValue;
                for (int j = 0; j < GenomeSize; j++)
                {
                    var arrow = ArrowBetween(j, i);
                    var dist = arrow.Last().Dist(p);
                    if (dist < bestArrowDist)
                    {
                        bestArrowDist = dist;
                        bestArrow = j;
                    }
                }

                if (bestArrowDist < ArrowSplit + ArrowForkLength)
                    return new ContextElementInfo(ContextElement.DevelopmentalTransformationMatrix, i, bestArrow);
            }

            return ContextElementInfo.None;
        }
    }

    public static class PointExtensions
    {
        public static float DistSqr(this PointF l, PointF r)
        {
            float dx = l.X - r.X;
            float dy = l.Y - r.Y;
            return dx * dx + dy * dy;
        }

        public static float Dist(this PointF l, PointF r)
        {
            float dx = l.X - r.X;
            float dy = l.Y - r.Y;
            return (float)Math.Sqrt(dx * dx + dy * dy);
        }

        public static PointF Mid(this PointF l, PointF r)
        {
            return new PointF((l.X + r.X) / 2f, (l.Y + r.Y) / 2f);
        }

        public static PointF Offset(this PointF p, float dx, float dy)
        {
            return new PointF(p.X + dx, p.Y + dy);
        }

        public static void Fork(this PointF l, PointF m, float length, float tighness, out PointF ka, out PointF kb)
        {
            var dx = l.X - m.X;
            var dy = l.Y - m.Y;
            var dist = m.Dist(l);

            ka = l.Offset(((-dx - dy / tighness) / dist) * length, ((-dy + dx / tighness) / dist) * length);
            kb = l.Offset(((-dx + dy / tighness) / dist) * length, ((-dy - dx / tighness) / dist) * length);
        }
    }
}
