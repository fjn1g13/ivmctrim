﻿namespace M4MDenseDev
{
    partial class TraceeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TraceeView = new M4MDenseDev.DenseTraceeView();
            this.SuspendLayout();
            // 
            // TraceeView
            // 
            this.TraceeView.DenseTraceeInfo = null;
            this.TraceeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TraceeView.Location = new System.Drawing.Point(0, 0);
            this.TraceeView.Name = "TraceeView";
            this.TraceeView.ShowControls = true;
            this.TraceeView.Size = new System.Drawing.Size(800, 450);
            this.TraceeView.TabIndex = 0;
            // 
            // TraceeForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TraceeView);
            this.Name = "TraceeForm";
            this.Text = "TraceeForm";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.TraceeForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.TraceeForm_DragEnter);
            this.ResumeLayout(false);

        }

        #endregion

        private DenseTraceeView TraceeView;
    }
}