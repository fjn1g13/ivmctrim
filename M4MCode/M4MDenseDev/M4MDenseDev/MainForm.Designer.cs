﻿namespace M4MDenseDev
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.EmptySize = new System.Windows.Forms.NumericUpDown();
            this.ComposeArgs = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySize)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Create";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // EmptySize
            // 
            this.EmptySize.Location = new System.Drawing.Point(12, 12);
            this.EmptySize.Name = "EmptySize";
            this.EmptySize.Size = new System.Drawing.Size(109, 20);
            this.EmptySize.TabIndex = 1;
            this.EmptySize.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // ComposeArgs
            // 
            this.ComposeArgs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComposeArgs.Location = new System.Drawing.Point(12, 84);
            this.ComposeArgs.Multiline = true;
            this.ComposeArgs.Name = "ComposeArgs";
            this.ComposeArgs.Size = new System.Drawing.Size(259, 71);
            this.ComposeArgs.TabIndex = 2;
            this.ComposeArgs.Text = "m4m gen=composedense targetpackage=ivmc:4 Z=0.25 lambda=0.1 name=ivmcexample pops" +
    "ize=1 epochs=1000 regfunc=l1\r\n";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(12, 161);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Compose";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 196);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ComposeArgs);
            this.Controls.Add(this.EmptySize);
            this.Controls.Add(this.button1);
            this.Name = "MainForm";
            this.Text = "M4M Dense Dev";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            ((System.ComponentModel.ISupportInitialize)(this.EmptySize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown EmptySize;
        private System.Windows.Forms.TextBox ComposeArgs;
        private System.Windows.Forms.Button button2;
    }
}

