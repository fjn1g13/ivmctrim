﻿using M4M;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M4MDenseDev
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files)
            {
                try
                {
                    if (System.IO.File.Exists(file))
                    {
                        var fileName = System.IO.Path.GetFileName(file);
                        
                        if (fileName.StartsWith("ist"))
                        {
                            continue; // we will be handled by someone else
                        }
                        else if (fileName.StartsWith("genome"))
                        {
                            var genome = M4M.Analysis.LoadGenome(file);
                            var exampleContext = Examples.CreateDefaultContext(genome.Size);
                            exampleContext.Genome = genome;
                            Forms.ShowDenseContext(file, exampleContext);
                        }
                        else if (fileName.StartsWith("rcs"))
                        {
                            var istFile = files.FirstOrDefault(f => System.IO.Path.GetFileName(f).StartsWith("ist"));
                            var rcs = M4M.Analysis.LoadTrajectories(file, out int samplePeriod);

                            if (istFile != null)
                            {
                                var ist = M4M.Analysis.LoadTrajectories(istFile, out int _istSamplePeriod);
                                if (_istSamplePeriod != samplePeriod)
                                    throw new Exception("Ist and Rcs sampleperiods must match");

                                Forms.ShowTraceeInfo(file, DenseTraceeInfo.FromRcsAndIstInfo(samplePeriod, rcs, ist));
                            }
                            else
                            {
                                Forms.ShowTraceeInfo(file, DenseTraceeInfo.FromRcsInfo(samplePeriod, rcs));
                            }
                        }
                        else
                        {
                            var data = M4M.State.GraphSerialisation.Read<object>(file);

                            if (data is M4M.PopulationExperiment<M4M.DenseIndividual> denseExp)
                            {
                                Forms.ShowDenseContext(file, DenseContext.FromPopulationExperiment(denseExp));
                            }
                            else if (data is M4M.TraceInfo<M4M.DenseIndividual> traceInfo)
                            {
                                Forms.ShowTraceeInfo(file, DenseTraceeInfo.FromTraceInfo(traceInfo, false));
                            }
                            else if (data is List<M4M.WholeSample<M4M.DenseIndividual>> wholesamples)
                            {
                                Forms.ShowTraceeInfo(file, DenseTraceeInfo.FromWholeSamples(wholesamples, false));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                }
            }
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int size = (int)EmptySize.Value;

            var context = Examples.CreateDefaultContext(size);

            Forms.ShowDenseContext("Example of Size " + size, context);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var argLine = ComposeArgs.Text.Replace("\r", "").Replace("\n", " ");

            var clips = new M4M.CliParams(false);
            clips.ConsumeLine(argLine);
            Cli.LoadParamFiles(clips);

            var exp = M4M.ExperimentComposition.Typical.DefaultDenseExperimentComposer.Compose("", TextWriter.Null, clips);

            var context = DenseContext.FromPopulationExperiment(exp);

            Forms.ShowDenseContext(argLine, context);
        }
    }

    public static class Examples
    {
        public static DenseContext CreateDefaultContext(int size)
        {
            var config = M4M.TypicalConfiguration.CreateConfig(
                size,
                M4M.TypicalConfiguration.CreateStandardDevelopmentRules(M4M.DevelopmentRules.TanhHalf),
                M4M.TypicalConfiguration.CreateStandardReproductionRules(2, 2E-5, 1.0, gMutationType: M4M.NoiseType.Binary),
                M4M.TypicalConfiguration.CreateStandardJudgementRules(0.1, M4M.JudgementRules.L1Equivalent),
                null,
                1000,
                1000,
                0,
                new M4M.Misc.Range(-1, 1.0),
                null);

            var genome = M4M.DenseGenome.CreateDefaultGenome(size);

            return new DenseContext(false, 0, config, genome);
        }
    }

    public static class Forms
    {
        public static TestForm ShowDenseContext(string title, DenseContext denceContext)
        {
            TestForm tf = new TestForm();
            tf.Text = title;
            tf.DenseContext = denceContext;
            tf.Show();
            return tf;
        }

        public static TraceeForm ShowTraceeInfo(string title, DenseTraceeInfo denseTraceeInfo)
        {
            TraceeForm tf = new TraceeForm();
            tf.Text = title;
            tf.DenseTraceeInfo = denseTraceeInfo;
            tf.Show();
            return tf;
        }
    }
}
