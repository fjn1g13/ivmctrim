﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;

namespace M4MDenseDev
{
    public delegate void DenseTraceeViewMoused(long time, TraceeSample closestSample);

    public partial class DenseTraceeView : UserControl
    {
        public bool ShowControls
        {
            get => ControlPanel.Visible;
            set => ControlPanel.Visible = value;
        }

        private DenseTraceeInfo _denseTraceeInfo;
        public DenseTraceeInfo DenseTraceeInfo
        {
            get => _denseTraceeInfo;
            set
            {
                if (_denseTraceeInfo != value)
                {
                    _denseTraceeInfo = value;
                    InfoChanged();
                }
            }
        }
        
        public OxyPlot.WindowsForms.PlotView PlotView { get; private set; }
        private PlotModel PlotModel;
        private OxyPlot.Axes.LinearAxis XAxis;
        private OxyPlot.Axes.LinearAxis InitialStateAxis;
        private OxyPlot.Axes.LinearAxis RegCoefAxis;
        private TrajectorySeries[] InitialStateSeries;
        private TrajectorySeries[] RegCoefSeries;
        
        public DenseTraceeView()
        {
            InitializeComponent();
        }

        private void DenseTraceeView_Load(object sender, EventArgs e)
        {
            DoLoad();
        }

        private void DoLoad()
        {
            if (PlotView == null)
            {
                PlotView = new OxyPlot.WindowsForms.PlotView();
                PlotView.Dock = DockStyle.Fill;
                this.Controls.Add(PlotView);
                PlotView.BringToFront();
            }
        }

        private void UpdateUi()
        {
            if (DenseTraceeInfo == null)
            {
                // ui
                IsCheck.Enabled = false;
                RcsCheck.Enabled = false;
            }
            else
            {
                // ui
                IsCheck.Enabled = DenseTraceeInfo.HasInitialStates;
                RcsCheck.Enabled = DenseTraceeInfo.HasRegCoefs;
            }

            if (ShowControls)
                ControlPanel.Invalidate(true);
        }

        private void InfoChanged()
        {
            UpdateUi();
            UpdatePlot();
        }

        private void UpdatePlot()
        {
            DoLoad();
            
            if (DenseTraceeInfo == null)
            {
                // plot
                PlotView.Model = null;
            }
            else
            {
                // plot
                PlotView.Model = null; // important
                PlotNow();
                PlotView.Model = PlotModel;
                PlotView.MouseDown += PlotView_MouseDown;
                PlotView.MouseUp += PlotView_MouseUp;
                PlotView.MouseLeave += PlotView_MouseLeave;
                PlotView.MouseMove += PlotView_MouseMove;
            }

            PlotView.Invalidate();
        }

        private void PlotView_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsMouseDown)
                PlotMouseEvent(e);
        }

        private void PlotView_MouseLeave(object sender, EventArgs e)
        {
            IsMouseDown = false;
        }

        private void PlotView_MouseUp(object sender, MouseEventArgs e)
        {
            IsMouseDown = false;
        }

        public event DenseTraceeViewMoused Moused;
        private bool IsMouseDown = false;
        private void PlotView_MouseDown(object sender, MouseEventArgs e)
        {
            IsMouseDown = true;
            PlotMouseEvent(e);
        }

        private void PlotMouseEvent(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            var time = (long)XAxis.InverseTransform(e.X);

            if (!M4M.Misc.FirstIndex(DenseTraceeInfo.Samples, ts => ts.Time >= time, out var index, out var _))
            {
                index = DenseTraceeInfo.Samples.Length - 1;
            }

            if (index > 0 && (time - DenseTraceeInfo.Samples[index - 1].Time) < (DenseTraceeInfo.Samples[index].Time - time))
                index--;

            var closestSample = DenseTraceeInfo.Samples[index];

            Moused?.Invoke(time, closestSample);
        }

        private void PlotNow()
        {
            PlotModel = new PlotModel();

            XAxis = new OxyPlot.Axes.LinearAxis() { Title = DenseTraceeInfo.TimeMeasure.Name, Position = OxyPlot.Axes.AxisPosition.Bottom, Key = "x" };
            int ytier = 0;
            InitialStateAxis =
                DenseTraceeInfo.HasInitialStates == false ? null :
                new OxyPlot.Axes.LinearAxis() { Title = "Initial State Expression", Position = OxyPlot.Axes.AxisPosition.Left, Key = "is", PositionTier = ytier++ };
            RegCoefAxis =
                DenseTraceeInfo.HasRegCoefs == false ? null :
                new OxyPlot.Axes.LinearAxis() { Title = "Regulation Coeficients", Position = OxyPlot.Axes.AxisPosition.Left, Key = "rcs", PositionTier = ytier++ };
            
            PlotModel.Axes.Add(XAxis);
            if (InitialStateAxis != null)
                PlotModel.Axes.Add(InitialStateAxis);
            if (RegCoefAxis != null)
                PlotModel.Axes.Add(RegCoefAxis);

            if (DenseTraceeInfo.HasInitialStates)
            {
                InitialStateSeries = new TrajectorySeries[DenseTraceeInfo.Size];
                var colours = Colours1D(DenseTraceeInfo.Size, OxyColors.DarkViolet, OxyColors.Violet);
                for (int i = 0; i < DenseTraceeInfo.Size; i++)
                {
                    var trajs = new TrajectorySeries(DenseTraceeInfo.Samples.Where(s => s.InitialState != null).Select(s => new DataPoint(s.Time, s.InitialState[i])).ToList(), TrajectoryDownsampleMode, Math.Max((int)PlotView.ClientArea.Width / 2 + 2, 10));
                    trajs.Color = colours[i];
                    trajs.XAxisKey = "x";
                    trajs.YAxisKey = "is";
                    trajs.Tag = "is" + i;
                    trajs.CanTrackerInterpolatePoints = false;
                    InitialStateSeries[i] = trajs;
                    PlotModel.Series.Add(trajs);
                }
            }
            else
            {
                InitialStateSeries = null;
            }

            if (DenseTraceeInfo.HasRegCoefs)
            {
                var c00 = OxyColors.DarkCyan;
                var c10 = OxyColors.LightCyan;
                var c01 = OxyColors.DarkGreen;
                var c11 = OxyColors.LightGreen;

                RegCoefSeries = new TrajectorySeries[DenseTraceeInfo.Size * DenseTraceeInfo.Size];
                var colours = Colours2D(DenseTraceeInfo.Size, c00, c10, c01, c11);
                for (int i = 0; i < DenseTraceeInfo.Size * DenseTraceeInfo.Size; i++)
                {
                    var trajs = new TrajectorySeries(DenseTraceeInfo.Samples.Where(s => s.DevelopmentalTransformationMatrix != null).Select(s => new DataPoint(s.Time, s.DevelopmentalTransformationMatrix[i])).ToList(), TrajectoryDownsampleMode, Math.Max((int)PlotView.ClientArea.Width / 2 + 2, 10));
                    trajs.Color = colours[i];
                    trajs.XAxisKey = "x";
                    trajs.YAxisKey = "rcs";
                    trajs.Tag = "rcs" + i;
                    trajs.CanTrackerInterpolatePoints = false;
                    RegCoefSeries[i] = trajs;
                    PlotModel.Series.Add(trajs);
                }
            }
            else
            {
                RegCoefSeries = null;
            }

            PlotModel.InvalidatePlot(true);
            RefreshSeriesVisibilty();
        }

        public static OxyColor[] Colours1D(int n, OxyColor c0, OxyColor c1)
        {
            return M4M.TrajectoryPlotting.Colours1D(n, c0, c1);
            // OxyColor[] res = new OxyColor[n];

            // for (int i = 0; i < n; i++)
            // {
            //     var z = (double)i / (n - 1);
            //     res[i] = OxyColor.Interpolate(c0, c1, z);
            // }

            // return res;
        }

        public static OxyColor[] Colours2D(int n, OxyColor c00, OxyColor c10, OxyColor c01, OxyColor c11)
        {
            return M4M.TrajectoryPlotting.Colours2D(n, c00, c10, c01, c11);
            // OxyColor[] res = new OxyColor[n * n];

            // for (int i = 0; i < n; i++)
            // {
            //     for (int j = 0; j < n; j++)
            //     {
            //         var iz = (double)i / (n - 1);
            //         var jz = (double)j / (n - 1);
            //         res[i * n + j] = OxyColor.Interpolate(OxyColor.Interpolate(c00, c10, iz), OxyColor.Interpolate(c01, c11, iz), jz);
            //     }
            // }

            // return res;
        }

        private bool _showRegCoefs = true;
        public bool ShowRegCoefs
        {
            get => _showRegCoefs;
            set
            {
                _showRegCoefs = RcsCheck.Checked = value;
                RefreshSeriesVisibilty();
            }
        }

        private bool _showInitialStates = false;
        public bool ShowInitialStates
        {
            get => _showInitialStates;
            set
            {
                _showInitialStates = IsCheck.Checked = value;
                RefreshSeriesVisibilty();
            }
        }

        private TrajectoryDownsampleMode _trajectoryDownsampleMode = TrajectoryDownsampleMode.Mean;
        public TrajectoryDownsampleMode TrajectoryDownsampleMode
        {
            get => _trajectoryDownsampleMode;
            set
            {
                _trajectoryDownsampleMode = value;
                this.MeanDownsampleCheck.Checked = _trajectoryDownsampleMode == TrajectoryDownsampleMode.Mean;

                RefreshSeriesDownsampling();
            }
        }

        private bool _trackerHidden = false;
        public bool TrackerHidden
        {
            get => _trackerHidden;
            set
            {
                if (_trackerHidden != value)
                {
                    _trackerHidden = value;
                    // TODO: apply to series
                    HideTrackerCheck.Checked = _trackerHidden;
                }
            }
        }

        private void RefreshSeriesVisibilty()
        {
            if (RegCoefSeries != null)
            {
                RegCoefAxis.IsAxisVisible = _showRegCoefs;
                foreach (var s in RegCoefSeries)
                    s.IsVisible = _showRegCoefs;
            }

            if (InitialStateSeries != null)
            {
                InitialStateAxis.IsAxisVisible = _showInitialStates;
                foreach (var s in InitialStateSeries)
                    s.IsVisible = _showInitialStates;
            }

            PlotView.Invalidate();
        }

        private void RefreshSeriesDownsampling()
        {
            if (RegCoefSeries != null)
            {
                RegCoefAxis.IsAxisVisible = _showRegCoefs;
                foreach (var s in RegCoefSeries)
                    s.DownsampleMode = _trajectoryDownsampleMode;
            }

            if (InitialStateSeries != null)
            {
                InitialStateAxis.IsAxisVisible = _showInitialStates;
                foreach (var s in InitialStateSeries)
                    s.DownsampleMode = _trajectoryDownsampleMode;
            }

            PlotView.Invalidate(true);
        }
        
        private void RcsCheck_CheckedChanged(object sender, EventArgs e)
        {
            ShowRegCoefs = RcsCheck.Checked;
        }

        private void IsCheck_CheckedChanged(object sender, EventArgs e)
        {
            ShowInitialStates = IsCheck.Checked;
        }

        private void MeanDownsampleCheck_CheckedChanged(object sender, EventArgs e)
        {
            var downsampleMode = this.MeanDownsampleCheck.Checked ? TrajectoryDownsampleMode.Mean : TrajectoryDownsampleMode.None;
            TrajectoryDownsampleMode = downsampleMode;
        }

        private void HideTrackerCheck_CheckedChanged(object sender, EventArgs e)
        {
            TrackerHidden = HideTrackerCheck.Checked;
        }
    }
}
