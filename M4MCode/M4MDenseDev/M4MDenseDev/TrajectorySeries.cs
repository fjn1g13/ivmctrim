﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace M4MDenseDev
{
    public enum TrajectoryDownsampleMode
    {
        None = 0,
        Skip = 1,
        Mean = 2,
    }

    public class TrajectorySeries : OxyPlot.Series.LineSeries
    {
        public TrajectorySeries(IReadOnlyList<DataPoint> trueDataPoints, TrajectoryDownsampleMode downsampleMode, int maxRenderedSamples=0)
        {
            TrueDataPoints = trueDataPoints;
            DownsampleMode = downsampleMode;
            MaxRenderedSamples = maxRenderedSamples;
            UpdateTrajectory(true);
        }

        public IReadOnlyList<DataPoint> TrueDataPoints { get; set; }
        public TrajectoryDownsampleMode DownsampleMode { get; set; }
        public int MaxRenderedSamples { get; set; }
    
        public override void Render(IRenderContext rc)
        {
            UpdateTrajectory(false);
            base.Render(rc);
        }
        
        private double? PreviousActualMinimum = null;
        private double? PreviousActualMaximum = null;

        public void Invalidate()
        {
            PreviousActualMinimum = null;
            PreviousActualMaximum = null;
        }

        public void UpdateTrajectory(bool extremes)
        {
            if (extremes)
            {
                var extremePoints = new List<DataPoint>();
                extremePoints.Add(M4M.Misc.ArgMin(TrueDataPoints, dp => dp.X));
                extremePoints.Add(M4M.Misc.ArgMax(TrueDataPoints, dp => dp.X));
                extremePoints.Add(M4M.Misc.ArgMin(TrueDataPoints, dp => dp.Y));
                extremePoints.Add(M4M.Misc.ArgMax(TrueDataPoints, dp => dp.Y));

                Invalidate();
                
                this.Points.Clear();
                this.Points.AddRange(extremePoints);
                this.UpdateData();
            }
            else
            {
                this.EnsureAxes();
                var xaxis = this.XAxis;

                if (PreviousActualMinimum == xaxis.ActualMinimum && PreviousActualMaximum == xaxis.ActualMaximum)
                    return; // don't redo if unnecessary

                int min = Math.Max(0, TrueDataPoints.TakeWhile(dp => dp.X < xaxis.ActualMinimum).Count() - 1);
                int max = Math.Min(TrueDataPoints.Count - 1, TrueDataPoints.TakeWhile(dp => dp.X < xaxis.ActualMaximum).Count() + 1);
                int count = max - min + 1;

                PreviousActualMinimum = xaxis.ActualMinimum;
                PreviousActualMaximum = xaxis.ActualMaximum;
                
                var points = new List<DataPoint>();
                
                if (count > 4)
                {
                    int samplePeriod = MaxRenderedSamples == 0 ? 4 : Math.Max(count / MaxRenderedSamples, 1);

                    // normalise min
                    min = Math.Max(0, min - 1);
                    min = min - (min % samplePeriod);

                    // normalise max
                    max = Math.Min(TrueDataPoints.Count - 1, max - 1);
                    max = max + (samplePeriod - max % samplePeriod);
                    max = Math.Min(TrueDataPoints.Count - 1, max);

                    // add datapoints
                    if (DownsampleMode == TrajectoryDownsampleMode.Skip)
                    {
                        for (int i = min; i <= max; i += samplePeriod)
                        {
                            points.Add(TrueDataPoints[i]);
                        }
                    }
                    else if (DownsampleMode == TrajectoryDownsampleMode.Mean)
                    {
                        // push max along 1 more
                        max = Math.Min(TrueDataPoints.Count - 1, max + samplePeriod);
                        max = max - (max % samplePeriod);

                        for (int i = min; i < max; i += samplePeriod)
                        {
                            double t = 0.0;
                            for (int subi = i; subi < i + samplePeriod; subi++)
                                t += TrueDataPoints[subi].Y;

                            double y = t / samplePeriod;
                            points.Add(new DataPoint(TrueDataPoints[i].X, y));
                        }
                    }
                    else
                    {
                        for (int i = min; i <= max; i += 1)
                        {
                            points.Add(TrueDataPoints[i]);
                        }
                    }
                }
                else
                {
                    max = Math.Min(TrueDataPoints.Count - 1, max + 2);
                    min = Math.Max(0, min - 2);

                    for (int i = min; i <= max; i++)
                    {
                        Points.Add(TrueDataPoints[i]);
                    }
                }
                
                this.Points.Clear();
                this.Points.AddRange(points);
                this.UpdateData();
            }
        }
    }
}
