﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using M4M;

namespace M4MDenseDev
{
    [Flags]
    public enum DenseContextThing
    {
        Individual = 1,
        ExperimentConfiguration = 2,
        Seed = 4,
        Epigenetic = 8,
        Genome = 16,
        Everything = Individual | ExperimentConfiguration | Seed | Epigenetic | Genome
    }

    public delegate void DenseContextUpdated(DenseContextThing things);

    public class DenseContext
    {
        public static DenseContext FromPopulationExperiment(M4M.PopulationExperiment<M4M.DenseIndividual> exp)
        {
            var rand = new MathNet.Numerics.Random.MersenneTwister();
            var example = exp.Population.PeekRandom(rand);

            // HACK: reset targets if epoch == 0
            if (exp.Epoch == 0)
            {
                foreach (var t in exp.PopulationConfig.ExperimentConfiguration.Targets)
                {
                    ExposureInformation exposureInformation = new ExposureInformation(exp.PopulationConfig.ExperimentConfiguration.GenerationsPerTargetPerEpoch); // ignored
                    t.NextExposure(rand, exp.PopulationConfig.ExperimentConfiguration.JudgementRules, exp.Epoch, ref exposureInformation);
                }
            }
            
            foreach (var t in exp.PopulationConfig.ExperimentConfiguration.Targets)
                t.NextGeneration(rand, exp.PopulationConfig.ExperimentConfiguration.JudgementRules);

            return new DenseContext(example.Epigenetic, 0, exp.PopulationConfig.ExperimentConfiguration, example.Genome);
        }

        private bool _epigenetic;
        public bool Epigenetic
        {
            get => _epigenetic;
            set
            {
                if (_epigenetic != value)
                {
                    _epigenetic = value;
                    Redevelop(DenseContextThing.Epigenetic);
                }
            }
        }

        private int _seed;
        public int Seed
        {
            get => _seed;
            set
            {
                if (_seed != value)
                {
                    _seed = value;
                    Redevelop(DenseContextThing.Seed);
                }
            }
        }

        private M4M.ExperimentConfiguration _experimentConfiguration;
        public M4M.ExperimentConfiguration ExperimentConfiguration
        {
            get => _experimentConfiguration;
            set
            {
                if (_experimentConfiguration != value)
                {
                    _experimentConfiguration = value;
                    Redevelop(DenseContextThing.ExperimentConfiguration);
                }
            }
        }

        public bool _computeTrajectories = true;
        public bool ComputeTrajectories
        {
            get => _computeTrajectories;
            set
            {
                if (_computeTrajectories != value)
                {
                    _computeTrajectories = value;
                    if (_computeTrajectories)
                        Redevelop(DenseContextThing.Seed); // eh, good enough
                }
            }
        }
        
        private M4M.DenseGenome _genome;

        public double[][] Trajectories { get; private set; }

        public DenseContext(bool epigenetic, int seed, ExperimentConfiguration experimentConfiguration, DenseGenome genome)
        {
            _epigenetic = epigenetic;
            _seed = seed;
            _experimentConfiguration = experimentConfiguration;
            _genome = genome;

            Redevelop(DenseContextThing.Everything);
        }

        public M4M.DenseGenome Genome
        {
            get => _genome;
            set
            {
                if (_genome != value)
                {
                    _genome = value;
                    Redevelop(DenseContextThing.Genome);
                }
            }
        }

        public void NotifyChange(DenseContextThing changes = DenseContextThing.Everything)
        {
            Redevelop(changes);
        }

        public M4M.DenseIndividual Individual { get; private set; }

        public event DenseContextUpdated DenseContextUpdated;

        private void Redevelop(DenseContextThing changes)
        {
            var rand1 = new MathNet.Numerics.Random.MersenneTwister(Seed);
            var modelExecutionContext = new ModelExecutionContext(rand1);
            Individual = M4M.DenseIndividual.Develop(Genome.Clone(modelExecutionContext), modelExecutionContext, ExperimentConfiguration.DevelopmentRules, Epigenetic);
            
            var rand2 = new MathNet.Numerics.Random.MersenneTwister(Seed);

            if (ComputeTrajectories)
            {
                double[][] trajectories = null;
                Genome.DevelopWithTrajectories(rand2, ExperimentConfiguration.DevelopmentRules, ref trajectories);
                Trajectories = trajectories;
            }
            else
            {
                Trajectories = null;
            }

            DenseContextUpdated?.Invoke(changes | DenseContextThing.Individual);
        }

        public DenseContext Clone()
        {
            var rand1 = new MathNet.Numerics.Random.MersenneTwister(Seed);
            var modelExecutionContext = new ModelExecutionContext(rand1);
            var clone = new DenseContext(Epigenetic, Seed, ExperimentConfiguration, Genome.Clone(modelExecutionContext));

            clone.ComputeTrajectories = ComputeTrajectories;
            return clone;
        }
    }
}
