﻿namespace M4MDenseDev
{
    partial class DenseTraceeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.RcsCheck = new System.Windows.Forms.CheckBox();
            this.IsCheck = new System.Windows.Forms.CheckBox();
            this.MeanDownsampleCheck = new System.Windows.Forms.CheckBox();
            this.HideTrackerCheck = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ControlPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ControlPanel
            // 
            this.ControlPanel.AutoSize = true;
            this.ControlPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ControlPanel.Controls.Add(this.groupBox1);
            this.ControlPanel.Controls.Add(this.MeanDownsampleCheck);
            this.ControlPanel.Controls.Add(this.HideTrackerCheck);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ControlPanel.Location = new System.Drawing.Point(0, 0);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(101, 212);
            this.ControlPanel.TabIndex = 2;
            // 
            // RcsCheck
            // 
            this.RcsCheck.AutoSize = true;
            this.RcsCheck.Checked = true;
            this.RcsCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RcsCheck.Location = new System.Drawing.Point(6, 19);
            this.RcsCheck.Name = "RcsCheck";
            this.RcsCheck.Size = new System.Drawing.Size(76, 17);
            this.RcsCheck.TabIndex = 0;
            this.RcsCheck.Text = "Reg Coefs";
            this.RcsCheck.UseVisualStyleBackColor = true;
            this.RcsCheck.CheckedChanged += new System.EventHandler(this.RcsCheck_CheckedChanged);
            // 
            // IsCheck
            // 
            this.IsCheck.AutoSize = true;
            this.IsCheck.Location = new System.Drawing.Point(6, 42);
            this.IsCheck.Name = "IsCheck";
            this.IsCheck.Size = new System.Drawing.Size(83, 17);
            this.IsCheck.TabIndex = 1;
            this.IsCheck.Text = "Initial States";
            this.IsCheck.UseVisualStyleBackColor = true;
            this.IsCheck.CheckedChanged += new System.EventHandler(this.IsCheck_CheckedChanged);
            // 
            // MeanDownsampleCheck
            // 
            this.MeanDownsampleCheck.AutoSize = true;
            this.MeanDownsampleCheck.Location = new System.Drawing.Point(6, 100);
            this.MeanDownsampleCheck.Name = "MeanDownsampleCheck";
            this.MeanDownsampleCheck.Size = new System.Drawing.Size(83, 17);
            this.MeanDownsampleCheck.TabIndex = 1;
            this.MeanDownsampleCheck.Text = "Mean Downsample";
            this.MeanDownsampleCheck.UseVisualStyleBackColor = true;
            this.MeanDownsampleCheck.Checked = true;
            this.MeanDownsampleCheck.CheckedChanged += new System.EventHandler(this.MeanDownsampleCheck_CheckedChanged);
            // 
            // HideTrackerCheck
            // 
            this.HideTrackerCheck.AutoSize = true;
            this.HideTrackerCheck.Location = new System.Drawing.Point(6, 120);
            this.HideTrackerCheck.Name = "HideTrackerCheck";
            this.HideTrackerCheck.Size = new System.Drawing.Size(83, 17);
            this.HideTrackerCheck.TabIndex = 1;
            this.HideTrackerCheck.Text = "Hide Tracker";
            this.HideTrackerCheck.UseVisualStyleBackColor = true;
            this.HideTrackerCheck.Checked = false;
            this.HideTrackerCheck.CheckedChanged += new System.EventHandler(this.HideTrackerCheck_CheckedChanged);
            this.HideTrackerCheck.Enabled = false;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.RcsCheck);
            this.groupBox1.Controls.Add(this.IsCheck);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(95, 78);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lines";
            // 
            // DenseTraceeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ControlPanel);
            this.Name = "DenseTraceeView";
            this.Size = new System.Drawing.Size(369, 212);
            this.Load += new System.EventHandler(this.DenseTraceeView_Load);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.CheckBox RcsCheck;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox IsCheck;
        private System.Windows.Forms.CheckBox MeanDownsampleCheck;
        private System.Windows.Forms.CheckBox HideTrackerCheck;
    }
}
