﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using M4M;

namespace M4MDenseDev
{
    public static class DeltaTestHelpers
    {
        public static DeltaTest Test(DenseContext denseContext, ITarget target, double deltaMag, IDeltaMode deltaMode)
        {
            return DeltaTest.Test(denseContext.Clone().Genome, denseContext.ExperimentConfiguration.DevelopmentRules, denseContext.ExperimentConfiguration.JudgementRules, target, deltaMag, deltaMode);
        }
    }
}
