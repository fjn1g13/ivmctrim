﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4MDenseDev
{
    public interface ITimeMeasure
    {
        string Name { get; }
    }

    public class Generations : ITimeMeasure
    {
        public string Name => "Generations";
    }

    public class Epochs : ITimeMeasure
    {
        public string Name => "Epochs";
    }

    public struct TraceeSample
    {
        public readonly long Time;
        public readonly double[] InitialState;
        public readonly double[] DevelopmentalTransformationMatrix;

        public TraceeSample(long time, double[] initialState, double[] developmentalTransformationMatrix)
        {
            Time = time;
            InitialState = initialState;
            DevelopmentalTransformationMatrix = developmentalTransformationMatrix;
        }
    }

    public class DenseTraceeInfo
    {
        public int Size { get; }
        public TraceeSample[] Samples { get; }
        public ITimeMeasure TimeMeasure { get; }

        public bool HasRegCoefs { get; }
        public bool HasInitialStates { get; }

        public DenseTraceeInfo(int size, TraceeSample[] samples, ITimeMeasure measure)
        {
            Size = size;
            Samples = samples;
            TimeMeasure = measure;
            
            HasRegCoefs = samples.Any(ts => ts.DevelopmentalTransformationMatrix != null);
            HasInitialStates = samples.Any(ts => ts.InitialState != null);
        }

        public static DenseTraceeInfo FromTraceInfo(M4M.TraceInfo<M4M.DenseIndividual> traceInfo, bool epochsx)
        {
            return FromWholeSamples(traceInfo.TraceWholeSamples, epochsx);
        }

        public static DenseTraceeInfo FromWholeSamples(List<M4M.WholeSample<M4M.DenseIndividual>> wholesamples, bool epochsx)
        {
            int size = wholesamples[0].Target.Size;

            var samples = wholesamples.Select(ws =>
                new TraceeSample(
                    epochsx ? ws.Epoch : ws.Generations,
                    ws.Judgements[0].Individual.Genome.InitialState.ToArray(),
                    ws.Judgements[0].Individual.Genome.TransMat.ToRowMajorArray()
                    )).OrderBy(ts => ts.Time).ToArray();

            var measure = epochsx ? (ITimeMeasure)new Epochs() : new Generations();

            return new DenseTraceeInfo(size, samples, measure);
        }

        public static DenseTraceeInfo FromRcsInfo(int samplePeriod, double[][] rcsTrajectories)
        {
            int size = (int)Math.Round(Math.Sqrt(rcsTrajectories.Length));
            var baseRcsSamples = M4M.Misc.TrajectoriesToSamples(rcsTrajectories);
            var samples = baseRcsSamples.Select((dtm, i) => new TraceeSample(i * samplePeriod, null, dtm)).ToArray();

            var measure = new Epochs();

            return new DenseTraceeInfo(size, samples, measure);
        }

        public static DenseTraceeInfo FromRcsAndIstInfo(int samplePeriod, double[][] rcsTrajectories, double[][] istTrajectories)
        {
            int size = (int)Math.Round(Math.Sqrt(rcsTrajectories.Length));
            var baseRcsSamples = M4M.Misc.TrajectoriesToSamples(rcsTrajectories);
            var baseIstSamples = M4M.Misc.TrajectoriesToSamples(istTrajectories);
            var samples = baseRcsSamples.Select((dtm, i) => new TraceeSample(i * samplePeriod, baseIstSamples[i], dtm)).ToArray();

            var measure = new Epochs();

            return new DenseTraceeInfo(size, samples, measure);
        }
    }
}
