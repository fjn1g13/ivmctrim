﻿using M4M;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Random;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MindlessPlotting
{
    class Program
    {
        static void Main(string[] args)
        {
            var fixedG = @"C:\M4MResults\IvmcSplitSingleBVFastRepeatsL010BExZ100FixedG\IvmcSplitSingleBVFastRepeatsL010BExZ100FixedG\IvmcSplitSingleBVFastRepeatsL010BExZ100FixedG\IvmcSplitSingleBVFastRepeatsL010BExZ100FixedGZ100runsOne\r0";
            var ivmcL1 = @"C:\M4MResults\IvmcSplitSingleBVFastRepeatsL010BEx\IvmcSplitSingleBVFastRepeatsL010BEx\IvmcSplitSingleBVFastRepeatsL010BExZ095runsOne\r0";
            var ivmcL0 = @"C:\M4MResults\IvmcSplitSingleBVFastRepeatsBExVaryL\IvmcSplitSingleBVFastRepeatsBExVaryL\IvmcSplitSingleBVFastRepeatsBExVaryLL000runsOne\r0"; // also Z=0.95
            var ivmcL2 = @"C:\M4MResults\IvmcSplitSingleBVFastL2SpreadBEx\IvmcSplitSingleBVFastL2SpreadBEx\IvmcSplitSingleBVFastL2SpreadBExL002runsOne\Z095"; // also Z=0.95
            var ivmcL2Z0 = @"C:\M4MResults\IvmcSplitSingleBVFastL2SpreadBEx\IvmcSplitSingleBVFastL2SpreadBEx\IvmcSplitSingleBVFastL2SpreadBExL002runsOne\Z000"; // also Z=0
            var ivmcL1lowlambda = @"C:\M4MResults\IvmcSplitSingleBVFastRepeatsBExVaryL\IvmcSplitSingleBVFastRepeatsBExVaryL\IvmcSplitSingleBVFastRepeatsBExVaryLL002runsOne\r0";
            var ivmcL1And2 = @"C:\M4MTest\L1And2\VaryLB\VaryLB\VaryLBL010runsOne\B0002";
            var ivmcMMSO = @"C:\M4MResults\IvmcSplitSingleBVFastRepeatsL010BExMMSO\IvmcSplitSingleBVFastRepeatsL010BExMMSO\IvmcSplitSingleBVFastRepeatsL010BExMMSOZ000runsOne\r0"; // Z=0

            GoWeirdIvmcThing(ivmcMMSO, "ivmcMMSO_weird.gif", skip: 2, lastEpoch: 20000, showDev: false);
            GoWeirdIvmcThing(ivmcL2, "ivmcL2_weird.gif", skip: 10, lastEpoch: 100000, showDev: false);
            GoWeirdIvmcThing(ivmcL1And2, "ivmcL1And2_weird.gif", skip: 10, lastEpoch: 100000, showDev: false);
            GoWeirdIvmcThing(ivmcL1, "ivmcL1_weird.gif", skip: 20, lastEpoch: 240000, showDev: false);

            // sample period is 10, so skip is in 10s of epochs
            //Go(fixedG, "fixedG.gif", skip: 5, lastEpoch: 10000, showDev : false); // per paper
            //Go(ivmcL1, "ivmcL1.gif", skip: 50, lastEpoch: 120000, showDev: false); // per paper
            //Go(ivmcL0, "ivmcL0.gif", skip: 200, lastEpoch: 400000, showDev: false); // not in paper
            //Go(ivmcL2, "ivmcL2.gif", skip: 50, lastEpoch: 120000, showDev: false); // not in paper
            //Go(ivmcl1lowlambda, "ivmcl1lowlambda.gif", skip: 50, lastEpoch: 120000, showDev: false); // not in paper

            // we seem to run out of memory if we don't skip some extra stuff here...
            //Go(fixedG, "fixedGT.gif", skip: 10, lastEpoch: 10000, showDev: true); // per paper
            //Go(ivmcL1, "ivmcL1T.gif", skip: 100, lastEpoch: 120000, showDev: true); // per paper
            //Go(ivmcL0, "ivmcL0T.gif", skip: 500, lastEpoch: 400000, showDev: true); // not in paper

            // Tracees
            //GoTracee(ivmcL1, "ivmcL1_tracee.gif", samplePeriod: 1, lastEpoch: 120000, epochs: 5, generationCutoff: 200);
            //GoTracee(ivmcL0, "ivmcL0_tracee.gif", samplePeriod: 1, lastEpoch: 120000, epochs: 5, generationCutoff: 200);
            //GoTracee(ivmcL2, "ivmcL2_tracee.gif", samplePeriod: 1, lastEpoch: 120000, epochs: 5, generationCutoff: 200);
            //GoTracee(ivmcL1, "ivmcL1_traceetest.gif", samplePeriod: 1, lastEpoch: 120000, epochs: 5, generationCutoff: 200);
        }

        static void GoTracee(string dir, string outfile, int samplePeriod, int lastEpoch, int epochs, int generationCutoff)
        {
            var rand = new CustomMersenneTwister(1);
            var wholeSamples = WholeSample<DenseIndividual>.LoadWholeSamples(System.IO.Path.Combine(dir, "wholesamplese400000.dat"));
            var exp = PopulationExperiment<DenseIndividual>.Load(System.IO.Path.Combine(dir, "epoch0savestart.dat"));
            var ctx = new ModelExecutionContext(rand);
            var tracee = M4M.PopulationTrace.RunTrace(Console.Out, ctx, exp.Population, exp.PopulationConfig, exp.PopulationConfig.ExperimentConfiguration.Targets.ToArray(), samplePeriod, exp.Epoch, epochs);

            if (exp.PopulationConfig.ExperimentConfiguration.Targets[0] is M4M.Epistatics.IvmcProperChanging ipc)
            {
                // not good code
                ipc.GetType().GetProperty("Z").SetValue(ipc, 1);
            }

            exp.Population.Clear();
            exp.Population.IntroduceMany(wholeSamples.First(w => w.Epoch == lastEpoch).Judgements.Select(ij => ij.Individual));

            int highlight = -1;
            int totalGenerations = 0;
            int generation = 0;
            int c = 0;
            var benefitSequence = new List<KeyValuePair<int, double>>();
            var individualSequence = new List<KeyValuePair<int, HighlightTargetIndividual>>();
            void feedback(FileStuff stuff, IReadOnlyList<IndividualJudgement<DenseIndividual>> populationJudgements, int epochCount, long generationCount, ITarget target)
            {
                if (generation++ >= generationCutoff)
                {
                    return;
                }
                totalGenerations++;

                if (c++ != 0)
                {
                    if (c == samplePeriod)
                        c = 0;
                    return;
                }
                else if (c == samplePeriod)
                {
                    c = 0;
                }

                var t = target as M4M.IPerfectPhenotypeTarget;
                var pp = t.PreparePerfectP();

                var individual = populationJudgements[0].Individual.Clone(ctx);
                var g = individual.Genome.InitialState;
                for (int i = 0; i < pp.Count; i++)
                {
                    //// high -> correct
                    //g[i] = g[i] * Math.Sign(pp[i]);
                }

                individual.DevelopInplace(ctx, exp.PopulationConfig.ExperimentConfiguration.DevelopmentRules);
                individualSequence.Add(new KeyValuePair<int, HighlightTargetIndividual>(totalGenerations, new HighlightTargetIndividual(individual, highlight, pp)));
                benefitSequence.Add(new KeyValuePair<int, double>(totalGenerations, populationJudgements[0].Judgement.CombinedFitness));
                highlight = -1;
            }

            void reset(FileStuff stuff, Population<DenseIndividual> population, ITarget target, int epoch, long generationCount, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
            {
                // just reset the sampler; we re-compute the perfectP every frame because who really cares
                c = 0;
                generation = 0;
            }

            var mutator = new InitialStateMutationInterceptingMutator();
            mutator.Mutation += h => highlight = h;
            foreach (var individual in exp.Population.PeekAll())
                individual.Genome.SetCustomInitialStateMutator(mutator);
            M4M.PopulationTrace.RunTrace(Console.Out, ctx, null, exp.Population, exp.PopulationConfig, exp.PopulationConfig.ExperimentConfiguration.Targets.ToArray(), feedback, reset, exp.Epoch, epochs);

            var dtm = individualSequence[0].Value.Individual.Genome.TransMat;
            var modules = InferModules(dtm, out _, out var totalWeight);
            var strengthRescale = 41 / totalWeight;

            var plot = FitnessPlot(benefitSequence, -10, totalGenerations + 10);
            MakeSpringyGif(exp, individualSequence, plot, 1800, 800, outfile, 5, lastEpoch, modules, strengthRescale, false, preSpin: 100000, withinSpin: 1, titleFormat: "Generation {0}", false);
        }

        static void GoWeirdIvmcThing(string dir, string outfile, int skip, int lastEpoch, bool showDev)
        {
            Console.WriteLine($"{dir} -> {outfile}");

            var wsFile = System.IO.Directory.GetFiles(dir, "wholesamples*")[0];
            var wholeSamples = WholeSample<DenseIndividual>.LoadWholeSamples(wsFile).Where(ws => ws.Epoch <= lastEpoch).TakeEvery(skip).ToList();

            var targetPackage = M4M.Epistatics.IvmcTargetPackages.IvmcProper1_4x4(M4M.Epistatics.IvmcProperJudgementMode.Split, 1, 0.7, 0, 1);
            var target = targetPackage.Targets[0] as IPerfectPhenotypeTarget;
            var drules = TypicalConfiguration.CreateStandardDevelopmentRules(DevelopmentRules.TanhHalf);
            var jrules = TypicalConfiguration.CreateStandardJudgementRules(0, JudgementRules.ConstantEquivalent);
            var rrules = TypicalConfiguration.CreateStandardReproductionRules(2, 0, 0, true, 1, null, NoiseType.Binary);
            var config = new ExperimentConfiguration(targetPackage.TargetSize, targetPackage.Targets, Cyclers.Loop, 1000, 400, 0, new Misc.Range(-1, 1), false, drules, rrules, jrules);
            var mutator = new InitialStateMutationInterceptingMutator();
            var genome = new DenseGenome(CreateVector.Dense<double>(targetPackage.TargetSize, 0), CreateMatrix.Dense<double>(targetPackage.TargetSize, targetPackage.TargetSize), customInitialStateMutator: mutator);
            var ctx = new ModelExecutionContext(new CustomMersenneTwister(42));
            var individual = DenseIndividual.Develop(genome, ctx, drules, false).Clone(ctx);
            var candidate = individual.Clone(ctx);

            // gonna write the inner loop manually; this is too big a deviation from previous intents
            //var popConfig = new PopulationExperimentConfig<DenseIndividual>(config, SelectorPreparers<DenseIndividual>.Ranked, 1, true, null, null, null);
            //var pop = new Population<DenseIndividual>(new[] { individual });
            //var exp = new PopulationExperiment<DenseIndividual>(pop, popConfig, null);

            var sequence = new List<KeyValuePair<int, HighlightTargetIndividual>>();

            int epochLength = 200;
            int generation = epochLength - 1;
            int epoch = 0;
            int highlight = -1;
            mutator.Mutation += (h => highlight = h);
            ExposureInformation e = new ExposureInformation(epochLength);
            foreach (var ws in wholeSamples)
            {
                if (++generation == epochLength)
                {
                    epoch++;
                    if (epoch >= lastEpoch)
                        break;
                    generation = 0;
                    target.NextExposure(ctx.Rand, jrules, epoch, ref e);
                }

                target.NextGeneration(ctx.Rand, jrules);

                var b = ws.Judgements[0].Individual.Genome.TransMat;
                
                b.CopyTo(individual.Genome.TransMat);
                individual.DevelopInplace(ctx, drules);
                individual.MutateInto(candidate, ctx, drules, rrules);

                var judgement = MultiMeasureJudgement.Judge(individual.Genome, individual.Phenotype, jrules, target);
                var candidateJudgement = MultiMeasureJudgement.Judge(candidate.Genome, candidate.Phenotype, jrules, target);
                if (candidateJudgement.CombinedFitness > judgement.CombinedFitness)
                    (individual, candidate) = (candidate, individual);

                sequence.Add(new KeyValuePair<int, HighlightTargetIndividual>(ws.Epoch, new HighlightTargetIndividual(individual.Clone(ctx), highlight, target.PreparePerfectP())));
            }

            var dtm = wholeSamples.Last().Judgements.First().Individual.Genome.TransMat;
            var modules = InferModules(dtm, out var g, out var totalWeight);
            var strengthRescale = 41 / totalWeight;

            var plot = HuskyPlot(sequence, modules, -1000, lastEpoch + 1000);
            MakeSpringyGif(null, sequence, plot, 1800, 800, outfile, 5, lastEpoch, modules, strengthRescale, showDev, titleFormat: "{0}", showG: false);
        }

        static void Go(string dir, string outfile, int skip, int lastEpoch, bool showDev)
        {
            Console.WriteLine($"{dir} -> {outfile}");

            var wholeSamples = WholeSample<DenseIndividual>.LoadWholeSamples(System.IO.Path.Combine(dir, "wholesamplese400000.dat"));
            var exp = PopulationExperiment<DenseIndividual>.Load(System.IO.Path.Combine(dir, "epoch0savestart.dat"));
            var ctx = new ModelExecutionContext(null);

            OldType(wholeSamples, exp, ctx, skip, lastEpoch, outfile, showDev);
        }

        // this might be doing too much now
        /// <param name="dtm">The DTM from which to infer modules</param>
        /// <param name="demoG">A genotype, where motivators are +1 and everyone else is -1</param>
        /// <param name="totalWeight">The total weight in the DTM</param>
        /// <returns>The inferred modules</returns>
        static M4M.Modular.Modules InferModules(Matrix<double> dtm, out Vector<double> demoG, out double totalWeight)
        {
            double threshold = M4M.DtmClassification.ComputeAutoThreshold(dtm, 10.0);
            var dtmModules = M4M.DtmClassification.DiscerneTrueModules(dtm, threshold);
            var dtmInfo = new M4M.DtmInfo(dtm, dtmModules);
            Console.WriteLine(dtmInfo.Description);

            var modules = new M4M.Modular.Modules(dtmModules.Select(m => m.Motivatees.Concat(m.Motivators).Distinct())); // just lump everyone together: will be unhappy if they overlap

            demoG = CreateVector.Dense(dtm.RowCount, -1.0);

            foreach (var module in dtmModules)
            {
                foreach (var motivator in module.Motivators)
                    demoG[motivator] = +1.0;
            }

            totalWeight = dtm.Enumerate().Sum(Math.Abs);

            return modules;
        }

        static void OldType(List<WholeSample<DenseIndividual>> wholeSamples, PopulationExperiment<DenseIndividual> exp, ModelExecutionContext ctx, int samplePeriod, int lastEpoch, string outfile, bool showDev)
        {
            var dtm = wholeSamples.Last().Judgements.First().Individual.Genome.TransMat;
            var modules = InferModules(dtm, out var g, out var totalWeight);

            DenseIndividual run(DenseIndividual old)
            {
                var genome = old.Genome.Clone(ctx, g);
                return DenseIndividual.Develop(genome, ctx, exp.PopulationConfig.ExperimentConfiguration.DevelopmentRules, old.Epigenetic);
            }

            var sequence = wholeSamples.Where(ws => ws.Epoch <= lastEpoch).TakeEvery(samplePeriod).Select(ws => new KeyValuePair<int, HighlightTargetIndividual>(ws.Epoch, run(ws.Judgements[0].Individual))).ToList();

            var strengthRescale = 41 / totalWeight;

            var plot = HuskyPlot(sequence, modules, -1000, lastEpoch + 1000);
            MakeSpringyGif(exp, sequence, plot, 1800, 800, outfile, 5, lastEpoch, modules, strengthRescale, showDev);
        }

        public static OxyColor[] Colours(OxyColor c0, OxyColor c1, int n)
        {
            var res = new OxyColor[n];

            for (int i = 0; i < res.Length; i++)
            {
                var z = (double)i / Math.Max(1, (res.Length - 1));
                res[i] = OxyColor.Interpolate(c0, c1, z);
            }

            return res;
        }

        public static PlotModel FitnessPlot(IEnumerable<KeyValuePair<int, double>> sequence, int xmin, int xmax)
        {
            var plot = new OxyPlot.PlotModel();
            var x = new OxyPlot.Axes.LinearAxis() { Position = OxyPlot.Axes.AxisPosition.Bottom, Title = "Generation", Minimum = xmin, Maximum = xmax };
            var h = new OxyPlot.Axes.LinearAxis() { Position = OxyPlot.Axes.AxisPosition.Left, Title = "Fitness", Minimum = -0.015, Maximum = 1.015 };
            plot.Axes.Add(x);
            plot.Axes.Add(h);

            var benefits = new ScatterSeries() { MarkerFill = OxyColors.Red, MarkerType = MarkerType.Diamond, MarkerSize = 2 };
            plot.Series.Add(benefits);

            foreach (var s in sequence)
            {
                benefits.Points.Add(new ScatterPoint(s.Key, s.Value));
            }

            return plot;
        }

        public static PlotModel HuskyPlot(IEnumerable<KeyValuePair<int, HighlightTargetIndividual>> sequence, M4M.Modular.Modules modules, int xmin, int xmax)
        {
            var c0 = OxyColors.DarkSlateGray;
            var c1 = OxyColors.SlateGray;
            var colours = Colours(c0, c1, modules.ModuleCount);

            var plot = new OxyPlot.PlotModel();
            var x = new OxyPlot.Axes.LinearAxis() { Position = OxyPlot.Axes.AxisPosition.Bottom, Title = "Epoch", Minimum = xmin, Maximum = xmax, StringFormat = "0.0E+0" };
            var h = new OxyPlot.Axes.LinearAxis() { Position = OxyPlot.Axes.AxisPosition.Left, Title = "Degree of Hierarchy", Minimum = -0.015, Maximum = 1.015 };
            plot.Axes.Add(x);
            plot.Axes.Add(h);

            var lines = colours.Select(c => new LineSeries() { Color = c }).ToArray();

            foreach (var line in lines)
                plot.Series.Add(line);

            foreach (var s in sequence)
            {
                var huskyness = M4M.Analysis.ComputeModuleHuskyness(s.Value.Individual.Genome.TransMat, modules.ModuleAssignments);
                for (int i = 0; i < lines.Length; i++)
                {
                    lines[i].Points.Add(new DataPoint(s.Key, huskyness[i]));
                }
            }

            return plot;
        }

        // duration is centi-seconds, I think
        public static void MakeSpringyGif(PopulationExperiment<DenseIndividual> exp, IEnumerable<KeyValuePair<int, HighlightTargetIndividual>> sequence, PlotModel plot, int width, int height, string outfile, int duration, int lastEpoch, M4M.Modular.Modules modules, double strengthRescale = 1.0, bool showDevTime = false, int preSpin = 5000, int withinSpin = 500, string titleFormat = "Epoch {0}", bool showG = true)
        {
            // init gid rendering
            var font = new System.Drawing.Font(System.Drawing.FontFamily.GenericMonospace, 12, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            var genc = new System.Windows.Media.Imaging.GifBitmapEncoder();
            var bmp = new System.Drawing.Bitmap(width, height);
            var g = System.Drawing.Graphics.FromImage(bmp);

            // init some stuff
            var springy = new M4MDenseDev.IndividualSpringyPlot();

            bool first = true;
            double[][] trajectories = null;

            // render frames
            foreach (var s in sequence)
            {
                Console.WriteLine(s.Key);

                if (first)
                {
                    springy.Update(s.Value.Individual, strengthRescale);
                    springy.Step(preSpin);
                    first = false;
                }

                // render frame
                g.Clear(System.Drawing.Color.White);

                var rc = new OxyPlot.WindowsForms.GraphicsRenderContext(g);
                plot.Title = string.Format(titleFormat, s.Key);
                plot.InvalidatePlot(true);
                plot.Axes[0].FilterMaxValue = s.Key;
                ((IPlotModel)plot).Update(true);
                ((IPlotModel)plot).Render(rc, width - height, height);

                void addFrame()
                {
                    System.IO.MemoryStream bms = new System.IO.MemoryStream();
                    bmp.Save(bms, System.Drawing.Imaging.ImageFormat.Gif);
                    var bmpf = System.Windows.Media.Imaging.BitmapFrame.Create(bms);
                    genc.Frames.Add(bmpf);
                }

                var rect = new Rectangle(width - height, 0, height, height);

                springy.Springy.Target = s.Value.Target;
                springy.Springy.ShowG = showG;
                springy.Springy.NodeRadius = 20;
                springy.Springy.AttrationStrength = 0.05;
                if (showDevTime)
                {
                    var drules = exp.PopulationConfig.ExperimentConfiguration.DevelopmentRules;
                    var individual = s.Value.Individual;
                    springy.Update(individual, strengthRescale);
                    springy.Springy.Highlights.Clear();
                    if (s.Value.Highlight >= 0)
                        springy.Springy.Highlights.Add(s.Value.Highlight);
                    springy.Step(withinSpin);
                    individual.Genome.DevelopWithTrajectories(null, drules, ref trajectories);
                    for (int t = 0; t < trajectories[0].Length; t++)
                    {
                        g.FillRectangle(Brushes.White, rect);
                        var y = M4M.Analysis.ExtractVector(trajectories, t) / drules.UpdateRate * drules.DecayRate;
                        springy.Update(individual.Genome.TransMat, y, y, strengthRescale);
                        springy.Draw(g, rect);
                        g.DrawString($"t = {t}", font, Brushes.Black, rect);
                        addFrame();
                    }
                }
                else
                {
                    springy.UpdateStepAndDraw(g, rect, s.Value.Individual, 500, strengthRescale, sp => { });
                    springy.Springy.Highlights.Clear();
                    if (s.Value.Highlight >= 0)
                        springy.Springy.Highlights.Add(s.Value.Highlight);
                    addFrame();
                }
            }

            // produce gif
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            genc.Save(ms);
            byte[] data = ms.GetBuffer();
            byte[] netscape = { 0x21, 0xFF, 0x0B, 0x4E, 0x45, 0x54, 0x53, 0x43, 0x41, 0x50, 0x45, 0x32, 0x2E, 0x30, 0x03, 0x01, 0x00, 0x00, 0x00 };

            int last = -1;

            // promise yourself now you will never use this in production code
            // I've not read enough of the GIF spec to know if this is a bad idea or not
            for (int i = 0; i < ms.Length - 5; i++)
            {
                if (data[i] == 0x21 && data[i + 1] == 0xF9 && data[i + 2] == 0x04 && data[i + 3] == 01)
                {
                    data[i + 4] = (byte)(duration & 255); // something endian (least significant first)
                    data[i + 5] = (byte)((duration >> 8) & 255);
                    last = i + 4;
                }
            }

            if (last != -1)
            {
                data[last] = (byte)(duration & 255);
                data[last + 1] = (byte)((duration >> 8) & 255);
            }

            using (System.IO.FileStream fs = new System.IO.FileStream(outfile, System.IO.FileMode.Create))
            {
                fs.Write(data, 0, 13);

                // behold
                fs.Write(netscape, 0, netscape.Length);

                fs.Write(data, 13, (int)ms.Length - 13); // lets hope these aren't in excess of 2GB
            }

            g.Dispose();
            bmp.Dispose();
        }
    }

    public delegate void GMutation(int gene);

    /// <summary>
    /// (re)implements the default mutator, but notifies when a mutation takes place
    /// </summary>
    public class InitialStateMutationInterceptingMutator : M4M.IInitialStateMutator
    {
        public string Name => "InitialStateMutationInterceptingMutator";

        public event GMutation Mutation;

        public Vector<double> Mutate(Vector<double> current, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            var target = current.Clone();
            MutateInplace(target, initialStateIndexOpenEntries, rand, rules);
            return target;
        }

        public void MutateInplace(Vector<double> current, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            int i;
            if (initialStateIndexOpenEntries != null)
            {
                i = initialStateIndexOpenEntries[rand.Next(initialStateIndexOpenEntries.Count)];
            }
            else
            {
                i = rand.Next(current.Count);
            }
            current[i] = rules.InitialStateClamping.Clamp(current[i] + M4M.Misc.NextNoise(rand, rules.InitialStateMutationSize, rules.InitialStateMutationType));
            Mutation?.Invoke(i);
        }

        public void MutateInto(Vector<double> current, Vector<double> target, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            current.CopyTo(target);
            MutateInplace(target, initialStateIndexOpenEntries, rand, rules);
        }
    }

    public struct HighlightTargetIndividual
    {
        public HighlightTargetIndividual(DenseIndividual individual, int highlight, Vector<double> target)
        {
            Individual = individual ?? throw new ArgumentNullException(nameof(individual));
            Highlight = highlight;
            Target = target;
        }

        public static implicit operator HighlightTargetIndividual(DenseIndividual i) => new HighlightTargetIndividual(i, -1, null);

        public DenseIndividual Individual { get; }
        public int Highlight { get; }
        public Vector<double> Target { get; }
    }

    public static class Extensions
    {
        public static IEnumerable<T> TakeEvery<T>(this IEnumerable<T> sequence, int skip)
        {
            int i = 0;
            foreach (var t in sequence)
            {
                if (i == 0)
                {
                    yield return t;
                    i = skip;
                }

                i--;
            }
        }

    }
}
