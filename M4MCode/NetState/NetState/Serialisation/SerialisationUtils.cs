﻿using System;
using System.IO;

namespace NetState.Serialisation
{
    public enum SizeClass
    {
        /// <summary>
        /// 32bit
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 16bit
        /// </summary>
        Short = 1,
        /// <summary>
        /// 8bit
        /// </summary>
        VeryShort = 2
    }

    /// <summary>
    /// Utility methods to write and read various things to and from streams in BigEndian
    /// No complexity here
    /// </summary>
    public class Utils
    {
        public static readonly byte[] EmptyBarr = new byte[0];

        public static void EndianFlip(byte[] barr)
        {
            if (System.BitConverter.IsLittleEndian)
            {
                Array.Reverse(barr); // this has some horrid fast-path, I understand, which should be faster than anything in readable C#
            }
        }

        //
        // Read
        //

        public static byte ReadByte(Stream stream)
        {
            return (byte)stream.ReadByte();
        }

        public static bool ReadBool(Stream stream)
        {
            return stream.ReadByte() > 0 ? true : false;
        }

        // TODO: check this works, and write the 3 other associates
        // note that you probably never want to use this, unless you expect lots of small (but with the odd long) ints
        public static int ReadCroppedUInt(Stream stream)
        {
            int b1 = stream.ReadByte();
            if ((b1 & 0b10000000) == 0) // first bit indicates 7bit or not
            {
                // 8
                return b1;
            }
            else if ((b1 & 0b01000000) == 0) // second bit indicates 30bit or 14bit
            {
                // 16
                b1 = b1 & 0b00111111;
                int b2 = stream.ReadByte();
                return (int)b1 << 8 + b2;
            }
            else
            {
                // 32
                b1 = b1 & 0b00111111;
                int b2 = stream.ReadByte();
                int b3 = stream.ReadByte();
                int b4 = stream.ReadByte();
                return (int)b1 << 24 + b2 << 16 + b3 << 8 + b4;
            }
        }

        public static int ReadSizeClass(Stream stream, SizeClass sizeClass)
        {
            switch (sizeClass)
            {
                case SizeClass.Normal:
                    return ReadInt32(stream);
                case SizeClass.Short:
                    return ReadInt16(stream);
                case SizeClass.VeryShort:
                    return ReadByte(stream);
                default:
                    throw new Exception("Unsupported SizeClass: " + sizeClass.ToString());
            }
        }

        public static short ReadInt16(Stream stream)
        {
            return System.BitConverter.ToInt16(ReadEndianFlipBytes(stream, 2), 0);
        }

        public static ushort ReadUInt16(Stream stream)
        {
            return System.BitConverter.ToUInt16(ReadEndianFlipBytes(stream, 2), 0);
        }

        public static int ReadInt32(Stream stream)
        {
            return System.BitConverter.ToInt32(ReadEndianFlipBytes(stream, 4), 0);
        }

        public static uint ReadUInt32(Stream stream)
        {
            return System.BitConverter.ToUInt32(ReadEndianFlipBytes(stream, 4), 0);
        }

        public static long ReadInt64(Stream stream)
        {
            return System.BitConverter.ToInt64(ReadEndianFlipBytes(stream, 8), 0);
        }

        public static ulong ReadUInt64(Stream stream)
        {
            return System.BitConverter.ToUInt64(ReadEndianFlipBytes(stream, 8), 0);
        }

        public static float ReadFloat32(Stream stream)
        {
            return System.BitConverter.ToSingle(ReadEndianFlipBytes(stream, 4), 0);
        }

        public static double ReadFloat64(Stream stream)
        {
            return System.BitConverter.ToDouble(ReadEndianFlipBytes(stream, 8), 0);
        }

        public static DateTime ReadDateTime(Stream stream)
        {
            return DateTime.FromBinary(ReadInt64(stream));
        }

        public static string ReadString(Stream stream, int length)
        {
            return System.Text.Encoding.UTF8.GetString(ReadBytes(stream, length));
        }

        public static string ReadPascalString(Stream stream)
        {
            int length = Utils.ReadInt32(stream);
            if (length < 0)
                return null;
            else
                return System.Text.Encoding.UTF8.GetString(ReadBytes(stream, length));
        }

        public static string ReadShortPascalString(Stream stream)
        {
            int length = Utils.ReadInt16(stream);
            if (length < 0)
                return null;
            else
                return System.Text.Encoding.UTF8.GetString(ReadBytes(stream, length));
        }

        public static string ReadVeryShortPascalString(Stream stream)
        {
            int length = Utils.ReadByte(stream);
            if (length == 255)
                return null;
            else
                return System.Text.Encoding.UTF8.GetString(ReadBytes(stream, length));
        }

        public static byte[] ReadBytes(Stream stream, int count)
        {
            byte[] barr = new byte[count];
            stream.Read(barr, 0, count);
            return barr;
        }

        public static byte[] ReadPascalBytes(Stream stream)
        {
            int length = Utils.ReadInt32(stream);
            if (length < 0)
                return null;
            else
                return ReadBytes(stream, length);
        }

        public static byte[] ReadShortPascalBytes(Stream stream)
        {
            int length = Utils.ReadInt16(stream);
            if (length < 0)
                return null;
            else
                return ReadBytes(stream, length);
        }

        public static byte[] ReadVeryShortPascalBytes(Stream stream)
        {
            int length = Utils.ReadByte(stream);
            if (length == 255)
                return null;
            else
                return ReadBytes(stream, length);
        }

        public static byte[] ReadEndianFlipBytes(Stream stream, int count)
        {
            byte[] barr = new byte[count];
            stream.Read(barr, 0, count);
            EndianFlip(barr);
            return barr;
        }

        //
        // Read (with position indicator)
        //

        public static byte ReadByte(Stream stream, ref int p)
        {
            p += 1;
            return (byte)stream.ReadByte();
        }

        public static bool ReadBool(Stream stream, ref int p)
        {
            p += 1;
            return stream.ReadByte() > 0 ? true : false;
        }

        public static int ReadSizeClass(Stream stream, SizeClass sizeClass, ref int p)
        {
            switch (sizeClass)
            {
                case SizeClass.Normal:
                    return ReadInt32(stream, ref p);
                case SizeClass.Short:
                    return ReadInt16(stream, ref p);
                case SizeClass.VeryShort:
                    return ReadByte(stream, ref p);
                default:
                    throw new Exception("Unsupported SizeClass: " + sizeClass.ToString());
            }
        }

        public static short ReadInt16(Stream stream, ref int p)
        {
            p += 2;
            return System.BitConverter.ToInt16(ReadEndianFlipBytes(stream, 2), 0);
        }

        public static uint ReadUInt16(Stream stream, ref int p)
        {
            p += 2;
            return System.BitConverter.ToUInt16(ReadEndianFlipBytes(stream, 2), 0);
        }

        public static int ReadInt32(Stream stream, ref int p)
        {
            p += 4;
            return System.BitConverter.ToInt32(ReadEndianFlipBytes(stream, 4), 0);
        }

        public static uint ReadUInt32(Stream stream, ref int p)
        {
            p += 4;
            return System.BitConverter.ToUInt32(ReadEndianFlipBytes(stream, 4), 0);
        }

        public static long ReadInt64(Stream stream, ref int p)
        {
            p += 8;
            return System.BitConverter.ToInt64(ReadEndianFlipBytes(stream, 8), 0);
        }

        public static ulong ReadUInt64(Stream stream, ref int p)
        {
            p += 8;
            return System.BitConverter.ToUInt64(ReadEndianFlipBytes(stream, 8), 0);
        }

        public static float ReadFloat32(Stream stream, ref int p)
        {
            p += 4;
            return System.BitConverter.ToSingle(ReadEndianFlipBytes(stream, 4), 0);
        }

        public static double ReadFloat64(Stream stream, ref int p)
        {
            p += 8;
            return System.BitConverter.ToDouble(ReadEndianFlipBytes(stream, 8), 0);
        }

        public static DateTime ReadDateTime(Stream stream, ref int p)
        {
            p += 8;
            return DateTime.FromBinary(ReadInt64(stream));
        }

        public static string ReadString(Stream stream, int length, ref int p)
        {
            p += length;
            return System.Text.Encoding.UTF8.GetString(ReadBytes(stream, length));
        }

        public static string ReadPascalString(Stream stream, ref int p)
        {
            int length = Utils.ReadInt32(stream);
            if (length < 0)
            {
                p += 4;
                return null;
            }
            else
            {
                p += 4 + length;
                return System.Text.Encoding.UTF8.GetString(ReadBytes(stream, length));
            }
        }

        public static string ReadShortPascalString(Stream stream, ref int p)
        {
            int length = Utils.ReadInt16(stream);
            if (length < 0)
            {
                p += 2;
                return null;
            }
            else
            {
                p += 2 + length;
                return System.Text.Encoding.UTF8.GetString(ReadBytes(stream, length));
            }
        }

        public static string ReadVeryShortPascalString(Stream stream, ref int p)
        {
            int length = Utils.ReadByte(stream);
            if (length == 255)
            {
                p += 1;
                return null;
            }
            else
            {
                p += 1 + length;
                return System.Text.Encoding.UTF8.GetString(ReadBytes(stream, length));
            }
        }

        public static byte[] ReadBytes(Stream stream, int count, ref int p)
        {
            p += count;
            byte[] barr = new byte[count];
            stream.Read(barr, 0, count);
            return barr;
        }

        public static byte[] ReadPascalBytes(Stream stream, ref int p)
        {
            int length = Utils.ReadInt32(stream);
            if (length < 0)
            {
                p += 4;
                return null;
            }
            else
            {
                p += 4 + length;
                return ReadBytes(stream, length);
            }
        }

        public static byte[] ReadShortPascalBytes(Stream stream, ref int p)
        {
            int length = Utils.ReadInt16(stream);
            if (length < 0)
            {
                p += 2;
                return null;
            }
            else
            {
                p += 2 + length;
                return ReadBytes(stream, length);
            }
        }

        public static byte[] ReadVeryShortPascalBytes(Stream stream, ref int p)
        {
            int length = Utils.ReadByte(stream);
            if (length == 255)
            {
                p += 1;
                return null;
            }
            else
            {
                p += 1 + length;
                return ReadBytes(stream, length);
            }
        }

        public static byte[] ReadEndianFlipBytes(Stream stream, int count, ref int p)
        {
            p += count;
            byte[] barr = new byte[count];
            stream.Read(barr, 0, count);
            EndianFlip(barr);
            return barr;
        }

        //
        // Write
        //

        public static void WriteByte(Stream stream, byte v)
        {
            stream.WriteByte(v);
        }

        public static void WriteBool(Stream stream, bool v)
        {
            stream.WriteByte(v ? (byte)1 : (byte)0);
        }
        
        public static void WriteSizeClass(Stream stream, SizeClass sizeClass, int size)
        {
            switch (sizeClass)
            {
                case SizeClass.Normal:
                    WriteInt32(stream, size);
                    break;
                case SizeClass.Short:
                    WriteInt16(stream, (short)size);
                    break;
                case SizeClass.VeryShort:
                    WriteByte(stream, (byte)size);
                    break;
                default:
                    throw new Exception("Unsupported SizeClass: " + sizeClass.ToString());
            }
        }

        public static void WriteInt16(Stream stream, short v)
        {
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteUInt16(Stream stream, ushort v)
        {
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteInt32(Stream stream, int v)
        {
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteUInt32(Stream stream, uint v)
        {
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteInt64(Stream stream, long v)
        {
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteUInt64(Stream stream, ulong v)
        {
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteFloat32(Stream stream, float v)
        {
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteFloat64(Stream stream, double v)
        {
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }
        
        public static void WriteDateTime(Stream stream, DateTime v)
        {
            WriteInt64(stream, v.ToBinary());
        }
        
        public static void WriteString(Stream stream, string str)
        {
            WriteBytes(stream, System.Text.Encoding.UTF8.GetBytes(str));
        }
        
        /// <summary>
        /// Supports null (represented as -1 length string)
        /// </summary>
        public static void WritePascalString(Stream stream, string str)
        {
            if (str == null)
            {
                WriteInt32(stream, -1);
            }
            else
            {
                byte[] barr = System.Text.Encoding.UTF8.GetBytes(str);
                WriteInt32(stream, barr.Length);
                stream.Write(barr, 0, barr.Length);
            }
        }

        /// <summary>
        /// Supports null (represented as -1 length string)
        /// </summary>
        public static void WriteShortPascalString(Stream stream, string str)
        {
            if (str == null)
            {
                WriteInt16(stream, -1);
            }
            else
            {
                byte[] barr = System.Text.Encoding.UTF8.GetBytes(str);
                WriteInt16(stream, (short)barr.Length);
                stream.Write(barr, 0, barr.Length);
            }
        }

        /// <summary>
        /// Supports null (represented as 255 (-1) length string)
        /// </summary>
        public static void WriteVeryShortPascalString(Stream stream, string str)
        {
            if (str == null)
            {
                WriteByte(stream, 255);
            }
            else
            {
                byte[] barr = System.Text.Encoding.UTF8.GetBytes(str);
                WriteByte(stream, (byte)barr.Length);
                stream.Write(barr, 0, barr.Length);
            }
        }
        
        /// <summary>
        /// Returns the length of the SizeClass in bytes
        /// </summary>
        public static int MeasureSizeClass(SizeClass sizeClass)
        {
            switch (sizeClass)
            {
                case SizeClass.Normal:
                    return 4;
                case SizeClass.Short:
                    return 2;
                case SizeClass.VeryShort:
                    return 1;
                default:
                    throw new Exception("Unsupported SizeClass: " + sizeClass.ToString());
            }
        }

        /// <summary>
        /// Includes the 4 bytes for the int
        /// </summary>
        public static int MeasurePascalString(string str)
        {
            if (str == null)
                return 4;
            else
                return System.Text.Encoding.UTF8.GetByteCount(str) + 4;
        }

        public static void WriteBytes(Stream stream, byte[] barr)
        {
            stream.Write(barr, 0, barr.Length);
        }

        /// <summary>
        /// Supports null (represented as -1 length string)
        /// </summary>
        public static void WritePascalBytes(Stream stream, byte[] barr)
        {
            if (barr == null)
            {
                WriteInt32(stream, -1);
            }
            else
            {
                WriteInt32(stream, barr.Length);
                stream.Write(barr, 0, barr.Length);
            }
        }

        /// <summary>
        /// Supports null (represented as -1 length string)
        /// </summary>
        public static void WriteShortPascalBytes(Stream stream, byte[] barr)
        {
            if (barr == null)
            {
                WriteInt16(stream, -1);
            }
            else
            {
                WriteInt16(stream, (short)barr.Length);
                stream.Write(barr, 0, barr.Length);
            }
        }

        /// <summary>
        /// Supports null (represented as 255 (-1) length string)
        /// </summary>
        public static void WriteVeryShortPascalBytes(Stream stream, byte[] barr)
        {
            if (barr == null)
            {
                WriteByte(stream, 255);
            }
            else
            {
                WriteByte(stream, (byte)barr.Length);
                stream.Write(barr, 0, barr.Length);
            }
        }

        /// <summary>
        /// Includes the 4 bytes for the int
        /// </summary>
        public static int MeasurePascalBytes(byte[] barr)
        {
            if (barr == null)
                return 4;
            else
                return barr.Length + 4;
        }

        /// <summary>
        ///  modifies your byte[]
        /// </summary>
        public static void WriteEndianFlipBytes(Stream stream, byte[] barr)
        {
            EndianFlip(barr);
            stream.Write(barr, 0, barr.Length);
        }

        //
        // Write
        //

        public static void WriteByte(Stream stream, byte v, ref int p)
        {
            p += 1;
            stream.WriteByte(v);
        }

        public static void WriteBool(Stream stream, bool v, ref int p)
        {
            p += 1;
            stream.WriteByte(v ? (byte)1 : (byte)0);
        }
        
        public static void WriteSizeClass(Stream stream, SizeClass sizeClass, int size, ref int p)
        {
            switch (sizeClass)
            {
                case SizeClass.Normal:
                    WriteInt32(stream, size, ref p);
                    break;
                case SizeClass.Short:
                    WriteInt16(stream, (short)size, ref p);
                    break;
                case SizeClass.VeryShort:
                    WriteByte(stream, (byte)size, ref p);
                    break;
                default:
                    throw new Exception("Unsupported SizeClass: " + sizeClass.ToString());
            }
        }

        public static void WriteInt16(Stream stream, short v, ref int p)
        {
            p += 2;
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteUInt16(Stream stream, ushort v, ref int p)
        {
            p += 2;
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteInt32(Stream stream, int v, ref int p)
        {
            p += 4;
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteUInt32(Stream stream, uint v, ref int p)
        {
            p += 4;
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteInt64(Stream stream, long v, ref int p)
        {
            p += 8;
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteUInt64(Stream stream, ulong v, ref int p)
        {
            p += 8;
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteFloat32(Stream stream, float v, ref int p)
        {
            p += 4;
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteFloat64(Stream stream, double v, ref int p)
        {
            p += 8;
            WriteEndianFlipBytes(stream, System.BitConverter.GetBytes(v));
        }

        public static void WriteDateTime(Stream stream, DateTime v, ref int p)
        {
            p += 8;
            WriteInt64(stream, v.ToBinary());
        }

        public static void WriteString(Stream stream, string str, ref int p)
        {
            byte[] barr = System.Text.Encoding.UTF8.GetBytes(str);
            p += barr.Length;
            WriteBytes(stream, barr);
        }

        /// <summary>
        /// Supports null (represented as -1 length string)
        /// </summary>
        public static void WritePascalString(Stream stream, string str, ref int p)
        {
            if (str == null)
            {
                p += 4;
                WriteInt32(stream, -1);
            }
            else
            {
                byte[] barr = System.Text.Encoding.UTF8.GetBytes(str);
                WriteInt32(stream, barr.Length);
                p += barr.Length + 4;
                stream.Write(barr, 0, barr.Length);
            }
        }
        

        /// <summary>
        /// Supports null (represented as -1 length string)
        /// </summary>
        public static void WriteShortPascalString(Stream stream, string str, ref int p)
        {
            if (str == null)
            {
                p += 2;
                WriteInt16(stream, -1);
            }
            else
            {
                byte[] barr = System.Text.Encoding.UTF8.GetBytes(str);
                WriteInt16(stream, (short)barr.Length);
                p += barr.Length + 2;
                stream.Write(barr, 0, barr.Length);
            }
        }

        /// <summary>
        /// Supports null (represented as 255 (-1) length string)
        /// </summary>
        public static void WriteVeryShortPascalString(Stream stream, string str, ref int p)
        {
            if (str == null)
            {
                p += 1;
                WriteByte(stream, 255);
            }
            else
            {
                byte[] barr = System.Text.Encoding.UTF8.GetBytes(str);
                WriteByte(stream, (byte)barr.Length);
                p += barr.Length + 1;
                stream.Write(barr, 0, barr.Length);
            }
        }

        public static void WriteBytes(Stream stream, byte[] barr, ref int p)
        {
            p += barr.Length;
            stream.Write(barr, 0, barr.Length);
        }

        /// <summary>
        /// Supports null (represented as -1 length string)
        /// </summary>
        public static void WritePascalBytes(Stream stream, byte[] barr, ref int p)
        {
            if (barr == null)
            {
                p += 4;
                WriteInt32(stream, -1);
            }
            else
            {
                p += 4 + barr.Length;
                WriteInt32(stream, barr.Length);
                stream.Write(barr, 0, barr.Length);
            }
        }

        /// <summary>
        ///  modifies your byte[]
        /// </summary>
        public static void WriteEndianFlipBytes(Stream stream, byte[] barr, ref int p)
        {
            p += barr.Length;
            EndianFlip(barr);
            stream.Write(barr, 0, barr.Length);
        }
    }
}
