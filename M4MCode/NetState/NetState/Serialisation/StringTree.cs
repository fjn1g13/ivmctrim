﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace NetState.Serialisation
{
    public abstract class StringTreeContextBase<STC> : IStringTreeContext<STC> where STC : StringTreeContextBase<STC>
    {
        public string Name { get; }
        private Dictionary<string, string> Attributes { get; }
        private List<STC> ChildContexts { get; }
        
        public IEnumerable<KeyValuePair<string, string>> EnumerateAttributes() => Attributes;
        public IEnumerable<STC> EnumerateChildContexts() => ChildContexts;
        public IEnumerable<STC> EnumerateChildContexts(string name) => ChildContexts.Where(cc => cc.Name == name);
        IEnumerable<IStringTreeContext> IStringTreeContext.EnumerateChildContexts() => ChildContexts;
        IEnumerable<IStringTreeContext> IStringTreeContext.EnumerateChildContexts(string name) => ChildContexts.Where(cc => cc.Name == name);

        protected StringTreeContextBase(string name)
        {
            Name = name;
        }

        IStringTreeContext IStringTreeContext.GetChildContext(string name) => GetChildContext(name);
        public STC GetChildContext(string name)
        {
            var matching = EnumerateChildContexts(name);
            int count = matching.Count();

            if (count == 0)
                throw new Exception("No child context with name '" + name + "'");
            if (matching.Count() > 1)
                throw new Exception("More than one child context with name '" + name + "'");

            return matching.First();
        }
        
        IStringTreeContext IStringTreeContext.TryGetChildContext(string name) => TryGetChildContext(name);
        public STC TryGetChildContext(string name)
        {
            var matching = EnumerateChildContexts(name);
            int count = matching.Count();
            
            if (count == 0)
                return null;
            if (matching.Count() > 1)
                throw new Exception("More than one child context with name '" + name + "'");
            else
            {
                return matching.First();
            }
        }

        IStringTreeContext IStringTreeContext.AddChildContext(string name) => AddChildContext(name);
        public STC AddChildContext(string name)
        {
            STC cc = PrepareChildContext(name);
            ChildContexts.Add(cc);
            return cc;
        }

        protected abstract STC PrepareChildContext(string name);

        public void SetAttribute(string key, string value)
        {
            Attributes[key] = value;
        }

        public string GetAttribute(string key)
        {
            return Attributes[key];
        }
    }

    //public class StringTreeContext<CX> : IStringTreeContext<StringTreeContext<CX>> where CX : class
    //{
    //    public CX AdditionalContext { get; }

    //    public string Name { get; }
    //    private Dictionary<string, string> Attributes { get; }
    //    private List<StringTreeContext<CX>> ChildContexts { get; }
        
    //    public IEnumerable<KeyValuePair<string, string>> EnumerateAttributes() => Attributes;
    //    public IEnumerable<StringTreeContext<CX>> EnumerateChildContexts() => ChildContexts;
    //    public IEnumerable<StringTreeContext<CX>> EnumerateChildContexts(string name) => ChildContexts.Where(cc => cc.Name == name);
    //    IEnumerable<IStringTreeContext> IStringTreeContext.EnumerateChildContexts() => ChildContexts;
    //    IEnumerable<IStringTreeContext> IStringTreeContext.EnumerateChildContexts(string name) => ChildContexts.Where(cc => cc.Name == name);

    //    public StringTreeContext(string name, CX additionalContext)
    //    {
    //        Name = name;
    //        AdditionalContext = additionalContext;
    //    }

    //    IStringTreeContext IStringTreeContext.GetChildContext(string name) => GetChildContext(name);
    //    public StringTreeContext<CX> GetChildContext(string name)
    //    {
    //        var matching = EnumerateChildContexts(name);
    //        int count = matching.Count();

    //        if (count == 0)
    //            throw new Exception("No child context with name '" + name + "'");
    //        if (matching.Count() > 1)
    //            throw new Exception("More than one child context with name '" + name + "'");

    //        return matching.First();
    //    }
        
    //    IStringTreeContext IStringTreeContext.TryGetChildContext(string name) => TryGetChildContext(name);
    //    public StringTreeContext<CX> TryGetChildContext(string name)
    //    {
    //        var matching = EnumerateChildContexts(name);
    //        int count = matching.Count();
            
    //        if (count == 0)
    //            return null;
    //        if (matching.Count() > 1)
    //            throw new Exception("More than one child context with name '" + name + "'");
    //        else
    //        {
    //            return matching.First();
    //        }
    //    }

    //    IStringTreeContext IStringTreeContext.AddChildContext(string name) => AddChildContext(name);
    //    public StringTreeContext<CX> AddChildContext(string name)
    //    {
    //        var context = new StringTreeContext<CX>(name, AdditionalContext);
    //        return context;
    //    }

    //    public void SetAttribute(string key, string value)
    //    {
    //        Attributes[key] = value;
    //    }

    //    public string GetAttribute(string key)
    //    {
    //        return Attributes[key];
    //    }
    //}

    public interface IStringTreeContextSerialiser
    {
        void Serialise(Stream stream, IStringTreeContext context);
        void Deserialise(Stream stream, IStringTreeContext context);
    }

    public class XmlStringTreeContextSerialiser : IStringTreeContextSerialiser
    {
        private struct FlatElem
        {
            public XmlNode Parent { get; }
            public IStringTreeContext Context;

            public FlatElem(XmlNode parent, IStringTreeContext context) : this()
            {
                Parent = parent;
                Context = context;
            }
        }

        public void Deserialise(Stream stream, IStringTreeContext context)
        {
            throw new NotImplementedException("Implement XmlStringTree Deserialiser");
        }

        /// <summary>
        /// Serialises the IStringTreeContext to the given stream as barely legal Xml
        /// </summary>
        public void Serialise(Stream stream, IStringTreeContext context)
        {
            XmlDocument doc = new XmlDocument();
            var root = doc.CreateElement("XmlRoot");

            Queue<FlatElem> due = new Queue<FlatElem>();
            due.Enqueue(new FlatElem(root, context));

            while (due.Count > 0)
                SerialiseNode(doc, root, context, due);
            
            doc.WriteTo(XmlWriter.Create(stream));
        }

        private void SerialiseNode(XmlDocument doc, XmlElement parent, IStringTreeContext context, Queue<FlatElem> dueQueue)
        {
            // prepare us
            var node = doc.CreateElement(context.Name);

            // fill in our attributes
            foreach (var attribute in context.EnumerateAttributes())
            {
                node.SetAttribute(attribute.Key, attribute.Value);
            }

            // queue children
            foreach (var child in context.EnumerateChildContexts())
            {
                dueQueue.Enqueue(new FlatElem(node, child));
            }

            // append us
            parent.AppendChild(node);
        }
    }
}
