﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NetState.Serialisation
{
    public interface IStream
    {
        byte ReadByte();
        void WriteByte(byte b);
        void Read(byte[] buffer, int offset, int count);
        void Write(byte[] buffer, int offset, int count);
    }

    //public struct MemoryMappedViewStream : IStream
    //{
    //    public MemoryMappedViewStream(System.IO.MemoryMappedFiles.MemoryMappedViewAccessor memoryMappedViewAccessor)
    //    {
    //        MemoryMappedViewAccessor = memoryMappedViewAccessor;
    //    }

    //    public System.IO.MemoryMappedFiles.MemoryMappedViewAccessor MemoryMappedViewAccessor { get; }

    //    public void Read(byte[] buffer, int offset, int count)
    //    {
    //        MemoryMappedViewAccessor.read
    //        Stream.Read(buffer, offset, count);
    //    }

    //    public byte ReadByte()
    //    {
    //        return (byte)Stream.ReadByte();
    //    }

    //    public void Write(byte[] buffer, int offset, int count)
    //    {
    //        Stream.Write(buffer, offset, count);
    //    }

    //    public void WriteByte(byte b)
    //    {
    //        Stream.WriteByte(b);
    //    }
    //}

    public struct StreamStream : IStream
    {
        public StreamStream(Stream stream)
        {
            Stream = stream;
        }

        public Stream Stream { get; }

        public void Read(byte[] buffer, int offset, int count)
        {
            Stream.Read(buffer, offset, count);
        }

        public byte ReadByte()
        {
            return (byte)Stream.ReadByte();
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            Stream.Write(buffer, offset, count);
        }

        public void WriteByte(byte b)
        {
            Stream.WriteByte(b);
        }
    }

    /// <summary>
    /// Convenience class providing a concrete StreamReaderWriter for the Stream class
    /// Hopefully it won't bleed much performance
    /// </summary>
    public sealed class StreamReaderWriter : StreamReaderWriter<StreamStream>
    {
        public new Stream Stream
        {
            get
            {
                return base.Stream.Stream;
            }
            set
            {
                base.Stream = new StreamStream(value);
            }
        }
        
        public StreamReaderWriter(Stream stream, bool littleEndian = false, Encoding encoding = null) : base(new StreamStream(stream), littleEndian, encoding)
        {
            // nix
        }
    }

    /// <summary>
    /// Convenience class providing a an abstract StreamReaderWriter
    /// </summary>
    public sealed class GenericStreamReaderWriter : StreamReaderWriter<IStream>
    {
        public GenericStreamReaderWriter(IStream stream, bool littleEndian = false, Encoding encoding = null) : base(stream, littleEndian, encoding)
        {
            // nix
        }
    }

    /// <summary>
    /// Provides Read/Write methods over a generic TStream
    /// Note: many of these methods are untested
    /// </summary>
    public class StreamReaderWriter<TStream> where TStream : IStream
    {
        private const int MinBuffer = 8; // 64bits is pretty much the smallest sensible thing
        
        private byte[] Buffer = new byte[MinBuffer];

        public TStream Stream { get; set; }
        public bool IsLittleEndian { get; set; }
        public Encoding Encoding { get; set; }

        /// <summary>
        /// Initialises a StreamReaderWriter
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="littleEndian"></param>
        /// <param name="encoding">The default encoding is UTF-8</param>
        public StreamReaderWriter(TStream stream, bool littleEndian = false, Encoding encoding = null)
        {
            Stream = stream;
            IsLittleEndian = littleEndian;
            Encoding = encoding ?? UTF8Encoding.UTF8;
        }
        
        // checks for MinBuffer, which may enable this to be optimised away in some instances
        private void AllocateConst(int capacity)
        {
            if (capacity > MinBuffer && Buffer.Length < capacity)
            {
                var newBuffer = new byte[Math.Max(Buffer.Length * 2, capacity)];
                Buffer = newBuffer;
            }
        }
        
        // don't check MinBuffer where we have no idea
        private void Allocate(int capacity)
        {
            if (capacity > MinBuffer && Buffer.Length < capacity)
            {
                var newBuffer = new byte[Math.Max(Buffer.Length * 2, capacity)];
                Buffer = newBuffer;
            }
        }
        
        // don't check MinBuffer where we have no idea
        private void Allocate(int capacity, int preserveCount)
        {
            if (capacity > MinBuffer && Buffer.Length < capacity)
            {
                var newBuffer = new byte[Math.Max(Buffer.Length * 2, capacity)];
                if (preserveCount > 0)
                   System.Buffer.BlockCopy(Buffer, 0, newBuffer, 0, preserveCount);
                Buffer = newBuffer;
            }
        }

        // reading

        private void Read(int count)
        {
            Allocate(count, 0);
            Stream.Read(Buffer, 0, count);
        }

        /// <summary>
        /// Returns the internal buffer: do NOT mess with it
        /// </summary>
        public byte[] ViewBufferedBytes(int count)
        {
            Read(count);
            return Buffer;
        }

        /// <summary>
        /// Reads the given number of bytes from the stream
        /// Count must be >= 0
        /// </summary>
        public byte[] ReadBytes(int count)
        {
            byte[] barr = new byte[count];
            Stream.Read(barr, 0, count);
            return barr;
        }

        /// <summary>
        /// Reads bytes rom the stream
        /// </summary>
        public void ReadBytes(byte[] barr, int offset, int count)
        {
            Stream.Read(barr, offset, count);
        }

        /// <summary>
        /// Reads the given number of bytes from the stream, using the given buffer if possible
        /// Count must be >= 0
        /// </summary>
        public byte[] ReadBytes(int count, byte[] availableBuffer)
        {
            byte[] barr = availableBuffer.Length >= count ? availableBuffer : new byte[count];
            Stream.Read(barr, 0, count);
            return barr;
        }

        /// <summary>
        /// Returns the internal buffer (if not null): do NOT mess with it
        /// </summary>
        public byte[] ViewBufferedLongPascalBytes()
        {
            int count = ReadInt32();

            if (count < 0)
            {
                return null;
            }
            else
            {
                Read(count);
                return Buffer;
            }
        }
        
        /*
        /// <summary>
        /// This method does not work
        /// </summary>
        public string ReadNullTerminatedString()
        {
            return ReadNullTerminatedString(out int _);
        }
        
        /// <summary>
        /// This method does not work
        /// </summary>
        public string ReadNullTerminatedString(out int bytesRead)
        {
            var decoder = Encoding.GetDecoder();
            
            int partialSize = 1024;
            byte[] byteBuffer = new byte[partialSize];
            char[] charBuffer = new char[partialSize];

            StringBuilder sb = new StringBuilder();
            bytesRead = 0;
            
            int carryCount = 0;
            while (true)
            {
                int offset = carryCount;
                int pos = offset;
                carryCount = 0;
                bool provisionalEnd = false;

                while (pos < partialSize)
                {
                    byte b = ReadByte();
                    byteBuffer[pos++] = b;

                    if (b == 0)
                    {
                        provisionalEnd = true;
                        carryCount = 1;
                        break;
                    }
                }
                bytesRead += pos - offset;
                
                // don't convert the carry stuff
                decoder.Convert(byteBuffer, offset, pos - carryCount, charBuffer, 0, partialSize, false, out int bytesUsed, out int charsUsed, out bool completed);

                // stuff what we have do far onto the string
                sb.Append(charBuffer, 0, charsUsed);

                if (provisionalEnd && completed)
                {
                    // saw a null, and the decoder is happy: finish
                    return sb.ToString();
                }
                else if (carryCount > 0)
                {
                    // stuff on the end to carry over
                    for (int i = 0; i < carryCount; i++)
                        byteBuffer[i] = byteBuffer[pos - carryCount + i];
                }
            }
        }
        */
        
        public byte[] ReadLongPascalBytes()
        {
            int count = ReadInt32();

            if (count < 0)
            {
                return null;
            }
            else
            {
                byte[] barr = new byte[count];
                Stream.Read(barr, 0, count);
                return barr;
            }
        }
        
        public byte[] ReadLongPascalBytes(byte[] availableBuffer)
        {
            int count = ReadInt32();

            if (count < 0)
            {
                return null;
            }
            else
            {
                byte[] barr = availableBuffer.Length >= count ? availableBuffer : new byte[count];
                Stream.Read(barr, 0, count);
                return barr;
            }
        }

        public string ReadString(int length)
        {
            int count = length;

            if (count < 0)
            {
                return null;
            }
            else
            {
                Read(count);
                return Encoding.GetString(Buffer, 0, count);
            }
        }

        /// <summary>
        /// Length is int32
        /// </summary>
        public string ReadLongPascalString()
        {
            int count = ReadInt32();

            if (count < 0)
            {
                return null;
            }
            else
            {
                Read(count);
                return Encoding.GetString(Buffer, 0, count);
            }
        }
        
        /// <summary>
        /// Length is int32, supports null
        /// </summary>
        public string ReadLongPascalString(out int bytesRead)
        {
            int count = ReadInt32();

            if (count < 0)
            {
                bytesRead = 4;
                return null;
            }
            else
            {
                Read(count);
                bytesRead = 4 + count;
                return Encoding.GetString(Buffer, 0, count);
            }
        }
        
        /// <summary>
        /// Length is uint16
        /// </summary>
        public string ReadUShortPascalString()
        {
            int count = ReadUInt16();
            
            Read(count);
            return Encoding.GetString(Buffer, 0, count);
        }
        
        /// <summary>
        /// Length is uint16
        /// </summary>
        public string ReadUShortPascalString(out int bytesRead)
        {
            int count = ReadUInt16();
            
            Read(count);
            bytesRead = 2 + count;
            return Encoding.GetString(Buffer, 0, count);
        }
        
        /// <summary>
        /// Length is int16, supports null
        /// </summary>
        public string ReadShortPascalString()
        {
            int count = ReadInt16();

            if (count < 0)
            {
                return null;
            }
            else
            {
                Read(count);
                return Encoding.GetString(Buffer, 0, count);
            }
        }
        
        /// <summary>
        /// Length is int16, supports null
        /// </summary>
        public string ReadShortPascalString(out int bytesRead)
        {
            int count = ReadInt16();

            if (count < 0)
            {
                bytesRead = 2;
                return null;
            }
            else
            {
                Read(count);
                bytesRead = 2 + count;
                return Encoding.GetString(Buffer, 0, count);
            }
        }
        
        /// <summary>
        /// Length is byte
        /// </summary>
        public string ReadUVeryShortPascalString()
        {
            int count = ReadByte();
            
            Read(count);
            return Encoding.GetString(Buffer, 0, count);
        }
        
        /// <summary>
        /// Length is byte
        /// </summary>
        public string ReadUVeryShortPascalString(out int bytesRead)
        {
            int count = ReadByte();
            
            Read(count);
            bytesRead = 1 + count;
            return Encoding.GetString(Buffer, 0, count);
        }
        
        /// <summary>
        /// Length is sbyte, supports null
        /// </summary>
        public string ReadVeryShortPascalString()
        {
            sbyte count = ReadSByte();

            if (count < 0)
            {
                return null;
            }
            else
            {
                Read(count);
                return Encoding.GetString(Buffer, 0, count);
            }
        }
        
        /// <summary>
        /// Length is sbyte, supports null
        /// </summary>
        public string ReadVeryShortPascalString(out int bytesRead)
        {
            sbyte count = ReadSByte();

            if (count < 0)
            {
                bytesRead = 1;
                return null;
            }
            else
            {
                Read(count);
                bytesRead = 1 + count;
                return Encoding.GetString(Buffer, 0, count);
            }
        }
        
        public byte ReadByte()
        {
            return (byte)Stream.ReadByte();
        }
        
        public sbyte ReadSByte()
        {
            unchecked
            {
                return (sbyte)ReadByte();
            }
        }
        
        public bool ReadBool()
        {
            return ReadByte() > 0;
        }
        
        public ushort ReadUInt16()
        {
            unchecked
            {
                return (ushort)ReadInt16Internal();
            }
        }
        
        public short ReadInt16()
        {
            unchecked
            {
                return (short)ReadInt16Internal();
            }
        }

        private int ReadInt16Internal()
        {
            Read(2);

            if (IsLittleEndian)
            {
                return
                    Buffer[0] << 0 |
                    Buffer[1] << 8;
            }
            else
            {
                return
                    Buffer[0] << 8 |
                    Buffer[1] << 0;
            }
        }
        
        public uint ReadUInt32()
        {
            unchecked
            {
                return (uint)ReadInt32();
            }
        }

        public int ReadInt32()
        {
            Read(4);

            if (IsLittleEndian)
            {
                return
                    Buffer[0] << 0 |
                    Buffer[1] << 8 |
                    Buffer[2] << 16 |
                    Buffer[3] << 24;
            }
            else
            {
                return
                    Buffer[0] << 24|
                    Buffer[1] << 16 |
                    Buffer[2] << 8 |
                    Buffer[3] << 0;
            }
        }
        
        public long ReadInt64()
        {
            unchecked
            {
                return (long)ReadUInt64();
            }
        }

        public ulong ReadUInt64()
        {
            Read(8);

            unchecked
            {
                if (IsLittleEndian)
                {
                    uint low = (uint)(
                        Buffer[0] << 0 |
                        Buffer[1] << 8 |
                        Buffer[2] << 16 |
                        Buffer[3] << 24);
                    uint high = (uint)(
                        Buffer[4] << 0 |
                        Buffer[5] << 8 |
                        Buffer[6] << 16 |
                        Buffer[7] << 24);
                    return (ulong)(low + ((ulong)high << 32));
                }
                else
                {
                    uint high = (uint)(
                        Buffer[0] << 24 |
                        Buffer[1] << 16 |
                        Buffer[2] << 8 |
                        Buffer[3] << 0);
                    uint low = (uint)(
                        Buffer[4] << 24 |
                        Buffer[5] << 16 |
                        Buffer[6] << 8 |
                        Buffer[7] << 0);
                    return (low + ((ulong)high << 32));
                }
            }
        }

        public float ReadFloat32()
        {
            Read(4);

            if (IsLittleEndian != BitConverter.IsLittleEndian)
            {
                Array.Reverse(Buffer, 0, 4);
            }

            return BitConverter.ToSingle(Buffer, 0);
        }

        public double ReadFloat64()
        {
            Read(8);
            
            if (IsLittleEndian != BitConverter.IsLittleEndian)
            {
                Array.Reverse(Buffer, 0, 8);
            }

            return BitConverter.ToDouble(Buffer, 0);
        }

        // writing
        
        private void Write(int count)
        {
            Stream.Write(Buffer, 0, count);
        }
        
        private void WriteBytes(byte[] barr, int offset, int count)
        {
            Stream.Write(barr, offset, count);
        }

        /*
        /// <summary>
        /// Does not support null
        /// </summary>
        public void WriteNullTerminatedString(string str)
        {
            if (str == null)
            {
                throw new ArgumentNullException(nameof(str), "Cannot serialise a null string as a null-terminated string");
            }
            else
            {
                int count = Encoding.GetByteCount(str);

                Allocate(count);
                Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
                Write(count);
                WriteByte(0);
            }
        }

        /// <summary>
        /// Does not support null
        /// </summary>
        public void WriteNullTerminatedString(string str, out int bytesWritten)
        {
            if (str == null)
            {
                throw new ArgumentNullException(nameof(str), "Cannot serialise a null string as a null-terminated string");
            }
            else
            {
                int count = Encoding.GetByteCount(str);

                Allocate(count);
                Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
                Write(count);
                WriteByte(0);
                bytesWritten = count + 1;
            }
        }
        */

        public void WriteLongPascalBytes(byte[] bytes, int offset, int count)
        {
            if (bytes == null)
            {
                WriteInt32(-1);
            }
            else
            {
                WriteInt32(count);
                Stream.Write(bytes, offset, count);
            }
        }

        public void WriteString(string str, int declaredLength)
        {
            if (str == null)
                return;

            Allocate(declaredLength);

            Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
            Write(declaredLength);
        }

        public void WriteLongPascalString(string str)
        {
            if (str == null)
            {
                WriteInt32(-1);
            }
            else
            {
                int count = Encoding.GetByteCount(str);

                Allocate(count);
                WriteInt32(count);
                
                Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
                Write(count);
            }
        }

        public void WriteLongPascalString(string str, out int bytesWritten)
        {
            if (str == null)
            {
                bytesWritten = 4;
                WriteInt32(-1);
            }
            else
            {
                int count = Encoding.GetByteCount(str);

                Allocate(count);
                WriteInt32(count);
                
                Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
                bytesWritten = 4 + count;
                Write(count);
            }
        }

        public void WriteUShortPascalString(string str)
        {
            ushort count = (ushort)Encoding.GetByteCount(str);

            Allocate(count);
            WriteUInt16(count);
                
            Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
            Write(count);
        }

        public void WriteUShortPascalString(string str, out int bytesWritten)
        {
            ushort count = (ushort)Encoding.GetByteCount(str);

            Allocate(count);
            WriteUInt16(count);
                
            Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
            bytesWritten = 2 + count;
            Write(count);
        }

        public void WriteShortPascalString(string str)
        {
            if (str == null)
            {
                WriteInt16(-1);
            }
            else
            {
                short count = (short)Encoding.GetByteCount(str);

                Allocate(count);
                WriteInt16(count);
                
                Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
                Write(count);
            }
        }

        public void WriteShortPascalString(string str, out int bytesWritten)
        {
            if (str == null)
            {
                WriteInt16(-1);
                bytesWritten = 2;
            }
            else
            {
                short count = (short)Encoding.GetByteCount(str);

                Allocate(count);
                WriteInt16(count);
                
                Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
                bytesWritten = 2 + count;
                Write(count);
            }
        }

        public void WriteUVeryShortPascalString(string str)
        {
            byte count = (byte)Encoding.GetByteCount(str);

            Allocate(count);
            WriteByte(count);
                
            Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
            Write(count);
        }

        public void WriteUVeryShortPascalString(string str, out int bytesWritten)
        {
            byte count = (byte)Encoding.GetByteCount(str);

            Allocate(count);
            WriteByte(count);
                
            Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
            bytesWritten = 1 + count;
            Write(count);
        }

        public void WriteVeryShortPascalString(string str)
        {
            if (str == null)
            {
                WriteSByte(-1);
            }
            else
            {
                sbyte count = (sbyte)Encoding.GetByteCount(str);

                Allocate(count);
                WriteSByte(count);
                
                Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
                Write(count);
            }
        }

        public void WriteVeryShortPascalString(string str, out int bytesWritten)
        {
            if (str == null)
            {
                WriteSByte(-1);
                bytesWritten = 1;
            }
            else
            {
                sbyte count = (sbyte)Encoding.GetByteCount(str);

                Allocate(count);
                WriteSByte(count);
                
                Encoding.GetBytes(str, 0, str.Length, Buffer, 0);
                bytesWritten = 1 + count;
                Write(count);
            }
        }
        
        public void WriteSByte(sbyte v)
        {
            unchecked
            {
                WriteByte((byte)v);
            }
        }

        public void WriteByte(byte v)
        {
            Stream.WriteByte(v);
        }

        public void WriteBool(bool v)
        {
            Stream.WriteByte(v ? (byte)1 : (byte)0);
        }

        public void WriteInt16(short v)
        {
            unchecked
            {
                WriteUInt16((ushort)v);
            }
        }

        public void WriteUInt16(ushort v)
        {
            AllocateConst(2);

            unchecked
            {
                if (IsLittleEndian)
                {
                    Buffer[0] = (byte)(v >> 0);
                    Buffer[1] = (byte)(v >> 8);
                }
                else
                {
                    Buffer[0] = (byte)(v >> 8);
                    Buffer[1] = (byte)(v >> 0);
                }
            }

            Write(2);
        }
        
        public void WriteInt32(int v)
        {
            unchecked
            {
                WriteUInt32((uint)v);
            }
        }

        public void WriteUInt32(uint v)
        {
            AllocateConst(4);

            unchecked
            {
                if (IsLittleEndian)
                {
                    Buffer[0] = (byte)(v >> 0);
                    Buffer[1] = (byte)(v >> 8);
                    Buffer[2] = (byte)(v >> 16);
                    Buffer[3] = (byte)(v >> 24);
                }
                else
                {
                    Buffer[0] = (byte)(v >> 24);
                    Buffer[1] = (byte)(v >> 16);
                    Buffer[2] = (byte)(v >> 8);
                    Buffer[3] = (byte)(v >> 0);
                }
            }

            Write(4);
        }
        
        public void WriteInt64(long v)
        {
            unchecked
            {
                WriteUInt64((ulong)v);
            }
        }

        public void WriteUInt64(ulong v)
        {
            AllocateConst(8);

            unchecked
            {
                if (IsLittleEndian)
                {
                    Buffer[0] = (byte)(v >> 0);
                    Buffer[1] = (byte)(v >> 8);
                    Buffer[2] = (byte)(v >> 16);
                    Buffer[3] = (byte)(v >> 24);
                    Buffer[4] = (byte)(v >> 32);
                    Buffer[5] = (byte)(v >> 40);
                    Buffer[6] = (byte)(v >> 48);
                    Buffer[7] = (byte)(v >> 56);
                }
                else
                {
                    Buffer[0] = (byte)(v >> 56);
                    Buffer[1] = (byte)(v >> 48);
                    Buffer[2] = (byte)(v >> 40);
                    Buffer[3] = (byte)(v >> 32);
                    Buffer[4] = (byte)(v >> 24);
                    Buffer[5] = (byte)(v >> 16);
                    Buffer[6] = (byte)(v >> 8);
                    Buffer[7] = (byte)(v >> 0);
                }
            }

            Write(8);
        }

        public void WriteFloat32(float v)
        {
            // TODO: allocation: bad
            var bytes = BitConverter.GetBytes(v);

            if (IsLittleEndian == BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            WriteBytes(bytes, 0, 4);
        }

        public void WriteFloat64(double v)
        {
            // TODO: allocation: bad
            var bytes = BitConverter.GetBytes(v);

            if (IsLittleEndian == BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            WriteBytes(bytes, 0, 8);
        }
    }
}
