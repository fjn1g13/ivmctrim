﻿using NetState.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetState.Serialisation
{
    // TODO: these should not be here
    public interface ISimpleContext<TBase>
    {
        IStateLookup<TBase> StateLookup { get; }
    }

    public interface ISimpleStreamContext<TBase> : ISimpleContext<TBase>, IStreamContext
    {
    }

    public class SimpleContext<TBase> : ISimpleContext<TBase>
    {
        public SimpleContext(IStateLookup<TBase> stateLookup)
        {
            StateLookup = stateLookup;
        }

        public IStateLookup<TBase> StateLookup { get; }
    }

    public interface IEntity
    {
        long Id { get; }
    }

    public abstract class AEntity : IEntity
    {
        public long Id { get; }

        protected AEntity(long id)
        {
            Id = id;
        }
    }

    public class SimpleSteamContext<TBase> : SimpleContext<TBase>, ISimpleStreamContext<TBase>
    {
        public SimpleSteamContext(StreamReaderWriter streamRw, IStateLookup<TBase> stateLookup) : base(stateLookup)
        {
            StreamRw = streamRw;
        }

        public StreamReaderWriter StreamRw { get; }
    }
    
    // (relatively) low-overhead and (hopefully) high(ish) performance context
    public interface IStreamContext
    {
        StreamReaderWriter StreamRw { get; }
    }

    // more of an interchange context
    public interface IStringTreeContext
    {
        string Name { get; }
        IStringTreeContext AddChildContext(string name);
        void SetAttribute(string key, string value);
        string GetAttribute(string key);
        IEnumerable<KeyValuePair<string, string>> EnumerateAttributes();
        IEnumerable<IStringTreeContext> EnumerateChildContexts();
        IEnumerable<IStringTreeContext> EnumerateChildContexts(string name);
        /// <summary>
        /// Throws if no such context exists
        /// </summary>
        IStringTreeContext GetChildContext(string name);
        /// <summary>
        /// Returns null if no such context exists
        /// </summary>
        IStringTreeContext TryGetChildContext(string name);
    }
    
    // more of an interchange context
    public interface IStringTreeContext<out STC> : IStringTreeContext where STC : IStringTreeContext<STC>
    {
        new STC AddChildContext(string name);
        new IEnumerable<STC> EnumerateChildContexts();
        new IEnumerable<STC> EnumerateChildContexts(string name);
        /// <summary>
        /// Throws if no such context exists
        /// </summary>
        new STC GetChildContext(string name);
        /// <summary>
        /// Returns null if no such context exists
        /// </summary>
        new STC TryGetChildContext(string name);
    }

    public interface IStreamContext<CX> : IStreamContext
    {
        CX Context { get; }
    }

    // basic providers

    public class StreamContext<CX> : IStreamContext<CX>
    {
        public StreamContext(StreamReaderWriter streamRw, CX context)
        {
            StreamRw = streamRw;
            Context = context;
        }

        public StreamReaderWriter StreamRw { get; }
        public CX Context { get; }
    }

    public interface IStateProviderPreparer<in TWriteContext, in TReadContext>
    {
        IStateProvider<T, TWriteContext, TReadContext> Prepare<T>();
    }

    public interface IStateProvider<T, in TWriteContext, in TReadContext> : IStateWriter<T, TWriteContext>, IStateReader<T, TReadContext>
    {
        // nix (combination interface)
    }

    public interface IClassStateProviderFactory<T, in TWriteContext, in TReadContext, in TProviderWriteContext, in TProviderReadContext> where T : class
    {
        IClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> CreateNew();
    }

    public class InstanceClassStateProviderFactory<T> : IClassStateProviderFactory<T, object, object, object, object> where T : class
    {
        public InstanceClassStateProviderFactory(IClassStateProvider<T, object, object, object, object> instance)
        {
            Instance = instance;
        }

        private IClassStateProvider<T, object, object, object, object> Instance { get; }

        public IClassStateProvider<T, object, object, object, object> CreateNew()
        {
            return Instance;
        }
    }

    // TODO: should we differentiate between State and Create Contexts? (e.g. Create should not have access to Graphing methods)
    public interface IClassStateProvider<T, in TWriteContext, in TReadContext, in TProviderWriteContext, in TProviderReadContext> : IStateProvider<T, TWriteContext, TReadContext> where T : class
    {
        /// <summary>
        /// Writes the provider configuration
        /// </summary>
        void WriteProviderConfig(TProviderWriteContext context);

        /// <summary>
        /// Reads the provider configuration
        /// </summary>
        void ReadProviderConfig(TProviderReadContext context);

        /// <summary>
        /// Write the mutable state
        /// </summary>
        void WriteState(TWriteContext context, T state);

        /// <summary>
        /// Read the mutable state
        /// </summary>
        void ReadState(TReadContext context, T state);

        /// <summary>
        /// Construct, and read anything necessary to construct
        /// </summary>
        T ReadCreate(TReadContext context);

        /// <summary>
        /// Write anything necessary to construct
        /// </summary>
        /// <param name="context"></param>
        /// <param name="state"></param>
        void WriteCreate(TWriteContext context, T state);
    }
    
    public class StringStateProvider
        : IStateProvider<string, IStreamContext, IStreamContext>, IClassStateProvider<string, IStreamContext, IStreamContext, object, object>, IClassStateProviderFactory<string, IStreamContext, IStreamContext, object, object>
        , IStateProvider<string, IStringTreeContext, IStringTreeContext>, IClassStateProvider<string, IStringTreeContext, IStringTreeContext, object, object>, IClassStateProviderFactory<string, IStringTreeContext, IStringTreeContext, object, object>
    {
        public static readonly StringStateProvider Instance = new StringStateProvider();
        
        private StringStateProvider()
        {
            // nix
        }
        
#region IStreamContext

        IClassStateProvider<string, IStreamContext, IStreamContext, object, object> IClassStateProviderFactory<string, IStreamContext, IStreamContext, object, object>.CreateNew()
        {
            return this;
        }
        
        public void WriteProviderConfig(object context)
        {
            // nix
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public string ReadCreate(IStreamContext context)
        {
            return Read(context);
        }

        public void ReadState(IStreamContext context, string state)
        {
            // nix
        }

        public void WriteCreate(IStreamContext context, string state)
        {
            Write(context, state);
        }

        public void WriteState(IStreamContext context, string state)
        {
            // nix
        }

        public string Read(IStreamContext context)
        {
            return context.StreamRw.ReadLongPascalString();
        }

        public void Write(IStreamContext context, string state)
        {
            context.StreamRw.WriteLongPascalString(state);
        }
        
#endregion IStreamContext
#region IStringTreeContext
        
        IClassStateProvider<string, IStringTreeContext, IStringTreeContext, object, object> IClassStateProviderFactory<string, IStringTreeContext, IStringTreeContext, object, object>.CreateNew()
        {
            return this;
        }

        public string ReadCreate(IStringTreeContext context)
        {
            return Read(context);
        }

        public void ReadState(IStringTreeContext context, string state)
        {
            // nix
        }

        public void WriteCreate(IStringTreeContext context, string state)
        {
            Write(context, state);
        }

        public void WriteState(IStringTreeContext context, string state)
        {
            // nix
        }

        public string Read(IStringTreeContext context)
        {
            return context.GetChildContext("String").GetAttribute("value");
        }

        public void Write(IStringTreeContext context, string state)
        {
            var vc = context.AddChildContext("String");
            vc.SetAttribute("value", state);
        }
#endregion IStringTreeContext
    }
    
    public class IntStateProvider
        : IStateProvider<int, IStreamContext, IStreamContext>
        , IStateProvider<int, IStringTreeContext, IStringTreeContext>
    {
        public static readonly IntStateProvider Instance = new IntStateProvider();

        private IntStateProvider()
        {
            // nix
        }

        public int Read(IStreamContext context)
        {
            return context.StreamRw.ReadInt32();
        }

        public void Write(IStreamContext context, int state)
        {
            context.StreamRw.WriteInt32(state);
        }
        
        public int Read(IStringTreeContext context)
        {
            return int.Parse(context.GetChildContext("Int").GetAttribute("value"));
        }

        public void Write(IStringTreeContext context, int state)
        {
            var vc = context.AddChildContext("Int");
            vc.SetAttribute("value", state.ToString());
        }
    }

    public class LongStateProvider
        : IStateProvider<long, IStreamContext, IStreamContext>
        , IStateProvider<long, IStringTreeContext, IStringTreeContext>
    {
        public static readonly LongStateProvider Instance = new LongStateProvider();

        private LongStateProvider()
        {
            // nix
        }

        public long Read(IStreamContext context)
        {
            return context.StreamRw.ReadInt64();
        }

        public void Write(IStreamContext context, long state)
        {
            context.StreamRw.WriteInt64(state);
        }
        
        public long Read(IStringTreeContext context)
        {
            return long.Parse(context.GetChildContext("Long").GetAttribute("value"));
        }

        public void Write(IStringTreeContext context, long state)
        {
            var vc = context.AddChildContext("Long");
            vc.SetAttribute("value", state.ToString());
        }
    }

    public class ByteStateProvider
        : IStateProvider<byte, IStreamContext, IStreamContext>
        , IStateProvider<byte, IStringTreeContext, IStringTreeContext>
    {
        public static readonly ByteStateProvider Instance = new ByteStateProvider();

        private ByteStateProvider()
        {
            // nix
        }

        public byte Read(IStreamContext context)
        {
            return context.StreamRw.ReadByte();
        }

        public void Write(IStreamContext context, byte state)
        {
            context.StreamRw.WriteByte(state);
        }
        
        public byte Read(IStringTreeContext context)
        {
            return byte.Parse(context.GetChildContext("Byte").GetAttribute("value"));
        }

        public void Write(IStringTreeContext context, byte state)
        {
            var vc = context.AddChildContext("Byte");
            vc.SetAttribute("value", state.ToString());
        }
    }

    public class BoolStateProvider
        : IStateProvider<bool, IStreamContext, IStreamContext>
        , IStateProvider<bool, IStringTreeContext, IStringTreeContext>
    {
        public static readonly BoolStateProvider Instance = new BoolStateProvider();

        private BoolStateProvider()
        {
            // nix
        }

        public bool Read(IStreamContext context)
        {
            return context.StreamRw.ReadBool();
        }

        public void Write(IStreamContext context, bool state)
        {
            context.StreamRw.WriteBool(state);
        }
        
        public bool Read(IStringTreeContext context)
        {
            return bool.Parse(context.GetChildContext("Bool").GetAttribute("value"));
        }

        public void Write(IStringTreeContext context, bool state)
        {
            var vc = context.AddChildContext("Bool");
            vc.SetAttribute("value", state.ToString());
        }
    }

    public class FloatStateProvider
        : IStateProvider<float, IStreamContext, IStreamContext>
        , IStateProvider<float, IStringTreeContext, IStringTreeContext>
    {
        public static readonly FloatStateProvider Instance = new FloatStateProvider();

        private FloatStateProvider()
        {
            // nix
        }

        public float Read(IStreamContext context)
        {
            return Utils.ReadFloat32(context.StreamRw.Stream);
        }

        public void Write(IStreamContext context, float state)
        {
            Utils.WriteFloat32(context.StreamRw.Stream, state);
        }
        
        public float Read(IStringTreeContext context)
        {
            return float.Parse(context.GetChildContext("Float").GetAttribute("value"));
        }

        public void Write(IStringTreeContext context, float state)
        {
            var vc = context.AddChildContext("Float");
            vc.SetAttribute("value", state.ToString("R"));
        }
    }

    public class DoubleStateProvider
        : IStateProvider<double, IStreamContext, IStreamContext>
        , IStateProvider<double, IStringTreeContext, IStringTreeContext>
    {
        public static readonly DoubleStateProvider Instance = new DoubleStateProvider();

        private DoubleStateProvider()
        {
            // nix
        }

        public double Read(IStreamContext context)
        {
            return Utils.ReadFloat64(context.StreamRw.Stream);
        }

        public void Write(IStreamContext context, double state)
        {
            Utils.WriteFloat64(context.StreamRw.Stream, state);
        }
        
        public double Read(IStringTreeContext context)
        {
            return double.Parse(context.GetChildContext("Double").GetAttribute("value"));
        }

        public void Write(IStringTreeContext context, double state)
        {
            var vc = context.AddChildContext("Double");
            vc.SetAttribute("value", state.ToString("R"));
        }
    }

    // this is a terrible class
    public class EnumStateProvider<T, TStorage> : IStateProvider<T, IStreamContext, IStreamContext> where T : struct where TStorage : struct
    {
        // derived from https://stackoverflow.com/a/26289874/383598
        private static class EnumConverter
        {
            public static readonly Func<TStorage, T> Convert = PrepareConverter();
            public static readonly Func<T, TStorage> ConvertBack = PrepareBackConverter();

            static Func<TStorage, T> PrepareConverter()
            {
                var parameter = System.Linq.Expressions.Expression.Parameter(typeof(TStorage));
                var dynamicMethod = System.Linq.Expressions.Expression.Lambda<Func<TStorage, T>>(
                    System.Linq.Expressions.Expression.Convert(parameter, typeof(T)),
                    parameter);
                return dynamicMethod.Compile();
            }

            static Func<T, TStorage> PrepareBackConverter()
            {
                var parameter = System.Linq.Expressions.Expression.Parameter(typeof(T));
                var dynamicMethod = System.Linq.Expressions.Expression.Lambda<Func<T, TStorage>>(
                    System.Linq.Expressions.Expression.Convert(parameter, typeof(TStorage)),
                    parameter);
                return dynamicMethod.Compile();
            }
        }

        public EnumStateProvider(IStateProvider<TStorage, IStreamContext, IStreamContext> stateProvider)
        {
            StateProvider = stateProvider;
        }

        public IStateProvider<TStorage, IStreamContext, IStreamContext> StateProvider { get; }

        public T Read(IStreamContext context)
        {
            return EnumConverter.Convert(StateProvider.Read(context));
        }

        public void Write(IStreamContext context, T state)
        {
            StateProvider.Write(context, EnumConverter.ConvertBack(state));
        }
    }

    // this is a terrible class
    public class EnumStringTreeStateProvider<T, TStorage> : IStateProvider<T, IStringTreeContext, IStringTreeContext> where T : struct where TStorage : struct
    {
        // derived from https://stackoverflow.com/a/26289874/383598
        private static class EnumConverter
        {
            public static readonly Func<TStorage, T> Convert = PrepareConverter();
            public static readonly Func<T, TStorage> ConvertBack = PrepareBackConverter();

            static Func<TStorage, T> PrepareConverter()
            {
                var parameter = System.Linq.Expressions.Expression.Parameter(typeof(TStorage));
                var dynamicMethod = System.Linq.Expressions.Expression.Lambda<Func<TStorage, T>>(
                    System.Linq.Expressions.Expression.Convert(parameter, typeof(T)),
                    parameter);
                return dynamicMethod.Compile();
            }

            static Func<T, TStorage> PrepareBackConverter()
            {
                var parameter = System.Linq.Expressions.Expression.Parameter(typeof(T));
                var dynamicMethod = System.Linq.Expressions.Expression.Lambda<Func<T, TStorage>>(
                    System.Linq.Expressions.Expression.Convert(parameter, typeof(TStorage)),
                    parameter);
                return dynamicMethod.Compile();
            }
        }

        public EnumStringTreeStateProvider(IStateProvider<TStorage, IStringTreeContext, IStringTreeContext> stateProvider)
        {
            StateProvider = stateProvider;
        }

        public IStateProvider<TStorage, IStringTreeContext, IStringTreeContext> StateProvider { get; }

        public T Read(IStringTreeContext context)
        {
            return EnumConverter.Convert(StateProvider.Read(context));
        }

        public void Write(IStringTreeContext context, T state)
        {
            StateProvider.Write(context, EnumConverter.ConvertBack(state));
        }
    }
    
    public class ArrayStateProvider<T, TWriteContext, TReadContext> : IClassStateProvider<T[], TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<T[], TWriteContext, TReadContext, object, object> where TWriteContext : IStreamContext where TReadContext : IStreamContext
    {
        public ArrayStateProvider(IStateProvider<T, TWriteContext, TReadContext> elementStateProvider)
        {
            ElementStateProvider = elementStateProvider;
        }

        public IStateProvider<T, TWriteContext, TReadContext> ElementStateProvider { get; }

        public IClassStateProvider<T[], TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this;
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }
        
        public void WriteProviderConfig(object context)
        {
            // nix
        }

        public T[] ReadCreate(TReadContext context)
        {
            int count = context.StreamRw.ReadInt32();
            
            T[] arr = new T[count];

            return arr;
        }

        public void ReadState(TReadContext context, T[] state)
        {
            for (int i = 0; i < state.Length; i++)
            {
                state[i] = ElementStateProvider.Read(context);
            }
        }
        
        public void WriteCreate(TWriteContext context, T[] state)
        {
            int count = state.Length;
            context.StreamRw.WriteInt32(count);
        }

        public void WriteState(TWriteContext context, T[] state)
        {
            for (int i = 0; i < state.Length; i++)
            {
                ElementStateProvider.Write(context, state[i]);
            }
        }
        
        public T[] Read(TReadContext context)
        {
            int count = context.StreamRw.ReadInt32();

            if (count < 0)
                return null;

            T[] arr = new T[count];

            for (int i = 0; i < count; i++)
            {
                arr[i] = ElementStateProvider.Read(context);
            }

            return arr;
        }

        public void Write(TWriteContext context, T[] state)
        {
            if (state == null)
            {
                context.StreamRw.WriteInt32(-1);
                return;
            }

            int count = state.Length;
            context.StreamRw.WriteInt32(count);
            
            for (int i = 0; i < count; i++)
            {
                ElementStateProvider.Write(context, state[i]);
            }
        }
    }
    
    public class ArrayStringTreeStateProvider<TElement, TWriteContext, TReadContext> : IClassStateProvider<TElement[], TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<TElement[], TWriteContext, TReadContext, object, object> where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext>
    {
        public ArrayStringTreeStateProvider(IStateProvider<TElement, TWriteContext, TReadContext> elementStateProvider)
        {
            ElementStateProvider = elementStateProvider;
        }
        
        public IStateProvider<TElement, TWriteContext, TReadContext> ElementStateProvider { get; }
        
        public IClassStateProvider<TElement[], TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this; // we are immutable
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public void WriteProviderConfig(object context)
        {
            // nix
        }
        
        public TElement[] ReadCreate(TReadContext context)
        {
            var length = IntStateProvider.Instance.Read(context.GetChildContext("Length"));
            return new TElement[length];
        }

        public void ReadState(TReadContext context, TElement[] state)
        {
            foreach (var entry in context.GetChildContext("Array").EnumerateChildContexts("Entry"))
            {
                int index = IntStateProvider.Instance.Read(context.GetChildContext("Index"));
                TElement elem = ElementStateProvider.Read(context.GetChildContext("Value"));

                state[index] = elem;
            }
        }
        
        public void WriteCreate(TWriteContext context, TElement[] state)
        {
            // nix
        }

        public void WriteState(TWriteContext context, TElement[] state)
        {
            context = context.AddChildContext("Array");
            
            for (int i = 0; i < state.Length; i++)
            {
                var entry = context.AddChildContext("Entry");
                IntStateProvider.Instance.Write(entry.AddChildContext("Index"), i);
                ElementStateProvider.Write(entry.AddChildContext("Value"), state[i]);
            }
        }
        
        public TElement[] Read(TReadContext context)
        {
            if (context.TryGetChildContext("Null") != null)
                return null;
            
            TElement[] dict = ReadCreate(context);
            ReadState(context, dict);

            return dict;
        }

        public void Write(TWriteContext context, TElement[] state)
        {
            if (state == null)
            {
                context.AddChildContext("Null");
            }
            
            WriteCreate(context, state);
            WriteState(context, state);
        }
    }

    // TODO: this can be generalised to TList : class, IList<T>, new()
    // errr.... maybe it can't... since it's an IClassStateProvider
    public class ListStateProvider<T, TWriteContext, TReadContext> : IClassStateProvider<List<T>, TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<List<T>, TWriteContext, TReadContext, object, object> where TWriteContext : IStreamContext where TReadContext : IStreamContext
    {
        public ListStateProvider(IStateProvider<T, TWriteContext, TReadContext> elementStateProvider)
        {
            ElementStateProvider = elementStateProvider;
        }

        public IStateProvider<T, TWriteContext, TReadContext> ElementStateProvider { get; }

        public IClassStateProvider<List<T>, TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this; // we are immutable
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public void WriteProviderConfig(object context)
        {
            // nix
        }

        public List<T> ReadCreate(TReadContext context)
        {
            return new List<T>();
        }

        public void ReadState(TReadContext context, List<T> state)
        {
            int count = context.StreamRw.ReadInt32();
            
            for (int i = 0; i < count; i++)
            {
                state.Add(ElementStateProvider.Read(context));
            }
        }

        public void WriteCreate(TWriteContext context, List<T> state)
        {
            // nix
        }

        public void WriteState(TWriteContext context, List<T> state)
        {
            int count = state.Count;
            context.StreamRw.WriteInt32(count);
            
            for (int i = 0; i < count; i++)
            {
                ElementStateProvider.Write(context, state[i]);
            }
        }
        
        public List<T> Read(TReadContext context)
        {
            int count = context.StreamRw.ReadInt32();

            if (count < 0)
                return null;

            List<T> list = new List<T>(count);

            for (int i = 0; i < count; i++)
            {
                list.Add(ElementStateProvider.Read(context));
            }

            return list;
        }
        
        public void Write(TWriteContext context, List<T> state)
        {
            if (state == null)
            {
                context.StreamRw.WriteInt32(-1);
                return;
            }

            int count = state.Count;
            context.StreamRw.WriteInt32(count);
            
            for (int i = 0; i < count; i++)
            {
                ElementStateProvider.Write(context, state[i]);
            }
        }
    }
    
    public class ListStringTreeStateProvider<TElement, TWriteContext, TReadContext> : IClassStateProvider<List<TElement>, TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<List<TElement>, TWriteContext, TReadContext, object, object> where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext>
    {
        public ListStringTreeStateProvider(IStateProvider<TElement, TWriteContext, TReadContext> elementStateProvider)
        {
            ElementStateProvider = elementStateProvider;
        }
        
        public IStateProvider<TElement, TWriteContext, TReadContext> ElementStateProvider { get; }
        
        public IClassStateProvider<List<TElement>, TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this; // we are immutable
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public void WriteProviderConfig(object context)
        {
            // nix
        }
        
        public List<TElement> ReadCreate(TReadContext context)
        {
            return new List<TElement>();
        }

        public void ReadState(TReadContext context, List<TElement> state)
        {
            foreach (var entry in context.GetChildContext("List").EnumerateChildContexts("Entry"))
            {
                state.Add(ElementStateProvider.Read(entry));
            }
        }
        
        public void WriteCreate(TWriteContext context, List<TElement> state)
        {
            // nix
        }

        public void WriteState(TWriteContext context, List<TElement> state)
        {
            context = context.AddChildContext("List");
            
            foreach (var elem in state)
            {
                var entry = context.AddChildContext("Entry");
                ElementStateProvider.Write(entry, elem);
            }
        }
        
        public List<TElement> Read(TReadContext context)
        {
            if (context.TryGetChildContext("Null") != null)
                return null;
            
            List<TElement> list = ReadCreate(context);
            ReadState(context, list);

            return list;
        }

        public void Write(TWriteContext context, List<TElement> state)
        {
            if (state == null)
            {
                context.AddChildContext("Null");
            }
            
            WriteCreate(context, state);
            WriteState(context, state);
        }
    }

    public class DictionaryStateProvider<TKey, TValue, TWriteContext, TReadContext> : IClassStateProvider<Dictionary<TKey, TValue>, TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<Dictionary<TKey, TValue>, TWriteContext, TReadContext, object, object> where TWriteContext : IStreamContext where TReadContext : IStreamContext
    {
        public DictionaryStateProvider(IStateProvider<TKey, TWriteContext, TReadContext> keyStateProvider, IStateProvider<TValue, TWriteContext, TReadContext> valueStateProvider)
        {
            KeyStateProvider = keyStateProvider;
            ValueStateProvider = valueStateProvider;
        }

        public IStateProvider<TKey, TWriteContext, TReadContext> KeyStateProvider { get; }
        public IStateProvider<TValue, TWriteContext, TReadContext> ValueStateProvider { get; }
        
        public IClassStateProvider<Dictionary<TKey, TValue>, TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this; // we are immutable
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public void WriteProviderConfig(object context)
        {
            // nix
        }
        
        public Dictionary<TKey, TValue> ReadCreate(TReadContext context)
        {
            return new Dictionary<TKey, TValue>();
        }
        public void ReadState(TReadContext context, Dictionary<TKey, TValue> state)
        {
            int count = context.StreamRw.ReadInt32();
            
            for (int i = 0; i < count; i++)
            {
                TKey key = KeyStateProvider.Read(context);
                TValue value = ValueStateProvider.Read(context);

                state.Add(key, value);
            }
        }
        
        public void WriteCreate(TWriteContext context, Dictionary<TKey, TValue> state)
        {
            // nix
        }

        public void WriteState(TWriteContext context, Dictionary<TKey, TValue> state)
        {
            int count = state.Count;
            context.StreamRw.WriteInt32(count);
            
            foreach (var vk in state)
            {
                KeyStateProvider.Write(context, vk.Key);
                ValueStateProvider.Write(context, vk.Value);
            }
        }
        
        public Dictionary<TKey, TValue> Read(TReadContext context)
        {
            int count = context.StreamRw.ReadInt32();

            if (count < 0)
                return null;

            Dictionary<TKey, TValue> dict = new Dictionary<TKey, TValue>(count);

            for (int i = 0; i < count; i++)
            {
                TKey key = KeyStateProvider.Read(context);
                TValue value = ValueStateProvider.Read(context);

                dict.Add(key, value);
            }

            return dict;
        }

        public void Write(TWriteContext context, Dictionary<TKey, TValue> state)
        {
            if (state == null)
            {
                context.StreamRw.WriteInt32(-1);
                return;
            }

            int count = state.Count;
            context.StreamRw.WriteInt32(count);
            
            foreach (var vk in state)
            {
                KeyStateProvider.Write(context, vk.Key);
                ValueStateProvider.Write(context, vk.Value);
            }
        }
    }

    public class DictionaryStringTreeStateProvider<TKey, TValue, TWriteContext, TReadContext> : IClassStateProvider<Dictionary<TKey, TValue>, TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<Dictionary<TKey, TValue>, TWriteContext, TReadContext, object, object> where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext>
    {
        public DictionaryStringTreeStateProvider(IStateProvider<TKey, TWriteContext, TReadContext> keyStateProvider, IStateProvider<TValue, TWriteContext, TReadContext> valueStateProvider)
        {
            KeyStateProvider = keyStateProvider;
            ValueStateProvider = valueStateProvider;
        }

        public IStateProvider<TKey, TWriteContext, TReadContext> KeyStateProvider { get; }
        public IStateProvider<TValue, TWriteContext, TReadContext> ValueStateProvider { get; }
        
        public IClassStateProvider<Dictionary<TKey, TValue>, TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this; // we are immutable
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public void WriteProviderConfig(object context)
        {
            // nix
        }
        
        public Dictionary<TKey, TValue> ReadCreate(TReadContext context)
        {
            return new Dictionary<TKey, TValue>();
        }

        public void ReadState(TReadContext context, Dictionary<TKey, TValue> state)
        {
            foreach (var entry in context.GetChildContext("Dictionary").EnumerateChildContexts("Entry"))
            {
                TKey key = KeyStateProvider.Read(context.GetChildContext("Key"));
                TValue value = ValueStateProvider.Read(context.GetChildContext("Value"));

                state.Add(key, value);
            }
        }
        
        public void WriteCreate(TWriteContext context, Dictionary<TKey, TValue> state)
        {
            // nix
        }

        public void WriteState(TWriteContext context, Dictionary<TKey, TValue> state)
        {
            context = context.AddChildContext("Dictionary");
            
            foreach (var vk in state)
            {
                var entry = context.AddChildContext("Entry");
                KeyStateProvider.Write(entry.AddChildContext("Key"), vk.Key);
                ValueStateProvider.Write(entry.AddChildContext("Value"), vk.Value);
            }
        }
        
        public Dictionary<TKey, TValue> Read(TReadContext context)
        {
            if (context.TryGetChildContext("Null") != null)
                return null;
            
            Dictionary<TKey, TValue> dict = ReadCreate(context);
            ReadState(context, dict);

            return dict;
        }

        public void Write(TWriteContext context, Dictionary<TKey, TValue> state)
        {
            if (state == null)
            {
                context.AddChildContext("Null");
            }
            
            WriteCreate(context, state);
            WriteState(context, state);
        }
    }

    public class TableStateProvider<T, TBase> : IStateProvider<T, ISimpleStreamContext<TBase>, ISimpleStreamContext<TBase>> where T : class, TBase
    {
        public static readonly TableStateProvider<T, TBase> Instance = new TableStateProvider<T, TBase>();

        public TableStateProvider()
        {
        }

        public T Read(ISimpleStreamContext<TBase> context)
        {
            // -1 is null (0 is error)
            long id = context.StreamRw.ReadInt64();

            if (id == 0)
                throw new Exception("Id read as 0");

            if (id == -1)
                return null;

            return (T)context.StateLookup.Lookup(id);
        }

        public void Write(ISimpleStreamContext<TBase> context, T state)
        {
            if (state == null)
            {
                context.StreamRw.WriteInt64(-1);
            }
            else
            {
                context.StreamRw.WriteInt64(context.StateLookup.LookupId(state));
            }
        }
    }
    
    public class EntityStateProvider<T, TBase> : IStateProvider<T, ISimpleStreamContext<TBase>, ISimpleStreamContext<TBase>> where T : class, TBase, IEntity
    {
        public static readonly EntityStateProvider<T, TBase> Instance = new EntityStateProvider<T, TBase>();

        public EntityStateProvider()
        {
        }
        
        public T Read(ISimpleStreamContext<TBase> context)
        {
            // -1 is null (0 is error)
            long id = context.StreamRw.ReadInt64();

            if (id == 0)
                throw new Exception("Id read as 0");

            if (id == -1)
                return null;

            return (T)context.StateLookup.Lookup(id);
        }

        public void Write(ISimpleStreamContext<TBase> context, T state)
        {
            if (state == null)
            {
                context.StreamRw.WriteInt64(-1);
            }
            else
            {
                context.StreamRw.WriteInt64(state.Id);
            }
        }
    }

    //public class BoxClassStateProvider<T, TProvider, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> : IClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderWriteContext>, IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderWriteContext> where TProvider : IStateProvider<T, TWriteContext, TReadContext> where T : class
    //{
    //    public static readonly BoxClassStateProvider<T, TProvider, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> Instance = new BoxClassStateProvider<T, TProvider, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>();

    //    private readonly IStateProvider<T, TWriteContext, TReadContext> StateProvider;

    //    public BoxClassStateProvider(IStateProvider<T, TWriteContext, TReadContext> stateProvider)
    //    {
    //        StateProvider = stateProvider;
    //    }

    //    public IClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderWriteContext> CreateNew()
    //    {
    //        return this;
    //    }

    //    public T Read(TReadContext context)
    //    {
    //        return ReadCreate(context);
    //    }

    //    public T ReadCreate(TReadContext context)
    //    {
    //        return StateProvider.Read(context);
    //    }

    //    public void ReadProviderConfig(TProviderWriteContext context)
    //    {
    //        // nix
    //    }

    //    public void ReadState(TReadContext context, T state)
    //    {
    //        // nix
    //    }

    //    public void Write(TWriteContext context, T state)
    //    {
    //        WriteCreate(context, state);
    //    }

    //    public void WriteCreate(TWriteContext context, T state)
    //    {
    //        StateProvider.Write(context, state);
    //    }

    //    public void WriteProviderConfig(TProviderWriteContext context)
    //    {
    //        // nix
    //    }

    //    public void WriteState(TWriteContext context, T state)
    //    {
    //        // nix
    //    }
    //}
}
