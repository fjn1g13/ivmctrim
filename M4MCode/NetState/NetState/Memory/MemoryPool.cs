﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace NetState.Memory
{
    public interface IPooledLightBuffer
    {
        LightBuffer LightBuffer { get; }
        void Free();
    }

    public class LightBufferPool
    {
        private ConcurrentStack<PooledLightbuffer> PooledBuffers = new ConcurrentStack<PooledLightbuffer>();

        private class PooledLightbuffer : IPooledLightBuffer
        {
            public PooledLightbuffer(LightBufferPool pool)
            {
                Pool = pool;
            }

            public bool Taken { get; private set; }
            private LightBufferPool Pool { get; }
            public LightBuffer LightBuffer { get; } = new LightBuffer();

            public void Take()
            {
                System.Diagnostics.Debug.Assert(!Taken);

                Taken = true;
            }

            public void Free()
            {
                System.Diagnostics.Debug.Assert(Taken);

                Pool.Free(this);
                Taken = false;

                LightBuffer.Clear();
            }
        }

        public IPooledLightBuffer Take()
        {
            if (PooledBuffers.TryPop(out var plb))
            {
                plb.Take();
                return plb;
            }
            else
            {
                plb = new PooledLightbuffer(this);
                plb.Take();
                return plb;
            }
        }

        private void Free(PooledLightbuffer plb)
        {
            PooledBuffers.Push(plb);
        }
    }

    public interface IPooledMemoryStream
    {
        MemoryStream MemoryStream { get; }
        void Free();
        IBufferView PrepareFixedView();
        IBufferView PrepareFixedView(int offset, int length);
    }

    public class MemoryStreamPool
    {
        private ConcurrentStack<PooledMemoryStream> PooledStreams = new ConcurrentStack<PooledMemoryStream>();

        private class PooledMemoryStream : IPooledMemoryStream
        {
            private struct FixedView : IBufferView
            {
                public FixedView(PooledMemoryStream pooledMemoryStream, int offset, int length) : this()
                {
                    PooledMemoryStream = pooledMemoryStream;
                    Offset = offset;
                    Length = length;

                    // we know this is a nice (0-offset) stream, because PooledMemoryStream made it
                    
                    Buffer = PooledMemoryStream.MemoryStream.GetBuffer();
                }

                private PooledMemoryStream PooledMemoryStream { get; }
                public int Offset { get; }
                public int Length { get; }
                
                private byte[] Buffer { get; }
                
                public async Task WriteToAsync(Stream targetStream)
                {
                    await targetStream.WriteAsync(Buffer, Offset, Length).ConfigureAwait(false);
                }
            }

            public PooledMemoryStream(MemoryStreamPool pool)
            {
                Pool = pool;
            }

            public bool Taken { get; private set; }
            private MemoryStreamPool Pool { get; }
            public MemoryStream MemoryStream { get; } = new MemoryStream();

            public void Take()
            {
                System.Diagnostics.Debug.Assert(!Taken);

                Taken = true;
            }

            public void Free()
            {
                System.Diagnostics.Debug.Assert(Taken);

                Pool.Free(this);
                Taken = false;

                MemoryStream.SetLength(0);
            }

            public IBufferView PrepareFixedView()
            {
                return PrepareFixedView(0, (int)MemoryStream.Length);
            }

            public IBufferView PrepareFixedView(int offset, int length)
            {
                if (offset < 0 || offset > MemoryStream.Length)
                    throw new ArgumentException("offset must be non-negative and no more than the length of the buffer", nameof(offset));
                if (length < 0 || offset + length > MemoryStream.Length)
                    throw new ArgumentException("length must be non-negative and must not extend beyond the length of the buffer", nameof(length));

                return new FixedView(this, offset, length);
            }
        }

        public IPooledMemoryStream Take()
        {
            if (PooledStreams.TryPop(out var pms))
            {
                pms.Take();
                return pms;
            }
            else
            {
                pms = new PooledMemoryStream(this);
                pms.Take();
                return pms;
            }
        }

        private void Free(PooledMemoryStream pms)
        {
            PooledStreams.Push(pms);
        }
    }
}
