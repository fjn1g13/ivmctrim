﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetState.Memory
{
    public interface IBufferView
    {
        Task WriteToAsync(Stream targetStream);
        int Length { get; }
    }

    public class CompositeBufferView : IBufferView
    {
        public CompositeBufferView(IBufferView[] bufferViews)
        {
            BufferViews = bufferViews;
        }

        public IBufferView[] BufferViews { get; }

        public int Length => BufferViews.Sum(bv => bv.Length);

        public async Task WriteToAsync(Stream targetStream)
        {
            for (int i = 0; i < BufferViews.Length; i++)
                await BufferViews[i].WriteToAsync(targetStream);
        }
    }

    public class ByteArrayBufferView : IBufferView
    {
        public ByteArrayBufferView(int length, int offset, byte[] buffer)
        {
            Length = length;
            Offset = offset;
            Buffer = buffer;
        }

        public int Length { get; }
        private int Offset { get; }
        private byte[] Buffer { get; }

        public async Task WriteToAsync(Stream targetStream)
        {
            await targetStream.WriteAsync(Buffer, Offset, Length).ConfigureAwait(false);
        }
    }

    public class SoloMemoryStreamBufferView : IBufferView
    {
        public SoloMemoryStreamBufferView(MemoryStream memoryStream, int offset)
        {
            MemoryStream = memoryStream;
            Offset = offset;
        }
        
        public SoloMemoryStreamBufferView(MemoryStream memoryStream)
        {
            MemoryStream = memoryStream;
            Offset = (int)memoryStream.Position;
        }

        private MemoryStream MemoryStream { get; }
        public int Length => (int)MemoryStream.Length - Offset;
        private int Offset { get; }

        public async Task WriteToAsync(Stream targetStream)
        {
            MemoryStream.Position = Offset;
            await MemoryStream.CopyToAsync(targetStream).ConfigureAwait(false);
        }
    }

    /// <summary>
    /// A simple buffer
    /// </summary>
    public class LightBuffer
    {
        private static readonly byte[] EmptyBuffer = new byte[0];

        private class View : IBufferView
        {
            public View(LightBuffer buffer, int offset, int length)
            {
                Buffer = buffer;
                Offset = offset;
                Length = length;
            }

            private LightBuffer Buffer { get; }
            private int Offset { get; }
            public int Length { get; }

            public async Task WriteToAsync(Stream targetStream)
            {
                await targetStream.WriteAsync(Buffer.Barr, Offset, Length).ConfigureAwait(false);
            }
        }

        private byte[] Barr;
        private int _Position;
        
        public LightBuffer()
        {
            Barr = EmptyBuffer;
            Length = 0;
            Position = 0;
        }

        public int Length { get; private set; }

        public int Position
        {
            get => _Position;
            set
            {
                if (value < 0 || value > Length)
                    throw new ArgumentException("Value must be non-negeative and no more than the length of the buffer", nameof(value));

                _Position = value;
            }
        }

        private void Allocate(int capacity)
        {
            if (Barr.Length < capacity)
            {
                var newBuffer = new byte[Math.Max(Barr.Length * 2, capacity)];
                System.Buffer.BlockCopy(Barr, 0, newBuffer, 0, Length);
                Barr = newBuffer;

                Length = capacity;
            }
        }

        public async Task WriteAsync(Stream sourceStream, int count)
        {
            Allocate(Position + count);
            await sourceStream.ReadAsync(Barr, Position, count).ConfigureAwait(false);
        }

        public void Write(byte[] sourceBarr, int offset, int count)
        {
            Allocate(Position + count);
            System.Buffer.BlockCopy(sourceBarr, offset, Barr, Position, count);
        }

        public void Clear()
        {
            Length = 0;
            Position = 0;
        }

        public int Capacity => Barr.Length;
        
        public IBufferView PrepareFixedView(int offset, int length)
        {
            if (offset < 0 || offset > Length)
                throw new ArgumentException("offset must be non-negeative and no more than the length of the buffer", nameof(offset));
            if (length < 0 || offset + length > Length)
                throw new ArgumentException("length must be non-negeative and not extend beyond the length of the buffer", nameof(length));

            return new View(this, offset, length);
        }
    }
}
