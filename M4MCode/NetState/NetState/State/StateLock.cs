﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NetState.State
{
    public class StateLock
    {
        System.Threading.SemaphoreSlim Semaphore = new System.Threading.SemaphoreSlim(1);
        private int LockToken = 0;

        public async Task<int> WaitAsync()
        {
            await Semaphore.WaitAsync().ConfigureAwait(false);
            if (LockToken > 100)
                LockToken = 0;
            return LockToken;
        }

        public int Wait()
        {
            Semaphore.Wait();
            if (LockToken > 100)
                LockToken = 0;
            return LockToken;
        }

        public void Free(int lockToken)
        {
            int newToken = System.Threading.Interlocked.Increment(ref LockToken); // returns incremented value
            Semaphore.Release();

            if (lockToken + 1 != newToken)
            {
                throw new Exception("Invalid LockToken, State may be compromised");
            }
        }
    }
}
