﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetState.State
{
    public interface IStateFactory<out T, in CX>
    {
        T Create(CX context);
        string UniqueName { get; }
        Type Type { get; }
    }

    public delegate T StateFactory<out T, in CX>(CX context);

    public class LambdaStateFactory<T, CX> : IStateFactory<T, CX>
    {
        private readonly StateFactory<T, CX> Factory;

        public LambdaStateFactory(StateFactory<T, CX> factory, string uniqueName)
        {
            Factory = factory;
            UniqueName = uniqueName;
            Type = typeof(T);
        }

        public string UniqueName { get; }
        public Type Type { get; }

        public T Create(CX context)
        {
            return Factory(context);
        }
    }

    public interface IStateFactoryLookup<out TBase, in CX, TKey>
    {
        IStateFactory<TBase, CX> Lookup(TKey key);
    }

    public class CustomFactoryLookup<TBase, CX, TKey> : IStateFactoryLookup<TBase, CX, TKey>
    {
        private readonly Dictionary<TKey, IStateFactory<TBase, CX>> Table = new Dictionary<TKey, IStateFactory<TBase, CX>>();
        
        public void Add(TKey key, IStateFactory<TBase, CX> factory)
        {
            Table.Add(key, factory);
        }

        public IStateFactory<TBase, CX> Lookup(TKey key)
        {
            return Table[key];
        }
    }

    public class FactoryLookup<TBase, CX> : IStateFactoryLookup<TBase, CX, string>
    {
        private readonly Dictionary<string, IStateFactory<TBase, CX>> NameTable = new Dictionary<string, IStateFactory<TBase, CX>>();
        
        public void Add(IStateFactory<TBase, CX> factory)
        {
            NameTable.Add(factory.UniqueName, factory);
        }

        public IStateFactory<TBase, CX> Lookup(string name)
        {
            return NameTable[name];
        }
    }

    // serious stuff

    public interface IStateFactoryInfo<TState, TStateFactoryContext, TCreationInfo>
    {
        IStateFactory<TState, TStateFactoryContext> StateFactory { get; }
        int StateFactoryId { get; }
        T Create<T>(TStateFactoryContext context, TCreationInfo creationInfo) where T : TState;
    }
    
    public interface IDestroyHandle
    {
        void Destroy();
    }
    
    public delegate void StateCreated<TState, TCreationInfo>(TState state, TCreationInfo creationInfo);

    // provides methods to lookup by name, assign by id, look up by id, and register callbacks
    public class NegotiableAssignmentStateFactoryProvider<TState, TCreationInfo, TStateFactoryContext>
    {
        private class StateFactoryInfo : IStateFactoryInfo<TState, TStateFactoryContext, TCreationInfo>
        {
            public StateFactoryInfo(IStateFactory<TState, TStateFactoryContext> stateFactory, int stateFactoryId)
            {
                StateFactory = stateFactory;
                StateFactoryId = stateFactoryId;
            }

            public IStateFactory<TState, TStateFactoryContext> StateFactory { get; }
            public int StateFactoryId { get; }

            public List<IUntypedStateFactoryCreatedCallback> Callbacks { get; } = new List<IUntypedStateFactoryCreatedCallback>();

            public T Create<T>(TStateFactoryContext context, TCreationInfo creationInfo) where T : TState
            {
                T t = (T)StateFactory.Create(context);

                foreach (var callback in Callbacks)
                    callback.Notify<T>(t, creationInfo);

                return t;
            }
        }

        private interface IUntypedStateFactoryCreatedCallback : IDestroyHandle
        {
            void Notify<T>(T state, TCreationInfo creationInfo);
        }

        private class StateFactoryCreatedCallback<T> : IUntypedStateFactoryCreatedCallback
        {
            private StateFactoryCreatedCallback(StateFactoryInfo stateFactoryInfo, StateCreated<T, TCreationInfo> callback)
            {
                StateFactoryInfo = stateFactoryInfo;
                Callback = callback;
            }

            public StateFactoryInfo StateFactoryInfo { get; }
            public StateCreated<T, TCreationInfo> Callback { get; }

            /// <summary>
            /// hooks itself up to the SFI
            /// </summary>
            public static IDestroyHandle Prepare(StateFactoryInfo stateFactoryInfo, StateCreated<T, TCreationInfo> callback)
            {
                if (typeof(T) != stateFactoryInfo.StateFactory.Type)
                    throw new Exception("Callback type not the same as SF type");

                StateFactoryCreatedCallback<T> sfcc = new StateFactoryCreatedCallback<T>(stateFactoryInfo, callback);
                stateFactoryInfo.Callbacks.Add(sfcc);

                return sfcc;
            }

            public void Destroy()
            {
                StateFactoryInfo.Callbacks.Remove(this);
            }

            public void Notify<TAssumed>(TAssumed state, TCreationInfo creationInfo)
            {
                if (state is T)
                {
                    Callback((T)(object)state, creationInfo);
                }
                else
                {
                    throw new Exception("Wrong type used somewhere to do with SF " + StateFactoryInfo.StateFactory.UniqueName + " (Local-Internal SFID " + StateFactoryInfo.StateFactoryId + ")");
                }
            }
        }
        
        private readonly IStateFactoryLookup<TState, TStateFactoryContext, string> StateFactoryLookupByName;

        private readonly Dictionary<string, StateFactoryInfo> StateFactoriesByString = new Dictionary<string, StateFactoryInfo>();
        private readonly Dictionary<int, StateFactoryInfo> StateFactoriesById = new Dictionary<int, StateFactoryInfo>();
        private int _nextSfId = 1;
        private int NextSfId => _nextSfId++;

        public NegotiableAssignmentStateFactoryProvider(IStateFactoryLookup<TState, TStateFactoryContext, string> stateFactoryLookupByName)
        {
            StateFactoryLookupByName = stateFactoryLookupByName;
        }

        public IDestroyHandle RegisterOnStateCreated<T>(string sfName, StateCreated<T, TCreationInfo> callback)
        {
            var sfInfo = InternalGrabStateInfoByName(sfName);
            return StateFactoryCreatedCallback<T>.Prepare(sfInfo, callback);
        }
        
        public IStateFactoryInfo<TState, TStateFactoryContext, TCreationInfo> LookupStateFactoryById(int id)
        {
            return StateFactoriesById[id];
        }

        public IStateFactoryInfo<TState, TStateFactoryContext, TCreationInfo> AssignFactoryByName(string name)
        {
            return InternalAssignFactoryByName(name);
        }

        private StateFactoryInfo InternalAssignFactoryByName(string name)
        {
            return InternalAssignFactoryIdByName(NextSfId, name);
        }

        public IStateFactoryInfo<TState, TStateFactoryContext, TCreationInfo> AssignFactoryIdByName(int id, string name)
        {
            return InternalAssignFactoryIdByName(id, name);
        }

        private StateFactoryInfo InternalAssignFactoryIdByName(int id, string name)
        {
            var factory = StateFactoryLookupByName.Lookup(name);
            return AssignFactory(id, factory);
        }

        private StateFactoryInfo AssignFactory(int sfId, IStateFactory<TState, TStateFactoryContext> stateFactory)
        {
            var sfi = new StateFactoryInfo(stateFactory, sfId);

            StateFactoriesByString.Add(stateFactory.UniqueName, sfi);
            StateFactoriesById.Add(sfId, sfi);

            return sfi;
        }

        public IStateFactoryInfo<TState, TStateFactoryContext, TCreationInfo> GrabStateInfoByName(string sfName)
        {
            return InternalGrabStateInfoByName(sfName);
        }

        private StateFactoryInfo InternalGrabStateInfoByName(string sfName)
        {
            if (StateFactoriesByString.TryGetValue(sfName, out StateFactoryInfo res))
            {
                return res;
            }
            else
            {
                return InternalAssignFactoryByName(sfName);
            }
        }

        public bool TryGetStateFactory(string sfName, out IStateFactoryInfo<TState, TStateFactoryContext, TCreationInfo> res)
        {
            if (StateFactoriesByString.TryGetValue(sfName, out StateFactoryInfo found))
            {
                res = found;
                return true;
            }
            else
            {
                res = null;
                return false;
            }
        }

        public IEnumerable<IStateFactoryInfo<TState, TStateFactoryContext, TCreationInfo>> EnumerateAssignedStateFactories()
        {
            return StateFactoriesById.Values;
        }

        public int AssignedFactoryCount => StateFactoriesById.Count;
    }
}
