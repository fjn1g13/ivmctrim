﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetState.State
{
    // state provider interfaces

    /// <summary>
    /// Writes the state out
    /// </summary>
    public interface IStateWriter<in T, in CX>
    {
        void Write(CX context, T state);
    }

    /// <summary>
    /// Reads, assembles, and returns the state
    /// </summary>
    public interface IStateReader<out T, in CX>
    {
        T Read(CX context);
    }

    /// <summary>
    /// Reads state into an existing instance
    /// </summary>
    public interface IStateInplaceReader<in T, in CX> where T : class
    {
        void Read(CX context, T state);
    }

    // state management interfaces

    public class IdRange
    {
        /// <summary>
        /// A range of IDs
        /// </summary>
        /// <param name="first">The first ID</param>
        /// <param name="last">The last ID (inclusive)</param>
        public IdRange(long first, long last)
        {
            if (first > last)
                throw new Exception("Last ID must be greater than First ID");

            if (first <= 0 && 0 <= last)
                throw new Exception("0 is not a legal ID");

            if (first <= -1 && -1 <= last)
                throw new Exception("-1 is not a legal ID");

            First = first;
            Last = last;
        }
        
        public long First { get; }
        public long Last { get; }

        public static IdRange FromCount(long first, long count)
        {
            return new IdRange(first, first + count - 1);
        }
    }

    public interface IStateLookup<TBase>
    {
        TBase Lookup(long id);
        long LookupId(TBase t);
    }

    public interface IStateTable<in TBase>
    {
        void Add(long id, TBase t);
        void RemoveById(long id);
        void Remove(TBase t);
        int Count { get; }
    }
    
    // could just pour stuff into this... and then have an `EntityTable` extend it
    public class SimpleStateTable<TBase> : IStateTable<TBase>, IStateLookup<TBase>
    {
        private readonly Dictionary<TBase, long> ReverseIndex = new Dictionary<TBase, long>();
        private readonly Dictionary<long, TBase> Table = new Dictionary<long, TBase>();

        public int Count => Table.Count;

        public void Add(long id, TBase t)
        {
            ReverseIndex.Add(t, id);
            Table.Add(id, t);
        }

        public TBase Lookup(long id)
        {
            return Table[id];
        }

        public long LookupId(TBase t)
        {
            return ReverseIndex[t];
        }

        public void Remove(TBase t)
        {
            var id = ReverseIndex[t];
            ReverseIndex.Remove(t);
            Table.Remove(id);
        }

        public void RemoveById(long id)
        {
            TBase t = Table[id];
            ReverseIndex.Remove(t);
            Table.Remove(id);
        }
        
        public bool TryLookup(long id, out TBase result)
        {
            return Table.TryGetValue(id, out result);
        }

        public bool LookupId(TBase t, out long result)
        {
            return ReverseIndex.TryGetValue(t, out result);
        }
    }
}
