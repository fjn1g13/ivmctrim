﻿using NetState.AutoState;
using NetState.Serialisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetState.SoftState
{
    public class SoftStateStringTreeClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> : IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where T : class where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext> where TProviderWriteContext : IStringTreeContext<TProviderWriteContext> where TProviderReadContext : IStringTreeContext<TProviderReadContext>
    {
        public static readonly SoftStateStringTreeClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> Instance = new SoftStateStringTreeClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>();

        public IClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> CreateNew()
        {
            return new SoftStateStringTreeClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>();
        }
    }

    public class SoftStateStringTreeClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> : IClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where T : class where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext> where TProviderWriteContext : IStringTreeContext<TProviderWriteContext> where TProviderReadContext : IStringTreeContext<TProviderReadContext>
    {
        private ISoftStatePropertyInfo<T, TWriteContext, TReadContext>[] Configuration = null;
        
        // force this as soon as we instanciate the class
        private static Func<T> Constructor = DefaultConstructor<T>.Constructor;

        public SoftStateStringTreeClassStateProvider()
        {
            SetDefaultConfiguration();
        }

        public void SetDefaultConfiguration()
        {
            Configuration = SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.SoftStateProperties.Values.ToArray();
        }

        // config
        public void ReadProviderConfig(TProviderReadContext context)
        {
            var softProperties = context.EnumerateChildContexts("SoftProperty").ToArray();
            
            int propertyCount = softProperties.Length;

            Configuration = new ISoftStatePropertyInfo<T, TWriteContext, TReadContext>[propertyCount];

            foreach (var propertyContext in softProperties)
            {
                int index = int.Parse(propertyContext.GetAttribute("Index"));
                string name = propertyContext.GetAttribute("Name");
                
                Configuration[index] = SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.SoftStateProperties[name];
            }
        }
        
        public void WriteProviderConfig(TProviderWriteContext context)
        {
            for (int i = 0; i < Configuration.Length; i++)
            {
                var name = Configuration[i].Name;

                var propertyContext = context.AddChildContext("SoftProperty");
                propertyContext.SetAttribute("Index", i.ToString());
                propertyContext.SetAttribute("Name", name);
            }
        }

        // create/state
        public T ReadCreate(TReadContext context)
        {
            return Constructor();
        }

        public void ReadState(TReadContext context, T state)
        {
            SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.PreRead?.Invoke(state, context);

            for (int i = 0; i < Configuration.Length; i++)
            {
                var propertyContext = context.AddChildContext("P");
                context.SetAttribute("Index", i.ToString());
                context.SetAttribute("Debug_Name", Configuration[i].Name);
                Configuration[i].Read(state, propertyContext);
            }

            SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.PostRead?.Invoke(state, context);
        }
        
        public void WriteCreate(TWriteContext context, T state)
        {
            // nix
        }

        public void WriteState(TWriteContext context, T state)
        {
            SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.PreWrite?.Invoke(state, context);

            for (int i = 0; i < Configuration.Length; i++)
                Configuration[i].Write(state, context);

            SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.PostWrite?.Invoke(state, context);
        }
        
        // state provider methods (no null support...)
        public void Write(TWriteContext context, T state)
        {
            if (state == null)
            {
                context.AddChildContext("Null");
            }
            else
            {
                WriteCreate(context.AddChildContext("Create"), state);
                WriteState(context.AddChildContext("State"), state);
            }
        }

        public T Read(TReadContext context)
        {
            if (context.TryGetChildContext("Null") != null)
                return null;
            
            T state = ReadCreate(context.GetChildContext("Create"));
            ReadState(context.GetChildContext("State"), state);
            return state;
        }
    }
    
    /// <summary>
    /// Provides a SoftStateClassProviderFactory for anything marked with an ISoftStateAttribute
    /// </summary>
    public class AutoSoftStateStringTreeProviders<TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> : ICustomStateProviderProvider<object, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext> where TProviderWriteContext : IStringTreeContext<TProviderWriteContext> where TProviderReadContext : IStringTreeContext<TProviderReadContext>
    {
        public readonly static AutoSoftStateStringTreeProviders<TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> Instance = new AutoSoftStateStringTreeProviders<TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>();
        
        private AutoSoftStateStringTreeProviders()
        {
        }

        public IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> TryResolve<T>(ref int version) where T : class
        {
            Type provisional = TryResolve(typeof(T));

            if (provisional == null)
                return null;

            return AutoSerialisationHelpers.AcquireInstance<IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>>(provisional);
        }

        public Type TryResolve(Type t)
        {
            if (t.GetCustomAttributesData().Any(cad => cad.AttributeType.GetInterfaces().Contains(typeof(ISoftStateAttribute))))
            {
                var at = typeof(SoftStateClassStateProviderFactory<,,,,>);
                var agt = at.MakeGenericType(
                    t,
                    typeof(TWriteContext),
                    typeof(TReadContext),
                    typeof(TProviderWriteContext),
                    typeof(TProviderReadContext)
                    );

                return agt;
            }
            
            return null;
        }
    }
}
