﻿using NetState.AutoState;
using NetState.Serialisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetState.SoftState
{
    public interface ISoftStateMethod
    {
    }

    public class SoftStateMethodAttribute : Attribute, ISoftStateMethod
    {
    }

    public interface ISoftStateAttribute
    {
    }

    public class SoftClassAttribute : Attribute, ISoftStateAttribute
    {
    }

    //[AttributeUsage(AttributeTargets.Property)] // not valid on iface
    public interface ISoftStatePropertyAttribute : IStateProviderAttribute
    {
        string Name { get; }
        object Label { get; }

        bool Deprecated { get; }
    }
    
    public class SoftPropertyAttributeAttribute : Attribute, ISoftStatePropertyAttribute
    {
        public string Name { get; }
        public object Label { get; }

        public bool Deprecated { get; }

        private object[] StateProviders;

        public SoftPropertyAttributeAttribute(string name, object label, params Type[] stateProviderTypes)
        {
            Name = name;
            Label = label;

            StateProviders = stateProviderTypes.Select(spt => AutoSerialisationHelpers.AcquireInstance<object>(spt)).ToArray();

            Deprecated = false;
        }

        public SoftPropertyAttributeAttribute(string name, object label, bool deprecated, params Type[] stateProviderTypes)
        {
            Name = name;
            Label = label;

            StateProviders = stateProviderTypes.Select(spt => AutoSerialisationHelpers.AcquireInstance<object>(spt)).ToArray();

            Deprecated = deprecated;
        }

        public virtual IStateProvider<PT, TSourceContext, TSinkContext> Prepare<PT, TSourceContext, TSinkContext>()
        {
            foreach (var stateProvider in StateProviders)
            {
                if (stateProvider is IStateProvider<PT, TSourceContext, TSinkContext> foundProvider)
                {
                    return foundProvider;
                }
                if (stateProvider is IStateProviderPreparer<TSourceContext, TSinkContext> foundProviderPreparer)
                {
                    var provisional = foundProviderPreparer.Prepare<PT>();

                    if (provisional != null)
                        return provisional;
                }
            }
            
            // nothing here is compatible; just return null
            return null;
        }
    }

    public interface ISoftStatePropertyInfo<OT, TWriteContext, TReadContext>
    {
        string Name { get; }

        bool Deprecated { get; }

        void Write(OT target, TWriteContext context);
        void Read(OT target, TReadContext context);
    }

    public class SoftStateProviderInfo<OT, TWriteContext, TReadContext>/* where TWriteContext : IStreamContext where TReadContext : IStreamContext*/
    {
        private class SoftStatePropertyInfo : ISoftStatePropertyInfo<OT, TWriteContext, TReadContext>
        {
            public string Name { get; }
            public AutoState.IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext> UntypedPropertyStateProvider { get; }

            public bool Deprecated { get; }

            public SoftStatePropertyInfo(string name, IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext> untypedPropertyStateProvider, bool deprecated)
            {
                Name = name;
                UntypedPropertyStateProvider = untypedPropertyStateProvider;

                Deprecated = deprecated;
            }

            public void Write(OT target, TWriteContext context)
            {
                UntypedPropertyStateProvider.Write(target, context);
            }

            public void Read(OT target, TReadContext context)
            {
                UntypedPropertyStateProvider.Read(target, context);
            }
        }

        private static ISoftStatePropertyInfo<OT, TWriteContext, TReadContext> Prepare<PT>(System.Reflection.PropertyInfo propertyInfo, ISoftStatePropertyAttribute sspa)
        {
            var usps = PropertyStateProvisioning<OT, TWriteContext, TReadContext>.Prepare<PT>(propertyInfo, sspa);
            return new SoftStatePropertyInfo(sspa.Name, usps, sspa.Deprecated);
        }

        private static readonly Lazy<SoftStateProviderInfo<OT, TWriteContext, TReadContext>> _instance = new Lazy<SoftStateProviderInfo<OT, TWriteContext, TReadContext>>(() => new SoftStateProviderInfo<OT, TWriteContext, TReadContext>());
        public static SoftStateProviderInfo<OT, TWriteContext, TReadContext> Instance => _instance.Value;
        
        public Dictionary<string, ISoftStatePropertyInfo<OT, TWriteContext, TReadContext>> SoftStateProperties { get; }

        public int Version { get; }
        
        public Action<OT, TReadContext> PreRead { get; }
        public Action<OT, TReadContext> PostRead { get; }
        public Action<OT, TWriteContext> PreWrite { get; }
        public Action<OT, TWriteContext> PostWrite { get; }

        public SoftStateProviderInfo()
        {
            Type type = typeof(OT);

            var properties = AutoSerialisationHelpers.EnumerateAllProperties(type);
            
            // accumultate properties
            SoftStateProperties = new Dictionary<string, ISoftStatePropertyInfo<OT, TWriteContext, TReadContext>>();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(true);
                bool hasAsa = false;
                bool hasValidAsa = false;

                foreach (var attribute in attributes)
                {
                    if (attribute is ISoftStatePropertyAttribute sspa)
                    {
                        hasAsa = true;
                        
                        var name = sspa.Name;
                        var sspi = AutoSerialisationHelpers.CallNamedStatic<ISoftStatePropertyInfo<OT, TWriteContext, TReadContext>>(
                            typeof(SoftStateProviderInfo<OT, TWriteContext, TReadContext>),
                            null,
                            nameof(SoftStateProviderInfo<OT, TWriteContext, TReadContext>.Prepare),
                            new[] { property.PropertyType },
                            property,
                            sspa);

                        if (sspi != null)
                        {
                            if (SoftStateProperties.ContainsKey(name))
                                throw new Exception("More than one property with name '" + name + "' in type " + type.Name + " (have you considered sub-types?)");

                            SoftStateProperties.Add(name, sspi);

                            hasValidAsa = true;
                            break;
                        }
                    }
                }

                if (hasAsa && !hasValidAsa)
                {
                    throw new Exception($"Property {property.Name} on type {type.FullName} has one or more associated StateProviders, but none are compatible with WriteContext {typeof(TWriteContext).FullName} and ReadContext {typeof(TReadContext).FullName}");
                }
            }

            // accumulate pre/post call methods
            var methods = AutoSerialisationHelpers.EnumerateAllMethods(type);

            foreach (var mi in methods)
            {
                if (!mi.GetCustomAttributesData().Any(cad => cad.AttributeType.GetInterfaces().Contains(typeof(ISoftStateMethod))))
                    continue;

                var mparams = mi.GetParameters();
                
                if (mi.Name == "PreRead" && mparams.Length == 1 && mparams[0].ParameterType.IsAssignableFrom(typeof(TReadContext)) && mi.ReturnType == typeof(void))
                {
                    PreRead = (Action<OT, TReadContext>)mi.CreateDelegate(typeof(Action<OT, TReadContext>));
                }

                if (mi.Name == "PostRead" && mparams.Length == 1 && mparams[0].ParameterType.IsAssignableFrom(typeof(TReadContext)) && mi.ReturnType == typeof(void))
                {
                    PostRead = (Action<OT, TReadContext>)mi.CreateDelegate(typeof(Action<OT, TReadContext>));
                }
                
                if (mi.Name == "PreWrite" && mparams.Length == 1 && mparams[0].ParameterType.IsAssignableFrom(typeof(TWriteContext)) && mi.ReturnType == typeof(void))
                {
                    PreWrite = (Action<OT, TWriteContext>)mi.CreateDelegate(typeof(Action<OT, TWriteContext>));
                }

                if (mi.Name == "PostWrite" && mparams.Length == 1 && mparams[0].ParameterType.IsAssignableFrom(typeof(TWriteContext)) && mi.ReturnType == typeof(void))
                {
                    PostWrite = (Action<OT, TWriteContext>)mi.CreateDelegate(typeof(Action<OT, TWriteContext>));
                }
            }
            
            // heh
            Version = -1;
        }
    }

    public class SoftStateClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> : IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where T : class where TWriteContext : IStreamContext where TReadContext : IStreamContext where TProviderWriteContext : IStreamContext where TProviderReadContext : IStreamContext
    {
        public static readonly SoftStateClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> Instance = new SoftStateClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>();

        public IClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> CreateNew()
        {
            return new SoftStateClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>();
        }
    }

    public class SoftStateClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> : IClassStateProvider<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where T : class where TWriteContext : IStreamContext where TReadContext : IStreamContext where TProviderWriteContext : IStreamContext where TProviderReadContext : IStreamContext
    {
        private ISoftStatePropertyInfo<T, TWriteContext, TReadContext>[] Configuration = null;
        
        // force this as soon as we instanciate the class
        private static Func<T> Constructor = DefaultConstructor<T>.Constructor;

        public SoftStateClassStateProvider()
        {
            SetDefaultConfiguration();
        }

        public void SetDefaultConfiguration()
        {
            Configuration = SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.SoftStateProperties.Values.Where(sspi => !sspi.Deprecated).ToArray();
        }

        // config
        public void ReadProviderConfig(TProviderReadContext context)
        {
            short propertyCount = context.StreamRw.ReadInt16();
            
            Configuration = new ISoftStatePropertyInfo<T, TWriteContext, TReadContext>[propertyCount];

            for (int i = 0; i < propertyCount; i++)
            {
                var name = context.StreamRw.ReadShortPascalString();
                Configuration[i] = SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.SoftStateProperties[name];
            }
        }
        
        public void WriteProviderConfig(TProviderWriteContext context)
        {
            int propertyCount = Configuration.Length;
            context.StreamRw.WriteInt16((short)propertyCount);
            
            for (int i = 0; i < propertyCount; i++)
            {
                var name = Configuration[i].Name;
                context.StreamRw.WriteShortPascalString(name);
            }
        }

        // create/state
        public T ReadCreate(TReadContext context)
        {
            return Constructor();
        }

        public void ReadState(TReadContext context, T state)
        {
            SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.PreRead?.Invoke(state, context);

            for (int i = 0; i < Configuration.Length; i++)
                Configuration[i].Read(state, context);

            SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.PostRead?.Invoke(state, context);
        }
        
        public void WriteCreate(TWriteContext context, T state)
        {
            // nix
        }

        public void WriteState(TWriteContext context, T state)
        {
            SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.PreWrite?.Invoke(state, context);

            for (int i = 0; i < Configuration.Length; i++)
                Configuration[i].Write(state, context);

            SoftStateProviderInfo<T, TWriteContext, TReadContext>.Instance.PostWrite?.Invoke(state, context);
        }
        
        // state provider methods (no null support...)
        public void Write(TWriteContext context, T state)
        {
            bool notNull = state != null;
            context.StreamRw.WriteBool(notNull);

            if (notNull)
            {
                WriteCreate(context, state);
                WriteState(context, state);
            }
        }

        public T Read(TReadContext context)
        {
            bool notNull = context.StreamRw.ReadBool();

            if (notNull)
            {
                T state = ReadCreate(context);
                ReadState(context, state);
                return state;
            }
            else
            {
                return null;
            }
        }
    }
    
    /// <summary>
    /// Provides a SoftStateClassProviderFactory for anything marked with an ISoftStateAttribute
    /// </summary>
    public class AutoSoftStateProviders<TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> : ICustomStateProviderProvider<object, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where TWriteContext : IStreamContext where TReadContext : IStreamContext where TProviderWriteContext : IStreamContext where TProviderReadContext : IStreamContext
    {
        public readonly static AutoSoftStateProviders<TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> Instance = new AutoSoftStateProviders<TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>();
        
        private AutoSoftStateProviders()
        {
        }

        public IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> TryResolve<T>(ref int version) where T : class
        {
            Type provisional = TryResolve(typeof(T));

            if (provisional == null)
                return null;

            return AutoSerialisationHelpers.AcquireInstance<IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>>(provisional);
        }

        public Type TryResolve(Type t)
        {
            if (t.GetCustomAttributesData().Any(cad => cad.AttributeType.GetInterfaces().Contains(typeof(ISoftStateAttribute))))
            {
                var at = typeof(SoftStateClassStateProviderFactory<,,,,>);
                var agt = at.MakeGenericType(
                    t,
                    typeof(TWriteContext),
                    typeof(TReadContext),
                    typeof(TProviderWriteContext),
                    typeof(TProviderReadContext)
                    );

                return agt;
            }
            
            return null;
        }
    }
}
