﻿using NetState.Serialisation;
using NetState.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NetState.AutoState
{
    public class AutoSerialisationStateProvider<OT, TWriteContext, TReadContext> where TWriteContext : IStreamContext where TReadContext : IStreamContext
    {
        private static IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext> Prepare<PT>(System.Reflection.PropertyInfo propertyInfo, IAutoSerialiseAttribute asa)
        {
            return PropertyStateProvisioning<OT, TWriteContext, TReadContext>.Prepare<PT>(propertyInfo, asa);
        }

        private static readonly Lazy<AutoSerialisationStateProvider<OT, TWriteContext, TReadContext>> _instance = new Lazy<AutoSerialisationStateProvider<OT, TWriteContext, TReadContext>>(() => new AutoSerialisationStateProvider<OT, TWriteContext, TReadContext>());
        public static AutoSerialisationStateProvider<OT, TWriteContext, TReadContext> Instance => _instance.Value;

        private readonly IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>[] UntypedStateProviderProviders;
        private readonly IAutoSerialiseAttribute[] AutoSerialiseAttributes;

        public int Version { get; }
        public int MaxVersion => AutoSerialiseAttributes.Max(asa => asa.MaxVersion);

        public AutoSerialisationStateProvider()
        {
            Type type = typeof(OT);

            var properties = type.GetProperties();
            var untypedStateProviderProviders = new List<KeyValuePair<IAutoSerialiseAttribute, IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>>>();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(true);
                bool hasAsa = false;
                bool hasValidAsa = false;

                foreach (var attribute in attributes)
                {
                    if (attribute is IAutoSerialiseAttribute asa)
                    {
                        hasAsa = true;

                        var idx = asa.Index;
                        var ussp = AutoSerialisationHelpers.CallNamedStatic<IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>>(
                            typeof(AutoSerialisationStateProvider<OT, TWriteContext, TReadContext>),
                            null,
                            nameof(Prepare),
                            new[] { property.PropertyType },
                            property,
                            asa);

                        if (ussp != null)
                        {
                            untypedStateProviderProviders.Add(new KeyValuePair<IAutoSerialiseAttribute, IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>>(asa, ussp));
                            hasValidAsa = true;
                            break;
                        }
                    }
                }

                if (hasAsa && !hasValidAsa)
                {
                    throw new Exception($"Property {property.Name} on type {type.FullName} has one or more AutoSerialisationAttributes, but none are compatible with WriteContext {typeof(TWriteContext).FullName} and ReadContext {typeof(TReadContext).FullName}");
                }
            }

            UntypedStateProviderProviders = untypedStateProviderProviders.Any() ? new IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>[untypedStateProviderProviders.Max(ussp => ussp.Key.Index) + 1] : new IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>[0];
            AutoSerialiseAttributes = untypedStateProviderProviders.Any() ? new IAutoSerialiseAttribute[untypedStateProviderProviders.Max(ussp => ussp.Key.Index) + 1] : new IAutoSerialiseAttribute[0];

            foreach (var kv in untypedStateProviderProviders)
            {
                UntypedStateProviderProviders[kv.Key.Index] = kv.Value;
                AutoSerialiseAttributes[kv.Key.Index] = kv.Key;
            }

            Version = -1;
        }

        public AutoSerialisationStateProvider(int version, Predicate<object> labelPredicate)
        {
            AutoSerialiseAttributes = Instance.AutoSerialiseAttributes.Where(
                x => (version == -1 || (x.MinVersion <= version && x.MaxVersion >= version))
                && (labelPredicate == null || labelPredicate(x.Label))
                ).ToArray();
            UntypedStateProviderProviders = new IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>[AutoSerialiseAttributes.Length];

            for (int i = 0; i < AutoSerialiseAttributes.Length; i++)
                UntypedStateProviderProviders[i] = Instance.UntypedStateProviderProviders[AutoSerialiseAttributes[i].Index];
        }

        public void Write(OT owner, TWriteContext context)
        {
            for (int i = 0; i < UntypedStateProviderProviders.Length; i++)
            {
                UntypedStateProviderProviders[i].Write(owner, context);
            }
        }

        public void Read(OT owner, TReadContext context)
        {
            for (int i = 0; i < UntypedStateProviderProviders.Length; i++)
            {
                UntypedStateProviderProviders[i].Read(owner, context);
            }
        }
    }

    public class DefaultConstructor<T>
    {
        public readonly static Func<T> Constructor = PrepareDefaultConstructor();

        private static Func<T> PrepareDefaultConstructor()
        {
            var ctor = typeof(T).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], null);

            if (ctor == null)
                throw new Exception("No default constructor for type " + typeof(T).Name);

            var lambda = System.Linq.Expressions.Expression.Lambda<Func<T>>(
                System.Linq.Expressions.Expression.New(ctor));
            var func = lambda.Compile();

            return func;
        }
    }

    public class AutoSerialisingClassStateProvider<OT, TWriteContext, TReadContext> : IClassStateProvider<OT, TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<OT, TWriteContext, TReadContext, object, object> where TWriteContext : IStreamContext where TReadContext : IStreamContext where OT : class
    {
        public static AutoSerialisingClassStateProvider<OT, TWriteContext, TReadContext> Instance => new AutoSerialisingClassStateProvider<OT, TWriteContext, TReadContext>(-1, null);
        private AutoSerialisationStateProvider<OT, TWriteContext, TReadContext> SerialisationInstance { get; }

        public string UniqueName { get; }

        public Type Type => typeof(OT);

        public int Version { get; }
        public object Label { get; }

        // force this as soon as we instanciate the class
        private static Func<OT> Constructor = DefaultConstructor<OT>.Constructor;

        public AutoSerialisingClassStateProvider(int version, object label)
        {
            Version = version;
            Label = label;

            SerialisationInstance = new AutoSerialisationStateProvider<OT, TWriteContext, TReadContext>(version, l => label == null || l == label);

            if (version == -1)
            {
                Version = SerialisationInstance.MaxVersion;
            }

            UniqueName = $"AutoSerialisingStateProvider<{typeof(OT).FullName}, {typeof(TWriteContext).FullName}, {typeof(TReadContext).FullName}>({Version}, {label}>";
        }

        public IClassStateProvider<OT, TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this;
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public void WriteProviderConfig(object context)
        {
            // nix
        }

        // class state provider methods
        public OT ReadCreate(TReadContext context)
        {
            return Constructor();
        }

        public void ReadState(TReadContext context, OT state)
        {
            SerialisationInstance.Read(state, context);
        }

        public void WriteState(TWriteContext context, OT state)
        {
            SerialisationInstance.Write(state, context);
        }

        public void WriteCreate(TWriteContext context, OT state)
        {
            // nix
        }

        // state provider methods
        public void Write(TWriteContext context, OT state)
        {
            WriteCreate(context, state);
            WriteState(context, state);
        }

        public OT Read(TReadContext context)
        {
            OT state = ReadCreate(context);
            ReadState(context, state);
            return state;
        }
    }
}
