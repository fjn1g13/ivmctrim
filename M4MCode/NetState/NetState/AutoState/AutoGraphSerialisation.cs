﻿using NetState.Serialisation;
using NetState.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NetState.AutoState
{
    public interface ICustomClassStateProviderAttribute
    {
        IClassStateProviderFactory<T, TSourceContext, TSinkContext, object, object> Prepare<T, TSourceContext, TSinkContext>() where T : class;
        int Version { get; }
    }
    
    public class CustomClassStateProviderAttribute : Attribute, ICustomClassStateProviderAttribute
    {
        public CustomClassStateProviderAttribute(Type stateProviderType, int version = 0)
        {
            StateProvider = AutoSerialisationHelpers.AcquireInstance<object>(stateProviderType);
            Version = version;
        }
        
        private object StateProvider { get; }
        public int Version { get; }

        public virtual IClassStateProviderFactory<T, TSourceContext, TSinkContext, object, object> Prepare<T, TSourceContext, TSinkContext>() where T : class
        {
            if (StateProvider is IClassStateProviderFactory<T, TSourceContext, TSinkContext, object, object> yep)
            {
                return yep;
            }
            else
            {
                throw new Exception($"Custom State Provider type {StateProvider.GetType().FullName} is not compatible with {typeof(IClassStateProviderFactory<T, TSourceContext, TSinkContext, object, object>)}");
            }
        }
    }

    // this stuff assumes a Type-based factory lookup
    public interface IWriteGraphStreamContext<TBase> : IStreamContext
    {
        void WriteRef<T>(T t) where T : class, TBase;
    }
    
    public interface IReadGraphStreamContext<TBase> : IStreamContext
    {
        T ReadRef<T>() where T : class, TBase;
    }

    public class AutoGraphStateProviderPreparer<TBase> : IStateProviderPreparer<IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>>
    {
        public IStateProvider<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> Prepare<T>()
        {
            var at = typeof(GraphStateProvider<,>);
            var agt = at.MakeGenericType(
                typeof(T),
                typeof(TBase)
                );

            // type gap to satisfy constraints
            return AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>>>(agt);
        }
    }

    public class GraphStateProvider<T, TBase> : IStateProvider<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> where T : class, TBase
    {
        public static readonly GraphStateProvider<T, TBase> Instance = new GraphStateProvider<T, TBase>();

        public GraphStateProvider()
        {
        }
        
        public T Read(IReadGraphStreamContext<TBase> context)
        {
            return context.ReadRef<T>();
        }

        public void Write(IWriteGraphStreamContext<TBase> context, T state)
        {
            context.WriteRef<T>(state);
        }
    }

    public interface ICustomStateProviderProvider<TBase, in TWriteContext, in TReadContext, in TProviderWriteContext, in TProviderReadContext> where TBase : class
    {
        /// <summary>
        /// Returns null if unable to resolve a state provider for the given generic type
        /// </summary>
        IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> TryResolve<T>(ref int version) where T : class, TBase;
    }

    public class CustomStateProviderProviders<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where TBase : class
    {
        private List<ICustomStateProviderProvider<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>> Providers { get; }

        public CustomStateProviderProviders(IEnumerable<ICustomStateProviderProvider<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>> providers = null)
        {
            Providers = providers?.ToList() ?? new List<ICustomStateProviderProvider<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext>>();
        }

        public void AddProvider(ICustomStateProviderProvider<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> provider)
        {
            Providers.Add(provider);
        }

        public IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> TryResolve<T>(ref int version) where T : class, TBase
        {
            foreach (var provider in Providers)
            {
                IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> provisonal = provider.TryResolve<T>(ref version);
                if (provisonal != null)
                    return provisonal;
            }

            return null;
        }
    }
    
    internal class Versioned<T>
    {
        public Versioned(T entry, int version)
        {
            Entry = entry;
            Version = version;
        }

        public T Entry { get; }
        public int Version { get; }
    }
    
    // TODO: need one of these which supports versions better, so that it can lookup by type and version, and uses the one with the highest version by default
    // (e.g. store a list of versioned providers, if version == -1, use the highest version, otherwise look up the specific version)
    public class CustomClassStateProviderTable<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> : ICustomStateProviderProvider<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where TBase : class
    {
        // csps
        private readonly Dictionary<Type, Dictionary<int, Versioned<object>>> Table = new Dictionary<Type, Dictionary<int, Versioned<object>>>();

        public IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> TryResolve<T>(ref int version) where T : class, TBase
        {
            if (!Table.TryGetValue(typeof(T), out var subTable))
            {
                return null;
            }

            if (subTable.TryGetValue(version, out var provisional))
            {
                if (provisional.Entry is IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> yep)
                {
                    return yep;
                }
                else
                {
                    throw new Exception($"A provider was found for type {typeof(T)} and version {version}, but was not compatible; please report this to the maintainer");
                }
            }
            else
            {
                var maxVersion = subTable.Max(vk => vk.Key);
                version = maxVersion;
                provisional = subTable[maxVersion];

                if (provisional.Entry is IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> yepMax)
                {
                    return yepMax;
                }
                else
                {
                    throw new Exception($"A default provider was found for type {typeof(T)}, but was not compatible; please report this to the maintainer");
                }
            }
        }

        public void Assign<T>(IClassStateProviderFactory<T, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> classStateProvider, int version) where T : class, TBase
        {
            if (version < 0)
                throw new ArgumentException("Version must be non-negative", nameof(version));

            if (!Table.TryGetValue(typeof(T), out var subTable))
            {
                subTable = new Dictionary<int, Versioned<object>>();
                Table.Add(typeof(T), subTable);
            }

            if (subTable.ContainsKey(version))
                throw new ArgumentException("An entry is already provided for the given type and version");

            subTable.Add(version, new Versioned<object>(classStateProvider, version));
        }
    }

    public class SerialisationInfo<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> where TBase : class
    {
        public SerialisationInfo(CustomClassStateProviderTable<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> customClassStateProviderTable, CustomStateProviderProviders<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> customProviders)
        {
            CustomClassStateProviderTable = customClassStateProviderTable;
            CustomClassStateProviderProviders = customProviders;
        }

        public CustomClassStateProviderTable<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> CustomClassStateProviderTable { get; }
        public CustomStateProviderProviders<TBase, TWriteContext, TReadContext, TProviderWriteContext, TProviderReadContext> CustomClassStateProviderProviders { get; }
    }

    public class GraphStreamSerialisationInfo<TBase> : SerialisationInfo<TBase, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> where TBase : class
    {
        public GraphStreamSerialisationInfo(CustomClassStateProviderTable<TBase, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> customClassStateProviderTable, CustomStateProviderProviders<TBase, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> customProviders, bool fallbackToAutoClassStateProvider) : base(customClassStateProviderTable, customProviders)
        {
            //CustomClassStateProviderTable = customClassStateProviderTable;
            //CustomClassStateProviderProviders = customProviders;

            FallbackToAutoClassStateProvider = fallbackToAutoClassStateProvider;
        }

        //public CustomClassStateProviderTable<TBase, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> CustomClassStateProviderTable { get; }
        //public CustomStateProviderProviders<TBase, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> CustomClassStateProviderProviders { get; }
        
        public bool FallbackToAutoClassStateProvider { get; }
    }

    public interface IGraphStreamWriter<TBase> where TBase : class
    {
        void WriteRoot<T>(T root) where T : class, TBase;
        void WriteRoots<T>(IReadOnlyList<T> roots) where T : class, TBase;
        void Reset(bool resetTypeAssignments);
    }

    public interface IGraphStreamReader<TBase> where TBase : class
    {
        T ReadRoot<T>() where T : class, TBase;
        IReadOnlyList<T> ReadRoots<T>() where T : class, TBase;
        void Reset(bool resetTypeAssignments);
    }

    public class AutoGraphSerialisation
    {
        private class BasicWriteContext<TBase> : IGraphStreamWriter<TBase>, IWriteGraphStreamContext<TBase> where TBase : class
        {
            public BasicWriteContext(StreamReaderWriter streamRw, GraphStreamSerialisationInfo<TBase> gsi)
            {
                StreamRw = streamRw;
                Gsi = gsi;
            }
            
            public Dictionary<Type, int> TypeAssignments { get; }  = new Dictionary<Type, int>();
            public Dictionary<int, IUntypedClassStateProvider<TBase>> ClassStateProviders { get; } = new Dictionary<int, IUntypedClassStateProvider<TBase>>();
            
            public Dictionary<TBase, long> Table { get; } = new Dictionary<TBase, long>();
            public Queue<KeyValuePair<long, TBase>> Queue { get; } = new Queue<KeyValuePair<long, TBase>>();

            public StreamReaderWriter StreamRw { get; }
            public GraphStreamSerialisationInfo<TBase> Gsi { get; }

            private int AssignType(Type t)
            {
                if (!TypeAssignments.TryGetValue(t, out int typeId))
                {
                    typeId = TypeAssignments.Count + 1;
                    TypeAssignments.Add(t, typeId);
                    
                    var stateProvider = AcquireStateProvider<TBase>(t, -1, Gsi); // version = -1 means we take the default
                    ClassStateProviders.Add(typeId, stateProvider);

                    return ~typeId;
                }
                else
                {
                    return typeId;
                }
            }

            public int LookupTypeId(Type t)
            {
                return TypeAssignments[t];
            }

            public void WriteRef<T>(T state) where T : class, TBase
            {
                if (state == null)
                {
                    StreamRw.WriteInt64(-1);
                    return;
                }

                if (!Table.TryGetValue(state, out long id))
                {
                    id = Table.Count + 1;
                    Table.Add(state, id);

                    // write id first
                    StreamRw.WriteInt64(~id);

                    var t = state.GetType();
                    var typeId = AssignType(t);

                    // then type info
                    StreamRw.WriteInt32(typeId);
                    if (typeId < 0)
                    {
                        typeId = ~typeId;

                        var csp = ClassStateProviders[typeId];
                        
                        StreamRw.WriteLongPascalString("#" + AutoSerialisationHelpers.AssembleSimpleTypeName(t));
                        StreamRw.WriteInt32(csp.Version);
                        csp.WriteConfig(this);
                    }
                    
                    ClassStateProviders[typeId].WriteCreate(this, state);
                    
                    Queue.Enqueue(new KeyValuePair<long, TBase>(id, state));
                }
                else
                {
                    StreamRw.WriteInt64(id);
                }
            }

            public void WriteRoot<T>(T root) where T : class, TBase
            {
                WriteRoots(new[] { root });
            }

            public void WriteRoots<T>(IReadOnlyList<T> roots) where T : class, TBase
            {
                StreamRw.WriteInt32(roots.Count);

                // write roots
                foreach (var root in roots)
                {
                    WriteRef<T>(root);
                }

                while (Queue.Count > 0)
                {
                    // these have already had their refs written out, so we know the type is good
                    var next = Queue.Dequeue();
                
                    var id = next.Key;
                    var state = next.Value;

                    var t = state.GetType();
                    var typeId = LookupTypeId(t);

                    StreamRw.WriteInt64(id);
                    StreamRw.WriteInt32(typeId);
                    ClassStateProviders[typeId].WriteState(this, next.Value);
                }

                // signal end
                StreamRw.WriteInt64(-1);
            }

            public void Reset(bool resetTypeAssignments)
            {
                Table.Clear();
                if (resetTypeAssignments)
                    TypeAssignments.Clear();
            }
        }

        private class BasicReadContext<TBase> : IGraphStreamReader<TBase>, IReadGraphStreamContext<TBase> where TBase : class
        {
            public BasicReadContext(StreamReaderWriter streamRw, GraphStreamSerialisationInfo<TBase> gsi)
            {
                StreamRw = streamRw;
                Gsi = gsi;
            }

            public Dictionary<long, TBase> Table { get; } = new Dictionary<long, TBase>();
            public Dictionary<int, IUntypedClassStateProvider<TBase>> ClassStateProviders { get; } = new Dictionary<int, IUntypedClassStateProvider<TBase>>();

            public StreamReaderWriter StreamRw { get; }
            public GraphStreamSerialisationInfo<TBase> Gsi { get; }

            public T ReadRef<T>() where T : class, TBase
            {
                long id = StreamRw.ReadInt64();

                if (id == 0)
                {
                    throw new Exception("Read invalid ID of 0");
                }

                if (id == -1)
                {
                    return null;
                }

                if (id < 0)
                {
                    id = ~id;

                    int typeId = StreamRw.ReadInt32();

                    if (typeId < 0)
                    {
                        typeId = ~typeId;
                        
                        string ftStr = StreamRw.ReadLongPascalString();
                        int typeVersion = StreamRw.ReadInt32();

                        char typeNameType = ftStr[0];
                        string typeName = ftStr.Substring(1);

                        switch (typeNameType)
                        {
                            case '#':
                                {
                                    ClassStateProviders.Add(typeId, AcquireStateProvider<TBase>(AutoSerialisationHelpers.ResolveTypeFromSimpleName(typeName), typeVersion, Gsi));
                                    break;
                                }
                            default:
                                throw new Exception("Unsupported type name type: '" + typeNameType + "'");
                        }
                    
                        ClassStateProviders[typeId].ReadConfig(this);
                    }

                    var csp = ClassStateProviders[typeId];

                    TBase state = csp.ReadCreate(this);
                    Table.Add(id, state);
                }

                return (T)Table[id];
            }

            public T ReadRoot<T>() where T : class, TBase
            {
                return ReadRoots<T>()[0];
            }

            public IReadOnlyList<T> ReadRoots<T>() where T : class, TBase
            {
                int rootCount = StreamRw.ReadInt32();
            
                T[] roots = new T[rootCount];

                // init roots
                for (int ri = 0; ri < rootCount; ri++)
                {
                    roots[ri] = ReadRef<T>();
                }

                while (true)
                {
                    // these have already had their refs read, so we know the typeIds are good, and that the state exists
                    long id = StreamRw.ReadInt64();

                    // end signal
                    if (id == -1)
                        break;

                    TBase state = Table[id];
                    int typeId = StreamRw.ReadInt32();
                
                    var csp = ClassStateProviders[typeId];
                    csp.ReadState(this, state);
                }

                return roots;
            }

            public void Reset(bool resetTypeAssignments)
            {
                Table.Clear();
                if (resetTypeAssignments)
                    ClassStateProviders.Clear();
            }
        }

        private interface IUntypedClassStateProvider<TBase>
        {
            void WriteConfig(IWriteGraphStreamContext<TBase> context);
            void ReadConfig(IReadGraphStreamContext<TBase> context);
            void WriteState(IWriteGraphStreamContext<TBase> context, TBase state);
            void ReadState(IReadGraphStreamContext<TBase> context, TBase state);
            TBase ReadCreate(IReadGraphStreamContext<TBase> context);
            void WriteCreate(IWriteGraphStreamContext<TBase> context, TBase state);
            int Version { get; }
        }

        private class TypedStateProvider<T, TBase> : IUntypedClassStateProvider<TBase> where T : class, TBase
        {
            public int Version { get; }

            private readonly IClassStateProvider<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> ClassStateProvider;
            
            public TypedStateProvider(IClassStateProviderFactory<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> classStateProviderFactory, int version)
                : this(classStateProviderFactory.CreateNew(), version)
            {
            }

            private TypedStateProvider(IClassStateProvider<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> classStateProvider, int version)
            {
                ClassStateProvider = classStateProvider;
                Version = version;
            }

            public void ReadConfig(IReadGraphStreamContext<TBase> context)
            {
                ClassStateProvider.ReadProviderConfig(context);
            }

            public void WriteConfig(IWriteGraphStreamContext<TBase> context)
            {
                ClassStateProvider.WriteProviderConfig(context);
            }

            public TBase ReadCreate(IReadGraphStreamContext<TBase> context)
            {
                return ClassStateProvider.ReadCreate(context);
            }

            public void WriteCreate(IWriteGraphStreamContext<TBase> context, TBase state)
            {
                ClassStateProvider.WriteCreate(context, (T)state);
            }

            public void ReadState(IReadGraphStreamContext<TBase> context, TBase state)
            {
                ClassStateProvider.ReadState(context, (T)state);
            }

            public void WriteState(IWriteGraphStreamContext<TBase> context, TBase state)
            {
                ClassStateProvider.WriteState(context, (T)state);
            }
        }
        
        private static IUntypedClassStateProvider<TBase> AcquireStateProvider<TBase>(Type targetType, int version, GraphStreamSerialisationInfo<TBase> gsi) where TBase : class
        {
            return AutoSerialisationHelpers.CallNamedStatic<IUntypedClassStateProvider<TBase>>(
                typeof(AutoGraphSerialisation),
                null,
                nameof(InternalAcquireStateProvider),
                new[] { targetType, typeof(TBase) },
                version, gsi);
        }

        private static IUntypedClassStateProvider<TBase> InternalAcquireStateProvider<T, TBase>(int version, GraphStreamSerialisationInfo<TBase> gsi) where TBase : class where T : class, TBase
        {
            // check the gsi custom table
            if (gsi != null && gsi.CustomClassStateProviderTable != null)
            {
                if (gsi.CustomClassStateProviderTable.TryResolve<T>(ref version) is IClassStateProviderFactory<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> found)
                {
                    var csp = found;
                    return new TypedStateProvider<T, TBase>(csp, version);
                }
            }
            
            // check the gsi custom providers
            if (gsi != null && gsi.CustomClassStateProviderProviders != null)
            {
                if (gsi.CustomClassStateProviderProviders.TryResolve<T>(ref version) is IClassStateProviderFactory<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>> found)
                {
                    var csp = found;
                    return new TypedStateProvider<T, TBase>(csp, version);
                }
            }

            // try a custom attribute
            var uccspa = typeof(T).GetCustomAttributes().FirstOrDefault(ca => ca is ICustomClassStateProviderAttribute);
            if (uccspa is ICustomClassStateProviderAttribute ccspa)
            {
                var csp = ccspa.Prepare<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>>();
                return new TypedStateProvider<T, TBase>(csp, ccspa.Version);
            }

            if (gsi == null || gsi.FallbackToAutoClassStateProvider)
            {
                // fall back to an Automatically constructed one   
                return AutoSerialisationHelpers.CallNamedStatic<IUntypedClassStateProvider<TBase>>(
                    typeof(AutoGraphSerialisation),
                    null,
                    nameof(InternalAcquireAutoClassStateProvider),
                    new[] { typeof(T), typeof(TBase) },
                    version);
            }

            throw new Exception("Unable to acquire a state provider for type " + typeof(T).FullName);
        }
        
        private static IUntypedClassStateProvider<TBase> InternalAcquireAutoClassStateProvider<T, TBase>(int version) where T : class, TBase
        {
            var csp = new AutoSerialisingClassStateProvider<T, IWriteGraphStreamContext<TBase>, IReadGraphStreamContext<TBase>>(version, null);
            return new TypedStateProvider<T, TBase>(csp, csp.Version);
        }

        public static IGraphStreamWriter<TBase> CreateWriter<TBase>(StreamReaderWriter streamRw, GraphStreamSerialisationInfo<TBase> gsi = null) where TBase : class
        {
           return new BasicWriteContext<TBase>(streamRw, gsi);
        }

        public static void WriteGraph<T, TBase>(StreamReaderWriter streamRw, T root, GraphStreamSerialisationInfo<TBase> gsi = null) where T : class, TBase where TBase : class
        {
            WriteGraphs<T, TBase>(streamRw, new T[] { root }, gsi);
        }
        
        public static void WriteGraphs<T, TBase>(StreamReaderWriter streamRw, T[] roots, GraphStreamSerialisationInfo<TBase> gsi = null) where T : class, TBase where TBase : class
        {   
            BasicWriteContext<TBase> context = new BasicWriteContext<TBase>(streamRw, gsi);
            context.WriteRoots<T>(roots);
        }
        
        public static IGraphStreamReader<TBase> CreateReader<TBase>(StreamReaderWriter streamRw, GraphStreamSerialisationInfo<TBase> gsi = null) where TBase : class
        {
           return new BasicReadContext<TBase>(streamRw, gsi);
        }

        public static T ReadGraph<T, TBase>(StreamReaderWriter streamRw, GraphStreamSerialisationInfo<TBase> gsi = null) where T : class, TBase where TBase : class
        {
            return ReadGraphs<T, TBase>(streamRw, gsi)[0];
        }
        
        public static IReadOnlyList<T> ReadGraphs<T, TBase>(StreamReaderWriter streamRw, GraphStreamSerialisationInfo<TBase> gsi = null) where T : class, TBase where TBase : class
        {
            BasicReadContext<TBase> context = new BasicReadContext<TBase>(streamRw, gsi);
            return context.ReadRoots<T>();
        }
    }
}
