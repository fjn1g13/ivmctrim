﻿using NetState.Serialisation;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetState.AutoState
{
    // NOTE: should use unmanged/enum if I can ever get 7.3 on my work machine
    #region enum
    public class AutoEnumStateProvider<T, TStorage, TStorageProvider> where TStorageProvider : IStateProvider<TStorage, IStreamContext, IStreamContext> where T : struct where TStorage : struct
    {
        public static readonly EnumStateProvider<T, TStorage> Instance = new EnumStateProvider<T, TStorage>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<TStorage, IStreamContext, IStreamContext>>(typeof(TStorageProvider)));
    }
    
    public class AutoEnumStringTreeStateProvider<T, TStorage, TStorageProvider> where TStorageProvider : IStateProvider<TStorage, IStringTreeContext, IStringTreeContext> where T : struct where TStorage : struct
    {
        public static readonly EnumStringTreeStateProvider<T, TStorage> Instance = new EnumStringTreeStateProvider<T, TStorage>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<TStorage, IStringTreeContext, IStringTreeContext>>(typeof(TStorageProvider)));
    }
    #endregion enum

    #region array
    public class AutoArrayStateProvider<T, TElementProvider> where TElementProvider : IStateProvider<T, IStreamContext, IStreamContext>
    {
        public static readonly ArrayStateProvider<T, IStreamContext, IStreamContext> Instance = new ArrayStateProvider<T, IStreamContext, IStreamContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, IStreamContext, IStreamContext>>(typeof(TElementProvider)));
    }

    public class AutoArrayStateProvider<T, TElementProvider, TWriteContext, TReadContext> where TElementProvider : IStateProvider<T, TWriteContext, TReadContext> where TWriteContext : IStreamContext where TReadContext : IStreamContext
    {
        public static readonly ArrayStateProvider<T, TWriteContext, TReadContext> Instance = new ArrayStateProvider<T, TWriteContext, TReadContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, TWriteContext, TReadContext>>(typeof(TElementProvider)));
    }

    public class AutoArrayStringTreeStateProvider<T, TElementProvider, TWriteContext, TReadContext> where TElementProvider : IStateProvider<T, TWriteContext, TReadContext> where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext>
    {
        public static readonly ArrayStringTreeStateProvider<T, TWriteContext, TReadContext> Instance = new ArrayStringTreeStateProvider<T, TWriteContext, TReadContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, TWriteContext, TReadContext>>(typeof(TElementProvider)));
    }
    #endregion array

    #region list
    public class AutoListStateProvider<T, TElementProvider> where TElementProvider : IStateProvider<T, IStreamContext, IStreamContext>
    {
        public static readonly ListStateProvider<T, IStreamContext, IStreamContext> Instance = new ListStateProvider<T, IStreamContext, IStreamContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, IStreamContext, IStreamContext>>(typeof(TElementProvider)));
    }

    public class AutoListStateProvider<T, TElementProvider, TWriteContext, TReadContext> where TElementProvider : IStateProvider<T, TWriteContext, TReadContext> where TWriteContext : IStreamContext where TReadContext : IStreamContext
    {
        public static readonly ListStateProvider<T, TWriteContext, TReadContext> Instance = new ListStateProvider<T, TWriteContext, TReadContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, TWriteContext, TReadContext>>(typeof(TElementProvider)));
    }

    public class AutoListStringTreeStateProvider<T, TElementProvider, TWriteContext, TReadContext> where TElementProvider : IStateProvider<T, TWriteContext, TReadContext> where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext>
    {
        public static readonly ListStringTreeStateProvider<T, TWriteContext, TReadContext> Instance = new ListStringTreeStateProvider<T, TWriteContext, TReadContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, TWriteContext, TReadContext>>(typeof(TElementProvider)));
    }
    #endregion list
    
    #region dictionary
    public class AutoDictionaryStateProvider<TKey, TValue, TKeyElementProvider, TValueElementProvider> where TKeyElementProvider : IStateProvider<TKey, IStreamContext, IStreamContext> where TValueElementProvider : IStateProvider<TValue, IStreamContext, IStreamContext>
    {
        public static readonly DictionaryStateProvider<TKey, TValue, IStreamContext, IStreamContext> Instance = new DictionaryStateProvider<TKey, TValue, IStreamContext, IStreamContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<TKey, IStreamContext, IStreamContext>>(typeof(TKeyElementProvider)), AutoSerialisationHelpers.AcquireInstance<IStateProvider<TValue, IStreamContext, IStreamContext>>(typeof(TValueElementProvider)));
    }

    public class AutoDictionaryStateProvider<TKey, TValue, TKeyElementProvider, TValueElementProvider, TWriteContext, TReadContext> where TKeyElementProvider : IStateProvider<TKey, TWriteContext, TReadContext> where TValueElementProvider : IStateProvider<TValue, TWriteContext, TReadContext> where TWriteContext : IStreamContext where TReadContext : IStreamContext
    {
        public static readonly DictionaryStateProvider<TKey, TValue, TWriteContext, TReadContext> Instance = new DictionaryStateProvider<TKey, TValue, TWriteContext, TReadContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<TKey, TWriteContext, TReadContext>>(typeof(TKeyElementProvider)), AutoSerialisationHelpers.AcquireInstance<IStateProvider<TValue, TWriteContext, TReadContext>>(typeof(TValueElementProvider)));
    }
    
    public class AutoDictionaryStringTreeStateProvider<TKey, TValue, TKeyElementProvider, TValueElementProvider, TWriteContext, TReadContext> where TKeyElementProvider : IStateProvider<TKey, TWriteContext, TReadContext> where TValueElementProvider : IStateProvider<TValue, TWriteContext, TReadContext> where TWriteContext : IStringTreeContext<TWriteContext> where TReadContext : IStringTreeContext<TReadContext>
    {
        public static readonly DictionaryStringTreeStateProvider<TKey, TValue, TWriteContext, TReadContext> Instance = new DictionaryStringTreeStateProvider<TKey, TValue, TWriteContext, TReadContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<TKey, TWriteContext, TReadContext>>(typeof(TKeyElementProvider)), AutoSerialisationHelpers.AcquireInstance<IStateProvider<TValue, TWriteContext, TReadContext>>(typeof(TValueElementProvider)));
    }
    #endregion dictionary
}
