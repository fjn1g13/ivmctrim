﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection;
using NetState.Serialisation;
using System.Collections.Concurrent;

namespace NetState.AutoState
{
    public delegate PT Getter<OT, PT>(OT owner);
    public delegate void Setter<OT, PT>(OT owner, PT value);
    
    public static class AutoSerialisationHelpers
    {
        private static readonly ConcurrentDictionary<Type, object> StateProviderCache = new ConcurrentDictionary<Type, object>();

        public static IEnumerable<PropertyInfo> EnumerateAllProperties<T>()
        {
            return EnumerateAllProperties(typeof(T));
        }

        public static IEnumerable<PropertyInfo> EnumerateAllProperties(Type type)
        {
            if (type == null)
                return Enumerable.Empty<PropertyInfo>();

            var mine = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            return mine.Concat(EnumerateAllProperties(type.BaseType));
        }

        public static IEnumerable<MethodInfo> EnumerateAllMethods<T>()
        {
            return EnumerateAllMethods(typeof(T));
        }

        public static IEnumerable<MethodInfo> EnumerateAllMethods(Type type)
        {
            if (type == null)
                return Enumerable.Empty<MethodInfo>();

            var mine = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            return mine.Concat(EnumerateAllMethods(type.BaseType));
        }

        public static TCast AcquireInstance<TCast>(Type stateProviderType, bool autoCache = true)
        {
            if (!StateProviderCache.TryGetValue(stateProviderType, out var stateProvider))
            {
                stateProvider = CreateInstance(stateProviderType);

                if (autoCache)
                {
                    StateProviderCache.TryAdd(stateProviderType, stateProvider);
                }
            }

            if (stateProvider is TCast yep)
            {
                return yep;
            }
            else
            {
                throw new Exception("Instance acquired from type \"" + stateProviderType.FullName + "\" was not of type \"" + typeof(TCast).FullName + "\"");
            }
        }

        private static object CreateInstance(Type stateProviderType)
        {
            var instanceField = stateProviderType.GetField("Instance", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            if (instanceField != null)
                return instanceField.GetValue(null);

            var instanceProperty = stateProviderType.GetProperty("Instance", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            if (instanceProperty != null)
                return instanceProperty.GetValue(null);

            return stateProviderType.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)[0].Invoke(new object[0]);
        }
        
        public static RT CallNamedStatic<RT>(Type type, Type[] typeArguments, string methodName, Type[] methodTypeArguments, params object[] methodArguments)
        {
            Type typedType = typeArguments != null ? type.MakeGenericType(typeArguments) : type;
            MethodInfo[] mis = typedType.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            MethodInfo imi = mis.First(mi => mi.Name == methodName);
            MethodInfo gmi = methodTypeArguments != null ? imi.MakeGenericMethod(methodTypeArguments) : imi;
            return (RT)gmi.Invoke(null, methodArguments);
        }
        
        public static string AssembleSimpleTypeName(Type t)
        {
            // need FullName, because it qualifies things like mscorlib:List<mydll:MyType>
            return t.Assembly.GetName().Name + "%" + t.FullName;
        }
        
        private static AssemblyMappings ProvisionalMappings = AssemblyMappings.PrepareDefaults();
        private static Dictionary<string, Assembly> FoundAssemblies = new Dictionary<string, Assembly>();

        /// <summary>
        /// When set to true, prevents the loading of assemblies.
        /// Any attempt to load an assembly which is not already loaded into the current AppDomain will fail with an exception.
        /// </summary>
        public static bool DisallowAssemblyLoading { get; set; } = true;

        /// <summary>
        /// When set to true, prevents the loading of assemblies that do not appear in AllowLoadWhiteList when DisallowAssemblyLoading is false.
        /// Has no effect if DisallowAssemblyLoading is true.
        /// </summary>
        public static bool EnableAllowLoadWhiteList { get; set; } = true;

        /// <summary>
        /// A set of assembly names that are allowed to be loaded when EnableAllowLoadWhiteList is set to true and DisallowAssemblyLoading is set to false.
        /// </summary>
        public static HashSet<string> AllowLoadWhiteList { get; } = new HashSet<string>();

        public static Assembly TryLoadAssembly(string name)
        {
            lock (FoundAssemblies)
            {
                try
                {
                    if (FoundAssemblies.TryGetValue(name, out var found))
                        return found;
                    else
                    {
                        Assembly a;
                        if (DisallowAssemblyLoading)
                        {
                            a = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(_a => _a.GetName().Name == name);
                            if (a == null)
                                throw new Exception("Assembly not already loaded.");
                        }
                        else if (!EnableAllowLoadWhiteList || AllowLoadWhiteList.Contains(name))
                        {
                            a = Assembly.Load(name);
                        }
                        else
                        {
                            throw new Exception("Assemly not in whitelist.");
                        }

                        FoundAssemblies.Add(name, a);
                        return a;
                    }
                }
                catch (Exception lex)
                {
                    foreach (var provisional in ProvisionalMappings[name])
                    {
                        try
                        {
                            Assembly a;
                            if (DisallowAssemblyLoading)
                            {
                                a = AppDomain.CurrentDomain.GetAssemblies().First(_a => _a.GetName().Name == provisional);
                                if (a == null)
                                    throw new Exception("Assembly not already loaded.");
                            }
                            else if (!EnableAllowLoadWhiteList || AllowLoadWhiteList.Contains(provisional))
                            {
                                a = Assembly.Load(provisional);
                            }
                            else
                            {
                                throw new Exception("Assemly not in whitelist.");
                            }

                            FoundAssemblies.Add(name, a);
                            return a;
                        }
                        catch
                        {
                        }
                    }

                    throw new Exception("Unable to load assembly '" + name + "', or any of it's provisional mappings: add more mappings to ProvisionalMappings if necessary. " + lex.Message, lex);
                }
            }
        }

        public static Type ResolveTypeFromSimpleName(string name)
        {
            var seperatorIdx = name.IndexOf('%');
            var assemblyName = name.Substring(0, seperatorIdx);
            var typeName = name.Substring(seperatorIdx + 1);
            return ResolveType(assemblyName, typeName);
        }

        private static Type ResolveType(string assemblyName, string typeName)
        {
            var a = TryLoadAssembly(assemblyName);
            return GetType(a, typeName);
        }

        private static Type GetType(string fullName)
        {
            // type names look like:
            // TypeName`3[[T1],[T2],[T3]][,,,], assemblyName, other stuff
            // We deal with finding the assemblyName, before passing to GetType(,)

            // name ends with the last bracket, else just before the first comma
            var nameEnd = fullName.LastIndexOf(']');
            if (nameEnd == -1)
                nameEnd = fullName.IndexOf(',') - 1;

            var start = fullName.IndexOf(',', nameEnd + 1) + 1;
            var end = fullName.IndexOf(',', start);

            var assemblyName = fullName.Substring(start, end - start).Trim();
            var assembly = TryLoadAssembly(assemblyName);
            var typeName = fullName.Substring(0, nameEnd + 1);
            return GetType(assembly, typeName);
        }

        private static Type GetType(Assembly assembly, string typeName)
        {
            // type names look like:
            // TypeName`3[[T1],[T2],[T3]][,,,], assemblyName, other stuff
            // We deal with everything before ", assemblyname"

            // first, check if it is an array
            int i = typeName.Length - 1;
            if (typeName[i] == ']')
            {
                i--;
                while (typeName[i] == ',')
                    i--;
                if (typeName[i] == '[')
                {
                    // we are an array
                    var rank = typeName.Length - i - 1;

                    var nonArrayTypeName = typeName.Substring(0, i);
                    var nonArrayType = GetType(assembly, nonArrayTypeName);

                    if (rank == 1)
                    {
                        return nonArrayType.MakeArrayType();
                    }
                    else
                    {
                        return nonArrayType.MakeArrayType(rank);
                    }
                }
            }

            // next, check if it generic
            var openTypeEnd = typeName.IndexOf('[');

            if (openTypeEnd >= 0)
            {
                // generic or array
                var openTypeName = typeName.Substring(0, openTypeEnd);
                var openType = assembly.GetType(openTypeName);

                var typeArguments = new List<Type>();

                i = openTypeEnd;

                int depth = 0;
                int lastStart = -1;
                while (i < typeName.Length)
                {
                    var c = typeName[i];
                    if (c == '[')
                    {
                        if (depth == 1)
                        {
                            lastStart = i + 1;
                        }

                        depth++;
                    }
                    else if (c == ']')
                    {
                        depth--;

                        if (depth == 1)
                        {
                            var subTypeName = typeName.Substring(lastStart, i - lastStart);
                            typeArguments.Add(GetType(subTypeName));
                        }
                    }
                    i++;
                }

                return openType.MakeGenericType(typeArguments.ToArray());
            }
            else
            {
                // standard
                return assembly.GetType(typeName);
            }
        }
    }

    public class AssemblyMappings
    {
        private Dictionary<string, HashSet<string>> Mappings = new Dictionary<string, HashSet<string>>();

        public static AssemblyMappings PrepareDefaults()
        {
            AssemblyMappings amaps = new AssemblyMappings();
            
            // HACK: this is inherently a bad idea, because stuff might appear in different assemblies, but hopefully it will allow me to focus on real work and not compatibility issues
            //  -> we should provide some way for CSPs to do something smarter... I don't konw what
            // NOTE: this only solves the problem of serialising these types themselves... they will still cause problems when used as generic parameters
            //  -> that issue is addressed by manually parsing the Full typename in the GetType methods
            amaps.Map("mscorlib", "System.Private.CoreLib"); // framework -> core
            amaps.Map("System.Private.CoreLib", "mscorlib"); // core -> framework

            return amaps;
        }

        public void Map(string from, string to)
        {
            if (Mappings.TryGetValue(from, out var list))
            {
                list.Add(to);
            }
            else
            {
                Mappings.Add(from, new HashSet<string>() { to });
            }
        }

        public bool TryGetMappings(string name, out IEnumerable<string> mappings)
        {
            if (Mappings.TryGetValue(name, out var list))
            {
                mappings = list;
                return true;
            }
            else
            {
                mappings = null;
                return false;
            }
        }

        public IEnumerable<string> this[string name]
        {
            get
            {
                if (TryGetMappings(name, out var mappings))
                    return mappings;
                else
                    return Enumerable.Empty<string>();
            }
        }
    }

    public interface IStateProviderAttribute
    {
        IStateProvider<PT, TSourceContext, TSinkContext> Prepare<PT, TSourceContext, TSinkContext>();
    }
    
    public interface IAutoSerialiseAttribute : IStateProviderAttribute
    {
        int Index { get; }
        int MinVersion { get; }
        int MaxVersion { get; }
        object Label { get; }
    }
    
    public class AutoSerialiseAttribute : Attribute, IAutoSerialiseAttribute
    {
        public AutoSerialiseAttribute(int index, object label, int minVersion, int maxVersion, params Type[] stateProviderTypes)
        {
            Index = index;

            Label = label;

            MinVersion = minVersion;
            MaxVersion = maxVersion;

            StateProviders = stateProviderTypes.Select(spt => AutoSerialisationHelpers.AcquireInstance<object>(spt)).ToArray();
        }
        
        public AutoSerialiseAttribute(int index, params Type[] stateProviderTypes)
        {
            Index = index;

            MinVersion = 0;
            MaxVersion = 0;

            Label = null;

            StateProviders = stateProviderTypes.Select(spt => AutoSerialisationHelpers.AcquireInstance<object>(spt)).ToArray();
        }
        
        public int Index { get; }
        private object[] StateProviders { get; }

        public int MinVersion { get; }
        public int MaxVersion { get; }

        public object Label { get; }

        public virtual IStateProvider<PT, TSourceContext, TSinkContext> Prepare<PT, TSourceContext, TSinkContext>()
        {
            foreach (var stateProvider in StateProviders)
            {
                if (stateProvider is IStateProvider<PT, TSourceContext, TSinkContext> yep)
                {
                    return yep;
                }
            }
            
            // nothing here is compatible; just return null
            return null;
        }
    }
    
    public interface IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>
    {
        void Write(OT owner, TWriteContext context);
        void Read(OT owner, TReadContext context);
    }

    public class SimplePropertyStateProvider<OT, TWriteContext, TReadContext, PT> : IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext>
    {
        public SimplePropertyStateProvider(IStateProvider<PT, TWriteContext, TReadContext> provider, Getter<OT, PT> getter, Setter<OT, PT> setter)
        {
            Provider = provider;
            Getter = getter;
            Setter = setter;
        }

        public readonly IStateProvider<PT, TWriteContext, TReadContext> Provider;
        public readonly Setter<OT, PT> Setter;
        public readonly Getter<OT, PT> Getter;

        public void Read(OT owner, TReadContext context)
        {
            Setter(owner, Provider.Read(context));
        }

        public void Write(OT owner, TWriteContext context)
        {
            if (Provider == null)
            {
                Console.WriteLine("NP");
            }
            if (owner == null)
            {
                Console.WriteLine("NO");
            }
            if (Getter == null)
            {
                Console.WriteLine("NG");
            }

            Provider.Write(context, Getter(owner));
        }
    }

    public static class PropertyStateProvisioning<OT, TWriteContext, TReadContext>
    {
        public static IUntypedPropertyStateProvider<OT, TWriteContext, TReadContext> Prepare<PT>(PropertyInfo propertyInfo, IStateProviderAttribute spa, bool throwOnMissingSetter = true)
        {
            var setter = (Setter<OT, PT>)propertyInfo.SetMethod?.CreateDelegate(typeof(Setter<OT, PT>));
            var getter = (Getter<OT, PT>)propertyInfo.GetMethod?.CreateDelegate(typeof(Getter<OT, PT>));
            var provider = spa.Prepare<PT, TWriteContext, TReadContext>();

            if (throwOnMissingSetter && setter == null)
            {
                var eMsg = "No setter for property " + propertyInfo.Name + " on type " + typeof(OT).Name;
                throw new Exception(eMsg);
            }

            if (getter == null)
            {
                var eMsg = "No getter for property " + propertyInfo.Name + " on type " + typeof(OT).Name;
                throw new Exception(eMsg);
            }

            if (provider == null)
            {
                var eMsg = "No compatible StateProvider found for property " + propertyInfo.Name + " on type " + typeof(OT).FullName;
                throw new Exception(eMsg);
            }

            var spsp = new SimplePropertyStateProvider<OT, TWriteContext, TReadContext, PT>(provider, getter, setter);

            return spsp;
        }
    }
}
