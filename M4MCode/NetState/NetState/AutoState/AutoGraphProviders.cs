﻿using NetState.Serialisation;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetState.AutoState
{
    /// <summary>
    /// Provides StateProviders for common primitive types
    ///  - Bool
    ///  - Byte
    ///  - Int
    ///  - Long
    ///  - String
    /// Class
    /// </summary>
    public class AutoGraphStateProviderTable : IStateProviderPreparer<IWriteGraphStreamContext<Object>, IReadGraphStreamContext<Object>>
    {
        public readonly static AutoGraphStateProviderTable Instance = new AutoGraphStateProviderTable();

        private readonly Dictionary<Type, Type> PrimitiveLookup = new Dictionary<Type, Type>();

        public AutoGraphStateProviderTable(bool includeDefaults = true)
        {
            if (includeDefaults)
                AddDefaults();
        }

        private void AddDefaults()
        {
            AddProvider(typeof(bool), typeof(BoolStateProvider));
            AddProvider(typeof(byte), typeof(ByteStateProvider));
            AddProvider(typeof(int), typeof(IntStateProvider));
            AddProvider(typeof(long), typeof(LongStateProvider));
            AddProvider(typeof(float), typeof(FloatStateProvider));
            AddProvider(typeof(double), typeof(DoubleStateProvider));
            AddProvider(typeof(string), typeof(StringStateProvider)); // TODO: make these graph objects?
        }

        public void AddProvider(Type targetType, Type providerType)
        {
            PrimitiveLookup.Add(targetType, providerType);
        }

        public IStateProvider<T, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>> TryResolveInstance<T>()
        {
            Type provisional = TryResolve(typeof(T));

            if (provisional == null)
                return null;

            return AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>>>(provisional);
        }

        public Type TryResolve(Type t)
        {
            if (PrimitiveLookup.TryGetValue(t, out var found))
            {
                return found;
            }
            else if (t.IsValueType == false)
            {
                Type gspt = typeof(GraphStateProvider<,>);
                Type gspgt = gspt.MakeGenericType(new[] { t, typeof(object) });
                return gspgt;
            }
            else
            {
                return null;
            }
        }

        public IStateProvider<T, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>> Prepare<T>()
        {
            return TryResolveInstance<T>();
        }
    }

    /// <summary>
    /// Provides ClassStateProviders for common types over the given AutoGraphStateProviderTable
    ///  - String
    ///  - List of T
    ///  - Dictionary of TKey, TValue
    /// </summary>
    public class AutoGraphClassStateProviderTable : ICustomStateProviderProvider<object, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>>
    {
        public readonly static AutoGraphClassStateProviderTable Instance = new AutoGraphClassStateProviderTable(AutoGraphStateProviderTable.Instance);
        
        private readonly Dictionary<Type, Type> PrimitiveLookup = new Dictionary<Type, Type>();
        public AutoGraphStateProviderTable AutoGraphStateProviderTable { get; }

        public AutoGraphClassStateProviderTable(AutoGraphStateProviderTable autoGraphStateProviderTable)
        {
            PrimitiveLookup.Add(typeof(string), typeof(StringStateProvider));
            AutoGraphStateProviderTable = autoGraphStateProviderTable;
        }

        public IClassStateProviderFactory<T, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>> TryResolve<T>(ref int version) where T : class
        {
            Type provisional = TryResolve(typeof(T));

            if (provisional == null)
                return null;

            return AutoSerialisationHelpers.AcquireInstance<IClassStateProviderFactory<T, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>>>(provisional);
        }

        public Type TryResolve(Type t)
        {
            // TODO: need to support Multi-Dim arrays
            // TODO: need to support BOXED Primitives

            if (PrimitiveLookup.TryGetValue(t, out var provisional))
            {
                return provisional;
            }

            if (t.IsArray)
            {
                if (t.GetArrayRank() != 1)
                    throw new NotImplementedException("Mutli-dimensional arrays are not presently supported for automatic serialisation; shout at the maintainer");
                
                var at = typeof(AutoArrayStateProvider<,,,>);
                var elementType = t.GetElementType();

                var agt = at.MakeGenericType(new[] {
                    elementType,
                    AutoGraphStateProviderTable.TryResolve(elementType),
                    typeof(IWriteGraphStreamContext<object>),
                    typeof(IReadGraphStreamContext<object>)
                    });

                return agt;
            }
            else if (t.IsGenericType)
            {
                var gtd = t.GetGenericTypeDefinition();
                var gta = t.GetGenericArguments();
                
                // TODO: ought to do arrays somehow
                if (gtd == typeof(List<>))
                { // List<ElementType>
                    var at = typeof(AutoListStateProvider<,,,>);
                    var elementType = gta[0];

                    var agt = at.MakeGenericType(new[] {
                        elementType,
                        AutoGraphStateProviderTable.TryResolve(elementType),
                        typeof(IWriteGraphStreamContext<object>),
                        typeof(IReadGraphStreamContext<object>)
                        });

                    return agt;
                }
                else if (gtd == typeof(Dictionary<,>))
                { // Dictionary<KeyType, ValueType>
                    var at = typeof(AutoDictionaryStateProvider<,,,,,>);
                    var keyType = gta[0];
                    var valueType = gta[1];

                    var agt = at.MakeGenericType(new[] {
                        keyType,
                        valueType,
                        AutoGraphStateProviderTable.TryResolve(keyType),
                        AutoGraphStateProviderTable.TryResolve(valueType),
                        typeof(IWriteGraphStreamContext<object>),
                        typeof(IReadGraphStreamContext<object>)
                        });

                    return agt;
                }
            }

            return null;
        }
    }
}
