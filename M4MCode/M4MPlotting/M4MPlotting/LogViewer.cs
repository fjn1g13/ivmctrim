﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M4MPlotting
{
    public partial class LogViewer : Form
    {
        public string Logs
        {
            get
            {
                return LogText.Text;
            }
            set
            {
                LogText.Text = value;
            }
        }

        public LogViewer()
        {
            InitializeComponent();
        }
    }
}
