﻿namespace M4MPlotting
{
    partial class PlotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopMenuStrip = new System.Windows.Forms.MenuStrip();
            this.FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FileOpenMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FileExportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.pathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ArgsText = new System.Windows.Forms.TextBox();
            this.ViewPanel = new System.Windows.Forms.Panel();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.InteractionsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.MainOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.MainSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.viewLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TopMenuStrip.SuspendLayout();
            this.ViewPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopMenuStrip
            // 
            this.TopMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenu});
            this.TopMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.TopMenuStrip.Name = "TopMenuStrip";
            this.TopMenuStrip.Size = new System.Drawing.Size(800, 24);
            this.TopMenuStrip.TabIndex = 0;
            this.TopMenuStrip.Text = "menuStrip1";
            // 
            // FileMenu
            // 
            this.FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileOpenMenu,
            this.FileExportMenu,
            this.pathToolStripMenuItem,
            this.viewLogsToolStripMenuItem});
            this.FileMenu.Name = "FileMenu";
            this.FileMenu.Size = new System.Drawing.Size(37, 20);
            this.FileMenu.Text = "File";
            // 
            // FileOpenMenu
            // 
            this.FileOpenMenu.Name = "FileOpenMenu";
            this.FileOpenMenu.Size = new System.Drawing.Size(180, 22);
            this.FileOpenMenu.Text = "Open";
            this.FileOpenMenu.Click += new System.EventHandler(this.FileOpenMenu_Click);
            // 
            // FileExportMenu
            // 
            this.FileExportMenu.Name = "FileExportMenu";
            this.FileExportMenu.Size = new System.Drawing.Size(180, 22);
            this.FileExportMenu.Text = "Export";
            this.FileExportMenu.Click += new System.EventHandler(this.FileExportMenu_Click);
            // 
            // pathToolStripMenuItem
            // 
            this.pathToolStripMenuItem.Name = "pathToolStripMenuItem";
            this.pathToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pathToolStripMenuItem.Text = "Path";
            this.pathToolStripMenuItem.Click += new System.EventHandler(this.pathToolStripMenuItem_Click);
            // 
            // ArgsText
            // 
            this.ArgsText.Dock = System.Windows.Forms.DockStyle.Top;
            this.ArgsText.Location = new System.Drawing.Point(0, 24);
            this.ArgsText.Name = "ArgsText";
            this.ArgsText.Size = new System.Drawing.Size(800, 20);
            this.ArgsText.TabIndex = 1;
            this.ArgsText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ArgsText_KeyDown);
            // 
            // ViewPanel
            // 
            this.ViewPanel.Controls.Add(this.ErrorLabel);
            this.ViewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewPanel.Location = new System.Drawing.Point(0, 44);
            this.ViewPanel.Name = "ViewPanel";
            this.ViewPanel.Size = new System.Drawing.Size(800, 406);
            this.ViewPanel.TabIndex = 2;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.BackColor = System.Drawing.SystemColors.Control;
            this.ErrorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ErrorLabel.Location = new System.Drawing.Point(0, 0);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(800, 406);
            this.ErrorLabel.TabIndex = 0;
            this.ErrorLabel.Text = "label1";
            this.ErrorLabel.Visible = false;
            // 
            // InteractionsPanel
            // 
            this.InteractionsPanel.AutoSize = true;
            this.InteractionsPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.InteractionsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.InteractionsPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.InteractionsPanel.Location = new System.Drawing.Point(800, 44);
            this.InteractionsPanel.Name = "InteractionsPanel";
            this.InteractionsPanel.Size = new System.Drawing.Size(0, 406);
            this.InteractionsPanel.TabIndex = 0;
            // 
            // MainSaveFileDialog
            // 
            this.MainSaveFileDialog.AddExtension = false;
            this.MainSaveFileDialog.DefaultExt = "pdf";
            // 
            // viewLogsToolStripMenuItem
            // 
            this.viewLogsToolStripMenuItem.Name = "viewLogsToolStripMenuItem";
            this.viewLogsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewLogsToolStripMenuItem.Text = "View Logs";
            this.viewLogsToolStripMenuItem.Click += new System.EventHandler(this.viewLogsToolStripMenuItem_Click);
            // 
            // PlotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ViewPanel);
            this.Controls.Add(this.InteractionsPanel);
            this.Controls.Add(this.ArgsText);
            this.Controls.Add(this.TopMenuStrip);
            this.DoubleBuffered = true;
            this.Name = "PlotForm";
            this.Text = "M4M Plot Preview";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.TopMenuStrip.ResumeLayout(false);
            this.TopMenuStrip.PerformLayout();
            this.ViewPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip TopMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileMenu;
        private System.Windows.Forms.ToolStripMenuItem FileOpenMenu;
        private System.Windows.Forms.TextBox ArgsText;
        private System.Windows.Forms.Panel ViewPanel;
        private System.Windows.Forms.OpenFileDialog MainOpenFileDialog;
        private System.Windows.Forms.ToolStripMenuItem FileExportMenu;
        private System.Windows.Forms.SaveFileDialog MainSaveFileDialog;
        private System.Windows.Forms.FlowLayoutPanel InteractionsPanel;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.ToolStripMenuItem pathToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewLogsToolStripMenuItem;
    }
}

