﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M4MPlotting
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var form = new PlotForm();
            if (args.Length == 1)
            {
                form.DataFileName = args[0];
            }
            else if (args.Length > 1)
            {
                form.DataFileName = args[0];
                form.Args = args.Skip(1).ToArray();
            }
            Application.Run(form);
        }
    }
}
