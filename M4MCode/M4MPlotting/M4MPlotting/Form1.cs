﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using M4M;
using OxyPlot;
using OxyPlot.WindowsForms;

namespace M4MPlotting
{
    public interface IPlotInteractions
    {
        string Key { get; }
        void Apply(PlotForm plotForm);
        void Disable();
    }

    public partial class PlotForm : Form
    {
        public OxyPlot.WindowsForms.PlotView PlotView { get; }
        private List<IPlotInteractions> PlotInteractions { get; }
        private List<ICliPlotter> FilePlotters { get; }
        private CliPlot CliPlot { get; }
        private CliMultiPlot CliMultiPlot { get; }
        public string DataFileName { get; set; } = null;
        private IPlotInteractions CurrentPlotInteractions = null;
        public Panel PlotInteractionsPanel => this.InteractionsPanel;
        public string LastError { get; private set; }

        private string logs { get; set; }
        private LogViewer logViewer;

        private void UpdateLogViewer()
        {
            if (logViewer != null)
            {
                logViewer.Logs = logs;
            }
        }

        public LogViewer GetLogViewer()
        {
            if (logViewer == null || logViewer.IsDisposed)
            {
                // to lazy to make this thread safe
                logViewer = new LogViewer();
                logViewer.Logs = logs;
            }

            return logViewer;
        }

        public string[] Args
        {
            get
            {
                return M4M.StringHelpers.Split(ArgsText.Text).ToArray();
            }
            set
            {
                ArgsText.Text = M4M.StringHelpers.Join(value);
            }
        }

        private ICliPlotter DeterminePlotterForFilename(string filename)
        {
            string justname = System.IO.Path.GetFileName(filename);
            return FilePlotters.Single(fp => justname.StartsWith(fp.Prefix, StringComparison.InvariantCultureIgnoreCase));
        }

        private IPlotInteractions DeterminePlotInteractionsForFilename(string filename)
        {
            string justname = System.IO.Path.GetFileName(filename);
            return PlotInteractions.SingleOrDefault(fp => justname.StartsWith(fp.Key, StringComparison.InvariantCultureIgnoreCase));
        }

        public PlotForm()
        {
            InitializeComponent();
            PlotView = new OxyPlot.WindowsForms.PlotView();
            PlotView.KeyDown += PlotView_KeyDown;

            FilePlotters = CliPlot.CreateDefaultPlotters().ToList();

            CliPlot = new CliPlot(null, FilePlotters, CliPlot.DefaultPresets);
            CliMultiPlot = new CliMultiPlot(CliPlot);

            PlotInteractions = new List<IPlotInteractions> { new TrajectoryPlotInteractions("rcs", "epoch"), new TracePlotInteractions("wholesamples", "epoch"), new TracePlotInteractions("tracee", "generation"), new GenomePlotInteractions("genome"), new GenomePlotInteractions("epoch") };
        }

        private void PlotView_KeyDown(object sender, KeyEventArgs e)
        {
            if (PlotView?.Model != null)
            {
                if (e.KeyCode == Keys.Space)
                {
                    foreach (var s in PlotView.Model.Series)
                        s.ClearSelection();
                    PlotView.Invalidate();
                }
            }
        }

        private PlotModel Plot(string filename, CliParams clips)
        {
            var sb = new StringBuilder();
            var console = new System.IO.StringWriter(sb);

            try
            {
                bool multiPlot = clips.IsSet("multiplot");

                string title = clips.Get("title", System.IO.Path.GetFileName(filename));

                PlotModel plot;
                if (multiPlot)
                {
                    if (!CliMultiPlot.PreparePlot(console, clips, filename, title, out plot))
                    {
                        LastError = "Unable to multi-plot";
                        return null;
                    }
                }
                else
                {
                    if (!CliPlot.PreparePlot(console, clips, filename, title, out plot))
                    {
                        LastError = "Unable to plot";
                        return null;
                    }
                }

                CliPlot.PreProcessPlot(clips, plot);

                Cli.PrintUnobserved(console, clips);

                CliPlot.PrintBasicInfo(console, plot);

                LastError = null;
                return plot;
            }
            catch (Exception ex)
            {
                LastError = ex.Message + "\n" + ex.StackTrace;
                return null;
            }
            finally
            {
                console.Dispose();
                var unmodified = sb.ToString();
                logs = System.Text.RegularExpressions.Regex.Replace(unmodified, "\r?\n", Environment.NewLine);
            }
        }
        
        private async void Form1_Load(object sender, EventArgs e)
        {
            ViewPanel.Controls.Add(PlotView);
            PlotView.Dock = DockStyle.Fill;
            PlotView.BackColor = Color.White;

            await ReloadOffThread();
        }

        private async Task ReloadAsync()
        {
            CliParams clips = new CliParams();
            clips.Consume(Args);

            if (DataFileName == null)
            {
                if (clips.IsSet("misc"))
                    DataFileName = "misc";
                else
                    return;
            }
            
            var model = await Task.Run(() => Plot(DataFileName, clips));
            PlotView.Model = model;
            PlotView.InvalidatePlot(true);

            if (LastError == null)
            {
                ErrorLabel.Text = "";
                ErrorLabel.Visible = false;
            }
            else
            {
                ErrorLabel.Text = LastError;
                ErrorLabel.Visible = true;
            }

            CurrentPlotInteractions?.Disable();
            CurrentPlotInteractions = DeterminePlotInteractionsForFilename(DataFileName);
            CurrentPlotInteractions?.Apply(this);

            UpdateLogViewer();
        }
        
        public async Task ReloadOffThread()
        {
            await ReloadAsync();
//            await Task.Run(ReloadAsync);
        }

        private async void ArgsText_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = false;
            if (e.KeyCode == Keys.Return)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                await ReloadOffThread();
            }
        }

        private async void FileOpenMenu_Click(object sender, EventArgs e)
        {
            var result = MainOpenFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                DataFileName = MainOpenFileDialog.FileName;

                await ReloadOffThread();
            }
        }

        private void FileExportMenu_Click(object sender, EventArgs e)
        {
            var result = MainSaveFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string saveFilename = MainSaveFileDialog.FileName;
                if (saveFilename.EndsWith(".pdf"))
                    ExportToPdf(saveFilename.Substring(0, saveFilename.Length - 4));
                else if (saveFilename.EndsWith(".png"))
                    ExportToPng(saveFilename.Substring(0, saveFilename.Length - 4));
                else
                {
                    // default to pdf
                    ExportToPdf(saveFilename);
                }
            }
        }

        private void pathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(DataFileName);
        }

        public void ExportToPng(string filename)
        {
            var plot = PlotView.Model;
            
            OxyPlot.WindowsForms.PngExporter exporter = new OxyPlot.WindowsForms.PngExporter();
            exporter.Width = PlotView.Width;
            exporter.Height = PlotView.Height;
        
            filename += ".png";
            if (System.IO.File.Exists(filename))
                System.IO.File.Delete(filename); // something doesn't like writing onto plots apparently

            using (var fs = new System.IO.FileStream(filename, System.IO.FileMode.OpenOrCreate))
            {
                exporter.Export(plot, fs);
            }
        }

        public void ExportToPdf(string filename)
        {
            var plot = PlotView.Model;

            //PlotView.Model = plot;
            
            OxyPlot.PdfExporter exporter = new PdfExporter();
            exporter.Width = PlotView.Width;
            exporter.Height = PlotView.Height;
        
            filename += ".pdf";
            if (System.IO.File.Exists(filename))
                System.IO.File.Delete(filename); // something doesn't like writing onto plots apparently

            using (var fs = new System.IO.FileStream(filename, System.IO.FileMode.OpenOrCreate))
            {
                exporter.Export(plot, fs);
            }
        }

        private void viewLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var lv = GetLogViewer();
            lv.Show();
            lv.BringToFront();
        }
    }

    public class GenomePlotInteractions : IPlotInteractions
    {
        public GenomePlotInteractions(string key)
        {
            Key = key;

            InitGenomePanel();
        }

        public string Key { get; }
        
        private Panel GenomePanel;
        private RadioButton[] GenomeRadios;
        private RadioButton DtmRadio;
        private RadioButton IsRadio;
        private RadioButton PsRadio;
        private RadioButton DevRadio;
        private RadioButton BiasRadio;

        private PlotForm TargetPlotForm { get; set; }

        private void InitGenomePanel()
        {
            GenomePanel = new Panel();
            GenomePanel.AutoSize = true;

            GenomeRadios = new RadioButton[] {
                DtmRadio = new RadioButton() { Text = "Dtm", Checked = false, Tag = "dtm" },
                IsRadio = new RadioButton() { Text = "Is", Checked = false, Tag = "is" },
                PsRadio = new RadioButton() { Text = "Ps", Checked = false, Tag = "ps" },
                DevRadio = new RadioButton() { Text = "Dev", Checked = false, Tag = "dev" },
                BiasRadio = new RadioButton() { Text = "Bias", Checked = false, Tag = "biasvector" },
            };

            int y = 10;
            int x = 10;

            foreach (var r in GenomeRadios)
            {
                GenomePanel.Controls.Add(r);
                r.AutoSize = true;
                r.Location = new Point(x, y);
                y += 20 + 5;
                r.CheckedChanged += CheckedChanged;
            }
        }

        public void Apply(PlotForm plotForm)
        {
            bool changed = TargetPlotForm != plotForm;

            Disable();
            
            TargetPlotForm = plotForm;
            TargetPlotForm.PlotInteractionsPanel.Controls.Add(GenomePanel);

            if (changed)
                Configure();
        }

        public void Disable()
        {
            if (TargetPlotForm != null)
            {
                if (GenomePanel != null && TargetPlotForm.PlotInteractionsPanel.Controls.Contains(GenomePanel))
                    TargetPlotForm.PlotInteractionsPanel.Controls.Remove(GenomePanel);
                TargetPlotForm = null;
            }
        }

        private async void Configure()
        {
            List<string> args = TargetPlotForm.Args.ToList();

            bool refresh = false;
            
            foreach (var gr in GenomeRadios)
            {
                var thing = "thing=" + (string)gr.Tag;
                if (gr.Checked)
                {
                    if (!args.Contains(thing))
                    {
                        args.Add(thing);
                        refresh = true;
                    }
                }
                else
                {
                    if (args.Contains(thing))
                    {
                        args.Remove(thing);
                        refresh = true;
                    }
                }
            }

            if (refresh)
            {
                TargetPlotForm.Args = args.ToArray();
                await TargetPlotForm.ReloadOffThread();
            }
        }
        
        private void CheckedChanged(object sender, EventArgs e)
        {
            Configure();
        }
    }

    public class TracePlotInteractions : TrajectoryPlotInteractions
    {
        public TracePlotInteractions(string key, string timeType) : base(key, timeType)
        {
            InitGenomePanel();
            InitTracePanel();
        }

        private void InitGenomePanel()
        {
            GenomePanel = new Panel();
            GenomePanel.AutoSize = true;

            GenomeRadios = new RadioButton[] {
                DtmRadio = new RadioButton() { Text = "Dtm", Checked = false, Tag = "dtm" },
                IsRadio = new RadioButton() { Text = "Is", Checked = false, Tag = "is" },
                PsRadio = new RadioButton() { Text = "Ps", Checked = false, Tag = "ps" },
                DevRadio = new RadioButton() { Text = "Dev", Checked = false, Tag = "dev" },
                BiasRadio = new RadioButton() { Text = "Bias", Checked = false, Tag = "biasvector" },
            };

            int y = 10;
            int x = 10;

            foreach (var r in GenomeRadios)
            {
                GenomePanel.Controls.Add(r);
                r.Location = new Point(x, y);
                y += 20 + 5;
                r.CheckedChanged += CheckedChanged;
            }
        }

        private void InitTracePanel()
        {
            TracePanel = new Panel();
            TracePanel.AutoSize = true;

            TraceCheckBoxes = new CheckBox[] {
                RegCoefsCheck = new CheckBox() { Text = "RegCoefs", Checked = false, Tag="regcoefs=l" },
                PTraitsCheck = new CheckBox() { Text = "PTraits", Checked = false, Tag="ptraits=l" },
                GTraitsCheck = new CheckBox() { Text = "GTraits", Checked = false, Tag="gtraits=l" },
                FitnessCheck = new CheckBox() { Text = "Fitness", Checked = true, Tag="fitness=r" },
                HuskynessCheck = new CheckBox() { Text = "Huskyness", Checked = false, Tag="huskyness=l" }
            };

            int y = 10;
            int x = 10;

            foreach (var c in TraceCheckBoxes)
            {
                TracePanel.Controls.Add(c);
                c.Location = new Point(x, y);
                y += 20 + 5;
                c.CheckedChanged += CheckedChanged;
            }
        }

        private void CheckedChanged(object sender, EventArgs e)
        {
            Configure();
        }

        private Panel TracePanel;
        private CheckBox[] TraceCheckBoxes;
        private CheckBox RegCoefsCheck;
        private CheckBox PTraitsCheck;
        private CheckBox GTraitsCheck;
        private CheckBox FitnessCheck;
        private CheckBox HuskynessCheck;

        private Panel GenomePanel;
        private RadioButton[] GenomeRadios;
        private RadioButton DtmRadio;
        private RadioButton IsRadio;
        private RadioButton PsRadio;
        private RadioButton DevRadio;
        private RadioButton BiasRadio;

        private PlotForm TargetPlotForm { get; set; }

        public override void Apply(PlotForm plotForm)
        {
            base.Apply(plotForm);

            bool changed = TargetPlotForm != plotForm;

            if (changed)
                Disable(false);

            TargetPlotForm = plotForm;
            if (plotForm.Args.Any(a => a.StartsWith(TimeType + "=")))
            {
                TargetPlotForm.PlotInteractionsPanel.Controls.Add(GenomePanel);
            }
            else
            {
                TargetPlotForm.PlotInteractionsPanel.Controls.Add(TracePanel);
            }

            if (changed)
                Configure();
        }

        public override void Disable()
        {
            Disable(true);
        }

        private void Disable(bool baseToo)
        {
            if (baseToo)
                base.Disable();

            if (TargetPlotForm != null)
            {
                if (TracePanel != null && TargetPlotForm.PlotInteractionsPanel.Controls.Contains(TracePanel))
                    TargetPlotForm.PlotInteractionsPanel.Controls.Remove(TracePanel);
                if (GenomePanel != null && TargetPlotForm.PlotInteractionsPanel.Controls.Contains(GenomePanel))
                    TargetPlotForm.PlotInteractionsPanel.Controls.Remove(GenomePanel);
                TargetPlotForm = null;
            }
        }

        private async void Configure()
        {
            List<string> args = TargetPlotForm.Args.ToList();

            bool refresh = false;

            if (!FitnessCheck.Checked)
            {
                if (!args.Contains("fitness="))
                {
                    args.Add("fitness=");
                    refresh = true;
                }
            }
            else
            {
                if (args.Contains("fitness="))
                {
                    args.Remove("fitness=");
                    refresh = true;
                }
            }

            if (RegCoefsCheck.Checked)
            {
                if (!args.Contains("regcoefs=l"))
                {
                    args.Add("regcoefs=l");
                    refresh = true;
                }
            }
            else
            {
                if (args.Contains("regcoefs=l"))
                {
                    args.Remove("regcoefs=l");
                    refresh = true;
                }
            }

            if (GTraitsCheck.Checked)
            {
                if (!args.Contains("gtraits=l"))
                {
                    args.Add("gtraits=l");
                    refresh = true;
                }
            }
            else
            {
                if (args.Contains("gtraits=l"))
                {
                    args.Remove("gtraits=l");
                    refresh = true;
                }
            }

            if (PTraitsCheck.Checked)
            {
                if (!args.Contains("ptraits=l"))
                {
                    args.Add("ptraits=l");
                    refresh = true;
                }
            }
            else
            {
                if (args.Contains("ptraits=l"))
                {
                    args.Remove("ptraits=l");
                    refresh = true;
                }
            }

            if (HuskynessCheck.Checked)
            {
                if (!args.Contains("huskyness=l"))
                {
                    args.Add("huskyness=l");
                    refresh = true;
                }
            }
            else
            {
                if (args.Contains("huskyness=l"))
                {
                    args.Remove("huskyness=l");
                    refresh = true;
                }
            }

            foreach (var gr in GenomeRadios)
            {
                var thing = "thing=" + (string)gr.Tag;
                if (gr.Checked)
                {
                    if (!args.Contains(thing))
                    {
                        args.Add(thing);
                        refresh = true;
                    }
                }
                else
                {
                    if (args.Contains(thing))
                    {
                        args.Remove(thing);
                        refresh = true;
                    }
                }
            }

            if (refresh)
            {
                TargetPlotForm.Args = args.ToArray();
                await TargetPlotForm.ReloadOffThread();
            }
        }
    }

    public delegate void TrajectoryClick(int epoch, int lineIndex);

    public class TrajectoryPlotInteractions : IPlotInteractions
    {
        public string Key { get; }
        public string TimeType { get; }
        private PlotView TargetPlotView { get; set; }
        private PlotForm TargetPlotForm { get; set; }
        private PlotForm _subordinatePlotForm = null;
        public TrajectoryClick TrajectoryClick { get; set; }
        public bool UseClickWindow { get; set; } = true;

        public TrajectoryPlotInteractions(string key, string timeType)
        {
            Key = key;
            TimeType = timeType;
        }

        private PlotForm SubordinatePlotForm
        {
            get
            {
                if (_subordinatePlotForm == null)
                {
                    _subordinatePlotForm = new PlotForm();
                    _subordinatePlotForm.Visible = true;
                    _subordinatePlotForm.FormClosed += _subordinatePlotForm_FormClosed;
                }

                return _subordinatePlotForm;
            }
        }

        private void _subordinatePlotForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _subordinatePlotForm = null;
        }

        public virtual void Apply(PlotForm plotForm)
        {
            Disable();

            TargetPlotForm = plotForm;
            TargetPlotView = TargetPlotForm.PlotView;
            foreach (var series in TargetPlotView.Model.Series)
                series.MouseDown += TrajectoryPlotInteractions_MouseDown;
        }

        public virtual void Disable()
        {
            if (TargetPlotView != null)
            {
                foreach (var series in TargetPlotView.Model.Series)
                    series.MouseDown -= TrajectoryPlotInteractions_MouseDown;

                TargetPlotForm = null;
                TargetPlotView = null;
            }
        }

        private async void TrajectoryPlotInteractions_MouseDown(object sender, OxyMouseDownEventArgs e)
        {
            try
            {
                if (e.ChangedButton != OxyMouseButton.Left)
                {
                    return;
                }

                var series = (OxyPlot.Series.LineSeries)sender;
                var screenPoint = e.Position;
                var hitResult = series.GetNearestPoint(screenPoint, false);
                if (hitResult != null)
                {
                    int epoch = (int)hitResult.DataPoint.X;
                    int index = series.Tag as int? ?? 0;

                    TrajectoryClick?.Invoke(epoch, index);

                    if (UseClickWindow)
                    {
                        int n = (int)Math.Sqrt(TargetPlotView.Model.Series.Count);
                        int j = index % n;
                        int i = index / n;

                        foreach (var s in TargetPlotView.Model.Series)
                            s.ClearSelection();
                        series.Select();

                        TargetPlotView.InvalidatePlot(false);

                        var clips = new CliParams();
                        clips.Consume(TargetPlotForm.Args);
                        Cli.LoadParamFiles(clips);
                        clips.Consume(SubordinatePlotForm.Args);
                        Cli.LoadParamFiles(clips);

                        clips.Set($"{TimeType}", $"{epoch}");
                        clips.Set($"title", $"{index}{TimeType}{epoch}");

                        SubordinatePlotForm.DataFileName = TargetPlotForm.DataFileName;
                        SubordinatePlotForm.Visible = true;
                        SubordinatePlotForm.Args = clips.ToStringArray();
                        SubordinatePlotForm.DataFileName = TargetPlotForm.DataFileName;
                        e.Handled = true;
                        SubordinatePlotForm.PlotView.Model = null;
                        await SubordinatePlotForm.ReloadOffThread();
                        SubordinatePlotForm.PlotView.Model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = j });
                        SubordinatePlotForm.PlotView.Model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Horizontal, Y = i });
                        SubordinatePlotForm.PlotView.InvalidatePlot(false);
                    }
                }
            }
            catch
            {
            }
        }
    }
}
