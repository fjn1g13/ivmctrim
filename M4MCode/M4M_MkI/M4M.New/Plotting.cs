﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using OxyPlot.Annotations;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public class FigureLabelAnnotation : OxyPlot.Annotations.Annotation
    {
        public FigureLabelAnnotation(OxyPlot.PlotModel plot, string text, OxyColor color)
        {
            Plot = plot;
            Text = text;
            Color = color;
            Layer = OxyPlot.Annotations.AnnotationLayer.AboveSeries;
        }

        private OxyPlot.PlotModel _plot;
        public OxyPlot.PlotModel Plot
        {
            get => _plot;
            set
            {
                if (_plot != null && _plot.Annotations.Contains(this))
                    _plot.Annotations.Remove(this);
                if (value.Annotations.Contains(this))
                    value.Annotations.Remove(this);
                _plot = value;
                _plot.Annotations.Add(this);
            }
        }

        public string Text { get; set; }
        public OxyColor Color { get; set; }

        public override void Render(IRenderContext rc)
        {
            var fontSize = !double.IsNaN(this.FontSize) ? this.FontSize : this.PlotModel.TitleFontSize;
            var font = this.Font ?? Plot.DefaultFont;
            rc.DrawText(new ScreenPoint(0, 0), Text, Color, font, fontSize, Plot.TitleFontWeight, 0, HorizontalAlignment.Left, VerticalAlignment.Top);
        }
    }
    public class WaterMarkAnnotation : OxyPlot.Annotations.Annotation
    {
        public WaterMarkAnnotation(OxyPlot.PlotModel plot, string text, OxyColor color)
        {
            Plot = plot;
            Text = text;
            Color = color;
            Layer = OxyPlot.Annotations.AnnotationLayer.BelowAxes;
        }

        private OxyPlot.PlotModel _plot;
        public OxyPlot.PlotModel Plot
        {
            get => _plot;
            set
            {
                if (_plot != null && _plot.Annotations.Contains(this))
                    _plot.Annotations.Remove(this);
                if (value.Annotations.Contains(this))
                    value.Annotations.Remove(this);
                _plot = value;
                _plot.Annotations.Add(this);
            }
        }

        public string Text { get; set; }
        public OxyColor Color { get; set; }
        
        public override void Render(IRenderContext rc)
        {
            var fontSize = !double.IsNaN(this.FontSize) ? this.FontSize : this.PlotModel.TitleFontSize * 2;
            rc.DrawText(this.PlotModel.PlotArea.Center, Text, Color, Plot.DefaultFont, fontSize, Plot.TitleFontWeight, 0, HorizontalAlignment.Center, VerticalAlignment.Middle);
        }
    }

    public class SideAnnotation : Annotation
    {
        public SideAnnotation(AxisPosition position, string text)
        {
            if (!(position == AxisPosition.Left || position == AxisPosition.Right || position == AxisPosition.Top || position == AxisPosition.Bottom))
                throw new ArgumentException("Must be l/r/t/b", nameof(position));

            this.Position = position;
            this.Text = text;
            this.TextColor = OxyColors.Automatic;
            this.Layer = AnnotationLayer.AboveSeries;
        }

        public AxisPosition Position { get; }
        public string Text { get; }

        public override void Render(IRenderContext rc)
        {
            if (string.IsNullOrEmpty(this.Text))
                return;

            var actualTextColor = this.ActualTextColor;
            var actualFont = this.ActualFont;
            var actualFontSize = this.ActualFontSize;
            var actualFontWeight = this.ActualFontWeight;

            var m = rc.MeasureText(Text, actualFont, actualFontSize, actualFontWeight);

            ScreenPoint p;
            double angle;

            var s = 1.0;

            switch (Position)
            {
                case AxisPosition.Left:
                    angle = -90;
                    p = new ScreenPoint(this.PlotModel.PlotArea.Left - m.Height * s, this.PlotModel.PlotArea.Center.Y);
                    break;
                case AxisPosition.Right:
                    angle = 90;
                    p = new ScreenPoint(this.PlotModel.PlotArea.Right + m.Height * s, this.PlotModel.PlotArea.Center.Y);
                    break;
                case AxisPosition.Top:
                    angle = 0;
                    p = new ScreenPoint(this.PlotModel.PlotArea.Center.X, this.PlotModel.PlotArea.Top - m.Height * s);
                    break;
                case AxisPosition.Bottom:
                    angle = 0;
                    p = new ScreenPoint(this.PlotModel.PlotArea.Center.X, this.PlotModel.PlotArea.Bottom + m.Height * s);
                    break;
                default:
                    throw new InvalidOperationException();
            }

            rc.DrawText(p, this.Text, actualTextColor, actualFont, actualFontSize, actualFontWeight, angle, OxyPlot.HorizontalAlignment.Center, OxyPlot.VerticalAlignment.Middle);
        }
    }

    public class ViewBackgroundAnnotation : OxyPlot.Annotations.Annotation
    {
        public ViewBackgroundAnnotation(OxyPlot.PlotModel plot, OxyColor color)
        {
            Plot = plot;
            Color = color;
            Layer = OxyPlot.Annotations.AnnotationLayer.BelowAxes;
        }

        private OxyPlot.PlotModel _plot;
        public OxyPlot.PlotModel Plot
        {
            get => _plot;
            set
            {
                if (value.Annotations.Contains(this))
                    value.Annotations.Remove(this);
                _plot = value;
                _plot.Annotations.Add(this);
            }
        }

        public OxyPlot.OxyColor Color { get; set; }

        public override void Render(IRenderContext rc)
        {
            rc.DrawRectangle(new OxyRect(0, 0, Plot.Width, Plot.Height), Color, OxyColors.Transparent);
        }
    }

    public class RepeatingLineAnnotation : OxyPlot.Annotations.Annotation
    {
        public RepeatingLineAnnotation(OxyPlot.PlotModel plot, OxyColor color, double interval)
        {
            Plot = plot;
            Color = color;
            Interval = interval;
            Layer = OxyPlot.Annotations.AnnotationLayer.AboveSeries;
        }

        private OxyPlot.PlotModel _plot;
        public OxyPlot.PlotModel Plot
        {
            get => _plot;
            set
            {
                if (value.Annotations.Contains(this))
                    value.Annotations.Remove(this);
                _plot = value;
                _plot.Annotations.Add(this);
            }
        }

        public OxyPlot.OxyColor Color { get; set; }
        public double Interval { get; }

        public override void Render(IRenderContext rc)
        {
            var amin = XAxis.ActualMinimum;
            var amax = XAxis.ActualMaximum;
            var min = (int)(amin / Interval);
            var max = (int)(amax / Interval);

            if (min > max)
            {
                var t = min;
                min = max;
                max = t;
            }

            if (min >= 0)
                min++;
            if (max <= 0)
                max--;

            var y0 = YAxis.ActualMinimum;
            var y1 = YAxis.ActualMaximum;

            var pen = new OxyPen(Color);

            for (int i = min; i <= max; i++)
            {
                var p0 = XAxis.Transform(i * Interval, y0, YAxis);
                var p1 = XAxis.Transform(i * Interval, y1, YAxis);
                rc.DrawLine(p0.X, p0.Y, p1.X, p1.Y, pen);
            }
        }
    }

    public class PlotExportReport
    {
        public PlotExportReport(string filename, double exportedWidth, double exportedHeight)
        {
            Filename = filename;
            ExportedWidth = exportedWidth;
            ExportedHeight = exportedHeight;
        }

        public string Filename { get; }
        public double ExportedWidth { get; }
        public double ExportedHeight { get; }

        public string Summary => $"Output File: {Filename}, Width: {ExportedWidth}, Height: {ExportedHeight}";
    }
    
    public delegate void PostProcessPlot(PlotModel model);
    
    public interface IPlotExporter
    {
        /// <summary>
        /// Adds the extention to the filename
        /// </summary>
        PlotExportReport ExportPlot(string filename, PlotModel model, double size = 1.0, bool forceAspect = false, PostProcessPlot postProcessor = null);

        /// <summary>
        /// Adds the extention to the filename
        /// </summary>
        PlotExportReport ExportPlot(string filename, PlotModel model, double width, double height, bool forceAspect = false, PostProcessPlot postProcessor = null);
    }

    public class SimplePdfPlotExporter : IPlotExporter
    {
        public PlotExportReport ExportPlot(string filename, PlotModel model, double size, bool forceAspect, PostProcessPlot postProcessor = null)
        {
            double width = 297 / 25.4 * 72 * size; // A4
            double height = 210 / 25.4 * 72 * size;
            
            return ExportToPdf(model, filename + ".pdf", width, height, true, forceAspect, postProcessor );
        }

        public PlotExportReport ExportPlot(string filename, PlotModel model, double width, double height, bool forceAspect, PostProcessPlot postProcessor = null)
        {
            return ExportToPdf(model, filename + ".pdf", width, height, true, forceAspect, postProcessor);
        }

        public static PlotExportReport ExportToPdf(OxyPlot.PlotModel model, string ofname, double width, double height, bool bigger, bool forceAspect, PostProcessPlot postProcessor)
        {
            double fc = 1.0;
            double prl = 2.0;
            double prc = 2.0;
            if (model.Axes.Any(a => a.Position == AxisPosition.Right))
                prc = 0.0;
            if (model.Axes.Any(a => a.Position == AxisPosition.Left))
                prl = 0.0;

            model.Padding = new OxyPlot.OxyThickness(model.Padding.Right * prl, 0.0, model.Padding.Right * prc, 0.0);
            model.TitleFontSize *= fc;
            model.DefaultFontSize *= fc;

            if (bigger)
            {
                foreach (var s in model.Series)
                {
                    if (s is OxyPlot.Series.LineSeries)
                        ((OxyPlot.Series.LineSeries)s).StrokeThickness *= 2.0;
                }
                model.LegendFontSize *= fc;
            }

            OxyPlot.PdfExporter exporter = new PdfExporter();
            exporter.Width = width;
            exporter.Height = height;
            
            void doPlot()
            {
                model.ResetAllAxes();
                model.InvalidatePlot(true);
                
                using (var fs = new System.IO.FileStream(ofname, System.IO.FileMode.Create))
                {
                    exporter.Export(model, fs);
                }
            }

            void doForceAspect()
            {
                if (model.PlotArea.Width > model.PlotArea.Height)
                {
                    width = width - model.PlotArea.Width + model.PlotArea.Height;
                }
                else
                {
                    height = height + model.PlotArea.Width - model.PlotArea.Height;
                }

                exporter.Width = width;
                exporter.Height = height;
            }

            doPlot();

            if (forceAspect)
            {
                doForceAspect();
                doPlot();
            }

            if (postProcessor != null)
            {
                postProcessor(model);
                doPlot();
                
                if (forceAspect)
                {
                    doForceAspect();
                    doPlot();
                }
            }
            
            return new PlotExportReport(ofname, width, height);
        }
    }

    public enum TrajectoryPlotType
    {
        Line = 0,
        Scatter = 1,
        Rectangles = 2,
    }

    public class TrajectoryPlotting
    {
        public static PlotModel PrepareTrajectoriesPlot(string title, string xlabel, string ylabel, string colorlabel, double? ymin, double? ymax, string xAxisKey, string yAxisKey, string colorAxisKey, bool logX, bool logY, bool reverseY)
        {
            PlotModel model = new PlotModel() { Title = title };

            if (xAxisKey != null)
            {
                Axis xaxis = logX
                    ? (Axis)new LogarithmicAxis() { Key = xAxisKey, Title = xlabel, Position = AxisPosition.Bottom }
                    : (Axis)new LinearAxis() { Key = xAxisKey, Title = xlabel, Position = AxisPosition.Bottom };

                model.Axes.Add(xaxis);
            }

            if (yAxisKey != null)
            {
                Axis yaxis = logY
                    ? (Axis)new LogarithmicAxis() { Key = yAxisKey, Title = ylabel, Position = AxisPosition.Left }
                    : (Axis)new LinearAxis() { Key = yAxisKey, Title = ylabel, Position = AxisPosition.Left };

                if (reverseY)
                {
                    yaxis.StartPosition = 1;
                    yaxis.EndPosition = 0;
                }
            
                if (ymin != null)
                    yaxis.Minimum = ymin.Value;
                if (ymax != null)
                    yaxis.Maximum = ymax.Value;
            
                model.Axes.Add(yaxis);
            }    

            if (colorAxisKey != null)
            {
                LinearColorAxis coloraxis = new LinearColorAxis() { Key = colorAxisKey, Title = colorlabel, Position = AxisPosition.Right };
                coloraxis.Palette = OxyPalettes.Gray(100);
                model.Axes.Add(coloraxis);
            }

            return model;
        }

        public static void AddY2Axis(PlotModel model, string y2label, string y2AxisKey, double? y2min, double? y2max)
        {
            Axis y2axis = new LinearAxis() { Key = y2AxisKey, Title = y2label, Position = AxisPosition.Right };
            
            if (y2min != null)
                y2axis.Minimum = y2min.Value;
            if (y2max != null)
                y2axis.Maximum = y2max.Value;
            
            model.Axes.Add(y2axis);
        }

        /// <summary>
        /// This very intentionally igores null entries, so that the colours are as they would be if they were not null
        /// </summary>
        public static PlotModel PlotTrajectories(string title, string xlabel, string ylabel, string colorlabel, string seriesName, double[][] trajectories, int samplePeriod, double x0, double? ymin, double? ymax, OxyColor? c0, OxyColor? c1, double strokeThickness, int resolution, TrajectoryPlotType plotType, string xAxisKey, string yAxisKey, string colorAxisKey, bool logX, bool logY, bool reverseY)
        {
            PlotModel model = PrepareTrajectoriesPlot(title, xlabel, ylabel, colorlabel, ymin, ymax, xAxisKey, yAxisKey, colorAxisKey, logX, logY, reverseY);

            PlotTrajectories(model, seriesName, trajectories, samplePeriod, x0, c0, c1, strokeThickness, resolution, plotType, xAxisKey, yAxisKey);

            return model;
        }

        /// <summary>
        /// This very intentionally igores null entries, so that the colours are as they would be if they were not null
        /// </summary>
        public static PlotModel PlotTrajectories(string title, string xlabel, string ylabel, string colorlabel, string seriesName, double[][] trajectories, int samplePeriod, double x0, double? ymin, double? ymax, object[] tags, OxyColor[] colors, double strokeThickness, int resolution, TrajectoryPlotType plotType, string xAxisKey, string yAxisKey, string colorAxisKey, bool logX, bool logY, bool reverseY)
        {
            PlotModel model = PrepareTrajectoriesPlot(title, xlabel, ylabel, colorlabel, ymin, ymax, xAxisKey, yAxisKey, colorAxisKey, logX, logY, reverseY);

            PlotTrajectories(model, seriesName, trajectories, samplePeriod, x0, tags, colors, strokeThickness, resolution, plotType, xAxisKey, yAxisKey, colorAxisKey);

            return model;
        }
        
        /// <summary>
        /// This very intentionally igores null entries, so that the colours are as they would be if they were not null
        /// </summary>
        public static void PlotTrajectories(PlotModel model, string seriesName, double[][] trajectories, int samplePeriod, double x0, OxyColor? c0, OxyColor? c1, double strokeThickness, int resolution, TrajectoryPlotType plotType, string xAxisKey, string yAxisKey)
        {
            int n = trajectories.Length;
            var tags = Tags1D(n, i => i);
            var colors = Colours1D(trajectories.Length, c0, c1);
            PlotTrajectories(model, seriesName, trajectories, samplePeriod, x0, tags, colors, strokeThickness, resolution, plotType, xAxisKey, yAxisKey, null);
        }
        
        
        /// <summary>
        /// This very intentionally igores null entries, so that the colours are as they would be if they were not null
        /// </summary>
        public static void PlotTrajectories(PlotModel model, string seriesName, double[][] trajectories, int samplePeriod, double x0, object[] tags, OxyColor[] colors, double strokeThickness, int resolution, TrajectoryPlotType plotType, string xAxisKey, string yAxisKey, string colorAxisKey)
        {    
            int N = trajectories.Length;

            if (plotType == TrajectoryPlotType.Rectangles)
            {
                var rs = new OxyPlot.Series.RectangleSeries();
                rs.XAxisKey = xAxisKey;
                rs.YAxisKey = yAxisKey;
                rs.ColorAxisKey = colorAxisKey;
                rs.Title = seriesName;
                rs.CanTrackerInterpolatePoints = false;

                for (int i = 0; i < N; i++)
                {
                    double x = x0 - samplePeriod * resolution;
                    var samples = trajectories[i];
                    var lastX = x0;
                    var last = double.NaN;
                    foreach (var sample in samples)
                    {
                        if (!double.IsNaN(last) && !double.IsNaN(sample))
                        {
                            x += samplePeriod * resolution;
                            var rect = new RectangleItem(lastX, x, i, i+1, sample);
                            rs.Items.Add(rect);
                        }

                        last = sample;
                        lastX = x;
                    }
                }

                rs.RenderInLegend = false;

                model.Series.Add(rs);
            }
            else
            {
                bool first = true;
                int li = 0;
                foreach (var line in trajectories)
                {
                    if (line == null)
                        goto skip;

                    OxyColor color = colors != null ? colors[li] : OxyColors.Automatic;

                    Series s;
                    Action<DataPoint> addDataPoint;

                    switch (plotType)
                    {
                        case TrajectoryPlotType.Line:
                            LineSeries ls = new LineSeries() { LineStyle = OxyPlot.LineStyle.Solid, MarkerType = OxyPlot.MarkerType.None, StrokeThickness = strokeThickness, Color = color, Tag = tags[li] };
                            addDataPoint = dp => ls.Points.Add(dp);
                            ls.XAxisKey = xAxisKey;
                            ls.YAxisKey = yAxisKey;
                            s = ls;
                            break;
                        case TrajectoryPlotType.Scatter:
                            ScatterSeries ss = new ScatterSeries() { MarkerType = OxyPlot.MarkerType.Diamond, MarkerFill = color, MarkerSize = strokeThickness, Tag = tags[li] };
                            addDataPoint = dp => ss.Points.Add(new ScatterPoint(dp.X, dp.Y));
                            ss.XAxisKey = xAxisKey;
                            ss.YAxisKey = yAxisKey;
                            s = ss;
                            break;
                        default:
                            throw new Exception("Unsupported TrajectoryPlottingType: " + plotType);
                    }

                    if (first)
                    {
                        s.Title = seriesName;
                        s.TrackerFormatString += $"\n(li)";
                        first = false;
                    }

                    double x = x0;

                    for (int i = 0; i < line.Length; i += resolution)
                    {
                        addDataPoint(new DataPoint(x, line[i]));
                        x += samplePeriod * resolution;
                    }

                    model.Series.Add(s);

                skip:
                    li++;
                }
            }
        }
        
        public static OxyColor[] Colours1D(int n, OxyColor? c0, OxyColor? c1)
        {
            OxyColor[] res = new OxyColor[n];
            
            if (c0 == null || c1 == null)
            {
                if (n == 1 && c0 != null)
                {
                    res[0] = c0.Value;
                }
                else
                {
                    OxyColor c = c0 == null ? (c1 == null ? OxyColors.Automatic : c1.Value) : c0.Value;
                    for (int i = 0; i < n; i++)
                    {
                        res[i] = c;
                    }
                }
            }
            else if (n == 1)
            {
                res[0] = c0.Value;
            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    var z = (double)i / (n - 1);
                    res[i] = OxyColor.Interpolate(c0.Value, c1.Value, z);
                }
            }

            return res;
        }

        public static OxyColor[] Colours2D(int n, OxyColor c00, OxyColor c10, OxyColor c01, OxyColor c11)
        {
            OxyColor[] res = new OxyColor[n * n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    var iz = (double)i / Math.Max(1, (n - 1));
                    var jz = (double)j / Math.Max(1, (n - 1));
                    res[i * n + j] = OxyColor.Interpolate(OxyColor.Interpolate(c00, c10, iz), OxyColor.Interpolate(c01, c11, iz), jz);
                }
            }

            return res;
        }

        public static object[] Tags1D(int n, Func<int, object> tagFunc1d)
        {
            object[] res = new object[n];

            for (int i = 0; i < n; i++)
            {
                res[i] = tagFunc1d(i);
            }

            return res;
        }

        public static object[] Tags2D(int n, Func<int, int, object> tagFunc2d)
        {
            object[] res = new object[n * n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    res[i * n + j] = tagFunc2d(i, j);
                }
            }

            return res;
        }
    }

    public struct MaxMin
    {
        public readonly double Max;
        public readonly double Min;

        public MaxMin(double max, double min)
        {
            Max = max;
            Min = min;
        }
    }

    public class TracePlotting
    {
        public static PlotModel PreparePlot(string title, bool showLegend, bool epochsx, string xAxisKey, bool logX)
        {
            PlotModel model = new PlotModel() { Title = title };
            
            Axis xaxis;
            if (logX)
            {
                xaxis = new LogarithmicAxis() { Key = xAxisKey, Title = epochsx ? "Epochs" : "Generations", Position = AxisPosition.Bottom };
            }
            else
            {
                xaxis = new LinearAxis() { Key = xAxisKey, Title = epochsx ? "Epochs" : "Generations", Position = AxisPosition.Bottom };
            }
            model.Axes.Add(xaxis);

            model.IsLegendVisible = showLegend;
            model.LegendPosition = LegendPosition.RightTop;
            model.LegendPlacement = LegendPlacement.Outside;

            return model;
        }

        public static IEnumerable<WholeSample<T>> EnumerateSamples<T>(int offset, TraceInfo<T> traceInfo, int resolution) where T : IIndividual<T>
        {
            for (int i = offset; i < offset + resolution && i < traceInfo.TraceWholeSamples.Count; i++)
            {
                yield return traceInfo.TraceWholeSamples[i];
            }
        }

        public static Func<IEnumerable<T>, double> AverageMetric<T>(Func<T, double> sampler)
        {
            return s => s.Select(sampler).Average();
        }

        public static Func<IEnumerable<T>, double> DiffMetric<T>(Func<IEnumerable<T>, double> metric, double d0)
        {
            return s => metric(s) - d0;
        }

        public static Func<IEnumerable<WholeSample<T>>, double> AverageMetric<T>(TraceInfo<T> traceInfo, bool diff, Func<WholeSample<T>, double> sampler) where T : IIndividual<T>
        {
            var sm = AverageMetric(sampler);
            if (diff)
                sm = DiffMetric(sm, sampler(traceInfo.TraceWholeSamples.First()));
            
            return sm;
        }

        public static IEnumerable<DataPoint> Sample<T>(TraceInfo<T> traceInfo, int resolution, Func<IEnumerable<WholeSample<T>>, double> samplerMetric, bool epochsx, double offset) where T : IIndividual<T>
        {
            if (epochsx)
            {
                // exclude the last set if it is deficient
                for (int i = 0; i + resolution - 1 < traceInfo.TraceWholeSamples.Count; i += resolution)
                {
                    var x = traceInfo.TraceWholeSamples[i].Epoch + offset;
                    var y = samplerMetric(EnumerateSamples(i, traceInfo, resolution));
                    yield return new DataPoint(x, y);
                }
            }
            else
            {
                // exclude the last set if it is deficient
                for (int i = 0; i + resolution - 1 < traceInfo.TraceWholeSamples.Count; i += resolution)
                {
                    var x = traceInfo.TraceWholeSamples[i].Generations + offset;
                    var y = samplerMetric(EnumerateSamples(i, traceInfo, resolution));
                    yield return new DataPoint(x, y);
                }
            }
        }

        public static IEnumerable<DataPoint> InjectBreaks(IEnumerable<DataPoint> points)
        {
            var lastX = double.NaN;

            foreach (var dp in points)
            {
                if (dp.X == lastX)
                {
                    yield return DataPoint.Undefined;
                }

                lastX = dp.X;
                yield return dp;
            }
        }

        public static IEnumerable<DataPoint> MaybeInjectBreaks(IEnumerable<DataPoint> points, bool injectBreaks)
        {
            if (injectBreaks)
                return InjectBreaks(points);
            else
                return points;
        }

        public static void PlotTraceRegCoegs(PlotModel model, TraceInfo<DenseIndividual> traceInfo, int resolution, double? regcoefsmin, double? regcoefsmax, bool onLeft, bool epochsx, bool diff, double offset)
        {
            Axis regcoefsaxis = new LinearAxis() { Key = "regcoefs", Title = (diff ? "Change in " : "") + "Regulation Coefficient", Position = onLeft ? AxisPosition.Left : AxisPosition.Right };
            model.Axes.Add(regcoefsaxis);

            if (regcoefsmin != null)
                regcoefsaxis.Minimum = regcoefsmin.Value;
            if (regcoefsmax != null)
                regcoefsaxis.Maximum = regcoefsmax.Value;

            var exampleIndividual = traceInfo.TraceWholeSamples[0].Judgements[0].Individual;
            
            var c00 = OxyColors.DarkCyan;
            var c10 = OxyColors.LightCyan;
            var c01 = OxyColors.DarkGreen;
            var c11 = OxyColors.LightGreen;

            int N = exampleIndividual.Phenotype.Size;

            bool first = true;
            int li = 0;
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    var iz = (double)i / (N - 1);
                    var jz = (double)j / (N - 1);
                    var c = OxyColor.Interpolate(OxyColor.Interpolate(c00, c10, iz), OxyColor.Interpolate(c01, c11, iz), jz);
                    
                    LineSeries traitLine = new LineSeries() { Title = "Regulation Coefficients", RenderInLegend = first, Color = c, YAxisKey = regcoefsaxis.Key, Tag = li };
                    li++;

                    Func<WholeSample<DenseIndividual>, double> sampler = ws => ws.Judgements.Average(ij => ij.Individual.Genome.TransMat[i, j]);
                    double w0 = sampler(traceInfo.TraceWholeSamples[0]);

                    if (diff)
                        traitLine.Points.AddRange(Sample(traceInfo, resolution, wss => wss.Average(sampler) - w0, epochsx, offset));
                    else
                        traitLine.Points.AddRange(Sample(traceInfo, resolution, wss => wss.Average(sampler), epochsx, offset));

                    model.Series.Add(traitLine);

                    first = false;
                }
            }
        }

        public static Series PlotTraceLine<T>(PlotModel model, TraceInfo<T> traceInfo, string xAxisKey, string yAxisKey, string label, int resolution, bool epochsx, Func<IEnumerable<WholeSample<T>>, double> samplerMetric, OxyColor colour, double strokeThickness, double offset, TrajectoryPlotType plotType, MarkerType markerType, OxyColor markerColour, LineStyle lineStyle, bool injectBreaks, LineStyle brokenLineStyle) where T : IIndividual<T>
        {
            switch (plotType)
            {
                case TrajectoryPlotType.Line:
                    LineSeries ls = new LineSeries() { Title = label, Color = colour, XAxisKey = xAxisKey, YAxisKey = yAxisKey, StrokeThickness = strokeThickness, MarkerSize = strokeThickness, BrokenLineStyle = brokenLineStyle, BrokenLineColor = colour, BrokenLineThickness = strokeThickness };
                    ls.Points.AddRange(MaybeInjectBreaks(Sample(traceInfo, resolution, samplerMetric, epochsx, offset), injectBreaks));
                    GeneralPlotting.ApplyMarkers(ls, markerType, markerColour);
                    model.Series.Add(ls);
                    ls.LineStyle = lineStyle;
                    return ls;
                case TrajectoryPlotType.Scatter:
                    ScatterSeries ss = new ScatterSeries() { Title = label, MarkerFill = colour, XAxisKey = xAxisKey, YAxisKey = yAxisKey, MarkerSize = strokeThickness, MarkerStrokeThickness = 1, MarkerType = OxyPlot.MarkerType.Diamond };
                    ss.Points.AddRange(Sample(traceInfo, resolution, samplerMetric, epochsx, offset).Select(dp => new ScatterPoint(dp.X, dp.Y)));
                    GeneralPlotting.ApplyMarkers(ss, markerType, markerColour);
                    model.Series.Add(ss);
                    return ss;
                default:
                throw new Exception("Unsupported plot type: " + plotType);
            }
        }

        public static void PlotTraceLines1D<T>(PlotModel model, TraceInfo<T> traceInfo, string axiskey, string title, string legendTitle, int resolution, double? min, double? max, bool onLeft, bool epochsx, IReadOnlyList<Func<IEnumerable<WholeSample<T>>, double>> samplerMetrics, OxyColor c0, OxyColor c1, double strokeThickness, double offset, TrajectoryPlotType plotType, MarkerType markerType, LineStyle lineStyle, bool injectBreaks, LineStyle brokenLineStyle) where T : IIndividual<T>
        {
            Axis axis = new LinearAxis() { Key = axiskey, Title = title, Position = onLeft ? AxisPosition.Left : AxisPosition.Right };
            model.Axes.Add(axis);

            if (min != null)
                axis.Minimum = min.Value;
            if (max != null)
                axis.Maximum = max.Value;
            
            bool first = true;
            for (int i = 0; i < samplerMetrics.Count; i++)
            {
                var z = samplerMetrics.Count == 1 ? 0.0 : (double)i / (samplerMetrics.Count-1);
                var c = OxyColor.Interpolate(c0, c1, z);

                var s = PlotTraceLine(model, traceInfo, "generations", axis.Key, legendTitle, resolution, epochsx, samplerMetrics[i], c, strokeThickness, offset, plotType, markerType, c, lineStyle, injectBreaks, brokenLineStyle);
                s.RenderInLegend = first;
                //model.Series.Add(s);

                first = false;
            }
        }

        public static void PlotTraceLinesAverage1D<T>(PlotModel model, TraceInfo<T> traceInfo, string axiskey, string title, string legendTitle, int resolution, double? min, double? max, bool onLeft, bool epochsx, bool diff, IReadOnlyList<Func<WholeSample<T>, double>> samplers, OxyColor c0, OxyColor c1, double strokeThickness, double offset, TrajectoryPlotType plotType, MarkerType markerType, LineStyle lineStyle, bool injectBreaks, LineStyle brokenLineStyle) where T : IIndividual<T>
        {
            var samplerMetrics = samplers.Select(s => AverageMetric(traceInfo, diff, s)).ToArray();
            PlotTraceLines1D(model, traceInfo, axiskey, (diff ? "Change in " : "") + title, legendTitle, resolution, min, max, onLeft, epochsx, samplerMetrics, c0, c1, strokeThickness, offset, plotType, markerType, lineStyle, injectBreaks, brokenLineStyle);
        }

        public static void PlotPhenotypicTraceTraits<T>(PlotModel model, TraceInfo<T> traceInfo, int resolution, double? traitmin, double? traitmax, bool onLeft, bool epochsx, bool diff, double offset, TrajectoryPlotType plotType, MarkerType markerType, bool injectBreaks, LineStyle brokenLineStyle) where T : IIndividual<T>
        {
            var exampleIndividual = traceInfo.TraceWholeSamples[0].Judgements[0].Individual;

            var c0 = OxyColors.DarkBlue;
            var c1 = OxyColors.LightBlue;

            int N = exampleIndividual.Phenotype.Size;
            
            Func<WholeSample<T>, double>[] samplers =
                Enumerable.Range(0, N).Select<int, Func<WholeSample<T>, double>>(i => ws => ws.Judgements.Average(j => j.Individual.Phenotype[i])).ToArray();

            PlotTraceLinesAverage1D(model, traceInfo, "ptrait", "Phenotypic Trait Expression", "Phenotypic Trait Expression", resolution, traitmin, traitmax, onLeft, epochsx, diff, samplers, c0, c1, 2.0, offset, plotType, markerType, LineStyle.Automatic, injectBreaks, brokenLineStyle);
        }

        public static void PlotGenotypicTraceTraits(PlotModel model, TraceInfo<DenseIndividual> traceInfo, int resolution, double? traitmin, double? traitmax, bool onLeft, bool epochsx, bool diff, double offset, TrajectoryPlotType plotType, MarkerType markerType, bool injectBreaks, LineStyle brokenLineStyle)
        {
            var exampleIndividual = traceInfo.TraceWholeSamples[0].Judgements[0].Individual;

            var c0 = OxyColors.DarkViolet;
            var c1 = OxyColors.Violet;

            int N = exampleIndividual.Genome.Size;

            var g0 = Enumerable.Range(0, N).Select(i => traceInfo.TraceWholeSamples[0].Judgements.Average(j => j.Individual.Genome.InitialState[i])).ToArray();

            Func<WholeSample<DenseIndividual>, double>[] samplers =
                Enumerable.Range(0, N).Select<int, Func<WholeSample<DenseIndividual>, double>>(i => ws => ws.Judgements.Average(j => j.Individual.Genome.InitialState[i])).ToArray();

            PlotTraceLinesAverage1D(model, traceInfo, "gtrait", (diff ? "Change in " : "") + "Genotypic Trait Expression", "Genotypic Trait Expression", resolution, traitmin, traitmax, onLeft, epochsx, diff, samplers, c0, c1, 2.0, offset, plotType, markerType, LineStyle.Automatic, injectBreaks, brokenLineStyle);
        }

        public static void PlotTraceHuskyness(PlotModel model, TraceInfo<DenseIndividual> traceInfo, int[][] moduleTraits, int resolution, double? huskynessmin, double? huskynessmax, bool onLeft, bool epochsx, bool diff, double offset, LineStyle lineStyle, TrajectoryPlotType plotType, MarkerType markerType, bool injectBreaks, LineStyle brokenLineStyle)
        {
            var c0 = OxyColors.DarkSlateGray;
            var c1 = OxyColors.SlateGray;

            Func<WholeSample<DenseIndividual>, double>[] samplers =
                moduleTraits.Select<int[], Func<WholeSample<DenseIndividual>, double>>(mts => ws => ws.Judgements.Average(j => Analysis.ComputeModuleHuskyness(j.Individual.Genome.TransMat, mts))).ToArray();

            PlotTraceLinesAverage1D(model, traceInfo, "huskyness", (diff ? "Change in " : "") + "Degree of Hierarchy", "Degree of Hierarchy", resolution, huskynessmin, huskynessmax, onLeft, epochsx, diff, samplers, c0, c1, 2.0, offset, plotType, markerType, LineStyle.Automatic, injectBreaks, brokenLineStyle);
        }

        public static void PlotTraceDevTimeDominance(PlotModel model, MathNet.Numerics.Random.RandomSource rand, DevelopmentRules drules, TraceInfo<DenseIndividual> traceInfo, int[][] moduleTraits, int resolution, double? dtdmin, double? dtdmax, bool onLeft, bool epochsx, bool diff, double offset, LineStyle lineStyle, TrajectoryPlotType plotType, MarkerType markerType, bool injectBreaks, LineStyle brokenLineStyle)
        {
            var c0 = OxyColors.DarkGoldenrod;
            var c1 = OxyColors.DarkGoldenrod;

            Func<WholeSample<DenseIndividual>, double>[] samplers = new[] {
                new Func<WholeSample<DenseIndividual>, double>(ws => ws.Judgements.Average(j => Analysis.MeasureDevTimeDominance(rand, drules, j.Individual.Genome, moduleTraits)))
            };

            PlotTraceLinesAverage1D(model, traceInfo, "dtd", (diff ? "Change in " : "") + "Dev-Time Dominance", "Dev-Time Dominance", resolution, dtdmin, dtdmax, onLeft, epochsx, diff, samplers, c0, c1, 2.0, offset, plotType, markerType, LineStyle.Automatic, injectBreaks, brokenLineStyle);
        }

        public static void PlotTraceFitness<T>(PlotModel model, TraceInfo<T> traceInfo, int resolution, double? fitnessmin, double? fitnessmax, bool onLeft, bool epochsx, bool diff, double offset, LineStyle lineStyle, TrajectoryPlotType plotType, MarkerType markerType) where T : IIndividual<T>
        {
            Axis fitnessaxis = new LinearAxis() { Key = "fitness", Title = (diff ? "Change in " : "") + "Fitness", Position = onLeft ? AxisPosition.Left : AxisPosition.Right };
            model.Axes.Add(fitnessaxis);

            if (fitnessmin != null)
                fitnessaxis.Minimum = fitnessmin.Value;
            if (fitnessmax != null)
                fitnessaxis.Maximum = fitnessmax.Value;
            
            double f0 = traceInfo.TraceWholeSamples[0].Judgements.Average(j => j.Judgement.CombinedFitness);
            
            // TODO: would be nice to be able to use a Scatter here, so we should abstract this out so that all the PlotTraceeWhatever methods can use it
            Func<IEnumerable<WholeSample<T>>, double> sampler = diff
                ? new Func<IEnumerable<WholeSample<T>>, double>(wss => wss.Average(ws => ws.Judgements.Average(j => j.Judgement.CombinedFitness)) - f0)
                : new Func<IEnumerable<WholeSample<T>>, double>(wss => wss.Average(ws => ws.Judgements.Average(j => j.Judgement.CombinedFitness)));
            var samples = Sample(traceInfo, resolution, sampler, epochsx, offset);


            switch (plotType)
            {
                case TrajectoryPlotType.Line:
                    LineSeries ls = new LineSeries() { Title = "Fitness", RenderInLegend = true, Color = OxyColors.Red, YAxisKey = fitnessaxis.Key, StrokeThickness = 2, LineStyle = lineStyle };
                    ls.Points.AddRange(samples);
                    GeneralPlotting.ApplyMarkers(ls, markerType, OxyColors.Red);
                    model.Series.Add(ls);
                    break;
                case TrajectoryPlotType.Scatter:
                    ScatterSeries ss = new ScatterSeries() { Title = "Fitness", RenderInLegend = true, YAxisKey = fitnessaxis.Key, MarkerSize = 2 };
                    ss.Points.AddRange(samples.Select(dp => new ScatterPoint(dp.X, dp.Y)));
                    GeneralPlotting.ApplyMarkers(ss, markerType, OxyColors.Red);
                    model.Series.Add(ss);
                    break;
                default:
                throw new Exception("Unsupported plot type: " + plotType);
            }
        }

        public static void AnnotateTraceTargetTransitions<T>(PlotModel model, TraceInfo<T> traceInfo, bool targetLabels, double offset) where T : IIndividual<T>
        {
            ITarget lastTarget = null;

            foreach (var ws in traceInfo.TraceWholeSamples)
            {
                ITarget nextTarget = ws.Target;

                if (nextTarget != lastTarget)
                {
                    var la = new LineAnnotation() { Type = LineAnnotationType.Vertical, X = ws.Generations + offset, Color = OxyColors.Gray };
                    
                    if (targetLabels)
                    {
                        la.TextOrientation = AnnotationTextOrientation.AlongLine;
                        la.Text = nextTarget.FriendlyName;
                    }

                    model.Annotations.Add(la);
                    lastTarget = nextTarget;

                }
            }
        }

        public static void AnnotateTraceEpochsTransitions<T>(PlotModel model, TraceInfo<T> traceInfo, bool epochLabels, double offset) where T : IIndividual<T>
        {
            int lastEpoch = -1;

            foreach (var ws in traceInfo.TraceWholeSamples)
            {
                int nextEpoch = ws.Epoch;

                if (nextEpoch != lastEpoch)
                {
                    var la = new LineAnnotation() { Type = LineAnnotationType.Vertical, X = ws.Generations + offset, Color = OxyColors.Gray, Layer = AnnotationLayer.AboveSeries };

                    if (epochLabels)
                    {
                        la.TextOrientation = AnnotationTextOrientation.AlongLine;
                        la.Text = $"Epoch {nextEpoch}";
                    }

                    model.Annotations.Add(la);
                    lastEpoch = nextEpoch;
                }
            }
        }
        
        public static void AnnotateTracesTargetTransitions(PlotModel model, TracesInfo tracesInfo, bool targetLabels)
        {
            foreach (var ts in tracesInfo.TargetSwitches)
            {
                var la = new LineAnnotation() { Type = LineAnnotationType.Vertical, X = ts.Generation, Color = OxyColors.Gray };
                    
                if (targetLabels)
                {
                    la.TextOrientation = AnnotationTextOrientation.AlongLine;
                    la.Text = ts.Target.FriendlyName;
                }

                model.Annotations.Add(la);
            }
        }
    }

    public class GenomePlotting
    {
        public static PlotModel PlotDtm(string title, Linear.Matrix<double> transMat, double? min, double? max)
        {
            var plot = GeneralPlotting.PlotMatrix(title, transMat, "Trait j", "Trait i", false, min, max);
            
            plot.PlotType = PlotType.Cartesian;
            
            return plot;
        }

        public static PlotModel PlotExpressionVector(string title, Linear.Vector<double> transMat, int moduleCount, double? min, double? max)
        {
            var plot = GeneralPlotting.PlotVectorFolded(title, transMat, moduleCount, "Sub-Trait", "Module", false, min, max);
            
            plot.PlotType = PlotType.Cartesian;
            
            return plot;
        }
    }

    public class GeneralPlotting
    {
        public static PlotModel PlotMatrix(string title, Linear.Matrix<double> matrix, string xlabel = "X", string ylabel = "Y", bool labels = false, double? min = null, double? max = null)
        {
            double[,] darr = matrix.Transpose().ToArray();
            return PlotHeatmap(title, darr, false, true, xlabel, ylabel, labels, min: min, max: max);
        }

        public static PlotModel PlotVectorFolded(string title, Linear.Vector<double> vector, int foldCount, string xlabel = "X", string ylabel = "Y", bool labels = false, double? min = null, double? max = null)
        {
            int foldLength = vector.Count / foldCount;
            double[,] darr = new double[foldLength, foldCount];

            for (int i = 0; i < foldLength; i++)
                for (int j = 0; j < foldCount; j++)
                    darr[i, j] = vector[j * foldLength + i];

            return PlotHeatmap(title, darr, false, true, xlabel, ylabel, labels, min: min, max: max);
        }

        public static PlotModel PlotHeatmap(string title, double[,] darr, bool flipX = false, bool flipY = false, string xlabel = "X", string ylabel = "Y", bool labels = false, double? min = null, double? max = null)
        {
            // pad
            bool padX = false;
            if (darr.GetLength(0) <= 1)
            {
                padX = true;

                var ndarr = new double[2, darr.GetLength(1)];
                for (int i = 0; i < darr.GetLength(1); i++)
                {
                    ndarr[0, i] = darr[0, i];
                    ndarr[1, i] = darr[0, i];
                }
                darr = ndarr;
            }
            bool padY = false;
            if (darr.GetLength(1) <= 1)
            {
                padY = true;

                var ndarr = new double[darr.GetLength(0), 2];
                for (int i = 0; i < darr.GetLength(0); i++)
                {
                    ndarr[i, 0] = darr[i, 0];
                    ndarr[i, 1] = darr[i, 0];
                }
                darr = ndarr;
            }

            // plot
            PlotModel model = new PlotModel() { Title = title };

            model.IsLegendVisible = false;
            Axis xaxis = new LinearAxis() { Key = "x", Title = xlabel, Position = AxisPosition.Bottom, MinimumMinorStep = 1, MinimumMajorStep = 1 };
            Axis yaxis = new LinearAxis() { Key = "y", Title = ylabel, Position = AxisPosition.Left, MinimumMinorStep = 1, MinimumMajorStep = 1 };
            Axis coloraxis = ColoursGrey(min: min, max: max);

            if (flipX)
            {
                xaxis.StartPosition = 1;
                xaxis.EndPosition = 0;
            }

            if (flipY)
            {
                yaxis.StartPosition = 1;
                yaxis.EndPosition = 0;
            }

            model.Axes.Add(xaxis);
            model.Axes.Add(yaxis);
            model.Axes.Add(coloraxis);

            double x0 = padX ? -1.0 : 0.0;
            double x1 = padX ? 1.0 : darr.GetLength(0) - 1.0;
            double y0 = padY ? -1.0 : 0.0;
            double y1 = padY ? 1.0 : darr.GetLength(1) - 1.0;

            // don't use Sillyness for these: they are integers
            //xaxis.MajorStep = Math.Max(1.0, Math.Floor(x1 / 5.0));
            //yaxis.MajorStep = Math.Max(1.0, Math.Floor(y1 / 5.0));

            //xaxis.MaximumPadding = 0;
            //xaxis.MinimumPadding = 0;
            //yaxis.MaximumPadding = 0;
            //yaxis.MinimumPadding = 0;

            double datamin = darr.Min2D(true);
            double datamax = darr.Max2D();
            double datarange = datamax - datamin;
            //coloraxis.MajorStep = SillyFloor10(datarange / 3.0);
            //coloraxis.MajorStep = SillyFloor10(datarange / 3.0);

            var hmap = new OxyPlot.Series.HeatMapSeries() { Title = title, RenderMethod = OxyPlot.Series.HeatMapRenderMethod.Rectangles, X0 = x0, X1 = x1, Y0 = y0, Y1 = y1, XAxisKey = "x", YAxisKey = "y", ColorAxisKey = "c" };
            hmap.Data = darr;
            model.Series.Add(hmap);

            if (labels)
                hmap.LabelFontSize = 0.2;

            return model;
        }

        public static double SillyFloor10(double d)
        {
            double l10 = Math.Log10(d);
            l10 = Math.Floor(l10);
            double e = Math.Pow(10, l10);

            if (e * 5 <= d)
                return e * 5;
            if (e * 2 <= d)
                return e * 2;
            return e;
        }

        public static double SillyCeil10(double d)
        {
            double l10 = Math.Log10(d);
            l10 = Math.Ceiling(l10);
            double e = Math.Pow(10, l10);

            if (e / 5 >= d)
                return e / 5;
            if (e / 2 >= d)
                return e / 2;
            return e;
        }

        public static LinearColorAxis ColoursGrey(string key = "c", double? min = null, double? max = null)
        {
            var axis = new OxyPlot.Axes.LinearColorAxis { Key = key, Title = "", Position = OxyPlot.Axes.AxisPosition.Right, IsPanEnabled = false, IsZoomEnabled = false, HighColor = OxyPlot.OxyColors.White, LowColor = OxyPlot.OxyColors.Black, Palette = OxyPlot.OxyPalettes.Gray(100) };

            if (max.HasValue)
                axis.Maximum = max.Value;
            if (min.HasValue)
                axis.Minimum = min.Value;

            return axis;
        }

        public static void ApplyMarkers(Series s, MarkerType markerType, OxyColor color)
        {
            if (s is LineSeries ls)
                ApplyMarkers(ls, markerType, color);
            if (s is LineSeries ss)
                ApplyMarkers(ss, markerType, color);
        }

        public static void ApplyMarkers(LineSeries ls, MarkerType markerType, OxyColor color)
        {
            ls.MarkerType = markerType;
            if (!color.IsAutomatic())
            {
                if (MarkerNeedsLine(markerType))
                    ls.MarkerStroke = color;
                else
                    ls.MarkerFill = color;
            }   
        }

        public static void ApplyMarkers(ScatterSeries ss, MarkerType markerType, OxyColor color)
        {
            ss.MarkerType = markerType;
            if (!color.IsAutomatic())
            {
                if (MarkerNeedsLine(markerType))
                    ss.MarkerStroke = color;
                else
                    ss.MarkerFill = color;
            }
        }

        public static bool MarkerNeedsLine(MarkerType markerType)
        {
            return markerType == MarkerType.Cross
                || markerType == MarkerType.Star;
        }
    }

    public class HistogramPlotting
    {
        public static PlotModel PrepareDodgyHistogram(string title, string xTitle, string yTitle, string xAxisKey, string yAxisKey)
        {
            var plot = new PlotModel() { Title = title };
            plot.Axes.Add(new LinearAxis() { Title = xTitle, Position = AxisPosition.Bottom, Key = xAxisKey ?? "x" });
            plot.Axes.Add(new LinearAxis() { Title = yTitle, Position = AxisPosition.Left, Key = yAxisKey ?? "y" });
            return plot;
        }

        public static void DodgyHistogram(PlotModel plot, IReadOnlyList<double[]> samples, IReadOnlyList<OxyColor> colors, double min, double max, int binCount)
        {
            for (int i = 0; i < samples.Count; i++)
            {
                var hs = new HistogramSeries() { FillColor = colors[i] };
                var bins = HistogramHelpers.Collect(samples[i], HistogramHelpers.CreateUniformBins(min, max, binCount), new BinningOptions(BinningOutlierMode.RejectOutliers, BinningIntervalType.InclusiveLowerBound, BinningExtremeValueMode.IncludeExtremeValues)).ToArray();
                bins = Dodgify(bins, i, samples.Count, samples[i].Length).ToArray();
                
                hs.Items.AddRange(bins);
                plot.Series.Add(hs);
            }
        }

        public static IEnumerable<HistogramItem> Dodgify(IEnumerable<HistogramItem> his, int index, int count, int totalHeight)
        {
            return his.Select(hi =>
            {
                var w = (hi.RangeEnd - hi.RangeStart) / count;
                var s = hi.RangeStart + w * index;
                var e = s + w;
                return new HistogramItem(s, e, hi.Area * totalHeight * w, hi.Count); // convert to a count
            });
        }
    }

    public class BoxPlotting
    {
        public static PlotModel PrepareBoxPlot(IReadOnlyList<string> labels, string categoryTitle, string yTitle, string xAxisKey, string yAxisKey)
        {
            var plot = new PlotModel() { Title = "Mean Fitness Distributions" };
            var catAxis = new CategoryAxis() { Title = categoryTitle, Position = AxisPosition.Bottom, Key = xAxisKey ?? "x" };
            plot.Axes.Add(catAxis);
            plot.Axes.Add(new LinearAxis() { Title = yTitle, Position = AxisPosition.Left, Key = yAxisKey ?? "y" });

            foreach (var label in labels)
                catAxis.Labels.Add(label);

            return plot;
        }

        public static BoxPlotSeries BoxPlot(PlotModel plot, IReadOnlyList<double[]> samples)
        {
            var bp = new BoxPlotSeries();
            for (int i = 0; i < samples.Count; i++)
            {
                var sample = samples[i];

                if (sample == null)
                    continue;

                var bi = BoxPlotting.Box(i, sample);
                bp.Items.Add(bi);
            }
            plot.Series.Add(bp);

            return bp;
        }

        public static BoxPlotItem Box(double x, IEnumerable<double> samples)
        {
            var sorted = samples.OrderBy(s => s).ToArray();

            if (sorted.Contains(double.NaN))
                return null;

            var lq = MathNet.Numerics.Statistics.ArrayStatistics.LowerQuartileInplace(sorted);
            var uq = MathNet.Numerics.Statistics.ArrayStatistics.UpperQuartileInplace(sorted);
            var median = MathNet.Numerics.Statistics.ArrayStatistics.MedianInplace(sorted);

            var lt = lq - (median - lq) * 1.5;
            var ut = uq + (uq - median) * 1.5;

            var low = Math.Min(samples.Where(s => s >= lt).Min(), lq);
            var high = Math.Max(samples.Where(s => s <= ut).Max(), uq);

            var bi = new BoxPlotItem(x, low, lq, median, uq, high);

            foreach (var o in sorted.Where(s => s < low || s > high))
                bi.Outliers.Add(o);

            return bi;
        }

        public static double Median(IReadOnlyList<double> samples)
        {
            int c = samples.Count;
            if (c % 2 == 1)
                return samples[c / 2];
            else
                return (samples[c / 2 - 1] + samples[c / 2]) / 2.0;
        }
    }
}
