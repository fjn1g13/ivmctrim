﻿using System;
using System.Collections.Generic;
using System.Text;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using System.Linq;
using System.IO;

namespace M4M
{
    public class ColourfulTraces
    {
        public ColourfulTraces(string name, OxyColor colour, IReadOnlyList<double[]> samples, int samplePeriod, long sampleOffset)
        {
            Name = name;
            Colour = colour;
            Samples = samples;
            SamplePeriod = samplePeriod;
            SampleOffset = SampleOffset;
        }

        public string Name { get; }
        public OxyColor Colour { get; }
        public IReadOnlyList<double[]> Samples { get; }
        public int SamplePeriod { get; }
        public long SampleOffset { get; }
    }

    public class TracePercentileInformation
    {
        public TracePercentileInformation(double low, double high, OxyColor colour, double strokeThickness, string label)
        {
            if (low < 0 || high > 1 || low > high)
                throw new ArgumentException("Low and high must be in the range [0, 1], and low must be no more than high.");

            Low = low;
            High = high;
            Colour = colour;
            StrokeThickness = strokeThickness;
            Label = label;
        }

        public TracePercentileInformation(double single, OxyColor colour, double strokeThickness, string label)
            : this(single, single, colour, strokeThickness, label)
        {
            // nix
        }

        public double Low { get; }
        public double High { get; }
        public OxyColor Colour { get; }
        public double StrokeThickness { get; }
        public string Label { get; }

        public bool IsSingle => Low == High;
    }

    public class EvolvabilityTraces
    {
        public static IReadOnlyList<TracePercentileInformation> DefaultTracePercentileInformations(string name, OxyColor colour)
        {
            return new []
            {
                new TracePercentileInformation(0.25, 0.75, colour.ChangeIntensity(0.8), 0.0, name + " 25-75"),
                new TracePercentileInformation(0.1, 0.9, colour.ChangeIntensity(0.5), 0.0, name + " 10-90"),
                new TracePercentileInformation(0.5, colour, 2.0, name + " median")
            };
        }

        public static IReadOnlyList<TracePercentileInformation> SymetricTracePercentileInformations(string name, OxyColor colour, IEnumerable<int> lows, bool labelAreas, double strokeThickness)
        {
            return lows.Select(l => 
            {
                if (l != 50)
                    return new TracePercentileInformation(l/100.0, 1.0 - l/100.0, colour.ChangeIntensity(l/100.0 + 0.5), 0.0, labelAreas ? $"{name} {l:00}-{100-l:00}" : null);
                else
                    return new TracePercentileInformation(0.5, colour, strokeThickness, labelAreas ? name + " median" : name);
            }).ToArray();
        }

        public static ColourfulTraces[] RunTraces(TextWriter console, PopulationExperimentConfig<DenseIndividual> popConfig, IReadOnlyList<Population<DenseIndividual>> populations, IReadOnlyList<string> names, IReadOnlyList<OxyColor> colours, ITarget[] targets, int count, int samplePeriod, int startEpoch, int epochs)
        {
            var rand = new MathNet.Numerics.Random.MersenneTwister(false);
            var context = new ModelExecutionContext(rand);

            var results = new ColourfulTraces[populations.Count];
            for (int i = 0; i < populations.Count; i++)
            {
                var traces = PopulationTrace.RunTraces(console, context, populations[i], popConfig, targets, count, samplePeriod, startEpoch, epochs);
                results[i] = new ColourfulTraces(names[i], colours[i], traces.BestFitnesses, samplePeriod, 0);
            }

            return results;
        }

        /// <summary>
        /// Do not store the results, they are liable to be modified
        /// </summary>
        public static IEnumerable<Percentile[]> Percentiles(IReadOnlyList<double[]> results, double[] positions, int step = 1)
        {
            for (int i = 0; i < results[0].Length; i += step)
            {
                double[] buffer = null;
                Percentile[] percentiles = null;
                Percentile.ComputePercentiles(positions, results.Select(r => r[i]), ref buffer, ref percentiles);
                yield return percentiles;
            }
        }

        /// <summary>
        /// Plots an area with DefaultTracePercentileInformations
        /// </summary>
        public static void PlotArea(PlotModel model, ColourfulTraces traces, int resolution)
        {
            var colour = traces.Colour;
            var name = traces.Name;

            var tpis = DefaultTracePercentileInformations(name, colour);
            PlotArea(model, traces, tpis, resolution);
            return;
        }

        public static void PlotArea(PlotModel model, ColourfulTraces traces, IReadOnlyList<TracePercentileInformation> tpis, int resolution)
        {
            var positions = tpis.SelectMany(tpi => tpi.IsSingle ? new[] { tpi.Low } : new[] { tpi.Low, tpi.High }).ToArray();
            var percentiles = Percentiles(traces.Samples, positions, resolution);

            List<AreaSeries> areas = new List<AreaSeries>();
            List<LineSeries> lines = new List<LineSeries>();
            
            foreach (var tpi in tpis)
            {
                if (!tpi.IsSingle)
                {
                    var s = new AreaSeries() { Color = tpi.Colour, LineStyle = LineStyle.Solid, StrokeThickness = tpi.StrokeThickness, Title = tpi.Label };
                    areas.Add(s);
                    model.Series.Add(s);
                }
                else
                {
                    var s = new LineSeries() { Color = tpi.Colour, LineStyle = LineStyle.Solid, StrokeThickness = tpi.StrokeThickness, Title = tpi.Label };
                    lines.Add(s);
                    model.Series.Add(s);
                }
            }

            int i = 0;
            foreach (var p in percentiles)
            {
                long generation = traces.SampleOffset + resolution * traces.SamplePeriod * i;
                var pcts = p;

                int j = 0;
                int ai = 0;
                int li = 0;

                foreach (var tpi in tpis)
                {
                    if (!tpi.IsSingle)
                    {
                        areas[ai].Points.Add(new DataPoint(generation, pcts[j++].Value));
                        areas[ai].Points2.Add(new DataPoint(generation, pcts[j++].Value));
                        ai++;
                    }
                    else
                    {
                        lines[li].Points.Add(new DataPoint(generation, pcts[j++].Value));
                        li++;
                    }
                }

                i++;
            }
        }

        public PlotModel PlotAreas(IEnumerable<ColourfulTraces> manyTraces, string title, int resolution)
        {
            PlotModel model = new PlotModel() { Title = title };

            model.Axes.Add(new LinearAxis() { Title = "Generation", Position = AxisPosition.Bottom });
            model.Axes.Add(new LinearAxis() { Title = "Fitness", Position = AxisPosition.Left });

            foreach (var traces in manyTraces)
            {
                PlotArea(model, traces, resolution);
            }
            
            return model;
        }
    }
}
