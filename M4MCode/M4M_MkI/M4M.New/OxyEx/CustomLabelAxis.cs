﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M4M.OxyEx
{
    public class CustomAxisLabel
    {
        public CustomAxisLabel(double value, string text)
        {
            Value = value;
            Text = text;
        }

        public double Value { get; }
        public string Text { get; }
    }

    public class CustomLabelLinearAxis : OxyPlot.Axes.LinearAxis
    {
        public bool ForceMax { get; set; } = false;
        public bool ForceMin { get; set; } = false;
        public bool IncludeTick { get; set; } = true;
        public bool Exclusive { get; set; } = false;

        public List<CustomAxisLabel> CustomAxisLabels = new List<CustomAxisLabel>();

        public CustomLabelLinearAxis()
        {
        }
            
        protected override string FormatValueOverride(double val)
        {
            double eps = Math.Abs(this.ActualMinimum - this.ActualMaximum) * 0.0001;
            
            string provisional = CustomAxisLabels.FirstOrDefault(cal => Math.Abs(val - cal.Value) < eps)?.Text;
            string @default = base.FormatValueOverride(val);

            if (provisional == null)
                provisional = @default;
            else if (provisional.Contains("$$$"))
                provisional = provisional.Replace("$$$", @default);

            return provisional;
        }

        public override void GetTickValues(out IList<double> majorLabelValues, out IList<double> majorTickValues, out IList<double> minorTickValues)
        {
            base.GetTickValues(out majorLabelValues, out majorTickValues, out minorTickValues);

            double eps = Math.Abs(this.ActualMinimum - this.ActualMaximum) * 0.0001;
            
            List<CustomAxisLabel> minMax = new List<CustomAxisLabel>();
            if (ForceMin)
                minMax.Add(new CustomAxisLabel(this.ActualMinimum, null));
            if (ForceMax)
                minMax.Add(new CustomAxisLabel(this.ActualMaximum, null));

            if (Exclusive)
            {
                majorLabelValues.Clear();
                if (IncludeTick)
                    majorTickValues.Clear();
            }

            foreach (var cal in CustomAxisLabels.Concat(minMax))
            {
                if (this.ActualMinimum <= cal.Value && cal.Value <= this.ActualMaximum
                    && majorLabelValues.All(lv => Math.Abs(lv - cal.Value) >= eps))
                {
                    majorLabelValues.Add(cal.Value);

                    if (IncludeTick)
                        majorTickValues.Add(cal.Value);
                }
            }
        }
    }
}
