using M4M;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M.ExperimentGroups
{
    public class GroupPlotting
    {
        public static PlotModel PlotBlockTracees<TIndividual>(IEnumerable<ExpWholeSamples<TIndividual>> exps, Func<WholeSample<TIndividual>, double> sampler) where TIndividual : IIndividual<TIndividual>
        {
            var cts = MakeBlockTracees(exps, sampler);
            return PlotBockTracees(cts);
        }

        public static PlotModel PlotBockTracees<TSample>(IEnumerable<ExpSamples<TSample>> exps, Func<TSample, double> sampler, int sampleRate)
        {
            var cts = MakeBlockTracees(exps, sampler, sampleRate);
            return PlotBockTracees(cts);
        }
        
        private static PlotModel PlotBockTracees(Dictionary<string, ColourfulTraces> tracees)
        {
            var plot = M4M.TrajectoryPlotting.PrepareTrajectoriesPlot("Fitness Trajectories", "Epoch", "Fitness", null, 0, 500, "x", "y", null, false, false, false);
            var multiPlot = new PlotModel();
            foreach (var ct in tracees.OrderBy(ct => ct.Key))
                M4M.EvolvabilityTraces.PlotArea(plot, ct.Value, 1);
            return plot;
        }

        public static double TerminalWholesampleSelector<TIndividual>(IEnumerable<WholeSample<TIndividual>> samples) where TIndividual : IIndividual<TIndividual>
        {
            return samples.Last().Judgements[0].Judgement.Benefit;
        }

        public static double MeanWholesampleSelector<TIndividual>(IEnumerable<WholeSample<TIndividual>> samples) where TIndividual : IIndividual<TIndividual>
        {
            return samples.Average(s => s.Judgements[0].Judgement.Benefit);
        }

        public static PlotModel PlotBox<TSample>(IEnumerable<ExpSamples<TSample>> exps, string title, string xTitle, string yTitle, Func<string, string> blockFormatter, Func<IEnumerable<TSample>, double> selector)
        {
            blockFormatter = blockFormatter ?? (x => x);

            var plot = new PlotModel() { Title = title };
            var caxis = new CategoryAxis() { Title = xTitle, Position = AxisPosition.Bottom, Key = "x" };
            plot.Axes.Add(caxis);
            plot.Axes.Add(new LinearAxis() { Title = yTitle, Position = AxisPosition.Left, Key = "y" });

            var bp = new BoxPlotSeries();
            int i = 0;
            foreach (var block in exps.GroupBy(e => e.ExpInfo.FullBlockRunName).OrderBy(ct => ct.Key))
            {
                caxis.Labels.Add(blockFormatter(block.Key));
                var samples = block.Select(ws => selector(ws.Samples));
                if (BoxPlotting.Box(i++, samples) is BoxPlotItem bi)
                    bp.Items.Add(bi);
            }
            //bp.BoxWidth = 0.8;
            plot.Series.Add(bp);

            return plot;
        }

        public static PlotModel PlotAverage<TSample>(IEnumerable<ExpSamples<TSample>> exps, string title, string xTitle, string yTitle, Func<string, string> blockFormatter, Func<IEnumerable<TSample>, double> selector)
        {
            blockFormatter = blockFormatter ?? (x => x);

            var plot = new PlotModel() { Title = title };
            var caxis = new CategoryAxis() { Title = xTitle, Position = AxisPosition.Bottom, Key = "x" };
            plot.Axes.Add(caxis);
            plot.Axes.Add(new LinearAxis() { Title = yTitle, Position = AxisPosition.Left, Key = "y" });

            var cs = new ColumnSeries();
            int i = 0;
            foreach (var block in exps.GroupBy(e => e.ExpInfo.FullBlockRunName).OrderBy(ct => ct.Key))
            {
                Console.WriteLine(block.Key);
                Console.WriteLine(blockFormatter(block.Key));
                caxis.Labels.Add(blockFormatter(block.Key));
                cs.Items.Add(new ColumnItem(block.Average(s => selector(s.Samples)), i++));
            }

            plot.Series.Add(cs);

            return plot;
        }

        public static Dictionary<string, M4M.ColourfulTraces> MakeBlockTracees<TIndividual>(IEnumerable<ExpWholeSamples<TIndividual>> exps, Func<WholeSample<TIndividual>, double> sampler) where TIndividual : IIndividual<TIndividual>
        {
            var blocks = exps.GroupBy(e => e.ExpInfo.Block).ToList();
            var dict = new Dictionary<string, M4M.ColourfulTraces>();
            var colors = new M4M.TrajectoryColours1D(OxyColors.LightBlue, OxyColors.Red).Colours(blocks.Count);

            int ci = 0;
            foreach (var block in blocks)
            {
                int sr = block.First().Samples[1].Epoch - block.First().Samples[0].Epoch;
                var samples = block.Select(r => r.Samples.Select(sampler).ToArray()).ToList();
                var ct = new M4M.ColourfulTraces($"Q={block.Key}", colors[ci++], samples, sr, 0);
                dict.Add(block.Key, ct);
            }

            return dict;
        }

        public static Dictionary<string, M4M.ColourfulTraces> MakeBlockTracees<TSample>(IEnumerable<ExpSamples<TSample>> exps, Func<TSample, double> selector, int sampleRate)
        {
            var grps = exps.GroupBy(e => e.ExpInfo.Block).ToList();
            var dict = new Dictionary<string, M4M.ColourfulTraces>();
            var colors = new M4M.TrajectoryColours1D(OxyColors.LightBlue, OxyColors.Red).Colours(grps.Count);

            int ci = 0;
            foreach (var grp in grps)
            {
                var samples = grp.Select(r => r.Samples.Select(selector).ToArray()).ToList();
                var ct = new M4M.ColourfulTraces($"Q={grp.Key}", colors[ci++], samples, sampleRate, 0);
                dict.Add(grp.Key, ct);
            }

            return dict;
        }
    }
}
