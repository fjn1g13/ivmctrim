using M4M;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace M4M.ExperimentGroups
{
    public class ExpSamples<TSample>
    {
        public ExpSamples(ExpInfo expInfo, IReadOnlyList<TSample> samples, string comment)
        {
            ExpInfo = expInfo ?? throw new ArgumentNullException(nameof(expInfo));
            Samples = samples ?? throw new ArgumentNullException(nameof(samples));
            Comment = comment;
        }

        public ExpInfo ExpInfo { get; }
        public IReadOnlyList<TSample> Samples { get; }
        public string Comment { get; }
    }

    public static class ExpSamplers
    {
        public static ExpSamples<double> SampleTrajectory(ExpInfo expInfo, string trajectoryFile, int trajectory, string comment, out int samplePeriod)
        {
            var fullpath = System.IO.Path.Combine(expInfo.Dir, trajectoryFile);
            var samples = Analysis.LoadTrajectories(fullpath, out samplePeriod)[trajectory];
            return new ExpSamples<double>(expInfo, samples, comment);
        }
    }

    // shouldn't be a class, but I'm too lazy to update all the code that uses it
    public class ExpWholeSamples<TIndividual> : ExpSamples<WholeSample<TIndividual>> where TIndividual : IIndividual<TIndividual>
    {
        public ExpWholeSamples(ExpInfo expInfo, IReadOnlyList<WholeSample<TIndividual>> samples, string comment)
         : base(expInfo, samples, comment)
        {
        }

        public static ExpWholeSamples<TIndividual> Sample(ExpInfo expInfo, string wholesampleFilename, string comment)
        {
            var fullpath = System.IO.Path.Combine(expInfo.Dir, wholesampleFilename);
            var samples = WholeSample<TIndividual>.LoadWholeSamples(fullpath);
            return new ExpWholeSamples<TIndividual>(expInfo, samples, comment);
        }

        public ExpSamples<TSample> Select<TSample>(Func<WholeSample<TIndividual>, TSample> selector, string comment)
        {
            return new ExpSamples<TSample>(ExpInfo, Samples.Select(selector).ToArray(), comment);
        }
    }

    public class ExpInfo
    {
        public ExpInfo(string dir, string block, string run, int repeat, string fullBlockRunName)
        {
            Dir = dir ?? throw new ArgumentNullException(nameof(dir));
            Block = block;
            Run = run;
            Repeat = repeat;
            FullBlockRunName = fullBlockRunName;
        }

        public string Dir { get; }
        public string Block { get; }
        public string Run { get; }
        public int Repeat { get; }
        public string FullBlockRunName { get; }

        /// <summary>
        /// Enumerates any repeated experiments that can be found in the given directory based on the indicator filePattern.
        /// Extracts information based on the pattern {blockPrefix}(block){blockPostfix}(run)/r(repeat)/.
        /// </summary>
        /// <param name="dir">The directory in which to recursively search.</param>
        /// <param name="blockPrefix">The block prefix (e.g. name of the run group)</param>
        /// <param name="filePattern">The file pattern to use as an indicator of a directory containing an appropriate experiment.</param>
        /// <param name="blockPostfix">The block postfix (e.g. 'runs')</param>
        /// <returns>Enumerates all the repeat experiments with the given </returns>
        public static IEnumerable<ExpInfo> EnumerateExps(string dir, string blockPrefix, string filePattern, string blockPostfix)
        {
            var files = System.IO.Directory.GetFiles(dir, filePattern, System.IO.SearchOption.AllDirectories);
            foreach (var f in files)
            {
                var block = Regex.Match(f, $@"(?<={blockPrefix})[^\\/]+(?={blockPostfix})").Groups[0].Value;
                var run = Regex.Match(f, $@"(?<={blockPrefix}{block}{blockPostfix})[^\\/]+(?=[\\/])").Groups[0].Value;
                var repeat = int.Parse(Regex.Match(f, @"(?<=[\\/]r)\d+(?=[\\/])").Groups[0].Value);

                yield return new ExpInfo(System.IO.Path.GetDirectoryName(f), block, run, repeat, blockPrefix + block + blockPostfix + run);
            }
        }
    }
}
