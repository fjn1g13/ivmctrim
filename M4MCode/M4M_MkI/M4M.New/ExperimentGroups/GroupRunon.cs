using M4M;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M.ExperimentGroups
{
    public class RunonExperimentData<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        // TODO: there is way too much in this constructor
        public RunonExperimentData(TextWriter console, string dir, string groupName, string runonExpFile, string expFileName, string postfix, string blockTerminator)
        {
            Dir = dir;
            GroupName = groupName;

            RunonExp = M4M.PopulationExperiment<TIndividual>.Load(runonExpFile);
            Exps = ExpInfo.EnumerateExps(dir, groupName, expFileName, blockTerminator).ToList();
            Runons = EnumerateRunon(console, Exps, RunonExp, expFileName, postfix).ToArray();
        }

        public string Dir { get; }
        public string GroupName { get; }

        public PopulationExperiment<TIndividual> RunonExp { get; }
        public IReadOnlyList<ExpInfo> Exps { get; }
        public ExpWholeSamples<TIndividual>[] Runons { get; }

        public static IEnumerable<ExpWholeSamples<TIndividual>> EnumerateRunon(TextWriter console, IEnumerable<ExpInfo> exps, M4M.PopulationExperiment<TIndividual> runonExp, string expFileName, string postfix)
        {
            var rand = new M4M.CustomMersenneTwister(1);
            var context = new M4M.ModelExecutionContext(rand);

            int i = 0;
            foreach (var e in exps)
            {
                var runonDir = System.IO.Path.Combine(e.Dir, "runon" + postfix);
                if (!System.IO.Directory.Exists(runonDir))
                    System.IO.Directory.CreateDirectory(runonDir);

                var runonWholesampleFile = System.IO.Path.Combine(runonDir, "wholesampleseRunon.dat");
                if (System.IO.File.Exists(runonWholesampleFile))
                {
                    var wsSat = M4M.WholeSample<TIndividual>.LoadWholeSamples(runonWholesampleFile);
                    yield return new ExpWholeSamples<TIndividual>(e, wsSat, "runon");
                }
                else
                {
                    var unRunonExpFile = System.IO.Path.Combine(e.Dir, expFileName);
                    var unRunonExp = PopulationExperiment<TIndividual>.Load(unRunonExpFile);

                    var exp = PopulationExperimentRunners.PrepareExperiment(unRunonExp.Population, runonExp.PopulationConfig, runonDir, disableAllIO: true, appendTimestamp: false);
                    var feedback = new WholeSampleFeedback<TIndividual>(new PopulationExperimentFeedback<TIndividual>(), 1, false);
                    var cliPrefix = $"{i}: ";
                    i++;

                    PopulationExperimentRunners.RunEpochs(console, cliPrefix, runonExp.PopulationConfig.ExperimentConfiguration.Epochs, context, exp, feedback.Feedback, -1, -1, nosave: true);
                    var wsRunon = feedback.WholeSamples;

                    exp.Save("runon", false);
                    WholeSample<TIndividual>.SaveWholeSamples(runonWholesampleFile, wsRunon);
                    yield return new ExpWholeSamples<TIndividual>(e, wsRunon, "runon");
                }
            }
        }
    }
}
