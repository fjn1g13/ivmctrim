using M4M;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace M4M.ExperimentGroups
{
    public class SaturatedExperimentData<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        // TODO: too much stuff in this constructor
        public SaturatedExperimentData(TextWriter console, string dir, string groupName, string satExpFile, string wholesampleFilename, string blockTerminator, ProjectionMode projectionMode, bool saturateExistingTarget)
        {
            Dir = dir;
            GroupName = groupName;
            ProjectionMode = projectionMode;

            SatExp = M4M.PopulationExperiment<TIndividual>.Load(satExpFile);
            Exps = ExpInfo.EnumerateExps(dir, groupName, wholesampleFilename + ".dat", blockTerminator).ToList();
            Sats = EnumerateSaturated(console, Exps, SatExp, wholesampleFilename, projectionMode, saturateExistingTarget).ToArray();
        }

        public string Dir { get; }
        public string GroupName { get; }
        public ProjectionMode ProjectionMode { get; }

        public PopulationExperiment<TIndividual> SatExp { get; }
        public IReadOnlyList<ExpInfo> Exps { get; }
        public ExpWholeSamples<TIndividual>[] Sats { get; }

        public static PopulationExperiment<TIndividual> ReplaceTargets(PopulationExperiment<TIndividual> exp, ITarget[] targets, FileStuff fileStuff)
        {
            var popConfig = exp.PopulationConfig;
            var config = exp.PopulationConfig.ExperimentConfiguration;
            var newConfig = new ExperimentConfiguration(config.Size, targets, config.TargetCycler, config.Epochs, config.GenerationsPerTargetPerEpoch, config.InitialStateResetProbability, config.InitialStateResetRange, config.ShuffleTargets, config.DevelopmentRules, config.ReproductionRules, config.JudgementRules);
            var newPopConfig = new PopulationExperimentConfig<TIndividual>(newConfig, popConfig.SelectorPreparer, popConfig.EliteCount, popConfig.HillclimberMode, popConfig.PopulationEndTargetOperation, popConfig.PopulationResetOperation, popConfig.CustomPopulationSpinner);
            var newExp = new PopulationExperiment<TIndividual>(exp.Population, newPopConfig, fileStuff);
            return newExp;
        }

        public static PopulationExperiment<TIndividual> Saturate(PopulationExperiment<TIndividual> exp, ITarget[] targets,  double min, double threshold, double max, FileStuff fileStuff)
        {
            var satTargets = targets.Select(t => new SaturationTarget(t, min, threshold, max)).ToArray();
            return ReplaceTargets(exp, satTargets, fileStuff);
        }

        public static IEnumerable<ExpWholeSamples<TIndividual>> EnumerateSaturated(TextWriter console, IEnumerable<ExpInfo> exps, M4M.PopulationExperiment<TIndividual> satExp, string wholesampleFilename, ProjectionMode projectionMode, bool saturateExistingTarget)
        {
            var rand = new M4M.CustomMersenneTwister(1);
            var context = new M4M.ModelExecutionContext(rand);
            IWholeSampleProjector<TIndividual> projector = saturateExistingTarget
                ? null 
                : new M4M.BasicExtractorWholeSampleProjectorPreparer(projectionMode, false).PrepareProjector(satExp);

            foreach (var e in exps)
            {
                var satPath = System.IO.Path.Combine(e.Dir, wholesampleFilename + "sat.dat");
                if (System.IO.File.Exists(satPath))
                {
                    var wsSat = M4M.WholeSample<TIndividual>.LoadWholeSamples(satPath);
                    yield return new ExpWholeSamples<TIndividual>(e, wsSat, "saturated");
                }
                else
                {
                    var unSatPath = System.IO.Path.Combine(e.Dir, wholesampleFilename + ".dat");
                    var ws = M4M.WholeSample<TIndividual>.LoadWholeSamples(unSatPath);

                    if (saturateExistingTarget)
                    {
                        var targets = new[] { ws.Last().Target };
                        var satTargetTemplate = (SaturationTarget)satExp.PopulationConfig.ExperimentConfiguration.Targets[0];
                        var fileStuff = FileStuff.CreateNow(e.Dir, "", "", false, false);
                        var exp = Saturate(satExp, targets, satTargetTemplate.Min, satTargetTemplate.Threshold, satTargetTemplate.Max, fileStuff);
                        projector = new M4M.BasicExtractorWholeSampleProjectorPreparer(projectionMode, false).PrepareProjector(exp);
                        exp.Save("sat", false);
                        console.WriteLine("SaturateExistingTarget: " + exp.FileStuff.OutDir);
                    }

                    var wsSat = M4M.WholeSampleProjectionHelpers.Project(console, context, ws, projector, false, false);
                    M4M.WholeSample<TIndividual>.SaveWholeSamples(satPath, wsSat);
                    yield return new ExpWholeSamples<TIndividual>(e, wsSat, "saturated");
                }
            }
        }

        public static void ProcessWholeSamples(TextWriter console, IEnumerable<ExpWholeSamples<TIndividual>> sats, M4M.PopulationExperiment<TIndividual> satExp, Func<string, string> blockFormatter)
        {
            blockFormatter = blockFormatter ?? (x => x);

            var satTarget = (M4M.SaturationTarget)satExp.PopulationConfig.ExperimentConfiguration.Targets[0];

            if (satTarget.Target is M4M.Epistatics.CorrelationMatrixTarget cmTarget)
            {
                var bMax = cmTarget.CorrelationMatrix.Enumerate().Count(x => x != 0.0);

                foreach (var e in sats)
                {
                    var last = e.Samples.Last();
                    var bt = last.Judgements[0].Judgement.Benefit;
                    var flatLine = e.Samples.AsEnumerable().Reverse().TakeWhile(s => s.Judgements[0].Judgement.Benefit == bt);
                    console.WriteLine($"{blockFormatter(e.ExpInfo.Block)}/runs{e.ExpInfo.Run}/r{e.ExpInfo.Repeat}: {last.Epoch - flatLine.Last().Epoch} epochs at {bt}/{bMax}");
                }
            }
            else// if (satTarget.Target is M4M.Epistatics.IIvmcProperTarget ivmcProperTarget)
            {
                console.WriteLine("ProcessWholeSamples: Unsupproted target type: " + satTarget.Target.GetType().FullName);
            }
        }
    }
}