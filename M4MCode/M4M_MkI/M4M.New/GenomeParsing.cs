﻿using M4M.Epistatics;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static M4M.Misc;

namespace M4M
{
    public static class GenomeParsing
    {
        public static IInitialStateMutator GetInitialStateMutator(string name, int size)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Equals("default", StringComparison.InvariantCultureIgnoreCase))
                    return null; // totally legit

                if (name.StartsWith("modules", StringComparison.InvariantCultureIgnoreCase))
                {
                    int moduleCount = int.Parse(name.Remove(0, "modules".Length));
                    return new ModuleInitialStateMutator(size, moduleCount);
                }

                if (name.StartsWith("onehotmodules", StringComparison.InvariantCultureIgnoreCase))
                {
                    var modules = Modular.MultiModulesStringParser.Instance.Parse(name.Remove(0, "onehotmodules".Length));
                    return new InitialStateOneHotModulesMutator(modules, 1.0, 0.0);
                }
            }

            throw new ArgumentException("Unrecognised initial state mutator: '" + name + "'. Consider one of these:\n" +
                " - default\n" +
                " - modules{moduleCount}");
        }

        public static ITransMatMutator GetTransMatMutator(string name, int size)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Equals("default", StringComparison.InvariantCultureIgnoreCase))
                    return null; // totally legit

                if (name.Equals("null", StringComparison.InvariantCultureIgnoreCase))
                    return NullTransMatMutator.Instance;

                if (name.Equals("throw", StringComparison.InvariantCultureIgnoreCase))
                    return ThrowTransMatMutator.Instance;

                if (name.Equals("singlecell", StringComparison.InvariantCultureIgnoreCase))
                    return new SingleCellTransMatMutator();

                if (name.Equals("singlecellsymmetric", StringComparison.InvariantCultureIgnoreCase))
                    return new SingleCellSymmetricTransMatMutator();

                if (name.Equals("celltransfer", StringComparison.InvariantCultureIgnoreCase))
                    return new CellTransferTransMatMutator();

                if (name.StartsWith("singlebymoduledivideweights", StringComparison.InvariantCultureIgnoreCase))
                {
                    var modules = Modular.MultiModulesStringParser.Instance.Parse(name.Remove(0, "singlebymoduledivideweights".Length));
                    return new SingleByModuleTransMatMutator(modules, true);
                }

                if (name.StartsWith("singlebymodule", StringComparison.InvariantCultureIgnoreCase))
                {
                    var modules = Modular.MultiModulesStringParser.Instance.Parse(name.Remove(0, "singlebymodule".Length));
                    return new SingleByModuleTransMatMutator(modules, false);
                }

                if (name.StartsWith("multicell", StringComparison.InvariantCultureIgnoreCase))
                {
                    int updateCount = Misc.ParseInt(name.Remove(0, "multicell".Length));
                    return new MutliCellTransMatMutator(updateCount);
                }

                if (name.StartsWith("columns", StringComparison.InvariantCultureIgnoreCase))
                {
                    int moduleCount = int.Parse(name.Remove(0, "columns".Length));
                    return new Columns2TransMatMutator(size, moduleCount);
                }

                if (name.StartsWith("singlecolumn", StringComparison.InvariantCultureIgnoreCase))
                {
                    int moduleCount = int.Parse(name.Remove(0, "singlecolumn".Length));
                    return new SingleColumnTransMatMutator(size, moduleCount);
                }
            }

            throw new ArgumentException("Unrecognised transmat mutator: '" + name + "'. Consider one of these:\n" +
                " - default\n" +
                " - null\n" +
                " - singlecell\n" +
                " - multicell{count}\n" +
                " - singlecolumn{moduleCount}\n" +
                " - columns{moduleCount}");
        }

        public static IInitialStateCombiner GetInitialStateCombiner(string name, int size)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Equals("default", StringComparison.InvariantCultureIgnoreCase))
                    return null; // totally legit

                if (name.Equals("uniformcrossover", StringComparison.InvariantCultureIgnoreCase))
                    return UniformCrossOver.Instance;
            }

            throw new ArgumentException("Unrecognised initial state Combiner: '" + name + "'. Consider one of these:\n" +
                " - default\n" +
                "uniformcrossover");
        }

        public static ITransMatCombiner GetTransMatCombiner(string name, int size)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Equals("default", StringComparison.InvariantCultureIgnoreCase))
                    return null; // totally legit

                if (name.Equals("uniformcrossover", StringComparison.InvariantCultureIgnoreCase))
                    return UniformCrossOver.Instance;
            }

            throw new ArgumentException("Unrecognised transmat Combiner: '" + name + "'. Consider one of these:\n" +
                " - default\n" +
                "uniformcrossover");
        }

        public static MatrixEntryAddress[] GetTransMatOpenEntries(string name, int size)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Equals("default", StringComparison.InvariantCultureIgnoreCase))
                    return null; // totally legit

                if (name.Equals("all", StringComparison.InvariantCultureIgnoreCase))
                    return null; // totally legit

                if (name.Equals("nodiagonal", StringComparison.InvariantCultureIgnoreCase))
                    return Enumerable.Range(0, size).SelectMany(i => Enumerable.Range(0, size - 1).Select(j => new MatrixEntryAddress(i, j >= i ? j + 1 : j))).ToArray();

                if (name.Equals("diagonal", StringComparison.InvariantCultureIgnoreCase))
                    return Enumerable.Range(0, size).Select(i => new MatrixEntryAddress(i, i)).ToArray();

                if (name.StartsWith("onblocks:", StringComparison.InvariantCultureIgnoreCase))
                    return Modular.MultiModulesStringParser.Instance.Parse(name.Substring("onblocks:".Length)).EnumerateOnBlockMatrixEntryAddresses().ToArray();

                if (name.StartsWith("offblocks:", StringComparison.InvariantCultureIgnoreCase))
                    return Modular.MultiModulesStringParser.Instance.Parse(name.Substring("offblocks:".Length)).EnumerateOffBlockMatrixEntryAddresses().ToArray();
            }

            throw new ArgumentException("Unrecognised TransMatOpenEntries: '" + name + "'. Consider one of these:\n" +
                " - all\n" +
                " - diagonal\n" +
                " - nodiagonal\n" +
                " - onblocks:modules\n" +
                " - offblocks:modules\n" +
                " - default\n}");
        }

        public static int[] GetInitialStateOpenEntries(string name, int size)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Equals("default", StringComparison.InvariantCultureIgnoreCase))
                    return null; // totally legit

                if (name.Equals("all", StringComparison.InvariantCultureIgnoreCase))
                    return null; // totally legit

                if (name.Equals("none", StringComparison.InvariantCultureIgnoreCase))
                    return new int[0]; // totally legit
            }

            throw new ArgumentException("Unrecognised GetInitialStateOpenEntries: '" + name + "'. Consider one of these:\n" +
                " - all\n" +
                " - none\n");
        }
    }
}
