﻿using M4M.Hebbian;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static M4M.Misc;

namespace M4M.ExperimentComposition
{
    public class BasicRegularisationFunctionComposer : IGenericComposer<IRegularisationFunction<IGenome>>
    {
        public PrefixedComposers<IRegularisationFunction<IGenome>> PrefixedComposers { get; } = new PrefixedComposers<IRegularisationFunction<IGenome>>();

        public IRegularisationFunction<IGenome> Compose(string name, TextWriter console, CliParams clips)
        {
            if (PrefixedComposers.TryCompose(name, console, clips, out var done))
            {
                return done;
            }

            // basic reg-funcs
            return ExperimentParsing.ParseRegFunction(name);
        }
    }

    public interface IExperimentConfigurationComposer
    {
        ExperimentConfiguration Compose(CliParams clips, TextWriter console, ITargetPackage targetPackage);
    }

    public class DefaultDevelopmentRulesComposer : IGenericPrefixedComposer<DevelopmentRules>
    {
        public IGenericComposer<ISquash> SquashComposer { get; set; }

        public string Prefix => "default";
        public string Syntax => "";

        public DevelopmentRules Compose(string name, TextWriter console, CliParams clips)
        {
            // drules
            int developmentalTimeSteps = clips.Get("T", int.Parse, 10);
            if (clips.IsSet("tau"))
                throw new Exception("Parameter tau has been deprecated; use decayRate (tau2) or updateRate (tau1) instead");
            double decayRate = clips.Get("decayRate", double.Parse, 0.2);
            double updateRate = clips.Get("udpateRate", double.Parse, 1.0);
            double rescaleScale = clips.Get("rescaleScale", double.Parse, 1.0);
            double rescaleOffset = clips.Get("rescaleOffset", double.Parse, 0.0);

            // g resets
            ISquash squash = DevelopmentRules.TanhHalf;
            if (clips.IsSet("squash"))
            {
                var squashStr = clips.Get("squash");
                if (SquashComposer != null)
                {
                    squash = SquashComposer.Compose(squashStr, console, clips);
                }
                else
                {
                    if (squashStr.StartsWith("tanh:"))
                    { // this shouldn't be here
                        var factorStr = squashStr.Substring("tanh:".Length);
                        double factor = double.Parse(factorStr);
                        squash = new TanhSquash(factor, $"tanh({factorStr})");
                    }
                    else
                    {
                        squash = DevelopmentRules.CommonSquashes.First(s => s.Name.Equals(squashStr, StringComparison.InvariantCultureIgnoreCase));
                    }
                }
            }

            //
            // Now, put it all together...
            //

            var drules = TypicalConfiguration.CreateStandardDevelopmentRules(
                squash: squash,
                timeSteps: developmentalTimeSteps,
                decayRate: decayRate,
                updateRate: updateRate,
                rescaleScale : rescaleScale,
                rescaleOffset: rescaleOffset
                );

            return drules;
        }
    }

    public class DefaultReproductionRulesComposer : IGenericPrefixedComposer<ReproductionRules>
    {
        public string Prefix => "default";
        public string Syntax => "";

        public ReproductionRules Compose(string name, TextWriter console, CliParams clips)
        {
            double gNoiseTerm = clips.Get("MG", double.Parse, 2.0);
            double bNoiseTerm = clips.Get("MB", double.Parse, 2E-5);
            
            NoiseType gNoiseType = clips.Get("MGtype", ExperimentParsing.ParseNoiseType, NoiseType.Binary);
            NoiseType bNoiseType = clips.Get("MBtype", ExperimentParsing.ParseNoiseType, NoiseType.Uniform);
            
            // b mutation weirdness
            double bprob = clips.Get("bprob", double.Parse, 1.0);
            bool bex = clips.Get("bex", bool.Parse, false);

            int gUpdates = clips.Get("CG", int.Parse, 1);

            double gMin = clips.Get("GMin", double.Parse, -1);
            double gMax = clips.Get("GMax", double.Parse, +1);
            double bMin = clips.Get("BMin", double.Parse, double.NegativeInfinity);
            double bMax = clips.Get("BMax", double.Parse, double.PositiveInfinity);

            var rrules = TypicalConfiguration.CreateStandardReproductionRules(
                gMutationMagnitude: gNoiseTerm,
                gMutationType: gNoiseType,
                bMutationMagnitude: bNoiseTerm,
                bMutationProbability: bprob,
                bMutationType: bNoiseType,
                exclusiveBMutation: bex,
                gUpdates: gUpdates,
                gClamping: new Range(gMin, gMax),
                bClamping: new Range(bMin, bMax)
                );

            return rrules;
        }
    }

    public class DefaultJudgementRulesComposer : IGenericPrefixedComposer<JudgementRules>
    {
        public IGenericComposer<IRegularisationFunction<IGenome>> RegularisationFunctionComposer { get; } = new BasicRegularisationFunctionComposer();

        public string Prefix => "default";
        public string Syntax => "";

        public JudgementRules Compose(string name, TextWriter console, CliParams clips)
        {
            double lambda = clips.Get("lambda", double.Parse);
            double noisefactor = clips.Get("kappa", double.Parse, 0.0);

            IRegularisationFunction<IGenome> regFunc = RegularisationFunctionComposer.Compose(clips.Get("regfunc", "L1"), console, clips);
            
            var jrules = TypicalConfiguration.CreateStandardJudgementRules(
                regularisationFactor: lambda,
                regularisationFunction: regFunc,
                noiseFactor: noisefactor
                );

            return jrules;
        }
    }

    public class BasicExperimentConfigurationComposer<TGenome> : IExperimentConfigurationComposer where TGenome : IGenome<TGenome>
    {
        public PrefixedComposers<DevelopmentRules> DevelopmentRulesComposers { get; set; } = new PrefixedComposers<DevelopmentRules>(new[] { new DefaultDevelopmentRulesComposer() });
        public PrefixedComposers<ReproductionRules> ReproductionRulesComposers { get; set; } = new PrefixedComposers<ReproductionRules>(new[] { new DefaultReproductionRulesComposer() });
        public PrefixedComposers<JudgementRules> JudgementRulesComposers { get; set; } = new PrefixedComposers<JudgementRules>(new[] { new DefaultJudgementRulesComposer() });

        public ExperimentConfiguration Compose(CliParams clips, TextWriter console, ITargetPackage targetPackage)
        {   
            var drules = DevelopmentRulesComposers.Compose(clips.Get("drules", "default"), console, clips);
            var rrules = ReproductionRulesComposers.Compose(clips.Get("rrules", "default"), console, clips);
            var jrules = JudgementRulesComposers.Compose(clips.Get("jrules", "default"), console, clips);
            
            int epochs = clips.Get("epochs", Misc.ParseInt, 100000);
            int K = clips.Get("K", Misc.ParseInt, 1000);

            double gResetProb = clips.Get("gresetprob", double.Parse, 0.0);

            double gResetMin = clips.Get("gresetmin", double.Parse, -1);
            double gResetMax = clips.Get("gresetmax", double.Parse, +1);

            int N = targetPackage.TargetSize;

            var cycler = Cyclers.GetStandardCycler(clips.Get("cycler", "loop"));
            var shuffleTargets = clips.Get("shuffleTargets", bool.Parse, false);

            //
            // Now, put it all together...
            // TODO: put this in its own Composer?
            //

            var config = TypicalConfiguration.CreateConfig(
                N: N,
                targets: targetPackage.Targets,
                targetCycler: cycler,
                epochs: epochs,
                generationsPerTargetPerEpoch: K,
                gResetProb: gResetProb,
                gResetRange: new Range(gResetMin, gResetMax),
                shuffleTargets: shuffleTargets,
                drules: drules,
                rrules: rrules,
                jrules: jrules
                );

            return config;
        }
    }

    public interface IPopulationExperimentConfigurationComposer<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        PopulationExperimentConfig<TIndividual> Compose(TextWriter console, CliParams clips, ExperimentConfiguration experimentConfiguration);
    }

    public class DefaultPopulationSpinnerComposer<TIndividual> : IGenericPrefixedComposer<IPopulationSpinner<TIndividual>> where TIndividual : IIndividual<TIndividual>
    {
        public string Prefix => "default:";
        public string Syntax => "type";

        public IPopulationSpinner<TIndividual> Compose(string name, TextWriter console, CliParams clips)
        {
            return Parse(name);
        }

        public static IPopulationSpinner<TIndividual> Parse(string name)
        {
            if (name.Equals("default", StringComparison.InvariantCultureIgnoreCase))
            {
                return DefaultPopulationSpinner<TIndividual>.Instance;
            }
            else if (name.Equals("paired", StringComparison.InvariantCultureIgnoreCase))
            {
                return PairedPopulationSpinner<TIndividual>.Instance;
            }
            else if (name.Equals("trios", StringComparison.InvariantCultureIgnoreCase))
            {
                return RecombinantTriosPopulationSpinner<TIndividual>.Instance;
            }
            else
            {
                throw new Exception($"Unrecognised default population spinner type: {name}; consider one of default/paired/trios");
            }
        }
    }

    public class DensePopulationSpinnerComposer : IGenericPrefixedComposer<IPopulationSpinner<DenseIndividual>>
    {
        public string Prefix => "dense:";
        public string Syntax => "type";

        public IPopulationSpinner<DenseIndividual> Compose(string name, TextWriter console, CliParams clips)
        {
            return Parse(name, clips);
        }

        public static IPopulationSpinner<DenseIndividual> Parse(string name, CliParams clips)
        {
            if (name.Equals("deltarule", StringComparison.InvariantCultureIgnoreCase))
            {
                var deltaRuleFitnessPowerFactor = clips.Get("deltarulefitnesspowerfactor", double.Parse, 0.0);
                return new DeltaRule.DeltaRuleSpinner(deltaRuleFitnessPowerFactor);
            }
            else if (name.Equals("hebbian", StringComparison.InvariantCultureIgnoreCase))
            {
                var deltaRuleFitnessPowerFactor = clips.Get("hebbianfitnesspowerfactor", double.Parse, 0.0);
                var hebbianType = clips.Get("hebbiantype", ExperimentParsing.ParseHebbianType, Hebbian.HebbianType.Standard);
                var signedHebbian = clips.Get("signedHebbian", bool.Parse, false);
                var innerSpinner = clips.Get("innerSpinner", s => Parse(s, clips), null);
                var noDiagonal = clips.Get("hebbianNoDiagonal", bool.Parse, false);
                var matrixNormalisation = clips.Get("hebbianMatrixNormalisation", ExperimentParsing.ParseMatrixNormalisation, MatrixNormalisation.None);
                var fitnessRescale = clips.Get("hebbianFitnessRescale", double.Parse, 1.0);
                var fitnessOffset = clips.Get("hebbianFitnessOffset", double.Parse, 0.0);
                return new Hebbian.HebbianSpinner(deltaRuleFitnessPowerFactor, hebbianType, signedHebbian, innerSpinner, noDiagonal, matrixNormalisation, fitnessRescale, fitnessOffset);
            }
            else if (name.Equals("hopfield", StringComparison.InvariantCultureIgnoreCase))
            {
                var squashStr = clips.Get("squash", "(Tanh(x/2)+1)/2");
                var updateRate = clips.Get("hopfieldupdaterate", double.Parse);
                var decayRate = clips.Get("hopfielddecayrate", double.Parse, updateRate);
                var squash = DevelopmentRules.CommonSquashes.First(s => s.Name.Equals(squashStr, StringComparison.InvariantCultureIgnoreCase)); // deficient
                var biasType = clips.Get("hopfieldBiasType", ExperimentParsing.ParseBiasType, Hopfield.BiasType.Constant);
                var interactionType = clips.Get("hopfieldInteractionType", ExperimentParsing.ParseInteractionType, Hopfield.InteractionType.Linear);
                var interactionMatrixSource = clips.Get("hopfieldInteractionMatrixSource", ExperimentParsing.ParseInteractionMatrixSource, Hopfield.InteractionMatrixSource.Genome);
                var biasVectorSource = clips.Get("hopfieldBiasVectorSource", ExperimentParsing.ParseBiasVectorSource, Hopfield.BiasVectorSource.Genome);
                return new Hopfield.HopfieldSpinner(squash, updateRate, decayRate, biasType, interactionType, interactionMatrixSource, biasVectorSource);
            }
            else if (name.Equals("lotkavolterra", StringComparison.InvariantCultureIgnoreCase))
            {
                var m = clips.Get("growthrate", double.Parse);
                return new LotkaVolterra.LotkaVolterraSpinner(m);
            }
            else
            {
                throw new Exception($"Unrecognised Dense population spinner type: {name}; consider deltarule");
            }
        }
    }

    public class BasicDensePopulationExperimentConfigurationComposer : IPopulationExperimentConfigurationComposer<DenseIndividual>
    {
        public PrefixedComposers<IPopulationSpinner<DenseIndividual>> SpinnerComposer = new PrefixedComposers<IPopulationSpinner<DenseIndividual>>();

        public BasicDensePopulationExperimentConfigurationComposer(IEnumerable<IGenericPrefixedComposer<IPopulationSpinner<DenseIndividual>>> spinnerComposers, string defaultPopulationSpinnerComposer)
        {
            SpinnerComposer = new PrefixedComposers<IPopulationSpinner<DenseIndividual>>(spinnerComposers);
            DefaultPopulationSpinnerComposer = defaultPopulationSpinnerComposer;
        }

        public string DefaultPopulationSpinnerComposer { get; set; } = null;

        public PopulationExperimentConfig<DenseIndividual> Compose(TextWriter console, CliParams clips, ExperimentConfiguration experimentConfiguration)
        {
            bool binaryGResets = clips.Get("gresetbinary", bool.Parse, experimentConfiguration.ReproductionRules.InitialStateMutationType == NoiseType.Binary);
            var resetIndividualInitialStateOperation = clips.Get("resetoperation", ExperimentParsing.ParsePopulationResetOperation, experimentConfiguration.InitialStateResetProbability == 0 ? PopulationResetOperations.None : (binaryGResets ? PopulationResetOperations.RandomBinaryIndependent : PopulationResetOperations.RandomIndependent));

            // hill-climber by default
            bool hillclimberMode = clips.Get("hc", bool.Parse, true);
            int eliteCount = clips.Get("eliteCount", int.Parse, 1);
            var spinner = SpinnerComposer.Compose(clips.Get("spinner", DefaultPopulationSpinnerComposer), console, clips);
            var selectorPreparer = clips.Get("selectorpreparer", ExperimentParsing.ParseSelectorPreparer<DenseIndividual>, SelectorPreparers<DenseIndividual>.Ranked);

            var popConfig = TypicalConfiguration.CreatePopulationConfig(
                config: experimentConfiguration,
                eliteCount: eliteCount,
                hillclimberMode: hillclimberMode,
                resetIndividualInitialStateOperation: resetIndividualInitialStateOperation,
                customPopulationSpinner : spinner,
                selector: selectorPreparer
                );

            return popConfig;
        }
    }
}
