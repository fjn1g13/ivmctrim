﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M.ExperimentComposition
{
    public interface ITargetPackageComposer
    {
        ITargetPackage Compose(TextWriter console, CliParams clips);
    }

    public class BasicTargetPackageComposer : ITargetPackageComposer
    {
        public BasicTargetPackageComposer(IReadOnlyList<IGenericPrefixedComposer<ITargetPackage>> prefixedComposers, string defaultTargetPackage)
        {
            PrefixedComposers = new PrefixedComposers<ITargetPackage>(prefixedComposers);
            DefaultTargetPackage = defaultTargetPackage;
        }

        public PrefixedComposers<ITargetPackage> PrefixedComposers { get; }
        public string DefaultTargetPackage { get; set; }

        public ITargetPackage Compose(TextWriter console, CliParams clips)
        {
            var targetPackageStr = DefaultTargetPackage != null ? clips.Get("targetPackage", DefaultTargetPackage) : clips.Get("targetPackage");
            var targetPackage = PrefixedComposers.Compose(targetPackageStr, console, clips);

            if (clips.IsSet("duties"))
            {
                var duties = CliPlotHelpers.ParseDoubleList(clips.Get("duties"));

                if (duties.Count != targetPackage.Targets.Count + 1)
                {
                    throw new Exception($"Duty {duties.Count} count was not equal to one plut the target count {targetPackage.Targets.Count}");
                }

                var dutyTargets = new DutyTarget[targetPackage.Targets.Count];
                for (int i = 0; i < dutyTargets.Length; i++)
                {
                    dutyTargets[i] = new DutyTarget(targetPackage.Targets[i], duties[i], duties[i + 1]);
                }

                targetPackage = new TargetPackage($"Duty({targetPackage.Name})", dutyTargets);
            }

            if (clips.Get("saturate", bool.Parse, false))
            {
                var satMin = clips.Get("satMin", double.Parse, -1.0);
                var satThreshold = clips.Get("satThreshold", double.Parse, 0.0);
                var satMax = clips.Get("satMax", double.Parse, +1.0);

                targetPackage = new TargetPackage($"Sat({targetPackage.Name})", targetPackage.Targets.Select(t => new SaturationTarget(t, satMin, satThreshold, satMax)).ToArray());
            }

            return targetPackage;
        }
    }

    public class ExpTargetPackageComposer : IGenericPrefixedComposer<ITargetPackage>
    {
        public string Prefix => "exp:";
        public string Syntax => "expfile";

        public ITargetPackage Compose(string fileName, TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("ExpTargetPackageComposer reads targets from the given expfile");
            }

            // read from an experiment file
            var expConfig = CliPlotHelpers.LoadExperimentConfig(fileName);
            return new Epistatics.EpistaticTargetPackage(fileName, expConfig.Targets.ToArray());
        }
    }

    public class FileTargetPackageComposer : IGenericPrefixedComposer<ITargetPackage>
    {
        public FileTargetPackageComposer(ITargetPackageComposer superComposer)
        {
            SuperComposer = superComposer;
        }

        public string Prefix => "file:";
        public string Syntax => "filename";

        public ITargetPackageComposer SuperComposer { get; }

        public ITargetPackage Compose(string fileName, TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("FileTargetPackageComposer reads multiple targets from the given file (line by line)");
            }

            // read many from a file
            var lines = System.IO.File.ReadLines(fileName);

            List<ITarget> targets = new List<ITarget>();
            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line) || line.StartsWith("//"))
                    continue;

                var cclips = clips.CloneCliParams();
                cclips.ConsumeLine(line);
                var innerTargets = SuperComposer.Compose(console, clips);
                targets.AddRange(innerTargets.Targets);
            }

            return new Epistatics.EpistaticTargetPackage(fileName, targets.ToArray());
        }
    }

    public class IvmcTargetPackageComposer : IGenericPrefixedComposer<ITargetPackage>
    {
        public string Prefix => "ivmc:";
        public string Syntax => "ivmctargetclass";
        
        public ITargetPackage Compose(string name, TextWriter console, CliParams clips)
        {
            return ExperimentParsing.ParseIvmcTargetPackage(console, clips, name);
        }
    }

    public class SimpleStackedTargetPackageComposer : IGenericPrefixedComposer<ITargetPackage>
    {
        public string Prefix => "simpleStacked:";
        public string Syntax => "simpleStackedTargetClass";

        public ITargetPackage Compose(string name, TextWriter console, CliParams clips)
        {
            return ExperimentParsing.ParseSimpleStackedTargetPackage(console, clips, name);
        }
    }

    public class CorrelationMatrixTargetPackageComposer : IGenericPrefixedComposer<ITargetPackage>
    {
        public string Prefix => "correl:";
        public string Syntax => "CorrelationMatrixTargetclass";

        public ITargetPackage Compose(string name, TextWriter console, CliParams clips)
        {
            return ExperimentParsing.ParseCorrelationMatrixTargetPackage(clips, name);
        }
    }

    public class McTargetPackageComposer : IGenericPrefixedComposer<ITargetPackage>
    {
        public string Prefix => "mc:";
        public string Syntax => "mctargetclass";
        
        public ITargetPackage Compose(string name, TextWriter console, CliParams clips)
        {
            return ExperimentParsing.ParseMcTargetPackage(console, clips, name);
        }
    }

    public class ClassicTargetPackageComposer : IGenericPrefixedComposer<ITargetPackage>
    {
        public static Dictionary<string, Func<CliParams, IVectorTargetJudger>> DefaultCustomJudgerPrepares = new Dictionary<string, Func<CliParams, IVectorTargetJudger>>()
        {
            ["mse"] = clips => new EuclideanTargetJudger(clips.Get("msefactor", double.Parse), clips.Get("msebias", double.Parse, 0.0))
        };

        public ClassicTargetPackageComposer(Dictionary<string, Func<CliParams, IVectorTargetJudger>> customJudgerPreparers)
        {
            CustomJudgerPreparers = customJudgerPreparers;
        }

        public ClassicTargetPackageComposer()
        {
            CustomJudgerPreparers = new Dictionary<string, Func<CliParams, IVectorTargetJudger>>();
        }

        public Dictionary<string, Func<CliParams, IVectorTargetJudger>> CustomJudgerPreparers { get; }

        public string Prefix => "classic:";
        public string Syntax => "classictargetclass";
        
        public ITargetPackage Compose(string targetPackageClass, TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("ClassicTargetPackageComposer assembles classic (M4M/HMOD) target packages. Supported classes include:" +
                    " - default\n" +
                    " - modular\n" +
                    " - orig4x4 and std4x4\n" +
                    "Common parameters include:\n" +
                    " - customerjudger, the custom target judger (one of " + string.Join(", ", CustomJudgerPreparers.Keys) + ")\n" +
                    " - normalisecustomjudger, whether to normalise any custom judger\n" +
                    " - targetstring, basic +/- sequence\n" +
                    " - modulesstring, module assignments\n" +
                    " - variations, modular variations ");
            }

            if (targetPackageClass.Equals("default", StringComparison.InvariantCultureIgnoreCase))
            {
                var targetStrings = clips.Get("targetStrings", s => s.Split('|'));
                var name = clips.Get("targetpackagename", "classic");
                
                IVectorTargetJudger judger = clips.Get("customjudger", s => CustomJudgerPreparers[s](clips), null);
                bool normaliseCustomJudger = clips.Get("normalisecustomjudger", bool.Parse, true);

                List<VectorTarget> targets = new List<VectorTarget>();

                int i = 0;
                foreach (var ts in targetStrings)
                {
                    var t = new VectorTarget(ts, "Target" + i, judger, normaliseCustomJudger);

                    targets.Add(t);
                    i++;
                }

                return new TargetPackage(name, targets);
            }
            else if (targetPackageClass.Equals("std4x4", StringComparison.InvariantCultureIgnoreCase))
            {
                return Modular.ModularTargetPackage.Standard4x4;
            }
            else if (targetPackageClass.Equals("orig4x4", StringComparison.InvariantCultureIgnoreCase))
            {
                return Modular.ModularTargetPackage.Original4x4;
            }
            else if (targetPackageClass.StartsWith("modular", StringComparison.InvariantCultureIgnoreCase))
            {
                var modulesString = clips.Get("modulesString", targetPackageClass.Substring("modular".Length));
                var modules = Modular.MultiModulesStringParser.Instance.Parse(modulesString);

                var moduleVariations = clips.Get("variations", s => s.Split('|'));

                var targetString = clips.Get("targetString", new string('+', modules.ElementCount));
                var baseTargetVector = Misc.ParseExtremesVector(targetString, -1.0, 1.0, 0.0);

                var name = clips.Get("targetpackagename", "classic");

                IVectorTargetJudger judger = clips.Get("customjudger", s => CustomJudgerPreparers[s](clips), null);
                bool normaliseCustomJudger = clips.Get("normalisecustomjudger", bool.Parse, true);

                List<VectorTarget> targets = new List<VectorTarget>();

                int i = 0;
                foreach (var mv in moduleVariations)
                {
                    var extreme = Misc.ParseExtremesVector(mv, -1.0, 1.0, 0.0).ToArray();
                    var expanded = MathNet.Numerics.LinearAlgebra.CreateVector.DenseOfArray(modules.Expand(extreme));
                    expanded.PointwiseMultiply(baseTargetVector, expanded);

                    var t = new VectorTarget(expanded, "ModularTarget" + i, judger, normaliseCustomJudger);

                    targets.Add(t);
                    i++;
                }

                return new TargetPackage(name, targets);
            }
            else
            {
                throw new ArgumentException($"Unrecognised Classic TargetPackageClass: {targetPackageClass}; consider one of:\n" +
                    " - default\n" +
                    " - std4x4\n" +
                    " - orig4x4\n" +
                    " - modular{modulesstring}", nameof(targetPackageClass));
            }
        }
    }
    public class BinaryPuzzleTargetPackageComposer : IGenericPrefixedComposer<ITargetPackage>
    {
        public string Prefix => "bin:";
        public string Syntax => "bintargetclass";

        public ITargetPackage Compose(string binaryPuzzleTargetClass, TextWriter console, CliParams clips)
        {
            if (binaryPuzzleTargetClass.Equals("square"))
            {
                return ExperimentParsing.ParseSquareBinaryPuzzle(clips);
            }
            else
            {
                throw new ArgumentException($"Unrecognised BinaryPuzzle TargetClass: {binaryPuzzleTargetClass}; consider one of:\n" +
                    " - square{target}", nameof(binaryPuzzleTargetClass));
            }
        }
    }

}
