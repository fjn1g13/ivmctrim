﻿using M4M.Epistatics;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M.ExperimentComposition
{
    public interface IPopulationComposer<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        Population<TIndividual> Compose(TextWriter console, CliParams clips, ModelExecutionContext context, PopulationExperimentConfig<TIndividual> populationExperimentConfiguration);
    }
    
    public interface IPrefixedPopulationComposer<TIndividual> : IPrefixed where TIndividual : IIndividual<TIndividual>
    {
        Population<TIndividual> Compose(string populationType, TextWriter console, CliParams clips, ModelExecutionContext context, PopulationExperimentConfig<TIndividual> populationExperimentConfiguration);
    }

    public class BasicPopulationComposer<TIndividual> : IPopulationComposer<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        public BasicPopulationComposer(IReadOnlyList<IPrefixedPopulationComposer<TIndividual>> prefixedComposers, string defaultPopulationType)
        {
            PrefixedComposers = new Prefixeds<IPrefixedPopulationComposer<TIndividual>>(prefixedComposers);
            DefaultPopulationType = defaultPopulationType;
        }

        public Prefixeds<IPrefixedPopulationComposer<TIndividual>> PrefixedComposers { get; } = new Prefixeds<IPrefixedPopulationComposer<TIndividual>>();
        public string DefaultPopulationType { get; set; }

        public Population<TIndividual> Compose(TextWriter console, CliParams clips, ModelExecutionContext context, PopulationExperimentConfig<TIndividual> populationExperimentConfiguration)
        {
            return PrefixedComposers.Perform(clips.Get("pop", DefaultPopulationType), (pc, subName) => pc.Compose(subName, console, clips, context, populationExperimentConfiguration));
        }
    }

    public class BasicDensePopulationComposer : IPrefixedPopulationComposer<DenseIndividual>
    {
        public string Prefix => "dense:";
        public string Syntax => "populationtype";
        
        public Population<DenseIndividual> Compose(string populationType, TextWriter console, CliParams clips, ModelExecutionContext context, PopulationExperimentConfig<DenseIndividual> populationExperimentConfiguration)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("DenseHillclimber parameters include:\n" +
                    " - popSize\n" +
                    " - initialstatemutator\n" +
                    " - transmatmutator\n" +
                    " - initialstatecombiner\n" +
                    " - transmatcombiner\n" +
                    " - transmatopenentries\n" +
                    " - g0string (extreme value string describing the initial initial-state vector\n" +
                    " - b0string, columns\n" +
                    " - templateGenome, a template genome file\n" +
                    " - rndnormalb, produces a random b with the given stddev around 0, default is 1; multiplies by the otherwise specified matrix if already qualified\n" +
                    " - rnduniformb, produces a random b with a uniform distribution with the given maximum magnitute; multiplies by the otherwise specified matrix if already qualified");
            }
            
            var popConfig = populationExperimentConfiguration;
            var config = popConfig.ExperimentConfiguration;

            int popSize = clips.Get("popSize", int.Parse, 1);
            IInitialStateMutator initialStateMutator = GenomeParsing.GetInitialStateMutator(clips.Get("initialstatemutator", "default"), config.Size);
            ITransMatMutator transMatMutator = GenomeParsing.GetTransMatMutator(clips.Get("transmatmutator", "default"), config.Size);
            IInitialStateCombiner initialStateCombiner = GenomeParsing.GetInitialStateCombiner(clips.Get("initialstatecombiner", "default"), config.Size);
            ITransMatCombiner transMatCombiner = GenomeParsing.GetTransMatCombiner(clips.Get("transmatcombinerr", "default"), config.Size);
            
            var gOpenEntries = GenomeParsing.GetInitialStateOpenEntries(clips.Get("initialstateopenentries", "default"), config.Size);
            var bOpenEntries = GenomeParsing.GetTransMatOpenEntries(clips.Get("transmatopenentries", "default"), config.Size);

            var g0 = ComposeG0(clips, context, config);
            var b0 = ComposeB0(clips, context, config);
            var biasVector0 = ComposeBiasVector0(clips, context, config);

            var epigenetic = clips.Get("epigenetic", bool.Parse, false);

            switch (populationType)
            {
                case "hillclimber":
                    return TypicalConfiguration.CreatePopulation(context, popSize, config.Size, config.DevelopmentRules, initialStateMutator: initialStateMutator, transMatMutator: transMatMutator, g0: g0, b0: b0, gOpenEntries: gOpenEntries, bOpenEntries: bOpenEntries, epigenetic: epigenetic, biasVector0: biasVector0);
            }

            throw new Exception("Unrecognised dense population type: " + populationType);
        }

        public static MathNet.Numerics.LinearAlgebra.Vector<double> ComposeG0(CliParams clips, ModelExecutionContext context, ExperimentConfiguration config)
        {
            // g0
            var g0string = clips.Get("g0string", null);
            var g0 = g0string == null ? null : Misc.ParseExtremesVector(g0string, config.ReproductionRules.InitialStateClamping.Min, config.ReproductionRules.InitialStateClamping.Max, 0.0);
            
            if (g0 == null && clips.IsSet("templategenome"))
            {
                var templateGenome = Analysis.LoadGenome(clips.Get("templategenome"));
                g0 = templateGenome.InitialState;
            }

            return g0;
        }

        public static MathNet.Numerics.LinearAlgebra.Matrix<double> ComposeB0(CliParams clips, ModelExecutionContext context, ExperimentConfiguration config)
        {
            // b0
            var b0string = clips.Get("b0string", null);
            var b0 = b0string == null ? null : ExperimentParsing.ParseMatrix(b0string);

            if (b0 == null && clips.IsSet("templategenome"))
            {
                var templateGenome = Analysis.LoadGenome(clips.Get("templategenome"));
                b0 = templateGenome.TransMat;
            }

            if (clips.IsSet("cmtb0"))
            {
                var cmt = (config.Targets[0] as CorrelationMatrixTarget).CorrelationMatrix.Clone();
                b0 = cmt;
            }

            if (clips.IsSet("rndnormalb"))
            {
                var mag = clips.Get("rndnormalb", double.Parse, 1.0);
                var mean = clips.Get("rndnormalbmean", double.Parse, 0.0);
                var nrmMat = MathNet.Numerics.LinearAlgebra.CreateMatrix.Random<double>(config.Size, config.Size, new MathNet.Numerics.Distributions.Normal(mean, mag, context.Rand));

                if (b0 != null)
                    b0.PointwiseMultiply(nrmMat, b0);
                else
                    b0 = nrmMat;
            }

            if (clips.IsSet("rnduniformb"))
            {
                var mag = clips.Get("rnduniformb", double.Parse, 1.0);
                var mean = clips.Get("rnduniformbmean", double.Parse, 0.0);
                var uniformMat = MathNet.Numerics.LinearAlgebra.CreateMatrix.Random<double>(config.Size, config.Size, new MathNet.Numerics.Distributions.ContinuousUniform(mean - mag, mean + mag, context.Rand));

                if (b0 != null)
                    b0.PointwiseMultiply(uniformMat, b0);
                else
                    b0 = uniformMat;
            }

            return b0;
        }

        public static MathNet.Numerics.LinearAlgebra.Vector<double> ComposeBiasVector0(CliParams clips, ModelExecutionContext context, ExperimentConfiguration config)
        {
            // biasVector0
            var biasVector0string = clips.Get("biasVector0", null);

            MathNet.Numerics.LinearAlgebra.Vector<double> biasVector0;
            if (biasVector0string != null && biasVector0string.StartsWith("flat", StringComparison.InvariantCultureIgnoreCase))
            {
                var v = double.Parse(biasVector0string.Substring("flat".Length));
                biasVector0 = MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(config.Size, v);
            }
            else
            {
                biasVector0 = biasVector0string == null ? null : Misc.ParseExtremesVector(biasVector0string, config.ReproductionRules.InitialStateClamping.Min, config.ReproductionRules.InitialStateClamping.Max, 0.0);
            }

            if (biasVector0 == null && clips.IsSet("templategenome"))
            {
                var templateGenome = Analysis.LoadGenome(clips.Get("templategenome"));
                biasVector0 = templateGenome.InitialState;
            }

            if (clips.IsSet("cmtbias"))
            {
                var cmtMul = clips.Get("cmtbias", x => x == null ? 1.0 : double.Parse(x));

                var cmt = (config.Targets[0] as CorrelationMatrixTarget).ForcingVector.Clone();
                if (biasVector0 == null)
                    biasVector0 = cmt * cmtMul;
                else
                    biasVector0 += cmt * cmtMul;
            }

            return biasVector0;
        }
    }

    public class DenseGenomePopulationComposer : IPrefixedPopulationComposer<DenseIndividual>
    {
        public string Prefix => "densegenome:";
        public string Syntax => "filename";
        
        public Population<DenseIndividual> Compose(string filename, TextWriter console, CliParams clips, ModelExecutionContext context, PopulationExperimentConfig<DenseIndividual> populationExperimentConfiguration)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("DenseGenomePopulationComposer parameters include:\n" +
                    " - popSize (default is 1)\n" +
                    " - epigentic");
            }
            
            var popConfig = populationExperimentConfiguration;
            var config = popConfig.ExperimentConfiguration;
            var epigentic = clips.Get("epigentic", bool.Parse);
            
            DenseGenome fileGenome;
            try
            {
                fileGenome = M4M.Analysis.LoadGenome(filename);
            }
            catch (Exception ex)
            {
                throw new Exception("File did not contain a valid DenseGenome", ex);
            }

            DenseIndividual template = DenseIndividual.Develop(fileGenome, context, config.DevelopmentRules, epigentic);

            int popSize = clips.Get("popSize", int.Parse, 1);

            var population = new Population<DenseIndividual>(template, popSize);

            return population;
        }
    }

    public class ExpPopulationComposer<TIndividual> : IPrefixedPopulationComposer<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        public string Prefix => "exp:";
        public string Syntax => "populationtype";
        
        public Population<TIndividual> Compose(string filename, TextWriter console, CliParams clips, ModelExecutionContext context, PopulationExperimentConfig<TIndividual> populationExperimentConfiguration)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("DenseExpPopulationComposer parameters include:\n" +
                    " - popSize (default is to keep the existing population)");
            }
            
            var popConfig = populationExperimentConfiguration;
            var config = popConfig.ExperimentConfiguration;
            
            Population<TIndividual> filePopulation;
            try
            {
                var exp = M4M.State.GraphSerialisation.Read<PopulationExperiment<TIndividual>>(filename);
                filePopulation = exp.Population;
            }
            catch (Exception ex)
            {
                throw new Exception("File did not contain a valid PopulationExperiment of " + typeof(TIndividual).FullName, ex);
            }

            int popSize = clips.Get("popSize", int.Parse, filePopulation.Count);

            var population = new Population<TIndividual>(CliReconfig.TakeLoop(filePopulation.ExtractAll(), popSize));

            return population;
        }
    }
}
