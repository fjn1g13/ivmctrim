﻿using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static M4M.Misc;

namespace M4M.ExperimentComposition
{
    public interface IGenericComposer<T>
    {
        T Compose(string name, TextWriter console, CliParams clips);
    }

    public interface IPrefixed
    {
        string Prefix { get; }
        string Syntax { get; }
    }

    public delegate TResult PrefixedOperation<TPrefixed, TResult>(TPrefixed prefixed, string subName);

    public class Prefixeds<TPrefixed> where TPrefixed : IPrefixed
    {
        private readonly List<TPrefixed> Table;
        
        public Prefixeds(IEnumerable<TPrefixed> prefixeds)
        {
            Table = new List<TPrefixed>(prefixeds);
        }
        
        public Prefixeds()
        {
            Table = new List<TPrefixed>();
        }

        public void Add(TPrefixed prefixed)
        {
            Table.Add(prefixed);
        }

        public void Remove(TPrefixed prefixed)
        {
            Table.Remove(prefixed);
        }
        
        public TResult Perform<TResult>(string name, PrefixedOperation<TPrefixed, TResult> operation)
        {
            if (TryPerform(name, operation, out var result))
            {
                return result;
            }
            else
            {
                throw new Exception($"No composers applicable to '{name}', consider one of the following:\n{StringOfComposers()}");
            }
        }
        
        public bool TryPerform<TResult>(string name, PrefixedOperation<TPrefixed, TResult> operation, out TResult result)
        {
            var prefixed = Table.FirstOrDefault(c => name.StartsWith(c.Prefix));

            if (prefixed == null)
            {
                result = default(TResult);
                return false;
            }
            
            string subName = name.Substring(prefixed.Prefix.Length);
            result = operation(prefixed, subName);
            return true;
        }

        public string StringOfComposers(string prefix = " - ")
        {
            StringBuilder sb = new StringBuilder();

            foreach (var c in Table)
            {
                sb.AppendLine(prefix + c.Prefix + c.Syntax);
            }

            return sb.ToString();
        }
    }

    public interface IGenericPrefixedComposer<T>
    {
        string Prefix { get; }
        string Syntax { get; }
        T Compose(string name, TextWriter console, CliParams clips);
    }

    public class PrefixedComposers<T> : IGenericComposer<T>
    {
        private readonly List<IGenericPrefixedComposer<T>> Composers;
        
        public PrefixedComposers(IEnumerable<IGenericPrefixedComposer<T>> composers)
        {
            Composers = new List<IGenericPrefixedComposer<T>>(composers);
        }
        
        public PrefixedComposers()
        {
            Composers = new List<IGenericPrefixedComposer<T>>();
        }

        public void Add(IGenericPrefixedComposer<T> composer)
        {
            Composers.Add(composer);
        }

        public void Remove(IGenericPrefixedComposer<T> composer)
        {
            Composers.Remove(composer);
        }

        public T Compose(string name, TextWriter console, CliParams clips)
        {
            if (name == null)
                throw new Exception($"No composer indicated; consider one of the following:\n{StringOfComposers()}");

            var composer = Composers.FirstOrDefault(c => name.StartsWith(c.Prefix, StringComparison.InvariantCultureIgnoreCase));

            if (composer == null)
                throw new Exception($"No composers applicable to '{name}'; consider one of the following:\n{StringOfComposers()}");

            return composer.Compose(name.Substring(composer.Prefix.Length), console, clips);
        }

        public bool TryCompose(string name, TextWriter console, CliParams clips, out T result)
        {
            var composer = Composers.FirstOrDefault(c => name.StartsWith(c.Prefix));

            if (composer == null)
            {
                result = default(T);
                return false;
            }
            
            result = composer.Compose(name.Substring(composer.Prefix.Length), console, clips);
            return true;
        }

        public string StringOfComposers(string prefix = " - ")
        {
            StringBuilder sb = new StringBuilder();

            foreach (var c in Composers)
            {
                sb.AppendLine(prefix + c.Prefix + c.Syntax);
            }

            return sb.ToString();
        }
    }

    public delegate T GenericComposerDelegate<T>(string name, TextWriter console, CliParams clips);

    public class DelegatePrefixedComposer<T> : IGenericPrefixedComposer<T>
    {
        public GenericComposerDelegate<T> Delegate { get; }
        public string Prefix { get; }
        public string Syntax { get; }

        public DelegatePrefixedComposer(GenericComposerDelegate<T> @delegate, string prefix, string syntax)
        {
            Delegate = @delegate;
            Prefix = prefix;
            Syntax = syntax;
        }

        public T Compose(string name, TextWriter console, CliParams clips)
        {
            return Delegate(name, console, clips);
        }
    }

    public interface IPopulationExperimentConfig<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        IPopulationExperimentConfig<TIndividual> Compose(string topdir, CliParams clips);
    }

    public interface IExperimentComposer<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        PopulationExperiment<TIndividual> Compose(string topdir, TextWriter console, CliParams clips);
    }

    public class DefaultExperimentComposer<TIndividual, TGenome> : IExperimentComposer<TIndividual> where TIndividual : IIndividual<TIndividual> where TGenome : IGenome<TGenome>
    {
        public DefaultExperimentComposer(ITargetPackageComposer targetPackageComposer, IExperimentConfigurationComposer experimentConfigurationComposer, IPopulationExperimentConfigurationComposer<TIndividual> populationExperimentConfigurationComposer, IPopulationComposer<TIndividual> populationComposer)
        {
            TargetPackageComposer = targetPackageComposer;
            ExperimentConfigurationComposer = experimentConfigurationComposer;
            PopulationExperimentConfigurationComposer = populationExperimentConfigurationComposer;
            PopulationComposer = populationComposer;
        }

        public ITargetPackageComposer TargetPackageComposer { get; set; }
        public IExperimentConfigurationComposer ExperimentConfigurationComposer { get; set; }
        public IPopulationExperimentConfigurationComposer<TIndividual> PopulationExperimentConfigurationComposer { get; set; }
        public IPopulationComposer<TIndividual> PopulationComposer { get; set; }

        public PopulationExperiment<TIndividual> Compose(string topdir, TextWriter console, CliParams clips)
        {
            //
            // create ModelExecutionContext
            //

            int? seed = clips.Get("seed", s => int.Parse(s), (int?)null);
            var rand = seed.HasValue ? new MersenneTwister(seed.Value, false) : new MersenneTwister(false);
            var context = new ModelExecutionContext(rand);
            
            //
            // compose everything
            //

            var targetPackage = TargetPackageComposer.Compose(console, clips);
            var config = ExperimentConfigurationComposer.Compose(clips, console, targetPackage);
            var popConfig = PopulationExperimentConfigurationComposer.Compose(console, clips, config); 
            var pop = PopulationComposer.Compose(console, clips, context, popConfig);

            //
            // build path
            //
            
            string name = clips.Get("name");
            bool appendTimestamp = clips.IsSet("appendTimestamp");
            topdir = System.IO.Path.Combine(topdir, name);
            
            //
            // prepare the experiment, and return (no IO)
            //
            
            var popExperiment = PopulationExperimentRunners.PrepareExperiment<TIndividual>(pop, popConfig, topdir, "", name, true, appendTimestamp);
            return popExperiment;
        }

        public Action<TextWriter, string, CliParams> Gen => (console, topdir, clips) =>
            {
                // might need these
                int? ioSeed = clips.Get("ioSeed", s => int.Parse(s), (int?)null);
                var ioRand = ioSeed.HasValue ? new MersenneTwister(ioSeed.Value, false) : new MersenneTwister(false);

                // compose
                var popExperiment = Compose(topdir, console, clips);

                // save
                popExperiment.FileStuff.CreateDir();
                popExperiment.Save("starter", false);

                // write out other
                if (!clips.IsSet("noinitials"))
                {
                    PopulationExperimentRunners.WriteOutInitials(ioRand, popExperiment); // mosty for config.txt, so that a human/GradStudent can look it over
                }
            };
    }

    public static class Typical
    {
        public static ITargetPackageComposer PrepareDefaultTargetPackageComposer()
        {
            var btpc = new BasicTargetPackageComposer(new IGenericPrefixedComposer<ITargetPackage>[]
            {
                new ExpTargetPackageComposer(),
                new IvmcTargetPackageComposer(),
                new SimpleStackedTargetPackageComposer(),
                new CorrelationMatrixTargetPackageComposer(),
                new McTargetPackageComposer(),
                new ClassicTargetPackageComposer(ClassicTargetPackageComposer.DefaultCustomJudgerPrepares),
                new BinaryPuzzleTargetPackageComposer(),
            }, null);

            var ftpc = new FileTargetPackageComposer(btpc);
            btpc.PrefixedComposers.Add(ftpc);

            return btpc;
        }

        public static readonly DefaultExperimentComposer<DenseIndividual, DenseGenome> DefaultDenseExperimentComposer = PrepareDefaultDenseExperimentComposer();

        public static DefaultExperimentComposer<DenseIndividual, DenseGenome> PrepareDefaultDenseExperimentComposer() => new DefaultExperimentComposer<DenseIndividual, DenseGenome>(
                PrepareDefaultTargetPackageComposer(),
                new BasicExperimentConfigurationComposer<DenseGenome>(),
                new BasicDensePopulationExperimentConfigurationComposer(new IGenericPrefixedComposer<IPopulationSpinner<DenseIndividual>> []
                    {
                        new DefaultPopulationSpinnerComposer<DenseIndividual>(),
                        new DensePopulationSpinnerComposer()
                    }, "default:default"),
                new BasicPopulationComposer<DenseIndividual>(new IPrefixedPopulationComposer<DenseIndividual>[] { new BasicDensePopulationComposer(), new DenseGenomePopulationComposer(), new ExpPopulationComposer<DenseIndividual>() }, "dense:hillclimber")
            );
    }
}
