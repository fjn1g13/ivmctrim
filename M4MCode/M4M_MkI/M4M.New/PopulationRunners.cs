﻿using M4M.Epistatics;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static M4M.Misc;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public static class LowRunners
    {
        // hopefully this will make repeat seeds most unlikely when we run the same experiment at the same time
        private static readonly Random SeedProvider = new Random();

        private static int GrabSeed()
        {
            lock (SeedProvider)
            {
                return SeedProvider.Next();
            }
        }

        public static void RunExperiment(TextWriter console, string cliPrefix, string topdir, PopulationExperiment<DenseIndividual> experiment, int? seed = null, DensePopulationExperimentFeedbackFactory densePopulationExperimentFactory = null, DensePopulationExperimentFeedbackConfigurator densePopulationExperimentFeedbackConfigurator = null, bool appendTimestamp = true, int savePeriodEpochs = 500, int savePeriodSeconds = 60*60)
        {
            experiment = PopulationExperimentRunners.PrepareExperiment<DenseIndividual>(experiment.Population.Clone(), experiment.PopulationConfig, topdir, appendTimestamp: appendTimestamp);
            RunExperiment(console, cliPrefix, experiment, seed, densePopulationExperimentFactory, densePopulationExperimentFeedbackConfigurator, savePeriodEpochs, savePeriodSeconds);
        }

        public static void RunExperiment(TextWriter console, string cliPrefix, PopulationExperiment<DenseIndividual> experiment, int? seed = null, DensePopulationExperimentFeedbackFactory densePopulationExperimentFactory = null, DensePopulationExperimentFeedbackConfigurator densePopulationExperimentFeedbackConfigurator = null, int savePeriodEpochs = 500, int savePeriodSeconds = 60*60)
        {
            var rand = seed == null ? new CustomMersenneTwister(GrabSeed()) : new CustomMersenneTwister(seed.Value);
            var ioRand = seed == null ? new CustomMersenneTwister(GrabSeed()) : new CustomMersenneTwister(seed.Value);
            var context = new ModelExecutionContext(rand);

            var feedback = densePopulationExperimentFactory != null ? densePopulationExperimentFactory(ioRand, experiment.PopulationConfig, null) : null;
            densePopulationExperimentFeedbackConfigurator?.Invoke(feedback);

            PopulationExperimentRunners.WriteOutInitials(ioRand, experiment); // I've been using rand in various places: this is not good
            experiment.Save("start", false);
            PopulationExperimentRunners.RunEpochs(console, cliPrefix, experiment.PopulationConfig.ExperimentConfiguration.Epochs, context, experiment, feedback?.Feedback, savePeriodEpochs, savePeriodSeconds);
        }

        public static void ContinueExperiment(TextWriter console, string cliPrefix, string topdir, PopulationExperiment<DenseIndividual> experiment, int? seed = null, DensePopulationExperimentFeedbackFactory densePopulationExperimentFactory = null, DensePopulationExperimentFeedbackConfigurator densePopulationExperimentFeedbackConfigurator = null, bool appendTimestamp = true, int savePeriodEpochs = 500, int savePeriodSeconds = 60*60)
        {
            experiment = PopulationExperimentRunners.PrepareExperiment<DenseIndividual>(experiment.Population.Clone(), experiment.PopulationConfig, topdir, experiment.Epoch, experiment.TotalGenerationCount, appendTimestamp: appendTimestamp);
            ContinueExperiment(console, cliPrefix, experiment, seed, densePopulationExperimentFactory, densePopulationExperimentFeedbackConfigurator, savePeriodEpochs, savePeriodSeconds);
        }

        public static void ContinueExperiment(TextWriter console, string cliPrefix, PopulationExperiment<DenseIndividual> experiment, int? seed = null, DensePopulationExperimentFeedbackFactory densePopulationExperimentFactory = null, DensePopulationExperimentFeedbackConfigurator densePopulationExperimentFeedbackConfigurator = null, int savePeriodEpochs = 500, int savePeriodSeconds = 60*60)
        {
            int epochsLeft = experiment.PopulationConfig.ExperimentConfiguration.Epochs - experiment.Epoch;

            var rand = seed == null ? new CustomMersenneTwister(GrabSeed()) : new CustomMersenneTwister(seed.Value);
            var ioRand = seed == null ? new CustomMersenneTwister(GrabSeed()) : new CustomMersenneTwister(seed.Value);
            var context = new ModelExecutionContext(rand);

            var feedback = densePopulationExperimentFactory != null ? densePopulationExperimentFactory(ioRand, experiment.PopulationConfig, null) : null;
            densePopulationExperimentFeedbackConfigurator?.Invoke(feedback);
            
            PopulationExperimentRunners.RunEpochs(console, cliPrefix, epochsLeft, context, experiment, feedback?.Feedback, savePeriodEpochs, savePeriodSeconds);
        }
    }
}
