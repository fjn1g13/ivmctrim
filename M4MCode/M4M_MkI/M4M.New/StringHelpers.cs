﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M4M
{
    // why am I doing this?
    public struct CodePoint
    {
        /// <summary>
        /// Prepares a CodePoint from a pair from a High and Low surrogate
        /// </summary>
        /// <param name="highWord">The High surrogate</param>
        /// <param name="lowWord">The Low surrogate</param>
        public CodePoint(char highWord, char lowWord) : this()
        {
            HighWord = highWord;
            LowWord = lowWord;

            if (!char.IsSurrogatePair(highWord, lowWord))
                throw new Exception("Char pair is not a surrogate pair");
        }

        /// <summary>
        /// Prepares a CodePoint from a single-word character
        /// </summary>
        public CodePoint(char soloWord) : this()
        {
            HighWord = soloWord;
            LowWord = default(char);

            if (char.IsHighSurrogate(soloWord) || char.IsLowSurrogate(soloWord))
                throw new Exception("Character is part of a surrogate pair");
        }

        /// <summary>
        /// The High-word of a Surrogate Pair, or the single character of a Solo CodeWord.
        /// </summary>
        private readonly char HighWord;

        /// <summary>
        /// The Low-word of a Surrogate Pair, or else a default value.
        /// </summary>
        private readonly char LowWord;

        /// <summary>
        /// Returns whether the CodePoint represents a Surrogate Pair
        /// </summary>
        public bool IsSurrogatePair => char.IsHighSurrogate(HighWord);

        /// <summary>
        /// Enumerates the CodePoints is the given string
        /// </summary>
        public static IEnumerable<CodePoint> EnumerateCodePoints(string str)
        {
            int pos = 0;
            while (NextCodePoint(str, ref pos, out CodePoint cp))
                yield return cp;
        }

        /// <summary>
        /// Determines the next CodePoint in the given string
        /// Returns true if there is such a CodePoint; returns false if positioned at the end of the string
        /// </summary>
        /// <param name="str">The string whence to extract the CodePoint</param>
        /// <param name="position">The position in the string that is the start of the CodePoint (modified by the callee)</param>
        /// <param name="codePoint">The CodePoint at the given position, or a default value if the end of the string has been reached</param>
        /// <returns></returns>
        public static bool NextCodePoint(string str, ref int position, out CodePoint codePoint)
        {
            if (position == str.Length)
            {
                codePoint = default(CodePoint);
                return false;
            }
            
            codePoint = CodePointAtInternal(str, ref position);
            return true;
        }

        /// <summary>
        /// Retrieves the code-point at the given position in the given string
        /// </summary>
        /// <param name="str">The string whence to extract the CodePoint</param>
        /// <param name="position">The position in the string that is the start of the CodePoint</param>
        public static CodePoint CodePointAt(string str, int position)
        {
            return CodePointAtInternal(str, ref position);
        }

        /// <summary>
        /// Retrieves the code-point at the given position in the given string
        /// Increments the position according to the size of the CodePoint (position is unchanged if an exception is thrown)
        /// </summary>
        /// <param name="str">The string whence to extract the CodePoint</param>
        /// <param name="pos">The position in the string that is the start of the CodePoint</param>
        private static CodePoint CodePointAtInternal(string str, ref int pos)
        {
            if (pos < 0 || pos >= str.Length)
                throw new ArgumentOutOfRangeException("Position is outside of the string");

            if (char.IsHighSurrogate(str, pos))
            {
                if (pos + 1 >= str.Length)
                    throw new ArgumentOutOfRangeException("Position contains a High surrogate, but is the last element of the string");

                return new CodePoint(str[pos++], str[pos++]);
            }
            else if (char.IsLowSurrogate(str, pos))
            {
                throw new ArgumentException("Position refers to a Low surrogate");
            }
            else
            {
                return new CodePoint(str[pos++]);
            }
        }

        /// <summary>
        /// Converts the CodePoint into a String
        /// </summary>
        public override string ToString()
        {
            if (IsSurrogatePair)
                return "" + HighWord + LowWord;
            else
                return "" + HighWord;
        }

        /// <summary>
        /// Appends the CodeWord to the given StringBuilder
        /// </summary>
        public void AppendTo(StringBuilder sb)
        {
            if (IsSurrogatePair)
            {
                sb.Append(HighWord);
                sb.Append(LowWord);
            }
            else
            {
                sb.Append(HighWord);
            }
        }
        
        public static bool operator ==(CodePoint l, CodePoint r)
        {
            return l.HighWord == r.HighWord && l.LowWord == r.LowWord;
        }

        public static bool operator !=(CodePoint l, CodePoint r)
        {
            return l.HighWord != r.HighWord || l.LowWord != r.LowWord;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is CodePoint))
            {
                return false;
            }

            var point = (CodePoint)obj;
            return HighWord == point.HighWord &&
                   LowWord == point.LowWord;
        }

        public override int GetHashCode()
        {
            var hashCode = -254445490;
            hashCode = hashCode * -1521134295 + HighWord.GetHashCode();
            hashCode = hashCode * -1521134295 + LowWord.GetHashCode();
            return hashCode;
        }

        public static implicit operator CodePoint(char c)
        {
            return new CodePoint(c);
        }
    }

    public static class StringHelpers
    {
        /// <summary>
        /// Compares two strings with StringComparison.InvariantCultureIgnoreCase. 
        /// </summary>
        public static bool SoftEquals(this string str, string other)
        {
            return string.Equals(str, other, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// The default delimiters to be used when Splitting and Joining strings.
        /// </summary>
        public static readonly IReadOnlyList<char> DefaultDelimiters = new char[] { ' ', '\t' };

        /// <summary>
        /// HashSet of CodePoints mirroring the entries in <see cref="DefaultDelimiters"/>
        /// </summary>
        private static readonly HashSet<CodePoint> DefaultCodePointDelimiters = new HashSet<CodePoint>(DefaultDelimiters.Select(c => (CodePoint)c));

        /// <summary>
        /// The default quote single to be used when Spliting and Joining strings.
        /// </summary>
        public static readonly char DefaultQuote = '"';

        /// <summary>
        /// The default quote single to be used when Spliting and Joining strings.
        /// </summary>
        public static readonly char DefaultEscape = '\\';

        /// <summary>
        /// Splits the given string uses the default delimiters, quote, and escape characters.
        /// </summary>
        public static IEnumerable<string> Split(this string str)
        {
            return Split(str, DefaultCodePointDelimiters, DefaultQuote, DefaultEscape);
        }
        
        /// <summary>
        /// Splits the given string using the given delimiter, quote, and escape CodePoints.
        /// </summary>
        public static IEnumerable<string> Split(this string str, IEnumerable<CodePoint> delimiters, CodePoint quote, CodePoint escape)
        {
            HashSet<CodePoint> delims = new HashSet<CodePoint>(delimiters);
            return Split(str, delims, quote, escape);
        }
        
        /// <summary>
        /// Splits the given string using the given delimiter, quote, and escape CodePoints.
        /// </summary>
        public static IEnumerable<string> Split(this string str, HashSet<CodePoint> delimiters, CodePoint quote, CodePoint escape)
        {
            // why did I write this? it's not going to be much more efficient than using strings, and all it gives you is some confidence that you arn't splitting on something daft

            int pos = 0;
            CodePoint c;
            bool escapeNext = false;
            bool inQuote = false;

            StringBuilder sb = new StringBuilder();
            while (CodePoint.NextCodePoint(str, ref pos, out c))
            {
                if (escapeNext)
                {
                    c.AppendTo(sb);
                    
                    escapeNext = false;
                    continue;
                }

                if (c == escape)
                {
                    escapeNext = true;
                    continue;
                }

                if (c == quote)
                {
                    inQuote = !inQuote;
                    continue;
                }

                if (!inQuote && delimiters.Contains(c))
                {
                    yield return sb.ToString();
                    sb.Clear();
                    continue;
                }

                c.AppendTo(sb);
            }

            if (sb.Length > 0)
                yield return sb.ToString();
        }

        /// <summary>
        /// Escapes quotes and escapes in the given string.
        /// </summary>
        public static string Escape(string str, CodePoint quote, CodePoint escape)
        {
            int pos = 0;
            CodePoint c;

            bool noChange = true;
            StringBuilder sb = new StringBuilder();
            while (CodePoint.NextCodePoint(str, ref pos, out c))
            {
                if (c == escape || c == quote)
                {
                    escape.AppendTo(sb);
                    noChange = false;
                }

                c.AppendTo(sb);
            }

            if (noChange)
                return str;
            else
                return sb.ToString();
        }

        /// <summary>
        /// Joins the given strings with the default delimiter, escaping quote and escape codepoints.
        /// </summary>
        public static string Join(this IEnumerable<string> strs)
        {
            return Join(strs, DefaultDelimiters[0], DefaultQuote, DefaultEscape);
        }

        /// <summary>
        /// Joins the given strings with the given delimiter, escaping quote and escape codepoints.
        /// </summary>
        public static string Join(this IEnumerable<string> strs, CodePoint delimiter, CodePoint quote, CodePoint escape)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var str in strs)
            {
                if (sb.Length > 0)
                    sb.Append(delimiter);
                
                var escaped = Escape(str, quote, escape);
                sb.Append(escaped);
            }

            return sb.ToString();
        }
    }
}
