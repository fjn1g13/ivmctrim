﻿using M4M.State;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static M4M.Analysis;

namespace M4M
{
    /// <summary>
    /// Does sampling with a given period, and allows some automatic file-output stuff (genomes and rcses and more)
    /// </summary>
    public class CommonDensePopulationExperimentFeedback : IDensePopulationExperimentFeedback
    {
        public static DensePopulationExperimentFeedbackFactory SimpleCommonDensePopulationExperimentFeedback(TextWriter console, bool disableTransientIO = false, bool disableAllIO = false, int sampleMax = 2000, int savePeriod = 1000, int plotPeriod = 2000, int wholeSamplePeriod = 10, int wholeSampleWritePeriod = 2000, int tracePeriod = 0, int traceDuration = 1, int traceSamplePeriod = 1, bool rememberTraces = false, bool noRcs = false)
        {
            return (rand, popConfig, feedback) => new CommonDensePopulationExperimentFeedback(console, rand, popConfig, disableTransientIO, disableAllIO, savePeriod, plotPeriod, feedback, Math.Max(1, popConfig.ExperimentConfiguration.Epochs / sampleMax), wholeSamplePeriod, wholeSampleWritePeriod, tracePeriod, traceDuration, traceSamplePeriod, rememberTraces, noRcs);
        }

        public PopulationExperimentConfig<DenseIndividual> PopulationConfig { get; }
        public ExperimentConfiguration Config { get; }

        /// <summary>
        /// The PopulationExperimentFeedback this instance uses
        /// </summary>
        public PopulationExperimentFeedback<DenseIndividual> Feedback { get; }

        protected List<int> _sampleEpochs { get; }  = new List<int>();
        protected List<double> _fitness { get; } = new List<double>();
        protected List<IndividualJudgement<DenseIndividual>> _bestSamples { get; } = new List<IndividualJudgement<DenseIndividual>>();
        
        protected List<WholeSample<DenseIndividual>> _wholeSamples { get; } = new List<WholeSample<DenseIndividual>>();

        protected List<TraceInfo<DenseIndividual>> _traces { get; } = new List<TraceInfo<DenseIndividual>>();

        public IReadOnlyList<IndividualJudgement<DenseIndividual>> BestSamples => _bestSamples;
        public IReadOnlyList<int> SampleEpochs => _sampleEpochs;

        public int SamplePeriod { get; }
        public int WholeSamplePeriod { get; }
        public int TraceSamplePeriod { get; }
        public bool NoRcs { get; }
        protected int N { get; }

        public CommonDensePopulationExperimentFeedback(TextWriter console, RandomSource rand, PopulationExperimentConfig<DenseIndividual> populationConfig, bool disableTransientIO, bool disableAllIO, int genomeSavePeriod, int partialPlotPeriod = 100, PopulationExperimentFeedback<DenseIndividual> feedback = null, int samplePeriod = 1, int wholeSamplePeriod = 10, int wholeSampleWritePeriod = 2000, int tracePeriod = 0, int traceDuration = 1, int traceSamplePeriod = 1, bool rememberTraces = false, bool noRcs = false)
        {
            disableTransientIO = disableTransientIO | disableAllIO;

            // if not set, make a new empty one
            Feedback = feedback ?? new PopulationExperimentFeedback<DenseIndividual>();
            
            ITarget lastTarget = null;
            
            PopulationConfig = populationConfig;
            Config = populationConfig.ExperimentConfiguration;
            N = Config.Size;
            SamplePeriod = samplePeriod;
            WholeSamplePeriod = wholeSamplePeriod;
            TraceSamplePeriod = traceSamplePeriod;
            NoRcs = noRcs;

            PopulationTraceRecorder<DenseIndividual> currentTraceRecorder = null;
            int traceAge = -1;
            void startTrace()
            {
                currentTraceRecorder = new PopulationTraceRecorder<DenseIndividual>(TraceSamplePeriod);
                traceAge = 0;
                Feedback.Judged.Register(currentTraceRecorder.Recorder);
            }

            void endTrace(FileStuff stuff, int endEpoch)
            {
                if (currentTraceRecorder != null)
                {
                    // save
                    var traceInfo = new TraceInfo<DenseIndividual>(currentTraceRecorder.TraceWholeSamples);

                    if (rememberTraces)
                        _traces.Add(traceInfo);

                    SaveTrace(stuff, "e" + endEpoch, traceInfo);

                    // remove trace
                    Feedback.Judged.Unregister(currentTraceRecorder.Recorder);
                    currentTraceRecorder = null;
                    traceAge = -1;
                }
            }

            void sample(FileStuff stuff, Population<DenseIndividual> population, int epoch, IReadOnlyList<IndividualJudgement<DenseIndividual>> opj)
            {
                if (epoch % SamplePeriod == 0)
                {
                    _sampleEpochs.Add(epoch);

                    if (opj != null)
                    {
                        _fitness.Add(opj.Average((IndividualJudgement<DenseIndividual> ij) => ij.Judgement.CombinedFitness));
                        _bestSamples.Add(opj.ArgMax((IndividualJudgement<DenseIndividual> ij) => ij.Judgement.CombinedFitness));
                    }
                }

                if (epoch % genomeSavePeriod == 0 || epoch == populationConfig.ExperimentConfiguration.Epochs)
                {
                    DenseIndividual di = population.PeekRandom(rand);
                    DenseGenome g = di.Genome;
                    Phenotype p = di.Phenotype;

                    // save genome (this is transient..., but it's important... and relatively cheap (and no nil crossover, etc.))
                    if (!disableAllIO)
                    {
                        SaveGenome(stuff.File("genome" + epoch + ".dat"), g);
                    }
                }
                
                if (epoch > 0 && epoch % partialPlotPeriod == 0)
                {
                    if (!disableTransientIO)
                    {
                        if (!NoRcs)
                            SaveRcs(stuff, epoch + "(best)");
                        SaveIst(stuff, epoch + "(best)");
                        SavePst(stuff, epoch + "(best)");
                        SaveFitness(stuff, epoch + "(best)");
                    }
                }
            }
            
            void wholeSample(FileStuff stuff, long generationCount, int epoch, ITarget target, IReadOnlyList<IndividualJudgement<DenseIndividual>> opj)
            {
                if (WholeSamplePeriod > 0)
                {
                    if (epoch % WholeSamplePeriod == 0)
                    {
                        _wholeSamples.Add(new WholeSample<DenseIndividual>(generationCount, epoch, target, opj));
                    }
                }
            }

            Feedback.EndTarget.Register((stuff, population, target, epoch, generationCount, oldPopulationJudgements) =>
            {
                // record last Target
                lastTarget = target;
                
                wholeSample(stuff, generationCount, epoch, target, oldPopulationJudgements);
            });
            
            Feedback.Started.Register((stuff, population) =>
            {
                // TODO: this doesn't make a whole lot of sense... infact, we probably ought to be using our own target for all samples, so that we know it is consistent...
                // ... but there is something nice about using the target we 'just saw', only such a target doesn't exist here
                var epoch0target = Config.Targets[Config.Targets.Count - 1];
                epoch0target.NextGeneration(rand, Config.JudgementRules);

                var opj = population.PeekAndJudgeAll(Config.JudgementRules, epoch0target);
                sample(stuff, population, 0, opj);
                wholeSample(stuff, 0, 0, epoch0target, opj);

                // trace the first ones, if we are tracing at all
                if (tracePeriod > 0)
                {
                    startTrace();
                }
            });

            // feedback
            Feedback.EndEpoch.Register((stuff, population, epoch, generationCount, opj) =>
            {
                sample(stuff, population, epoch, opj);
                   
                if (wholeSamplePeriod > 0 && wholeSampleWritePeriod > 0 &&
                    epoch > 0 && epoch % wholeSampleWritePeriod == 0)
                {
                    SaveWholeSamples(stuff, "e" + epoch.ToString());
                    _wholeSamples.Clear();
                }
                
                if (traceAge >= 0)
                    traceAge++;
                if (traceAge >= traceDuration)
                    endTrace(stuff, epoch); // end a running trace ...
                if (tracePeriod > 0 && (traceAge == -1 && ((epoch + traceDuration) % tracePeriod == 0 || epoch == Config.Epochs - traceDuration))) // ... and start a new one if we should
                {
                    startTrace();
                }
            });

            Feedback.Finished.Register((stuff, population, epoch, generationCount) =>
            {
                console.WriteLine("Finished run; Collating results...");

                if (!disableAllIO) // we usually want the terminals
                {
                    DenseIndividual di = population.PeekRandom(rand);
                    DenseGenome g = di.Genome;
                    Phenotype p = di.Phenotype;
                    
                    // save genome & rcs
                    SaveGenome(stuff.File("terminalgenome.dat"), g); // backwards compatibility
                    SaveGenome(stuff.File("genome_t.dat"), g); // (prefered)
                    if (!NoRcs)
                        SaveRcs(stuff, "_t(best)");
                    SaveIst(stuff, "_t(best)");
                    SavePst(stuff, "_t(best)");
                    SaveFitness(stuff, "_t(best)");

                    if (WholeSamplePeriod > 0 && _wholeSamples.Count > 0)
                    {
                        SaveWholeSamples(stuff, "e" + epoch.ToString());
                        _wholeSamples.Clear();
                    }
                }
            });
        }

        public double[][] GetRcsData()
        {
            return Enumerable.Range(0, N * N).Select(i => _bestSamples.Select(ij => ij.Individual.Genome.TransMat[i / N, i % N]).ToArray()).ToArray();
        }
        
        public void SaveRcs(FileStuff stuff, string name)
        {
            double[][] rcs = GetRcsData();
            SaveTrajectories(stuff.File("rcs" + name + ".dat"), rcs, SamplePeriod);
        }

        public double[][] GetIstData()
        {
            return Enumerable.Range(0, N).Select(i => _bestSamples.Select(ij => ij.Individual.Genome.InitialState[i]).ToArray()).ToArray();
        }

        public double[][] GetPstData()
        {
            return Enumerable.Range(0, N).Select(i => _bestSamples.Select(ij => ij.Individual.Phenotype[i]).ToArray()).ToArray();
        }
        
        public void SaveIst(FileStuff stuff, string name)
        {
            double[][] ist = GetIstData();
            SaveTrajectories(stuff.File("ist" + name + ".dat"), ist, SamplePeriod);
        }
        
        public void SavePst(FileStuff stuff, string name)
        {
            double[][] pst = GetPstData();
            SaveTrajectories(stuff.File("pst" + name + ".dat"), pst, SamplePeriod);
        }

        /// <summary>
        /// f, b, c*lambda
        /// </summary>
        /// <returns></returns>
        public double[][] GetFitnessData()
        {
            // TODO: maybe add trajectories for every target? (does that make any sense? (could be optional))
            return new[] {
                _bestSamples.Select(ij => ij.Judgement.CombinedFitness).ToArray(),
                _bestSamples.Select(ij => ij.Judgement.Benefit).ToArray(),
                _bestSamples.Select(ij => ij.Judgement.CombinedFitness - ij.Judgement.Benefit).ToArray() // cost * lambda
                };
        }
        
        public void SaveFitness(FileStuff stuff, string name)
        {
            double[][] fitness = GetFitnessData();
            SaveTrajectories(stuff.File("fitness" + name + ".dat"), fitness, SamplePeriod);
        }
        
        public void SaveWholeSamples(FileStuff stuff, string name)
        {
            using (var fs = stuff.CreateBin("wholesamples" + name + ".dat"))
            {
                GraphSerialisation.Write(_wholeSamples, fs);
            }
        }

        public static double[][] ExtractRcs(IEnumerable<WholeSample<DenseIndividual>> wholeSamples, int N, int samplePeriod)
        {
            return ExtractTrajectories(wholeSamples, N * N, ws => ws.Generations % samplePeriod == 0, (ws, i) => ws.Judgements[0].Individual.Genome.TransMat[i / N, i % N]);
        }

        public static double[][] ExtractTrajectories<T>(IEnumerable<WholeSample<T>> wholeSamples, int count, Predicate<WholeSample<T>> filter, Func<WholeSample<T>, int, double> extractor) where T : IIndividual<T>
        {
            List<double>[] trajectories = new List<double>[count];

            for (int i = 0; i < count; i++)
                trajectories[i] = new List<double>();

            foreach (var ws in wholeSamples)
            {
                if (filter(ws))
                {
                    for (int i = 0; i < count; i++)
                        trajectories[i].Add(extractor(ws, i));
                }
            }

            return trajectories.Select(t => t.ToArray()).ToArray();
        }

        public static IReadOnlyList<string> DetectWholeSamples(string dir)
        {
            string trim(string path) => System.IO.Path.GetFileName(path);

            var filenames = System.IO.Directory.GetFiles(dir);
            var samplefiles = filenames.Where(fname => trim(fname).StartsWith("wholesamplese") && trim(fname).EndsWith(".dat"));
            var sortedsamplefiles = samplefiles.OrderBy(fname => int.Parse(trim(fname).Substring("wholesamplese".Length, trim(fname).Length - "wholesamplese.dat".Length)));

            return sortedsamplefiles.ToArray();
        }
        
        public void SaveTrace<T>(FileStuff stuff, string name, TraceInfo<T> traceInfo) where T : IIndividual<T>
        {
            traceInfo.Save(stuff.File("trace" + name + ".dat"));
        }
    }

    public class WholeSampleFeedback<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        public WholeSampleFeedback(PopulationExperimentFeedback<TIndividual> feedback, int wholeSamplePeriod, bool allowEarlyAccess = false)
        {
            Feedback = feedback ?? throw new ArgumentNullException(nameof(feedback));
            WholeSamplePeriod = wholeSamplePeriod;
            AllowEarlyAccess = allowEarlyAccess;
            Finished = false;

            Init();
        }

        /// <summary>
        /// The PopulationExperimentFeedback this instance uses
        /// </summary>
        public PopulationExperimentFeedback<TIndividual> Feedback { get; }
        public int WholeSamplePeriod { get; }
        public bool AllowEarlyAccess { get; }
        public bool Finished { get; set; }

        private List<WholeSample<TIndividual>> _wholeSamples { get; } = new List<WholeSample<TIndividual>>();
        public List<WholeSample<TIndividual>> WholeSamples
        {
            get
            {
                if (!Finished && !AllowEarlyAccess)
                    throw new InvalidOperationException("Cannot access the unfinished wholesamples");

                return _wholeSamples;
            }
        }

        public void FinishEarly()
        {
            Finished = true;
        }

        private void Init()
        {
            // only sample end-of-epoch
            Feedback.EndTarget.Register((stuff, population, target, epoch, generationCount, oldPopulationJudgements) =>
            {
                if (Finished)
                    throw new InvalidOperationException("Cannot reuse an instance of " + nameof(WholeSampleFeedback<TIndividual>));

                SampleWholeSample(generationCount, epoch, target, oldPopulationJudgements);
            });

            Feedback.Finished.Register((stuff, population, epoch, generationCount) =>
            {
                if (Finished)
                    throw new InvalidOperationException("Cannot reuse an instance of " + nameof(WholeSampleFeedback<TIndividual>));

                Finished = true;
            });
        }

        private void SampleWholeSample(long generationCount, int epoch, ITarget target, IReadOnlyList<IndividualJudgement<TIndividual>> opj)
        {
            if (WholeSamplePeriod > 0)
            {
                if (epoch % WholeSamplePeriod == 0)
                {
                    _wholeSamples.Add(new WholeSample<TIndividual>(generationCount, epoch, target, opj));
                }
            }
        }
    }

    public class ProjectedFitnessFeedback<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        public ProjectedFitnessFeedback(PopulationExperimentFeedback<TIndividual> feedback, IReadOnlyList<ITarget> targets, JudgementRules jrules, string postfix, int samplePeriod, int plotPeriod)
        {
            Feedback = feedback ?? throw new ArgumentNullException(nameof(feedback));
            Targets = targets;
            JudgementRules = jrules;
            Postfix = postfix;
            SamplePeriod = samplePeriod;
            PlotPeriod = plotPeriod;
            _samples = Enumerable.Range(0, targets.Count * 3).Select(_ => new List<double>()).ToArray();

            Init();
        }

        /// <summary>
        /// The PopulationExperimentFeedback this instance uses
        /// </summary>
        public PopulationExperimentFeedback<TIndividual> Feedback { get; }

        /// <summary>
        /// The targest this instance will use
        /// </summary>
        public IReadOnlyList<ITarget> Targets { get; }

        public JudgementRules JudgementRules { get; }
        public string Postfix { get; }
        public int SamplePeriod { get; }
        public int PlotPeriod { get; }
        public bool AllowEarlyAccess { get; }
        public bool Finished { get; set; }

        private IReadOnlyList<List<double>> _samples { get; }
        public IReadOnlyList<IReadOnlyList<double>> Samples
        {
            get
            {
                return _samples;
            }
        }

        private void Init()
        {
            // only sample end-of-epoch
            Feedback.EndTarget.Register((stuff, population, target, epoch, generationCount, oldPopulationJudgements) =>
            {
                SampleSample(generationCount, epoch, target, oldPopulationJudgements);
                if (PlotPeriod > 0 && epoch % PlotPeriod == 0)
                    SaveFitness(stuff, epoch + "(best)");
            });

            Feedback.Finished.Register((stuff, population, epoch, generationCount) =>
            {
                Finished = true;
                SaveFitness(stuff, "_t(best)");
            });
        }

        private void SampleSample(long generationCount, int epoch, ITarget oldTarget, IReadOnlyList<IndividualJudgement<TIndividual>> opj)
        {
            if (SamplePeriod > 0)
            {
                if (epoch % SamplePeriod == 0)
                {
                    for (int ti = 0; ti < Targets.Count; ti++)
                    {
                        var target = Targets[ti];
                        var s = opj.Select(ij => ij.Individual.Judge(JudgementRules, target)).ArgMax(_s => _s.CombinedFitness);
                        _samples[ti + 0 * Targets.Count].Add(s.CombinedFitness);
                        _samples[ti + 1 * Targets.Count].Add(s.Benefit);
                        _samples[ti + 2 * Targets.Count].Add(s.Cost);
                    }
                }
            }
        }

        public void SaveFitness(FileStuff stuff, string name)
        {
            double[][] fitness = _samples.Select(t => t.ToArray()).ToArray();
            SaveTrajectories(stuff.File("fitness" + Postfix + name + ".dat"), fitness, SamplePeriod);
        }
    }
}
