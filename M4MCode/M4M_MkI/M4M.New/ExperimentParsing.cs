﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using M4M.Epistatics;
using M4M.Modular;
using MathNet.Numerics.Random;
using static M4M.Misc;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public class ExperimentParsing
    {
        public static NoiseType ParseNoiseType(string name)
        {
            if (string.Equals(name, "uniform", StringComparison.InvariantCultureIgnoreCase))
            {
                return NoiseType.Uniform;
            }
            else if (string.Equals(name, "binary", StringComparison.InvariantCultureIgnoreCase))
            {
                return NoiseType.Binary;
            }
            else if (string.Equals(name, "gaussian", StringComparison.InvariantCultureIgnoreCase))
            {
                return NoiseType.Gaussian;
            }
            else
            {
                throw new Exception($"Unrecognised noise type '{name}'");
            }
        }

        public static Hebbian.HebbianType ParseHebbianType(string name)
        {
            if (string.Equals(name, "standard", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hebbian.HebbianType.Standard;
            }
            else if (string.Equals(name, "increaseonly", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hebbian.HebbianType.IncreaseOnly;
            }
            else if (string.Equals(name, "decreaseonly", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hebbian.HebbianType.DecreaseOnly;
            }
            else
            {
                throw new Exception($"Unrecognised hebbian type '{name}'");
            }
        }

        public static Hebbian.MatrixNormalisation ParseMatrixNormalisation(string name)
        {
            if (string.Equals(name, "none", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hebbian.MatrixNormalisation.None;
            }
            else if (string.Equals(name, "iterative", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hebbian.MatrixNormalisation.IterativeNormalisation;
            }
            else
            {
                throw new Exception($"Unrecognised matrix normalisation '{name}'");
            }
        }

        public static Hopfield.BiasType ParseBiasType(string name)
        {
            if (string.Equals(name, "none", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.BiasType.None;
            }
            else if (string.Equals(name, "constant", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.BiasType.Constant;
            }
            else if (string.Equals(name, "linear", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.BiasType.Linear;
            }
            else if (string.Equals(name, "quadratic", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.BiasType.Quadratic;
            }
            else
            {
                throw new Exception($"Unrecognised bias type '{name}'");
            }
        }

        public static Hopfield.InteractionType ParseInteractionType(string name)
        {
            if (string.Equals(name, "none", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.InteractionType.None;
            }
            else if (string.Equals(name, "linear", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.InteractionType.Linear;
            }
            else if (string.Equals(name, "quadratic", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.InteractionType.Quadratic;
            }
            else
            {
                throw new Exception($"Unrecognised interaction type '{name}'");
            }
        }

        public static Hopfield.InteractionMatrixSource ParseInteractionMatrixSource(string name)
        {
            if (string.Equals(name, "genome", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.InteractionMatrixSource.Genome;
            }
            else if (string.Equals(name, "target", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.InteractionMatrixSource.Target;
            }
            else if (string.Equals(name, "genomeAndTarget", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.InteractionMatrixSource.GenomeAndTarget;
            }
            else
            {
                throw new Exception($"Unrecognised interaction matrix source '{name}'");
            }
        }

        public static Hopfield.BiasVectorSource ParseBiasVectorSource(string name)
        {
            if (string.Equals(name, "genome", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.BiasVectorSource.Genome;
            }
            else if (string.Equals(name, "target", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.BiasVectorSource.Target;
            }
            else if (string.Equals(name, "genomeAndTarget", StringComparison.InvariantCultureIgnoreCase))
            {
                return Hopfield.BiasVectorSource.GenomeAndTarget;
            }
            else
            {
                throw new Exception($"Unrecognised bias vector source type '{name}'");
            }
        }

        public static Population<DenseIndividual> ParseDensePopulation(TextWriter console, CliParams clips, ModelExecutionContext context, PopulationExperimentConfig<DenseIndividual> popConfig, string populationType)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("DensePopulations can either come from an experiment file (exp:filename), or be one of the following types:\n" +
                    " - hillclimber\n" +
                    "Generally applicable parameters include:\n" +
                    " - popSize");
            }

            if (populationType.StartsWith("exp:", StringComparison.InvariantCultureIgnoreCase))
            {
                var filename = populationType.Substring("exp:".Length);

                PopulationExperiment<DenseIndividual> exp = PopulationExperiment<DenseIndividual>.Load(filename);
                return exp.Population;
            }

            var config = popConfig.ExperimentConfiguration;

            int popSize = clips.Get("popSize", int.Parse, 1);
            ITransMatMutator transMatMutator = GenomeParsing.GetTransMatMutator(clips.Get("transmatmutator", "default"), config.Size);

            // g0
            var g0string = clips.Get("g0string", null);
            Linear.Vector<double> g0 = g0string == null ? null : ParseExtremesVector(g0string, config.ReproductionRules.InitialStateClamping.Min, config.ReproductionRules.InitialStateClamping.Max, 0.0);

            switch (populationType)
            {
                case "hillclimber":
                    return TypicalConfiguration.CreatePopulation(context, popSize, config.Size, config.DevelopmentRules, transMatMutator: transMatMutator, g0: g0);
            }

            throw new Exception("Unparsable population type: " + populationType);
        }

        public static Linear.Matrix<double> ParseMatrix(string str)
        {
            string type = null;

            if (str.Contains(":"))
            {
                var data = str.Split(':');
                type = data[0];
                str = data[1];
            }

            // TODO: more
            switch (type)
            {
                case "flat":
                {
                    var data = str.Split(';');
                    var size = int.Parse(data[0]);
                    var val = double.Parse(data[1]);
                    return Linear.CreateMatrix.Dense<double>(size, size, val);
                }
                case "diagonal":
                {
                    var data = str.Split(';');
                    var size = int.Parse(data[0]);
                    var val = double.Parse(data[1]);
                    return Linear.CreateMatrix.DenseDiagonal<double>(size, size, val);
                }
                case "cols":
                    var cols = str.Split(';').Select(double.Parse).ToArray();
                    return Misc.Columns(cols.Length, cols);
                case "rows":
                    var rows = str.Split(';').Select(double.Parse).ToArray();
                    return Misc.Rows(rows, rows.Length);
                case "dense":
                {
                    var data = str.Split(';');
                    var intra = double.Parse(data[0]);
                    var inter = double.Parse(data[1]);
                    var modules = MultiModulesStringParser.Instance.Parse(string.Join(";", data.Skip(2)));
                    return Misc.Dense(modules, intra, inter);
                }
                default:
                    throw new Exception("Unparsable matrix string: " + str);
            }
        }

        public static IPopulationResetOperation<DenseIndividual> ParsePopulationResetOperation(string name)
        {
            if (string.Equals(name, "none", StringComparison.InvariantCultureIgnoreCase))
            {
                return PopulationResetOperations.None;
            }
            else if (string.Equals(name, "uniform", StringComparison.InvariantCultureIgnoreCase))
            {
                return PopulationResetOperations.RandomIndependent;
            }
            else if (string.Equals(name, "binary", StringComparison.InvariantCultureIgnoreCase))
            {
                return PopulationResetOperations.RandomBinaryIndependent;
            }
            else if (string.Equals(name, "zero", StringComparison.InvariantCultureIgnoreCase))
            {
                return PopulationResetOperations.Zero;
            }
            else if (string.Equals(name, "perfect", StringComparison.InvariantCultureIgnoreCase))
            {
                return PopulationResetOperations.MatchTargetVectorPerfectPhenotype;
            }
            else if (name.StartsWith("modular", StringComparison.InvariantCultureIgnoreCase))
            {
                var data = name.Substring("modular".Length).Split(';');

                var noiseType = ParseNoiseType(data[0]);
                var noiseMag = double.Parse(data[1]);
                var modules = MultiModulesStringParser.Instance.Parse(data[2]);

                return new RandomModulesPopulationResetOperation(modules, noiseType, noiseMag);
            }
            else if (name.StartsWith("NeutralMutation", StringComparison.InvariantCultureIgnoreCase))
            {
                var count = ParseInt(name.Substring("NeutralMutation".Length));
                return new NeutralMutationPopulationResetOperation(count);
            }
            else if (string.Equals(name, "forcing", StringComparison.InvariantCultureIgnoreCase))
            {
                return PopulationResetOperations.MatchForcingVector;
            }
            else if (name.StartsWith("additiveNoise", StringComparison.InvariantCultureIgnoreCase))
            {
                var data = name.Substring("additiveNoise".Length).Split(';');

                var noiseType = ParseNoiseType(data[0]);
                var noiseMag = double.Parse(data[1]);
                var clamp = data.Length < 3 ? true : bool.Parse(data[2]);

                return new AdditiveNoisePopulationResetOperation(noiseType, noiseMag, clamp);
            }
            else if (name.StartsWith("IterativeNormalisation", StringComparison.InvariantCultureIgnoreCase))
            {
                var rowSum = double.Parse(name.Substring("IterativeNormalisation".Length));
                return new IterativeNormalisationResetOperation(rowSum);
            }
            else if (name.StartsWith("combined", StringComparison.InvariantCultureIgnoreCase))
            {
                var data = name.Substring("combined".Length).Split(new[] { "||" }, StringSplitOptions.None);

                var subOps = data.Select(ParsePopulationResetOperation).ToArray();
                return new CombinedPopulationResetOperation<DenseIndividual>(subOps);
            }
            else
            {
                throw new ArgumentException($"Unrecognised reset operation: \"{name}\"");
            }
        }

        public static ISelectorPreparer<TIndividual> ParseSelectorPreparer<TIndividual>(string name) where TIndividual : IIndividual<TIndividual>
        {
            if (string.Equals(name, "ranked", StringComparison.InvariantCultureIgnoreCase))
            {
                return SelectorPreparers<TIndividual>.Ranked;
            }
            else if (string.Equals(name, "pareto", StringComparison.InvariantCultureIgnoreCase))
            {
                return SelectorPreparers<TIndividual>.Pareto;
            }
            else if (string.Equals(name, "proportional", StringComparison.InvariantCultureIgnoreCase))
            {
                return SelectorPreparers<TIndividual>.Proportional;
            }
            else
            {
                throw new ArgumentException($"Unrecognised selector preparer: \"{name}\". Consider one of: ranked, pareto, proportional");
            }    
        }

        // let's start to think about a more general target loading (really we should be a table, or something...)
        public static EpistaticTargetPackage ParseEpistaticTargetPackage(TextWriter console, CliParams clips, string defaultTargetPackageClass, string targetPackageName)
        {
            console.WriteLine($"({defaultTargetPackageClass}) {targetPackageName}");

            // compound from file
            if (targetPackageName.StartsWith("file:", StringComparison.InvariantCultureIgnoreCase))
            {
                // read many from a file
                var filename = targetPackageName.Substring("file:".Length);
                var lines = System.IO.File.ReadLines(filename);
                
                List<ITarget> targets = new List<ITarget>();
                foreach (string line in lines)
                {
                    if (string.IsNullOrEmpty(line) || line.StartsWith("//"))
                        continue;

                    var cclips = clips.CloneCliParams();
                    cclips.ConsumeLine(line);
                    targets.AddRange(ParseEpistaticTargetPackage(console, cclips, defaultTargetPackageClass, cclips.Get("targetpackage")).Targets);
                }

                return new EpistaticTargetPackage(filename, targets.ToArray());
            }

            // from expfile
            if (targetPackageName.StartsWith("exp:", StringComparison.InvariantCultureIgnoreCase))
            {
                // read many from a experiment file
                var filename = targetPackageName.Substring("exp:".Length);

                var expConfig = CliPlotHelpers.LoadExperimentConfig(filename);

                return new EpistaticTargetPackage(filename, expConfig.Targets.ToArray());
            }

            // determine class
            string targetPackageClass;
            if (targetPackageName.Contains("/"))
            {
                string[] data = targetPackageName.Split('/');
                targetPackageClass = data[0];
                targetPackageName = data[1];
            }
            else
            {
                targetPackageClass = defaultTargetPackageClass;
            }

            if (clips.IsSet("help"))
            {
                console.WriteLine("Recognised TargetPackageClasses:\n" +
                    " - ivmc\n" +
                    " - mc");
            }
            
            // TODO: this should be less terrible
            Dictionary<string, Func<TextWriter, CliParams, string, EpistaticTargetPackage>> parsers = new Dictionary<string, Func<TextWriter, CliParams, string, EpistaticTargetPackage>>(StringComparer.InvariantCultureIgnoreCase);
            parsers.Add("ivmc", ParseIvmcTargetPackage);
            parsers.Add("mc", ParseMcTargetPackage);

            if (parsers.TryGetValue(targetPackageClass, out var parser))
            {
                return parser(console, clips, targetPackageName);
            }
            else
            {
                throw new Exception("Unrecognised target package class '" + targetPackageClass + "'. Qualify the class (e.g. 'ivmc/') or method (e.g. 'exp:'");
            }
        }

        public static EpistaticTargetPackage ParseMcTargetPackage(TextWriter console, CliParams clips, string targetPackageName)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("McTargetPackage Options (checkoncheck):\n" +
                    " - MCn\n" +
                    " - MCk\n" +
                    " - MCp");
            }

            int n = clips.Get("MCn", int.Parse);
            int k = clips.Get("MCk", int.Parse);
            double p = clips.Get("MCp", double.Parse);

            if (string.Equals(targetPackageName, "checkoncheck", StringComparison.InvariantCultureIgnoreCase))
            {
                return new EpistaticTargetPackage($"MCCheckOnCheck_n{n}k{k}", new[] { new MCTargetCheckOnCheck(n, k, p) });
            }
            else if (string.Equals(targetPackageName, "plain", StringComparison.InvariantCultureIgnoreCase))
            {
                return new EpistaticTargetPackage($"MCPlainOnPlain_n{n}k{k}", new[] { new MCTargetPlainOnPlain(n, k, p) });
            }
            else
            {
                throw new Exception("Unrecognised MC TargetPackage: '" + targetPackageName + "'");
            }
        }

        public static EpistaticTargetPackage ParseIvmcSinusoidTargetPackage(TextWriter console, CliParams clips, string targetPackageName)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("IvmcSinusoidTargetPackage Options (4/custom):\n" +
                    " - plusphases and minusphases\n" +
                    " - plusamps and minusamps\n" +
                    " - plusfreqs and minusfreqs\n" +
                    " - targetString (+/-)\n" +
                    " - modulesString");
            }
            
            var judgementPreparer = ParseIvmcProperJudgementPreparer(console, clips);
            
            if (targetPackageName == "4")
            {
                var period = clips.Get("period", double.Parse);
                var freq = 1.0 / period;
                var qperiod = period / 8.0;
                var mean = clips.Get("mean", double.Parse);
                var amp = clips.Get("amp", double.Parse);

                var target = ParseExtremesVector("++++++++++++++++", -1.0, 1.0, 0.0);
                var modules = Modular.MultiModulesStringParser.Instance.Parse("4*4");

                var plusPhases = new[] { qperiod * 0, qperiod * 1, qperiod * 2, qperiod * 3 };
                var minusPhases = new[] { qperiod * 4, qperiod * 5, qperiod * 6, qperiod * 7};
                var plusAmps = new[] { amp, amp, amp, amp };
                var minusAmps = new[] { amp, amp, amp, amp };
                var plusFreqs = new[] { freq, freq, freq, freq };
                var minusFreqs = new[] { freq, freq, freq, freq };
                var plusMeans = new[] { mean, mean, mean, mean };
                var minusMeans = new[] { mean, mean, mean, mean };

                string friendlyName = $"IvmcProperSinusoid4P{period}M{mean}A{amp}";
                
                return new EpistaticTargetPackage(friendlyName, new[]
                {
                new Epistatics.IvmcProperSinusoid(
                    judgementPreparer, modules,
                    plusPhases, minusPhases,
                    plusAmps, minusAmps,
                    plusFreqs, minusFreqs,
                    plusMeans, minusMeans,
                    target, friendlyName)
                });
            }
            else if (string.Equals(targetPackageName, "custom", StringComparison.InvariantCultureIgnoreCase))
            {
                string targetString = clips.Get("targetString");
                var target = ParseExtremesVector(targetString, -1.0, 1.0, 0.0);

                string modulesString = clips.Get("modulesString");
                var modules = Modular.MultiModulesStringParser.Instance.Parse(modulesString);

                var plusPhases = clips.Get("plusPhases", ParseDoubleList);
                var minusPhases = clips.Get("minusPhases", ParseDoubleList);
                var plusAmps = clips.Get("plusAmps", ParseDoubleList);
                var minusAmps = clips.Get("minusAmps", ParseDoubleList);
                var plusFreqs = clips.Get("plusFreqs", ParseDoubleList, clips.Get("plusPeriods", s => ParseDoubleList(s).Select(d => 1.0 / d).ToArray()));
                var minusFreqs = clips.Get("minusFreqs", ParseDoubleList, clips.Get("minusPeriods", s => ParseDoubleList(s).Select(d => 1.0 / d).ToArray()));
                var plusMeans = clips.Get("plusMeans", ParseDoubleList);
                var minusMeans = clips.Get("minusMeans", ParseDoubleList);

                string friendlyName = "Custom IvmcProperSinusoid";

                return new EpistaticTargetPackage(friendlyName, new[]
                {
                    new Epistatics.IvmcProperSinusoid(judgementPreparer, modules, plusPhases, minusPhases, plusAmps, minusAmps, plusFreqs, minusFreqs, plusMeans, minusMeans, target, friendlyName)
                });
            }

            throw new Exception("Unrecognised TargetPackage name: " + targetPackageName + "\n" +
                "Try one of:\n" +
                " - 4 (4x4 modules)\n" +
                " - custom");
        }

        public static IIvmcProperJudgementPreparer ParseIvmcProperJudgementPreparer(TextWriter console, CliParams clips)
        {
            var judgementMode = clips.Get("judgemode", TraditionalIvmcProperJudgementPreparer.ParseTraditionalIvmcProperJudgementMode, IvmcProperJudgementMode.Proper);

            if (judgementMode == IvmcProperJudgementMode.Stacked)
            {
                var mbe = ModuleBenefitFunctions.ParseModuleBenefitFunction(clips.Get("modulebenefitfunction", "split"));
                var decayFactor = clips.Get("decayfactor", double.Parse, 1.0);
                var rescaleFitness = clips.Get("rescalefitness", bool.Parse, true);
                var scaleFactor = clips.Get("scalefactor", int.Parse, 2);
                return new IvmcStackedProperJudgementPreparer(mbe, decayFactor, rescaleFitness, scaleFactor);
            }
            else
            {
                return new TraditionalIvmcProperJudgementPreparer(judgementMode);
            }
        }

        public static EpistaticTargetPackage ParseIvmcTargetPackage(TextWriter console, CliParams clips, string targetPackageName)
        {
            if (targetPackageName.StartsWith("sin:", StringComparison.InvariantCultureIgnoreCase))
            {
                return ParseIvmcSinusoidTargetPackage(console, clips, targetPackageName.Substring("sin:".Length));
            }

            if (clips.IsSet("help"))
            {
                console.WriteLine("ProperIvmcTargetPackage Options (4/custom):\n" +
                    " - Z (Z rate)\n" +
                    " - CH (High fitness, default 1.0)\n" +
                    " - CL (Low fitness, default 0.7)\n" +
                    " - C0 (Zero fitness, default 0.0)\n" +
                    " - targetString (+/-)\n" +
                    " - modulesString\n" +
                    " - variationmodulesString (default null)\n" +
                    " - variationModulePlusOverridesString (ivmcvmpos, default null)\n" +
                    " - variationModuleMinusOverridesString (ivmcvmmos, default null)\n" +
                    " - judgemode (IvmcProperJudgementMode, one of: proper, split, stackedsplit, or stacked)\n" +
                    "     if stacked, then qualify\n" +
                    "      - modulebenefitfunction (default split)\n" +
                    "      - decayfactor (default 1.0)\n" +
                    "      - rescalefitness (default true)\n" +
                    "      - scalefactor (default 2.0)");
            }
            
            double cHigh = clips.Get("CH", double.Parse, 1.0);
            double cLow = clips.Get("CL", double.Parse, 0.7);
            double cZero = clips.Get("C0", double.Parse, 0.0);
            double Z = clips.Get("Z", double.Parse);

            if (targetPackageName == "4")
            {
                var judgementMode = clips.Get("judgemode", TraditionalIvmcProperJudgementPreparer.ParseTraditionalIvmcProperJudgementMode, IvmcProperJudgementMode.Proper);
                return IvmcTargetPackages.IvmcProper1_4x4(judgementMode, cHigh, cLow, cZero, Z);
            }
            else if (string.Equals(targetPackageName, "custom", StringComparison.InvariantCultureIgnoreCase))
            {
                var judgementPreparer = ParseIvmcProperJudgementPreparer(console, clips);
                string targetString = clips.Get("targetString");
                string modulesString = clips.Get("modulesString");
                string variationModulesString = clips.Get("variationmodulesString", clips.Get("ivmcvms", null));
                string variationModulePlusOverridesString = clips.Get("variationModulePlusOverridesString", clips.Get("ivmcvmpos", null));
                string variationModuleMinusOverridesString = clips.Get("variationModuleMinusOverridesString", clips.Get("ivmcvmmos", null));
                int ivmcFixedSampleCount = clips.Get("ivmcFixedSampleCount", int.Parse, 0);

                if (ivmcFixedSampleCount > 0)
                {
                    int ivmcFixedSampleSeed = clips.Get("ivmcFixedSampleSeed", int.Parse);
                    var rand = new CustomMersenneTwister(ivmcFixedSampleSeed);
                    var moduleCount = MultiModulesStringParser.Instance.Parse(modulesString).ModuleCount;
                    IvmcTargetPackages.GenerateRandomIvmcHL0Overrides(rand, moduleCount, Z, ivmcFixedSampleCount, out variationModuleMinusOverridesString, out variationModulePlusOverridesString);
                }

                // multi module-override mode
                if ((!string.IsNullOrEmpty(variationModulePlusOverridesString) || !string.IsNullOrEmpty(variationModulePlusOverridesString))
                    && (variationModuleMinusOverridesString.Contains('|') || variationModulePlusOverridesString.Contains('|')))
                {
                    if (!string.IsNullOrEmpty(variationModulePlusOverridesString) && !string.IsNullOrEmpty(variationModulePlusOverridesString))
                    {
                        var variationModuleMinusOverridesData = variationModuleMinusOverridesString.Split('|');
                        var variationModulePlusOverridesData = variationModulePlusOverridesString.Split('|');
                        var customNames = clips.Get("targetnames", s => s.Split('|'), null);

                        if (variationModuleMinusOverridesData.Length != variationModulePlusOverridesData.Length)
                            throw new Exception("Multi module-override mode requires the same number of override strings for both Plus and Minus overrides");

                        var targets = new List<ITarget>();
                        for (int ti = 0; ti < variationModuleMinusOverridesData.Length; ti++)
                        {
                            var innerVariationModuleMinusOverridesString = variationModuleMinusOverridesData[ti];
                            var innerVariationModulePlusOverridesString = variationModulePlusOverridesData[ti];
                            var targetName = customNames == null ? null : customNames[ti];
                            targets.Add(IvmcTargetPackages.IvmcProper1_CustomTarget(modulesString, variationModulesString, innerVariationModulePlusOverridesString, innerVariationModuleMinusOverridesString, targetString, judgementPreparer, cHigh, cLow, cZero, Z, targetName));
                        }

                        return new EpistaticTargetPackage($"IvmcProper1_CustomCH{cHigh}CL{cLow}CZ{cZero}Z{Z}", targets.ToArray());
                    }
                    else
                    {
                        throw new Exception("Multi module-override mode requires both Plus and Minus override strings to be fully qualified");
                    }
                }

                return IvmcTargetPackages.IvmcProper1_Custom(modulesString, variationModulesString, variationModulePlusOverridesString, variationModuleMinusOverridesString, targetString, judgementPreparer, cHigh, cLow, cZero, Z, null);
            }

            throw new Exception("Unrecognised TargetPackage name: " + targetPackageName + "\n" +
                "Try one of:\n" +
                " - 4 (4x4 modules)\n" +
                " - custom");
        }

        public static EpistaticTargetPackage ParseSimpleStackedTargetPackage(TextWriter console, CliParams clips, string targetPackageName)
        {
            if (targetPackageName == "custom")
            {
                var m = clips.Get("moduleCount", int.Parse);
                var k = clips.Get("moduleSize", int.Parse);
                var cp = clips.Get("plusCoef", double.Parse, 1.0);
                var cm = clips.Get("minusCoef", double.Parse, 1.0);
                var scaleFactor = clips.Get("scaleFactor", int.Parse, m); // default to MC type
                var decayFactor = clips.Get("DecayFactor", double.Parse);
                var targetString = clips.GetOrCreate("targetString",
                    () => clips.IsSet("chaff")
                        ? string.Join("", Enumerable.Range(0, m).Select(i => i % 2 == 0 ? new string('+', k) : new string('-', k)))
                        : new string('+', m * k)); // default to all +
                var mbf = ModuleBenefitFunctions.ParseModuleBenefitFunction(clips.Get("modulebenefitfunction", "split"));
                var rescaleFitness = clips.Get("rescaleFitness", bool.Parse, true);

                var target = new SimpleStackedTarget(targetString, k, m, decayFactor, scaleFactor, mbf, cp, cm, rescaleFitness);

                return new EpistaticTargetPackage($"SimpleStackedTargetPackageM{m}K{k}cP{cp}cM{cm}SF{scaleFactor}DF{decayFactor}RF{rescaleFitness}{mbf.Name}_{targetString}", new[] { target });
            }

            throw new Exception("Unrecognised TargetPackage name: " + targetPackageName + "\n" +
                "Try one of:\n" +
                " - custom");
        }

        public static EpistaticTargetPackage ParseCorrelationMatrixTargetPackage(CliParams clips, string targetPackageName)
        {
            if (targetPackageName.StartsWith("ncs", StringComparison.InvariantCultureIgnoreCase))
            {
                int size = int.Parse(targetPackageName.Substring("ncs".Length));
                return new EpistaticTargetPackage("ConcentricSquares" + size, new[] { StandardCorrelationMatrixTargets.ConcentricSquares(size, true) });
            }
            else if (targetPackageName.StartsWith("llc", StringComparison.InvariantCultureIgnoreCase))
            {
                var data = targetPackageName.Substring("llc".Length).Split('x');
                int width = int.Parse(data[0]);
                int height = int.Parse(data[1]);
                var neighbourType = clips.Get("neighbourType", Neighbours.ParseNeightbourType, NeighbourType.Five);
                var pattern = clips.Get("matrixPattern", MatrixPatterns.GetPattern, CheckPattern.Instance);
                bool torus = clips.Get("torusTarget", bool.Parse, false);
                return new EpistaticTargetPackage($"Linear{pattern.Name}Local{neighbourType}Constraints{width}x{height}{(torus ? "Torus" : "")}", new[] { StandardCorrelationMatrixTargets.LinearLocalConstraints(width, height, pattern, neighbourType, torus, false) });
            }
            else if (targetPackageName.StartsWith("cs", StringComparison.InvariantCultureIgnoreCase))
            {
                int size = int.Parse(targetPackageName.Substring("cs".Length));
                return new EpistaticTargetPackage("ConcentricSquares" + size, new[] { StandardCorrelationMatrixTargets.ConcentricSquares(size, false) });
            }
            else if (targetPackageName.StartsWith("partialcs", StringComparison.InvariantCultureIgnoreCase))
            {
                var seed = clips.Get("targetpackageseed", int.Parse);
                var rand = new CustomMersenneTwister(seed);

                var pruneProbability = clips.Get("pruneProbability", double.Parse);
                int size = int.Parse(targetPackageName.Substring("partialcs".Length));
                return new EpistaticTargetPackage("PartialConcentricSquares" + size + "P" + pruneProbability, new[] { StandardCorrelationMatrixTargets.PartialConcentricSquares(size, false, pruneProbability, rand) });
            }
            else if (targetPackageName.StartsWith("randominconsistent", StringComparison.InvariantCultureIgnoreCase))
            {
                var seed = clips.Get("targetpackageseed", int.Parse);
                var rand = new CustomMersenneTwister(seed);

                var data = targetPackageName.Substring("randominconsistent".Length).Split(';');
                var size = int.Parse(data[0]);
                var mag = double.Parse(data[1]);
                var noiseType = ParseNoiseType(data[2]);
                return new EpistaticTargetPackage("RandomInconsistent" + size + ";" + mag + ";" + noiseType.ToString(), new[] { StandardCorrelationMatrixTargets.RandomInconsistentConstraintTarget(rand, size, mag, noiseType, false) });
            }
            else if (targetPackageName.StartsWith("chiff", StringComparison.InvariantCultureIgnoreCase))
            {
                int levels = int.Parse(targetPackageName.Substring("chiff".Length));
                var downFactor = clips.Get("downFactor", double.Parse, 0.5);
                bool chaff = clips.Get("chaff", bool.Parse, false);
                return new EpistaticTargetPackage("ContinuousHiff" + levels, new[] { StandardCorrelationMatrixTargets.ContinuousHiff(levels, false, downFactor, chaff) });
            }
            else if (targetPackageName.StartsWith("flat", StringComparison.InvariantCultureIgnoreCase))
            {
                int size = int.Parse(targetPackageName.Substring("flat".Length));
                var agreement = clips.Get("agreement", double.Parse, 0.0);
                return new EpistaticTargetPackage("Flat" + size, new[] { StandardCorrelationMatrixTargets.Flat(size, false, agreement) });
            }
            else if (targetPackageName.StartsWith("funkymca", StringComparison.InvariantCultureIgnoreCase))
            {
                int moduleCount = int.Parse(targetPackageName.Substring("funkymca".Length));
                var agreement = clips.Get("agreement", double.Parse);
                return new EpistaticTargetPackage("FunkyMcA" + moduleCount, new[] { StandardCorrelationMatrixTargets.FunkyMcAscending(moduleCount, false, agreement) });
            }
            else if (targetPackageName.StartsWith("funkymc", StringComparison.InvariantCultureIgnoreCase))
            {
                var moduleString = targetPackageName.Substring("funkymc".Length);
                var modules = Modular.MultiModulesStringParser.Instance.Parse(moduleString);
                var agreement = clips.Get("agreement", double.Parse);
                return new EpistaticTargetPackage("FunkyMc" + moduleString, new[] { StandardCorrelationMatrixTargets.FunkyMc(modules, false, agreement) });
            }
            else if (targetPackageName.StartsWith("sudoku", StringComparison.InvariantCultureIgnoreCase))
            {
                int size = int.Parse(targetPackageName.Substring("sudoku".Length));
                var diagonalCoef = clips.Get("diagonalCoef", double.Parse);
                var constraintCoef = clips.Get("constraintCoef", double.Parse);
                bool additiveConstraints = clips.Get("additiveconstraints", bool.Parse, true);
                var flatCoef = clips.Get("flatCoef", double.Parse, 0.0);
                var mat = Sudoku.GeneralSudoku(size, diagonalCoef, constraintCoef, additiveConstraints);
                mat.Add(flatCoef, mat);

                var partial = clips.Get("partialsolution", Sudoku.ParsePartialSolution, null);

                if (partial == null)
                {
                    var target = new CorrelationMatrixTarget(mat, "Sudoku" + size, false);
                    return new EpistaticTargetPackage("Sudoku" + size, new[] { target });
                }
                else
                {
                    var forcingMagnitude = clips.Get("forcingMagnitude", double.Parse, 1.0);
                    var forcingVector = Sudoku.PrepareForcingVector(partial, false) * forcingMagnitude;
                    var target = new CorrelationMatrixTarget(mat, "Sudoku" + size, false, forcingVector);
                    return new EpistaticTargetPackage("Sudoku" + size, new[] { target });
                }
            }
            else if (targetPackageName.StartsWith("nqueens", StringComparison.InvariantCultureIgnoreCase))
            {
                int size = int.Parse(targetPackageName.Substring("nqueens".Length));
                var diagonalCoef = clips.Get("diagonalCoef", double.Parse);
                var constraintCoef = clips.Get("constraintCoef", double.Parse);
                bool proportionalConstraints = clips.Get("proportionalConstraints", bool.Parse, true);
                var flatCoef = clips.Get("flatCoef", double.Parse, 0.0);
                var mat = NQueens.GeneralNQueens(size, diagonalCoef, constraintCoef, proportionalConstraints);
                mat.Add(flatCoef, mat);

                var partial = clips.Get("partialsolution", NQueens.ParsePartialSolution, null);

                if (partial == null)
                {
                    var target = new CorrelationMatrixTarget(mat, "NQueens" + size, false);
                    return new EpistaticTargetPackage("NQueens" + size, new[] { target });
                }
                else
                {
                    var forcingMagnitude = clips.Get("forcingMagnitude", double.Parse, 1.0);
                    var forcingVector = NQueens.PrepareForcingVector(partial, false) * forcingMagnitude;
                    var target = new CorrelationMatrixTarget(mat, "NQueens" + size, false, forcingVector);
                    return new EpistaticTargetPackage("NQueens" + size, new[] { target });
                }
            }

            throw new Exception("Unrecognised TargetPackage name: " + targetPackageName + "\n" +
                "Try one of:\n" +
                " - cs{size}\n" +
                " - ncs{size}\n" +
                " - randominconsistent{size;mag;noisetype}\n" +
                " - chiff\n" +
                " - flat\n" +
                " - funkymca\n" +
                " - sudoku{size}\n" +
                " - nqueens{size}\n");
        }

        public static IRegularisationFunction<IGenome> ParseRegFunction(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (name.Equals("const", StringComparison.InvariantCultureIgnoreCase))
                    return JudgementRules.ConstantEquivalent;

                if (name.Equals("l1", StringComparison.InvariantCultureIgnoreCase))
                    return JudgementRules.L1Equivalent;

                if (name.Equals("rowl1", StringComparison.InvariantCultureIgnoreCase))
                    return JudgementRules.RowL1Equivalent;

                if (name.Equals("coll1", StringComparison.InvariantCultureIgnoreCase))
                    return JudgementRules.ColL1Equivalent;

                if (name.Equals("l2", StringComparison.InvariantCultureIgnoreCase))
                    return JudgementRules.L2Equivalent;

                if (name.StartsWith("l1And2AB", StringComparison.InvariantCultureIgnoreCase))
                {
                    var ab = name.Remove(0, "l1And2AB".Length).Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(double.Parse).ToArray();
                    return JudgementRules.L1And2(ab[0], ab[1]);
                }

                if (name.StartsWith("mmsoAB", StringComparison.InvariantCultureIgnoreCase))
                {
                    var ab = name.Remove(0, "mmsoAB".Length).Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(double.Parse).ToArray();
                    return JudgementRules.MMSO(ab[0], ab[1]);
                }
                
                if (name.StartsWith("mmsolinABC", StringComparison.InvariantCultureIgnoreCase))
                {
                    var abc = name.Remove(0, "mmsolinABC".Length).Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(double.Parse).ToArray();
                    return JudgementRules.MMSOLin(abc[0], abc[1], abc[2]);
                }

                if (name.StartsWith("mmsolinAQ", StringComparison.InvariantCultureIgnoreCase))
                {
                    var aq = name.Remove(0, "mmsolinAQ".Length).Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(double.Parse).ToArray();
                    return JudgementRules.MMSOLin(aq[0], aq[1]);
                }
            }
            
            throw new ArgumentException("Unrecognised regularisation function: '" + name + "'. Consider one of these:\n" +
                " - const\n" +
                " - l1\n" +
                " - rowl1\n" +
                " - coll1\n" +
                " - l2\n" +
                " - MmsoAB{a;b}\n" +
                " - MmsoLinAQ{a;q}\n" +
                " - MmsoLinABC{a;b;c}");
        }

        public static ITargetPackage ParseSquareBinaryPuzzle(CliParams clips)
        {
            var targetStrings = clips.Get("targetstring").Split('|');
            var targetVectors = targetStrings.Select(ts => Misc.ParseExtremesVector(ts, -1, +1, 0.0)).ToArray();
            var rcc = clips.Get("rcc", double.Parse);
            var rlcc = clips.Get("rlcc", double.Parse);
            var d = (int)Math.Sqrt(targetVectors[0].Count);
            var targets = targetVectors.Select(tv => new BinaryPuzzleTarget(tv.ToArray(), d, d, rcc, rlcc)).ToArray();
            return new TargetPackage("SquareBinaryPuzzles", targets);
        }
    }
}
