﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Random;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using Linear = MathNet.Numerics.LinearAlgebra;
using RandomSource = MathNet.Numerics.Random.RandomSource;

namespace M4M
{
    public class CustomComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            if (double.TryParse(x, out var nx) && double.TryParse(y, out var ny))
            {
                return nx.CompareTo(ny);
            }
            else
            {
                return StringComparer.InvariantCultureIgnoreCase.Compare(x, y);
            }
        }
    }

    public interface IRandomVectorProvider
    {
        string Name { get; }
        Linear.Vector<double> RandomVector(Misc.Range range, int size, RandomSource rand);
    }

    public class BinaryRandomVectorProvider : IRandomVectorProvider
    {
        public static BinaryRandomVectorProvider Instance = new BinaryRandomVectorProvider();

        private BinaryRandomVectorProvider()
        {
            // nix
        }

        public string Name => "Binary";

        public Linear.Vector<double> RandomVector(Misc.Range range, int length, RandomSource rand)
        {
            return Linear.CreateVector.Dense(length, i => rand.NextBoolean() ? range.Min : range.Max);
        }
    }
    public class RandomVectorProviderPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public RandomVectorProviderPopulationResetOperation(IRandomVectorProvider randomVectorProvider)
        {
            RandomVectorProvider = randomVectorProvider ?? throw new ArgumentNullException(nameof(randomVectorProvider));
        }

        public IRandomVectorProvider RandomVectorProvider { get; }

        public string Name => $"RandomVectorProviderResetOperation {RandomVectorProvider.Name}";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Misc.Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(i =>
            {
                var clone = i.Clone(context);
                clone.Genome.CopyOverInitialState(RandomVectorProvider.RandomVector(gResetRange, clone.Genome.Size, context.Rand));
                return clone;
            });
        }
    }

    public class UniformRandomVectorProvider : IRandomVectorProvider
    {
        public static UniformRandomVectorProvider Instance = new UniformRandomVectorProvider();

        private UniformRandomVectorProvider()
        {
            // nix
        }

        public string Name => "Uniform";

        public Linear.Vector<double> RandomVector(Misc.Range range, int length, RandomSource rand)
        {
            return Linear.CreateVector.Dense(length, i => range.Sample(rand));
        }
    }

    public class ZeroVectorProvider : IRandomVectorProvider
    {
        public static ZeroVectorProvider Instance = new ZeroVectorProvider();

        private ZeroVectorProvider()
        {
            // nix
        }

        public string Name => "Zero";

        public Linear.Vector<double> RandomVector(Misc.Range range, int length, RandomSource rand)
        {
            return Linear.CreateVector.Dense(length, 0.0);
        }
    }

    public class CliBestiary : ICliProvider
    {
        public string Key => "bestiary";

        public void Run(TextWriter console, CliParams clips)
        {
            console.WriteLine("Note: Bestiary depends on shed loads of stuff (because ImageSharp depends on shed loads of stuff) so if it isn't working, try shuffling DLLs around (never tested under .NET Framework)");

            if (clips.IsSet("help"))
            {
                console.WriteLine("Bestiary, generates many phenotypes from an experiment's population and configuration. Takes the following parameters:\n" +
                    " - bestiary (filename of expfile)\n" +
                    " - gmode (vector random mode, either Mutate, Binary, or Uniform\n" +
                    " - out (output file name; .png may be appended by default if you forgot the extension\n" +
                    " - min (default -1)\n" +
                    " - max (default +1)\n" +
                    " - saturate (default false)\n" +
                    " - count (default 100)\n" +
                    " - seed (default 42)");
                console.WriteLine("Alternatively, specify a 'dir' and it will tabulate it for you");
            }

            if (clips.IsSet("dir"))
            {
                TabulateGenomes(console, clips, clips.Get("dir"));
            }
            else if (clips.IsSet("net"))
            {
                DrawNet(console, clips, clips.Get("net"));
            }
            else
            {
                GenerateBestiary(console, clips);
            }
        }

        // deficient for most purposes
        private struct Pair<TL, TR>
        {
            public readonly TL Left;
            public readonly TR Right;

            public Pair(TL left, TR right)
            {
                Left = left;
                Right = right;
            }
        }

        /// <summary>
        /// Draws 
        /// </summary>
        /// <param name="clips">CliParams</param>
        /// <param name="filename">Genome or EpochSaveFile</param>
        public static void DrawNet(TextWriter console, CliParams clips, string filename)
        {
            var outfile = clips.Get("out", filename + ".png");

            DenseGenome genome;
            if (System.IO.Path.GetFileName(filename).StartsWith("genome", StringComparison.InvariantCultureIgnoreCase))
            {
                genome = Analysis.LoadGenome(filename);
            }
            else
            {
                var exp = PopulationExperiment<DenseIndividual>.Load(filename);
                genome = exp.Population.ExtractAll()[0].Genome;
            }

            float r = clips.Get("radius", float.Parse, 10f);
            float s = clips.Get("spacing", float.Parse, 10f);

            string arrangementString = clips.Get("arrangement");

            if (arrangementString == "4x4std")
            {
                arrangementString = "0;1;4;5&2;3;6;7&8;9;12;13&10;11;14;15";
            }

            var nodeLines = arrangementString.Split('&').Select(l => l.Split(';').Select(int.Parse).ToArray()).ToArray();
            int w = nodeLines.Max(l => l.Length);
            int h = nodeLines.Length;

            float g = r * 2f + s;

            int imgWidth = (int)Math.Ceiling(w * g - s);
            int imgHeight = (int)Math.Ceiling(h * g - s);

            PointF[] nodeLocs = new PointF[genome.Size];
            float y = r;
            for (int j = 0; j < nodeLines.Length; j++)
            {
                var line = nodeLines[j];
                float x = r;

                for (int i = 0; i < line.Length; i++)
                {
                    var n = line[i];
                    if (n >= 0)
                    {
                        nodeLocs[n] = new PointF(x, y);
                    }

                    x += g;
                }

                y += g;
            }

            var dtm = genome.TransMat;
            double threshold = clips.Get("threshold", double.Parse, dtm.Enumerate().Max<double>(d => Math.Abs(d)) * 0.1);
            
            using (var img = new Image<Rgba32>(imgWidth, imgHeight))
            {
                DrawNetwork(img, dtm, r, 1.0f, Rgba32.Black, Rgba32.Red, Rgba32.Blue, threshold, nodeLocs);

                console.WriteLine("Saving to " + outfile);
                img.Save(outfile);
            }
        }

        /// <summary>
        /// Reads aliases from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ReadAliases(string filename)
        {
            var aliases = new Dictionary<string, string>();
            if (System.IO.File.Exists(filename))
            {
                foreach (var line in System.IO.File.ReadLines("xAliases.txt"))
                {
                    if (string.IsNullOrWhiteSpace(line))
                        continue;
                    var data = StringHelpers.Split(line).ToArray();
                    aliases.Add(data[0], data[1]);
                }
            }
            return aliases;
        }

        /// <summary>
        /// Returns a function that takes a key and returns the value found in the dictionary or the key itself if it is not present in the dictionary.
        /// </summary>
        /// <param name="aliases"></param>
        /// <returns></returns>
        public static Func<string, string> Defaulting(Dictionary<string, string> aliases)
        {
            return (string key) => aliases.TryGetValue(key, out var found) ? found : key;
        }

        /// <summary>
        /// Plots DTMs
        /// </summary>
        /// <param name="clips"></param>
        /// <param name="dir"></param>
        public static void TabulateGenomes(TextWriter console, CliParams clips, string dir)
        {
            var genomeFilename = clips.Get("gname", "genome_t.dat");
            var outfile = clips.Get("out");
            var min = clips.Get("min", double.Parse, -1.0);
            var max = clips.Get("max", double.Parse, +1.0);
            
            Dictionary<Pair<string, string>, DenseGenome> Table = new Dictionary<Pair<string, string>, DenseGenome>();
            SortedSet<string> xs = new SortedSet<string>(new CustomComparer());
            SortedSet<string> ys = new SortedSet<string>(new CustomComparer());

            DenseGenome example = null;

            var xAliases = ReadAliases("xAliases.txt");
            var aliasX = Defaulting(xAliases);

            var yAliases = ReadAliases("yAliases.txt");
            var aliasY = Defaulting(yAliases);

            var xdirs = System.IO.Directory.GetDirectories(dir);
            foreach (var xdir in xdirs)
            {
                var xaliasfile = System.IO.Path.Combine(xdir, "alias.txt");
                var x = System.IO.File.Exists(xaliasfile)
                    ? System.IO.File.ReadAllText(xaliasfile)
                    : aliasX(System.IO.Path.GetFileName(xdir));
                console.WriteLine(x);

                var ydirs = System.IO.Directory.GetDirectories(xdir);
                foreach (var ydir in ydirs)
                {
                    var yaliasfile = System.IO.Path.Combine(ydir, "alias.txt");
                    var y = System.IO.File.Exists(yaliasfile)
                        ? System.IO.File.ReadAllText(yaliasfile)
                        : aliasY(System.IO.Path.GetFileName(ydir));
                    console.WriteLine(y);

                    string gpath = System.IO.Path.Combine(ydir, genomeFilename);

                    console.WriteLine(gpath);
                    if (System.IO.File.Exists(gpath))
                    {
                        DenseGenome g;

                        if (genomeFilename.StartsWith("genome"))
                        {
                            var temp = Analysis.LoadGenome(gpath);
                            g = temp;
                        }
                        else if (genomeFilename.StartsWith("epoch"))
                        {
                            var temp = PopulationExperiment<DenseIndividual>.Load(gpath);
                            g = temp.Population.ExtractAll()[0].Genome;
                        }
                        else
                        {
                            throw new NotSupportedException("Can't extract a dense genome from " + gpath);
                        }

                        Table.Add(new Pair<string, string>(x, y), g);
                        xs.Add(x);
                        ys.Add(y);

                        if (example == null || example.Size < g.Size)
                            example = g;
                    }
                }
            }

            if (example == null)
            {
                console.WriteLine("No genomes found; try qualifying gname");
                return;
            }

            bool axes = clips.Get("axes", bool.Parse, false);

            // cell dimensions
            int cw = clips.Get("cw", int.Parse, 1);
            int ch = clips.Get("ch", int.Parse, 1);

            // spacing dimensions
            int sw = clips.Get("sw", int.Parse, 1);
            int sh = clips.Get("sh", int.Parse, 1);

            bool labels = clips.Get("labels", bool.Parse, true);
            
            string fontname = clips.Get("font", "Consolas");
            bool whot = clips.IsSet("whot") || !clips.IsSet("bhot");

            // matrix width and height
            int mw = example.TransMat.ColumnCount * cw;
            int mh = example.TransMat.RowCount * ch;

            // column and row counts
            int c = ys.Count;
            int r = xs.Count;

            int fontSize = clips.Get("fontSize", int.Parse, Math.Max(mw, mh));
            Font font = labels ? SystemFonts.CreateFont(fontname, fontSize, FontStyle.Regular) : null;
            
            int tp = 4;
            var xTitle = clips.Get("xtitle", null);
            var yTitle = clips.Get("ytitle", null);
            int tw = xTitle == null ? 0 : tp + (int)TextMeasurer.Measure(xTitle, new RendererOptions(font)).Height;
            int th = yTitle == null ? 0 : tp + (int)TextMeasurer.Measure(yTitle, new RendererOptions(font)).Height;

            int lp = 4; // padding
            int lw = labels ? (int)xs.Max(x => TextMeasurer.Measure(x, new RendererOptions(font)).Width) + lp : 0;
            int lh = labels ? (int)ys.Max(y => TextMeasurer.Measure(y, new RendererOptions(font)).Width) + lp : 0;

            int ap = axes ? 8 : 0;
            int aw = ap;
            int ah = ap;

            using (var img = new Image<Rgba32>(tw + lw + aw + (mw + sw) * c - sw, th + lh + ah + (mh + sh) * r - sh))
            {
                // draw titles
                if (xTitle != null)
                {
                    var m = TextMeasurer.Measure(xTitle, new RendererOptions(font));
                    var pen = new Pen(Color.Black, 1f);
                    var brush = new SolidBrush(Color.Black);

                    img.Mutate(ctx =>
                    {
                        ctx.RotateFlip(RotateMode.Rotate90, FlipMode.None);
                        ctx.DrawText(xTitle, font, brush, pen, new PointF(img.Width / 2 - m.Width / 2, 0));
                        ctx.RotateFlip(RotateMode.Rotate270, FlipMode.None);
                    });
                }
                if (yTitle != null)
                {
                    var m = TextMeasurer.Measure(yTitle, new RendererOptions(font));
                    var pen = new Pen(Color.Black, 1f);
                    var brush = new SolidBrush(Color.Black);

                    img.Mutate(ctx =>
                    {
                        ctx.DrawText(yTitle, font, brush, pen, new PointF(img.Width / 2 - m.Width / 2, 0));
                    });
                }

                // draw labels
                if (labels)
                {
                    var pen = new Pen(Color.Black, 1f);
                    var brush = new SolidBrush(Color.Black);

                    img.Mutate(ctx =>
                    {
                        int j = th + lh + ah;
                        foreach (var x in xs)
                        {
                            ctx.DrawText(x, font, brush, pen, new PointF(tw, j));
                            j += mh + sh;
                        }
                    });
                    
                    img.Mutate(ctx =>
                    {
                        ctx.RotateFlip(RotateMode.Rotate270, FlipMode.None);
                        int i = 0;
                        foreach (var y in ys.Reverse())
                        {
                            ctx.DrawText(y, font, brush, pen, new PointF(th, i));
                            i += mw + sw;
                        }
                        ctx.RotateFlip(RotateMode.Rotate90, FlipMode.None);
                    });
                }

                // draw arrows
                if (axes)
                {
                    var pen = new Pen(Color.Black, 2f);
                    img.Mutate(ctx =>
                    {
                        var yend = new PointF(tw + lw + aw / 2, img.Height);
                        ctx.DrawLines(pen, new[] { new PointF(tw + lw + aw / 2, th + lh + ah), yend });
                        ctx.DrawLines(pen, new[] { new PointF(tw + lw + 1, img.Height - 8), yend, new PointF(tw + lw + aw - 1, img.Height - 8) });

                        var xend = new PointF(img.Width, th + lh + ah / 2);
                        ctx.DrawLines(pen, new[] { new PointF(tw + lw + aw, th + lh + ah / 2), xend });
                        ctx.DrawLines(pen, new[] { new PointF(img.Width - 8, th + lh + 1), xend, new PointF(img.Width - 8, th + lh + ah - 1) });
                    });
                }

                // draw dtms
                if (true)
                {
                    int j = th + lh + ah;
                    foreach (var x in xs)
                    {
                        int i = tw + lw + aw;
                        foreach (var y in ys)
                        {
                            if (Table.TryGetValue(new Pair<string, string>(x, y), out var g))
                            {
                                DrawMatrix(img, g.TransMat, i, j, min, max, whot, cw, ch);
                            }

                            i += mw + sw;
                        }

                        j += mh + sh;
                    }
                }

                console.WriteLine("Saving to " + outfile);
                img.Save(outfile);
            }
        }

        /// <summary>
        /// Plots generated phenotypes
        /// </summary>
        public static void GenerateBestiary(TextWriter console, CliParams clips)
        {
            var filename = clips.Get("bestiary");
            var seed = clips.Get("seed", int.Parse, 42);
            var gmode = clips.Get("gmode");
            var outfile = clips.Get("out");
            var min = clips.Get("min", double.Parse, -1.0);
            var max = clips.Get("max", double.Parse, +1.0);
            var saturate = clips.Get("saturate", bool.Parse, false);
            bool whot = clips.IsSet("whot") || !clips.IsSet("bhot");
            int moduleCount = clips.Get("moduleCount", int.Parse, -1);
            int generations = clips.Get("generations", int.Parse, 0); // the number of generations to run each individual for after variation

            if (!System.IO.Path.GetFileName(outfile).Contains("."))
                outfile += ".png";
            
            bool mutate = gmode.Equals("mutate", StringComparison.InvariantCultureIgnoreCase);
            bool cycle = gmode.Equals("cycle", StringComparison.InvariantCultureIgnoreCase);
            var vp = mutate || cycle ? null : ParseVectorProvider(gmode);

            var exp = PopulationExperiment<DenseIndividual>.Load(filename);

            var count = clips.Get("count", int.Parse, cycle ? exp.PopulationConfig.ExperimentConfiguration.Size : 100);

            int exposureEpoch = clips.Get("exposureepoch", int.Parse, exp.Epoch);
            int targetIndex = clips.Get("target", int.Parse, exp.CurrentTargetIndex);

            var popConfig = exp.PopulationConfig;
            var config = popConfig.ExperimentConfiguration;
            var rrules = config.ReproductionRules;
            var drules = config.DevelopmentRules;
            var jrules = config.JudgementRules;
            var population = exp.Population;
            var range = rrules.InitialStateClamping;

            var rand = new MathNet.Numerics.Random.MersenneTwister(seed);

            List<Phenotype> phenotypes = new List<Phenotype>();

            var context = new ModelExecutionContext(rand);
            int n = exp.PopulationConfig.ExperimentConfiguration.Size;
            console.WriteLine($"Population size {population.Count}\nGnome size {n}");

            int popCount = population.Count;

            foreach (var template in population.ExtractAll())
            {
                var g = template.Genome;

                for (int i = 0; i < count; i++)
                {
                    DenseGenome g2 = mutate
                        ? g.Mutate(context, rrules)
                        : cycle
                        ? BinaryCycle(context, g, i)
                        : g.Clone(context, newInitialState: vp.RandomVector(range, n, rand));
                    var individual = DenseIndividual.Develop(g2, context, drules, template.Epigenetic);

                    if (generations > 0)
                    {
                        var target = config.Targets[0];

                        ExposureInformation exposureInformation = new ExposureInformation(generations);
                        target.NextExposure(rand, jrules, exp.Epoch, ref exposureInformation);
                        exposureInformation.ExposureDuration = generations; // ignore what the target says

                        var pop = new Population<DenseIndividual>(individual, popCount);
                        popConfig.CustomPopulationSpinner.SpinPopulation(pop, context, rrules, drules, jrules, target, popConfig.SelectorPreparer, generations, null, popConfig.EliteCount, popConfig.HillclimberMode);
                        individual = pop.PeekAll()[0];
                    }

                    var p = individual.Phenotype;
                    if (saturate)
                        p = p.Saturate(min, 0.0, max);
                    phenotypes.Add(p);
                }
            }

            if (clips.IsSet("distinct"))
            {
                phenotypes = phenotypes.Distinct(new SaturatedPhenotypeComparer()).ToList();
            }
            
            int w = moduleCount > 1 ? moduleCount : (int)Math.Sqrt(n);
            int h = (int)Math.Ceiling((double)n / w);
            int c = (int)Math.Sqrt(phenotypes.Count);
            int r = (int)Math.Ceiling((double)phenotypes.Count / c);
            using (var img = new Image<Rgba32>((w + 1) * c - 1, (h + 1) * r - 1))
            {
                for (int pi = 0; pi < phenotypes.Count; pi++)
                {
                    var p = phenotypes[pi];

                    int x = (w + 1) * (pi % c);
                    int y = (h + 1) * (pi / c);

                    DrawVector(img, p.Vector, x, y, w, min, max, whot);
                }

                console.WriteLine("Saving to " + outfile);
                img.Save(outfile);
            }

            Console.WriteLine(phenotypes.Distinct(new SaturatedPhenotypeComparer()).Count());
        }

        private class SaturatedPhenotypeComparer : IEqualityComparer<Phenotype>
        {
            public bool Equals(Phenotype x, Phenotype y)
            {
                if (Analysis.Matches(x.Vector, y.Vector))
                    return true;
                return x.Vector.SequenceEqual(y.Vector);
            }

            public int GetHashCode(Phenotype obj)
            {
                int i = 0;
                foreach (var v in obj.Vector)
                {
                    unchecked
                    {
                        i *= 7;
                        i += Math.Sign(v);
                    }
                }
                return i;
            }
        }

        private static DenseGenome BinaryCycle(ModelExecutionContext context, DenseGenome g, int i)
        {
            i = i % g.Size;
            var newg = g.InitialState.Clone();
            newg[i] *= -1;
            return g.Clone(context, newInitialState:newg);
        }

        public static void DrawNetwork(Image<Rgba32> img, Linear.Matrix<double> dtm, float r, float thickness, Rgba32 nodeCol, Rgba32 hotCol, Rgba32 coldCol, double threshold, SixLabors.Primitives.PointF[] nodeLocs)
        {
            int size = dtm.RowCount;

            img.Mutate(ctx => 
            {
                // draw connectors
                for (int i = 0; i < size; i++)
                {        
                    for (int j = 0; j < size; j++)
                    {
                        double mag = Math.Abs(dtm[i, j]);
                        if (mag < threshold)
                            continue;

                        // effect of i on j
                        var pen = new Pen(dtm[i, j] > 0 ? hotCol : coldCol, (float)(mag / threshold));

                        var ni = nodeLocs[i];
                        var nj = nodeLocs[j];
                        var mid = (ni + nj) / 2f;
                        var diff = nj - ni;
                        diff = new PointF(-diff.Y, diff.X);
                        var side = mid + diff * 0.2f;

                        var pts = new[] { ni, side, side, nj };

                        ctx.DrawBeziers(pen, pts);
                    }
                }

                // draw noes
                var nodePen = new Pen(nodeCol, 1);
                var nodeFill = new SolidBrush(Rgba32.White);
                foreach (var nl in nodeLocs)
                {
                    var ellipse = new SixLabors.Shapes.EllipsePolygon(nl.X, nl.Y, r, r);
                    ctx.Draw(nodePen, ellipse);
                    ctx.Fill(nodeFill, ellipse);
                }
            });
        }

        public static void DrawMatrix(Image<Rgba32> img, Linear.Matrix<double> m, int x, int y, double min, double max, bool whot, int cw, int ch)
        {
            int w = m.ColumnCount;
            int h = m.RowCount;

            for (int i = 0; i < w * cw; i++)
            {
                for (int j = 0; j < h * ch; j++)
                {
                    img[x + i, y + j] = Colour(m[j / ch, i / cw], min, max, whot);
                }
            }
        }

        public static void DrawVector(Image<Rgba32> img, IEnumerable<double> v, int x, int y, int w, double min, double max, bool whot)
        {
            int i = 0;
            int j = 0;
            foreach (double e in v)
            {
                img[x + i, y + j] = Colour(e, min, max, whot);

                if (++i >= w)
                {
                    i = 0;
                    j++;
                }
            }
        }

        public static Rgba32 Colour(double e, double min, double max, bool whot)
        {
            double d = (e - min) / (max - min);
            if (!whot)
                d = 1.0 - d;
            Rgba32 col;

            if (d < 0)
            {
                col = Color.Blue;
            }
            else if (d > 1)
            {
                col = Color.Red;
            }
            else
            {
                byte v = (byte)(d * 255);
                col = new Rgba32(v, v, v);
            }

            return col;
        }

        public static IRandomVectorProvider ParseVectorProvider(string name)
        {
            if (name.Equals("Binary", StringComparison.InvariantCultureIgnoreCase))
                return BinaryRandomVectorProvider.Instance;
            if (name.Equals("Uniform", StringComparison.InvariantCultureIgnoreCase))
                return UniformRandomVectorProvider.Instance;
            if (name.Equals("Zero", StringComparison.InvariantCultureIgnoreCase))
                return ZeroVectorProvider.Instance;

            throw new Exception("Unrecognised VectorProvider name, consider one of:\n" +
                " - Binary\n" +
                " - Uniform\n" +
                " - Zero");
        }
    }
}
