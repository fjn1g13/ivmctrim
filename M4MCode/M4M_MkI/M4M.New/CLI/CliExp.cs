﻿using M4M.Epistatics;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4M
{
    public interface IDensePopulationExperimentFactoryCliFactory
    {
        string Name { get; }
        DensePopulationExperimentFeedbackFactory Prepare(TextWriter console, CliParams clips);
    }

    public class CliRun : ICliProvider
    {
        public CliRun(CliExp cliExp)
        {
            CliExp = cliExp;
        }

        public string Key => "run";

        public CliExp CliExp { get; }

        public void Run(TextWriter console, CliParams clips)
        {
            string defSuperDir = clips.Get("run");

            if (defSuperDir == null)
            {
                console.WriteLine("Supply, as a minimum, a directory containing directories containing 'epoch0savestarter.dat' population experiment definitions.\n" +
                    "Optionally, you may provide a postfix and alternative topdir, and whatever stuff your feedback provider will take.");

                console.WriteLine("Your feedback provider is " + CliExp.FeedbackFactoryFactory.Name);

                return;
            }

            if (clips.IsSet("help"))
            {
                console.WriteLine("CliRun runs a directory full of directories containing experiments.");
                console.WriteLine("Parameters include:\n" + 
                    " - expfilename, the name of the experiment files to run\n" +
                    " - postfix, the postfix to append\n" +
                    " - continue, whether to continue an interrupted run\n" +
                    " - stl, when continuing, use the second-to-last expfile\n" +
                    " - seed, the base seed, which is incremented for each sub-experiment\n" +
                    " - repeats, the number of repeats to run\n");

                CliExp.PrintFeedbackHelp(console);
            }

            string expfilename = clips.Get("expfilename", "epoch0savestarter.dat");
            string postfix = clips.Get("postfix", "");
            
            bool @continue = clips.IsSet("continue");
            bool stl = clips.IsSet("stl");

            string superDir = clips.Get("topdir", defSuperDir + "runs" + postfix + "/");

            List<Action> actions = new List<Action>();
            
            int? seed = clips.Get("seed", s => (int?)int.Parse(s), null);
            int repeats = clips.Get("repeats", int.Parse, 1);
            int r0 = clips.Get("r0", int.Parse, 0);

            if (seed != null)
                seed += r0;

            string[] dirs = System.IO.Directory.GetDirectories(defSuperDir);
            Array.Sort(dirs, StringComparer.Ordinal);

            foreach (var d in dirs)
            {
                var expAddress = @continue
                    ? (stl ? FindNewestSicherSave(d) : FindNewestSave(d))
                    : System.IO.Path.Combine(d, expfilename);
                var topdir = d.Replace(defSuperDir, superDir); // soooo dodgy
                if (string.IsNullOrEmpty(topdir))
                    topdir = ".";

                if (System.IO.File.Exists(expAddress))
                {
                    for (int r = r0; r < r0 + repeats; r++)
                    {
                        string cliPrefix = "R" + actions.Count + " ";
                        
                        string repeatPostfix = repeats <= 1 ? "" : "r" + r;
                        int dseed = seed ?? CliExp.SeedSource.Next();

                        actions.Add(() =>
                        {
                            try
                            {
                                CliExp.RunExperiment(console, clips, cliPrefix, expAddress, topdir + repeatPostfix, dseed);
                            }
                            catch (Exception ex)
                            {
                                console.WriteLine(cliPrefix + "Crashed: " + ex.Message);
                                console.WriteLine(cliPrefix + "StackTrace: " + ex.StackTrace);
                            }
                        });
                        
                        if (seed != null)
                            seed = seed + 1;
                    }
                }
            }

            console.WriteLine("Running " + actions.Count + " experiments");
            Parallel.Invoke(actions.ToArray());
        }

        private static string FindNewestSave(string dir)
        {
            return System.IO.Directory.GetFiles(dir).Where(f => f.EndsWith("save.dat")).ArgMax(System.IO.File.GetLastWriteTimeUtc);
        }

        private static string FindNewestSicherSave(string dir)
        {
            return System.IO.Directory.GetFiles(dir).Where(f => f.EndsWith("save.dat")).OrderByDescending(System.IO.File.GetLastWriteTimeUtc).Take(2).Last();
        }
    }

    public class CliExp : ICliProvider
    {
        public static readonly MathNet.Numerics.Random.RandomSource SeedSource = new MathNet.Numerics.Random.MersenneTwister(true);
        
        public CliExp(IDensePopulationExperimentFactoryCliFactory feedbackFactoryFactory)
        {
            FeedbackFactoryFactory = feedbackFactoryFactory;
        }

        public string Key => "exp";

        public IDensePopulationExperimentFactoryCliFactory FeedbackFactoryFactory { get; }

        public void PrintFeedbackHelp(TextWriter console)
        {
            console.WriteLine("Feedback Parameters include:\n" + 
                " - savePeriodEpochs, the interval between writing an experiment file\n" +
                " - savePeriodSeconds, the maximum number of seconds to wait before writing an experiment file\n" +
                " - sampleMax, controls the number of samples in trajectorty (rcs, fitness, ist, and pst) files\n" +
                " - wholeSamplePeriod, interval between wholesamples\n" +
                " - wholeSampleWritePeriod, interval between writing wholesamples\n" +
                " - tracePeriod, interval between tracees\n" +
                " - traceDuration, number of epochs to trace for each tracee\n" +
                " - traceSamplePeriod, within-tracee sample period\n" +
                " - plotperiod, interval in epochs between producing trajectory files\n" +
                " - saveperiod, interval between writing out a genome file\n" +
                " - configurators, list of additional feedback configurators to use\n");
        }

        public void Run(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("CliExp runs a single experiment.");
                console.WriteLine("Parameters include:\n" + 
                    " - exp, the name of the experiment files to run\n" +
                    " - topdir, the directory wherein to run\n" +
                    " - repeats, the number of repeats to run\n" +
                    " - seed, the seed\n");

                PrintFeedbackHelp(console);
            }

            string expAddress = clips.Get("exp");
            string topdir = clips.Get("topdir", System.IO.Path.GetDirectoryName(expAddress));
            string cliPrefix = clips.Get("cliPrefix", "");
            if (cliPrefix != "")
                cliPrefix += " ";

            int seed = clips.Get("seed", s => (int?)int.Parse(s), null) ?? SeedSource.Next();

            int repeats = clips.Get("repeats", int.Parse, 0);
            int r0 = clips.Get("r0", int.Parse, 0);

            seed += r0;

            if (repeats > 0)
            {
                string innerDir = clips.Get("innerdir", "");
                string innerExp = clips.Get("innerexp", null);

                List<Action> actions = new List<Action>();

                if (string.IsNullOrEmpty(topdir))
                    topdir = ".";

                for (int r = r0; r < r0 + repeats; r++)
                {
                    string rCliPrefix = repeats > 1 ? "R" + actions.Count + cliPrefix : cliPrefix;
                    int rSeed = seed;

                    string repeatPostfix = repeats < 1 ? "" : "r" + r;
                    string rTopDir = System.IO.Path.Combine(topdir, repeatPostfix);
                    string rInnerDir = System.IO.Path.Combine(topdir, repeatPostfix, innerExp ?? "");
                    string rExpAdr = innerExp == null ? expAddress : System.IO.Path.Combine(rTopDir, innerExp);

                    actions.Add(() =>
                    {
                        try
                        {
                            RunExperiment(console, clips, rCliPrefix, rExpAdr, rInnerDir, rSeed);
                        }
                        catch (Exception ex)
                        {
                            console.WriteLine(rCliPrefix + "Crashed: " + ex.Message);
                            console.WriteLine(rCliPrefix + "StackTrace: " + ex.StackTrace);
                        }
                    });

                    seed++;
                }

                console.WriteLine("Running " + actions.Count + " experiments");
                Parallel.Invoke(actions.ToArray());
            }
            else
            {
                RunExperiment(console, clips, cliPrefix, expAddress, topdir, seed);
            }
        }

        public void RunExperiment(TextWriter console, CliParams clips, string cliPrefix, string expAddress, string topdir, int? seed)
        {
            string postfix2 = clips.Get("postfix2", "");
            bool appendTimestamp = clips.IsSet("appendTimestamp");

            bool @continue = clips.IsSet("continue");

            int savePeriodEpochs = clips.Get("savePeriodEpochs", Misc.ParseInt, 500);
            int savePeriodSeconds = clips.Get("savePeriodSeconds", Misc.ParseInt, 60*60);

            bool enableMatrixPools = clips.Get("enableMatrixPools", bool.Parse, false);
            MatrixPool.EnableMatrixPools = enableMatrixPools;
            if (enableMatrixPools)
            {
                console.WriteLine("Warning: Matrix Pools are enabled!");
            }

            var feedbackFactory = FeedbackFactoryFactory.Prepare(console, clips);

            if (@continue)
                ContinueExperiment(console, cliPrefix, expAddress, topdir, postfix2, seed, feedbackFactory, appendTimestamp, savePeriodEpochs, savePeriodSeconds);
            else
                RunExperiment(console, cliPrefix, expAddress, topdir, postfix2, seed, feedbackFactory, appendTimestamp, savePeriodEpochs, savePeriodSeconds);
        }

        public void RunExperiment(TextWriter console, string cliPrefix, string expAddress, string topdir, string postfix2, int? seed, DensePopulationExperimentFeedbackFactory feedbackFactory, bool appendTimestamp, int savePeriodEpochs, int savePeriodSeconds)
        {
            topdir += postfix2;

            if (string.IsNullOrEmpty(topdir))
                topdir = ".";

            console.WriteLine(cliPrefix + "Running prepackaged DenseIndividual experiment");
            console.WriteLine(cliPrefix + "Definition at " + expAddress);
            console.WriteLine(cliPrefix + "Running into  " + topdir);
            if (seed != null)
                console.WriteLine(cliPrefix + "Seed " + seed);

            var popExperiment = PopulationExperiment<DenseIndividual>.Load(expAddress);

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Restart();

            console.WriteLine(cliPrefix + popExperiment.PopulationConfig.ExperimentConfiguration.Epochs + " epochs of " + popExperiment.PopulationConfig.ExperimentConfiguration.GenerationsPerTargetPerEpoch + " generations");
            LowRunners.RunExperiment(console, cliPrefix, topdir, popExperiment, seed, feedbackFactory, null, appendTimestamp, savePeriodEpochs, savePeriodSeconds);

            sw.Stop();
            console.WriteLine(cliPrefix + "Took " + sw.ElapsedMilliseconds + "ms");
        }

        public void ContinueExperiment(TextWriter console, string cliPrefix, string expAddress, string topdir, string postfix2, int? seed, DensePopulationExperimentFeedbackFactory feedbackFactory, bool appendTimestamp, int savePeriodEpochs, int savePeriodSeconds)
        {
            topdir += postfix2;
            
            if (string.IsNullOrEmpty(topdir))
                topdir = ".";

            console.WriteLine(cliPrefix + "Continuing prepackaged DenseIndividual experiment");
            console.WriteLine(cliPrefix + "Definition at " + expAddress);
            console.WriteLine(cliPrefix + "Running into  " + topdir);
            if (seed != null)
                console.WriteLine(cliPrefix + "Seed " + seed);

            var popExperiment = PopulationExperiment<DenseIndividual>.Load(expAddress);

            int epochsLeft = popExperiment.PopulationConfig.ExperimentConfiguration.Epochs - popExperiment.Epoch;
            if (epochsLeft < 0)
            {
                console.WriteLine(cliPrefix + " Experiment already finished");
                return;
            }
            
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Restart();

            console.WriteLine(cliPrefix + epochsLeft + " epochs of " + popExperiment.PopulationConfig.ExperimentConfiguration.GenerationsPerTargetPerEpoch + " generations");
            LowRunners.ContinueExperiment(console, cliPrefix, topdir, popExperiment, seed, feedbackFactory, null, appendTimestamp, savePeriodEpochs, savePeriodSeconds);

            sw.Stop();
            console.WriteLine(cliPrefix + "Took " + sw.ElapsedMilliseconds + "ms");
        }
    }
    
    public class CommonFeedbackFactoryCliFactory : IDensePopulationExperimentFactoryCliFactory
    {
        public CommonFeedbackFactoryCliFactory(IEnumerable<IDensePoulationExperimentFeedbackConfiguratorCliFactory> configuratorFactories)
        {
            ConfiguratorFactories = configuratorFactories;
        }

        public string Name => "CommonFeedbackFactoryCliFactory";

        public IEnumerable<IDensePoulationExperimentFeedbackConfiguratorCliFactory> ConfiguratorFactories { get; }

        public DensePopulationExperimentFeedbackFactory Prepare(TextWriter console, CliParams clips)
        {
            int sampleMax = clips.Get("sampleMax", Misc.ParseInt, 2000); // not so much expensive, as we just don't want too many because RCSViewer will become unhappy
            int wholeSamplePeriod = clips.Get("wholeSamplePeriod", Misc.ParseInt, 10); // expensive
            int wholeSampleWritePeriod = clips.Get("wholeSampleWritePeriod", Misc.ParseInt, Math.Max(2000, wholeSamplePeriod * 200));
            int tracePeriod = clips.Get("tracePeriod", Misc.ParseInt, 0); // very expensive
            int traceDuration = clips.Get("traceDuration", Misc.ParseInt, 1); // very expensive
            int traceSamplePeriod = clips.Get("traceSamplePeriod", Misc.ParseInt, 1); // very expensive
            int plotPeriod = clips.Get("plotperiod", Misc.ParseInt, 5000); // not inexpensive
            int savePeriod = clips.Get("saveperiod", Misc.ParseInt, plotPeriod); // basically pointless, but not expensive
            var rememberTraces = clips.Get("rememberTraces", bool.Parse, false); // completely pointless, very expensive
            var noRcs = clips.Get("norcs", bool.Parse, false); // reduces output, not memory usage

            var feedbackFactory = CommonDensePopulationExperimentFeedback.SimpleCommonDensePopulationExperimentFeedback(
                console: console,
                sampleMax: sampleMax,
                plotPeriod: plotPeriod,
                savePeriod: savePeriod,
                wholeSamplePeriod: wholeSamplePeriod,
                wholeSampleWritePeriod: wholeSampleWritePeriod,
                tracePeriod: tracePeriod,
                traceDuration: traceDuration,
                traceSamplePeriod: traceSamplePeriod,
                rememberTraces: rememberTraces,
                noRcs: noRcs
                );

            var configuratorFactoryNames = clips.Get("configurators", s => new HashSet<string>(s.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries), StringComparer.InvariantCultureIgnoreCase), new HashSet<string>());
            var configuratorFactories = ConfiguratorFactories.Where(cf => configuratorFactoryNames.Contains(cf.Name)).ToArray();
            var configurators = configuratorFactories.Select(cf => cf.Prepare(console, clips)).ToArray();
            console.WriteLine("Configurators: " + string.Join(", ", configuratorFactories.Select(cf => cf.Name)));

            return (RandomSource rand, PopulationExperimentConfig<DenseIndividual> populationExperimentConfig, PopulationExperimentFeedback<DenseIndividual> feedback)
                =>
            {
                var denseFeedback = feedbackFactory(rand, populationExperimentConfig, feedback);

                foreach (var configurator in configurators)
                {
                    configurator(denseFeedback);
                }

                return denseFeedback;
            };
        }
        
        public static readonly List<IDensePoulationExperimentFeedbackConfiguratorCliFactory> DefaultConfiguratorFactories = new List<IDensePoulationExperimentFeedbackConfiguratorCliFactory>
        {
            new IvmcDensePoulationExperimentFeedbackConfiguratorCliFactory(),
            new SatFitnessExperimentFeedbackConfiguratorCliFactory(),
            new SudokuPoulationExperimentFeedbackConfiguratorCliFactory(),
            new NQueensPoulationExperimentFeedbackConfiguratorCliFactory(),
        };
    }

    public interface IDensePoulationExperimentFeedbackConfiguratorCliFactory
    {
        string Name { get; }
        DensePopulationExperimentFeedbackConfigurator Prepare(TextWriter console, CliParams clips);
    }

    public class IvmcDensePoulationExperimentFeedbackConfiguratorCliFactory : IDensePoulationExperimentFeedbackConfiguratorCliFactory
    {
        public string Name => "IvmcDenseFeedback";

        public DensePopulationExperimentFeedbackConfigurator Prepare(TextWriter console, CliParams clips)
        {
            int plotPeriod = clips.Get("ivmcSwitchPlotperiod", Misc.ParseInt, clips.Get("plotperiod", Misc.ParseInt, 5000)); // not inexpensive
            int resolution = clips.Get("ivmcSwitchResolution", Misc.ParseInt, 1); // not inexpensive
            bool recordProper = clips.Get("ivmcRecordPropers", bool.Parse, false);

            return (IDensePopulationExperimentFeedback densePopulationExperimentFeedback) =>
            {
                var target = densePopulationExperimentFeedback.PopulationConfig.ExperimentConfiguration.Targets[0];

                if (target is Epistatics.IIvmcProperTarget ivmcProperTarget)
                {
                    var proper = ivmcProperTarget.Proper;
                    int moduleCount = ivmcProperTarget.Proper.ModuleCount;

                    Epistatics.IvmcProperFeedback ivmcProperFeedback = new Epistatics.IvmcProperFeedback(resolution, plotPeriod, recordProper, moduleCount);
                    ivmcProperFeedback.Attach(densePopulationExperimentFeedback);
                    console.WriteLine("IvmcProperFeedback attached");
                }
                else
                {
                    console.WriteLine("IvmcProperFeedback NOT attached: target is not an IIvmcProperTarget");
                }
            };
        }
    }

    public class SudokuPoulationExperimentFeedbackConfiguratorCliFactory : IDensePoulationExperimentFeedbackConfiguratorCliFactory
    {
        public string Name => "SudokuFeedback";

        public DensePopulationExperimentFeedbackConfigurator Prepare(TextWriter console, CliParams clips)
        {
            int sudokuResolution = clips.Get("sudokuresolution", Misc.ParseInt, 1);
            int plotPeriod = clips.Get("plotperiod", Misc.ParseInt, 5000);

            return (IDensePopulationExperimentFeedback densePopulationExperimentFeedback) =>
            {
                var target = densePopulationExperimentFeedback.PopulationConfig.ExperimentConfiguration.Targets[0];

                if (target is Epistatics.CorrelationMatrixTarget cmt)
                {
                    Epistatics.SudokuFeedback sudokuFeedback = new Epistatics.SudokuFeedback(sudokuResolution, plotPeriod);
                    sudokuFeedback.Attach(densePopulationExperimentFeedback);
                    console.WriteLine("SudokuFeedback attached");
                }
                else
                {
                    console.WriteLine("SudokuFeedback NOT attached: target is not a CorrelationMatrixTarget");
                }
            };
        }
    }

    public class NQueensPoulationExperimentFeedbackConfiguratorCliFactory : IDensePoulationExperimentFeedbackConfiguratorCliFactory
    {
        public string Name => "NQueensFeedback";

        public DensePopulationExperimentFeedbackConfigurator Prepare(TextWriter console, CliParams clips)
        {
            int NQueensResolution = clips.Get("nqueensresolution", Misc.ParseInt, 1);
            int plotPeriod = clips.Get("plotperiod", Misc.ParseInt, 5000);
            var threshold = clips.Get("nqueensthreshold", double.Parse, 0.5);

            return (IDensePopulationExperimentFeedback densePopulationExperimentFeedback) =>
            {
                var target = densePopulationExperimentFeedback.PopulationConfig.ExperimentConfiguration.Targets[0];

                if (target is Epistatics.CorrelationMatrixTarget cmt)
                {
                    Epistatics.NQueensFeedback NQueensFeedback = new Epistatics.NQueensFeedback(NQueensResolution, plotPeriod, threshold);
                    NQueensFeedback.Attach(densePopulationExperimentFeedback);
                    console.WriteLine("NQueensFeedback attached");
                }
                else
                {
                    console.WriteLine("NQueensFeedback NOT attached: target is not a CorrelationMatrixTarget");
                }
            };
        }
    }

    public class SatFitnessExperimentFeedbackConfiguratorCliFactory : IDensePoulationExperimentFeedbackConfiguratorCliFactory
    {
        public string Name => "SatFitnessFeedback";

        public DensePopulationExperimentFeedbackConfigurator Prepare(TextWriter console, CliParams clips)
        {
            int samplePeriod = clips.Get("SatFitnessSampleRate", Misc.ParseInt, 1);
            int plotPeriod = clips.Get("SatFitnessPlotPeriod", Misc.ParseInt, clips.Get("PlotPeriod", Misc.ParseInt, -1));

            var satMin = clips.Get("satMin", double.Parse, -1.0);
            var satThreshold = clips.Get("satThreshold", double.Parse, 0.0);
            var satMax = clips.Get("satMax", double.Parse, +1.0);

            return (IDensePopulationExperimentFeedback densePopulationExperimentFeedback) =>
            {
                var feedback = densePopulationExperimentFeedback.Feedback;
                var jrules = densePopulationExperimentFeedback.PopulationConfig.ExperimentConfiguration.JudgementRules;
                var targets = densePopulationExperimentFeedback.PopulationConfig.ExperimentConfiguration.Targets.Select(t => t is SaturationTarget ? t : new SaturationTarget(t, satMin, satThreshold, satMax)).ToArray();
                
                var satFitnessFeedback = new ProjectedFitnessFeedback<DenseIndividual>(feedback, targets, jrules, "Sat", samplePeriod, plotPeriod);
                console.WriteLine("SatFitnessFeedback attached");
            };
        }
    }
}
