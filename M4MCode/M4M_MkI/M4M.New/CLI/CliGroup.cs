using M4M;
using M4M.ExperimentGroups;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace M4M
{
    public class CliStackGroup : ICliProvider
    {
        public string Key => "StackGroup";

        public void Run(TextWriter console, CliParams clips)
        {
            StackGroup(clips);
        }

        public static void StackGroup(CliParams clips)
        {
            var dir = clips.Get("StackGroup");
            var groupName = clips.Get("GroupName");
            var trajFilename = clips.Get("TrajFilename");
            var trajectory = clips.Get("TrajectoryIndex", int.Parse, 0);
            var blockName = clips.Get("Block", null);
            var prefix = clips.Get("Prefix", "trajTraces");
            var blockTerminator = clips.Get("blockTerminator", "runs");

            if (blockName == null)
            {
                var blocks = ExpInfo.EnumerateExps(dir, groupName, trajFilename, blockTerminator).GroupBy(e => e.Block);
                foreach (var block in blocks)
                {
                    var exps = block.AsEnumerable().OrderBy(e => e.Run).ThenBy(e => e.Repeat);
                    var trajs = StackTrajectories(exps, trajFilename, trajectory, out var samplePeriod);
                    Analysis.SaveTrajectories(prefix + block.First().Block + ".dat", trajs, samplePeriod);
                }
            }
            else
            {
                var trajs = StackTrajectories(dir, groupName, blockName, blockTerminator, trajFilename, trajectory, out var samplePeriod);
                Analysis.SaveTrajectories(prefix + blockName + ".dat", trajs, samplePeriod);
            }
        }

        public static double[][] StackTrajectories(string dir, string groupName, string blockName, string blockTerminator, string trajFilename, int trajectory, out int samplePeriod)
        {
            var exps = ExpInfo.EnumerateExps(dir, groupName, trajFilename, blockTerminator)
                .Where(e => e.Block.Equals(blockName, StringComparison.InvariantCultureIgnoreCase))
                .OrderBy(e => e.Run).ThenBy(e => e.Repeat);
            var trajs = StackTrajectories(exps, trajFilename, trajectory, out samplePeriod);
            return trajs;
        }

        public static double[][] StackTrajectories(IEnumerable<ExpInfo> exps, string trajFilename, int trajectory, out int samplePeriod)
        {
            int _samplePeriod = -1;

            var trajs = exps
                .Select(e => ExpSamplers.SampleTrajectory(e, trajFilename, trajectory, "", out _samplePeriod))
                .Select(s => s.Samples.ToArray())
                .ToArray();

            samplePeriod = _samplePeriod;
            return trajs;
        }
    }

    public class CliSatGroup : ICliProvider
    {
        public string Key => "SatGroup";

        public void Run(TextWriter console, CliParams clips)
        {
            SatGroup(console, clips);
        }

        public static void SatGroup(TextWriter console, CliParams clips)
        {
            var dir = clips.Get("SatGroup");
            var groupName = clips.Get("GroupName");
            var satExpFile = clips.Get("SatExpFile");
            var wholesampleFilename = clips.Get("WholesampleFilename");
            var blockTitle = clips.Get("BlockTitle");
            var projectionMode = clips.Get("ProjectionMode", CliProject.ParseProjectionMode, ProjectionMode.EvalOnly);
            var saturateExistingTarget = clips.Get("SaturateExistingTarget", bool.Parse, false);
            var blockTerminator = clips.Get("blockTerminator", "runs");

            var task = clips.Get("task", "all");

            var dirs = dir.Split(';');
            var groupNames = groupName.Split(';');

            if (task.Equals("all", StringComparison.InvariantCultureIgnoreCase))
            {
                SatGroupAll(console, clips, dirs, groupNames, satExpFile, wholesampleFilename, blockTitle, blockTerminator, projectionMode, saturateExistingTarget);
            }
            else if (task.Equals("project", StringComparison.InvariantCultureIgnoreCase))
            {
                ProjectSatGroup<DenseIndividual>(console, clips, dirs, groupNames, satExpFile, wholesampleFilename, blockTerminator, projectionMode, saturateExistingTarget);
            }
        }

        // TODO: this should really be broken up into one task that produces the saturated wholesamples, and the other tasks which do the work
        // These could appear as a CliBlockAnalysis class or something (err... 2 of them are plotters...)
        public static void SatGroupAll(TextWriter console, CliParams clips, IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string satExpFile, string wholesampleFilename, string blockTitle, string blockTerminator, ProjectionMode projectionMode, bool saturateExistingTarget)
        {
            if (dirs.Count == 0)
                throw new ArgumentException("Must provide atleast one Dir and GroupName");
            if (dirs.Count != groupNames.Count)
                throw new ArgumentException("Must provide the same number of Dirs and groupNames");

            var sats = ProjectSatGroup<DenseIndividual>(console, clips, dirs, groupNames, satExpFile, wholesampleFilename, blockTerminator, projectionMode, saturateExistingTarget);

            var aliases = GroupPlotHelpers.Aliases(clips);

            var traceesplot = GroupPlotting.PlotBlockTracees(sats, ws => ws.Judgements[0].Judgement.CombinedFitness);
            SimplePdfPlotExporter.ExportToPdf(traceesplot, "SatTracees.pdf", 300, 300, false, false, null);

            var boxplot = GroupPlotting.PlotBox(sats, "Mean fitness distribution", blockTitle, "Fitness", aliases, GroupPlotting.TerminalWholesampleSelector);
            SimplePdfPlotExporter.ExportToPdf(boxplot, "SatBoxPlots.pdf", 300, 300, false, false, null);
        }

        public static List<ExpWholeSamples<TIndividual>> ProjectSatGroup<TIndividual>(TextWriter console, CliParams clips, IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string satExpFile, string wholesampleFilename, string blockTerminator, ProjectionMode projectionMode, bool saturateExistingTarget) where TIndividual : IIndividual<TIndividual>
        {
            if (dirs.Count == 0)
                throw new ArgumentException("Must provide atleast one Dir and GroupName");
            if (dirs.Count != groupNames.Count)
                throw new ArgumentException("Must provide the same number of Dirs and groupNames");

            var sats = new List<ExpWholeSamples<TIndividual>>();

            for (int i = 0; i < dirs.Count; i++)
            {
                var sed = new SaturatedExperimentData<TIndividual>(console, dirs[i], groupNames[i], satExpFile, wholesampleFilename, blockTerminator, projectionMode, saturateExistingTarget);
                sats.AddRange(sed.Sats.OrderBy(e => e.ExpInfo.Block));
                SaturatedExperimentData<TIndividual>.ProcessWholeSamples(console, sats, sed.SatExp, null);
            }

            return sats;
        }
    }

    public class CliRunonGroup : ICliProvider
    {
        public string Key => "RunonGroup";

        public void Run(TextWriter console, CliParams clips)
        {
            RunonGroup(console, clips);
        }

        public static void RunonGroup(TextWriter console, CliParams clips)
        {
            var dir = clips.Get("RunonGroup");
            var groupName = clips.Get("GroupName");
            var runonExpFile = clips.Get("RunonExpFile");
            var expFilename = clips.Get("ExpFilename");
            var blockTitle = clips.Get("BlockTitle");
            var postfix = clips.Get("Postfix");
            var blockTerminator = clips.Get("blockTerminator", "runs");

            var width = clips.Get("width", double.Parse, 600);
            var height = clips.Get("height", double.Parse, 300);

            double[] thresholds = clips.Get("threshold", s => s.Split(';').Select(double.Parse).ToArray(), new double[0]);

            //
            var runons = RunonGroup<DenseIndividual>(console, dir.Split(';'), groupName.Split(';'), runonExpFile, expFilename, postfix, blockTerminator);

            var boxplot = GroupPlotting.PlotBox(runons, "Mean fitness distribution", blockTitle, "Fitness", null, GroupPlotting.MeanWholesampleSelector);
            foreach (var threshold in thresholds)
            {
                boxplot.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Horizontal, Y = threshold, Color = OxyPlot.OxyColors.Gray });
            }
            SimplePdfPlotExporter.ExportToPdf(boxplot, "RunonBoxPlots.pdf", width, height, false, false, null);

            foreach (var threshold in thresholds)
            {
                console.WriteLine($"# Threshold {threshold}");

                foreach (var exp in runons.GroupBy(e => e.ExpInfo.Block).OrderBy(ct => ct.Key))
                {
                    var passRate = (double)exp.Count(ws => ws.Samples.Average(s => s.Judgements[0].Judgement.Benefit) >= threshold) / exp.Count();
                    console.WriteLine(exp.Key + ": " + passRate);
                }

                console.WriteLine();
            }
        }

        public static List<ExpWholeSamples<TIndividual>> RunonGroup<TIndividual>(TextWriter console, IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string runonExpFile, string expFilename, string postfix, string blockTerminator) where TIndividual : IIndividual<TIndividual>
        {
            if (dirs.Count == 0)
                throw new ArgumentException("Must provide atleast one Dir and GroupName");
            if (dirs.Count != groupNames.Count)
                throw new ArgumentException("Must provide the same number of Dirs and groupNames");

            var runons = new List<ExpWholeSamples<TIndividual>>();

            for (int i = 0; i < dirs.Count; i++)
            {
                var runon = new RunonExperimentData<TIndividual>(console, dirs[i], groupNames[i], runonExpFile, expFilename, postfix, blockTerminator);
                runons.AddRange(runon.Runons.OrderBy(e => e.ExpInfo.Block));
            }

            return runons;
        }
    }

    public class CliTimeToHierarchyGroup : ICliProvider
    {
        public string Key => "TimeToHierarchyGroup";

        public void Run(TextWriter console, CliParams clips)
        {
            StackGroup(clips);
        }

        public static void StackGroup(CliParams clips)
        {
            var dir = clips.Get("StackGroup");
            var groupName = clips.Get("GroupName");
            var trajFilename = clips.Get("TrajFilename");
            var trajectory = clips.Get("TrajectoryIndex", int.Parse, 0);
            var blockName = clips.Get("Block", null);
            var prefix = clips.Get("Prefix", "trajTraces");
            var blockTerminator = clips.Get("blockTerminator", "runs");

            if (blockName == null)
            {
                var blocks = ExpInfo.EnumerateExps(dir, groupName, trajFilename, blockTerminator).GroupBy(e => e.Block);
                foreach (var block in blocks)
                {
                    var exps = block.AsEnumerable().OrderBy(e => e.Run).ThenBy(e => e.Repeat);
                    var trajs = StackTrajectories(exps, trajFilename, trajectory, out var samplePeriod);
                    Analysis.SaveTrajectories(prefix + block.First().Block + ".dat", trajs, samplePeriod);
                }
            }
            else
            {
                var trajs = StackTrajectories(dir, groupName, blockName, blockTerminator, trajFilename, trajectory, out var samplePeriod);
                Analysis.SaveTrajectories(prefix + blockName + ".dat", trajs, samplePeriod);
            }
        }

        public static double[][] StackTrajectories(string dir, string groupName, string blockName, string blockTerminator, string trajFilename, int trajectory, out int samplePeriod)
        {
            var exps = ExpInfo.EnumerateExps(dir, groupName, trajFilename, blockTerminator)
                .Where(e => e.Block.Equals(blockName, StringComparison.InvariantCultureIgnoreCase))
                .OrderBy(e => e.Run).ThenBy(e => e.Repeat);
            var trajs = StackTrajectories(exps, trajFilename, trajectory, out samplePeriod);
            return trajs;
        }

        public static double[][] StackTrajectories(IEnumerable<ExpInfo> exps, string trajFilename, int trajectory, out int samplePeriod)
        {
            int _samplePeriod = -1;

            var trajs = exps
                .Select(e => ExpSamplers.SampleTrajectory(e, trajFilename, trajectory, "", out _samplePeriod))
                .Select(s => s.Samples.ToArray())
                .ToArray();

            samplePeriod = _samplePeriod;
            return trajs;
        }
    }
}
