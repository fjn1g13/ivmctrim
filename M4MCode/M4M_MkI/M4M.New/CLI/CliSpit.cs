using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M
{
    public class CliSpit : ICliProvider
    {
        public string Key => "spit";

        public void Run(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Spits out config files");
                console.WriteLine(" - spit, the expfile wherefor to spit out a config listing");
                console.WriteLine(" - out, where to save the config");
            }
            
            string expFile = clips.Get("spit");
            string outFile = clips.Get("out", "config.txt");
            
            // load the experiment - inferring its type - and give us an IExperimentExtractor to work with
            var expExtractor = PopulationExperimentHelpers.LoadUnknownType(expFile, new ExperimentExtractorExperimentReceiver());

            expExtractor.WriteOutConfig(outFile);
        }
    }
}
