﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public interface ICliMiscPlotter
    {
        string Key { get; }
        PlotModel Plot(TextWriter console, CliParams clips, string title);
    }

    public class CliPlotMisc : ICliPlotter
    {
        public readonly static ICliMiscPlotter[] DefaultMiscPlotters = new ICliMiscPlotter[] {
            new CliMiscPlotPitOfDespair(),
            new CliMiscPlotDevTraj(),
            new CliMiscPlotBeam(),
            new CliMiscPlotExpBeam(),
            new CliMiscPlotExpsBeam(),
            new CliMiscPlotIvmcSwitchness(),
            new CliMiscPlotIvmcProperSwitchness(),
            new CliMiscPlotIvmcModel(),
            new CliMiscPlotCbbnkStepModel(),
            CliMiscEtaThings.IvmcProper,
            CliMiscEtaThings.IvmcSplit,
            CliMiscEtaEffective.IvmcProper,
            CliMiscEtaEffective.IvmcSplit,
            new CliMiscTransition(),
            new CliMiscMcPhenotypeTransition(),
            new CliTopologyOptimisation(),
            new CliQ(),
            new CliSaturationAnalysis(),
            new CliModularDevelopment(),
            new CliHi(),
            new CliSquash(),
            new CliCoins(),
        };

        public CliPlotMisc(IEnumerable<ICliMiscPlotter> miscPlotters)
        {
            foreach (var miscPlotter in miscPlotters)
                Table.Add(miscPlotter.Key, miscPlotter);
        }

        private Dictionary<string, ICliMiscPlotter> Table { get; } = new Dictionary<string, ICliMiscPlotter>(StringComparer.InvariantCultureIgnoreCase);

        public string Prefix => "misc";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            var misc = clips.Get("misc");

            if (Table.TryGetValue(misc, out var plotter))
            {
                return plotter.Plot(console, clips, title);
            }

            console.WriteLine("Available misc plotters are:");
            console.WriteLine(string.Join(", ", Table.Keys));
            
            throw new Exception("Unrecognised misc type: " + misc);
        }
    }

    public static class EtaEpistatics
    {
        public static Func<double, double> IvmcProperη(CliParams clips)
        {
            double cp = clips.Get("cp", double.Parse, 1.0);
            double cn = clips.Get("cn", double.Parse, 0.7);

            return IvmcProperη(cp, cn);
        }

        public static Func<double, double> IvmcProperη(double cp, double cn)
        {
            return x =>
            {
                double moduleFitness = 0.0;

                double positiveness = 0.5 + x * 0.5;
                moduleFitness += positiveness * positiveness * cp;
                
                double negativeness = 0.5 - x * 0.5;
                moduleFitness += negativeness * negativeness * cn;

                return moduleFitness;
            };
        }
        
        public static Func<double, double> IvmcSplitη(CliParams clips)
        {
            double cp = clips.Get("cp", double.Parse, 1.0);
            double cn = clips.Get("cn", double.Parse, 0.7);

            return IvmcSplitη(cp, cn);
        }

        public static Func<double, double> IvmcSplitη(double cp, double cn)
        {
            return x =>
            {
                if (x > 0)
                    return x * cp;
                else
                    return x * -cn;
            };
        }
    }
    
    public class CliMiscEtaThings : ICliMiscPlotter
    {
        public static CliMiscEtaThings IvmcProper = new CliMiscEtaThings("IvmcThing", "Phenotype Fitness", EtaEpistatics.IvmcProperη);
        public static CliMiscEtaThings IvmcSplit = new CliMiscEtaThings("IvmcSplitThing", "Phenotype Fitness", EtaEpistatics.IvmcSplitη);

        public CliMiscEtaThings(string key, string title, Func<CliParams, Func<double, double>> ηSource)
        {
            Key = key;
            Title = title;
            this.ηSource = ηSource;
        }

        public string Key { get; }
        public string Title { get; }
        public Func<CliParams, Func<double, double>> ηSource { get; }

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            bool diff = clips.IsSet("diff");

            var model = new PlotModel { Title = title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Module Sum", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = diff ? "Fitness" : "Fitness", Key = "y" });
            
            int N = clips.Get("n", int.Parse, 4);
            
            double totalWeight=clips.Get("weight", double.Parse, N * N);
            double strokeThickness = clips.Get("strokeThickness", double.Parse, 2.0);
            
            double xmin=clips.Get("xmin", double.Parse, -1);
            double xmax=clips.Get("xmax", double.Parse, +1);
            double ymin=clips.Get("ymin", double.Parse, 0);
            double ymax=clips.Get("ymax", double.Parse, 1);
            model.Axes[1].Minimum = ymin;
            model.Axes[1].Maximum = ymax;
            
            var η = ηSource(clips);
            
            var mainCurve = new FunctionSeries(x => η(x), xmin, xmax, 1000) { RenderInLegend = false, StrokeThickness = strokeThickness };
            model.Series.Add(mainCurve);
            
            int T = clips.Get("T", int.Parse, 10);
            double tau = clips.Get("tau", double.Parse, 0.2);
            var context = new ModelExecutionContext(new MathNet.Numerics.Random.MersenneTwister(0, false));
            var drules = new DevelopmentRules(T, 1.0, tau, DevelopmentRules.TanhHalf);
            
            if (N > 0)
            {
                Linear.Matrix<double> transMat;

                string genome = clips.Get("genome", null);

                if (genome != null)
                {
                    var g = Analysis.LoadGenome(genome);
                    transMat = g.TransMat;
                }
                else
                {
                    string dtm = clips.Get("dtm");
                    Func<double, Linear.Matrix<double>> createDtm = GenomeHelpers.PrepareDtmCreater(dtm, N);
                    transMat = createDtm(totalWeight);
                }
                
                // judges the initialState given transMat
                double judge(Linear.Vector<double> initialState, out double moduleMean)
                {
                    var g = new DenseGenome(initialState, transMat);
                    moduleMean = g.Develop(context, drules).Vector.Sum() / N;
                    var fitness = η(moduleMean);
                    return fitness;
                }
                
                var pointSeries = new ScatterSeries() { RenderInLegend = false, MarkerType = MarkerType.Circle, MarkerFill = OxyColors.Transparent, MarkerStroke = OxyColors.SteelBlue };
                foreach (var initialState in GenomeHelpers.EnumerateAllBinary(N))
                {
                    var fitness = judge(initialState, out var moduleMean);
                    pointSeries.Points.Add(new ScatterPoint(moduleMean, fitness));
                }
                model.Series.Add(pointSeries);

                // epistasis table, -1 -> +1 at (-1)
                if (clips.IsSet("et"))
                {
                    // creates a vector of length N containing all -1, except for the given entries, where it is +1
                    Linear.Vector<double> m1Vector(params int[] entries)
                    {
                        var v = Linear.CreateVector.Dense(N, -1.0);
                        foreach (var e in entries)
                            v[e] = 1.0;
                        return v;
                    }
                    
                    double f(Linear.Vector<double> initialState) => judge(initialState, out var _);

                    Linear.Matrix<double> epistasisTable = Linear.CreateMatrix.Dense(N, N, (a, b) =>
                    {
                        if (a == b)
                            return 0;

                        var ab = m1Vector();
                        var Ab = m1Vector(a);
                        var aB = m1Vector(b);
                        var AB = m1Vector(a, b);
                        
                        return f(AB) - f(Ab) - f(aB) + f(ab);
                    });

                    console.WriteLine(epistasisTable.ToString());
                }
            }

            model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = 0.0, Color = OxyColors.Gray, Layer = OxyPlot.Annotations.AnnotationLayer.BelowSeries });

            if (ymin < 0)
            {
                model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Horizontal, Y = 0.0, Color = OxyColors.Gray, Layer = OxyPlot.Annotations.AnnotationLayer.BelowSeries });
            }
            
            return model;
        }
    }
    
    public class CliMiscEtaEffective : ICliMiscPlotter
    {
        public static CliMiscEtaEffective IvmcProper = new CliMiscEtaEffective("IvmcEffective", "Phenotype Fitness", EtaEpistatics.IvmcProperη);
        public static CliMiscEtaEffective IvmcSplit = new CliMiscEtaEffective("IvmcSplitEffective", "Phenotype Fitness", EtaEpistatics.IvmcSplitη);
        
        public CliMiscEtaEffective(string key, string title, Func<CliParams, Func<double, double>> ηSource)
        {
            Key = key;
            Title = title;
            this.ηSource = ηSource;
        }

        public string Key { get; }
        public string Title { get; }
        public Func<CliParams, Func<double, double>> ηSource { get; }

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            bool diff = clips.IsSet("diff");

            var model = new PlotModel { Title = title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Module Sum", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = diff ? "Fitness" : "Fitness", Key = "y" });
            
            int N = clips.Get("n", int.Parse, 4);
            
            double lambda = clips.Get("lambda", double.Parse, 0.0);

            double maxWeight = clips.Get("maxweight", double.Parse, N * N);
            double dweight = clips.Get("dweight", double.Parse, maxWeight / 100.0);
            
            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, double.NaN);
            double ymin=clips.Get("ymin", double.Parse, 0);
            double ymax=clips.Get("ymax", double.Parse, double.NaN);
            model.Axes[1].Minimum = ymin;
            model.Axes[1].Maximum = ymax;
            model.Axes[0].Minimum = xmin;
            model.Axes[0].Maximum = xmax;
            
            var η = ηSource(clips);
            
            int T = clips.Get("T", int.Parse, 10);
            double tau = clips.Get("tau", double.Parse, 0.2);
            var context = new ModelExecutionContext(new MathNet.Numerics.Random.MersenneTwister(0, false));
            var drules = new DevelopmentRules(T, 1.0, tau, DevelopmentRules.TanhHalf);
            
            if (N > 0)
            {
                string dtm = clips.Get("dtm");
                Func<double, Linear.Matrix<double>> createDtm = GenomeHelpers.PrepareDtmCreater(dtm, N);
                
                Linear.Vector<double> initialState = Linear.CreateVector.Dense(N, 1.0);

                var pointSeries = new LineSeries() { RenderInLegend = false, Color = OxyColors.Red };
                for (double weight = 0.0; weight <= maxWeight; weight += dweight)
                {
                    Linear.Matrix<double> transMat = createDtm(weight);
                
                    var g = new DenseGenome(initialState, transMat);
                    var moduleMean = g.Develop(context, drules).Vector.Sum() / N;
                    var benefit = η(moduleMean);
                    var cost = weight / (N * N);
                    var fitness = benefit - lambda * cost;
                    pointSeries.Points.Add(new DataPoint(weight, fitness));
                }
                model.Series.Add(pointSeries);
            }

            model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = 0.0, Color = OxyColors.Gray });

            return model;
        }
    }
    
    public class CliMiscPlotBeam : ICliMiscPlotter
    {
        public string Key => "Beam";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var model = new PlotModel { Title = title == "" ? "Beam" : title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Total Weight", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Fitness", Key = "y" });

            model.LegendTitle="s/(s+3r)";
            int N = clips.Get("n", int.Parse, 16);
            int sN = N / 4;
            
            double srmin=clips.Get("min", double.Parse, 0);
            double srmax=clips.Get("max", double.Parse, 1);
            double delta=clips.Get("delta", double.Parse, 0.1);

            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, 10);
            
            double lambda=clips.Get("lambda", double.Parse, 0.2);
            model.Title = model.Title + $" (lambda = {lambda})";

            double y0=1;

            // Husky (Beam)
            double j(double s, double r, double l)
            {
                double ys=y0,yr=y0;

                for (int i=0;i<10;i++)
                {
                    yr += -0.2*yr+Math.Tanh(0.5*ys*r);
                    ys += -0.2*ys+Math.Tanh(0.5*ys*s);
                }

                ys/=5;
                yr/=5;

                var b=0.5 * (1 + (ys+yr*3)*sN/N);
                var c=(s+r*3)*sN/(N*N);
                return b-l*c;
            }

            double f(double t,double x,double l)
            {
                return j(t*x, (t-t*x)/3, l);
            }

            for(double sr=srmin;sr<=srmax;sr+=delta)
                model.Series.Add(new FunctionSeries(x => f(x, sr, lambda), xmin, xmax, 1000){Title=sr.ToString("0.0")});

            return model;
        }
    }
    
    public class CliMiscPlotExpBeam : ICliMiscPlotter
    {
        public string Key => "ExpBeam";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var config = clips.Get("expfile", CliPlotHelpers.LoadDenseExperimentConfig);
            var modules = clips.Get("modules", Modular.MultiModulesStringParser.Instance.Parse);

            if (clips.IsSet("reconfigconfig"))
            {
                config = CliReconfig.Reconfig(console, clips, config);
            }
            
            var moduleSize = modules.ModuleAssignments[0].Count;
            if (modules.ModuleAssignments.Any(m => m.Count != moduleSize))
                console.WriteLine("Modules have different sizes; Beam has little value in this instance.");

            int seed = clips.Get("seed", int.Parse, 1);
            var rand = new MathNet.Numerics.Random.MersenneTwister(seed, false);
            var context = new ModelExecutionContext(rand);

            int targetIdx = clips.Get("target", int.Parse, 0);
            var target = config.Targets[targetIdx];
            
            if (clips.Get("resetTarget", bool.Parse, false))
            {
                int exposureEpoch = clips.Get("exposureepoch", int.Parse, 1);

                ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
            }

            if (clips.IsSet("proper"))
            {
                if (target is Epistatics.IIvmcProperTarget ipt)
                {
                    double[] cplus = clips.Get("cplus", s => s.Split(';').Select(double.Parse).ToArray(), null);
                    double[] cminus = clips.Get("cminus", s => s.Split(';').Select(double.Parse).ToArray(), null);
                    if (cplus != null)
                    {
                        cplus.CopyTo(ipt.Proper.Cplus, 0);
                    }
                    if (cminus != null)
                    {
                        cminus.CopyTo(ipt.Proper.Cminus, 0);
                    }
                }
                else
                {
                    console.WriteLine("Ignoring proper parameter, as target is no an IIvmcProperTarget");
                }
            }
            
            target.NextGeneration(rand, config.JudgementRules);

            var model = new PlotModel { Title = !clips.IsSet("title") && title == "" ? "Beam" : title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Total Weight", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Fitness", Key = "y" });

            model.LegendTitle="sr Ratio";
            int N = config.Size;
            
            double srmin=clips.Get("min", double.Parse, 0);
            double srmax=clips.Get("max", double.Parse, 1);
            double srdelta=clips.Get("srdelta", double.Parse, 0.1);

            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, 10);
            
            int resolution = clips.Get("resolution", int.Parse, 1000);
            int srresolution = clips.Get("srresolution", int.Parse, 1);

            bool showBest = clips.Get("showBest", bool.Parse, true);
            bool saveBest = clips.Get("saveBest", bool.Parse, true);

            bool showBenefit = clips.Get("showBenefit", bool.Parse, false);
            bool showCost = clips.Get("showCost", bool.Parse, false);

            var g0 = ExperimentComposition.BasicDensePopulationComposer.ComposeG0(clips, context, config);
            g0 = g0 ?? ((VectorTarget)target).PreparePerfectP();

            DenseIndividual create(double t, double sr)
            {
                var b0 = PrepareHierarchicalMatrix(modules, t, sr);
                var genome = new DenseGenome(g0, b0);
                var individual = DenseIndividual.Develop(genome, context, config.DevelopmentRules, false);
                return individual;
            }

            double f(double t, double sr)
            {
                var individual = create(t, sr);
                return individual.Judge(config.JudgementRules, target).CombinedFitness;
            }

            double b(double t, double sr)
            {
                var individual = create(t, sr);
                return individual.Judge(config.JudgementRules, target).Benefit;
            }

            double c(double t, double sr)
            {
                var individual = create(t, sr);
                return individual.Judge(config.JudgementRules, target).Cost;
            }

            double[] ts = Enumerable.Range(0, resolution + 1).Select(i => xmin + ((double)i / resolution) * (xmax - xmin)).ToArray();
            IEnumerable<DataPoint> toDataPoints(double[] xs, double[] ys) => xs.Zip(ys, (x, y) => new DataPoint(x, y));

            double srBest = double.NaN;
            double tBest = double.NaN;
            double fBest = double.NegativeInfinity;

            int ci = 0;
            OxyColor nextColor()
            {
                var col = model.DefaultColors[ci];
                ci = (ci + 1) % model.DefaultColors.Count;
                return col;
            }

            for (double outerSr = srmin; outerSr <= srmax; outerSr += srdelta)
            {
                double sr = outerSr;
                for (int src = 0; src < srresolution; src++, sr += srdelta / srresolution)
                {
                    var beam = ts.Select(t => f(t, sr)).ToArray();

                    int best = beam.IndexMax(x => x);

                    if (beam[best] > fBest)
                    {
                        tBest = ts[best];
                        srBest = sr;
                        fBest = beam[best];
                    }

                    if (src == 0)
                    {
                        var color = showCost || showBenefit ? nextColor() : OxyColors.Automatic;

                        var line = new LineSeries() { Title = sr.ToString("0.00"), Color = color };
                        line.Points.AddRange(toDataPoints(ts, beam));
                        model.Series.Add(line);

                        if (showBenefit)
                        {
                            var bbeam = ts.Select(t => b(t, sr)).ToArray();
                            var bline = new LineSeries() { Title = null, Color = color, LineStyle = LineStyle.Dot };
                            bline.Points.AddRange(toDataPoints(ts, bbeam));
                            model.Series.Add(bline);
                        }

                        if (showCost)
                        {
                            var cbeam = ts.Select(t => c(t, sr)).ToArray();
                            var cline = new LineSeries() { Title = null, Color = color, LineStyle = LineStyle.Dot };
                            cline.Points.AddRange(toDataPoints(ts, cbeam));
                            model.Series.Add(cline);
                        }
                    }
                }
            }

            if (showBest)
                model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = tBest, Color = OxyColors.Gray, Text = $"T = {tBest}, SR = {srBest}, f = {fBest:0.00E+0}" });

            if (saveBest)
                Analysis.SaveGenome("genome_best.dat", create(tBest, srBest).Genome);

            return model;
        }
        
        public static Linear.Matrix<double> PrepareHierarchicalMatrix(Modular.Modules modules, double t, double sr)
        {
            var mat = Linear.CreateMatrix.Dense<double>(modules.ElementCount, modules.ElementCount);
            foreach (var module in modules.ModuleAssignments)
            {
                var i0 = module.First();

                double s = t * sr;
                double r = (t - s) / (module.Count - 1);
            
                foreach (var i in module)
                    mat[i, i0] = r;
                mat[i0, i0] = s;
            }
            return mat;
        }
    }

    public class CliMiscPlotExpsBeam : ICliMiscPlotter
    {
        public string Key => "ExpsBeam";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var configs = clips.Get("expfiles", s => s.Split(',').Select(CliPlotHelpers.LoadDenseExperimentConfig)).ToArray();
            var names = clips.Get("names", s => s.Split(',')).ToArray();
            var legendTitle = clips.Get("legendTitle");
            var modules = clips.Get("modules", Modular.MultiModulesStringParser.Instance.Parse);

            var moduleSize = modules.ModuleAssignments[0].Count;
            if (modules.ModuleAssignments.Any(m => m.Count != moduleSize))
                console.WriteLine("Modules have different sizes; Beam has little value in this instance.");

            int seed = clips.Get("seed", int.Parse, 1);
            var rand = new MathNet.Numerics.Random.MersenneTwister(seed, false);
            var context = new ModelExecutionContext(rand);

            int targetIdx = clips.Get("target", int.Parse, 0);

            var model = new PlotModel { Title = !clips.IsSet("title") && title == "" ? "Beam" : title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Total Weight", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Fitness", Key = "y" });

            model.LegendTitle = legendTitle;
            int N = configs[0].Size;

            double xmin = clips.Get("xmin", double.Parse, 0);
            double xmax = clips.Get("xmax", double.Parse, 10);

            int resolution = clips.Get("resolution", int.Parse, 1000);
            double sr = clips.Get("sr", double.Parse, 1);

            for (int ci = 0; ci < configs.Length; ci++)
            {
                var config = configs[ci];
                var name = names[ci];

                var target = config.Targets[targetIdx];

                if (clips.Get("resetTarget", bool.Parse, false))
                {
                    int exposureEpoch = clips.Get("exposureepoch", int.Parse, 1);

                    ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                    target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
                }

                target.NextGeneration(rand, config.JudgementRules);

                var g0 = ((VectorTarget)target).PreparePerfectP();

                DenseIndividual create(double t)
                {
                    var b0 = CliMiscPlotExpBeam.PrepareHierarchicalMatrix(modules, t, sr);
                    var genome = new DenseGenome(g0, b0);
                    var individual = DenseIndividual.Develop(genome, context, config.DevelopmentRules, false);
                    return individual;
                }

                double f(double t)
                {
                    var individual = create(t);
                    return individual.Judge(config.JudgementRules, target).CombinedFitness;
                }

                double[] ts = Enumerable.Range(0, resolution + 1).Select(i => xmin + ((double)i / resolution) * (xmax - xmin)).ToArray();
                IEnumerable<DataPoint> toDataPoints(double[] xs, double[] ys) => xs.Zip(ys, (x, y) => new DataPoint(x, y));

                var beam = ts.Select(t => f(t)).ToArray();
                var line = new LineSeries() { Title = name };
                line.Points.AddRange(toDataPoints(ts, beam));
                model.Series.Add(line);

                if (clips.IsSet("maxes"))
                {
                    var idx = beam.IndexMax(x => x);
                    model.Annotations.Add(new LineAnnotation() { Text = name, Type = LineAnnotationType.Vertical, X = ts[idx] });
                }
            }

            return model;
        }
    }
    
    public class CliMiscPlotExpReconfigBeam : ICliMiscPlotter
    {
        public string Key => "ExpReconfigBeam";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var config = clips.Get("expfile", CliPlotHelpers.LoadDenseExperimentConfig);
            var reconfigTargets = clips.Get("reconfigtargets", s => s.Split(';')).ToArray();
            var reconfigValues = reconfigTargets.Select(rt => clips.Get("values:" + rt, s => s.Split(';')).ToArray()).ToArray();
            var legendTitle = clips.Get("legendTitle");
            var modules = clips.Get("modules", Modular.MultiModulesStringParser.Instance.Parse);
            
            var moduleSize = modules.ModuleAssignments[0].Count;
            if (modules.ModuleAssignments.Any(m => m.Count != moduleSize))
                console.WriteLine("Modules have different sizes; Beam has little value in this instance.");

            int seed = clips.Get("seed", int.Parse, 1);
            var rand = new MathNet.Numerics.Random.MersenneTwister(seed, false);
            var context = new ModelExecutionContext(rand);

            int targetIdx = clips.Get("target", int.Parse, 0);

            var model = new PlotModel { Title = !clips.IsSet("title") && title == "" ? "Beam" : title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Total Weight", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Fitness", Key = "y" });

            model.LegendTitle = legendTitle;
            int N = config.Size;

            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, 10);
            
            int resolution = clips.Get("resolution", int.Parse, 1000);
            int srresolution = clips.Get("srresolution", int.Parse, 1);

            bool showBest = clips.Get("showBest", bool.Parse, true);
            bool saveBest = clips.Get("saveBest", bool.Parse, true);

            double sr = clips.Get("sr", double.Parse, 1.0 / N); // dense by default

            for (int vi = 0; vi < reconfigValues[0].Length; vi++)
            {
                var reconfigClips = new CliParams();
                for (int ri = 0; ri < reconfigTargets.Length; ri++)
                    reconfigClips.Set(reconfigTargets[ri], reconfigValues[ri][vi]);
                var reconfigedExp = CliReconfig.Reconfig(console, clips, config);
                var target = reconfigedExp.Targets[targetIdx];
                
                if (clips.Get("resetTarget", bool.Parse, false))
                {
                    int exposureEpoch = clips.Get("exposureepoch", int.Parse, 1);

                    ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                    target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
                }
                
                target.NextGeneration(rand, config.JudgementRules);

                var g0 = ((VectorTarget)target).PreparePerfectP();

                DenseIndividual create(double t)
                {
                    var b0 = CliMiscPlotExpBeam.PrepareHierarchicalMatrix(modules, t, sr);
                    var genome = new DenseGenome(g0, b0);
                    var individual = DenseIndividual.Develop(genome, context, config.DevelopmentRules, false);
                    return individual;
                }

                double f(double t)
                {
                    var individual = create(t);
                    return individual.Judge(config.JudgementRules, target).CombinedFitness;
                }

                double[] ts = Enumerable.Range(0, resolution + 1).Select(i => xmin + ((double)i / resolution) * (xmax - xmin)).ToArray();
                IEnumerable<DataPoint> toDataPoints(double[] xs, double[] ys) => xs.Zip(ys, (x, y) => new DataPoint(x, y));

                var beam = ts.Select(t => f(t)).ToArray();
                var line = new LineSeries() { Title = "" + vi };
                line.Points.AddRange(toDataPoints(ts, beam));
                model.Series.Add(line);
            }

            return model;
        }
    }

    public class BenefitPlotting
    {
        public static PlotModel Plot(TextWriter console, CliParams clips, string title, Func<double, double> eta, double[] steps, OxyEx.CustomAxisLabel[] xlabels, OxyEx.CustomAxisLabel[] ylabels)
        {
            steps = steps ?? new double[0];

            var model = new PlotModel { Title = title };
            var xaxis = new OxyEx.CustomLabelLinearAxis { Position = AxisPosition.Bottom, Title = "Sum over Module", Key = "x" };
            var yaxis = new OxyEx.CustomLabelLinearAxis { Position = AxisPosition.Left, Title = "Module Benefit", Key = "y", IncludeTick = true };
            model.Axes.Add(xaxis);
            model.Axes.Add(yaxis);

            if (xlabels != null)
                xaxis.CustomAxisLabels.AddRange(xlabels);
            if (ylabels != null)
                yaxis.CustomAxisLabels.AddRange(ylabels);

            model.LegendTitle = "";
            
            double xmin = clips.Get("xmin", double.Parse, -1);
            double xmax = clips.Get("xmax", double.Parse, 1);
            double delta = clips.Get("delta", double.Parse, 0.001);

            double bonus = clips.Get("bonus", double.Parse, 1.5);
            
            console.WriteLine(steps.Length);
            steps = (double[])steps.Clone();

            List<OxyPlot.DataPoint> points = new List<DataPoint>();

            int si = 0;

            // clear step before xmin
            while (si < steps.Length && steps[si] <= xmin)
                si++;

            double sd = delta / 10.0;

            for (double x = xmin; ; x += delta)
            {
                if (x >= xmax)
                    x = xmax;

                while (x <= xmax && si < steps.Length && steps[si] < x + sd)
                {
                    points.Add(new DataPoint(steps[si], eta(steps[si]-sd)));
                    points.Add(new DataPoint(double.NaN, double.NaN));
                    points.Add(new DataPoint(steps[si], eta(steps[si]+sd)));
                    
                    if (steps[si] > x - sd)
                        x += delta;

                    si++;
                }

                if (x >= xmax)
                    break;

                points.Add(new DataPoint(x, eta(x)));

                if (x >= xmax)
                    break;
            }

            var ls = new OxyPlot.Series.LineSeries();
            ls.Points.AddRange(points);
            ls.BrokenLineStyle = LineStyle.Dash;
            ls.BrokenLineColor = ls.Color;
            ls.BrokenLineThickness = ls.StrokeThickness / 2.0;
            model.Series.Add(ls);

            return model;
        }
    }

    public class CliMiscPlotCbbnkStepModel : ICliMiscPlotter
    {
        public string Key => "CbbnkStepModel";
        
        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            double omega = clips.Get("omega", double.Parse);
            double phi = clips.Get("phi", double.Parse);
            double xi = clips.Get("xi", double.Parse);

            double eta(double x) => x > omega
                ? Math.Abs(x) * phi + xi
                : Math.Abs(x) * phi;

            var plot = BenefitPlotting.Plot(console, clips, title ?? "CbbnkStep", eta, new[] { omega }, new OxyEx.CustomAxisLabel[] { new OxyEx.CustomAxisLabel(omega, "ω = $$$") }, null);
            return plot;
        }
    }

    public class CliMiscPlotIvmcModel : ICliMiscPlotter
    {
        public string Key => "IvmcModel";
        
        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var cplus = clips.Get("bonus", double.Parse, 1.5);
            var cminus = 1.0;

            double sqr(double x) => x * x;
            double eta(double x) => cplus * sqr((x + 1) / 2.0) + cminus * sqr((1 - x) / 2.0);

            var plot = BenefitPlotting.Plot(console, clips, title ?? "Ivmc", eta, null, null, new OxyEx.CustomAxisLabel[] { new OxyEx.CustomAxisLabel(cplus, "c^{+} = $$$"), new OxyEx.CustomAxisLabel(cminus, "c^{-} = $$$") });
            return plot;
        }
    }

    public class CliMiscPlotIvmcSwitchness : ICliMiscPlotter
    {
        public string Key => "IvmcSwitchness";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            bool diff = clips.IsSet("diff");

            var model = new PlotModel { Title = title == "" ? "Ivmc Switchness" : title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "s/(s+3r)", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = diff ? "Change in Fitness" : "Fitness", Key = "y" });

            model.LegendTitle="Total Weight";
            int N = clips.Get("n", int.Parse, 16);
            int sN = N / 4;
            
            double wmin=clips.Get("min", double.Parse, 0);
            double wmax=clips.Get("max", double.Parse, 5);
            double delta=clips.Get("delta", double.Parse, 1.0);

            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, 1);
            
            double lambda = clips.Get("lambda", double.Parse, 0.0);
            double bonus = clips.Get("bonus", double.Parse, 1.5);
            model.Title = model.Title + $" (lambda = {lambda})";
            
            // IvmcSwitchness, ys=1 is high
            double j(double ys0, double yr0, double s, double r, double l)
            {
                double ys=ys0,yr=yr0;

                for (int i=0;i<10;i++)
                {
                    yr += -0.2*yr+Math.Tanh(0.5*ys*s + 3*0.5*yr*r); // 'columns' topology
                    ys += -0.2*ys+Math.Tanh(0.5*ys*s + 3*0.5*yr*r);
                }
                
                ys/=5;
                yr/=5;

                double acc = (ys+yr*3) / 4; // average (signed) weight

                double hi = 0.5 + acc * 0.5;
                double lo = 0.5 - acc * 0.5;
                
                var b=0.5 * (1 + (hi*hi*bonus + lo*lo)*sN/N);
                var c=(Math.Abs(s)+Math.Abs(r)*3)*sN/(N*N);
                return b-l*c;
            }

            double f(double ys0, double yr0, double t,double x,double l)
            {
                return j(ys0, yr0, t*x, (t-t*x)/3, l);
            }

            if (diff)
            {
                for (double w = wmin; w <= wmax; w += delta)
                {
                    var col = OxyColor.Interpolate(OxyColors.Red, OxyColors.Blue, (w - wmin) / (wmax - wmin));
                    var d = new FunctionSeries(x => f(1.0, -1.0, w, x, lambda) - f(-1.0, -1.0, w, x, lambda), xmin, xmax, 1000) { Title = w.ToString("0.0"), Color = col };
                    model.Series.Add(d);
                }
            }
            else
            {
                for (double w = wmin; w <= wmax; w += delta)
                {
                    var col = OxyColor.Interpolate(OxyColors.Red, OxyColors.Blue, (w - wmin) / (wmax - wmin));
                    var ll = new FunctionSeries(x => f(-1.0, -1.0, w, x, lambda), xmin, xmax, 1000) { Title = "Negtv " + w.ToString("0.0"), Color = col };
                    var hl = new FunctionSeries(x => f(1.0, -1.0, w, x, lambda), xmin, xmax, 1000) { Title = "Swtch " + w.ToString("0.0"), Color = col, LineStyle = LineStyle.Dash };
                    model.Series.Add(ll);
                    model.Series.Add(hl);
                }
            }

            model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = 0.25, Color = OxyColors.Gray });

            return model;
        }
    }
    
    public class CliMiscPlotIvmcProperSwitchness : ICliMiscPlotter
    {
        public string Key => "IvmcProperSwitchness";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            bool diff = clips.IsSet("diff");

            int N = clips.Get("n", int.Parse, 16);
            int sN = N / 4;
            
            double wmin=clips.Get("min", double.Parse, 0);
            double wmax=clips.Get("max", double.Parse, 5);
            double delta=clips.Get("delta", double.Parse, 1.0);

            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, 1);

            double ymin=clips.Get("ymin", double.Parse, double.NaN);
            double ymax=clips.Get("ymax", double.Parse, double.NaN);
            
            double lambda = clips.Get("lambda", double.Parse, 0.0);
            double ch = clips.Get("CH", double.Parse, 1.0);
            double cl = clips.Get("CL", double.Parse, 0.7);
            
            var model = new PlotModel { Title = title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "s/(s+3r)", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = diff ? "Change in Fitness" : "Fitness", Key = "y", Maximum = ymax, Minimum = ymin });

            model.LegendTitle="Total Weight";
            
            // IvmcSwitchness, ys=1 is high
            double j(double ys0, double yr0, double s, double r, double l)
            {
                double ys=ys0,yr=yr0;

                for (int i=0;i<10;i++)
                {
                    yr += -0.2*yr+Math.Tanh(0.5*ys*s + 3*0.5*yr*r); // 'columns' topology
                    ys += -0.2*ys+Math.Tanh(0.5*ys*s + 3*0.5*yr*r);
                }
                
                ys/=5;
                yr/=5;

                double acc = (ys+yr*3) / 4; // average (signed) weight

                double hi = 0.5 + acc * 0.5;
                double lo = 0.5 - acc * 0.5;

                var b=0.5 * (1 + (hi*hi*ch + lo*lo*cl)); // module cost is already mean
                var c=(Math.Abs(s)+Math.Abs(r)*3)*sN/(N*N);
                return b-l*c;
            }

            double f(double ys0, double yr0, double t,double x,double l)
            {
                return j(ys0, yr0, t*x, (t-t*x)/3, l);
            }

            if (diff)
            {
                for (double w = wmin; w <= wmax; w += delta)
                {
                    var col = OxyColor.Interpolate(OxyColors.Red, OxyColors.Blue, (w - wmin) / (wmax - wmin));
                    var d = new FunctionSeries(x => f(1.0, -1.0, w, x, lambda) - f(-1.0, -1.0, w, x, lambda), xmin, xmax, 1000) { Title = w.ToString("0.0"), Color = col };
                    model.Series.Add(d);
                }
            }
            else
            {
                for (double w = wmin; w <= wmax; w += delta)
                {
                    var col = OxyColor.Interpolate(OxyColors.Red, OxyColors.Blue, (w - wmin) / (wmax - wmin));
                    var ll = new FunctionSeries(x => f(-1.0, -1.0, w, x, lambda), xmin, xmax, 1000) { Title = "Negtv " + w.ToString("0.0"), Color = col };
                    var hl = new FunctionSeries(x => f(1.0, -1.0, w, x, lambda), xmin, xmax, 1000) { Title = "Swtch " + w.ToString("0.0"), Color = col, LineStyle = LineStyle.Dash };
                    model.Series.Add(ll);
                    model.Series.Add(hl);
                }
            }

            model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = 0.25, Color = OxyColors.Gray });
            if (diff)
            {
                model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Horizontal, Y = 0, Color = OxyColors.Gray });
            }

            return model;
        }
    }

    public class CliMiscPlotPitOfDespair : ICliMiscPlotter
    {
        public string Key => "PitOfDespair";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var model = new PlotModel { Title = title == "" ? "Pit of Despair" : title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Total Weight", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Fitness", Key = "y" });

            model.LegendTitle="Lambda";
            int N = clips.Get("n", int.Parse, 16);
            int sN = N / 4;
            
            double lmin=clips.Get("min", double.Parse, 0);
            double lmax=clips.Get("max", double.Parse, 2);
            double delta=clips.Get("delta", double.Parse, 0.2);

            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, 0.1);

            double y0=1;

            // Husky (Beam)
            double j(double s, double r, double l)
            {
                double ys=y0,yr=y0;

                for (int i=0;i<10;i++)
                {
                    yr += -0.2*yr+Math.Tanh(0.5*ys*r);
                    ys += -0.2*ys+Math.Tanh(0.5*ys*s);
                }

                ys/=5;
                yr/=5;

                var b=0.5 * (1 + (ys+yr*3)*sN/N);
                var c=(s+r*3)*sN/(N*N);
                return b-l*c;
            }

            double f(double t,double x,double l)
            {
                return j(t*x, (t-t*x)/3, l);
            }

            for(double t=lmin;t<=lmax;t+=delta)
                model.Series.Add(new FunctionSeries(x => f(x, 1, t), xmin, xmax, 1000){Title=t.ToString("0.0")});

            return model;
        }
    }

    public class CliMiscPlotDevTraj : ICliMiscPlotter
    {
        public string Key => "DevTraj";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var model = new PlotModel { Title = title == "" ? "Development Trajectories" : title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Developmental Steps", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Trait Expression", Key = "y" });

            model.LegendTitle="Train";
            int N = clips.Get("n", int.Parse, 4);
            
            double lmin=clips.Get("min", double.Parse, 0);
            double lmax=clips.Get("max", double.Parse, 2);
            double delta=clips.Get("delta", double.Parse, 0.2);

            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, 0.1);
            
            int T = clips.Get("T", int.Parse, 10);
            double sqw(double x) => Math.Tanh(x / 2);

            double[][] d(double g, double r, double s) // b twiddle
            {
                double[][] res = new double[T + 1][];
                double ys = 1, yr = 1;
                res[0] = new[] { ys, yr };
                for (int i = 0; i < T; i++)
                {
                    double dys = -0.2 * ys + sqw(ys * s + 3 * yr * g);
                    double dyr = -0.2 * yr + sqw(ys * r + 3 * yr * g);
                    ys += dys;
                    yr += dyr;
                    res[i + 1] = new[] { ys, yr };
                }
                return res;
            }
            double L = 1.18, ror = 0.3;//~optimal

            double[][] k(double x, double t, double l)
            {
                x = 1 - x;
                double g0 = t / 16, s0 = g0, r0 = g0;
                double g1 = 0, s1 = ror * t, r1 = (t - s1) / 3;
                return d(x * g0 + (1 - x) * g1, x * r0 + (1 - x) * r1, x * s0 + (1 - x) * s1);
            }

            for (int z = 0; z < 2; z++)
            {
                var t = 5;
                double[][] fox = k(0, t, L);
                double[][] husky = k(1, t, L);
                model.Series.Add(new FunctionSeries(x => fox[(int)x][z], 0, T, T + 1) { Title = $"Fox{z}@{t}" });
                model.Series.Add(new FunctionSeries(x => husky[(int)x][z], 0, T, T + 1) { Title = $"Husky{z}@{t}" });
            }

            return model;
        }
    }
    
    public class CliMiscTransition : ICliMiscPlotter
    {
        public string Key => "Transition";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            int seed = clips.Get("seed", int.Parse, 1);

            var config = CliPlotHelpers.LoadExperimentConfig(clips.GetOrCreate("expfile", s=>s, () => clips.Get("config")));
            var rand = new MathNet.Numerics.Random.MersenneTwister(seed, false);
            var context = new ModelExecutionContext(rand);
            //var rand = context.DisableRandom();

            var model = new PlotModel { Title = title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Positive Initial Proportion", Key = "x" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Fitness", Key = "y" });

            int na = clips.Get("na", int.Parse, 1);
            int nb = clips.Get("nb", int.Parse, 1);

            int n = na + nb;

            if (n != config.Size)
                throw new Exception("Config size " + config.Size + " does not match na & nb");

            double weight;
            if (clips.IsSet("weight"))
            {
                weight = clips.Get("weight", double.Parse);
            }
            else if (clips.IsSet("expfile"))
            {
                var exp = PopulationExperiment<DenseIndividual>.Load(clips.Get("expfile"));
                weight = exp.Population.ExtractAll()[0].Genome.TransMat.Enumerate().Sum(d => Math.Abs(d));
                console.WriteLine("weight: " + weight);
            }
            else
            {
                throw new Exception("Total weight not specified; qualify expfile or weight");
            }

            DenseIndividual create(double a, double dba, double dbb)
            {
                var ba = (dba + a * weight) / (na * n);
                var bb = (dbb + (1 - a) * weight) / (nb * n);
                var g = Linear.CreateVector.Dense<double>(n, i => i < na ? +1.0 : -1.0);
                var b = Linear.CreateMatrix.Dense<double>(n, n, (i, j) => j < na ? ba : bb);
                
                var genome = new DenseGenome(g, b);
                var individual = DenseIndividual.Develop(genome, context, config.DevelopmentRules, false);

                return individual;
            }
            
            int targetIdx = clips.Get("target", int.Parse, 0);
            var target = config.Targets[targetIdx];
            
            if (clips.Get("resetTarget", bool.Parse, false))
            {
                int exposureEpoch = clips.Get("exposureepoch", int.Parse, 1);

                ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
            }
            
            target.NextGeneration(rand, config.JudgementRules);
            
            bool benefit = clips.IsSet("benefit");

            int resolution = clips.Get("resolution", int.Parse, 1000);
            
            double xmin=clips.Get("xmin", double.Parse, 0);
            double xmax=clips.Get("xmax", double.Parse, 1);
            double ymin=clips.Get("ymin", double.Parse, double.NaN);
            double ymax=clips.Get("ymax", double.Parse, double.NaN);
            model.Axes[1].Minimum = ymin;
            model.Axes[1].Maximum = ymax;
            //model.Axes[0].Minimum = xmin;
            //model.Axes[0].Maximum = xmax;

            double deltaProp = clips.Get("da", double.Parse, 0.0000001);
            double deltaAbs = clips.Get("dba", double.Parse, 0.0000001);
            

            int[] indicies = Enumerable.Range(0, resolution + 1).ToArray();
            double[] aValues = indicies.Select(i => xmin + ((double)i / resolution) * (xmax - xmin)).ToArray();
            
            double[] compute(Func<double, double> f) => aValues.Select(f).ToArray();

            double[] fValues = compute(a =>
            {
                var individual = create(a, 0.0, 0.0);
                var ij = individual.Judge(config.JudgementRules, target);
                return ij.CombinedFitness;
            });

            double[] dfdaValues = compute(a =>
            { // note: da is not linear in ba or bb
                var da = deltaProp;
                var i0 = create(a, 0.0, 0.0);
                var ij0 = i0.Judge(config.JudgementRules, target);
                var i1 = create(a + da, 0.0, 0.0);
                var ij1 = i1.Judge(config.JudgementRules, target);
                return benefit
                    ? (ij1.Benefit - ij0.Benefit) / da
                    : (ij1.CombinedFitness - ij0.CombinedFitness) / da;
            });
            double[] dfdaMirrorValues = dfdaValues.Reverse().ToArray();

            double[] dfdbaValues = compute(a =>
            {
                var dba = deltaAbs;
                var i0 = create(a, 0.0, 0.0);
                var ij0 = i0.Judge(config.JudgementRules, target);
                var i1 = create(a, dba, 0.0);
                var ij1 = i1.Judge(config.JudgementRules, target);
                return benefit
                    ? (ij1.Benefit - ij0.Benefit) / dba
                    : (ij1.CombinedFitness - ij0.CombinedFitness) / dba;
            });
            double[] dfdbaMirrorValues = dfdbaValues.Reverse().ToArray();

            double[] dfdbbValues = compute(a =>
            {
                var dbb = deltaAbs;
                var i0 = create(a, 0.0, 0.0);
                var ij0 = i0.Judge(config.JudgementRules, target);
                var i1 = create(a, 0.0, dbb);
                var ij1 = i1.Judge(config.JudgementRules, target);
                return benefit
                    ? (ij1.Benefit - ij0.Benefit) / dbb
                    : (ij1.CombinedFitness - ij0.CombinedFitness) / dbb;
            });
            double[] dfdbbMirrorValues = dfdbbValues.Reverse().ToArray();

            double[] ddiff = dfdbaValues.Zip(dfdbbValues, (a, b) => a - b).ToArray();
            double[] ddiffMirror = ddiff.Reverse().ToArray();
            
            void plotLine(double[] data, string lineTitle, OxyColor color, LineStyle style)
            {
                var line = new LineSeries() { Title = lineTitle, RenderInLegend = true, Color = color, LineStyle = style };
                for (int i = 0; i < aValues.Length; i++)
                {
                    line.Points.Add(new DataPoint(aValues[i], data[i]));
                }
                model.Series.Add(line);
            }

            plotLine(fValues, "fitness", OxyColors.Red, LineStyle.Solid);
            //plotLine(dfdbaValues, benefit ? "db/dba" : "df/dba", benefit ? OxyColors.Orange : OxyColors.Red, LineStyle.Solid);
            //plotLine(dfdbbValues, benefit ? "db/dba" : "df/dbb");
            //plotLine(ddiff, benefit ? "db/dba - db/dbb" : "df/dba - df/dbb");
            //plotLine(ddiffMirror, benefit ? "db/dba - db/dbb (mirror)" : "df/dba - df/dbb (mirror)");
            if (clips.IsSet("dfdba"))
            {
                plotLine(dfdbaValues, benefit ? "df/dba" : "df/dba", OxyColors.SlateBlue, LineStyle.Solid);
                plotLine(dfdbaMirrorValues, benefit ? "df/dba (mirror)" : "df/dba (mirror)", OxyColors.SlateBlue, LineStyle.Dash);
            }
            if (clips.IsSet("dfdbb"))
            {
                plotLine(dfdbbValues, benefit ? "df/dbb" : "df/dbb", OxyColors.SlateBlue, LineStyle.Solid);
                plotLine(dfdbbMirrorValues, benefit ? "df/dbb (mirror)" : "df/dbb (mirror)", OxyColors.SlateBlue, LineStyle.Dash);
            }
            if (clips.IsSet("dfda"))
            {
                plotLine(dfdaValues, benefit ? "df/da" : "df/da", OxyColors.SlateBlue, LineStyle.Solid);
                plotLine(dfdaMirrorValues, benefit ? "df/da (mirror)" : "df/da (mirror)", OxyColors.SlateBlue, LineStyle.Dash);
            }
            
            model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = 0.5, Color = OxyColors.Gray });
            model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Horizontal, Y = 0, Color = OxyColors.Gray });
            model.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Horizontal, Y = -config.JudgementRules.RegularisationFactor / (n * n), Color = OxyColors.Maroon });
            
            return model;
        }
    }

    public class CliModularDevelopment : ICliMiscPlotter
    {
        public string Key => "modd";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            int seed = clips.Get("seed", int.Parse, 1);

            var config = CliPlotHelpers.LoadExperimentConfig(clips.GetOrCreate("expfile", s => s, () => clips.Get("config")));
            var rand = new MathNet.Numerics.Random.MersenneTwister(seed, false);
            var context = new ModelExecutionContext(rand);

            var model = new PlotModel { Title = title };
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Positive Initial Proportion", Key = "B" });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Fitness", Key = "y" });

            var modules = clips.Get("modulesstring", Modular.MultiModulesStringParser.Instance.Parse);

            double delta = clips.Get("delta", double.Parse, 1e-7);

            int targetIdx = clips.Get("target", int.Parse, 0);
            var target = config.Targets[targetIdx] as VectorTarget;

            if (clips.Get("resetTarget", bool.Parse, false))
            {
                int exposureEpoch = clips.Get("exposureepoch", int.Parse, 1);

                ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
            }

            var g = target.PreparePerfectP();
            var sTraitCount = modules.ModuleAssignments.Sum(m => m.Count * m.Count);
            var rTraitCount = modules.ElementCount * modules.ElementCount - sTraitCount;
            DenseIndividual create(double s, double r)
            {
                var b = GenomeHelpers.CreateDense(g, modules, s / sTraitCount, r / rTraitCount);

                var genome = new DenseGenome(g, b);
                var individual = DenseIndividual.Develop(genome, context, config.DevelopmentRules, false);

                return individual;
            }

            target.NextGeneration(rand, config.JudgementRules);

            double judge2(double s, double r, bool benefit2)
            {
                var individual = create(s, r);
                var judgement = MultiMeasureJudgement.Judge(individual.Genome, individual.Phenotype, config.JudgementRules, target);

                if (benefit2)
                {
                    return judgement.Benefit;
                }
                else
                {
                    return judgement.CombinedFitness;
                }
            }

            var deltas = clips.IsSet("deltas");
            var deltar = clips.IsSet("deltar");

            if (deltas && deltar)
            {
                throw new Exception("Cannot plot deltas and deltar at the same time");
            }

            bool benefit = clips.IsSet("benefit");
            double judge3(double s, double r) => judge2(s, r, benefit);

            double judge(double s, double r)
            {
                if (deltas)
                {
                    return (judge3(s + delta, r) - judge3(s, r)) / delta;
                }
                else if (deltar)
                {
                    return (judge3(s, r + delta) - judge3(s, r)) / delta;
                }
                else
                {
                    return judge3(s, r);
                }
            }

            var mode = clips.Get("mode", "hms");
            switch (mode)
            {
                case "hms":
                    {
                        var maxmag = clips.Get("maxmag", double.Parse, 10);
                        var maxs = clips.Get("maxs", double.Parse, maxmag);
                        var mins = clips.Get("mins", double.Parse, 0.0);
                        var maxr = clips.Get("maxr", double.Parse, maxmag);
                        var minr = clips.Get("minr", double.Parse, 0.0);
                        var sampleCount = clips.Get("sampleCount", int.Parse, 50);

                        var samples = new double[sampleCount + 1, sampleCount + 1];
                        var rows = new double[sampleCount + 1];
                        var cols = new double[sampleCount + 1];

                        for (int i = 0; i <= sampleCount; i++)
                        {
                            var s = cols[i] = mins + (maxs - mins) * (i / (double)sampleCount);
                            for (int j = 0; j <= sampleCount; j++)
                            {
                                var r = rows[j] = minr + (maxr - minr) * (j / (double)sampleCount);
                                samples[i, j] = judge(s, -r);
                            }
                        }

                        var plot = new PlotModel() { Title = "HMS" };

                        var saxis = new LinearAxis() { Title = "s", Position = AxisPosition.Bottom, Key = "s" };
                        var raxis = new LinearAxis() { Title = "r", Position = AxisPosition.Left, Key = "r" };
                        plot.Axes.Add(saxis);
                        plot.Axes.Add(raxis);

                        var colorAxis = new LinearColorAxis() { Title = benefit ? "Benefit" : "Fitness", Position = AxisPosition.Right, Key = "c" };
                        colorAxis.Palette = OxyPalettes.Gray(100);
                        plot.Axes.Add(colorAxis);

                        var hms = new HeatMapSeries();
                        hms.X0 = 0;
                        hms.X1 = maxs;
                        hms.Y0 = 0;
                        hms.Y1 = maxr;
                        hms.RenderMethod = HeatMapRenderMethod.Rectangles;
                        hms.CoordinateDefinition = HeatMapCoordinateDefinition.Center;
                        hms.Data = samples;
                        plot.Series.Add(hms);

                        var cs = new ContourSeries();
                        cs.Data = samples;
                        cs.RowCoordinates = rows;
                        cs.ColumnCoordinates = cols;
                        plot.Series.Add(cs);

                        return plot;
                    }

                case "varysr":
                    {
                        // first, find optimum weight (doesn't need to be perfect)
                        var weight = clips.GetOrCreate("weight", double.Parse, () => Misc.BinaryArgMax(w => judge2(w, 0, false), new Misc.Range(delta, 1.0 / delta), delta)); // may be incorrect if there is a pit of despair
                        var sampleCount = clips.Get("sampleCount", int.Parse, 500);
                        var deltasr = clips.IsSet("deltasr");

                        var plot = new PlotModel() { Title = "VaryB", Subtitle = $"Max Weight = {weight}" };

                        var sraxis = new LinearAxis() { Title = "(s / (s + r))", Position = AxisPosition.Bottom, Key = "srratio" };
                        var faxis = new LinearAxis() { Title = benefit ? "Benefit" : "Fitness", Position = AxisPosition.Left, Key = "f" };
                        plot.Axes.Add(sraxis);
                        plot.Axes.Add(faxis);

                        var cls = new LineSeries() { Title = "Off-block correct", RenderInLegend = true };
                        var ils = new LineSeries() { Title = "Off-block incorrect", RenderInLegend = true };
                        var zls = new LineSeries() { Title = "Off-block zeroed", RenderInLegend = true };
                        var ols = new LineSeries() { Title = "Off-block only", RenderInLegend = true };

                        for (int i = 0; i <= sampleCount; i++)
                        {
                            var ratio = (double)i / sampleCount;

                            if (deltasr)
                            {
                                var deltaRatio = ratio + delta;
                                cls.Points.Add(new DataPoint(ratio, (judge(weight * deltaRatio, weight * (1 - deltaRatio)) - judge(weight * ratio, weight * (1 - ratio))) / delta));
                                ils.Points.Add(new DataPoint(ratio, (judge(weight * deltaRatio, -weight * (1 - deltaRatio)) - judge(weight * ratio, -weight * (1 - ratio))) / delta));
                                zls.Points.Add(new DataPoint(ratio, (judge(weight * deltaRatio, 0.0) - judge(weight * ratio, 0.0)) / delta));
                                ols.Points.Add(new DataPoint(ratio, (judge(0.0, weight * (1 - deltaRatio)) - judge(0.0, weight * (1 - ratio))) / delta));
                            }
                            else
                            {
                                cls.Points.Add(new DataPoint(ratio, judge(weight * ratio, weight * (1 - ratio))));
                                ils.Points.Add(new DataPoint(ratio, judge(weight * ratio, -weight * (1 - ratio))));
                                zls.Points.Add(new DataPoint(ratio, judge(weight * ratio, 0.0)));
                                ols.Points.Add(new DataPoint(ratio, judge(0.0, weight * (1 - ratio))));
                            }
                        }

                        plot.Series.Add(cls);
                        plot.Series.Add(ils);
                        plot.Series.Add(zls);
                        plot.Series.Add(ols);

                        return plot;
                    }

                default:
                    throw new Exception($"Unrecognised mode {mode}; try one of hms, varysr");
            }
        }
    }

    public class CliSaturationAnalysis : ICliMiscPlotter
    {
        public string Key => "satan";
         
        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var outfile = clips.Get("out", "report.txt");

            var config = clips.Get("config", CliPlotHelpers.LoadExperimentConfig);
            var moduleSize = clips.Get("moduleSize", int.Parse, config.Size);

            var cmin = clips.Get("cmin", double.Parse);
            var cmax = clips.Get("cmax", double.Parse);
            var cresolution = clips.Get("cresolution", int.Parse);

            var lambdamin = clips.Get("lambdamin", double.Parse, config.JudgementRules.RegularisationFactor);
            var lambdamax = clips.Get("lambdamax", double.Parse, config.JudgementRules.RegularisationFactor);
            var lambdaresolution = clips.Get("lambdaresolution", int.Parse, 0);

            var cmajor = clips.Get("cmajor", bool.Parse, false);

            var rand = new CustomMersenneTwister(1);
            var context = new ModelExecutionContext(rand);

            var plot = new PlotModel() { Title = title };

            var cs = CliPlotHelpers.LinearSamples(cmin, cmax, cresolution).ToArray();
            var lambdas = CliPlotHelpers.LinearSamples(lambdamin, lambdamax, lambdaresolution).ToArray();
            
            plot.Axes.Add(new LinearAxis() { Title = "Saturation Weight", Position = AxisPosition.Left, Key = "w" });

            if (cmajor)
            {
                plot.Axes.Add(new LinearAxis() { Title = "Regularisation Factor (lambda)", Position = AxisPosition.Bottom, Key = "lambda" });

                foreach (var c in cs)
                {
                    var ss = lambdas.Select(lambda => SaturationInfo.Measure(context, config.DevelopmentRules, c, lambda, moduleSize).SaturationPoint);

                    var ls = new LineSeries() { Title = $"c = {c}" };
                    ls.Points.AddRange(lambdas.Zip(ss, (x, y) => new DataPoint(x, y)));
                    plot.Series.Add(ls);
                }
            }
            else
            {
                plot.Axes.Add(new LinearAxis() { Title = "Benefit Curve (c)", Position = AxisPosition.Bottom, Key = "c" });

                foreach (var lambda in lambdas)
                {
                    var ss = cs.Select(c => SaturationInfo.Measure(context, config.DevelopmentRules, c, lambda, moduleSize).SaturationPoint);

                    var ls = new LineSeries() { Title = $"lambda = {lambda}" };
                    ls.Points.AddRange(cs.Zip(ss, (x, y) => new DataPoint(x, y)));
                    plot.Series.Add(ls); 
                }
            }

            return plot;
        }
    }

    public class CliMiscMcPhenotypeTransition : ICliMiscPlotter
    {
        public string Key => "McPhenotypeTransition";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("McPhenotypeTransition Options:\n" +
                    " - n (number of traits)\n" +
                    " - omega (ω, slow cut-off)\n" +
                    " - psi (ψ, slow gradient)\n" +
                    " - xi (ξ, step)\n" +
                    " - q (linear share)\n" +
                    " - a (MMSO shape)\n" +
                    " - bprob (default 1.0)\n" +
                    " - bex (default false)\n" +
                    " - MG (M_G, default 0.6)\n" +
                    " - MB (M_G, default 1/15N^2)\n" +
                    " - K (default 1000)\n" +
                    " - epochs (default 100000)\n" +
                    " - expfile\n" +
                    " - devtemplate");
            }

            int n, k, a, b;
            string terpMode;

            // target
            ITarget target;
            ExperimentConfiguration config = null;
            if (clips.IsSet("expfile"))
            {
                config = CliPlotHelpers.LoadDenseExperimentConfig(clips.Get("expfile"));
                target = config.Targets[0];

                k = clips.Get("k", int.Parse, 1);
                n = clips.Get("n", int.Parse, config.Size / k);

                a = clips.Get("a", int.Parse, 0); // number of modules with which we disagree at start
                b = clips.Get("b", int.Parse, n * k);
                terpMode = clips.Get("terpMode", "One");

                title = clips.Get("title", $"PhenotypeTransition{terpMode}N{n}K{k}A{a}");
            }
            else
            {
                k = clips.Get("k", int.Parse);
                n = clips.Get("n", int.Parse);

                a = clips.Get("a", int.Parse, 0); // number of modules with which we disagree at start
                b = clips.Get("b", int.Parse, n * k);
                terpMode = clips.Get("terpMode", "All");

                var p = clips.Get("p", double.Parse);

                target = new Epistatics.MCTargetPlainOnPlain(n, k, p);

                title = clips.Get("title", $"McPhenotypeTransition{terpMode}N{n}K{k}P{p}A{a}");
            }

            // development template
            DenseGenome devTemplate = null;
            if (clips.IsSet("devtemplate"))
            {
                if (config == null)
                    throw new InvalidOperationException("Must specify an expfile (may be a config) to use development templates");

                devTemplate = CliPlotHelpers.LoadDenseGenome(clips.Get("devtemplate"));
            }

            var perfectp = clips.GetOrCreate(
                "perfectp",
                s => Misc.ParseExtremesVector(s, -1.0, 1.0, 0.0),
                () =>
                {
                    Console.WriteLine(target.GetType().FullName);
                    if (target is IPerfectPhenotypeTarget ppt)
                        return ppt.PreparePerfectP();
                    else
                        return CreateVector.Dense(target.Size, 1.0);
                });

            int seed = clips.Get("seed", int.Parse, 1);
            var rand = new CustomMersenneTwister(seed);
            var ctx = new ModelExecutionContext(rand);

            ExposureInformation exposureInformation = new ExposureInformation(1); // ignored
            target.NextExposure(rand, new JudgementRules(0.0, null, 0.0), 0, ref exposureInformation);
            target.NextGeneration(rand, new JudgementRules(0.0, null, 0.0));

            Phenotype make(double x)
            {
                Linear.Vector<double> vec;
                if (terpMode.SoftEquals("all"))
                {
                    vec = Linear.CreateVector.Dense<double>(
                            n * k,
                            i => i < (a * k) ? 1
                                : i < (b * k) ? x / (b - a) * 2 - 1
                                : -1
                        );
                }
                else if (terpMode.SoftEquals("one"))
                {
                    vec = Linear.CreateVector.Dense<double>(
                            n * k,
                            i => i < (a * k) ? 1
                                : i < (b * k) ? Misc.Clamp(((x * k - (i - a * k)) * 2 - 1), -1, +1)
                                : -1
                        );
                }
                else if (terpMode.SoftEquals("module"))
                {
                    vec = Linear.CreateVector.Dense<double>(
                            n * k,
                            i => i < (a * k) ? 1
                                : i < (b * k) ? Misc.Clamp(((x - ((i / k) - a)) * 2 - 1), -1, +1)
                                : -1
                        );
                }
                else
                {
                    throw new Exception("Unrecognised Interpolation Mode: " + terpMode);
                }

                vec.PointwiseMultiply(perfectp, vec);

                if (devTemplate != null)
                {
                    var g = devTemplate.Clone(ctx, vec, null);
                    return g.Develop(ctx, config.DevelopmentRules);
                }
                return new Phenotype(vec);
            }

            if (clips.IsSet("target"))
            {
                return CliExpPlotter.PlotCorrelationMatrix(clips, ((Epistatics.CorrelationMatrixTarget)target).CorrelationMatrix, "Target");
            }

            if (clips.IsSet("x"))
            {
                double x = clips.Get("x", double.Parse);
                return CliGenomePlotter.PlotExpressionVector(clips, make(x).Vector, ""+x, null);
            }

            var plot = new PlotModel() { Title = title };

            plot.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Progress", Key = "x" });
            plot.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Benefit", Key = "y" });

            int resolution = clips.Get("resolution", int.Parse, 1000);
            var data = Misc.DRange(0.0, b-a, 1.0 / resolution).Select(x => new DataPoint(x, target.Judge(make(x)))).ToArray();

            if (clips.Get("showminimum", bool.Parse, true))
            {
                double minimum = Misc.ArgMin(data, dp => dp.Y).X;
                plot.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = minimum, Text = $"Minimum at {minimum}" });
            }

            plot.Series.Add(new LineSeries() { ItemsSource = data });

            return plot;
        }
    }

    public class CliTopologyOptimisation : ICliMiscPlotter
    {
        public string Key => "TopologyOptimisation";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            int seed = clips.Get("seed", int.Parse, 1);
            
            var exp = PopulationExperiment<DenseIndividual>.Load(clips.Get("expfile"));
            var config = exp.PopulationConfig.ExperimentConfiguration;
            var rand = new MathNet.Numerics.Random.MersenneTwister(seed, false);
            var context = new ModelExecutionContext(rand);

            int targetIdx = clips.Get("target", int.Parse, 0);
            var target = config.Targets[targetIdx];
            
            if (clips.Get("resetTarget", bool.Parse, false))
            {
                int exposureEpoch = clips.Get("exposureepoch", int.Parse, 1);

                ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
            }

            target.NextGeneration(rand, config.JudgementRules);

            var template = exp.Population.PeekAll()[0];
            var templateGenome = template.Genome;
            
            var maxWeight = clips.GetOrCreate("maxweight", double.Parse, () => template.Genome.TransMat.Enumerate().Sum());
            var resolution = clips.Get("resolution", int.Parse, 10);

            var threshold = template.Genome.TransMat.Enumerate().Select(Math.Abs).Max() / 10;
            console.WriteLine("threshold: " + threshold);
            var paramEntries = templateGenome.TransMat.EntriesWhere(x => Math.Abs(x) > threshold).ToArray();
            foreach (var e in paramEntries)
                console.WriteLine(e);
            var optimised = Optimise(context, templateGenome, paramEntries, config.DevelopmentRules, target, config.JudgementRules, maxWeight, resolution);
            
            // plot
            var plot = new PlotModel { Title = title };
            plot.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Total Weight", Key = "x", Minimum = 0 });
            plot.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Regulatory Assignments", Key = "y", Minimum = 0 });

            LineSeries[] ls = new LineSeries[paramEntries.Length];
            for (int i = 0; i < paramEntries.Length; i++)
            {
                var e = paramEntries[i];
                ls[i] = new LineSeries() { Title = e.ToString() };
                foreach (var vk in optimised)
                {
                    ls[i].Points.Add(new DataPoint(vk.Key, vk.Value.TransMat[e.Row, e.Col]));
                }
                plot.Series.Add(ls[i]);
            }

            return plot;
        }

        public static Dictionary<double, DenseGenome> Optimise(ModelExecutionContext context, DenseGenome template, IReadOnlyList<MatrixEntryAddress> paramEntries, DevelopmentRules drules, ITarget target, JudgementRules jrules, double maxTotalWeight, int resolution)
        {
            var results = new Dictionary<double, DenseGenome>();

            var paramCount = paramEntries.Count;
            var genome = template.Clone(context);
            var bestGenome = template.Clone(context);
            var phenotype = genome.Develop(context, drules); // really need to develop; just an easy way to get an appropriate Phenotype
            var bestFitness = double.NegativeInfinity;
            
            var paramArray = new double[paramCount];
            var maxes = Misc.Create(paramCount, _ => resolution * paramCount + 1);
            var signs = paramEntries.Select(e => Math.Sign(template.TransMat[e.Row, e.Col])).ToArray();
            for (double total = maxTotalWeight / resolution; total <= maxTotalWeight; total += maxTotalWeight / resolution)
            {
                foreach (var p in Combinatorics.EnumerateTotalUnorderedParameterisations(paramCount, total, resolution))
                {
                    for (int i = 0; i < paramCount; i++)
                    {
                        genome.TransMat[paramEntries[i].Row, paramEntries[i].Col] = p[i];
                    }

                    genome.DevelopInto(phenotype, context, drules);
                    var j = MultiMeasureJudgement.Judge(genome, phenotype, jrules, target);
                    var f = j.CombinedFitness;

                    if (f > bestFitness)
                    {
                        // swap them around
                        var t = bestGenome;
                        bestGenome = genome;
                        bestFitness = f;
                        genome = t;
                    }
                }

                results.Add(total, bestGenome.Clone(context));
                bestFitness = double.NegativeInfinity;
            }

            return results;
        }
    }

    public class CliQ : ICliMiscPlotter
    {
        public string Key => "Q";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var expFile = clips.Get("expfile", PopulationExperiment<DenseIndividual>.Load, null);

            int Q;
            int size;
            double mg;
            double gmin;
            double gmax;
            NoiseType mgType = NoiseType.Uniform; // make customisable
            if (expFile != null)
            {
                var rrules = expFile.PopulationConfig.ExperimentConfiguration.ReproductionRules;
                var config = expFile.PopulationConfig.ExperimentConfiguration;

                mg = clips.Get("mg", double.Parse, rrules.InitialStateMutationSize);
                gmin = clips.Get("gmin", double.Parse, rrules.InitialStateClamping.Min);
                gmax = clips.Get("gmax", double.Parse, rrules.InitialStateClamping.Max);
                mgType = rrules.InitialStateMutationType;
                
                size = clips.Get("size", int.Parse, config.Size);

                if (expFile.PopulationConfig.PopulationResetOperation is NeutralMutationPopulationResetOperation nmpro)
                {
                    Q = clips.Get("Q", Misc.ParseInt, nmpro.NeutralMutationCount);
                }
                else
                {
                    Q = clips.Get("Q", Misc.ParseInt);
                }
            }
            else
            {
                Q = clips.Get("Q", Misc.ParseInt);
                mg = clips.Get("mg", double.Parse);
                gmin = clips.Get("gmin", double.Parse);
                gmax = clips.Get("gmax", double.Parse);
                size = clips.Get("n", int.Parse);
            }

            int sampleCount = clips.Get("count", int.Parse, 1000);
            int subSize = clips.Get("subsize", int.Parse);
            int binCount = clips.Get("binCount", int.Parse, 20);
            int seed = clips.GetOrCreate("seed", int.Parse, () => CliExp.SeedSource.Next());

            var rnd = new Random(seed);

            var samples = Enumerable.Range(0, sampleCount).Select(_ => Sample(rnd, Q, mg, gmin, gmax, mgType, size)).Select(s => s.Take(subSize).Average()).ToArray();
            var hs = new HistogramSeries();
            var items = HistogramHelpers.Collect(samples, HistogramHelpers.CreateUniformBins(gmin, gmax, binCount), new BinningOptions(BinningOutlierMode.RejectOutliers, BinningIntervalType.InclusiveLowerBound, BinningExtremeValueMode.IncludeExtremeValues));
            hs.Items.AddRange(items);

            var mean = MathNet.Numerics.Statistics.Statistics.Mean(samples);
            var variance = MathNet.Numerics.Statistics.Statistics.Variance(samples);

            var plot = new PlotModel();
            plot.Title = title;
            plot.Subtitle = $"Mean={mean} Variance={variance}";

            var slices = clips.Get("slices", CliPlotHelpers.ParseDoubleList, Array.Empty<double>());

            foreach (var slice in slices)
            {
                var gprop = samples.Count(x => x > slice) / (double)samples.Length;

                var la = new LineAnnotation() { Text = $"{slice:0.000}: {gprop*100:00.00}%", Type = LineAnnotationType.Vertical, X = slice };
                plot.Annotations.Add(la);
            }

            var interactiveSlice = new LineAnnotation() { Text = null, Type = LineAnnotationType.Vertical, X = -1000, Color = OxyColors.Blue, TextVerticalAlignment = VerticalAlignment.Top  };
            plot.MouseMove += (s, e) =>
            {
                plot.Annotations.Remove(interactiveSlice);
                
                var slice = plot.DefaultXAxis.InverseTransform(e.Position.X);
                var gprop = samples.Count(x => x > slice) / (double)samples.Length;
                if (slice >= gmin && slice <= gmax)
                {
                    interactiveSlice.Text = $"{slice:0.000}: {gprop * 100:00.00}%";
                    interactiveSlice.X = slice;
                    plot.Annotations.Add(interactiveSlice);
                    plot.InvalidatePlot(false);
                }
            };
            plot.MouseDown += (s, e) =>
            {
                if (e.ChangedButton == OxyMouseButton.Left)
                {
                    interactiveSlice = new LineAnnotation() { Text = null, Type = LineAnnotationType.Vertical, X = -1000, Color = OxyColors.Blue };
                }
            };

            if (clips.IsSet("mctmins"))
            {
                var mcinfo = clips.Get("mctmins", CliPlotHelpers.ParseDoubleList);
                bool tmsMeanAdjusted = clips.Get("tmsMeanAdjusted", bool.Parse, true);
                var mcn = (int)mcinfo[0];
                var mck = (int)mcinfo[1];
                var mcp = mcinfo[2];

                for (int a = 0; a < mcn; a++)
                {
                    double T = mck;
                    double R = (mcn - 2 * a - 1) * (tmsMeanAdjusted ? -mean : 1.0);
                    double tms = -mcp * R;
                    var gprop = samples.Count(x => x > tms) / (double)samples.Length;

                    var la = new LineAnnotation() { Text = $"MC{a} at {tms:0.0000}: {gprop*100:00.00}%", Type = LineAnnotationType.Vertical, X = tms, Color = OxyColors.Red };
                    plot.Annotations.Add(la);
                }
            }

            plot.Series.Add(hs);
            return plot;
        }

        public static double[] Sample(Random rnd, int Q, double mg, double gMin, double gMax, NoiseType mgType, int size)
        {
            var x = Misc.Create(size, gMin);

            for (int i = 0; i < Q; i++)
            {
                int r = rnd.Next(size);
                x[r] = Misc.Clamp(x[r] + Misc.NextNoise(rnd, mg, mgType), gMin, gMax);
            }

            return x;
        }
    }

    public class CliDevStream : ICliMiscPlotter
    {
        public string Key => "DevStream";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var expFile = clips.Get("expfile", PopulationExperiment<DenseIndividual>.Load, null);
            var sampleCount = clips.Get("sampleCount", int.Parse, 10);

            var config = expFile.PopulationConfig.ExperimentConfiguration;
            var grange = config.InitialStateResetRange;

            int g0 = clips.Get("g0", int.Parse, 0);
            int g1 = clips.Get("g1", int.Parse, 1);

            var plot = new PlotModel() { Title = "DevStreams" };
            var template = expFile.Population.ExtractAll()[0];

            var rand = new CustomMersenneTwister(1);
            var ctx = new ModelExecutionContext(rand);

            double[][] trajectories = null;

            for (int i = 0; i <= sampleCount; i++)
            {
                var t0 = grange.Min + (grange.Max - grange.Min) * (i / (double)sampleCount);
                for (int j = 0; j <= sampleCount; j++)
                {
                    var t1 = grange.Min + (grange.Max - grange.Min) * (j / (double)sampleCount);

                    var g = CreateVector.Dense<double>(config.Size);

                    g[g0] = t0;
                    g[g1] = t1;

                    var individual = template.Clone(ctx);
                    individual.Genome.CopyOverInitialState(g);
                    individual.Genome.DevelopWithTrajectories(rand, config.DevelopmentRules, ref trajectories);
                }
            }



            return plot;
        }
    }

    public class CliHi : ICliMiscPlotter
    {
        public string Key => "Hi";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var config = clips.Get("expfile", CliPlotHelpers.LoadDenseExperimentConfig);
            var modules = clips.Get("modules", Modular.MultiModulesStringParser.Instance.Parse);

            int targetIdx = clips.Get("target", int.Parse, 0);
            var target = config.Targets[targetIdx];

            var perfectp = clips.GetOrCreate(
                "perfectp",
                s => Misc.ParseExtremesVector(s, -1.0, 1.0, 0.0),
                () =>
                {
                    if (target is IPerfectPhenotypeTarget ppt)
                        return ppt.PreparePerfectP();
                    else
                        return CreateVector.Dense(target.Size, 1.0);
                });

            var k = clips.Get("k", int.Parse, 1);
            var n = clips.Get("n", int.Parse, config.Size / k);
            
            var a = clips.Get("a", int.Parse, 0); // number of modules with which we disagree at start
            var b = clips.Get("b", int.Parse, n * k);
            var terpMode = clips.Get("terpMode", "One");

            int seed = clips.Get("seed", int.Parse, 1);
            var rand = new CustomMersenneTwister(seed);
            var ctx = new ModelExecutionContext(rand);

            var output = clips.Get("output", "Fitness");

            ExposureInformation exposureInformation = new ExposureInformation(1); // ignored
            target.NextExposure(rand, new JudgementRules(0.0, null, 0.0), 0, ref exposureInformation);
            target.NextGeneration(rand, new JudgementRules(0.0, null, 0.0));

            var parentMap = modules.GetDefaultLeaderMap();
            var correls = Misc.Correlate(perfectp);

            DenseGenome makeGenome(double x)
            {
                Linear.Matrix<double> mat = Linear.CreateMatrix.Dense<double>(n * k, n * k);
                if (terpMode.SoftEquals("one"))
                {
                    for (int i = 0; i < parentMap.Length; i++)
                    {
                        var j = parentMap[i];
                        if (i == j)
                        {
                            mat[i, i] = 1.0;
                        }
                        else
                        {
                            mat[i, i] = Misc.Clamp((x * k - (i - a * k)), 0, +1);
                            mat[i, j] = Misc.Clamp(1.0 - (x * k - (i - a * k)), 0, +1);
                        }
                    }
                }
                else
                {
                    throw new Exception("Unrecognised Interpolation Mode: " + terpMode);
                }

                mat.PointwiseMultiply(correls);

                var genome = new DenseGenome(perfectp, mat);
                return genome;
            }

            double judge(double x)
            {
                var genome = makeGenome(x);
                var phenotype = genome.Develop(ctx, config.DevelopmentRules);
                var mmq = MultiMeasureJudgement.Judge(genome, phenotype, config.JudgementRules, target);

                if (output == "Fitness")
                    return mmq.CombinedFitness;
                else if (output == "Benefit")
                    return mmq.Benefit;
                else if (output == "Cost")
                    return mmq.Cost;
                else
                    throw new Exception("Unrecognised output: " + output);
            }

            if (clips.IsSet("target"))
            {
                return CliExpPlotter.PlotCorrelationMatrix(clips, ((Epistatics.CorrelationMatrixTarget)target).CorrelationMatrix, "Target");
            }

            if (clips.IsSet("x"))
            {
                double x = clips.Get("x", double.Parse);
                var genome = makeGenome(x);
                Analysis.SaveGenome($"genome_{x}.dat", genome);
                return CliGenomePlotter.PlotDtm(clips, genome.TransMat, "" + x);
            }

            var plot = new PlotModel() { Title = title };

            plot.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Progress", Key = "x" });
            plot.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = output, Key = "y" });

            int resolution = clips.Get("resolution", int.Parse, 1000);
            var data = Misc.DRange(0.0, b - a, 1.0 / resolution).Select(x => new DataPoint(x, judge(x))).ToArray();

            double minimum = Misc.ArgMin(data, dp => dp.Y).X;
            plot.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Vertical, X = minimum, Text = $"Minimum at {minimum}" });

            plot.Series.Add(new LineSeries() { ItemsSource = data });

            return plot;
        }
    }

    public class CliSquash : ICliMiscPlotter
    {
        public string Key => "squash";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var config = clips.Get("expfile", CliPlotHelpers.LoadDenseExperimentConfig);

            var plot = new PlotModel() { Title = title };

            var xmin = clips.Get("xmin", double.Parse, -config.DevelopmentRules.UpdateRate / config.DevelopmentRules.DecayRate);
            var xmax = clips.Get("xmax", double.Parse, +config.DevelopmentRules.UpdateRate / config.DevelopmentRules.DecayRate);

            plot.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Input", Key = "x" });
            plot.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Output", Key = "y" });

            int resolution = clips.Get("resolution", int.Parse, 1000);
            var data = Misc.DRange(xmin, xmax, 1.0 / resolution).Select(x => new DataPoint(x, config.DevelopmentRules.Squash.Squash(x))).ToArray();

            plot.Series.Add(new LineSeries() { ItemsSource = data });

            return plot;
        }
    }

    public class CliCoins : ICliMiscPlotter
    {
        public string Key => "coins";

        public PlotModel Plot(TextWriter console, CliParams clips, string title)
        {
            var rand = new CustomMersenneTwister(1);

            // TODO: add Z support

            // generate random strings
            // take hebbian integral
            // plot distribution of inter-module correlations (they are all spurious)
            // what we really want to know is the standard deviation 

            if (clips.IsSet("log") || clips.IsSet("linear"))
            {
                var ns = clips.Get("n", Misc.ParseIntList).OrderBy(x => x); // number of modules
                var es = clips.Get("e", Misc.ParseIntList); // number of epochs / samples

                var plot = new PlotModel();
                var xaxis = clips.IsSet("log")
                    ? (Axis)new LogarithmicAxis() { Title = "n", Position = AxisPosition.Bottom }
                    : (Axis)new LinearAxis() { Title = "n", Position = AxisPosition.Bottom };
                var yaxis = new LinearAxis() { Title = "Std-dev", Position = AxisPosition.Left };
                plot.Axes.Add(xaxis);
                plot.Axes.Add(yaxis);

                foreach (var e in es)
                {
                    var ls = new LineSeries() { Title = $"e = {e}" };
                    foreach (var n in ns)
                    {
                        var samples = QuickCoinSample(rand, n, e);
                        var variance = MathNet.Numerics.Statistics.Statistics.Variance(samples);
                        var stddev = Math.Sqrt(variance);
                        ls.Points.Add(new DataPoint(n, stddev));
                    }
                    plot.Series.Add(ls);
                }

                return plot;
            }
            else
            {
                var n = clips.Get("n", int.Parse); // number of modules
                var e = clips.Get("e", int.Parse); // number of epochs / samples

                var binCount = clips.Get("bincount", int.Parse, e * 2 + 1);

                var plot = QuickCoinHistogram(e, QuickCoinSample(rand, n, e), binCount);
                plot.Title = title;
                return plot;
            }
        }

        public static PlotModel QuickCoinHistogram(int e, IEnumerable<double> samples, int binCount)
        {
            var hs = new HistogramSeries();
            var items = HistogramHelpers.Collect(samples, HistogramHelpers.CreateUniformBins(-e -0.5, +e + 0.5, binCount), new BinningOptions(BinningOutlierMode.RejectOutliers, BinningIntervalType.InclusiveLowerBound, BinningExtremeValueMode.IncludeExtremeValues));
            hs.Items.AddRange(items);

            var mean = MathNet.Numerics.Statistics.Statistics.Mean(samples);
            var variance = MathNet.Numerics.Statistics.Statistics.Variance(samples);
            var stddev = Math.Sqrt(variance);

            var xaxis = new LinearAxis() { Title = "Value", Position = AxisPosition.Bottom };
            var yaxis = new LinearAxis() { Title = "Frequency", Position = AxisPosition.Left };

            var plot = new PlotModel();
            plot.Subtitle = $"Mean={mean} Variance={variance}";
            plot.Series.Add(hs);
            plot.Axes.Add(xaxis);
            plot.Axes.Add(yaxis);
            return plot;
        }

        public static IReadOnlyList<double> QuickCoinSample(Random rand, int n, int e)
        {
            var mat = CreateMatrix.Dense<double>(n, n);
            var temp = CreateMatrix.Dense<double>(n, n);
            var v = CreateVector.Dense<double>(n);

            for (int i = 0; i < e; i++)
            {
                Misc.FillRandom(v, rand, 1, NoiseType.Binary);
                v.OuterProduct(v, temp);
                mat.Add(temp, mat);
            }

            var samples = new List<double>();
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    samples.Add(mat[i, j]);
                }
            }

            return samples;
        }
    }

    public static class GenomeHelpers
    {
        public static Func<double, Linear.Matrix<double>> PrepareDtmCreater(string dtm, int N)
        {
            return dtm == "identity" ? (Func<double, Linear.Matrix<double>>)(weight => GenomeHelpers.CreateIdentity(N, 1, weight / N)) :
                dtm == "dense" ? (Func<double, Linear.Matrix<double>>)(weight => GenomeHelpers.CreateDense(N, 1, weight / (N * N))) :
                dtm == "husky" ? (Func<double, Linear.Matrix<double>>)(weight => GenomeHelpers.CreateHusky(N, 1, weight / N)) :
                throw new Exception("Unrecognised dtm, try one of 'identity, dense, husky'");
        }

        public static Linear.Matrix<double> CreateIdentity(int moduleSize, int moduleCount, double commonWeight)
        {
            int n = moduleCount * moduleSize;
            Linear.Matrix<double> dtm = Linear.CreateMatrix.Dense(n, n, (i, j) => i == j ? commonWeight : 0.0);
            return dtm;
        }

        public static Linear.Matrix<double> CreateDense(int moduleSize, int moduleCount, double commonWeight)
        {
            int n = moduleCount * moduleSize;
            Linear.Matrix<double> dtm = Linear.CreateMatrix.Dense(n, n, (i, j) => i / moduleSize == j / moduleSize ? commonWeight : 0.0);
            return dtm;
        }

        public static Linear.Matrix<double> CreateHusky(int moduleSize, int moduleCount, double commonWeight)
        {
            int n = moduleCount * moduleSize;
            Linear.Matrix<double> dtm = Linear.CreateMatrix.Dense(n, n, (i, j) => i / moduleSize == j / moduleSize && j % moduleSize == 0 ? commonWeight : 0.0);
            return dtm;
        }

        public static Linear.Matrix<double> CreateDense(Linear.Vector<double> g, Modular.Modules modules, double onblock, double offblock)
        {
            int n = modules.ElementCount;
            Linear.Matrix<double> dtm = Linear.CreateMatrix.Dense(n, n, offblock);
            foreach (var module in modules.ModuleAssignments)
            {
                foreach (var i in module)
                {
                    foreach (var j in module)
                    {
                        dtm[i, j] = onblock * Math.Sign(g[i] * g[j]);
                    }
                }
            }
            return dtm;
        }

        public static Linear.Matrix<double> CreateHusky(Modular.Modules modules, double commonWeight)
        {
            int n = modules.ElementCount;
            Linear.Matrix<double> dtm = Linear.CreateMatrix.Dense(n, n, 0.0);
            foreach (var module in modules.ModuleAssignments)
            {
                foreach (var i in module)
                {
                    dtm[i, module[0]] = commonWeight;
                }
            }
            return dtm;
        }

        /// <summary>
        /// Enumerates all vectors of length <paramref name="size"/> where each entry is either <c>1</c> or <c>-1</c>.
        /// </summary>
        /// <param name="size">The length of the vectors.</param>
        /// <returns></returns>
        public static IEnumerable<Linear.Vector<double>> EnumerateAllBinary(int size)
        {
            double[] c = new double[size];

            for (int i = 0; i < size; i++)
            {
                c[i] = -1;
            }

            while (true)
            {
                yield return Linear.CreateVector.DenseOfArray(c);

                int i = 0;
                for (; i < size; i++)
                {
                    if (c[i] == -1)
                    {
                        c[i] = 1;
                        break;
                    }
                    else // c[i] == 1
                    {
                        c[i] = -1;
                    }
                }

                if (i == size)
                    yield break;
            }
        }
    }
}
