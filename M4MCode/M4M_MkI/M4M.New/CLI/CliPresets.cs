﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace M4M
{
    public class CliPresets
    {
        public string Key { get; }
        
        public Dictionary<string, string> Presets { get; }

        public CliPresets(string key, Dictionary<string, string> presets)
        {
            Key = key;
            Presets = presets;
        }

        public CliPresets(string key, bool caseSensitive = false)
        {
            Key = key;
            Presets = new Dictionary<string, string>(caseSensitive ? StringComparer.InvariantCulture : StringComparer.InvariantCultureIgnoreCase);
        }

        public void Apply(TextWriter console, CliParams clips)
        {
            var presets = clips.Get(Key, null);

            if (presets != null)
            {
                foreach (var preset in StringHelpers.Split(presets))
                {
                    if (Presets.TryGetValue(preset, out var presetArgs))
                    {
                        clips.ConsumeLine(presetArgs);
                    }
                    else
                    {
                        throw new Exception("Unrecognise " + Key + " preset '" + preset + "'");
                    }
                }
            }
        }
    }
}
