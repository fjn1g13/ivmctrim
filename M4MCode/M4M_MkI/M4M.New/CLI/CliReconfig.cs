﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M
{
    public class CliReconfig : ICliProvider
    {
        public static readonly CliPresets DefaultPresets = new CliPresets("reconfigpresets", new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
        {
            ["disableB"] = "rrules.mb=0", // we leave rb alone
            ["hillcimber"] = "pop.size=1 hillclimbermode=true elitecount=1", // we leave rb alone
        });

        public string Key => "reconfig";

        public CliPresets Presets { get; }

        public CliReconfig(CliPresets presets)
        {
            Presets = presets;
        }

        public void Run(TextWriter console, CliParams clips)
        {
            Presets.Apply(console, clips);

            var rand = new MathNet.Numerics.Random.MersenneTwister(false);

            var targetAddress = clips.Get("reconfig");
            var outdir = clips.Get("outdir", "");
            var postfix = clips.Get("postfix");
            
            var target = PopulationExperiment<DenseIndividual>.Load(targetAddress);
            
            var popExp = Reconfig(console, clips, target, rand, outdir + postfix);
            
            if (!clips.IsSet("quiet"))
                console.WriteLine("Writing reconfiged to " + popExp.FileStuff.OutDir);

            PopulationExperimentRunners.WriteOutInitials(rand, popExp);
            popExp.Save("starter", false);
        }

        public static DevelopmentRules Reconfig(CliParams clips, DevelopmentRules drules)
        {
            var timeSteps = clips.Get("drules.T", int.Parse, drules.TimeSteps);
            var updateRate = clips.Get("drules.tau1", double.Parse, drules.UpdateRate);
            var decayRate = clips.Get("drules.tau2", double.Parse, drules.DecayRate);
            var rescaleScale = clips.Get("drules.rescaleScale", double.Parse, drules.RescaleScale);
            var rescaleOffset = clips.Get("drules.rescaleOffset", double.Parse, drules.RescaleOffset);
            var squash = drules.Squash;
            drules = new DevelopmentRules(timeSteps, updateRate, decayRate, squash, rescaleScale, rescaleOffset);

            return drules;
        }

        public static ReproductionRules Reconfig(CliParams clips, ReproductionRules rrules)
        {
            var initialTraitUpdates = clips.Get("rrules.CG", int.Parse, rrules.InitialTraitUpdates);
            var exclusiveBMutation = clips.Get("rrules.bex", bool.Parse, rrules.ExclusiveBMutation);
            var dtmMutationRate = clips.Get("rrules.RB", double.Parse, rrules.DevelopmentalTransformationMatrixRate);
            var dtmMutationSize = clips.Get("rrules.MB", double.Parse, rrules.DevelopmentalTransformationMatrixMutationSize);
            var isMutationSize = clips.Get("rrules.MG", double.Parse, rrules.InitialStateMutationSize);
            var gNoiseType = clips.Get("rrules.MGtype", ExperimentParsing.ParseNoiseType, rrules.InitialStateMutationType);
            var bNoiseType = clips.Get("rrules.MBtype", ExperimentParsing.ParseNoiseType, rrules.DevelopmentalTransformationMatrixMutationType);
            rrules = new ReproductionRules(isMutationSize, dtmMutationSize, dtmMutationRate, exclusiveBMutation, initialTraitUpdates, rrules.InitialStateClamping, gNoiseType, bNoiseType);

            return rrules;
        }
        
        public static JudgementRules Reconfig(CliParams clips, JudgementRules jrules)
        {
            var lambda = clips.Get("jrules.lambda", double.Parse, jrules.RegularisationFactor);
            var kappa = clips.Get("jrules.kappa", double.Parse, jrules.NoiseFactor);
            var regularisationFunction = clips.Get("jrules.regfunc", ExperimentParsing.ParseRegFunction, jrules.RegularisationFunction);
            jrules = new JudgementRules(lambda, regularisationFunction, kappa);

            return jrules;
        }

        public static ITarget[] Retarget(CliParams clips, ITarget[] targets)
        {
            targets = targets.ToArray(); // soft copy

            if (clips.IsSet("SaturateTargets"))
            {                
                var satMin = clips.Get("satMin", double.Parse, -1.0);
                var satThreshold = clips.Get("satThreshold", double.Parse, 0.0);
                var satMax = clips.Get("satMax", double.Parse, +1.0);
                
                for (int i = 0; i < targets.Length; i++)
                {
                    if (targets[i] is SaturationTarget st)
                    {
                       targets[i] = st.Target;
                    }

                    targets[i] = new SaturationTarget(targets[i], satMin, satThreshold, satMax);
                }
            }

            return targets;
        }

        public static ExperimentConfiguration Reconfig(TextWriter console, CliParams clips, ExperimentConfiguration config)
        {
            // drules
            var drules = config.DevelopmentRules;
            drules = Reconfig(clips, drules);
            
            // rrules
            var rrules = config.ReproductionRules;
            rrules = Reconfig(clips, rrules);

            // jrules
            var jrules = config.JudgementRules;
            jrules = Reconfig(clips, jrules);

            // config
            var epochs = clips.Get("epochs", int.Parse, config.Epochs);
            var K = clips.Get("K", int.Parse, config.GenerationsPerTargetPerEpoch);
            var initialStateResetProbability = clips.Get("initialStateResetProbability", double.Parse, config.InitialStateResetProbability);
            var targets = clips.GetOrCreate("targets", targetsFile => ExperimentParsing.ParseEpistaticTargetPackage(console, clips, "default", targetsFile).Targets, () => Retarget(clips, config.Targets.ToArray()));
            if (clips.IsSet("targetpackage"))
                targets = ExperimentComposition.Typical.DefaultDenseExperimentComposer.TargetPackageComposer.Compose(console, clips).Targets.ToArray();
            var targetCycler = clips.Get("targetCycler", targetsFile => ExperimentExtractorExperimentReceiver.LoadExtractorFromFile(targetsFile).ExperimentConfiguration.TargetCycler, config.TargetCycler);
            config = new ExperimentConfiguration(config.Size, targets, targetCycler, epochs, K, initialStateResetProbability, config.InitialStateResetRange, false, drules, rrules, jrules);

            return config;
        }

        public static PopulationExperimentConfig<DenseIndividual> Reconfig(TextWriter console, CliParams clips, PopulationExperimentConfig<DenseIndividual> popConfig)
        {
            // config
            var config = popConfig.ExperimentConfiguration;
            config = Reconfig(console, clips, config);

            // popConfig
            var eliteCount = clips.Get("elitecount", int.Parse, popConfig.EliteCount);
            var hillclimberMode = clips.Get("hillclimberMode", bool.Parse, popConfig.HillclimberMode);
            var endTargetOperation = popConfig.PopulationEndTargetOperation;
            var spinner = clips.Get("spinner", ExperimentComposition.DefaultPopulationSpinnerComposer<DenseIndividual>.Parse, popConfig.CustomPopulationSpinner);
            var selectorPreparer = clips.Get("selectorpreparer", ExperimentParsing.ParseSelectorPreparer<DenseIndividual>, popConfig.SelectorPreparer);
            var resetOperation = clips.Get("resetoperation", ExperimentParsing.ParsePopulationResetOperation, popConfig.PopulationResetOperation);
            
            popConfig = new PopulationExperimentConfig<DenseIndividual>(config, selectorPreparer, eliteCount, hillclimberMode, endTargetOperation, resetOperation, spinner);

            return popConfig;
        }

        public static Population<DenseIndividual> Reconfig(CliParams clips, Population<DenseIndividual> population, MathNet.Numerics.Random.RandomSource rand, PopulationExperimentConfig<DenseIndividual> popConfig)
        {
            var config = popConfig.ExperimentConfiguration;

            if (clips.IsSet("population"))
            {
                population = clips.Get("population", populationFile => PopulationExperiment<DenseIndividual>.Load(populationFile).Population);
            }
            
            if (clips.IsSet("genome"))
            {
                var genome = clips.Get("genome", genomeFile => Analysis.LoadGenome(genomeFile));
                var context = new ModelExecutionContext(rand);
                var drules = config.DevelopmentRules;

                var newPopulation = new List<DenseIndividual>();
                foreach (var oi in population.ExtractAll())
                {
                    oi.Genome.CopyOverInitialState(genome.InitialState);
                    oi.Genome.CopyOverTransMat(genome.TransMat);
                    newPopulation.Add(DenseIndividual.Develop(oi.Genome, context, drules, oi.Epigenetic));
                }
                
                population = new Population<DenseIndividual>(newPopulation);
            }

            if (clips.IsSet("g0"))
            {
                var context = new ModelExecutionContext(rand);
                var drules = config.DevelopmentRules;

                var g0string = clips.Get("g0");
                var g0 = Misc.ParseExtremesVector(g0string, config.ReproductionRules.InitialStateClamping.Min, config.ReproductionRules.InitialStateClamping.Max, 0.0);

                var newPopulation = new List<DenseIndividual>();
                foreach (var oi in population.ExtractAll())
                {
                    oi.Genome.CopyOverInitialState(g0);
                    newPopulation.Add(DenseIndividual.Develop(oi.Genome, context, drules, oi.Epigenetic));
                }
                
                population = new Population<DenseIndividual>(newPopulation);
            }

            if (clips.IsSet("b0"))
            {
                var context = new ModelExecutionContext(rand);
                var drules = config.DevelopmentRules;

                var b0string = clips.Get("b0");
                var b0 = ExperimentParsing.ParseMatrix(b0string);

                if (b0 == null && clips.IsSet("templategenome"))
                {
                    var templateGenome = Analysis.LoadGenome(clips.Get("templategenome"));
                    b0 = templateGenome.TransMat;
                }

                if (clips.IsSet("rndnormalb"))
                {
                    var mag = clips.Get("rndnormalb", double.Parse, 1.0);
                    var nrmMat = MathNet.Numerics.LinearAlgebra.CreateMatrix.Random<double>(config.Size, config.Size, new MathNet.Numerics.Distributions.Normal(0.0, mag, context.Rand));

                    if (b0 != null)
                        b0.PointwiseMultiply(nrmMat, b0);
                    else
                        b0 = nrmMat;
                }

                if (clips.IsSet("rnduniformb"))
                {
                    var mag = clips.Get("rnduniformb", double.Parse, 1.0);
                    var uniformMat = MathNet.Numerics.LinearAlgebra.CreateMatrix.Random<double>(config.Size, config.Size, new MathNet.Numerics.Distributions.ContinuousUniform(-mag, mag, context.Rand));

                    if (b0 != null)
                        b0.PointwiseMultiply(uniformMat, b0);
                    else
                        b0 = uniformMat;
                }

                var newPopulation = new List<DenseIndividual>();
                foreach (var oi in population.ExtractAll())
                {
                    oi.Genome.CopyOverTransMat(b0);
                    newPopulation.Add(DenseIndividual.Develop(oi.Genome, context, drules, oi.Epigenetic));
                }
                
                population = new Population<DenseIndividual>(newPopulation);
            }
            
            if (clips.IsSet("transmatmutator"))
            {
                ITransMatMutator transMatMutator = GenomeParsing.GetTransMatMutator(clips.Get("transmatmutator", "default"), config.Size);
                
                foreach (var oi in population.PeekAll())
                    oi.Genome.SetCustomTransmatMutator(transMatMutator);
            }

            if (clips.IsSet("initialstatemutator"))
            {
                IInitialStateMutator initialStateMutator = GenomeParsing.GetInitialStateMutator(clips.Get("initialstatemutator", "default"), config.Size);
                
                foreach (var oi in population.PeekAll())
                    oi.Genome.SetCustomInitialStateMutator(initialStateMutator);
            }

            int popSize = clips.Get("pop.size", int.Parse, population.Count);
            var extracted = population.ExtractAll();
            Misc.ShuffleInplace(rand, extracted);
            population = new Population<DenseIndividual>(TakeLoop(extracted, popSize)); // apparently I don't need to clone... suits me

            return population;
        }

        public static PopulationExperiment<DenseIndividual> Reconfig(TextWriter console, CliParams clips, PopulationExperiment<DenseIndividual> target, MathNet.Numerics.Random.RandomSource rand, string outdir)
        {
            // popConfig
            var popConfig = target.PopulationConfig;
            popConfig = Reconfig(console, clips, popConfig);

            // population
            var population = target.Population;
            population = Reconfig(clips, population, rand, popConfig);

            bool timestamp = clips.IsSet("timestamp");

            // population experiment
            var fileStuff = FileStuff.CreateNow(outdir, "", "", true, timestamp);
            var popExp = new PopulationExperiment<DenseIndividual>(population, popConfig, fileStuff);

            return popExp;
        }

        public static IEnumerable<T> TakeLoop<T>(IReadOnlyList<T> source, int count)
        {
            int i = 0;
            for (int j = 0; j < count; j++)
            {
                yield return source[i];
                i = i + 1 % source.Count;
            }
        }
    }
}
