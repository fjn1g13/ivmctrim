﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M
{
    public class CliExtract : ICliProvider
    {
        public string Key => "extract";

        public void Run(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Extracts stuff from other stuff");
                console.WriteLine(" - extract, the source file to extract from (e.g. wholesamples)");
            }
            
            string source = clips.Get("extract");
            string fname = System.IO.Path.GetFileName(source);

            if (fname.StartsWith("wholesamples"))
            {
                ExtractFromWholeSamples(console, clips);
            }
            else if (fname.StartsWith("tracee"))
            {
                ExtractFromTracee(console, clips);
            }
            else if (fname.StartsWith("epoch"))
            {
                ExtractFromPopulationExperiment(console, clips);
            }
            else if (fname.StartsWith("rcs"))
            {
                ExtractDenseFromRcs(console, clips);
            }
            else
            {
                console.WriteLine("Can only extract from wholesamples and tracees");
            }
        }

        // will assume the experiment type for the experiment type
        public void ExtractFromWholeSamples(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Must provide the following to extract from a wholesamples:");
                console.WriteLine(" - extract, the wholesamples source file to extract from");
                console.WriteLine(" - expfile, the experiment save file to reconfigure");
                console.WriteLine(" - epoch, the epoch to extract");
                console.WriteLine(" - outdir, the experiment output directory");
                console.WriteLine(" - postfix, the experiment savefile postfix");
                console.WriteLine(" - zeroStart, whether to zero the start epochs and generations (default is false: keep)");
                console.WriteLine(" - firstOfEpoch, whether to take the first entry for the given epoch (default is false: takes the last)");
            }

            string wholeSamplesSource = clips.Get("extract");
            int[] epochs = clips.Get("epoch", Misc.ParseIntList);
            string expFile = clips.Get("expfile");
            string outdir = clips.Get("outdir");
            string postfix = clips.Get("postfix", "extract");
            bool firstOfEpoch = clips.Get("firstOfEpoch", bool.Parse, false);
            bool zeroStart = clips.Get("zeroStart", bool.Parse, false);
            bool appendEpoch = clips.Get("appendEpoch", bool.Parse, true);

            // load the experiment - inferring its type - and give us an IExperimentExtractor with which to work
            var expExtractor = PopulationExperimentHelpers.LoadUnknownType(expFile, new ExperimentExtractorExperimentReceiver());

            expExtractor.ExtractFromWholeSamples(wholeSamplesSource, epochs, firstOfEpoch, postfix, outdir, zeroStart, appendEpoch);
        }

        // will assume the experiment type for the experiment type
        public void ExtractFromTracee(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Must provide the following to extract from a tracee:");
                console.WriteLine(" - extract, the tracee source file to extract from");
                console.WriteLine(" - expfile, the experiment save file to reconfigure");
                console.WriteLine(" - generation, the generation to extract");
                console.WriteLine(" - outdir, the experiment output directory");
                console.WriteLine(" - postfix, the experiment savefile postfix");
                console.WriteLine(" - zeroStart, whether to zero the start epochs and generations (default is false: keep)");
                console.WriteLine(" - firstOfEpoch, whether to take the first entry for the given epoch (default is false: takes the last)");
            }

            string wholeSamplesSource = clips.Get("extract");
            int[] generations = clips.Get("generation", Misc.ParseIntList);
            string expFile = clips.Get("expfile");
            string outdir = clips.Get("outdir");
            string postfix = clips.Get("postfix", "extract");
            bool firstOfEpoch = clips.Get("firstOfEpoch", bool.Parse, false);
            bool zeroStart = clips.Get("zeroStart", bool.Parse, false);
            bool appendGeneration = clips.Get("appendGeneration", bool.Parse, true);

            // load the experiment - inferring its type - and give us an IExperimentExtractor with which to work
            var expExtractor = ExperimentExtractorExperimentReceiver.LoadExtractorFromFile(expFile);

            expExtractor.ExtractFromTraceInfo(wholeSamplesSource, generations, firstOfEpoch, postfix, outdir, zeroStart, appendGeneration);
        }

        // will assume the experiment type for the experiment type
        public void ExtractFromPopulationExperiment(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Must provide the following to extract from a tracee:");
                console.WriteLine(" - extract, the population experiment source file to extract from");
                console.WriteLine(" - expfile, the experiment save file to reconfigure");
                console.WriteLine(" - outdir, the output directory");
            }

            string expSoure = clips.Get("extract");
            string prefix = clips.Get("prefix", "");
            string outdir = clips.Get("outdir");

            // load the experiment - inferring its type - and give us an IExperimentExtractor with which to work
            var expExtractor = ExperimentExtractorExperimentReceiver.LoadExtractorFromFile(expSoure);
            expExtractor.ExtractGenomesFromWithin(prefix, outdir);
        }

        // will assume the experiment type for the experiment type
        public void ExtractDenseFromRcs(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Must provide the following to extract a dense experiment from an rcs and ist combination:");
                console.WriteLine(" - extract, the rcs source file to extract from");
                console.WriteLine(" - ist, the ist source file to extract from");
                console.WriteLine(" - epoch, the epoch to sample");
                console.WriteLine(" - expfile, the experiment save file to reconfigure");
                console.WriteLine(" - outdir, the experiment output directory");
                console.WriteLine(" - postfix, the experiment savefile postfix");
                console.WriteLine(" - zeroStart, whether to zero the start epochs and generations (default is false: keep)");
                console.WriteLine(" - seed, the random seed");
            }

            string rcsSource = clips.Get("extract");
            string istSource = clips.Get("ist");
            int epoch = clips.Get("epoch", Misc.ParseInt);
            string expFile = clips.Get("expfile");
            string outdir = clips.Get("outdir");
            int seed = clips.GetOrCreate("seed", s => int.Parse(s), () => CliExp.SeedSource.Next());
            string postfix = clips.Get("postfix", "extract");
            bool zeroStart = clips.Get("zeroStart", bool.Parse, false);
            int nominalSamplePeriod = clips.Get("nominalSamplePeriod", int.Parse, -1);

            var rcs = Analysis.LoadTrajectories(rcsSource, out int rcsSamplePeriod);
            var ist = Analysis.LoadTrajectories(istSource, out int istSamplePeriod);

            // all this is just handling the sample periods
            if (nominalSamplePeriod != -1)
            {
                if (rcsSamplePeriod != -1 && nominalSamplePeriod != rcsSamplePeriod)
                {
                    console.Write("Nominal sample period did not match sample period reported by rcs; ignoring nominal");
                }
                if (istSamplePeriod != -1 && nominalSamplePeriod != rcsSamplePeriod)
                {
                    console.WriteLine("Nominal sample period did not match sample period reported by rcs; ignoring nominal");
                }
            }

            if (rcsSamplePeriod == -1)
            {
                rcsSamplePeriod = nominalSamplePeriod;
            }
            if (istSamplePeriod == -1)
            {
                istSamplePeriod = nominalSamplePeriod;
            }

            if (istSamplePeriod < 0)
            {
                throw new Exception("Invalid ist sample periods");
            }
            if (rcsSamplePeriod < 0)
            {
                throw new Exception("Invalid rcs sample periods");
            }

            if (istSamplePeriod != rcsSamplePeriod)
            {
                // this is not a problem itself, but probably implies the user messed up
                console.WriteLine("rcs and ist sample periods are different");
            }
            //

            int rcsIndex = epoch / rcsSamplePeriod;
            int istIndex = epoch / rcsSamplePeriod;
            console.WriteLine("rcs epoch: " + rcsIndex * rcsSamplePeriod);
            console.WriteLine("ist epoch: " + istIndex * istSamplePeriod);

            var rand = new CustomMersenneTwister(seed);
            var context = new ModelExecutionContext(rand);
            var exp = PopulationExperiment<DenseIndividual>.Load(expFile);

            var expExtractor = new ExperimentExtractor<DenseIndividual>(exp);
            var config = expExtractor.ExperimentConfiguration;
            var drules = config.DevelopmentRules;
            int generationsPerEpoch = config.Targets.Count * config.GenerationsPerTargetPerEpoch;

            var example = exp.Population.PeekAll()[0];
            var genome = ExtractDenseGenomeFromRcsAndIst(rcs, ist, rcsIndex, istIndex, example.Genome);

            // popConfig
            var popConfig = exp.PopulationConfig;
            var template = DenseIndividual.Develop(genome, context, drules, example.Epigenetic);
            var population = new Population<DenseIndividual>(template, exp.Population.Count);

            // population experiment
            var fileStuff = FileStuff.CreateNow(outdir, "", "", true, false);
            var popExp = new PopulationExperiment<DenseIndividual>(population, popConfig, fileStuff, zeroStart ? 0 : epoch, zeroStart ? 0L : epoch * generationsPerEpoch);
            popExp.Save(postfix, false);
            Analysis.SaveGenome(System.IO.Path.Combine(outdir, "genome" + postfix + ".dat"), genome);
        }

        public static DenseGenome ExtractDenseGenomeFromRcsAndIst(double[][] rcs, double[][] ist, int rcsIndex, int istIndex, DenseGenome template)
        {
            int size = ist.Length;
            var dtm = Analysis.ExtractMatrix(rcs, rcsIndex, size, size);
            var initialState = Analysis.ExtractVector(ist, istIndex);
            return new DenseGenome(initialState, dtm, null, template.TransMatIndexOpenEntries, template.CustomTransMatMutator, template.CustomInitialStateMutator, template.CustomTransMatCombiner, template.CustomInitialStateCombiner, template.CustomDevelopmentStep, null, false);
        }
    }

    // this stuff is all so that we can use it for any type of individual
    public class ExperimentExtractorExperimentReceiver : IUnknownPopulationExperimentTypeReceiver<IExperimentExtractor>
    {
        public IExperimentExtractor Receive<T>(PopulationExperiment<T> populationExperiment) where T : IIndividual<T>
        {
            return new ExperimentExtractor<T>(populationExperiment);
        }

        /// <summary>
        /// Loads an IExperimentExtractor from the file, which provides helper methods which don't require knowledge of the individual type
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static IExperimentExtractor LoadExtractorFromFile(string fileName)
        {
            var expExtractor = PopulationExperimentHelpers.LoadUnknownType(fileName, new ExperimentExtractorExperimentReceiver());
            return expExtractor;
        }
    }
    
    public interface IExperimentExtractor
    {
        /// <summary>
        /// Retrieves the ExperimentConfiguration for the experiment
        /// </summary>
        ExperimentConfiguration ExperimentConfiguration { get; }

        /// <summary>
        /// Extracts an experiment from the WholeSamples
        /// </summary>
        /// <param name="sourceFile">wholesamples source file</param>
        /// <param name="epochs">The epochs to extract</param>
        /// <param name="firstOfEpoch">whether to take the first entry for the given epoch, or the last</param>
        /// <param name="postfix">a postfix? I guess</param>
        /// <param name="outdir">experiment output directory</param>
        /// <param name="zeroStart">whether to zero the start epochs and generations</param>
        void ExtractFromWholeSamples(string sourceFile, IEnumerable<int> epochs, bool firstOfEpoch, string postfix, string outdir, bool zeroStart, bool appendEpoch);
        
        /// <summary>
        /// Extracts an experiment from the TraceInfo
        /// </summary>
        /// <param name="sourceFile">wholesamples source file</param>
        /// <param name="generations">The generations to extract</param>
        /// <param name="firstOfEpoch">whether to take the first entry for the given epoch, or the last</param>
        /// <param name="postfix">a postfix? I guess</param>
        /// <param name="outdir">experiment output directory</param>
        /// <param name="zeroStart">whether to zero the start epochs and generations</param>
        void ExtractFromTraceInfo(string sourceFile, IEnumerable<int> generations, bool firstOfEpoch, string postfix, string outdir, bool zeroStart, bool appendGeneration);
        
        /// <summary>
        /// Projects a WholeSamples
        /// </summary>
        /// <param name="sourceFile">wholesamples source file</param>
        /// <param name="start">start epoch</param>
        /// <param name="end">end epoch</param
        /// <param name="outdir">experiment output directory</param>
        /// <param name="projectorPreparer">The projector preparer</param>
        void ProjectFromWholeSamples(string sourceFile, int? start, int? end, string outdir, IExtractorWholeSampleProjectorPreparer projectorPreparer);
        
        /// <summary>
        /// Extracts genomes from the experiment
        /// </summary>
        /// <param name="postfix">a postfix? I guess</param>
        /// <param name="outdir">experiment output directory</param>
        void ExtractGenomesFromWithin(string prefix, string outdir);

        /// <summary>
        /// Writes out a config.txt for this experiment
        /// </summary>
        /// <param name="outFilename">Where to save the config file</param>
        /// <param name="seed">Seed used for random sampling</param>
        void WriteOutConfig(string outFilename, int seed = 1);
    }

    /// <summary>
    /// The projection mode
    /// </summary>
    public enum ProjectionMode
    {
        Spin = 0,
        EvalOnly = 1,
    }

    /// <summary>
    /// Extracts experiments from things, basing them on a given experiment definition
    /// (Individual Agnostic)
    /// </summary>
    /// <typeparam name="T">Individual Type</typeparam>
    public class ExperimentExtractor<T> : IExperimentExtractor where T : IIndividual<T>
    {
        public ExperimentExtractor(PopulationExperiment<T> populationExperiment)
        {
            PopulationExperiment = populationExperiment;
        }

        public PopulationExperiment<T> PopulationExperiment { get; }
        public ExperimentConfiguration ExperimentConfiguration => PopulationExperiment.PopulationConfig.ExperimentConfiguration;

        public void ExtractFromWholeSamples(string sourceFile, IEnumerable<int> epochs, bool firstOfEpoch, string postfix, string outdir, bool zeroStart, bool appendEpoch)
        {
            var wholeSamples = WholeSample<T>.LoadWholeSamples(sourceFile);
            foreach (var epoch in epochs)
            {
                var wholeSample = firstOfEpoch
                    ? wholeSamples.First(ws => ws.Epoch == epoch)
                    : wholeSamples.Last(ws => ws.Epoch == epoch);
                ExtractFromWholeSample(wholeSample, appendEpoch ? postfix + "_" + epoch : postfix, outdir, zeroStart);
            }
        }

        public void ExtractFromTraceInfo(string sourceFile, IEnumerable<int> generations, bool firstOfEpoch, string postfix, string outdir, bool zeroStart, bool appendGeneration)
        {
            var wholeSamples = TraceInfo<T>.Load(sourceFile).TraceWholeSamples;
            foreach (var generation in generations)
            {
                var wholeSample = firstOfEpoch
                ? wholeSamples.First(ws => ws.Generations == generation)
                : wholeSamples.Last(ws => ws.Generations == generation);
                ExtractFromWholeSample(wholeSample, appendGeneration ? postfix + "_" + generation : postfix, outdir, zeroStart);
            }
        }

        private void ExtractFromWholeSample(WholeSample<T> wholeSample, string postfix, string outdir, bool zeroStart)
        {
            int epoch = zeroStart ? 0 : wholeSample.Epoch;
            long generationCount = zeroStart ? 0L : wholeSample.Generations;
            var population = new Population<T>(wholeSample.Judgements.Select(ij => ij.Individual));

            ExtractFromPopulation(epoch, generationCount, population, postfix, outdir);
        }

        private void ExtractFromPopulation(int epoch, long generationCount, Population<T> population, string postfix, string outdir)
        {
            // popConfig
            var popConfig = PopulationExperiment.PopulationConfig;

            // population experiment
            var fileStuff = FileStuff.CreateNow(outdir, "", "", true, false);
            var popExp = new PopulationExperiment<T>(population, popConfig, fileStuff, 0, generationCount);
            popExp.Save(postfix, false);
        }
        
        public void ProjectFromWholeSamples(string sourceFile, int? start, int? end, string outFile, IExtractorWholeSampleProjectorPreparer projectorPreparer)
        {
            List<WholeSample<T>> wholeSamples;
            if (sourceFile.StartsWith("tracee"))
            {
                wholeSamples = TraceInfo<T>.Load(sourceFile).TraceWholeSamples;
            }
            else
            {
                wholeSamples = WholeSample<T>.EnumerateWholeSamplesSeries2(sourceFile.Split(';')).Where(ws => (start == null || ws.Epoch >= start) && (end == null || ws.Epoch <= end)).ToList();
            }

            var projector = projectorPreparer.PrepareProjector<T>(PopulationExperiment);
            var rand = new MathNet.Numerics.Random.MersenneTwister();
            var context = new ModelExecutionContext(rand);

            List<WholeSample<T>> projected = WholeSampleProjectionHelpers.Project(null, context, wholeSamples, projector, true, false);
            WholeSample<T>.SaveWholeSamples(outFile, projected);
        }

        public void ExtractGenomesFromWithin(string prefix, string outdir)
        {
            var population = PopulationExperiment.Population;
            
            FileStuff stuff = FileStuff.CreateNow(outdir, prefix, prefix, true, false);
            using (var fs = stuff.CreateBin("initialpopulation.dat"))
            {
                M4M.State.GraphSerialisation.Write(population, fs);
            }
            
            int j = 0;
            foreach (var i in population.PeekAll())
            {
                if (i is object o && o is DenseIndividual di)
                {
                    Analysis.SaveGenome(stuff.File("genome" + j + ".dat"), di.Genome);
                }
                else
                {
                    throw new Exception("Cannot save genome of individual of type " + i.GetType().FullName);
                }

                j++;
            }
        }

        public void WriteOutConfig(string outFilename, int seed = 1)
        {
            using (var cw = new System.IO.StreamWriter(outFilename))
            {
                PopulationExperiment.WriteOutConfig(cw, new CustomMersenneTwister(seed));
            }
        }
    }

    public static class WholeSampleProjectionHelpers
    {
        public static List<WholeSample<T>> Project<T>(TextWriter console, ModelExecutionContext context, List<WholeSample<T>> wholeSamples, IWholeSampleProjector<T> projector, bool replaceTarget, bool printProgress) where T : IIndividual<T>
        {
            var sw = new System.Diagnostics.Stopwatch();
            var projected = new List<WholeSample<T>>();

            for (int i = 0; i < wholeSamples.Count; i++)
            {
                var ws = wholeSamples[i];

                projected.Add(projector.Project(context, ws, true));
                if (printProgress && sw.ElapsedMilliseconds > 5000) // keep the user informed
                {
                    console.WriteLine($"{i} / {wholeSamples.Count} = {((double)i / wholeSamples.Count) * 100:0.00}%");
                    sw.Restart();
                }
            }

            return projected;
        }
    }

    public interface IExtractorWholeSampleProjectorPreparer
    {
        IWholeSampleProjector<T> PrepareProjector<T>(PopulationExperiment<T> evalExp) where T : IIndividual<T>;
    }

    public class BasicExtractorWholeSampleProjectorPreparer : IExtractorWholeSampleProjectorPreparer
    {
        public BasicExtractorWholeSampleProjectorPreparer(ProjectionMode projectionMode, bool noNextExposure)
        {
            ProjectionMode = projectionMode; // TODO: validate here
            NoNextExposure = noNextExposure;
        }

        public ProjectionMode ProjectionMode { get; }
        public bool NoNextExposure { get; }

        public IWholeSampleProjector<T> PrepareProjector<T>(PopulationExperiment<T> evalExp) where T : IIndividual<T>
        {
            var popConfig = evalExp.PopulationConfig;
            var config = popConfig.ExperimentConfiguration;

            var populationSpinner = popConfig.CustomPopulationSpinner ?? DefaultPopulationSpinner<T>.Instance;

            var rrules = config.ReproductionRules;
            var drules = config.DevelopmentRules;
            var jrules = config.JudgementRules;

            var selectorPreparer = popConfig.SelectorPreparer;
            var generations = config.GenerationsPerTargetPerEpoch;

            WholeSample<T> project(ModelExecutionContext context, WholeSample<T> ws, bool replaceTarget)
            {
                var rand = context.Rand;
                var pop = new Population<T>(ws.Judgements.Select(ij => ij.Individual));

                if (ProjectionMode == ProjectionMode.Spin)
                {
                    IReadOnlyList<IndividualJudgement<T>> judgements = null;

                    ITarget lastTarget = ws.Target;
                    // TODO: this should probably be using a target cycler
                    foreach (var target in config.Targets)
                    {
                        if (!NoNextExposure)
                        {
                            ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                            target.NextExposure(rand, jrules, ws.Epoch, ref exposureInformation);
                        }

                        judgements = populationSpinner.SpinPopulation(pop, context, rrules, drules, jrules, target, selectorPreparer, generations, null, popConfig.EliteCount, popConfig.HillclimberMode);

                        lastTarget = target;
                    }

                    return new WholeSample<T>(ws.Generations, ws.Epoch, replaceTarget ? lastTarget : ws.Target, judgements);
                }
                else if (ProjectionMode == ProjectionMode.EvalOnly)
                {
                    var target = config.Targets.Last();
                    target.NextGeneration(rand, jrules);
                    var judgements = pop.ExtractAndJudgeAll(jrules, target);
                    return new WholeSample<T>(ws.Generations, ws.Epoch, replaceTarget ? target : ws.Target, judgements);
                }
                else
                {
                    throw new ArgumentException("Invalid projection mode: " + ProjectionMode.ToString());
                }
            }

            return new LambdaWholesampleProjection<T>(project);
        }
    }

    public interface IWholeSampleProjector<T> where T : IIndividual<T>
    {
        WholeSample<T> Project(ModelExecutionContext context, WholeSample<T> wholeSample, bool replaceTarget);
    }

    public class LambdaWholesampleProjection<T> : IWholeSampleProjector<T> where T : IIndividual<T>
    {
        public LambdaWholesampleProjection(Func<ModelExecutionContext, WholeSample<T>, bool, WholeSample<T>> projector)
        {
            Projector = projector;
        }

        private Func<ModelExecutionContext, WholeSample<T>, bool, WholeSample<T>> Projector { get; }

        public WholeSample<T> Project(ModelExecutionContext context, WholeSample<T> wholeSample, bool replaceTarget)
        {
            return Projector(context, wholeSample, replaceTarget);
        }
    }
}
