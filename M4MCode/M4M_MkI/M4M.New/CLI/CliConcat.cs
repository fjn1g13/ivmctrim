﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M
{
    public class CliConcat : ICliProvider
    {
        public string Key => "concat";

        public void Run(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Concat");
                console.WriteLine(" - Files should be separated by semi-colons");
                console.WriteLine(" - Indicate output file with out=path/filename");
                console.WriteLine(" - You must qualify the type of files being processed and how to process them with the mode parameters:");
                console.WriteLine("   - mode=traj -> concatenate trajectories (e.g. rcs, ist, pst, fitness)");
                console.WriteLine("   - mode=ws -> concatenate wholesamples");
                console.WriteLine(" - You can set sampleperiod where it is unknown, but if any known sample periods do not match the process will fail");
                console.WriteLine(" - Set trimEnds flag if you want to trim the last entry in trajectories (except for the last one) so that they don't overlap");
            }

            string[] files = clips.Get("files", s => s.Split(';'));
            string ofname = clips.Get("out");
            string mode = clips.Get("mode");

            if (mode.StartsWith("traj"))
            {
                int trueSamplePeriod = clips.Get("sampleperiod", int.Parse, -1);
                bool trimEnds = clips.IsSet("trimEnds");

                double[][][] trajectoriesies = new double[files.Length][][];

                int totalLength = 0;
                int fileSamplePeriod = -1;
                for (int i = 0; i < files.Length; i++)
                {
                    trajectoriesies[i] = Analysis.LoadTrajectories(files[i], out fileSamplePeriod);
                    totalLength += trajectoriesies[i][0].Length;

                    if (trueSamplePeriod == -1)
                        trueSamplePeriod = fileSamplePeriod;
                    else
                    {
                        if (fileSamplePeriod != -1 && fileSamplePeriod != trueSamplePeriod)
                            throw new Exception("Sample periods do not match");
                    }
                }

                if (trimEnds)
                    totalLength -= (trajectoriesies.Length - 1);

                double[][] trajectories = new double[trajectoriesies[0].Length][];

                for (int i = 0; i < trajectories.Length; i++)
                {
                    trajectories[i] = new double[totalLength];

                    int pos = 0;
                    for (int j = 0; j < trajectoriesies.Length; j++)
                    {
                        int l = trajectoriesies[j][i].Length - (trimEnds && j < trajectories.Length - 1 ? 1 : 0);
                        Array.Copy(trajectoriesies[j][i], 0, trajectories[i], pos, l);
                        pos += l;
                    }
                }

                Analysis.SaveTrajectories(ofname, trajectories, trueSamplePeriod);
            }
            else if (mode.StartsWith("wholesamples") || mode == "ws")
            {
                if (clips.IsSet("help"))
                {
                    console.WriteLine("Wholesample Concat\n" +
                     " - resequence: resequence wholesamples in order supplied\n" +
                     " - skipfirst: skip the first entry of successive files");
                }

                bool resequence = clips.IsSet("resequence");
                bool skipfirst = clips.IsSet("skipfirst");

                var wsm = WholeSamplesHelpers.LoadUnknownType<IWholeSamplesManipulator>(files[0], new WholeSamplesManipulatorReceiver());
                wsm.AppendSamples(files.Skip(1), resequence, skipfirst);
                wsm.SaveSamples(ofname);
            }

            // TODO: concat tracees
        }
    }
}
