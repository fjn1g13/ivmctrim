﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Linear = MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Random;
using MathNet.Numerics.LinearAlgebra;
using System.IO;

namespace M4M
{
    public interface IVectorExtractor<T> where T : IIndividual<T>
    {
        string Name { get; }
        string Description { get; }
        Linear.Vector<double> Extract(T individual);
    }

    public class InitialStateExtractor : IVectorExtractor<DenseIndividual>
    {
        private InitialStateExtractor()
        {
        }

        public static InitialStateExtractor Instance { get; } = new InitialStateExtractor();

        public string Name => nameof(InitialStateExtractor);
        public string Description => Name;

        public Vector<double> Extract(DenseIndividual individual)
        {
            return individual.Genome.InitialState;
        }
    }

    public class PhenotypeExtractor : IVectorExtractor<DenseIndividual>
    {
        private PhenotypeExtractor()
        {
        }

        public static PhenotypeExtractor Instance { get; } = new PhenotypeExtractor();

        public string Name => nameof(PhenotypeExtractor);
        public string Description => Name;

        public Vector<double> Extract(DenseIndividual individual)
        {
            return individual.Phenotype.Vector;
        }
    }

    public class CliHebbian : ICliProvider
    {
        public string Key => "hebbian";

        public void Run(TextWriter console, CliParams clips)
        {
            var source = clips.Get("hebbian");
            var filename = System.IO.Path.GetFileName(source);

            if (filename.StartsWith("epoch"))
            {
                var exp = PopulationExperiment<DenseIndividual>.Load(source);

                RunOnExperiment(console, clips, exp);
            }
            else if (filename.StartsWith("wholesamples"))
            {
                List<WholeSample<DenseIndividual>> wholeSamples;
                if (source.Contains(";"))
                {
                    wholeSamples = WholeSample<DenseIndividual>.EnumerateWholeSamplesSeries2(source.Split(';')).ToList();
                }
                else
                {
                    wholeSamples = WholeSample<DenseIndividual>.LoadWholeSamples(source);
                }

                TraceInfo<DenseIndividual> traceInfo = new TraceInfo<DenseIndividual>(wholeSamples.ToList());
                RunOnTracee(console, clips, traceInfo, true);
            }
            else if (filename.StartsWith("tracee"))
            {
                var traceInfo = TraceInfo<DenseIndividual>.Load(source);
                RunOnTracee(console, clips, traceInfo, false);
            }
            else
            {
                throw new Exception("Hebbian must occur on an experiment file, wholesamples, ist, or pst");
            }
        }

        public void RunOnExperiment(TextWriter console, CliParams clips, PopulationExperiment<DenseIndividual> exp)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Hebbian (epoch) Options:\n" +
                    " - seed\n" +
                    " - generations\n" +
                    " - extractor\n" +
                    " - initialReset\n" +
                    " - fitnesspowerfactor\n");
            }

            int seed = clips.Get("seed", int.Parse, 1);
            var rand = new CustomMersenneTwister(seed);
            var context = new ModelExecutionContext(rand);
            var generationsOverride = clips.Get("generations", s => (int?)Misc.ParseInt(s), null);

            var extractor = clips.Get("extractor", ParseDenseExtractor, InitialStateExtractor.Instance);

            var fitnessPowerFactor = clips.GetOrCreate("fitnesspowerfactor", double.Parse, () => clips.IsSet("weighted") ? 1.0 : 0.0);
            var initialReset = clips.GetOrCreate("initialReset", ExperimentParsing.ParsePopulationResetOperation,
                () => clips.Get("randomVector", s => new RandomVectorProviderPopulationResetOperation(CliBestiary.ParseVectorProvider(s)), null));

            var samples = GenerateSamples(console, exp, clips, rand, true, initialReset, generationsOverride);
            var mean = MeanHebbian(samples, extractor, fitnessPowerFactor);
            WriteOutHebbian(console, clips, exp.Population.PeekAll()[0].Genome, context, mean);
        }

        public void RunOnTracee(TextWriter console, CliParams clips, TraceInfo<DenseIndividual> traceInfo, bool epochsx)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Hebbian (wholesamples) Options:\n" +
                    " - seed\n" +
                    " - start\n" +
                    " - end\n" +
                    " - extractor\n" +
                    " - initialReset\n" +
                    " - fitnesspowerfactor\n");
            }

            if (epochsx)
            {
                int start = clips.Get("start", Misc.ParseInt, traceInfo.TraceWholeSamples.First().Epoch);
                int end = clips.Get("end", Misc.ParseInt, traceInfo.TraceWholeSamples.Last().Epoch);

                traceInfo = traceInfo.ExtractEpochs(start, end);
            }
            else
            {
                long start = clips.Get("start", Misc.ParseLong, traceInfo.TraceWholeSamples.First().Generations);
                long end = clips.Get("end", Misc.ParseLong, traceInfo.TraceWholeSamples.Last().Generations);

                traceInfo = traceInfo.ExtractGenerations(start, end);
            }

            int seed = clips.Get("seed", int.Parse, 1);
            var rand = new CustomMersenneTwister(seed);
            var context = new ModelExecutionContext(rand);

            var extractor = clips.Get("extractor", ParseDenseExtractor, InitialStateExtractor.Instance);

            var fitnessPowerFactor = clips.GetOrCreate("fitnesspowerfactor", double.Parse, () => clips.IsSet("weighted") ? 1.0 : 0.0);
            var initialReset = clips.GetOrCreate("initialReset", ExperimentParsing.ParsePopulationResetOperation,
                () => clips.Get("randomVector", s => new RandomVectorProviderPopulationResetOperation(CliBestiary.ParseVectorProvider(s)), null));

            var samples = traceInfo.TraceWholeSamples.SelectMany(ws => ws.Judgements).ToList();
            var mean = MeanHebbian(samples, extractor, fitnessPowerFactor);
            WriteOutHebbian(console, clips, samples[0].Individual.Genome, context, mean);
        }

        public static Matrix<double> MeanHebbian<T>(IReadOnlyList<IndividualJudgement<T>> samples, IVectorExtractor<T> extractor, double fitnessPowerFactor) where T : IIndividual<T>
        {
            Matrix<double> total = null;
            var fsum = 0.0;
            foreach (var sample in samples)
            {
                var v = extractor.Extract(sample.Individual).Clone();

                if (fitnessPowerFactor != 0)
                {
                    var ffac = Math.Pow(sample.Judgement.CombinedFitness, fitnessPowerFactor);
                    fsum += ffac;
                    v.Multiply(ffac, v);
                }
                else
                {
                    fsum++;
                }

                if (total == null)
                    total = CreateMatrix.Dense<double>(v.Count, v.Count);

                total.Add(v.OuterProduct(v), total);
            }

            if (total == null)
                throw new ArgumentException("No samples");

            return total / fsum;
        }

        public static void WriteOutHebbian(TextWriter console, CliParams clips, DenseGenome template, ModelExecutionContext context, Matrix<double> mean)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Hebbian write-out options:\n" +
                    " - title\n" +
                    " - out\n" +
                    " - outdir");
            }

            var postfix = clips.Get("postfix", "");
            var title = clips.Get("title", "HebbianMean");
            var outFile = clips.Get("out", title) + postfix;
            var outDir = clips.Get("outdir", "");

            if (!string.IsNullOrEmpty(outDir))
                Misc.EnsureDirectory(outDir);

            var plot = GenomePlotting.PlotDtm(title, mean, mean.Enumerate().Min(), mean.Enumerate().Max());
            SimplePdfPlotExporter.ExportToPdf(plot, Path.Combine(outDir, outFile + ".pdf"), 500, 500, true, true, null);

            var exportGenome = template.Clone(context, newTransMat: mean);
            Analysis.SaveGenome(Path.Combine(outDir, "genome_" + outFile + ".dat"), exportGenome);
        }

        public static List<IndividualJudgement<TIndividual>> GenerateSamples<TIndividual>(TextWriter console, PopulationExperiment<TIndividual> exp, CliParams clips, RandomSource rand, bool printProgress, IPopulationResetOperation<TIndividual> initialReset, int? generationsOverride) where TIndividual : IIndividual<TIndividual>
        {
            var popConfig = exp.PopulationConfig;
            var config = popConfig.ExperimentConfiguration;
            var startEpoch = clips.Get("startEpoch", int.Parse, exp.Epoch);
            var epochs = clips.Get("epochs", int.Parse, 1);

            var rrules = config.ReproductionRules;
            var drules = config.DevelopmentRules;
            var jrules = config.JudgementRules;

            int[] targetIndexes = clips.GetOrCreate("targets", s => s.Replace(" ", "").Split(new[] { ',', ';' }).Select(int.Parse).ToArray(), () => new int[] { clips.Get("target", int.Parse, 0) });
            var count = clips.Get("count", int.Parse, 1000);

            var templatePop = exp.Population;

            var context = new ModelExecutionContext(rand);
            var populationSpinner = popConfig.CustomPopulationSpinner ?? DefaultPopulationSpinner<TIndividual>.Instance;

            var resultantIndividuals = new List<IndividualJudgement<TIndividual>>();

            var sw = printProgress ? new System.Diagnostics.Stopwatch() : null;
            sw?.Start();

            for (int i = 0; i < count; i++)
            {
                // new population
                var pop = templatePop.Clone(context);

                // remember the last target so we can make a judgement at the end
                ITarget currentTarget = config.Targets[targetIndexes[0]];

                // reset if necessary
                if (initialReset != null)
                {
                    initialReset.Reset(context, pop, config.InitialStateResetRange, drules, rrules, currentTarget);
                }

                for (int epoch = startEpoch; epoch < startEpoch + epochs; epoch++)
                    foreach (var targetIndex in targetIndexes)
                    {
                        currentTarget = config.Targets[targetIndex];

                        ExposureInformation exposureInformation = new ExposureInformation(generationsOverride ?? popConfig.ExperimentConfiguration.GenerationsPerTargetPerEpoch);
                        currentTarget.NextExposure(context.Rand, jrules, epoch, ref exposureInformation);

                        // reset if we should
                        if (context.Rand.NextDouble() < config.InitialStateResetProbability)
                            popConfig.PopulationResetOperation?.Reset(context, pop, config.InitialStateResetRange, drules, rrules, currentTarget);

                        if (exposureInformation.ExposureDuration <= 0)
                            continue;

                        // mutate, judge, select cycle
                        populationSpinner.SpinPopulation(pop, context, rrules, drules, jrules, currentTarget, popConfig.SelectorPreparer, exposureInformation.ExposureDuration, null, popConfig.EliteCount, popConfig.HillclimberMode);
                    }

                // accumulate products
                currentTarget.NextGeneration(rand, jrules);
                resultantIndividuals.AddRange(pop.ExtractAndJudgeAll(jrules, currentTarget));

                if (printProgress && sw.ElapsedMilliseconds > 30000)
                {
                    console.WriteLine($"{i}/{count}");
                    sw.Restart();
                }
            }

            return resultantIndividuals;
        }

        public static IVectorExtractor<DenseIndividual> ParseDenseExtractor(string str)
        {
            if (str.Equals("g", StringComparison.InvariantCultureIgnoreCase))
            {
                return InitialStateExtractor.Instance;
            }
            else if (str.Equals("p", StringComparison.InvariantCultureIgnoreCase))
            {
                return PhenotypeExtractor.Instance;
            }
            else
            {
                throw new ArgumentException("Unrecognised dense extractor: " + str + "\nTry one of g or p.");
            }
        }
    }
}
