﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Linear = MathNet.Numerics.LinearAlgebra;
using System.IO;

namespace M4M
{
    public class CliSwitchingModules : ICliProvider
    {
        public string Key => "switchingmodules";

        public void Run(TextWriter console, CliParams clips)
        {
            var outdir = clips.Get("outdir");
            bool appendTimestamp = clips.Get("appendTimestamp", bool.Parse, true);

            var rand = CliPlotHelpers.ParseRand(clips);
            var context = new ModelExecutionContext(rand);

            var exp = clips.Get("expfile", PopulationExperiment<DenseIndividual>.Load);
            var popConfig = exp.PopulationConfig;
            var config = popConfig.ExperimentConfiguration;
            var target = CliPlotHelpers.ParseTarget(console, clips, config.Targets, config.Targets[0]);
            CliPlotHelpers.PrepTarget(rand, clips, config, target, true, 1);

            bool saturateInitialStates = clips.Get("saturateis", bool.Parse, true);
            double saturationMin = clips.Get("satmin", double.Parse, -1);
            double saturationThreshold = clips.Get("satthreshold", double.Parse, 0);
            double saturationMax = clips.Get("satmax", double.Parse, +1);

            var spinner = exp.PopulationConfig.CustomPopulationSpinner ?? DefaultPopulationSpinner<DenseIndividual>.Instance;

            int count = clips.Get("count", int.Parse, 1000);

            var counts = new Dictionary<Linear.Vector<double>, int>();
            var stuff = FileStuff.CreateNow(outdir, "", "", true, appendTimestamp);

            var sw = new System.Diagnostics.Stopwatch();
            sw.Restart();

            // simulate lots of epochs
            for (int i = 0; i < count; i++)
            {
                if (sw.ElapsedMilliseconds > 30000)
                {
                    console.WriteLine($"{i}/{count}");
                    sw.Restart();
                }

                var pop = exp.Population.Clone(context);

                popConfig.PopulationResetOperation.Reset(context, pop, config.InitialStateResetRange, config.DevelopmentRules, config.ReproductionRules, target);

                var ijs = spinner.SpinPopulation(pop, context, config.ReproductionRules, config.DevelopmentRules, config.JudgementRules, target, popConfig.SelectorPreparer, config.GenerationsPerTargetPerEpoch, null, popConfig.EliteCount, popConfig.HillclimberMode);
                var result = ijs[0].Individual.Genome.InitialState;
                if (saturateInitialStates)
                    result = Misc.Saturate(result, saturationMin, saturationThreshold, saturationMax);

                if (counts.TryGetValue(result, out int c))
                {
                    counts[result] = c + 1;
                }
                else
                {
                    counts[result] = 1;
                }
            }

            var report = new StringBuilder();
            report.AppendLine($"# SwitchingModules {Misc.NowTime}");
            report.AppendLine();
            report.AppendLine($"Index\tCount");

            int idx = 0;
            var templateIndividual = exp.Population.PeekAll()[0];
            foreach (var vk in counts.OrderBy(c => c.Value))
            {
                var exportGenome = templateIndividual.Genome.Clone(context, newInitialState: vk.Key);
                var exportIndividual = DenseIndividual.Develop(exportGenome, context, config.DevelopmentRules, templateIndividual.Epigenetic);
                var exportPop = new Population<DenseIndividual>(new[] { exportIndividual });
                var exportExp = new PopulationExperiment<DenseIndividual>(exportPop, popConfig, stuff, 0, 0);
                exportExp.Save($"u{idx}", false);

                report.AppendLine($"{idx}\t{vk.Value}");

                idx++;
            }

            System.IO.File.WriteAllText(stuff.File("report.txt"), report.ToString());
        }
    }
}
