﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public class SaturationInfo
    {
        public SaturationInfo(double saturationPoint)
        {
            SaturationPoint = saturationPoint;
        }

        public double SaturationPoint { get; }

        public static SaturationInfo Measure(ModelExecutionContext context, DevelopmentRules drules, double c, double lambda, int moduleSize)
        {
            var g = Linear.CreateVector.Dense<double>(moduleSize, 1.0);
            var zero = Linear.CreateMatrix.Dense<double>(moduleSize, moduleSize, 0.0);
            var b = Linear.CreateMatrix.Dense<double>(moduleSize, moduleSize, 0.0);
            var p = new Phenotype(g.Clone());
            var genome = new DenseGenome(g, b);

            double fitness(double w)
            {
                zero.Add(w, b);
                genome.DevelopInto(p, context, drules);
                var f = p.Vector[0] * c - w * lambda / moduleSize;
                return f;
            }

            var saturationPoint = Misc.ArgMax(fitness, new Misc.Range(0, 10), 0.001);

            return new SaturationInfo(saturationPoint);
        }
    }
}
