﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.IO;

namespace M4M
{
    public interface ICli
    {
        void Run(TextWriter console, string[] args);
    }

    public class CliParams
    {
        private Dictionary<string, string> Table { get; } // don't support concurrent write
        private ConcurrentDictionary<string, object> Unobserved { get; } // do support concurrent observe

        // cloning constructor
        private CliParams(CliParams orig)
        {
            Table = new Dictionary<string, string>(orig.Table, orig.Table.Comparer);
            Unobserved = orig.Unobserved; // I think we share this... nothing else makes any sense
        }

        public CliParams(bool caseSensitive = false)
        {
            StringComparer strComparer;

            if (caseSensitive)
            {
                strComparer = StringComparer.Ordinal;
            }
            else
            {
                strComparer = StringComparer.OrdinalIgnoreCase;
            }
            
            Table = new Dictionary<string, string>(strComparer);
            Unobserved = new ConcurrentDictionary<string, object>(strComparer);
        }

        private void SetInternal(string key, string value, bool unobserved = true)
        {
            Table[key] = value;
            if (unobserved)
                Unobserved.TryAdd(key, null);
        }

        private bool TryGet(string key, out string value)
        {
            if (Table.TryGetValue(key, out value))
            {
                Unobserved.TryRemove(key, out _);
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<string> Entries => Table.Keys;

        public void ConsumeLine(string line)
        {
            Consume(StringHelpers.Split(line).ToArray());
        }

        public void ConsumeFile(string fileName)
        {
            foreach (var line in System.IO.File.ReadLines(fileName))
            {
                if (!string.IsNullOrWhiteSpace(line) && !line.StartsWith("//"))
                {
                    ConsumeLine(line);
                }
            }
        }

        public void Consume(string[] args)
        {
            bool currentIsAppend = false;
            string currentCommand = null;

            void setIntercepted(string key, string value, bool append)
            {
                if (Table.Comparer.Equals(key, "params"))
                {
                    // expand (ignore add)
                    ConsumeLine(value);
                }
                else
                {
                    if (append)
                    {
                        var old = Get(key, null) ?? ""; // treat null/assigned as empty string
                        SetInternal(key, old + value);
                    }
                    else
                    {
                        // overwrite
                        SetInternal(key, value);
                    }
                }
            }

            foreach (string arg in args)
            {
                // +/-cmd val
                if (currentCommand != null)
                {
                    setIntercepted(currentCommand, arg, currentIsAppend);
                    currentCommand = null;
                    continue;
                }

                // -cmd
                if (arg.Length > 0 && arg[0] == '-')
                {
                    currentCommand = arg.Substring(1);
                    currentIsAppend = false;
                    continue;
                }

                // +cmd
                if (arg.Length > 0 && arg[0] == '+')
                {
                    currentCommand = arg.Substring(1);
                    currentIsAppend = true;
                    continue;
                }

                // cmd=val or cmd+=val
                int sidx = arg.IndexOf('=');
                if (sidx != -1)
                {
                    if (sidx > 0 && arg[sidx - 1] == '+')
                    {
                        // cmd+=val
                        string key = arg.Substring(0, sidx - 1);
                        string val = arg.Substring(sidx + 1);

                        setIntercepted(key, val, true);
                    }
                    else
                    {
                        // cmd=value
                        string key = arg.Substring(0, sidx);
                        string val = arg.Substring(sidx + 1);

                        setIntercepted(key, val, false);
                    }

                    continue;
                }

                // cmd
                setIntercepted(arg, null, false);
            }

            if (currentCommand != null)
                throw new Exception("Unspecified argument for command " + currentCommand);
        }

        public bool IsSet(string key)
        {
            return TryGet(key, out _);
        }
        
        /// <summary>
        /// Attempts to retrieve the value for the given key
        /// Throws if the key is not present
        /// </summary>
        public string Get(string key)
        {
            if (TryGet(key, out string val))
            {
                return val;
            }
            else
            {
                throw new Exception("No argument provided for required parameter '" + key + "'");
            }
        }
        
        /// <summary>
        /// Attempts to retrieve the value for the given key
        /// Returns the default value if the key is not present
        /// </summary>
        public string Get(string key, string @default)
        {
            if (TryGet(key, out string val))
            {
                return val;
            }
            else
            {
                return @default;
            }
        }
        
        /// <summary>
        /// Attempts to retrieve the value for the given key, returning the parsed value
        /// Throws is the key is not present
        /// </summary>
        public T Get<T>(string key, Func<string, T> parser)
        {
            if (TryGet(key, out string val))
            {
                try
                {
                    return parser(val);
                }
                catch (Exception ex)
                {
                    throw new Exception("Argument for parameter '" + key + "' was of the wrong format", ex);
                }
            }
            else
            {
                throw new Exception("No argument provided for required parameter '" + key + "'");
            }
        }
        
        /// <summary>
        /// Attempts to retrieve the value for the given key, returning the parsed value
        /// If key is not found, returns the default value
        /// </summary>
        public T Get<T>(string key, Func<string, T> parser, T @default)
        {
            if (TryGet(key, out string val))
            {
                try
                {
                    return parser(val);
                }
                catch (Exception ex)
                {
                    throw new Exception("Argument for parameter '" + key + "' was of the wrong format", ex);
                }
            }
            else
            {
                return @default;
            }
        }

        /// <summary>
        /// Attempts to retrieve the value for the given key, returning the parsed value
        /// If key is not found, generates the value from the creator (only called if the key is no present)
        /// </summary>
        public string GetOrCreate(string key, Func<string> creator)
        {
            if (TryGet(key, out string val))
            {
                return val;
            }
            else
            {
                return creator();
            }
        }

        /// <summary>
        /// Attempts to retrieve the value for the given key, returning the parsed value
        /// If key is not found, generates the value from the creator (only called if the key is no present)
        /// </summary>
        public T GetOrCreate<T>(string key, Func<string, T> parser, Func<T> creator)
        {
            if (TryGet(key, out string val))
            {
                try
                {
                    return parser(val);
                }
                catch (Exception ex)
                {
                    throw new Exception("Argument for parameter '" + key + "' was of the wrong format", ex);
                }
            }
            else
            {
                return creator();
            }
        }

        /// <summary>
        /// Sets the value for the given key to null if it is not already set
        /// Returns true if the value was set (it was previously unset)
        /// Does not mark the key as unobserved
        /// </summary>
        public bool SetIfUnset(string key)
        {
            return SetIfUnset(key, null);
        }

        /// <summary>
        /// Sets the value for the given key to null
        /// Does not mark the key as unobserved
        /// </summary>
        public void Set(string key)
        {
            SetInternal(key, null);
        }

        /// <summary>
        /// Sets the value for the given key to null
        /// Does not mark the key as unobserved
        /// </summary>
        public void Set(string key, string value)
        {
            SetInternal(key, value);
        }

        /// <summary>
        /// Clears the value for the given key if it is already set
        /// Returns true if the value was cleared (it was previously set)
        /// Does not mark the key as observed
        /// </summary>
        public bool ClearIfSet(string key)
        {
            return Table.Remove(key);
        }

        /// <summary>
        /// Sets the value for the given key to null if it is not already set
        /// Returns true if the value was set (it was previously unset)
        /// Marks the key as unobserved if it is set
        /// </summary>
        public bool SetUnobservedIfUnset(string key)
        {
            return SetUnobservedIfUnset(key, null);
        }
        
        /// <summary>
        /// Sets the value for the given key to given value if it is not already set
        /// Returns true if the value was set (it was previously unset)
        /// Does not mark the key as unobserved
        /// </summary>
        public bool SetIfUnset(string key, string value)
        {
            if (!IsSet(key))
            {
                SetInternal(key, value, false);

                return true;
            }
            else
            {
                return false;
            }
        }
        
        /// <summary>
        /// Sets the value for the given key to given value if it is not already set
        /// Returns true if the value was set (it was previously unset)
        /// Marks the key as unobserved if it is set
        /// </summary>
        public bool SetUnobservedIfUnset(string key, string value)
        {
            if (!IsSet(key))
            {
                SetInternal(key, value);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Creates a new CliParams object with the same set of parameters and equality comparer
        /// </summary>
        public CliParams CloneCliParams()
        {
            return new CliParams(this);
        }

        /// <summary>
        /// Enumerates all unobserved keys
        /// </summary>
        public IEnumerable<string> EnumerateUnobsered() => Unobserved.Keys;

        private static string EscapeAndQuote(string str)
        {
            str = StringHelpers.Escape(str, '"', '`');
            if (str.IndexOfAny(StringHelpers.DefaultDelimiters.ToArray()) > 0)
            {
                str = '"' + str + '"';
            }

            return str;
        }

        public string[] ToStringArray()
        {
            return Table.Select(kv => EscapeAndQuote(kv.Key) + (kv.Value == null ? "" : "=" + EscapeAndQuote(kv.Value))).ToArray();
        }

        public string ToStringLine()
        {
            return string.Join(" ", ToStringArray());
        }

        /// <summary>
        /// Invaludes parameters from the other <see cref="CliParams"/> instance where the key is unset in this instance.
        /// </summary>
        /// <param name="other">The other instance.</param>
        public void IncludeWhereUnset(CliParams other)
        {
            foreach (var vk in other.Table)
            {
                if (!IsSet(vk.Key))
                {
                    Set(vk.Key, vk.Value);
                }
            }
        }
    }

    public interface ICliProvider
    {
        string Key { get; }
        void Run(TextWriter console, CliParams clips);
    }

    public class Cli : ICli
    {
        public static string PlatformTopDir
        {
            get
            {
                var platform = System.Environment.OSVersion.Platform;
                string topdir = platform == PlatformID.Unix ? "./M4MExperiments" : "C:/M4MExperiments"; // ik ben lui
                return topdir;
            }
        }

        public static Cli PrepareDefaultCli(IDensePopulationExperimentFactoryCliFactory feedbackFactoryCliFactory = null, string defaultTopdir = null, Dictionary<string, Action<TextWriter,string, CliParams>> genSets = null, IEnumerable<ICliProvider> extraProviders = null, IPlotExporter plotExporter = null, IEnumerable<ICliPlotter> extraPlotters = null)
        {
            var exp = new CliExp(feedbackFactoryCliFactory ?? new CommonFeedbackFactoryCliFactory(CommonFeedbackFactoryCliFactory.DefaultConfiguratorFactories));
            var run = new CliRun(exp);
            var gen = new CliGen(defaultTopdir ?? PlatformTopDir, genSets ?? CliGen.DefaultGenSets);
            var plot = new CliPlot(plotExporter ?? new SimplePdfPlotExporter(), CliPlot.CreateDefaultPlotters().Concat(extraPlotters ?? Enumerable.Empty<ICliPlotter>()), CliPlot.DefaultPresets);
            var spit = new CliSpit();
            var tracee = new CliTracee();
            var traces = new CliTraces();
            var concat = new CliConcat();
            var extract = new CliExtract();
            var project = new CliProject();
            var analyse = new CliAnalyse(CliAnalyse.CreateDefaultAnalysers());
            var hebbian = new CliHebbian();
            var reconfig = new CliReconfig(CliReconfig.DefaultPresets);
            var bestiary = new CliBestiary();
            var satgroup = new CliSatGroup();
            var multiplot = new CliMultiPlot(plot);
            var runongroup = new CliRunonGroup();
            var stackGroup = new CliStackGroup();
            var sparseswitchers = new CliSparseSwitchers();
            var switchingModules = new CliSwitchingModules();

            Cli cli = new Cli(new ICliProvider[] { exp, run, gen, plot, spit, tracee, traces, concat, extract, project, analyse, hebbian, reconfig, bestiary, satgroup, multiplot, runongroup, stackGroup, sparseswitchers, switchingModules }.Concat(extraProviders ?? Enumerable.Empty<ICliProvider>()));
            return cli;
        }

        public Cli(IEnumerable<ICliProvider> providers)
        {
            Providers = providers.ToArray();
        }

        public ICliProvider[] Providers { get; }

        public static void LoadParamFiles(CliParams clips, string referenceDirectory = ".")
        {
            var files = new HashSet<string>();
            var due = new Stack<string>();

            void acquireParamFiles(CliParams _clips, string refDir)
            {
                var paramFiles = _clips.Get("paramfile", null);
                if (paramFiles != null)
                {
                    foreach (var candidate in StringHelpers.Split(paramFiles, new CodePoint[] { ';' }, '"', '`'))
                    {
                        var fullPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(refDir, candidate));
                        if (files.Add(fullPath))
                        {
                            due.Push(fullPath);
                        }
                    }
                }
            }

            acquireParamFiles(clips, referenceDirectory);

            while (due.Count > 0)
            {
                var next = due.Pop();
                var _clips = new CliParams();
                _clips.ConsumeFile(next);
                acquireParamFiles(_clips, System.IO.Path.GetDirectoryName(next));
                clips.IncludeWhereUnset(_clips);
            }
        }

        public void Run(TextWriter console, string[] args)
        {
            CliParams clips = new CliParams();
            clips.Consume(args);
            LoadParamFiles(clips);

            foreach (var provider in Providers)
            {
                if (clips.IsSet(provider.Key))
                {
                    provider.Run(console, clips);
                    PrintUnobserved(console, clips);

                    return;
                }
            }

            console.WriteLine("Available providers:");

            foreach (var provider in Providers)
                console.WriteLine(provider.Key);
        }

        public static void PrintUnobserved(TextWriter console, CliParams clips)
        {
            bool doNotListUnobserved = clips.IsSet("quiet") || clips.IsSet("donotlistunobserved");
            if (!doNotListUnobserved)
            {
                var unobserved = clips.EnumerateUnobsered().ToArray();
                if (unobserved.Length > 0)
                    console.WriteLine("Unobserved CliParams: " + string.Join(", ", unobserved));
            }
        }
    }

    public class CliPrompt : ICli
    {
        public CliPrompt(Cli cli)
        {
            Cli = cli;
        }

        public Cli Cli { get; }

        public void Run(TextWriter console, string[] args)
        {
            CliParams clips = new CliParams();
            clips.Consume(args);
            CliParams diagClips = new CliParams();
            diagClips.Consume(args);

            bool quiet = clips.IsSet("quiet");

            if (diagClips.IsSet("diag") && !diagClips.IsSet("noprediag"))
            {
                console.WriteLine("Start Pre-Diagnostics");
                PrintDiagnostics(console, clips, diagClips);
                console.WriteLine("End Pre-Diagnostics");
            }

            try
            {
                if (clips.IsSet("MathNetUseMultiThreading"))
                {
                    MathNet.Numerics.Control.UseMultiThreading();
                }

                if (clips.IsSet("forceInvarientCulture") || clips.IsSet("fic"))
                {
                    CliBodge.ForceInvarientCulture();
                    if (!quiet)
                        console.WriteLine("Forced Invarient Culture");
                }

                if (clips.IsSet("prompt"))
                {
                    console.WriteLine("M4M Prompt, 'quit' to exit");
                    RunPrompt();
                }
                else
                {
                    bool timeMe = clips.IsSet("timeme");
                    var sw = timeMe ? new System.Diagnostics.Stopwatch() : null;

                    sw?.Restart();
                    Cli.Run(console, args);
                    sw?.Stop();

                    if (timeMe)
                    {
                        console.WriteLine(sw.ElapsedMilliseconds + "ms");
                    }
                }    
            }
            catch (Exception ex)
            {
                console.WriteLine("~~~ Exception Thrown at "  + DateTime.Now.ToString("O") + " ~~~ ");
                console.WriteLine(ex.Message);
                console.WriteLine(ex.StackTrace);

                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    console.WriteLine("Inner Exception");
                    console.WriteLine(ex.Message);
                    console.WriteLine(ex.StackTrace);
                }

                if (diagClips.IsSet("rethrow"))
                    throw;
            }
            
            if (diagClips.IsSet("diag") && !diagClips.IsSet("nopostdiag"))
            {
                console.WriteLine("Start Post-Diagnostics");
                CliParams clips2 = new CliParams();
                clips2.Consume(args);
                PrintDiagnostics(console, clips, diagClips);
                console.WriteLine("End Post-Diagnostics");
            }
        }

        public void RunPrompt()
        {
            while (true)
            {
                Console.Write("M4M> ");
                string line = Console.ReadLine();
                string[] args = line.Split(' ');

                CliParams clips = new CliParams();
                clips.Consume(args);

                bool doQuit = clips.IsSet("quit") || clips.IsSet("exit");
                if (doQuit)
                    break;

                Cli.Run(System.Console.Out, args);
            }
        }

        private static string TryGetAssemblyLocation(System.Reflection.Assembly assembly)
        {
            try
            {
                return assembly.Location;
            }
            catch (Exception ex)
            {
                return "(" + ex.Message + ")";
            }
        }

        public static void PrintDiagnostics(TextWriter console, CliParams clips, CliParams diagClips)
        {
            console.WriteLine("~~ M4M Diag ~~~");
            console.WriteLine("DateTime:\t" + DateTime.Now.ToString("O"));
            console.WriteLine("Entry Assembly Location:\t" + TryGetAssemblyLocation(System.Reflection.Assembly.GetEntryAssembly()));
            console.WriteLine("M4M.New Assembly Location:\t" + TryGetAssemblyLocation(typeof(Cli).Assembly));
            console.WriteLine("M4M.Model Assembly Location:\t" + TryGetAssemblyLocation(typeof(ExperimentConfiguration).Assembly));
            console.WriteLine("NetState Assembly Location:\t" + TryGetAssemblyLocation(typeof(NetState.AutoState.AutoGraphSerialisation).Assembly));
            console.WriteLine("MathNET Assembly Location:\t" + TryGetAssemblyLocation(typeof(MathNet.Numerics.LinearAlgebra.Matrix<double>).Assembly));
            console.WriteLine("OxyPlot Assembly Location:\t" + TryGetAssemblyLocation(typeof(OxyPlot.PlotModel).Assembly));
            console.WriteLine("OS Version:\t" + System.Environment.OSVersion);
            console.WriteLine("Runtime Version:\t" + System.Environment.Version);
            console.WriteLine("Culture:\t" + System.Threading.Thread.CurrentThread.CurrentCulture.DisplayName);
            console.WriteLine("UI Culture:\t" + System.Threading.Thread.CurrentThread.CurrentUICulture.DisplayName);
            console.WriteLine("NetState DisallowAssemblyLoading:\t" + NetState.AutoState.AutoSerialisationHelpers.DisallowAssemblyLoading);
            console.WriteLine("NetState EnableAllowLoadWhiteList:\t" + NetState.AutoState.AutoSerialisationHelpers.EnableAllowLoadWhiteList);
            console.WriteLine("NetState AllowLoadWhiteList:\t" + string.Join(", ", NetState.AutoState.AutoSerialisationHelpers.AllowLoadWhiteList));
            console.WriteLine("");

            if (diagClips.Get("help", bool.Parse, true))
            {
                console.WriteLine("# Diagnostics Options");
                console.WriteLine(" - stacktrace: prints a stacktrace, so the calling code is apparent");
                console.WriteLine(" - clips: prints the CliParams");
                console.WriteLine(" - assemblies: prints information for loaded assemblies");
                console.WriteLine("   - types: prints type information for loaded assemblies");
                console.WriteLine("");
            }

            if (diagClips.IsSet("stacktrace"))
            {
                console.WriteLine("# StackTrace");
                console.WriteLine(System.Environment.StackTrace);
                console.WriteLine("");
            }

            if (diagClips.IsSet("clips"))
            {
                console.WriteLine("# CliParams");
                foreach (var k in clips.Entries)
                {
                    var v = clips.Get(k);
                    if (v == null)
                    {
                        console.WriteLine($"{k}\t(null)");
                    }
                    else
                    {
                        console.WriteLine($"{k}\t'{v}'");
                    }
                }
                console.WriteLine("");
            }

            if (diagClips.IsSet("assemblies"))
            {
                bool printTypes = diagClips.IsSet("types");

                console.WriteLine("# Assemblies");
                foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    console.WriteLine(assembly.GetName().Name);
                    console.WriteLine(" - FullName:\t" + assembly.GetName().FullName);
                    console.WriteLine(" - Version:\t" + assembly.GetName().Version);
                    console.WriteLine(" - Location:\t" + assembly.Location);
                    if (ManifestResources.TryReadResourceString(assembly, "Info.version.txt", out var infoVersionString))
                        console.WriteLine(" - Info.Version:\t" + infoVersionString);
                    if (ManifestResources.TryReadResourceString(assembly, "Info.buildinfo.txt", out var infoBuildInfoString))
                        console.WriteLine(" - Info.BuildInfo:\n     " + infoBuildInfoString.Replace("\n", "\n     "));

                    if (printTypes)
                    {
                        foreach (var module in assembly.GetLoadedModules())
                        {
                            console.WriteLine(" - Module " + module.Name);

                            foreach (var type in module.GetTypes())
                            {
                                console.WriteLine("   - " + type.FullName);
                            }
                        }
                    }
                }
            }
        }
    }

    public class ManifestResources
    {
        public static System.Text.Encoding Enc { get; } =  new System.Text.UTF8Encoding(false);

        public static string ReadResourceString(System.Reflection.Assembly assembly, string name)
        {
            var qualifiedName = assembly.GetName().Name + "." + name;
            var resourceStream = assembly.GetManifestResourceStream(qualifiedName); 
            using (var reader = new System.IO.StreamReader(resourceStream, Enc)) 
            { 
                return reader.ReadToEnd(); 
            }
        }

        public static bool TryReadResourceString(System.Reflection.Assembly assembly, string name, out string contents)
        {
            try
            {
                var qualifiedName = assembly.GetName().Name + "." + name;
                var resourceStream = assembly.GetManifestResourceStream(qualifiedName);
                using (var reader = new System.IO.StreamReader(resourceStream, Enc))
                {
                    contents = reader.ReadToEnd();
                }
                return true;
            }
            catch
            {
                contents = null;
                return false;
            }
        }
    }

    public static class CliBodge
    {
        public static void ForceInvarientCulture()
        {
            // we do this, so that numerical formatting is always US style, and we can keep using commas to separate things (because I'm lazy)
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.InvariantCulture;
        }
    }
}
