using M4M;
using M4M.ExperimentGroups;
using M4M.Modular;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace M4M
{
    public class CliGroupTraces : ICliPlotter
    {
        public string Prefix => "GroupTraces";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            var dir = filename;
            var groupName = clips.Get("GroupName");
            var wholesampleFilename = clips.Get("WholesampleFilename");
            var trajectoryFileName = clips.Get("TrajectoryFileName");
            var blockTerminator = clips.Get("blockTerminator", "runs");

            var dirs = dir.Split(';');
            var groupNames = groupName.Split(';');

            if (wholesampleFilename != null)
            {
                return PlotGroupTracees(dirs, groupNames, wholesampleFilename, blockTerminator);
            }
            else if (trajectoryFileName != null)
            {
                var trajectoryIndex = clips.Get("trajectoryIndex", int.Parse);
                return PlotGroupsTracees(dirs, groupNames, trajectoryFileName, trajectoryIndex, blockTerminator);
            }
            else
            {
                console.WriteLine("Must qualify either the WholesampleFilename or the TrajectoryFileName.");
                return null;
            }
        }

        public static PlotModel PlotGroupTracees(IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string wholesampleFilename, string blockTerminator)
        {
            var expSamples = GroupPlotHelpers.EnumerateManyExpInfos(dirs, groupNames, wholesampleFilename, blockTerminator).Select(e => ExpWholeSamples<DenseIndividual>.Sample(e, wholesampleFilename, ""));

            return GroupPlotting.PlotBlockTracees(expSamples, ws => ws.Judgements[0].Judgement.CombinedFitness);
        }

        public static PlotModel PlotGroupsTracees(IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string trajectoryFilename, int trajectoryIndex, string blockTerminator)
        {
            int sampleRate = -1;
            var expSamples = GroupPlotHelpers.EnumerateManyExpInfos(dirs, groupNames, trajectoryFilename, blockTerminator).Select(e => ExpSamplers.SampleTrajectory(e, trajectoryFilename, trajectoryIndex, ""+trajectoryIndex, out sampleRate));

            return GroupPlotting.PlotBockTracees(expSamples, x => x, sampleRate);
        }
    }

    public class CliGroupBox : ICliPlotter
    {
        public string Prefix => "GroupBox";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            var dir = filename;
            var groupName = clips.Get("GroupName");
            var runonExpFile = clips.Get("RunonExpFile");
            var blockTitle = clips.Get("BlockTitle");
            var wholesampleFilename = clips.Get("WholesampleFilename");
            var blockTerminator = clips.Get("blockTerminator", "runs");

            var dirs = dir.Split(';');
            var groupNames = groupName.Split(';');

            double[] thresholds = clips.Get("threshold", s => s.Split(';').Select(double.Parse).ToArray(), new double[0]);

            var selector = clips.Get("selector", s => GroupPlotHelpers.ParseSelector<DenseIndividual>(s));

            return PlotGroupBox(dirs, groupNames, blockTitle, wholesampleFilename, selector, blockTerminator);
        }

        public static PlotModel PlotGroupBox<TIndividual>(IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string blockTitle, string wholesampleFilename, Func<IEnumerable<WholeSample<TIndividual>>, double> selector, string blockTerminator) where TIndividual : IIndividual<TIndividual>
        {
            var expSamples = GroupPlotHelpers.EnumerateManyExpInfos(dirs, groupNames, wholesampleFilename, blockTerminator).Select(e => ExpWholeSamples<TIndividual>.Sample(e, wholesampleFilename, ""));
            return GroupPlotting.PlotBox(expSamples, "Mean Terminal Fitness Distributions", blockTitle, "Mean Fitness", null, selector);
        }
    }

    public class CliGroupMeanRowSum : ICliPlotter
    {
        public string Prefix => "GroupMeanRowSum";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            var expFilename = clips.Get("ExpFilename");
            var sampler = new RowSumExpSamples(expFilename);
            var aliases = GroupPlotHelpers.Aliases(clips);
            var expSamples = GroupPlotHelpers.GroupSampling(clips, expFilename, sampler);
            return GroupPlotting.PlotAverage(expSamples, title ?? "Mean row-sum", "Group", "Weight", aliases, ss => ss.Average());
        }
    }

    public class CliGroupPostWinFreq : ICliPlotter
    {
        public string Prefix => "GroupPostWinFreq";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            var expFilename = clips.Get("ExpFilename");
            var count = clips.Get("count", int.Parse, 10);
            var sampler = new RunonWinFreqExpSampler(count, expFilename);
            var aliases = GroupPlotHelpers.Aliases(clips);
            var expSamples = GroupPlotHelpers.GroupSampling(clips, expFilename, sampler);
            return GroupPlotting.PlotAverage(expSamples, title ?? "Mean frequency of most fit phenotype", "Group", "Frequency", aliases, ss => ss.Average(b => b ? 1 : 0));
        }
    }

    public class CliGroupTimeToHierarchy : ICliPlotter
    {
        public string Prefix => "GroupTimeToHierarchy";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            var rcsFilename = clips.Get("RcsFilename");
            var modules = clips.Get("modules", Modular.MultiModulesStringParser.Instance.Parse);
            var threshold = clips.Get("huskythreshold", double.Parse, 0.9);
            var sampler = new TimeToHierarchyExpSampler(modules, rcsFilename, threshold);
            var aliases = GroupPlotHelpers.Aliases(clips);
            var expSamples = GroupPlotHelpers.GroupSampling(clips, rcsFilename, sampler);

            if (clips.IsSet("boxplot"))
            {
                return GroupPlotting.PlotBox(expSamples, title ?? $"Mean time-to-hierarchy (threshold = {threshold})", "Group", "Epochs", aliases, ss => ss.Average(b => b.HasValue ? (double)b.Value : double.NaN));
            }
            else
            {
                return GroupPlotting.PlotAverage(expSamples, title ?? $"Time-to-hierarchy (threshold = {threshold})", "Group", "Epochs", aliases, ss => ss.Average(b => b.HasValue ? (double)b.Value : double.NaN));
            }
        }
    }

    public interface IExpSampler<T>
    {
        ExpSamples<T> Sample(ModelExecutionContext ctx, ExpInfo xpInfo);
    }

    public class RowSumExpSamples : IExpSampler<double>
    {
        public RowSumExpSamples(string expFileName)
        {
            ExpFileName = expFileName ?? throw new ArgumentNullException(nameof(expFileName));
        }

        public string ExpFileName { get; }

        public ExpSamples<double> Sample(ModelExecutionContext ctx, ExpInfo expInfo)
        {
            var expFile = System.IO.Path.Combine(expInfo.Dir, ExpFileName);
            Console.WriteLine("Exp " + expFile);

            var exp = PopulationExperiment<DenseIndividual>.Load(expFile);
            var samples = exp.Population.ExtractAll().SelectMany(i => i.Genome.TransMat.RowSums()).ToList();

            return new ExpSamples<double>(expInfo, samples, $"");
        }
    }

    public class RunonWinFreqExpSampler : IExpSampler<bool>
    {
        public RunonWinFreqExpSampler(int runonCount, string expFileName)
        {
            RunonCount = runonCount;
            ExpFileName = expFileName ?? throw new ArgumentNullException(nameof(expFileName));
        }

        public int RunonCount { get; }
        public string ExpFileName { get; }

        public ExpSamples<bool> Sample(ModelExecutionContext ctx, ExpInfo expInfo)
        {
            var expFile = System.IO.Path.Combine(expInfo.Dir, ExpFileName);
            Console.WriteLine("Exp " + expFile);

            var exp = PopulationExperiment<DenseIndividual>.Load(expFile);
            var targets = exp.PopulationConfig.ExperimentConfiguration.Targets.ToArray();

            var samples = new List<bool>();
            void feedback(FileStuff stuff, Population<DenseIndividual> population, ITarget target, int epoch, long generationCount, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
            {
                // test for win
                var p = oldPopulationJudgements[0].Individual.Phenotype.Vector.Saturate(-1, 0, +1);
                var t = ((VectorTarget)target).PreparePerfectP().Saturate(-1, 0, +1);
                samples.Add(t.Equals(p));
            }

            PopulationTrace.RunTrace(System.Console.Out, ctx, null, exp.Population, exp.PopulationConfig, targets, null, feedback, exp.Epoch, RunonCount);
            return new ExpSamples<bool>(expInfo, samples, $"Count = {RunonCount}");
        }
    }

    public class TimeToHierarchyExpSampler : IExpSampler<int?>
    {
        public TimeToHierarchyExpSampler(Modules modules, string rcsFileName, double huskyComputeThreshold)
        {
            Modules = modules ?? throw new ArgumentNullException(nameof(modules));
            RcsFileName = rcsFileName ?? throw new ArgumentNullException(nameof(rcsFileName));
            HuskyComputeThreshold = huskyComputeThreshold;
        }

        public Modular.Modules Modules { get; }
        public string RcsFileName { get; }
        public double HuskyComputeThreshold { get; }

        public ExpSamples<int?> Sample(ModelExecutionContext ctx, ExpInfo expInfo)
        {
            var rcsFile = System.IO.Path.Combine(expInfo.Dir, RcsFileName);
            var trajectories = Analysis.LoadTrajectories(rcsFile, out var samplePeriod);
            var N = (int)Math.Round(Math.Sqrt(trajectories.Length));
            var mts = Modules.ModuleAssignments;

            var dtms = Analysis.EnumerateMatricies(trajectories, N, N, 0, trajectories[0].Length, true);
            var huskynesses = dtms.Select(dtm => Analysis.ComputeModuleHuskyness(dtm, mts)).ToArray();

            var completions = Enumerable.Range(0, Modules.ModuleCount).Select(mi => huskynesses.FirstIndex(hs => hs[mi] >= HuskyComputeThreshold && hs[mi] <= 1, out var idx, out _) ? (int?)(idx * samplePeriod) : null).ToArray();
            Console.WriteLine(string.Join(", ", completions));
            return new ExpSamples<int?>(expInfo, completions, $"Threshold = {HuskyComputeThreshold}");
        }
    }

    public class GroupPlotHelpers
    {
        public static Func<IEnumerable<WholeSample<TIndividual>>, double> ParseSelector<TIndividual>(string name) where TIndividual : IIndividual<TIndividual>
        {
            if (name.Equals("mean", StringComparison.InvariantCultureIgnoreCase))
            {
                return GroupPlotting.MeanWholesampleSelector;
            }
            else if (name.Equals("terminal", StringComparison.InvariantCultureIgnoreCase))
            {
                return GroupPlotting.TerminalWholesampleSelector;
            }
            else
            {
                throw new ArgumentException("Unrecognised selector name " + name);
            }
        }

        public static IEnumerable<ExpInfo> EnumerateManyExpInfos(IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string pattern, string blockTerminator)
        {
            if (dirs.Count != groupNames.Count)
                throw new ArgumentException("Must provide the same number of directories and group mames");

            for (int i = 0; i < dirs.Count; i++)
            {
                foreach (var e in ExpInfo.EnumerateExps(dirs[i], groupNames[i], pattern, blockTerminator))
                    yield return e;
            }
        }

        public static List<ExpSamples<T>> GroupSampling<T>(CliParams clips, string filepattern, IExpSampler<T> sampler)
        {
            var dir = clips.Get("dir", ".");
            var groupName = clips.Get("GroupName");
            var blockTerminator = clips.Get("blockTerminator", "runs");
            var count = clips.Get("count", int.Parse, 10);
            var seed = clips.Get("seed", int.Parse, 1);

            var exps = ExpInfo.EnumerateExps(dir, groupName, filepattern, blockTerminator);
            var expSamples = new List<ExpSamples<T>>();

            var ctx = new ModelExecutionContext(new CustomMersenneTwister(seed));

            foreach (var expInfo in exps)
            {
                expSamples.Add(sampler.Sample(ctx, expInfo));
            }

            return expSamples;
        }

        public static Func<string, string> Aliases(CliParams clips)
        {
            var aliasFilename = clips.Get("aliases", "xAliases.txt");

            Func<string, string> aliases;
            if (File.Exists(aliasFilename))
            {
                aliases = CliBestiary.Defaulting(CliBestiary.ReadAliases(aliasFilename));
            }
            else
            {
                aliases = x => x;
            }

            return aliases;
        }
    }
}
