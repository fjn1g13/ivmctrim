﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public class CliSparseSwitchers : ICliProvider
    {
        public string Key => "sparseswitchers";

        public void Run(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("SparseSwitchers finds all the sparse networks which are switchable by switching the zeroth node when it has a weight slight weight advantage");
                console.WriteLine("Available parameters:");
                console.WriteLine(" - sparseswitchers, the config to use");
                console.WriteLine(" - module, the list of traits in the module, default (all)");
                console.WriteLine(" - target, the index of the target to use, default 0");
                console.WriteLine(" - epoch, target reset exposure epoch, implies reset target if set");
                console.WriteLine(" - resettarget, resets the target (flag)");
                console.WriteLine(" - seed, random seed");
                console.WriteLine(" - proper, enables/disables cplus and cminus (flag)");
                console.WriteLine(" - cplus, list of c_plus values for target modules");
                console.WriteLine(" - cminus, list of c_minus values for target modules");
                console.WriteLine(" - T, alternative developmental step count");
            }

            var config = clips.Get("sparseswitchers", M4M.CliPlotHelpers.LoadExperimentConfig);
            var rand = new CustomMersenneTwister(clips.Get("seed", int.Parse, 0));
            var context = new ModelExecutionContext(rand);

            var targetIdx = clips.Get("target", int.Parse, 0);
            var target = config.Targets[targetIdx];

            if (clips.IsSet("resettarget") || clips.IsSet("epoch"))
            {
                var epoch = clips.Get("epoch", int.Parse, 0);

                ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                target.NextExposure(rand, config.JudgementRules, epoch, ref exposureInformation);
            }

            target.NextGeneration(rand, config.JudgementRules);

            if (clips.IsSet("proper"))
            {
                if (target is M4M.Epistatics.IIvmcProperTarget ipt)
                {
                    double[] cplus = clips.Get("cplus", s => s.Split(';').Select(double.Parse).ToArray(), null);
                    double[] cminus = clips.Get("cminus", s => s.Split(';').Select(double.Parse).ToArray(), null);
                    if (cplus != null)
                    {
                        cplus.CopyTo(ipt.Proper.Cplus, 0);
                    }
                    if (cminus != null)
                    {
                        cminus.CopyTo(ipt.Proper.Cminus, 0);
                    }
                }
                else
                {
                    console.WriteLine("Ignoring proper parameter, as target is no an IIvmcProperTarget");
                }
            }

            var moduleTraits = clips.Get("module", CliPlotHelpers.ParseIntList, Enumerable.Range(0, target.Size)).ToArray();
            var module = new HashSet<int>(moduleTraits);

            // just hope this doesn't find the Pit Of Despair
            var baseWeight = FindIdealIdentityWeight(rand, config, target, moduleTraits);
            console.WriteLine($"Base weight: {baseWeight}");
            
            var gm = Linear.CreateVector.Dense<double>(target.Size, i => module.Contains(i) ? config.InitialStateResetRange.Min : 0);
            var gp = Linear.CreateVector.Dense<double>(target.Size, i => i == moduleTraits[0] ? config.InitialStateResetRange.Max : module.Contains(i) ? config.InitialStateResetRange.Min : 0);
            
            var T = clips.Get("t", int.Parse, config.DevelopmentRules.TimeSteps);
            var drules = new DevelopmentRules(T, config.DevelopmentRules.UpdateRate, config.DevelopmentRules.DecayRate, config.DevelopmentRules.Squash);
            config = new ExperimentConfiguration(config.Size, config.Targets, config.TargetCycler, config.Epochs, config.GenerationsPerTargetPerEpoch, config.InitialStateResetProbability, config.InitialStateResetRange, false, drules, config.ReproductionRules, config.JudgementRules);

            foreach (var s in Combinatorics.EnumerateCombinations(Enumerable.Repeat(moduleTraits.Length, moduleTraits.Length).ToArray()))
            {
                var entries = s.Select((j, i) => new MatrixEntryAddress(moduleTraits[i], moduleTraits[j])).OrderBy(e => e.Col).ToList(); // put more weight near the leader

                var weightDropoff = 0.001;
                var b = CreateSparse(target.Size, baseWeight, weightDropoff, entries);
                
                var genomem = new DenseGenome(gm, b);
                var pm = genomem.Develop(context, config.DevelopmentRules);
                var jm = MultiMeasureJudgement.Judge(genomem, pm, config.JudgementRules, target);

                var genomep = new DenseGenome(gp, b);
                var pp = genomep.Develop(context, config.DevelopmentRules);
                var jp = MultiMeasureJudgement.Judge(genomem, pp, config.JudgementRules, target);

                if (jp.CombinedFitness > jm.CombinedFitness)
                {
                    console.WriteLine(string.Join(", ", entries));
                }
            }
        }

        public static Linear.Matrix<double> CreateSparse(int n, double baseWeight, double weightDropoff, IEnumerable<MatrixEntryAddress> entries)
        {
            var mat = Linear.CreateMatrix.Dense<double>(n, n);
            foreach (var e in entries)
            {
                mat[e.Row, e.Col] = baseWeight;
                baseWeight -= weightDropoff;
            }

            return mat;
        }

        public static double FindIdealIdentityWeight(MathNet.Numerics.Random.RandomSource rand, ExperimentConfiguration config, ITarget target, int[] moduleTraits, double wmin = 0, double wAdd = 128, double wEpsilon = 0.00001)
        {
            var module = new HashSet<int>(moduleTraits);

            var context = new ModelExecutionContext(rand);

            var g = Linear.CreateVector.Dense<double>(target.Size, i => module.Contains(i) ? config.InitialStateResetRange.Max : 0);
            var b = Linear.CreateMatrix.Dense<double>(target.Size, target.Size, 0.0);
            var genome = new DenseGenome(g, b);
            var p = genome.Develop(context, config.DevelopmentRules);
            
            double Judge(double w)
            {
                // refit b
                foreach (var i in moduleTraits)
                    b[i, i] = w;

                // develop
                genome.DevelopInto(p, context, config.DevelopmentRules);

                // evalutate
                var j = MultiMeasureJudgement.Judge(genome, p, config.JudgementRules, target);
                var f = j.CombinedFitness;

                return f;
            }

            return Optimisation.MaximiseUnary(Judge);
        }

        public void Run(CliParams clips, string filename)
        {
            throw new NotImplementedException();
        }
    }

    public static class Optimisation
    {
        /// <summary>
        /// Optimises a function with a single plateau
        /// Returns a value w within wEpsilon of a value that locally maximises func(w)
        /// </summary>
        /// <param name="func">The function to optimise</param>
        /// <param name="wStart">Where to start</param>
        /// <param name="wAdd">The initial increment to try</param>
        /// <param name="wEpsilon">The minimum increment to try</param>
        /// <returns>The value w what maximises func(w)</returns>
        public static double MaximiseUnary(Func<double, double> func, double wStart = 0, double wAdd = 128, double wEpsilon = 0.00001)
        {
            double w = wStart;
            double wLast = wStart;
            double fLast = double.MinValue;
            double dw = wAdd;

            bool broken = false;

            while (Math.Abs(dw) > wEpsilon)
            {
                var f = func(w);

                // move
                if (f > fLast)
                { // we improved, so keep going that way
                    fLast = f;
                    wLast = w;

                    if (!broken)
                        dw *= 2.0;
                }
                else
                { // we got worse, so step-back, reduce dw, and try the other way
                    w = wLast;
                    dw = -dw / 2.0;
                    broken = true;
                }

                w += dw;
            }
            
            return wLast;
        }
    }
}
