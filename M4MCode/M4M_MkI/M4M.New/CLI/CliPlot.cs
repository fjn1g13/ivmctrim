﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using M4M.Epistatics;
using M4M.New.OxyEx;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Random;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace M4M
{
    public interface IColourFilter
    {
        OxyColor Filter(OxyColor color);
    }

    // naive RGB blend
    public class WashoutFilter : IColourFilter
    {
        public WashoutFilter(OxyColor washoutColour)
        {
            WashoutColour = washoutColour;
        }

        public OxyColor WashoutColour { get; set; }

        public OxyColor Filter(OxyColor color)
        {
            if (color.IsInvisible())
            { // transparent
                return OxyColors.Transparent;
            }

            if (color.A == 255)
            { // opaque
                return color;
            }

            double l = color.A / 255.0;

            color = OxyColor.FromRgb(color.R, color.G, color.B);
            return OxyColor.Interpolate(WashoutColour, color, l);
        }
    }

    /// <summary>
    /// A render decorator which clips transparent elements
    /// </summary>
    public class ClippingRenderDecorator : RenderContextBase
    {
        private IRenderContext RenderContext { get; }
        private HashSet<OxyColor> ClipColours { get; }
        private bool ClipInvisible { get; }
        private bool RemoveTransparency { get; }
        private IColourFilter PreFilter { get; }
        private bool ClipZeroWidth { get; }

        public ClippingRenderDecorator(IRenderContext renderContext, IEnumerable<OxyColor> clipColours, bool clipInvisible, IColourFilter preFilter, bool clipZeroWidth)
        {
            RenderContext = renderContext;
            ClipInvisible = clipInvisible;
            PreFilter = preFilter;
            ClipZeroWidth = clipZeroWidth;

            ClipColours = new HashSet<OxyColor>(clipColours ?? Enumerable.Empty<OxyColor>());
        }

        public bool Filter(ref OxyColor color)
        {
            if (PreFilter != null)
                color = PreFilter.Filter(color);
            return (color.IsInvisible() && ClipInvisible) || ClipColours.Contains(color);
        }

        public override void DrawLine(IList<ScreenPoint> points, OxyColor stroke, double thickness, double[] dashArray, LineJoin lineJoin, bool aliased)
        {
            if (ClipZeroWidth && Math.Abs(thickness) == 0.0)
                return;

            if (Filter(ref stroke))
                return;

            RenderContext.DrawLine(points, stroke, thickness, dashArray, lineJoin, aliased);
        }

        public override void DrawPolygon(IList<ScreenPoint> points, OxyColor fill, OxyColor stroke, double thickness, double[] dashArray, LineJoin lineJoin, bool aliased)
        {
            bool clipFill = Filter(ref fill);
            bool clipStroke = Filter(ref stroke);

            if (clipFill && clipStroke)
                return;

            if (clipFill)
                fill = OxyColors.Transparent;
            if (clipStroke)
                stroke = OxyColors.Transparent;

            RenderContext.DrawPolygon(points, fill, stroke, thickness, dashArray, lineJoin, aliased);
        }

        public override void DrawText(ScreenPoint p, string text, OxyColor fill, string fontFamily, double fontSize, double fontWeight, double rotate, HorizontalAlignment halign, VerticalAlignment valign, OxySize? maxSize)
        {
            if (ClipZeroWidth && Math.Abs(fontSize) == 0.0)
                return;

            if (Filter(ref fill))
                return;

            RenderContext.DrawText(p, text, fill, fontFamily, fontSize, fontWeight, rotate, halign, valign, maxSize);
        }

        public override OxySize MeasureText(string text, string fontFamily, double fontSize, double fontWeight)
        {
            return RenderContext.MeasureText(text, fontFamily, fontSize, fontWeight);
        }
    }

    public interface ICliPlotter
    {
        string Prefix { get; }
        OxyPlot.PlotModel Plot(TextWriter console, CliParams clips, string filename, string title);
    }

    public class CliPlot : ICliProvider
    {
        public static readonly CliPresets DefaultPresets = new CliPresets("plotpresets", new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
        {
            ["gridlines"] = "majorgridlinestyle=solid minorgridlinestyle=dot",
            ["nominors"] = "minorgridlinecolour=transparent minorticksize=0",
            ["small"] = "size=0.5 intervalsize=1 minpad=0.05 maxpad=0.05", // switch to ss-margin when we can
        });

        public CliPresets Presets { get; }

        public static IEnumerable<ICliPlotter> CreateDefaultPlotters()
        {
            var traceePlotter = new CliTraceePlotter(CliTraceePlotter.DefaultDenseCliTraceeProviders);
            var defaultPlotters = new ICliPlotter[] {
                traceePlotter,
                new CliTracesPlotter(),
                CliTrajectoryPlotter.FitnessPlotter,
                CliTrajectoryPlotter.SudokuSamplesPlotter,
                CliTrajectoryPlotter.NQueensSamplesPlotter,
                CliTrajectoryPlotter.IstPlotter,
                CliTrajectoryPlotter.PstPlotter,
                CliTrajectoryPlotter.IvmcModuleSwitchesPlotter,
                CliTrajectoryPlotter.IvmcModuleSolvesPlotter,
                CliTrajectoryPlotter.IvmcPropersPlotter,
                new CliRcsPlotter(),
                new CliGenomePlotter(),
                new CliWholeSamplePlotter(traceePlotter),
                new CliPlotMisc(CliPlotMisc.DefaultMiscPlotters),
                new CliExpPlotter(),
                new CliGroupTraces(),
                new CliGroupBox(),
                new CliTrajTracesPlotter(),
                new CliGroupMeanRowSum(),
                new CliGroupPostWinFreq(),
                new CliGroupTimeToHierarchy()
            };

            return defaultPlotters;
        }

        public string Key => "plot";

        public IPlotExporter PlotExporter { get; }
        public ICliPlotter[] Plotters { get; }

        public CliPlot(IPlotExporter plotExporter, IEnumerable<ICliPlotter> plotters, CliPresets presets)
        {
            PlotExporter = plotExporter;
            Plotters = plotters.ToArray();
            Presets = presets;
        }

        public void Run(TextWriter console, CliParams clips)
        {
            Presets.Apply(console, clips);

            string infname = clips.Get("plot");

            if (infname != null)
            {
                string outfname = clips.Get("out", infname);

                string ifname = System.IO.Path.GetFileName(infname);
                string ofname = System.IO.Path.GetFileName(outfname);

                string title = clips.Get("title", ofname);

                if (PreparePlot(console, clips, infname, title, out var plot))
                {
                    if (plot == null)
                    {
                        console.WriteLine("(No Plot)");
                    }
                    else
                    {
                        PlotPlot(console, clips, plot, outfname);
                    }

                    return;
                }
                else
                {
                    console.WriteLine("Unsure how to plot " + infname);
                }
            }

            console.WriteLine("Recognised files (by start of file-name) are:");

            foreach (var plotter in Plotters)
            {
                console.WriteLine(plotter.Prefix);
            }
        }

        /// <summary>
        /// Attempts to prepare a plot from the given CliParams, FileName, and Title
        /// Returns true if a plotter was available, otherwise false
        /// </summary>
        public bool PreparePlot(TextWriter console, CliParams clips, string inFileName, string title, out PlotModel plot)
        {
            Presets.Apply(console, clips); // re-apply for e.g. M4MPlotting

            string filePrefix = clips.Get("prefix", System.IO.Path.GetFileName(inFileName));
            console.WriteLine(filePrefix);

            foreach (var plotter in Plotters)
            {
                if (filePrefix.StartsWith(plotter.Prefix, StringComparison.InvariantCultureIgnoreCase))
                {
                    plot = plotter.Plot(console, clips, inFileName, title);
                    return true;
                }
            }

            plot = null;
            return false;
        }

        public void PlotPlot(TextWriter console, CliParams clips, PlotModel plot, string outfilename)
        {
            double size = clips.Get("size", double.Parse, 1.0);
            double width = clips.Get("width", double.Parse, -1);
            double height = clips.Get("height", double.Parse, -1);

            bool bgBox = clips.IsSet("bgbox");

            bool clipInvisible = clips.Get("clipInvisible", bool.Parse, false);
            bool clipZeroWidth = clips.Get("clipZeroWidth", bool.Parse, false);
            var clipColours = clips.Get("clipColours", s => s.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(CliPlotHelpers.ParseColour), null);
            var washout = clips.IsSet("washoutColour");

            bool forceAspect = clips.IsSet("forceAspect");

            if (bgBox)
            {
                var bgBoxColour = clips.Get("bgbox", c => c == null ? OxyColors.White : CliPlotHelpers.ParseColour(c));
                new ViewBackgroundAnnotation(plot, bgBoxColour); // bit of hackery
            }

            if (clipInvisible || clipColours != null || washout || clipZeroWidth)
            {
                IColourFilter preFilter = null;
                if (washout)
                {
                    var washoutColour = clips.Get("washoutColour", s => s == null ? OxyColors.White : CliPlotHelpers.ParseColour(s), OxyColors.White);
                    preFilter = new WashoutFilter(washoutColour);
                }

                plot.RenderingDecorator = rc => new ClippingRenderDecorator(rc, clipColours, clipInvisible, preFilter, clipZeroWidth);
            }

            PreProcessPlot(clips, plot);
            PostProcessPlot postProcessor = model => PostProcessPlot(clips, model);

            var plotReport = width > 0 && height > 0
                ? PlotExporter.ExportPlot(outfilename, plot, width, height, forceAspect, postProcessor)
                : PlotExporter.ExportPlot(outfilename, plot, size, forceAspect, postProcessor);

            if (!clips.IsSet("quiet"))
            {
                PrintBasicInfo(console, plot);
                console.WriteLine(plotReport.Summary);
            }
        }

        public static void PrintBasicInfo(TextWriter console, PlotModel plot)
        {
            console.WriteLine("Axes: " + string.Join(", ", plot.Axes.Select(a => a.Key)));
            console.WriteLine("Padding (Margins): " + plot.ActualPlotMargins);
            console.WriteLine("InnerPadding: " + plot.Padding);
        }

        private string Dp => System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;

        private string StripZeroDps(string str)
        {
            if (str.Contains(Dp) && str.EndsWith("0"))
            {
                str = str.Substring(0, str.LastIndexOf('0'));
            }

            return str;
        }

        private string FormatDefault(double d)
        {
            return StripZeroDps(d.ToString("g6"));
        }

        public void PostProcessPlot(CliParams clips, PlotModel plot)
        {
            // axes formatting
            foreach (var axis in plot.Axes)
            {
                // shoild not most of these be in PreProcessPlot?
                var axisFormat = clips.Get("format:" + axis.Key, null);
                var axisTitle = clips.Get("title:" + axis.Key, null);
                var axisVisible = clips.Get("visible:" + axis.Key, bool.Parse, true);
                double? intervalSize = clips.Get("intervalsize:" + axis.Key, s => (double?)double.Parse(s), null);

                if (intervalSize.HasValue)
                    axis.IntervalLength *= intervalSize.Value;

                axis.IsAxisVisible = axisVisible;
                if (!axisVisible)
                    continue; // don't try

                if (axisFormat == null || axisFormat == "auto")
                {
                    axis.GetTickValues(out var mjlvs, out var mjtvs, out var mntvs);
                    if (mjtvs.Select(FormatDefault).Any(g6str => g6str.Contains("E")))
                    {
                        // give up and use the G6 (can't seem to create a variable precision exponential format string)
                        axis.StringFormat = "g6";
                    }
                    else
                    {
                        int ups = mjtvs.Select(FormatDefault).Max(g6str => g6str.IndexOf(Dp) < 0 ? g6str.Length : g6str.IndexOf(Dp));
                        int dps = mjtvs.Select(FormatDefault).Max(g6str => g6str.IndexOf(Dp) < 0 ? 0 : g6str.Length - g6str.IndexOf(Dp) - Dp.Length);

                        // don't normalise ups for now
                        ups = 1;

                        string sf = new string('0', ups) + (dps > 0 ? "." + new string('0', dps) : "");
                        axis.StringFormat = sf;
                    }
                }
            }
        }

        public void PreProcessPlot(CliParams clips, PlotModel plot)
        {
            // remove yellow in default colours (too light), add SlateGrey on the end
            plot.DefaultColors.Add(OxyColors.SlateGray);
            plot.DefaultColors.RemoveAt(6);

            if (clips.IsSet("allgrey"))
            {
                int greyCount = Math.Max(2, (int)(plot.Series.Count * 1.1) + 1); // we don't want anything too white
                plot.DefaultColors = OxyPalettes.Gray(greyCount).Colors.Take(plot.Series.Count).ToArray();
            }

            if (clips.IsSet("defaultcolors"))
            {
                plot.DefaultColors = clips.Get("defaultcolors").Split(',', ';').Select(CliPlotHelpers.ParseColour).ToArray();
            }

            CursorInfoAnnotation cia = null;

            // axes formatting
            foreach (var axis in plot.Axes)
            {
                var axisFormat = clips.Get("format:" + axis.Key, null);
                if (axisFormat != null && axisFormat != "auto" && axisFormat != "default")
                {
                    axis.StringFormat = axisFormat;
                }

                axis.IsAxisVisible = clips.Get("visible:" + axis.Key, bool.Parse, axis.IsAxisVisible);
                axis.Title = clips.Get("title:" + axis.Key, axis.Title);

                axis.MajorTickSize = GetMaybeQualified(clips, "MajorTickSize", axis.Key, double.Parse, axis.MajorTickSize);
                axis.MajorGridlineStyle = GetMaybeQualified(clips, "MajorGridlinestyle", axis.Key, CliPlotHelpers.ParseLineStyle, axis.MajorGridlineStyle);
                axis.MajorGridlineColor = GetMaybeQualified(clips, "MajorGridlinecolour", axis.Key, CliPlotHelpers.ParseColour, axis.MajorGridlineColor);
                axis.MajorGridlineThickness = GetMaybeQualified(clips, "MajorGridlinethickness", axis.Key, double.Parse, axis.MajorGridlineThickness);

                axis.MinorTickSize = GetMaybeQualified(clips, "MinorTickSize", axis.Key, double.Parse, axis.MinorTickSize);
                axis.MinorGridlineStyle = GetMaybeQualified(clips, "MinorGridlinestyle", axis.Key, CliPlotHelpers.ParseLineStyle, axis.MinorGridlineStyle);
                axis.MinorGridlineColor = GetMaybeQualified(clips, "MinorGridlinecolour", axis.Key, CliPlotHelpers.ParseColour, axis.MinorGridlineColor);
                axis.MinorGridlineThickness = GetMaybeQualified(clips, "MinorGridlinethickness", axis.Key, double.Parse, axis.MinorGridlineThickness);

                axis.AxislineStyle = GetMaybeQualified(clips, "axislinestyle", axis.Key, CliPlotHelpers.ParseLineStyle, axis.AxislineStyle);
                axis.AxislineColor = GetMaybeQualified(clips, "axislinecolour", axis.Key, CliPlotHelpers.ParseColour, axis.AxislineColor);
                axis.AxislineThickness = GetMaybeQualified(clips, "axislinethickness", axis.Key, double.Parse, axis.AxislineThickness);

                axis.StartPosition = GetMaybeQualified(clips, "StartPosition", axis.Key, double.Parse, axis.StartPosition);
                axis.EndPosition = GetMaybeQualified(clips, "EndPosition", axis.Key, double.Parse, axis.EndPosition);

                axis.AxisTitleDistance = GetMaybeQualified(clips, "axistitledistance", axis.Key, double.Parse, axis.AxisTitleDistance);

                axis.TicklineColor = GetMaybeQualified(clips, "tickcolour", axis.Key, CliPlotHelpers.ParseColour, axis.TicklineColor);
                axis.TickStyle = GetMaybeQualified(clips, "tickstyle", axis.Key, CliPlotHelpers.ParseTickStyle, axis.TickStyle);
                axis.TextColor = GetMaybeQualified(clips, "axislabelcolour", axis.Key, CliPlotHelpers.ParseColour, axis.TextColor);
                axis.TitleColor = GetMaybeQualified(clips, "axistitlecolour", axis.Key, CliPlotHelpers.ParseColour, axis.TitleColor);

                axis.Unit = clips.Get("unit:" + axis.Key, axis.Unit);

                axis.Minimum = clips.Get("min:" + axis.Key, double.Parse, axis.Minimum);
                axis.Maximum = clips.Get("max:" + axis.Key, double.Parse, axis.Maximum);
                axis.MinimumPadding = GetMaybeQualified(clips, "minpad", axis.Key, double.Parse, axis.MinimumPadding);
                axis.MaximumPadding = GetMaybeQualified(clips, "maxpad", axis.Key, double.Parse, axis.MaximumPadding);
                axis.PositionTier = clips.Get("tier:" + axis.Key, int.Parse, axis.PositionTier);
                axis.PositionAtZeroCrossing = GetMaybeQualified(clips, "zerocrossing", axis.Key, bool.Parse, axis.PositionAtZeroCrossing);

                var axisPos = clips.Get("position:" + axis.Key, clips.Get("pos:" + axis.Key, null));
                if (!string.IsNullOrEmpty(axisPos))
                {
                    axis.Position = CliPlotHelpers.ParseAxisPosition(axisPos);
                }

                var cursorColor = GetMaybeQualified(clips, "cursors", axis.Key, s => s == null ? OxyColors.Automatic : s == "" ? OxyColors.Transparent : CliPlotHelpers.ParseColour(s), OxyColors.Undefined);
                if ((axis.IsHorizontal() || axis.IsVertical()) && (cursorColor.IsVisible() || cursorColor.IsAutomatic()))
                {
                    if (cia == null)
                    {
                        cia = new CursorInfoAnnotation();
                        plot.Annotations.Add(cia);
                    }

                    if (cursorColor.IsAutomatic())
                    {
                        var s = plot.Series.FirstOrDefault(_s => _s is XYAxisSeries xys && xys.YAxisKey == axis.Key);
                        if (s is LineSeries ls)
                            cursorColor = ls.Color;
                        else if (s is ScatterSeries ss)
                            cursorColor = ss.MarkerFill;
                        else if (s is AreaSeries @as)
                            cursorColor = @as.Fill;
                    }

                    if (cursorColor.IsAutomatic())
                    {
                        cursorColor = OxyColors.Gray;
                    }

                    Axis otherAxis = null;// plot.Axes.FirstOrDefault(a => (a.IsHorizontal() || a.IsVertical()) && a.IsHorizontal() != axis.IsHorizontal());

                    bool ry = axis.IsVertical() ^ (axis.Position == AxisPosition.Left || axis.Position == AxisPosition.Bottom);
                    bool rx = axis.IsVertical() ^ (axis.Position == AxisPosition.Left || axis.Position == AxisPosition.Bottom);
                    var low = new CursorAnnotation(axis.Key, otherAxis?.Key) { Color = cursorColor, ReverseX = rx, ReverseY = ry };
                    var high = new CursorAnnotation(axis.Key, otherAxis?.Key) { Color = cursorColor, ReverseX = rx, ReverseY = ry };
                    cia.AddPair(low, high);
                    plot.Annotations.Add(low);
                    plot.Annotations.Add(high);
                }
            }

            // border
            OxyThickness border = clips.Get("border", ParseOxyThickness, plot.PlotAreaBorderThickness);
            plot.PlotAreaBorderThickness = border;

            // size
            double size = clips.Get("size", double.Parse, 1.0);

            // padding (overrides min)
            if (clips.IsSet("padding"))
            {
                plot.PlotMargins = clips.Get("padding", ParseOxyThickness);
            }

            // innerpadding
            if (clips.IsSet("innerpadding"))
            {
                plot.Padding = clips.Get("innerpadding", ParseOxyThickness);
            }

            if (clips.IsSet("reverselegend"))
            {
                plot.LegendItemOrder = LegendItemOrder.Reverse;
            }

            // intervals
            double intervalSize = clips.Get("intervalsize", double.Parse, size);
            double horizontalIntervalSize = clips.Get("intervalsizeh", double.Parse, intervalSize);
            double verticleIntervalSize = clips.Get("intervalsizev", double.Parse, intervalSize);
            foreach (var axis in plot.Axes)
            {
                if (axis.IsHorizontal())
                    axis.IntervalLength *= horizontalIntervalSize;
                else if (axis.IsVertical())
                    axis.IntervalLength *= verticleIntervalSize;
                else
                    axis.IntervalLength *= intervalSize;
            }

            // legend
            var legend = clips.Get("legend", null);
            plot.LegendTitle = clips.Get("legendTitle", plot.LegendTitle);
            plot.LegendBackground = clips.Get("legendBackgroundColour", CliPlotHelpers.ParseColour, plot.LegendBackground);
            plot.LegendBorder = clips.Get("legendBorderColour", CliPlotHelpers.ParseColour, plot.LegendBorder);
            plot.LegendBorderThickness = clips.Get("legendBorderThickness", double.Parse, plot.LegendBorderThickness);

            if (legend != null)
            {
                // this should be in a method
                if (legend == "")
                {
                    plot.IsLegendVisible = false;
                }

                if (legend.Length > 0)
                {
                    var stewer = new StringChewer();

                    var inside = legend.Contains('i') || legend.Contains('I');
                    var outside = legend.Contains('o') || legend.Contains('O');
                    var bottom = legend.Contains('b') || legend.Contains('B');
                    var top = legend.Contains('t') || legend.Contains('T');
                    var middle = legend.Contains('m') || legend.Contains('M');
                    var left = legend.Contains('l') || legend.Contains('L');
                    var right = legend.Contains('r') || legend.Contains('R');
                    var horizontal = legend.Contains('h') || legend.Contains('H');
                    var vertial = legend.Contains('v') || legend.Contains('V');
                    var noPadding = legend.Contains('0');

                    if (int.TryParse(legend, out int legendPosition))
                    {
                        plot.LegendPosition = (LegendPosition)legendPosition;
                    }
                    else
                    {
                        LegendPlacement pl = plot.LegendPlacement;
                        LegendPosition lp = plot.LegendPosition;
                        LegendOrientation lo = plot.LegendOrientation;

                        if (inside)
                        {
                            pl = LegendPlacement.Inside;
                        }
                        else if (outside)
                        {
                            pl = LegendPlacement.Outside;
                        }

                        if (left)
                        {
                            lo = LegendOrientation.Vertical;

                            if (bottom)
                            {
                                lp = LegendPosition.BottomLeft;
                            }
                            else if (middle)
                            {
                                lp = LegendPosition.LeftMiddle;
                            }
                            else
                            {
                                lp = LegendPosition.TopLeft;
                            }
                        }
                        else if (right)
                        {
                            lo = LegendOrientation.Vertical;

                            if (bottom)
                            {
                                lp = LegendPosition.BottomRight;
                            }
                            else if (middle)
                            {
                                lp = LegendPosition.RightMiddle;
                            }
                            else
                            {
                                lp = LegendPosition.TopRight;
                            }
                        }
                        else
                        {
                            lo = LegendOrientation.Horizontal;

                            if (top)
                            {
                                lp = LegendPosition.TopCenter;
                            }
                            else if (bottom)
                            {
                                lp = LegendPosition.BottomCenter;
                            }
                        }

                        if (horizontal)
                        {
                            lo = LegendOrientation.Horizontal;
                        }
                        else if (vertial)
                        {
                            lo = LegendOrientation.Vertical;
                        }

                        plot.LegendPlacement = pl;
                        plot.LegendPosition = lp;
                        plot.LegendOrientation = lo;
                    }
                }
            }

            // font
            string font = clips.Get("font", "Arial");
            plot.DefaultFont = font;
            plot.TitleFont = font;
            plot.SubtitleFont = font;
            plot.LegendFont = font;

            plot.DefaultFontSize = clips.Get("fontSize", double.Parse, plot.DefaultFontSize);
            plot.TitleFontSize = clips.Get("titleFontSize", double.Parse, plot.TitleFontSize);
            plot.TitleFontWeight = clips.Get("titleFontWeight", double.Parse, plot.TitleFontWeight);

            plot.SubtitleFontSize = clips.Get("subtitleFontSize", double.Parse, plot.SubtitleFontSize);
            plot.SubtitleFontWeight = clips.Get("subtitleFontWeight", double.Parse, plot.SubtitleFontWeight);

            // figure label
            var figureLabel = clips.Get("fig", null);
            var figureLabelColour = clips.Get("figcolour", CliPlotHelpers.ParseColour, plot.TextColor);
            var figureLabelFontSize = clips.Get("figfontsize", double.Parse, 21);
            if (figureLabel != null)
                new FigureLabelAnnotation(plot, figureLabel, figureLabelColour) { FontSize = figureLabelFontSize, Font = font };

            // water mark
            var waterMark = clips.Get("watermark", null);
            var waterMarkColour = clips.Get("watermarkcolour", CliPlotHelpers.ParseColour, OxyColors.LightGray);
            if (waterMark != null)
                new WaterMarkAnnotation(plot, waterMark, waterMarkColour);

            // misc annotations
            var annotations = clips.Get("annotations", s => s.Split(';'), null);
            if (annotations != null)
            {
                foreach (var annStr in annotations)
                {
                    foreach (var a in ParseAnnotation(annStr, OxyColors.LightGray, OxyColors.DimGray, OxyColors.Automatic))
                    {
                        plot.Annotations.Add(a);
                    }
                }
            }

            // side annotations
            var sides = new[] { AxisPosition.Bottom, AxisPosition.Top, AxisPosition.Left, AxisPosition.Right };
            foreach (var side in sides)
            {
                var sideText = clips.Get("side:" + side, null);
                if (!string.IsNullOrWhiteSpace(sideText))
                    plot.Annotations.Add(new SideAnnotation(side, sideText));
            }

            // for when the target doesn't change
            double repeatLines = clips.Get("repeatlines", double.Parse, double.NaN);
            if (!double.IsNaN(repeatLines))
            {
                var rl = new RepeatingLineAnnotation(plot, OxyColors.Gray, repeatLines);
            }

            // series
            foreach (var s in plot.Series)
            {
                var key = s.Title;
                bool reduceGeometry = GetMaybeQualified(clips, "reduceGeometry", key, bool.Parse, false);

                if (reduceGeometry)
                {
                    switch (s)
                    {
                        case OxyPlot.Series.AreaSeries a:
                            {
                                var ps = CliPlotHelpers.Reduce(a.Points, 1E-10).ToArray();
                                a.Points.Clear();
                                a.Points.AddRange(ps);
                                ps = CliPlotHelpers.Reduce(a.Points2, 1E-10).ToArray();
                                a.Points2.Clear();
                                a.Points2.AddRange(ps);
                                break;
                            }
                        case OxyPlot.Series.LineSeries l:
                            {
                                var ps = CliPlotHelpers.Reduce(l.Points, 1E-10).ToArray();
                                l.Points.Clear();
                                l.Points.AddRange(ps);
                                break;
                            }
                    }
                }
            }
        }

        private static IEnumerable<Annotation> ParseAnnotation(string annotationString, OxyColor defaultStrokeColour, OxyColor defaultFillColour, OxyColor defaultTextColour)
        {
            var data = StringHelpers.Split(annotationString).ToArray();
            if (data.Length < 1)
                throw new Exception("Invalid annotation string: " + annotationString);

            var layer = AnnotationLayer.BelowSeries;

            // common args
            if (data[0].StartsWith("!"))
            {
                var common = data[0];
                data = data.Skip(1).ToArray();

                if (common.Contains("a"))
                {
                    layer = AnnotationLayer.AboveSeries;
                }
            }

            Annotation applyCommon(Annotation a)
            {
                a.Layer = layer;
                return a;
            }

            var cmd = data[0];

            T required<T>(int idx, Func<string, T> parser)
            {
                if (data.Length > idx + 1)
                    return parser(data[idx + 1]);
                else
                    throw new Exception("Invalid annotation string: " + annotationString);
            }

            T optional<T>(int idx, Func<string, T> parser, T @default)
            {
                if (data.Length > idx + 1)
                    return parser(data[idx + 1]);
                else
                    return @default;
            }

            // TODO: these should be in a dictionary
            if (cmd.SoftEquals("lx"))
            { // lx x text colour textColor
                var x = required(0, double.Parse);
                var text = optional(1, s => s, "");
                var color = optional(2, CliPlotHelpers.ParseColour, defaultStrokeColour);
                var textColor = optional(3, CliPlotHelpers.ParseColour, defaultTextColour);
                yield return applyCommon(new LineAnnotation() { X = x, Text = text, Color = color, Type = LineAnnotationType.Vertical, TextColor = textColor });
            }
            else if (cmd.SoftEquals("ly"))
            { // ly y text colour textColor
                var y = required(0, double.Parse);
                var text = optional(1, s => s, "");
                var color = optional(2, CliPlotHelpers.ParseColour, defaultStrokeColour);
                var textColor = optional(3, CliPlotHelpers.ParseColour, defaultTextColour);
                yield return applyCommon(new LineAnnotation() { Y = y, Text = text, Color = color, Type = LineAnnotationType.Horizontal, TextColor = textColor });
            }
            else if (cmd.SoftEquals("ax"))
            { // ax x0 x1 text colour textColor
                var x0 = required(0, double.Parse);
                var x1 = required(1, double.Parse);
                var text = optional(2, s => s, "");
                var color = optional(3, CliPlotHelpers.ParseColour, defaultFillColour);
                var textColor = optional(4, CliPlotHelpers.ParseColour, defaultTextColour);
                yield return applyCommon(new RectangleAnnotation() { MinimumX = x0, MaximumX = x1, MinimumY = double.NaN, MaximumY = double.NaN, Text = text, Fill = color, TextColor = textColor });
            }
            else if (cmd.SoftEquals("ay"))
            { // ay y0 x1 text colour textColor
                var y0 = required(0, double.Parse);
                var y1 = required(1, double.Parse);
                var text = optional(2, s => s, "");
                var color = optional(3, CliPlotHelpers.ParseColour, defaultFillColour);
                var textColor = optional(4, CliPlotHelpers.ParseColour, defaultTextColour);
                yield return applyCommon(new RectangleAnnotation() { MinimumY = y0, MaximumY = y1, MinimumX = double.NaN, MaximumX = double.NaN, Text = text, Fill = color, TextColor = textColor });
            }
        }

        public static string GetMaybeQualified(CliParams clips, string prefix, string qualifier, string @default)
        {
            return clips.GetOrCreate(prefix + ":" + qualifier, () => clips.Get(prefix, @default));
        }

        public static T GetMaybeQualified<T>(CliParams clips, string prefix, string qualifier, Func<string, T> parser, T @default)
        {
            return clips.GetOrCreate(prefix + ":" + qualifier, parser, () => clips.Get(prefix, parser, @default));
        }

        public static OxyThickness ParseOxyThickness(string str)
        {
            double[] arr = str.Split(new char[] { ',', ';' }).Select(s => s == "" ? double.NaN : double.Parse(s)).ToArray();

            if (arr.Length == 1)
            {
                return new OxyThickness(arr[0], arr[0], arr[0], arr[0]);
            }
            else if (arr.Length == 2)
            {
                return new OxyThickness(arr[0], arr[1], arr[0], arr[1]);
            }
            else if (arr.Length == 4)
            {
                return new OxyThickness(arr[0], arr[1], arr[2], arr[3]);
            }

            throw new ArgumentException(nameof(str), "Not a valid OxyThickness");
        }
    }

    public class StringChewer
    {
        private Dictionary<char, Action> Actions { get; } = new Dictionary<char, Action>();

        public StringChewer()
        {
        }

        public void Add(Action action, params char[] chars)
        {
            foreach (char c in chars)
                Actions.Add(c, action);
        }

        public string Chew(string str)
        {
            while (str.Length > 0)
            {
                char c = str[0];
                if (Actions.TryGetValue(c, out var action))
                {
                    action();
                    str = str.Substring(1);
                    continue;
                }

                break;
            }

            return str;
        }
    }

    public interface ICliTraceeProvider<T> where T : IIndividual<T>
    {
        string Prefix { get; }
        void PlotStuff(TextWriter console, PlotModel model, string name, CliParams clips, TraceInfo<T> traceInfo, bool epochsx, double offset);
    }

    public class HuskyProvider : ICliTraceeProvider<DenseIndividual>
    {
        public HuskyProvider(string prefix, string defaultTitle, OxyColor c0, OxyColor c1, double strokeThickness)
        {
            Prefix = prefix;
            DefaultTitle = defaultTitle;

            C0 = c0;
            C1 = c1;
            StrokeThickness = strokeThickness;
        }

        public string Prefix { get; }
        public string DefaultTitle { get; }
        public OxyColor C0 { get; }
        public OxyColor C1 { get; }
        public double StrokeThickness { get; }

        public static Func<int, Func<WholeSample<DenseIndividual>, double>> ModulesHuskySampler(IReadOnlyList<IReadOnlyList<int>> moduleAssignments)
        {
            return i => ModuleHuskySampler(moduleAssignments[i]);
        }

        public static Func<WholeSample<DenseIndividual>, double> ModuleHuskySampler(IReadOnlyList<int> mts)
        {
            return ws =>
            {
                return ws.Judgements.Average(j => Analysis.ComputeModuleHuskyness(j.Individual.Genome.TransMat, mts));
            };
        }

        public void PlotStuff(TextWriter console, PlotModel model, string name, CliParams clips, TraceInfo<DenseIndividual> traceInfo, bool epochsx, double offset)
        {
            var modules = clips.Get("modules:" + name, Modular.MultiModulesStringParser.Instance.Parse, CliPlotHelpers.GuessModules(console, clips, traceInfo.TraceWholeSamples[0].Target.Size));
            var lineCount = modules.ModuleCount;

            Tracee1DProvider<DenseIndividual>.PlotStuff(console, model, name, clips, traceInfo, epochsx, offset, lineCount, ModulesHuskySampler(modules.ModuleAssignments), DefaultTitle, C0, C1, StrokeThickness);
        }
    }

    public class ModuleIndependenceProvider : ICliTraceeProvider<DenseIndividual>
    {
        public ModuleIndependenceProvider(string prefix, string defaultTitle, OxyColor c0, OxyColor c1, double strokeThickness)
        {
            Prefix = prefix;
            DefaultTitle = defaultTitle;

            C0 = c0;
            C1 = c1;
            StrokeThickness = strokeThickness;
        }

        public string Prefix { get; }
        public string DefaultTitle { get; }
        public OxyColor C0 { get; }
        public OxyColor C1 { get; }
        public double StrokeThickness { get; }

        public static Func<int, Func<WholeSample<DenseIndividual>, double>> ModulesIndependenceSampler(IReadOnlyList<IReadOnlyList<int>> moduleAssignments)
        {
            return i => ModuleModuleIndependenceSampler(moduleAssignments[i]);
        }

        public static Func<WholeSample<DenseIndividual>, double> ModuleModuleIndependenceSampler(IReadOnlyList<int> mts)
        {
            return ws =>
            {
                return ws.Judgements.Average(j => Analysis.ComputeModuleIndependence(j.Individual.Genome.TransMat, mts));
            };
        }

        public void PlotStuff(TextWriter console, PlotModel model, string name, CliParams clips, TraceInfo<DenseIndividual> traceInfo, bool epochsx, double offset)
        {
            var modules = clips.Get("modules:" + name, Modular.MultiModulesStringParser.Instance.Parse, CliPlotHelpers.GuessModules(console, clips, traceInfo.TraceWholeSamples[0].Target.Size));
            var lineCount = modules.ModuleCount;

            Tracee1DProvider<DenseIndividual>.PlotStuff(console, model, name, clips, traceInfo, epochsx, offset, lineCount, ModulesIndependenceSampler(modules.ModuleAssignments), DefaultTitle, C0, C1, StrokeThickness);
        }
    }

    public class DeltaBProvider : ICliTraceeProvider<DenseIndividual>
    {
        public DeltaBProvider(string prefix, string defaultTitle, OxyColor c00, OxyColor c10, OxyColor c01, OxyColor c11, double strokeThickness)
        {
            Prefix = prefix;
            DefaultTitle = defaultTitle;

            C00 = c00;
            C10 = c10;
            C01 = c01;
            C11 = c11;
            StrokeThickness = strokeThickness;
        }

        public string Prefix { get; }
        public string DefaultTitle { get; }

        public OxyColor C00 { get; }
        public OxyColor C10 { get; }
        public OxyColor C01 { get; }
        public OxyColor C11 { get; }
        public double StrokeThickness { get; }

        public void PlotStuff(TextWriter console, PlotModel model, string name, CliParams clips, TraceInfo<DenseIndividual> traceInfo, bool epochsx, double offset)
        {
            var config = CliPlotHelpers.LoadDenseExperimentConfig(clips.Get("expfile"));

            var delta = clips.Get("delta", double.Parse);
            var signed = clips.Get("signed", bool.Parse);

            var strokeThickness = clips.Get("strokeThickness", double.Parse, StrokeThickness);
            var diff = clips.Get("diff:" + name, bool.Parse, clips.IsSet("diff"));
            var title = clips.Get("title:" + name, DefaultTitle);

            int dim1 = traceInfo.TraceWholeSamples[0].Judgements[0].Individual.Genome.Size;
            int dim2 = dim1;
            int[] lines = Enumerable.Range(0, dim1 * dim2).ToArray();

            if (clips.IsSet("keep:" + name))
            {
                IEnumerable<int> keep = clips.Get("keep:" + name, CliPlotHelpers.ParseIntList);
                lines = CliPlotHelpers.Keep(lines, keep, -1);
            }

            List<OxyColor> colours = new List<OxyColor>();
            List<Func<IEnumerable<WholeSample<DenseIndividual>>, double>> samplerMetrics = new List<Func<IEnumerable<WholeSample<DenseIndividual>>, double>>();

            OxyColor[] colors2d = TrajectoryPlotting.Colours2D(dim1, C00, C10, C01, C11);

            var context = new ModelExecutionContext(new CustomMersenneTwister(0));
            var rand = context.DisableRandom();

            Func<WholeSample<DenseIndividual>, double> sampler(int i, int j)
            {
                var meas = new MatrixEntryAddress[] { new MatrixEntryAddress(i, j) };
                return (WholeSample<DenseIndividual> ws) =>
                {
                    return Analysis.ComputeSimpleDelta<double>(context, ws.Target, config.JudgementRules, config.DevelopmentRules, ws.Judgements.First().Individual, meas, delta, signed, (f2, f1) => (f2.CombinedFitness - f1.CombinedFitness) / delta);
                };
            }

            context.EnableRandom(rand);

            // lines
            for (int i = 0; i < dim1; i++)
            {
                for (int j = 0; j < dim2; j++)
                {
                    if (lines[i * dim1 + j] < 0) // not-kept
                        continue;

                    var samplerMetric = sampler(i, j);

                    samplerMetrics.Add(many => samplerMetric(many.First()));
                    colours.Add(colors2d[i * dim1 + j]);
                }
            }

            Tracee1DProvider<DenseIndividual>.PlotStuffs(model, name, title, clips, traceInfo, epochsx, offset, samplerMetrics, colours, strokeThickness);
        }
    }

    public class Tracee1DProvider<T> : ICliTraceeProvider<T> where T : IIndividual<T>
    {
        public static int DenseSize(TraceInfo<DenseIndividual> traceInfo) => traceInfo.TraceWholeSamples.First().Judgements[0].Individual.Genome.Size;

        public Tracee1DProvider(string prefix, string defaultTitle, Func<int, Func<WholeSample<T>, double>> sampler1D, Func<TraceInfo<T>, int> defaultCount, OxyColor c0, OxyColor c1, double strokeThickness)
        {
            Prefix = prefix;
            DefaultTitle = defaultTitle;
            Sampler1D = sampler1D;
            DefaultCount = defaultCount;

            C0 = c0;
            C1 = c1;
            StrokeThickness = strokeThickness;
        }

        public string Prefix { get; }
        public string DefaultTitle { get; }
        public Func<int, Func<WholeSample<T>, double>> Sampler1D { get; }
        public Func<TraceInfo<T>, int> DefaultCount { get; }

        public OxyColor C0 { get; }
        public OxyColor C1 { get; }
        public double StrokeThickness { get; }

        public void PlotStuff(TextWriter console, PlotModel model, string name, CliParams clips, TraceInfo<T> traceInfo, bool epochsx, double offset)
        {
            int lineCount = DefaultCount(traceInfo);
            PlotStuff(console, model, name, clips, traceInfo, epochsx, offset, lineCount, Sampler1D, DefaultTitle, C0, C1, StrokeThickness);
        }

        public static void PlotStuff(TextWriter console, PlotModel model, string name, CliParams clips, TraceInfo<T> traceInfo, bool epochsx, double offset, int lineCount, Func<int, Func<WholeSample<T>, double>> sampler1D, string defaultTitle, OxyColor c0, OxyColor c1, double strokeThickness)
        {
            var diff = clips.Get("diff:" + name, bool.Parse, clips.IsSet("diff"));
            var title = clips.Get("title:" + name, defaultTitle);

            int[] lines = Enumerable.Range(0, lineCount).ToArray();

            if (clips.IsSet("keep:" + name))
            {
                IEnumerable<int> keep = clips.Get("keep:" + name, CliPlotHelpers.ParseIntList);
                lines = CliPlotHelpers.Keep(lines, keep, -1);
            }

            c0 = clips.Get("c0:" + name, CliPlotHelpers.ParseColour, c0);
            c1 = clips.Get("c1:" + name, CliPlotHelpers.ParseColour, c1);

            List<OxyColor> colours = new List<OxyColor>();
            List<Func<IEnumerable<WholeSample<T>>, double>> samplerMetrics = new List<Func<IEnumerable<WholeSample<T>>, double>>();

            // lines
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i] < 0) // not-kept
                    continue;

                OxyColor colour;

                if (lines.Length > 1 && c0 != null && c1 != null)
                {
                    var z = (double)i / (lines.Length - 1);
                    colour = OxyColor.Interpolate(c0, c1, z);
                }
                else
                {
                    colour = c0;
                }

                var sampler = sampler1D(i);
                var samplerMetric = TracePlotting.AverageMetric(traceInfo, diff, sampler);

                colours.Add(colour);
                samplerMetrics.Add(samplerMetric);
            }

            PlotStuffs(model, name, title, clips, traceInfo, epochsx, offset, samplerMetrics, colours, strokeThickness);
        }

        public static IReadOnlyList<OxyPlot.Series.Series> PlotStuffs(PlotModel model, string name, string title, CliParams clips, TraceInfo<T> traceInfo, bool epochsx, double offset, IReadOnlyList<Func<IEnumerable<WholeSample<T>>, double>> samplerMetrics, IReadOnlyList<OxyColor> colours, double strokeThickness)
        {
            strokeThickness = CliPlot.GetMaybeQualified(clips, "strokeThickness", name, double.Parse, strokeThickness);
            var yAxisKey = clips.Get("yaxiskey:" + name, name);
            var xAxisKey = clips.Get("xaxiskey:" + name, clips.Get("xaxiskey", "generations"));
            var resolution = CliPlot.GetMaybeQualified(clips, "resolution", name, Misc.ParseInt, 1);
            var onRight = clips.Get("onRight:" + name, bool.Parse, clips.IsSet("onRight"));
            var noAxis = clips.IsSet("noAxis:" + name) || model.Axes.Any(a => a.Key == yAxisKey);
            var diff = clips.Get("diff:" + name, bool.Parse, clips.IsSet("diff"));

            var colour = clips.Get("colour:" + name, s => s == null ? null : (OxyColor?)CliPlotHelpers.ParseColour(s), null);
            var markerColour = clips.Get("markerColour:" + name, s => s == null ? OxyColors.Transparent : CliPlotHelpers.ParseColour(s), OxyColors.Automatic);
            var plotType = clips.Get("plotType:" + name, CliPlotHelpers.ParseGenerousTrajectoryPlotType, clips.Get("plottype", CliPlotHelpers.ParseGenerousTrajectoryPlotType, TrajectoryPlotType.Line));
            var markerType = clips.Get("markerType:" + name, CliPlotHelpers.ParseMarkerType, clips.Get("markerType", CliPlotHelpers.ParseMarkerType, plotType == TrajectoryPlotType.Line ? MarkerType.None : MarkerType.Diamond));
            var lineStyle = clips.Get("lineStyle:" + name, CliPlotHelpers.ParseLineStyle, LineStyle.Automatic);
            var renderInLegend = clips.Get("legend:" + name, bool.Parse, plotType != TrajectoryPlotType.Rectangles);
            var injectBreaks = CliPlot.GetMaybeQualified(clips, "injectbreaks", name, bool.Parse, false);
            var brokenLineStyle = CliPlot.GetMaybeQualified(clips, "brokenLineStyle", name, CliPlotHelpers.ParseLineStyle, LineStyle.Dot);
            var seriesName = clips.Get("seriesName:" + name, title);

            if (plotType == TrajectoryPlotType.Rectangles)
            { // TODO: can we push this into PlotTraceLine?
                var colorAxisKey = CliPlot.GetMaybeQualified(clips, "coloraxiskey", name, "color");
                var noColorAxis = clips.IsSet("noColorAxis:" + name) || model.Axes.Any(a => a.Key == yAxisKey);

                // axes
                if (!noAxis)
                {
                    var yAxis = new OxyPlot.Axes.LinearAxis() { Key = yAxisKey, Title = "Gene", Position = onRight ? OxyPlot.Axes.AxisPosition.Right : OxyPlot.Axes.AxisPosition.Left, StartPosition = 1, EndPosition = 0 };
                    model.Axes.Add(yAxis);
                }

                if (!noColorAxis)
                {
                    var colorAxis = new OxyPlot.Axes.LinearColorAxis() { Key = colorAxisKey, Title = (diff ? "Change in " : "") + title, Position = OxyPlot.Axes.AxisPosition.Right };
                    colorAxis.Palette = OxyPalettes.Gray(100);
                    model.Axes.Add(colorAxis);
                }

                var s = new OxyPlot.Series.RectangleSeries();
                s.Title = title;
                s.ColorAxisKey = colorAxisKey;
                s.XAxisKey = xAxisKey;
                s.YAxisKey = yAxisKey;
                s.RenderInLegend = renderInLegend;
                s.CanTrackerInterpolatePoints = false;

                for (int i = 0; i < samplerMetrics.Count; i++)
                {
                    var samplerMetric = samplerMetrics[i];
                    var samples = TracePlotting.Sample(traceInfo, resolution, samplerMetric, epochsx, offset);
                    DataPoint last = DataPoint.Undefined;
                    foreach (var sample in samples)
                    {
                        if (last.IsDefined() && sample.IsDefined())
                        {
                            var rect = new RectangleItem(last.X, sample.X, i, i + 1, sample.Y);
                            s.Items.Add(rect);
                        }

                        last = sample;
                    }
                }

                model.Series.Add(s);
                return new OxyPlot.Series.Series[] { s };
            }
            else
            {
                // axes
                if (!noAxis)
                {
                    var yAxis = new OxyPlot.Axes.LinearAxis() { Key = yAxisKey, Title = (diff ? "Change in " : "") + title, Position = onRight ? OxyPlot.Axes.AxisPosition.Right : OxyPlot.Axes.AxisPosition.Left };
                    model.Axes.Add(yAxis);
                }

                // lines
                List<OxyPlot.Series.Series> series = new List<OxyPlot.Series.Series>();
                for (int i = 0; i < samplerMetrics.Count; i++)
                {
                    bool first = i == 0;

                    var lineColour = colour.HasValue ? colour.Value : colours[i];
                    var samplerMetric = samplerMetrics[i];
                    var s = TracePlotting.PlotTraceLine(model, traceInfo, xAxisKey, yAxisKey, title, resolution, epochsx, samplerMetric, lineColour, strokeThickness, offset, plotType, markerType, markerColour, lineStyle, injectBreaks, brokenLineStyle);
                    s.RenderInLegend = first && renderInLegend;
                    series.Add(s);
                }

                return series;
            }
        }
    }

    public class Tracee2DProvider<T> : ICliTraceeProvider<T> where T : IIndividual<T>
    {
        public static int DenseSize(TraceInfo<DenseIndividual> traceInfo) => traceInfo.TraceWholeSamples.First().Judgements[0].Individual.Genome.Size;

        public Tracee2DProvider(string prefix, string defaultTitle, Func<int, int, Func<WholeSample<T>, double>> sampler2D, Func<TraceInfo<T>, int> defaultDim1, Func<TraceInfo<T>, int> defaultDim2, OxyColor c00, OxyColor c10, OxyColor c01, OxyColor c11, double strokeThickness)
        {
            Prefix = prefix;
            DefaultTitle = defaultTitle;
            Sampler2D = sampler2D;
            DefaultDim1 = defaultDim1;
            DefaultDim2 = defaultDim2;

            C00 = c00;
            C10 = c10;
            C01 = c01;
            C11 = c11;
            StrokeThickness = strokeThickness;
        }

        public string Prefix { get; }
        public string DefaultTitle { get; }
        public Func<int, int, Func<WholeSample<T>, double>> Sampler2D { get; }
        public Func<TraceInfo<T>, int> DefaultDim1 { get; }
        public Func<TraceInfo<T>, int> DefaultDim2 { get; }

        public OxyColor C00 { get; }
        public OxyColor C10 { get; }
        public OxyColor C01 { get; }
        public OxyColor C11 { get; }
        public double StrokeThickness { get; }

        public void PlotStuff(TextWriter console, PlotModel model, string name, CliParams clips, TraceInfo<T> traceInfo, bool epochsx, double offset)
        {
            var strokeThickness = clips.Get("strokeThickness", double.Parse, StrokeThickness);
            var diff = clips.Get("diff:" + name, bool.Parse, clips.IsSet("diff"));
            var title = clips.Get("title:" + name, DefaultTitle);

            int dim1 = DefaultDim1(traceInfo);
            int dim2 = DefaultDim1(traceInfo);
            int[] lines = Enumerable.Range(0, dim1 * dim2).ToArray();

            if (clips.IsSet("keep:" + name))
            {
                IEnumerable<int> keep = clips.Get("keep:" + name, CliPlotHelpers.ParseIntList);
                lines = CliPlotHelpers.Keep(lines, keep, -1);
            }

            List<OxyColor> colours = new List<OxyColor>();
            List<Func<IEnumerable<WholeSample<T>>, double>> samplerMetrics = new List<Func<IEnumerable<WholeSample<T>>, double>>();

            OxyColor[] colors2d = TrajectoryPlotting.Colours2D(dim1, C00, C10, C01, C11);

            // lines
            for (int i = 0; i < dim1; i++)
            {
                for (int j = 0; j < dim2; j++)
                {
                    if (lines[i * dim1 + j] < 0) // not-kept
                        continue;

                    var sampler = Sampler2D(i, j);
                    var samplerMetric = TracePlotting.AverageMetric(traceInfo, diff, sampler);

                    samplerMetrics.Add(samplerMetric);
                    colours.Add(colors2d[i * dim1 + j]);
                }
            }

            Tracee1DProvider<T>.PlotStuffs(model, name, title, clips, traceInfo, epochsx, offset, samplerMetrics, colours, strokeThickness);
        }
    }

    public class CliTraceePlotter : ICliPlotter
    {
        private class PlotOptions
        {
            public static readonly PlotOptions DefaultOff = new PlotOptions(false, false);
            public static readonly PlotOptions DefaultOn = new PlotOptions(true, false);

            public PlotOptions(bool enabled, bool leftAxis, LineStyle lineStyle = LineStyle.Solid, TrajectoryPlotType plotType = TrajectoryPlotType.Line, MarkerType markerType = MarkerType.None)
            {
                Enabled = enabled;
                LeftAxis = leftAxis;
                LineStyle = lineStyle;
                PlotType = plotType;
                MarkerType = markerType;
            }

            public bool Enabled { get; }
            public bool LeftAxis { get; }
            public LineStyle LineStyle { get; }
            public TrajectoryPlotType PlotType { get; }
            public MarkerType MarkerType { get; }

            public static readonly Func<string, PlotOptions> DefaultParser = Parser(DefaultOn);

            public static Func<string, PlotOptions> Parser(PlotOptions template)
            {
                return str =>
                {
                    if (str == null)
                        return template;

                    if (str.Length == 0)
                        return DefaultOff; // off

                    bool leftAxis = str.Contains('l') ? true : str.Contains('r') ? false : template.LeftAxis;
                    LineStyle lineStyle = str.Contains('d') ? LineStyle.Dash : LineStyle.Solid;
                    TrajectoryPlotType plotType = str.Contains('s') ? TrajectoryPlotType.Scatter : TrajectoryPlotType.Line;
                    MarkerType markerType =
                        str.Contains("*") ? MarkerType.Star :
                        str.Contains("x") ? MarkerType.Cross :
                        str.Contains("o") ? MarkerType.Circle :
                        str.Contains("v") ? MarkerType.Diamond :
                        plotType == TrajectoryPlotType.Line ? MarkerType.None :
                        MarkerType.Diamond;

                    return new PlotOptions(true, leftAxis, lineStyle, plotType, markerType);
                };
            }
        }

        public string Prefix => "tracee";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            var traceInfo = TraceInfo<DenseIndividual>.Load(filename);

            if (clips.IsSet("gswitch"))
            {
                Func<WholeSample<DenseIndividual>, WholeSample<DenseIndividual>, bool> cutter = null;

                // cut-period in generations
                int cutPeriod = clips.Get("cutPeriod", Misc.ParseInt, -1);
                long cutStart = clips.Get("start", Misc.ParseLong, traceInfo.TraceWholeSamples.First().Generations);
                long cutEnd = clips.Get("end", Misc.ParseLong, traceInfo.TraceWholeSamples.Last().Generations);

                if (clips.IsSet("cutPeriod"))
                {
                    cutter = (a, b) => (a.Generations - cutStart) / cutPeriod != (b.Generations - cutStart) / cutPeriod;
                }
                else
                {
                    // cut by target if possible
                    var allTargets = traceInfo.TraceWholeSamples.Select(ws => ws.Target).Distinct().ToArray();
                    if (allTargets.Length > 1)
                    {
                        cutter = (a, b) => a.Target != b.Target;
                    }
                    else
                    {
                        throw new Exception("Cannot cut by target; only one target used: qualify cutperiod");
                    }
                }

                var extract = traceInfo.ExtractGenerations(cutStart, cutEnd);
                var cuts = Misc.CutOn(extract.TraceWholeSamples, cutter).Select(c => c.ToArray()).ToArray(); // each entry is a single 'switch'

                if (extract.TraceWholeSamples[0].Judgements.Count != 1)
                    console.WriteLine("gswitch on population without exactly one individual is meaningless");

                int size = extract.TraceWholeSamples[0].Judgements[0].Individual.Genome.InitialState.Count;
                List<int>[] delays = Misc.Create(size, i => new List<int>());

                foreach (var cut in cuts)
                {
                    // first entry is always 'from the previous target'
                    var ws0 = cut[0];
                    var g0 = ws0.Judgements[0].Individual.Genome.InitialState;
                    var generations0 = ws0.Generations;

                    for (int i = 0; i < size; i++)
                    {
                        var change = cut.FirstOrDefault(ws => ws.Judgements[0].Individual.Genome.InitialState[i] != g0[i]);

                        if (change == null)
                            continue;

                        var delay = (int)(change.Generations - generations0);
                        delays[i].Add(delay);
                    }

                }

                bool needlesslyWordyTraitLabels = clips.Get("needlesslyWordyTraitLabels", bool.Parse, false);
                var plot = new OxyPlot.PlotModel() { Title = title };

                var traitAxis = new OxyPlot.Axes.CategoryAxis() { Title = "Traits", Key = "traits" };
                var countAxis = new OxyPlot.Axes.LinearAxis() { Title = "Count", Key = "count", Position = OxyPlot.Axes.AxisPosition.Left, MinimumPadding = 0 };
                var delayAxis = new OxyPlot.Axes.LinearAxis() { Title = "Delay", Key = "delay", Position = OxyPlot.Axes.AxisPosition.Right, MinimumPadding = 0 };

                plot.Axes.Add(traitAxis);
                plot.Axes.Add(countAxis);
                plot.Axes.Add(delayAxis);

                var countSeries = new OxyPlot.Series.ColumnSeries() { Title = "Count", XAxisKey = traitAxis.Key, YAxisKey = countAxis.Key };
                var delaySeries = new OxyPlot.Series.ErrorColumnSeries() { Title = "Mean Delay", XAxisKey = traitAxis.Key, YAxisKey = delayAxis.Key };
                for (int i = 0; i < size; i++)
                {
                    traitAxis.Labels.Add(needlesslyWordyTraitLabels ? $"Trait {i}" : "" + i);
                    countSeries.Items.Add(new OxyPlot.Series.ColumnItem(delays[i].Count, i));
                    if (delays[i].Count > 0)
                    {
                        var stats = MathNet.Numerics.Distributions.Normal.Estimate(delays[i].Select(d => (double)d));
                        var confidenceWidth = 1.96 /* gaussian, 95% */ * stats.StdDev / Math.Sqrt(delays[i].Count);
                        delaySeries.Items.Add(new OxyPlot.Series.ErrorColumnItem(stats.Mean, confidenceWidth, i));
                    }
                }
                plot.Series.Add(countSeries);
                plot.Series.Add(delaySeries);

                plot.LegendPlacement = LegendPlacement.Outside;
                plot.LegendOrientation = LegendOrientation.Horizontal;
                plot.LegendPosition = LegendPosition.TopCenter;
                plot.IsLegendVisible = true;

                return plot;
            }

            if (clips.IsSet("generation"))
            {
                // plot last genome in epoch
                int generation = clips.Get("generation", Misc.ParseInt);

                var samples = traceInfo.TraceWholeSamples.Where(ws => ws.Generations == generation).ToArray();

                if (samples.Length == 0)
                    throw new Exception("No samples for generation: " + generation);

                int subSampleIndex = clips.Get("subsampleindex", int.Parse, samples.Length - 1); // default to last

                var individual = samples[subSampleIndex].Judgements.First().Individual;

                var thing = clips.Get("thing", null);

                if (thing == "ps")
                { // we use the known ps instead of generating one
                    var configFile = clips.Get("config", clips.Get("expfile", null));
                    var config = configFile == null ? null : CliPlotHelpers.LoadExperimentConfig(configFile);
                    
                    return CliGenomePlotter.PlotExpressionVector(clips, individual.Phenotype.Vector, title ?? "Phenotypic Expression", config);
                }
                else
                {
                    return CliGenomePlotter.PlotDenseGenome(console, clips, individual.Genome, title, null);
                }
            }
            else
            {
                return PlotTracee(console, clips, traceInfo, title, false);
            }
        }

        // this will do for now
        public static readonly List<ICliTraceeProvider<DenseIndividual>> DefaultDenseCliTraceeProviders = new List<ICliTraceeProvider<DenseIndividual>>()
        {
            new Tracee1DProvider<DenseIndividual>("fitness:", "Fitness", _ => ws => ws.Judgements.Average(ij => ij.Judgement.CombinedFitness), _ => 1, OxyColors.Red, OxyColors.Red, 2),
            new Tracee1DProvider<DenseIndividual>("benefit:", "Benefit", _ => ws => ws.Judgements.Average(ij => ij.Judgement.Benefit), _ => 1, OxyColors.OrangeRed, OxyColors.OrangeRed, 2),
            new Tracee1DProvider<DenseIndividual>("cost:", "Cost", _ => ws => ws.Judgements.Average(ij => ij.Judgement.Cost), _ => 1, OxyColors.Orange, OxyColors.Orange, 2),
            new Tracee1DProvider<DenseIndividual>("ptraits:", "Phenotypic Traits", i => ws => ws.Judgements.Average(ij => ij.Individual.Phenotype[i]), Tracee1DProvider<DenseIndividual>.DenseSize, OxyColors.DarkBlue, OxyColors.LightBlue, 2),
            new Tracee1DProvider<DenseIndividual>("gtraits:", "Genotypic Traits", i => ws => ws.Judgements.Average(ij => ij.Individual.Genome.InitialState[i]), Tracee1DProvider<DenseIndividual>.DenseSize, OxyColors.DarkViolet, OxyColors.Violet, 2),
            new Tracee2DProvider<DenseIndividual>("regcoefs:", "Regulation Coefficients", (i, j) => ws => ws.Judgements.Average(ij => ij.Individual.Genome.TransMat[i, j]), Tracee1DProvider<DenseIndividual>.DenseSize, Tracee1DProvider<DenseIndividual>.DenseSize, OxyColors.DarkCyan, OxyColors.LightCyan, OxyColors.DarkGreen, OxyColors.LightGreen, 2),
            new Tracee1DProvider<DenseIndividual>("gsum:", "Genotypic Trait Sum", i => ws => ws.Judgements.Average(ij => ij.Individual.Genome.InitialState.Sum()), ti => 1, OxyColors.Purple, OxyColors.Purple, 2),
            new Tracee1DProvider<DenseIndividual>("sudoku:", "Sudoku Score", i => ws => ws.Judgements.Average(ij => Sudoku.LocateConflicts(Sudoku.RenderSolution(ij.Individual.Phenotype.Vector, out _)).Count()), ti => 1, OxyColors.Brown, OxyColors.SaddleBrown, 2),
            new HuskyProvider("huskyness:", "Degree of Hierarchy", OxyColors.DarkSlateGray, OxyColors.SlateGray, 2),
            new ModuleIndependenceProvider("moduleindependence:", "Degree of Module Independence", OxyColors.Teal, OxyColors.DarkTurquoise, 2),
            new DeltaBProvider("deltab:", "db/dB", OxyColors.DarkCyan, OxyColors.LightCyan, OxyColors.DarkGreen, OxyColors.LightGreen, 2),
        };

        public CliTraceePlotter(IEnumerable<ICliTraceeProvider<DenseIndividual>> denseCliTraceeProviders)
        {
            Providers = new List<ICliTraceeProvider<DenseIndividual>>(denseCliTraceeProviders);
        }

        public List<ICliTraceeProvider<DenseIndividual>> Providers { get; }

        public PlotModel PlotTracee(TextWriter console, CliParams clips, TraceInfo<DenseIndividual> traceInfo, string title, bool epochsx)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("The trace plotter can plot a combination of things from a trace.\n" +
                    "All things are mean samples\n" +
                    "Here are the options:\n" +
                    " recoefs -> plots reg-coefs\n" +
                    " ptraits -> plots phenotypic trait expression\n" +
                    " gtraits -> plots genotypic trait expression\n" +
                    " fitness -> plots fitness (by default, this is plotted, and on the right-hand axis)\n" +
                    "You can change the start and end generation.\n" +
                    "You can downsample the data (i.e. reduce resolution without average; performed after target filtering)\n" +
                    "You can change the resolution (number of samples averaged per data-point (i.e. higher resolution gives courser plot))\n" +
                    "You can configure target change indicators targetlines=line/labels (labels by default)\n" +
                    "You can switch to plotting the change of metrics with diff\n" +
                    "You can specify the targets to look at, qualified by index");
            }

            if (epochsx)
            {
                int start = clips.Get("start", Misc.ParseInt, traceInfo.TraceWholeSamples.First().Epoch);
                int end = clips.Get("end", Misc.ParseInt, traceInfo.TraceWholeSamples.Last().Epoch);

                traceInfo = traceInfo.ExtractEpochs(start, end);
            }
            else
            {
                long start = clips.Get("start", Misc.ParseLong, traceInfo.TraceWholeSamples.First().Generations);
                long end = clips.Get("end", Misc.ParseLong, traceInfo.TraceWholeSamples.Last().Generations);

                traceInfo = traceInfo.ExtractGenerations(start, end);
            }

            // boundary dropping
            var dropMode = clips.Get("dropMode", null);
            if (string.IsNullOrEmpty(dropMode) || dropMode.Equals("none", StringComparison.InvariantCultureIgnoreCase))
            {
                // nix
            }
            else if (dropMode.Equals("dropFirst", StringComparison.InvariantCultureIgnoreCase))
            {
                traceInfo = traceInfo.DropEpochBoundaries(true);
            }
            else if (dropMode.Equals("dropLast", StringComparison.InvariantCultureIgnoreCase))
            {
                traceInfo = traceInfo.DropEpochBoundaries(false);
            }
            else
            {
                throw new Exception("Unrecognised dropMode: " + dropMode + "\nConsider one of: - dropFist\n - dropLast\n - none");
            }

            int downsample = clips.Get("downsample", Misc.ParseInt, 1); // skipping
            int resolution = clips.Get("resolution", Misc.ParseInt, 1); // averaging

            double offset = 0.0;
            if (clips.IsSet("retime"))
            {
                long retime = clips.Get("retime", Misc.ParseLong);
                offset = retime - (epochsx ? traceInfo.TraceWholeSamples[0].Epoch : traceInfo.TraceWholeSamples[0].Generations);
            }

            string targetlines = clips.Get("targetlines", "labels");
            string epochlines = clips.Get("epochlines", "none");
            string[] targets = clips.Get("targets", s => s.Split(new[] { ',', ';' }), null);

            if (targets != null && (targets.Length == 0 || (targets.Length == 1 && targets[0] == "")))
            { // use the first one we see, whatever it is
                targets = new[] { traceInfo.TraceWholeSamples[0].Target.FriendlyName };
                console.WriteLine("Using default target: " + targets[0]);
            }

            if (targets != null)
            {
                HashSet<string> ttable = new HashSet<string>(targets, StringComparer.InvariantCultureIgnoreCase);
                traceInfo = traceInfo.Filter(ws => ttable.Contains(ws.Target.FriendlyName));
            }
            else if (epochsx) // we don't want targets when processing at generation level
            {
                console.WriteLine("No targets specified. Available targets include: " + string.Join(", ", traceInfo.TraceWholeSamples.Select(ws => ws.Target.FriendlyName).Distinct().Take(5)));
            }

            if (downsample > 1)
            {
                traceInfo = traceInfo.DownSample(downsample, true);
            }

            string evalexpfile = clips.Get("evalexpfile", null);
            if (evalexpfile != null)
            {
                var evalExp = PopulationExperiment<DenseIndividual>.Load(evalexpfile);
                var projectionMode = clips.Get("projectionmode", CliProject.ParseProjectionMode);
                bool noNextExposure = clips.Get("noNextExposure", bool.Parse, false);

                var projector = new BasicExtractorWholeSampleProjectorPreparer(projectionMode, noNextExposure).PrepareProjector<DenseIndividual>(evalExp);
                var rand = new MathNet.Numerics.Random.MersenneTwister();
                var context = new ModelExecutionContext(rand);

                var projected = WholeSampleProjectionHelpers.Project(console, context, traceInfo.TraceWholeSamples, projector, true, true);
                traceInfo = new TraceInfo<DenseIndividual>(projected);
            }

            string xAxisKey = clips.Get("xaxiskey", "generations");
            bool logX = clips.Get("logX", bool.Parse, false);

            bool fitnessOnByDefault = true;
            var plot = TracePlotting.PreparePlot(title, true, epochsx, xAxisKey, logX);

            // provider plotting
            if (clips.IsSet("providers"))
            {
                var providerStrings = clips.Get("providers").Split(';');
                foreach (var ps in providerStrings)
                {
                    var provider = Providers.FirstOrDefault(p => ps.StartsWith(p.Prefix));
                    if (provider == null)
                    {
                        console.WriteLine("No provider available for " + ps);
                    }
                    else
                    {
                        provider.PlotStuff(console, plot, ps.Substring(provider.Prefix.Length), clips, traceInfo, epochsx, offset);
                    }
                }
                fitnessOnByDefault = false;
            }

            // legacy/specialist plotting
            double? ymin = clips.Get("ymin", s => (double?)double.Parse(s), null);
            double? ymax = clips.Get("ymax", s => (double?)double.Parse(s), null);
            double? fitnessmin = clips.Get("fitnessmin", s => (double?)double.Parse(s), null);
            double? fitnessmax = clips.Get("fitnessmax", s => (double?)double.Parse(s), null);
            bool diff = clips.IsSet("diff");
            var injectBreaks = clips.Get("injectBreaks", bool.Parse, false);
            var brokenLineStyle = clips.Get("brokenLineStyle", CliPlotHelpers.ParseLineStyle, LineStyle.Dot);

            // TODO: would like to abstract these out... the problem is that ptraits and fitness don't require a DenseIndividual... and I can't think right now how to handle that
            // (real problem is that TraceInfo<T> doesn't have an abstract version without type info (e.g. ITraceInfo) which can pull IIndividuals)
            PlotOptions regcoefs = clips.Get("regcoefs", PlotOptions.DefaultParser, PlotOptions.DefaultOff);
            PlotOptions ptraits = clips.Get("ptraits", PlotOptions.DefaultParser, PlotOptions.DefaultOff);
            PlotOptions gtraits = clips.Get("gtraits", PlotOptions.DefaultParser, PlotOptions.DefaultOff);
            PlotOptions fitness = clips.Get("fitness", PlotOptions.Parser(new PlotOptions(fitnessOnByDefault, false)), new PlotOptions(fitnessOnByDefault, false));
            PlotOptions huskyness = clips.Get("huskyness", PlotOptions.DefaultParser, PlotOptions.DefaultOff);
            PlotOptions devtimedominance = clips.Get("devtimedominance", PlotOptions.DefaultParser, PlotOptions.DefaultOff);

            if (regcoefs.Enabled)
                TracePlotting.PlotTraceRegCoegs(plot, traceInfo, resolution, ymin, ymax, regcoefs.LeftAxis, epochsx, diff, offset);
            if (ptraits.Enabled)
                TracePlotting.PlotPhenotypicTraceTraits(plot, traceInfo, resolution, ymin, ymax, ptraits.LeftAxis, epochsx, diff, offset, ptraits.PlotType, ptraits.MarkerType, injectBreaks, brokenLineStyle);
            if (gtraits.Enabled)
                TracePlotting.PlotGenotypicTraceTraits(plot, traceInfo, resolution, ymin, ymax, gtraits.LeftAxis, epochsx, diff, offset, gtraits.PlotType, gtraits.MarkerType, injectBreaks, brokenLineStyle);
            if (fitness.Enabled)
                TracePlotting.PlotTraceFitness(plot, traceInfo, resolution, fitnessmin, fitnessmax, fitness.LeftAxis, epochsx, diff, offset, fitness.LineStyle, fitness.PlotType, fitness.MarkerType);
            if (huskyness.Enabled || devtimedominance.Enabled)
            {
                int exampleN = traceInfo.TraceWholeSamples[0].Judgements[0].Individual.Genome.Size;

                var modules = CliPlotHelpers.GuessModules(console, clips, exampleN);
                var mts = modules.ToArray();

                if (huskyness.Enabled)
                {
                    TracePlotting.PlotTraceHuskyness(plot, traceInfo, mts, resolution, ymin, ymax, huskyness.LeftAxis, epochsx, diff, offset, huskyness.LineStyle, huskyness.PlotType, huskyness.MarkerType, injectBreaks, brokenLineStyle);
                }

                if (devtimedominance.Enabled)
                {
                    // need a config at a minimum
                    var configFile = clips.Get("config", clips.Get("expfile", null));
                    if (configFile != null)
                    {
                        ExperimentConfiguration config = CliPlotHelpers.LoadExperimentConfig(configFile);
                        int seed = clips.GetOrCreate("seed", int.Parse, () => CliExp.SeedSource.Next());
                        var rand = new MathNet.Numerics.Random.MersenneTwister(seed, false);

                        TracePlotting.PlotTraceDevTimeDominance(plot, rand, config.DevelopmentRules, traceInfo, mts, resolution, ymin, ymax, devtimedominance.LeftAxis, epochsx, diff, offset, devtimedominance.LineStyle, devtimedominance.PlotType, devtimedominance.MarkerType, injectBreaks, brokenLineStyle);
                    }
                    else
                    {
                        console.WriteLine("Require a config or expfile to compute dev-time dominance");
                    }
                }
            }

            var al = plot.Axes.Where(a => a.Position == OxyPlot.Axes.AxisPosition.Left);
            var ar = plot.Axes.Where(a => a.Position == OxyPlot.Axes.AxisPosition.Right);

            foreach (var axes in new[] { ar, al })
            {
                int ai = 1;
                foreach (var a in axes)
                {
                    a.PositionTier = ai++;
                }
            }

            if (!epochsx && (targetlines == null || targetlines == "line" || targetlines == "labels"))
                TracePlotting.AnnotateTraceTargetTransitions(plot, traceInfo, targetlines == "labels", offset);

            if (!epochsx && (epochlines == null || epochlines == "line" || epochlines == "labels"))
                TracePlotting.AnnotateTraceEpochsTransitions(plot, traceInfo, epochlines == "labels", offset);

            return plot;
        }
    }

    public class CliTracesPlotter : ICliPlotter
    {
        public string Prefix => "traces";

        public OxyPlot.PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("The traces plotter can plot one or more traces on the same graph\n" +
                    " - names (list of comma-separated names\n" +
                    " - resolution (plot sample period)\n" +
                    " - mean (flag, uses 'mean' rather than 'best' samples)" +
                    " - areas (flag, plots percentile areas rather than individual trajectories)\n" +
                    " - generation (plots a histogram for the given generation, rather than the whole trace\n" +
                    " - ymax/ymin");
            }

            double? ymin = clips.Get("ymin", s => (double?)double.Parse(s), null);
            double? ymax = clips.Get("ymax", s => (double?)double.Parse(s), null);
            
            string xAxisKey = clips.Get("xaxiskey", "x");
            string yAxisKey = clips.Get("yaxiskey", "y");

            var filenames = filename.Split(';');
            var tracesInfos = filenames.Select(fname => TracesInfo.Load(fname)).ToArray();
            bool mean = clips.IsSet("mean");
            var names = clips.Get("names", "").Split(new[] { ',', ';' });
            int resolution = clips.Get("resolution", Misc.ParseInt, 1);

            var strokeThickness = clips.Get("strokeThickness", double.Parse, 2.0);

            if (clips.IsSet("generation"))
            {
                // TODO: plotting
                long generation = clips.Get("generation", long.Parse);

                double[][] samples = mean
                    ? tracesInfos.Select(ti => ti.ExtractMeanSamples(generation)).ToArray()
                    : tracesInfos.Select(ti => ti.ExtractBestSamples(generation)).ToArray();
                
                // TODO: test this
                console.WriteLine("Pairwise Mann-whitney");
                for (int i = 0; i < samples.Length; i++)
                    for (int j = i + 1; j < samples.Length; j++)
                        console.WriteLine($"{i}, {j}:\t" + M4M.Stats.MannWhitneyU(samples[0], samples[1], TestType.TwoTailed).P);

                // not good
                return null;
            }
            else
            {
                bool logX = clips.Get("logX", bool.Parse, false);
                bool logY = clips.Get("logY", bool.Parse, false);

                var plot = TrajectoryPlotting.PrepareTrajectoriesPlot(title == "" ? (mean ? "Mean Fitnesses" : "Best Fitness") : title, "Generation", "Fitness", null, ymin, ymax, xAxisKey, yAxisKey, null, logX, logY, false);

                OxyColor[] colours = new[] { OxyColors.Red, OxyColors.Blue, OxyColors.Green, OxyColors.Purple, OxyColors.Goldenrod };
                colours = clips.Get("colors", s => s.Split(';').Select(CliPlotHelpers.ParseColour).ToArray(), colours);

                int tii = 0;
                foreach (var _tracesInfo in tracesInfos)
                {
                    long start = clips.Get("start", Misc.ParseLong, _tracesInfo.StartGeneration);
                    long end = clips.Get("end", Misc.ParseLong, _tracesInfo.EndGeneration);
                    var tracesInfo = _tracesInfo.Extract(start, end);

                    string targets = clips.Get("targets", "labels");

                    var colour = colours[tii % colours.Length]; // TODO: make this less terrible

                    var samples = mean ? tracesInfo.MeanFitnesses.ToArray() : tracesInfo.BestFitnesses.ToArray();

                    long retimeStart = clips.Get("retimeStart", long.Parse, tracesInfo.StartGeneration);

                    if (clips.IsSet("areas"))
                    {
                        var labelAreas = clips.Get("labelAreas", bool.Parse, true);
                        var cts = new ColourfulTraces(names[tii % names.Length], colour, samples, tracesInfo.SamplePeriod, retimeStart);
                        var wide = clips.Get("wide", str => str == null ? new[] { 0, 50 } : CliPlotHelpers.ParseStrictIntList(str), new[] { 10, 25, 50 });
                        EvolvabilityTraces.PlotArea(plot, cts, EvolvabilityTraces.SymetricTracePercentileInformations(cts.Name, cts.Colour, wide, labelAreas, strokeThickness), resolution);
                    }
                    else
                    {
                        TrajectoryPlotting.PlotTrajectories(plot, "Mean Fitness", samples, tracesInfo.SamplePeriod, retimeStart, colour, colour, strokeThickness, resolution, TrajectoryPlotType.Line, xAxisKey, yAxisKey);
                    }

                    if (tii == 0)
                    { // NOTE: these don't make a whole lot of sense for multiple traces
                        if (targets == null || targets == "line" || targets == "labels")
                            TracePlotting.AnnotateTracesTargetTransitions(plot, tracesInfo, targets == "labels");
                    }

                    tii++;
                }
                return plot;
            }
        }
    }

    public class CliTrajTracesPlotter : ICliPlotter
    {
        public string Prefix => "trajtraces";
        public OxyColor[] DefaultFixedColors = new[] { OxyColors.Red, OxyColors.Blue, OxyColors.Green, OxyColors.Purple, OxyColors.Goldenrod };
        public ITrajectoryColours DefaultColorGenerator = new TrajectoryColours1D(OxyColors.Blue, OxyColors.Red);

        public OxyColor[] DefaultColors(int count)
        {
            if (count <= DefaultFixedColors.Length)
                return DefaultFixedColors.Take(count).ToArray();
            else
                return DefaultColorGenerator.Colours(count);
        }

        public OxyPlot.PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("The traj traces plotter can plot one or more traj traces on the same graph\n" +
                    " - names (list of comma-separated names\n" +
                    " - resolution (plot sample period)\n" +
                    " - areas (flag, plots percentile areas rather than individual trajectories)\n" +
                    " - ymax/ymin");
            }
            
            int resolution = clips.Get("resolution", Misc.ParseInt, 1);

            var tracesFiles = clips.Get("files", s => s.Split(';'), new[] { filename });

            double? ymin = clips.Get("ymin", s => (double?)double.Parse(s), null);
            double? ymax = clips.Get("ymax", s => (double?)double.Parse(s), null);
            
            string xAxisKey = clips.Get("xaxiskey", "x");
            string yAxisKey = clips.Get("yaxiskey", "y");

            bool logX = clips.Get("logX", bool.Parse, false);
            bool logY = clips.Get("logY", bool.Parse, false);

            var strokeThickness = clips.Get("strokeThickness", double.Parse, 2.0);

            var keeper = CliPlotHelpers.Keeper(clips);
            var resampler = CliPlotHelpers.DefaultResampler(clips);
            void resample(ref double[][] trajectories, ref int samplePeriod, out int startTime, bool stripNulls)
            {
                keeper(ref trajectories, ref samplePeriod); // leaves nulls where things are no kept

                if (stripNulls)
                    trajectories = trajectories.Where(t => t != null).ToArray();

                resampler(ref trajectories, ref samplePeriod);

                var example = trajectories.First(t => t != null);

                int start = clips.Get("start", Misc.ParseInt, 0);
                int end = clips.Get("end", Misc.ParseInt, example.Length * samplePeriod);
                console.WriteLine(end);
                int skip = start / samplePeriod;
                int take = Math.Min((end + samplePeriod - start) / samplePeriod, example.Length - skip);
                trajectories = trajectories.Select(traj => traj?.Skip(skip).Take(take).ToArray()).ToArray();

                startTime = start;
            }

            if (clips.IsSet("generation") || clips.IsSet("epoch"))
            {
                var epochs = clips.Get("epoch", clips.Get("generation", "-1")).Split(';').Select(Misc.ParseInt).ToArray();
                var colors = clips.Get("color", s => s.Split(';').Select(CliPlotHelpers.ParseColour).ToArray(), DefaultColors(epochs.Length));

                var labels = clips.Get("label", s => s.Split(';'), Enumerable.Range(0, tracesFiles.Length).Select(i => ""+i).ToArray());
                var catTitle = clips.Get("catTitle", "Category");

                if (clips.IsSet("histogram"))
                {
                    var plot = HistogramPlotting.PrepareDodgyHistogram(title, $"(qualify title:{xAxisKey})", "Occurances", xAxisKey, yAxisKey);
                    var yAxis = plot.Axes.First(a => a.Key == yAxisKey);
                    if (ymin != null)
                        yAxis.Minimum = ymin.Value;
                    if (ymax != null)
                        yAxis.Maximum = ymax.Value;

                    var epoch = epochs.Single();

                    var samples = new List<double[]>();

                    for (int i = 0; i < tracesFiles.Length; i++)
                    {
                        var traj = Analysis.LoadTrajectories(tracesFiles[i], out var samplePeriod);
                        var idx = epoch / samplePeriod;
                        var sample = Analysis.ExtractArray(traj, idx);

                        samples.Add(sample);
                    }

                    double min = clips.Get("min", double.Parse, samples.Min(s => s.Min()));
                    double max = clips.Get("max", double.Parse, samples.Max(s => s.Max()));
                    int binCount = clips.Get("binCount", int.Parse, 10);

                    HistogramPlotting.DodgyHistogram(plot, samples, colors, min, max, binCount);

                    return plot;
                }
                else if(clips.IsSet("MWU"))
                {
                    // matrix of P-values
                    var plot = new PlotModel() { Title = title };

                    var samples = new List<double[]>();

                    for (int i = 0; i < tracesFiles.Length; i++)
                    {
                        var subsamples = new List<double>();

                        foreach (var epoch in epochs)
                        {
                            var traj = Analysis.LoadTrajectories(tracesFiles[i], out var samplePeriod);
                            var idx = epoch / samplePeriod;
                            subsamples.AddRange(Analysis.ExtractArray(traj, idx));
                        }

                        samples.Add(subsamples.ToArray());
                        console.WriteLine($"SubSamples for {catTitle} = {labels[i]}");
                        console.WriteLine(string.Join(",", subsamples));
                        console.WriteLine($"Distincts for {catTitle} = {labels[i]}");
                        console.WriteLine(string.Join(",", subsamples.Distinct()));
                    }

                    int scount = samples.Count;
                    var mat = new double[scount, scount];

                    var testType = clips.IsSet("twotailed") ? TestType.TwoTailed : TestType.UpperTailed;
                    var showu = clips.IsSet("showu");

                    // populate mat with p-values from MWU
                    for (int i = 0; i < scount; i++)
                    {
                        for (int j = 0; j < scount; j++)
                        {
                            if (showu)
                            {
                                mat[i, j] = Stats.MannWhitneyU(samples[i], samples[j], testType).U;
                            }
                            else
                            {
                                mat[i, j] = Math.Log10(Stats.MannWhitneyU(samples[i], samples[j], testType).P);
                            }
                        }
                    }

                    var ms = new HeatMapSeries()
                    {
                        CoordinateDefinition = HeatMapCoordinateDefinition.Center,
                        Data = mat,
                        X0 = 0,
                        X1 = scount - 1,
                        Y0 = 0,
                        Y1 = scount - 1,
                        Interpolate = false,
                        LabelFontSize = 0.25,
                    };

                    var cx = new CategoryAxis() { Position = AxisPosition.Bottom, Title = catTitle };
                    cx.Labels.AddRange(labels);

                    var cy = new CategoryAxis() { Position = AxisPosition.Left, Title = catTitle, StartPosition = 1, EndPosition = 0 };
                    cy.Labels.AddRange(labels);

                    var coloraxis = new OxyPlot.Axes.LinearColorAxis() { Title = showu ? "U" : "p-Value", Position = OxyPlot.Axes.AxisPosition.Right, Palette = OxyPalettes.Gray(10), Key = "color", InvalidNumberColor = OxyColors.Pink };

                    plot.Axes.Add(cx);
                    plot.Axes.Add(cy);
                    plot.Axes.Add(coloraxis);
                    plot.Series.Add(ms);

                    if (epochs.Length > 1)
                    {
                        console.WriteLine("Warning! Using multiple epochs violates the assumption of sample independence! Don't do it!");
                    }

                    var f = new[] { 14.8, 7.3, 5.6, 6.3, 9, 4.2, 10.6, 12.5, 12.9, 16.1, 11.4, 2.7 };
                    var t = new[] { 12.7, 14.2, 12.6, 2.1, 17.7, 11.8, 16.9, 7.9, 16, 10.6, 5.6, 5.6, 7.6, 11.3, 8.3, 6.7, 3.6, 1, 2.4, 6.4, 9.1, 6.7, 18.6, 3.2, 6.2, 6.1, 15.3, 10.6, 1.8, 5.9, 9.9, 10.6, 14.8, 5, 2.6, 4 };

                    console.WriteLine(Stats.MannWhitneyU(f, t, TestType.LowerTailed));
                    console.WriteLine(Stats.MannWhitneyU(f, t, TestType.UpperTailed));
                    console.WriteLine(Stats.MannWhitneyU(f, t, TestType.TwoTailed));

                    return plot;
                }
                else if (clips.IsSet("bar"))
                {

                }
                else // box plots
                {
                    var plot = BoxPlotting.PrepareBoxPlot(labels, catTitle, $"(qualify title:{yAxisKey})", xAxisKey, yAxisKey);

                    int ei = 0;
                    foreach (var epoch in epochs)
                    {
                        var samples = new List<double[]>();

                        for (int i = 0; i < tracesFiles.Length; i++)
                        {
                            var traj = Analysis.LoadTrajectories(tracesFiles[i], out var samplePeriod);
                            var idx = epoch / samplePeriod;
                            var sample = Analysis.ExtractArray(traj, idx);
                            
                            samples.Add(sample);
                        }

                        var bp = BoxPlotting.BoxPlot(plot, samples);
                        
                        var color = colors[ei++];
                        bp.Stroke = color;
                    }

                    return plot;
                }
            }

            if (clips.IsSet("MWU"))
            {
                if (tracesFiles.Length != 2)
                {
                    console.WriteLine("Mann-Whitney U over a range only works with exactly 2 traces");
                    return null;
                }

                // assume 2
                
                var traj1 = Analysis.LoadTrajectories(tracesFiles[0], out var samplePeriod1);
                var traj2 = Analysis.LoadTrajectories(tracesFiles[1], out var samplePeriod2);
                resample(ref traj1, ref samplePeriod1, out int start, true);
                resample(ref traj2, ref samplePeriod2, out start, true);

                if (samplePeriod1 != samplePeriod2)
                    console.WriteLine("Sample periods do not match.");

                var samples1 = Misc.TrajectoriesToSamples(traj1);
                var samples2 = Misc.TrajectoriesToSamples(traj2);

                List<double> mwu = new List<double>();
                for (int i = 0; i < traj1[0].Length; i += resolution)
                    mwu.Add(M4M.Stats.MannWhitneyU(samples1[i], samples2[i], TestType.TwoTailed).P);

                var plot = TrajectoryPlotting.PlotTrajectories(title, "Epochs", "Two-Tailed P-value", null, "", new [] { mwu.ToArray() }, samplePeriod1 * resolution, start, ymin, ymax, OxyColors.Green, null, 2, 1, TrajectoryPlotType.Scatter, xAxisKey, yAxisKey, null, logX, logY, false);
                var ls = (OxyPlot.Series.ScatterSeries)plot.Series.First();

                if (clips.IsSet("pvaluethreshold"))
                {
                    // nightmare inducing 'significance' colouring (makes little sense if you ask me)
                    var pvaluethreshold = clips.Get("pvaluethreshold", s => s == null ? 0.05 : double.Parse(s));
                    foreach (var sp in ls.Points)
                        sp.Value = sp.Y;
                    var ca = new OxyPlot.Axes.RangeColorAxis() { Minimum = 0.0, Maximum = 1.0, LowColor = OxyColors.DarkGreen, HighColor = OxyColors.DarkRed, Key = "pvalue_ca" };
                    ca.AddRange(0.0, pvaluethreshold, OxyColors.DarkGreen);
                    ca.AddRange(pvaluethreshold, 1.0, OxyColors.DarkRed);
                    ls.ColorAxisKey = ca.Key;
                    ls.MarkerFill = OxyColors.Automatic;
                    plot.Axes.Add(ca);
                }

                return plot;
            }
            else
            {
                var labels = clips.Get("label", s => s.Split(';'), Enumerable.Range(0, tracesFiles.Length).Select(i => ""+i).ToArray());
                var colors = clips.Get("color", s => s.Split(';').Select(CliPlotHelpers.ParseColour).ToArray(), DefaultColors(tracesFiles.Length));

                var plot = TrajectoryPlotting.PrepareTrajectoriesPlot(title, "Generation", $"(Qualify title:{yAxisKey})", null, ymin, ymax, xAxisKey, yAxisKey, null, logX, logY, false);

                for (int i = 0; i < tracesFiles.Length; i++)
                {
                    bool lines = clips.IsSet("lines");

                    var traj = Analysis.LoadTrajectories(tracesFiles[i], out var samplePeriod);
                    resample(ref traj, ref samplePeriod, out int start, lines == true);
                    var cts = new ColourfulTraces(labels[i], colors[i], traj, samplePeriod, start);
   
                    if (lines)
                    {
                        TrajectoryPlotting.PlotTrajectories(plot, "Mean Fitness", traj, samplePeriod * resolution, start, cts.Colour, cts.Colour, strokeThickness, resolution, TrajectoryPlotType.Line, xAxisKey, yAxisKey);
                    }
                    else // areas
                    {
                        var labelAreas = clips.Get("labelAreas", bool.Parse, true);
                        var wide = clips.Get("wide", str => str == null ? new[] { 0, 50 } : CliPlotHelpers.ParseStrictIntList(str), new[] { 10, 25, 50 });
                        EvolvabilityTraces.PlotArea(plot, cts, EvolvabilityTraces.SymetricTracePercentileInformations(cts.Name, cts.Colour, wide, labelAreas, strokeThickness), resolution);
                    }
                }

                return plot;
            }
        }
    }

    public class CliRcsPlotter : ICliPlotter
    {
        private static readonly IReadOnlyList<ICliTrajectoryAnnotater> DefaultAnnotators = new ICliTrajectoryAnnotater[] { new CliTrajectoryDtmTransitionAnnotater(), new CliTrajectoryHuskynessTransitionAnnotater(), new CliTrajectoryModuleIndependenceTransitionAnnotater() };
        private static readonly CliTrajectoryPlotter RcsTrajectoryPlotterGreen = new CliTrajectoryPlotter("rcs", "Epochs", "Regulation Coefficient", "Regulation Coefficients", new TrajectoryColours2D(OxyColors.DarkCyan, OxyColors.LightCyan, OxyColors.DarkGreen, OxyColors.LightGreen), 2.0, DefaultAnnotators);
        private static readonly CliTrajectoryPlotter RcsTrajectoryPlotter = new CliTrajectoryPlotter("rcs", "Epochs", "Regulation Coefficient", "Regulation Coefficients", new TrajectoryColours1D(null, null), 2.0, DefaultAnnotators);

        public string Prefix => "rcs";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            if (clips.IsSet("epoch"))
            {
                int samplePeriod;
                var trajectories = Analysis.LoadTrajectories(filename, out samplePeriod);
                samplePeriod = clips.Get("samplePeriod", Misc.ParseInt, samplePeriod);
                if (samplePeriod == -1)
                {
                    samplePeriod = 1; // default
                    console.WriteLine("SamplePeriod unqualified");
                }

                int epoch = clips.Get("epoch", Misc.ParseInt);

                int size = (int)Math.Round(Math.Sqrt(trajectories.Length));
                int index = epoch / samplePeriod;
                if (index * samplePeriod != epoch)
                    console.WriteLine("Plotting closest available epoch: " + (index * samplePeriod));

                // plot a genome, not trjectories
                var transMat = Analysis.ExtractMatrix(trajectories, index, size, size);

                return CliGenomePlotter.PlotDtm(clips, transMat, title);
            }
            else
            {
                if (clips.IsSet("green"))
                    return RcsTrajectoryPlotterGreen.Plot(console, clips, filename, title);
                else
                    return RcsTrajectoryPlotter.Plot(console, clips, filename, title);
            }
        }
    }

    public interface ICliTrajectoryAnnotater
    {
        string Key { get; }
        bool Default { get; }
        /// <summary>
        /// Plots the annotations
        /// </summary>
        /// <param name="console">The reporting console</param>
        /// <param name="plot">The plot model to modify</param>
        /// <param name="clips">CliParams</param>
        /// <param name="trajectories">The Rcs Trajectories being plotted</param>
        /// <param name="samplePeriod">The trajectories sample period</param>
        /// <param name="startTime">Plotting start time</param>
        /// <param name="xAxisKey">Temporal X-Axis Key</param>
        /// <param name="yAxisKey">Rcs Y-Axis Key</param>
        void AnnotateTrajectories(TextWriter console, PlotModel plot, CliParams clips, double[][] trajectories, int samplePeriod, long startTime, string temporalAxisKey, string rcsAxisKey);
    }

    public class CliTrajectoryDtmTransitionAnnotater : ICliTrajectoryAnnotater
    {
        public CliTrajectoryDtmTransitionAnnotater(double thresholdFactor = 10.0)
        {
            ThresholdFactor = thresholdFactor;
        }

        public string Key => "dtminfo";
        public bool Default => false;

        public double ThresholdFactor { get; }

        public void AnnotateTrajectories(TextWriter console, PlotModel plot, CliParams clips, double[][] trajectories, int samplePeriod, long startTime, string xAxisKey, string yAxisKey)
        {
            foreach (var tc in DtmTimeClassification.ClassifyTrajectories(trajectories, samplePeriod, startTime, ThresholdFactor, 5))
            {
                var la = new LineAnnotation() { Type = LineAnnotationType.Vertical, X = tc.Time, Color = OxyColors.DarkKhaki };

                la.TextOrientation = AnnotationTextOrientation.AlongLine;
                
                // auto classification is not good enough, just spit out the Dtm Summary
                //var type1 = DtmClassification.Type1Classify(tc.DtmInfo, DtmClassification.ComputeAutoThreshold(tc.DtmInfo.Matrix, ThresholdFactor));
                //la.Text = $"{type1} ({tc.DtmInfo.Summary})";

                la.Text = tc.DtmInfo.Summary;

                plot.Annotations.Add(la);
            }
        }
    }

    public class CliTrajectoryHuskynessTransitionAnnotater : ICliTrajectoryAnnotater
    {
        public CliTrajectoryHuskynessTransitionAnnotater()
        {
        }

        public string Key => "huskyinfo";
        public bool Default => false;
        int hnum = 5;

        public void AnnotateTrajectories(TextWriter console, PlotModel plot, CliParams clips, double[][] trajectories, int samplePeriod, long startTime, string temporalAxisKey, string rcsAxisKey)
        {
            double completeThreshold = clips.Get("huskythreshold", double.Parse, 0.9);
            int N = (int)Math.Round(Math.Sqrt(trajectories.Length));
            int resolution = clips.Get("resolution", Misc.ParseInt, 1);

            string huskynessAxisKey = clips.Get("huskynessAxiskey", "huskyness");

            var huskySamples = CliRcsAnalyser.ComputeHuskynessesSamples(console, clips, trajectories, samplePeriod, startTime, out var modules);
            int completeIndex = CliRcsAnalyser.DetectThreshold(huskySamples, completeThreshold, hnum);

            int[][] mts = modules.ToArray();

            if (completeIndex >= 0)
            {
                long completeTime = completeIndex * samplePeriod + startTime;
                console.WriteLine($"HuskyCompleted Epoch {completeTime} (Threshold {completeThreshold})");

                var la = new LineAnnotation() { Type = LineAnnotationType.Vertical, X = completeTime, Color = OxyColors.DarkKhaki };

                la.TextOrientation = AnnotationTextOrientation.AlongLine;

                la.Text = $"HuskyCompleted Epoch {completeTime} (Threshold {completeThreshold})";

                plot.Annotations.Add(la);
            }

            var c0 = OxyColors.DarkSlateGray;
            var c1 = OxyColors.SlateGray;

            var huskyTrajectories = Misc.SamplesToTrajectories(huskySamples);

            TrajectoryPlotting.AddY2Axis(plot, "Huskyness", huskynessAxisKey, 0, null);
            TrajectoryPlotting.PlotTrajectories(plot, "Huskyness", huskyTrajectories, samplePeriod, startTime, c0, c1, 2, resolution, TrajectoryPlotType.Line, temporalAxisKey, huskynessAxisKey);
        }
    }

    public class CliTrajectoryModuleIndependenceTransitionAnnotater : ICliTrajectoryAnnotater
    {
        public CliTrajectoryModuleIndependenceTransitionAnnotater()
        {
        }

        public string Key => "moduleindependenceinfo";
        public bool Default => false;
        int hnum = 5;

        public void AnnotateTrajectories(TextWriter console, PlotModel plot, CliParams clips, double[][] trajectories, int samplePeriod, long startTime, string temporalAxisKey, string rcsAxisKey)
        {
            double completeThreshold = clips.Get("ModuleIndependencethreshold", double.Parse, 0.9);
            int N = (int)Math.Round(Math.Sqrt(trajectories.Length));
            int resolution = clips.Get("resolution", Misc.ParseInt, 1);

            string moduleIndependenceAxisKey = clips.Get("ModuleIndependenceAxiskey", "ModuleIndependence");

            var moduleIndependenceSamples = CliRcsAnalyser.ComputeModuleIndependenceSamples(console, clips, trajectories, samplePeriod, startTime, out var modules);
            int completeIndex = CliRcsAnalyser.DetectThreshold(moduleIndependenceSamples, completeThreshold, hnum);

            int[][] mts = modules.ToArray();

            if (completeIndex >= 0)
            {
                long completeTime = completeIndex * samplePeriod + startTime;
                console.WriteLine($"ModuleIndependenceCompleted Epoch {completeTime} (Threshold {completeThreshold})");

                var la = new LineAnnotation() { Type = LineAnnotationType.Vertical, X = completeTime, Color = OxyColors.DarkKhaki };

                la.TextOrientation = AnnotationTextOrientation.AlongLine;

                la.Text = $"ModuleIndependenceCompleted Epoch {completeTime} (Threshold {completeThreshold})";

                plot.Annotations.Add(la);
            }

            var c0 = OxyColors.DarkSlateGray;
            var c1 = OxyColors.SlateGray;

            var moduleIndependenceTrajectories = Misc.SamplesToTrajectories(moduleIndependenceSamples);

            TrajectoryPlotting.AddY2Axis(plot, "Module Independence", moduleIndependenceAxisKey, 0, null);
            TrajectoryPlotting.PlotTrajectories(plot, "Module Independence", moduleIndependenceTrajectories, samplePeriod, startTime, c0, c1, 2, resolution, TrajectoryPlotType.Line, temporalAxisKey, moduleIndependenceAxisKey);
        }
    }

    public class CliTrajectoryPlotter : ICliPlotter
    {
        public static readonly CliTrajectoryPlotter IstPlotter = new CliTrajectoryPlotter("ist", "Epochs", "Genotypic Trait Expression", "Genotypic Trait Expression", new TrajectoryColours1D(OxyColors.DarkViolet, OxyColors.Violet), 2.0);
        public static readonly CliTrajectoryPlotter PstPlotter = new CliTrajectoryPlotter("pst", "Epochs", "Phenotypic Trait Expression", "Phenotypic Trait Expression", new TrajectoryColours1D(OxyColors.DarkBlue, OxyColors.LightBlue), 2.0);
        public static readonly CliTrajectoryPlotter FitnessPlotter = new CliTrajectoryPlotter("fitness", "Epochs", "Fitness", "Fitness", new TrajectoryColours1D(OxyColors.Red, OxyColors.DarkOrange), 2.0);
        public static readonly CliTrajectoryPlotter SudokuSamplesPlotter = new CliTrajectoryPlotter("sudokusamples", "Epochs", "Sudoku Score", "Sudoku Score", new TrajectoryColours1D(OxyColors.Brown, OxyColors.SaddleBrown), 2.0);
        public static readonly CliTrajectoryPlotter NQueensSamplesPlotter = new CliTrajectoryPlotter("nqueenssamples", "Epochs", "NQueens Score", "NQueens Score", new TrajectoryColours1D(OxyColors.Brown, OxyColors.SaddleBrown), 2.0);
        public static readonly CliTrajectoryPlotter IvmcModuleSwitchesPlotter = new CliTrajectoryPlotter("ivmcswitches", "Epochs", "Switch Event Frequency", "Switch Event Frequency", new TrajectoryColours1D(OxyColors.DeepPink, OxyColors.HotPink), 2.0);
        public static readonly CliTrajectoryPlotter IvmcModuleSolvesPlotter = new CliTrajectoryPlotter("ivmcsolves", "Epochs", "Solve Frequency", "Solve Frequency", new TrajectoryColours1D(OxyColors.IndianRed, OxyColors.DarkRed), 2.0);
        public static readonly CliTrajectoryPlotter IvmcPropersPlotter = new CliTrajectoryPlotter("ivmcproper", "Epochs", "Ivmc Module Benefit", "mc Module Benefit", new TrajectoryColours1D(OxyColors.SaddleBrown, OxyColors.SandyBrown), 2.0);

        public List<ICliTrajectoryAnnotater> TrajectoryAnnotaters { get; } = new List<ICliTrajectoryAnnotater>();

        public CliTrajectoryPlotter(string prefix, string xLabel, string yLabel, string seriesName, ITrajectoryColours colors, double strokeThickness, IEnumerable<ICliTrajectoryAnnotater> annotators = null)
        {
            Prefix = prefix;
            XLabel = xLabel;
            YLabel = yLabel;
            SeriesName = seriesName;
            Colors = colors;
            StrokeThickness = strokeThickness;

            if (annotators != null)
                TrajectoryAnnotaters.AddRange(annotators);
        }
        
        public string Prefix { get; }
        public string XLabel { get; }
        public string YLabel { get; }
        public string SeriesName { get; }

        public ITrajectoryColours Colors { get; }

        public double StrokeThickness { get; }

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            int samplePeriod;
            var trajectories = Analysis.LoadTrajectories(filename, out samplePeriod);
            samplePeriod = clips.Get("samplePeriod", Misc.ParseInt, samplePeriod);
            if (samplePeriod == -1)
            {
                samplePeriod = 1; // default
                console.WriteLine("SamplePeriod unqualified");
            }

            title = clips.Get("title", title);
            var seriesName = clips.Get("seriesname", SeriesName);

            return PlotTrajectories(console, clips, title, seriesName, trajectories, samplePeriod);
        }

        public PlotModel PlotTrajectories(TextWriter console, CliParams clips, string title, string seriesName, double[][] trajectories, int samplePeriod)
        {
            double? ymin = clips.Get("ymin", s => (double?)double.Parse(s), null);
            double? ymax = clips.Get("ymax", s => (double?)double.Parse(s), null);

            int start = clips.Get("start", Misc.ParseInt, 0);
            int end = clips.Get("end", Misc.ParseInt, trajectories[0].Length * samplePeriod);
            
            var resample = CliPlotHelpers.DefaultResampler(clips);
            double scaleFactor = clips.Get("scaleFactor", double.Parse, 1.0); // useful, e.g. for ivmcs which are cumulative totals rather than means
            int resolution = clips.Get("resolution", Misc.ParseInt, 1);

            var trajectoryPlotType = clips.Get("plottype", CliPlotHelpers.ParseGenerousTrajectoryPlotType, TrajectoryPlotType.Line);

            string xAxisKey = clips.Get("xaxiskey", "x");
            string yAxisKey = clips.Get("yaxiskey", "y");
            string colorAxisKey = clips.Get("coloraxiskey", trajectoryPlotType == TrajectoryPlotType.Rectangles ? "color" : null);

            int skip = start / samplePeriod;
            int take = Math.Min((end + samplePeriod - start) / samplePeriod, trajectories[0].Length - skip);
            trajectories = trajectories.Select(traj => traj.Skip(skip).Take(take).ToArray()).ToArray();

            resample(ref trajectories, ref samplePeriod);

            if (scaleFactor != 1.0)
            {
                for (int i = 0; i < trajectories.Length; i++)
                    for (int j = 0; j < trajectories[i].Length; j++)
                        trajectories[i][j] *= scaleFactor;
            }

            int startTime = start;

            CliPlotHelpers.Keeper(clips)(ref trajectories, ref samplePeriod);

            var strokeThickness = clips.Get("strokethickness", double.Parse, StrokeThickness);

            bool logX = clips.Get("logX", bool.Parse, false);
            bool logY = clips.Get("logY", bool.Parse, false);

            // at this point, swap out samplePeriod for alternativeSamplePeriod (so that we plot as if it were whatever sample period we want, e.g. can plot epochs instead of generations or w/e)
            int plotSamplePeriod = clips.Get("plotSamplePeriod", Misc.ParseInt, samplePeriod);

            var tags = TrajectoryPlotting.Tags1D(trajectories.Length, i => i);
            var colors = clips.Get("color", s => new ConstantColours(CliPlotHelpers.ParseColour(s)).Colours(trajectories.Length), Colors.Colours(trajectories.Length));

            PlotModel plot;
            if (clips.IsSet("annotationsOnly"))
            {
                plot = TrajectoryPlotting.PrepareTrajectoriesPlot(title, XLabel, null, null, ymin, ymax, xAxisKey, null, null, logX, logY, false);
            }
            else
            {
                if (trajectoryPlotType == TrajectoryPlotType.Rectangles)
                {
                    plot = TrajectoryPlotting.PlotTrajectories(title, XLabel, "Trait", YLabel, seriesName, trajectories, plotSamplePeriod, startTime, ymin, ymax, tags, colors, strokeThickness, resolution, trajectoryPlotType, xAxisKey, yAxisKey, colorAxisKey, logX, logY, true);
                }
                else
                {
                    plot = TrajectoryPlotting.PlotTrajectories(title, XLabel, YLabel, null, seriesName, trajectories, plotSamplePeriod, startTime, ymin, ymax, tags, colors, strokeThickness, resolution, trajectoryPlotType, xAxisKey, yAxisKey, null, logX, logY, false);
                }
            }

            foreach (var annotater in TrajectoryAnnotaters)
            {
                if (clips.Get(annotater.Key, bool.Parse, annotater.Default))
                    annotater.AnnotateTrajectories(console, plot, clips, trajectories, plotSamplePeriod, startTime, xAxisKey, yAxisKey);
            }

            return plot;
        }
    }

    public interface ITrajectoryTags
    {
        object[] Tags(int n);
    }

    public class TrajectoryTags1D : ITrajectoryTags
    {
        public TrajectoryTags1D(Func<int, object> tagger)
        {
            Tagger = tagger;
        }

        public Func<int, object> Tagger { get; }

        public object[] Tags(int n)
        {
            return TrajectoryPlotting.Tags1D(n, Tagger);
        }
    }

    public class TrajectoryTags2D : ITrajectoryTags
    {
        public TrajectoryTags2D(Func<int, int, object> tagger)
        {
            Tagger = tagger;
        }

        public Func<int, int, object> Tagger { get; }

        public object[] Tags(int n)
        {
            return TrajectoryPlotting.Tags2D(n, Tagger);
        }
    }

    public interface ITrajectoryColours
    {
        OxyColor[] Colours(int n);
    }

    public class ConstantColours : ITrajectoryColours
    {
        public ConstantColours(OxyColor colour)
        {
            Colour = colour;
        }

        public OxyColor Colour { get; }

        public OxyColor[] Colours(int n)
        {
            return Misc.Create(n, i => Colour);
        }
    }

    public class TrajectoryColours1D : ITrajectoryColours
    {
        public TrajectoryColours1D(OxyColor? c0, OxyColor? c1)
        {
            C0 = c0;
            C1 = c1;
        }

        public OxyColor? C0 { get; }
        public OxyColor? C1 { get; }

        public OxyColor[] Colours(int n)
        {
            return TrajectoryPlotting.Colours1D(n, C0, C1);
        }
    }

    public class TrajectoryColours2D : ITrajectoryColours
    {
        public TrajectoryColours2D(OxyColor c00, OxyColor c10, OxyColor c01, OxyColor c11)
        {
            C00 = c00;
            C10 = c10;
            C01 = c01;
            C11 = c11;
        }

        public OxyColor C00 { get; }
        public OxyColor C10 { get; }
        public OxyColor C01 { get; }
        public OxyColor C11 { get; }

        public OxyColor[] Colours(int n)
        {
            n = (int)Math.Round(Math.Sqrt(n));
            return TrajectoryPlotting.Colours2D(n, C00, C10, C01, C11);
        }
    }

    public class CliGenomePlotter : ICliPlotter
    {
        public string Prefix => "genome";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Plots genomes.");
            }

            var genome = Analysis.LoadGenome(filename);
            return PlotDenseGenome(console, clips, genome, title, null);
        }

        public static PlotModel PlotDenseGenome(TextWriter console, CliParams clips, DenseGenome genome, string title, ExperimentConfiguration config)
        {
            var thing = clips.Get("thing", "genome");

            if (thing == "genome" || thing == "dtm")
            {
                return PlotDtm(clips, genome.TransMat, title ?? "Regulatory Matrix");
            }
            else if (thing == "is")
            {
                var configFile = clips.Get("config", clips.Get("expfile", null));
                config = configFile == null ? config : CliPlotHelpers.LoadExperimentConfig(configFile);

                return PlotExpressionVector(clips, genome.InitialState, title ?? "Initial Trait Expression", config);
            }
            else if (thing == "ps")
            {   
                var configFile = clips.Get("config", clips.Get("expfile", null));
                config = configFile == null ? config : CliPlotHelpers.LoadExperimentConfig(configFile);

                int seed = clips.GetOrCreate("seed", int.Parse, () => CliExp.SeedSource.Next());
                var rand = new MathNet.Numerics.Random.MersenneTwister(seed);
                var ctx = new ModelExecutionContext(rand);

                var p = genome.Develop(ctx, config.DevelopmentRules);
                
                return PlotExpressionVector(clips, p.Vector, title ?? "Phenotypic Trait Expression", config);
            }
            else if (thing == "dev")
            {
                var expFile = clips.Get("expfile", null);
                var configFile = clips.Get("config", null);
                
                if (expFile != null)
                {
                    config = PopulationExperiment<DenseIndividual>.Load(expFile).PopulationConfig.ExperimentConfiguration;
                }
                else if (configFile != null)
                {
                    var cfg = M4M.State.GraphSerialisation.Read<object>(configFile);

                    if (cfg is PopulationExperimentConfig<DenseIndividual> pec)
                        config = pec.ExperimentConfiguration;
                    else if (cfg is ExperimentConfiguration ec)
                        config = ec;
                    else
                    {
                        throw new Exception("Config must be dense");
                    }
                }
                else if (config == null)
                {
                    throw new Exception("Development trajectories require an expfile or configfile");
                }
                
                int seed = clips.GetOrCreate("seed", int.Parse, () => CliExp.SeedSource.Next());

                var rand = new MathNet.Numerics.Random.MersenneTwister(seed);
                return PlotDevelopment(console, clips, genome, config.DevelopmentRules, rand, title ?? "Developmental Trajectories");
            }
            else if (thing == "biasvector")
            {
                var configFile = clips.Get("config", clips.Get("expfile", null));
                config = configFile == null ? config : CliPlotHelpers.LoadExperimentConfig(configFile);

                if (genome.BiasVector == null)
                    throw new Exception("No bias vector");

                return PlotExpressionVector(clips, genome.BiasVector, title ?? "Bias Vector", config);
            }
            else
            {
                throw new Exception("Not sure what a '" + thing + "' is, try one of these:\n" +
                    " - dtm\n" +
                    " - is\n" +
                    " - ps\n" +
                    " - dev\n" +
                    " - biasvector");
            }
        }

        public static PlotModel PlotDtm(CliParams clips, MathNet.Numerics.LinearAlgebra.Matrix<double> transMat, string title)
        {
            double? min = clips.Get("min", s => (double?)double.Parse(s), null);
            double? max = clips.Get("max", s => (double?)double.Parse(s), null);

            var plot = GenomePlotting.PlotDtm(title, transMat, min, max);

            return plot;
        }

        public static PlotModel PlotExpressionVector(CliParams clips, MathNet.Numerics.LinearAlgebra.Vector<double> expressionVector, string title, ExperimentConfiguration config)
        {
            if (clips.IsSet("sudoku"))
            {
                var sudokuPlot = PlotSudokuSolution(expressionVector, config?.Targets[0] as CorrelationMatrixTarget);
                sudokuPlot.Title = clips.Get("title", sudokuPlot.Title);
                return sudokuPlot;
            }

            double? min = clips.Get("min", s => (double?)double.Parse(s), null);
            double? max = clips.Get("max", s => (double?)double.Parse(s), null);

            int sqrt = (int)Math.Round(Math.Sqrt((double)expressionVector.Count));
            int moduleCount = clips.Get("moduleCount", int.Parse, sqrt * sqrt == expressionVector.Count ? sqrt : 1);

            var plot = GenomePlotting.PlotExpressionVector(title, expressionVector, moduleCount, min, max);

            // TODO: find a better place to put this
            if (clips.IsSet("localMatrixBoundryAnnotation"))
            {
                if (config == null)
                    throw new ArgumentException(nameof(config), "Config must be provided to plot localMatrixBoundryAnnotations");

                var localMatrixBoundryAnnotationColour = clips.Get("localMatrixBoundryAnnotation", s => s == null ? OxyColors.Red : CliPlotHelpers.ParseColour(s));
                var target = CliPlotHelpers.UnwrapTarget(config.Targets[0]);

                if (target is Epistatics.CorrelationMatrixTarget cmt)
                {
                    var mat = cmt.CorrelationMatrix;
                    var mih = new MatrixIndexHelper(moduleCount, moduleCount); // assume square

                    foreach (var unhappy in Epistatics.StandardCorrelationMatrixTargets.EnumerateUnhappyCorrelations(mat, expressionVector))
                    {
                        int i = unhappy.Row;
                        int j = unhappy.Col;

                        mih.ToRowCol(i, out int r, out int c);
                        mih.ToRowCol(j, out int r2, out int c2);
                        int dr = r2 - r;
                        int dc = c2 - c;

                        double x0 = c + dc / 2.0 + Math.Abs(dr) * -0.5;
                        double x1 = c + dc / 2.0 + Math.Abs(dr) * 0.5;
                        double y0 = r + dr / 2.0 + Math.Abs(dc) * -0.5;
                        double y1 = r + dr / 2.0 + Math.Abs(dc) * 0.5;

                        var la = new PolylineAnnotation();
                        la.Color = localMatrixBoundryAnnotationColour;
                        la.LineStyle = LineStyle.Solid;
                        la.Points.Add(new DataPoint(x0, y0));
                        la.Points.Add(new DataPoint(x1, y1));
                        la.StrokeThickness = 2;
                        plot.Annotations.Add(la);
                    }
                }
                else
                {
                    throw new ArgumentException(nameof(config), "Config must contain a single CorrelationMatrixTarget target");
                }
            }

            return plot;
        }

        public static PlotModel PlotSudokuSolution(MathNet.Numerics.LinearAlgebra.Vector<double> expressionVector, CorrelationMatrixTarget target)
        {
            var solution = Epistatics.Sudoku.RenderSolution(expressionVector, out int n);
            var partialSolution = Epistatics.Sudoku.RenderSolution(target?.ForcingVector ?? CreateVector.Dense<double>(solution.Length, 0), out _);
            for (int r = 0; r < n; r++)
            {
                for (int c = 0; c < n; c++)
                {
                    Console.Write(solution[r, c] + 1);
                }
                Console.WriteLine();
            }

            var conflicts = Epistatics.Sudoku.LocateConflicts(solution);

            var conflictCount = new int[n, n];
            foreach (var conflict in conflicts)
            {
                foreach (var mea in conflict.Entries)
                {
                    conflictCount[mea.Row, mea.Col]++;
                }
            }

            var plot = new PlotModel() { Title = "Sudoku" };

            var caxis = new OxyPlot.Axes.LinearAxis() { Title = "Col", Position = OxyPlot.Axes.AxisPosition.Bottom, MinimumMajorStep = 1, MinimumMinorStep = 1, Key = "col", MinimumPadding  = 0.01, MaximumPadding = 0.01 };
            var raxis = new OxyPlot.Axes.LinearAxis() { Title = "Row", Position = OxyPlot.Axes.AxisPosition.Left, StartPosition = 1, EndPosition = 0, MinimumMajorStep = 1, MinimumMinorStep = 1, Key = "row", MinimumPadding = 0.01, MaximumPadding = 0.01 };
            var coloraxis = new OxyPlot.Axes.LinearColorAxis() { Title = "Conflicts", Position = OxyPlot.Axes.AxisPosition.Right, Palette = OxyPalette.Interpolate(100, OxyColors.White, OxyColors.Red), Key = "color" };

            plot.Axes.Add(caxis);
            plot.Axes.Add(raxis);
            plot.Axes.Add(coloraxis);

            var rs = new RectangleSeries();
            rs.CanTrackerInterpolatePoints = false;

            for (int r = 0; r < n; r++)
            {
                for (int c = 0; c < n; c++)
                {
                    var rect = new RectangleItem(c - 0.5, c + 0.5, r - 0.5, r + 0.5, conflictCount[r, c]);
                    rs.Items.Add(rect);

                    var labelColor = target != null && partialSolution[r, c] >= 0 ? (partialSolution[r, c] == solution[r, c] ? OxyColors.Blue : OxyColors.Brown) : OxyColors.Black;
                    plot.Annotations.Add(new TextAnnotation() { TextPosition = new DataPoint(c, r), Text = ""+(solution[r, c] + 1), Layer = AnnotationLayer.AboveSeries, TextColor = labelColor, TextVerticalAlignment = VerticalAlignment.Middle, TextHorizontalAlignment = HorizontalAlignment.Center, StrokeThickness = 0 });
                }
            }

            int subn = (int)Math.Floor(Math.Sqrt(n));

            for (int r = 0; r <= subn; r++)
            {
                plot.Annotations.Add(new LineAnnotation() { Y = r * subn - 0.5, Type = LineAnnotationType.Horizontal, MinimumX = -0.5, MaximumX = n - 0.5, LineStyle = LineStyle.Solid, Color = OxyColors.Black, StrokeThickness = r == 0 || r == subn ? 2 : 1 });
                plot.Annotations.Add(new LineAnnotation() { X = r * subn - 0.5, Type = LineAnnotationType.Vertical, MinimumY = -0.5, MaximumY = n - 0.5, LineStyle = LineStyle.Solid, Color = OxyColors.Black, StrokeThickness = r == 0 || r == subn ? 2 : 1 });
            }

            plot.PlotAreaBorderColor = OxyColors.Transparent;
            plot.Series.Add(rs);

            return plot;
        }

        public static PlotModel PlotDevelopment(TextWriter console, CliParams clips, DenseGenome genome, DevelopmentRules drules, MathNet.Numerics.Random.RandomSource rand, string title)
        {
            double? min = clips.Get("min", s => (double?)double.Parse(s), null);
            double? max = clips.Get("max", s => (double?)double.Parse(s), null);
            
            string xAxisKey = clips.Get("xaxiskey", "x");
            string yAxisKey = clips.Get("yaxiskey", "y");

            double[][] trajectories = null;
            genome.DevelopWithTrajectories(rand, drules, ref trajectories);

            // this is NOT the job of a CliPlotter... but oh well
            if (clips.Get("dtd", bool.Parse, true))
            {
                string modulesstring = clips.Get("modulesstring", null);

                int[][] mts;

                if (modulesstring == null)
                {
                    int N = trajectories.Length;
                    int moduleSize = clips.Get("modulesize", int.Parse, (int)Math.Sqrt(N));
                    int moduleCount = N / moduleSize;

                    console.WriteLine("Assuming " + moduleCount + " block modules of size " + moduleSize + " for DTD (qualify modulesize or modulesstring)");
                    mts = Analysis.BlockModuleTraits(moduleSize, moduleCount);
                }
                else
                {
                    mts = Modular.MultiModulesStringParser.Instance.Parse(modulesstring).ToArray();
                }

                var dtd = Analysis.DetectDevTimeDominance(trajectories, mts);
                console.WriteLine(String.Join(", ", dtd));
            }

            if (clips.IsSet("keep"))
            {
                IEnumerable<int> keep = clips.Get("keep", CliPlotHelpers.ParseIntList);
                trajectories = CliPlotHelpers.KeepLeaveNull(trajectories, keep);
            }

            bool logX = clips.Get("logX", bool.Parse, false);
            bool logY = clips.Get("logY", bool.Parse, false);

            var plot = TrajectoryPlotting.PlotTrajectories(title, "Developmental Time", "Trait Expression", null, "Trait Expression", trajectories, 1, 0, min, max, (OxyColor?)null, null, 2, 1, TrajectoryPlotType.Line, xAxisKey, yAxisKey, null, logX, logY, false);

            return plot;
        }
    }

    public class CliWholeSamplePlotter : ICliPlotter
    {
        public CliWholeSamplePlotter(CliTraceePlotter traceePlotter)
        {
            TraceePlotter = traceePlotter;
        }

        public CliTraceePlotter TraceePlotter { get; }

        public string Prefix => "wholesample";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Plots wholesamples");
            }

            List<WholeSample<DenseIndividual>> wholeSamples;
            if (filename.Contains(";"))
            {
                wholeSamples = WholeSample<DenseIndividual>.EnumerateWholeSamplesSeries2(filename.Split(';')).ToList();
            }
            else
            {
                wholeSamples = WholeSample<DenseIndividual>.LoadWholeSamples(filename);
            }

            if (clips.IsSet("epoch"))
            {
                // plot last genome in epoch
                int epoch = clips.Get("epoch", Misc.ParseInt);

                var samples = wholeSamples.Where(ws => ws.Epoch == epoch).ToArray();
                
                var individual = samples.Last().Judgements.First().Individual;
                
                var thing = clips.Get("thing", null);

                if (thing == "ps")
                { // we use the known ps instead of generating one
                    var configFile = clips.Get("config", clips.Get("expfile", null));
                    var config = configFile == null ? null : CliPlotHelpers.LoadExperimentConfig(configFile);

                    return CliGenomePlotter.PlotExpressionVector(clips, individual.Phenotype.Vector, title ?? "Phenotypic Expression", config);
                }
                else
                {
                    return CliGenomePlotter.PlotDenseGenome(console, clips, individual.Genome, title, null);
                }
            }
            else
            {
                TraceInfo<DenseIndividual> traceInfo = new TraceInfo<DenseIndividual>(wholeSamples.ToList());
                return TraceePlotter.PlotTracee(console, clips, traceInfo, title, true);
            }
        }
    }

    public class CliExpPlotter : ICliPlotter
    {
        public string Prefix => "epoch";

        public PlotModel Plot(TextWriter console, CliParams clips, string filename, string title)
        {
            var exp = PopulationExperiment<DenseIndividual>.Load(filename);

            if (clips.IsSet("ma"))
            {
                if (clips.IsSet("help"))
                {
                    console.WriteLine("Plots fitness variation for mutants\n" +
                        " - mutantCount, the number of mutatnts to generate (default 10000)\n" +
                        " - binCount, the number of bins in the histogram (default 100)\n" +
                        " - min and max, min and max of the histogram x axis\n" +
                        " - seed, the random seed\n" +
                        " - target, the target to use\n" +
                        " - resetTarget, whether to reset the target before starting (default false)");
                }

                int mutantCount = clips.Get("mutantCount", Misc.ParseInt, 10000);

                int binCount = clips.Get("binCount", int.Parse, 100);
    
                int seed = clips.GetOrCreate("seed", int.Parse, () => CliExp.SeedSource.Next());
                var rand = new MathNet.Numerics.Random.MersenneTwister(seed, false);
                var context = new ModelExecutionContext(rand);
                var config = exp.PopulationConfig.ExperimentConfiguration;

                string targetName = clips.Get("target", null);
                var target = targetName != null
                    ? config.Targets.FirstOrDefault(t => t.FriendlyName == targetName)
                    : config.Targets.First();
                
                if (targetName == null)
                {
                    console.WriteLine("Assuming target in position 0. Available targets are:");
                    foreach (var t in exp.PopulationConfig.ExperimentConfiguration.Targets)
                        console.WriteLine(t.FriendlyName + " (" + t.FullName + ")");
                }

                if (clips.Get("resetTarget", bool.Parse, false))
                {
                    int exposureEpoch = clips.Get("exposureepoch", Misc.ParseInt, 1);

                    ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                    target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
                }
                
                target.NextGeneration(rand, config.JudgementRules);
                
                var ma = new MutantAnalysis<DenseGenome>(exp.Population.PeekAll()[0].Genome, mutantCount, context, config.ReproductionRules, config.DevelopmentRules, config.JudgementRules, target);
                
                double min = clips.Get("min", double.Parse, ma.FitnessChanges.Min());
                double max = clips.Get("max", double.Parse, ma.FitnessChanges.Max());

                // TODO: this should be in its own method in Potting
                PlotModel plot = new PlotModel() { Title = "Mutation Fitness Variation" };
                plot.Axes.Add(new OxyPlot.Axes.LinearAxis() { Title = "Fitness Delta", Key = "x", Position = OxyPlot.Axes.AxisPosition.Bottom });
                plot.Axes.Add(new OxyPlot.Axes.LinearAxis() { Title = "Frequency", Key = "y", Position = OxyPlot.Axes.AxisPosition.Left });

                OxyPlot.Series.HistogramSeries hs = new OxyPlot.Series.HistogramSeries() { Title = "Mutation Benefit Variation" };
                var bins = HistogramHelpers.Collect(ma.FitnessChanges, HistogramHelpers.CreateUniformBins(min, max, binCount), new BinningOptions(BinningOutlierMode.RejectOutliers, BinningIntervalType.InclusiveLowerBound, BinningExtremeValueMode.IncludeExtremeValues)).ToArray();
                hs.ItemsSource = bins;
                console.WriteLine("Bin width: " + bins[0].Width);
                plot.Series.Add(hs);

                return plot;
            }
            else if (clips.IsSet("target"))
            {
                string targetName = clips.Get("target");
                var target = exp.PopulationConfig.ExperimentConfiguration.Targets.FirstOrDefault(t => t.FriendlyName == targetName);

                if (target == null)
                {
                    console.WriteLine("Available targets are:");
                    foreach (var t in exp.PopulationConfig.ExperimentConfiguration.Targets)
                        console.WriteLine(t.FriendlyName + " (" + t.FullName + ")");
                    return null;
                }

                while (target is ITargetDecorator decorator)
                    target = decorator.Target;

                if (target is VectorTarget vectorTarget)
                {
                    int moduleCount = clips.Get("modulecount", int.Parse, (int)Math.Sqrt(target.Size));
                    return PlotTraitVector(clips, vectorTarget.Vector, moduleCount, title);
                }
                else if (target is Epistatics.CorrelationMatrixTarget correlTarget)
                {
                    string thing = clips.Get("thing", "mat");

                    if (thing == "mat")
                    {
                        return PlotCorrelationMatrix(clips, correlTarget.CorrelationMatrix, title);
                    }
                    else if (thing == "forcingvector")
                    {
                        return CliGenomePlotter.PlotExpressionVector(clips, correlTarget.ForcingVector, title ?? "Forcing", exp.PopulationConfig.ExperimentConfiguration);
                    }
                    else
                    {
                        throw new Exception("Not sure what a '" + thing + "' is, try one of these:\n" +
                            " - mat\n" +
                            " - forcingvector");
                    }
                }
            }
            else if (clips.IsSet("genome"))
            {
                return CliGenomePlotter.PlotDenseGenome(console, clips, exp.Population.PeekAll()[0].Genome, title, exp.PopulationConfig.ExperimentConfiguration);
            }
            else
            {
                console.WriteLine("Defaulting to genome plot; options are:\n" +
                    " - target\n" +
                    " - ma\n" +
                    " - genome");
                return CliGenomePlotter.PlotDenseGenome(console, clips, exp.Population.PeekAll()[0].Genome, title, exp.PopulationConfig.ExperimentConfiguration);
            }

            console.WriteLine("No idea what to do. Sorry this message is so unhelpful.");
            return null;
        }

        public static PlotModel PlotTraitVector(CliParams clips, MathNet.Numerics.LinearAlgebra.Vector<double> expressionVector, int moduleCount, string title)
        {
            double? min = clips.Get("min", s => (double?)double.Parse(s), null);
            double? max = clips.Get("max", s => (double?)double.Parse(s), null);

            var plot = GenomePlotting.PlotExpressionVector(title, expressionVector, moduleCount, min, max);

            return plot;
        }

        public static PlotModel PlotCorrelationMatrix(CliParams clips, MathNet.Numerics.LinearAlgebra.Matrix<double> correlationMatrix, string title)
        {
            double? min = clips.Get("min", s => (double?)double.Parse(s), null);
            double? max = clips.Get("max", s => (double?)double.Parse(s), null);

            var plot = GenomePlotting.PlotDtm(title, correlationMatrix, min, max);

            return plot;
        }
    }

    public delegate void TrajectoryResampler(ref double[][] trajs, ref int samplePeriod);

    public static class CliPlotHelpers
    {
        public static IEnumerable<double> LinearSamples(double min, double max, int resolution)
        {
            if (resolution == 0)
            {
                yield return min;
                yield break;
            }

            for (int i = 0; i <= resolution; i++)
            {
                var x = (double)i / resolution;
                yield return min * (1 - x) + max * x;
            }
        }

        public static RandomSource ParseRand(CliParams clips, int? defaultSeed = null)
        {
            if (clips.IsSet("seed"))
            {
                int seed = clips.Get("seed", int.Parse);
                return new CustomMersenneTwister(seed);
            }
            else if (defaultSeed.HasValue)
            {
                return new CustomMersenneTwister(defaultSeed.Value);
            }
            else
            {
                return new CustomMersenneTwister(CliExp.SeedSource.Next());
            }
        }

        public static ITarget ParseTarget(TextWriter console, CliParams clips, IReadOnlyList<ITarget> targets, ITarget defaultTarget = null)
        {
            var targetString = clips.Get("target", null);
            ITarget target = null;

            if (targetString != null)
            {
                // by name
                target = targets.FirstOrDefault(t => t.FriendlyName == targetString)
                    ?? targets.FirstOrDefault(t => t.FullName == targetString);

                // by idx
                if (target is null && int.TryParse(targetString, out var targetIdx))
                    target = targets[targetIdx];

                // compose
                if (targetString == "compose")
                    target = ExperimentComposition.Typical.PrepareDefaultTargetPackageComposer().Compose(console, clips).Targets[0];
            }

            if (target is null)
            {
                // use default
                console.WriteLine("Available targets are:");
                for (int ti = 0; ti < targets.Count; ti++)
                    console.WriteLine($" {ti}: {targets[ti].FullName}");

                if (!(defaultTarget is null))
                {
                    console.WriteLine("Using default target " + defaultTarget.FullName);
                    target = defaultTarget;
                }
                else
                {
                    throw new Exception("No default target");
                }
            }

            return target;
        }

        public static void PrepTarget(RandomSource rand, CliParams clips, ExperimentConfiguration config, ITarget target, bool resetDefault, int defaultExposureEpoch = 1)
        {
            if (clips.Get("resetTarget", bool.Parse, resetDefault))
            {
                int exposureEpoch = clips.Get("exposureepoch", Misc.ParseInt, defaultExposureEpoch);

                ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
            }

            target.NextGeneration(rand, config.JudgementRules);
        }

        public static OxyPlot.Axes.TickStyle ParseTickStyle(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return OxyPlot.Axes.TickStyle.Outside;
            }
            else if (str.Equals("inside", StringComparison.CurrentCultureIgnoreCase))
            { // don't use: if you want lines on the plot, use grid lines
                return OxyPlot.Axes.TickStyle.Inside;
            }
            else if (str.Equals("outside", StringComparison.CurrentCultureIgnoreCase))
            {
                return OxyPlot.Axes.TickStyle.Outside;
            }
            else if (str.Equals("crossing", StringComparison.CurrentCultureIgnoreCase))
            { // don't use
                return OxyPlot.Axes.TickStyle.Crossing;
            }
            else if (str.Equals("none", StringComparison.CurrentCultureIgnoreCase))
            {
                return OxyPlot.Axes.TickStyle.None;
            }

            throw new ArgumentException("Unrecognised TickStyle string: " + str, nameof(str));
        }

        public static OxyPlot.LineStyle ParseLineStyle(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return OxyPlot.LineStyle.None;
            }
            else if (str.Equals("none", StringComparison.CurrentCultureIgnoreCase))
            {
                return OxyPlot.LineStyle.None;
            }
            else if (str.Equals("solid", StringComparison.CurrentCultureIgnoreCase))
            {
                return OxyPlot.LineStyle.Solid;
            }
            else if (str.Equals("dash", StringComparison.CurrentCultureIgnoreCase))
            {
                return OxyPlot.LineStyle.Dash;
            }
            else if (str.Equals("dot", StringComparison.CurrentCultureIgnoreCase))
            {
                return OxyPlot.LineStyle.Dot;
            }
            else if (str.Equals("auto", StringComparison.CurrentCultureIgnoreCase))
            {
                return OxyPlot.LineStyle.Automatic;
            }
            else if (str.Equals("automatic", StringComparison.CurrentCultureIgnoreCase))
            {
                return OxyPlot.LineStyle.Automatic;
            }

            throw new ArgumentException("Unrecognised LineStyle string: " + str, nameof(str));
        }

        public static AxisPosition ParseAxisPosition(string str)
        {
            switch (str.ToLowerInvariant())
            {
                case "t":
                case "top":
                    return AxisPosition.Top;
                case "b":
                case "bottom":
                    return AxisPosition.Bottom;
                case "l":
                case "left":
                    return AxisPosition.Left;
                case "r":
                case "right":
                    return AxisPosition.Right;
                case "":
                case "none":
                    return AxisPosition.Right;
                default:
                    throw new ArgumentException("Unrecognised axis position: " + str);
            }
        }

        private static Dictionary<string, OxyColor> NamedColourTable;

        public static OxyColor ParseColour(string str)
        {
            if (str.StartsWith("magpie", StringComparison.InvariantCultureIgnoreCase))
                return new[] { OxyColors.Blue, OxyColors.Green, OxyColors.Purple }[new Random().Next(3)];

            if (str.StartsWith("#"))
            {
                if (str.Length == 1 + 1)
                {
                    var r = Convert.ToByte(str.Substring(1, 1), 16) * 16;
                    return OxyColor.FromRgb((byte)r, (byte)r, (byte)r);
                }
                else if (str.Length == 3 + 1)
                {
                    var r = Convert.ToByte(str.Substring(1, 1), 16) * 16;
                    var g = Convert.ToByte(str.Substring(2, 1), 16) * 16;
                    var b = Convert.ToByte(str.Substring(3, 1), 16) * 16;
                    return OxyColor.FromRgb((byte)r, (byte)g, (byte)b);
                }
                else if (str.Length == 6 + 1)
                {
                    var r = Convert.ToByte(str.Substring(1, 2), 16);
                    var g = Convert.ToByte(str.Substring(3, 2), 16);
                    var b = Convert.ToByte(str.Substring(5, 2), 16);
                    return OxyColor.FromRgb(r, g, b);
                }
            }
            else
            {
                if (NamedColourTable == null)
                {
                    NamedColourTable = new Dictionary<string, OxyColor>(StringComparer.InvariantCultureIgnoreCase);

                    foreach (var cmi in typeof(OxyColors).GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public))
                    {
                        NamedColourTable.Add(cmi.Name, (OxyColor)cmi.GetValue(null));
                    }
                    NamedColourTable.Add("crow", OxyColors.Black);
                    NamedColourTable.Add("grey", OxyColors.Gray);
                }

                if (NamedColourTable.TryGetValue(str, out var named))
                    return named;
            }

            throw new ArgumentException("Unrecognised colour string: " + str, nameof(str));
        }

        /// <summary>
        /// Parses strings that look like 1.0;2;4.5;0.2 into a list of doubles
        /// </summary>
        public static IReadOnlyList<double> ParseDoubleList(string str)
        {
            return str.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(double.Parse).ToArray();
        }

        /// <summary>
        /// Parses strings that look like 1-5,8,9-10 into a list of ints
        /// Sorts the list, and throws if there are duplicates
        /// </summary>
        public static IReadOnlyList<int> ParseStrictIntList(string str)
        {
            var baseList = ParseIntList(str);
            var uniques = baseList.OrderBy(x => x).Distinct().ToArray(); // by no means the most efficient mechanism immaginable
            if (uniques.Length != baseList.Count)
                throw new Exception("Duplicate entries in int-list: '" + str + "'");

            return uniques;
        }

        /// <summary>
        /// Parses strings that look like 1-5,8,9-10 into a list of ints
        /// </summary>
        public static IReadOnlyList<int> ParseIntList(string str)
        {
            string[] blocks = str.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);

            List<int> ints = new List<int>();
            foreach (var block in blocks)
            {
                ints.AddRange(MaybeDash(block));
            }

            return ints;
        }

        private static IEnumerable<int> MaybeDash(string str)
        {
            if (str.Contains("-"))
            {
                string[] data = str.Split('-');
                
                if (data.Length != 2)
                    throw new Exception("Cannot parse int list: '" + str + "'");

                int l = int.Parse(data[0]);
                int r = int.Parse(data[1]);
                
                if (l > r)
                    throw new Exception("Cannot parse int list: '" + str + "'");

                for (int i = l; i <= r; i++)
                {
                    yield return i;
                }
            }
            else
            {
                yield return int.Parse(str);
            }
        }

        public static TrajectoryPlotType ParseTrajectoryPlotType(string name)
        {
            if (string.Equals(name, "line", StringComparison.InvariantCultureIgnoreCase))
                return TrajectoryPlotType.Line;
            if (string.Equals(name, "scatter", StringComparison.InvariantCultureIgnoreCase))
                return TrajectoryPlotType.Scatter;

            throw new Exception("Unrecognised TrajectoryPlotType '" + name + "'. Please use 'line' or 'scatter'");
        }

        public static TrajectoryPlotType ParseGenerousTrajectoryPlotType(string name)
        {
            if (string.Equals(name, "line", StringComparison.InvariantCultureIgnoreCase))
                return TrajectoryPlotType.Line;
            if (string.Equals(name, "scatter", StringComparison.InvariantCultureIgnoreCase))
                return TrajectoryPlotType.Scatter;
            if (string.Equals(name, "rectangles", StringComparison.InvariantCultureIgnoreCase))
                return TrajectoryPlotType.Rectangles;

            throw new Exception("Unrecognised TrajectoryPlotType '" + name + "'. Please use 'line', 'scatter', or 'rectangles'");
        }

        public static MarkerType ParseMarkerType(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            if (name == "")
                return MarkerType.None;
            if (string.Equals(name, "none", StringComparison.InvariantCultureIgnoreCase))
                return MarkerType.None;

            if (string.Equals(name, "triangle", StringComparison.InvariantCultureIgnoreCase))
                return MarkerType.Triangle;
            if (name == "^")
                return MarkerType.Triangle;

            if (string.Equals(name, "diamond", StringComparison.InvariantCultureIgnoreCase))
                return MarkerType.Diamond;
            if (name == "v")
                return MarkerType.Diamond;
                
            if (string.Equals(name, "star", StringComparison.InvariantCultureIgnoreCase))
                return MarkerType.Star;
            if (name == "*")
                return MarkerType.Star;
                
            if (string.Equals(name, "circle", StringComparison.InvariantCultureIgnoreCase))
                return MarkerType.Circle;
            if (name == "o")
                return MarkerType.Circle;

            if (string.Equals(name, "cross", StringComparison.InvariantCultureIgnoreCase))
                return MarkerType.Cross;
            if (name == "x")
                return MarkerType.Cross;

            if (string.Equals(name, "plus", StringComparison.InvariantCultureIgnoreCase))
                return MarkerType.Plus;
            if (name == "+")
                return MarkerType.Plus;

            throw new Exception("Unrecognised MarkerType '" + name + "'.");
        }

        public static ExperimentConfiguration LoadExperimentConfig(string filename)
        {
            var cfg = M4M.State.GraphSerialisation.Read<object>(filename);

            if (cfg is ExperimentConfiguration ec)
                return ec;
            else
            { // try any type of experiment (may throw violently)
                var expExtractor = ExperimentExtractorExperimentReceiver.LoadExtractorFromFile(filename);
                return expExtractor.ExperimentConfiguration;
            }
        }

        public static ExperimentConfiguration LoadDenseExperimentConfig(string filename)
        {
            var cfg = M4M.State.GraphSerialisation.Read<object>(filename);

            if (cfg is PopulationExperiment<DenseIndividual> pe)
                return pe.PopulationConfig.ExperimentConfiguration;
            else if (cfg is PopulationExperimentConfig<DenseIndividual> pec)
                return pec.ExperimentConfiguration;
            else if (cfg is ExperimentConfiguration ec)
                return ec;

            throw new Exception("No dense experiment or config found in '" + filename + "'");
        }

        public static DenseGenome LoadDenseGenome(string filename)
        {
            try
            {
                if (System.IO.Path.GetFileName(filename).StartsWith("genome"))
                {
                    var genome = Analysis.LoadGenome(filename);
                    return genome;
                }
            }
            catch { }

            var exp = M4M.State.GraphSerialisation.Read<object>(filename);

            if (exp is PopulationExperiment<DenseIndividual> pe)
                return pe.Population.ExtractAll()[0].Genome;

            throw new Exception("No dense genome or experiment in '" + filename + "'");
        }

        /// <summary>
        /// Returns a new array, where the non-kept elements have the default value (e.g. null)
        /// </summary>
        public static T[] KeepLeaveNull<T>(T[] arr, IEnumerable<int> indicies)
        {
            HashSet<int> keep = new HashSet<int>(indicies);

            var kept = new T[arr.Length];
            foreach (int k in keep)
                kept[k] = arr[k];

            return kept;
        }

        /// <summary>
        /// Returns a new array, where the non-kept elements have the given nonValue
        /// </summary>
        public static T[] Keep<T>(T[] arr, IEnumerable<int> indicies, T nonValue)
        {
            HashSet<int> keep = new HashSet<int>(indicies);

            var kept = new T[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                kept[i] = keep.Contains(i) ? arr[i] : nonValue;

            return kept;
        }

        public static Modular.Modules GuessModules(TextWriter console, CliParams clips, int size)
        {
            string modulesstring = clips.Get("modulesstring", null);

            if (modulesstring == null)
            {
                int moduleSize = clips.Get("modulesize", int.Parse, (int)Math.Sqrt(size));
                int moduleCount;

                do
                {
                    moduleCount = size / moduleSize;
                }
                while (moduleSize * moduleCount != size && moduleSize-- > 0);

                if (moduleSize * moduleCount != size || moduleSize == 0)
                    throw new Exception($"Cannot guess modules for system of size {size}. \nYou must qualify modulesize or modulesstring.");

                console?.WriteLine("Assuming " + moduleCount + " block modules of size " + moduleSize + " (qualify modulesize or modulesstring)");
                return Modular.Modules.CreateBlockModules(moduleCount, moduleSize);
            }
            else
            {
                return Modular.MultiModulesStringParser.Instance.Parse(modulesstring);
            }
        }

        public static TrajectoryResampler Keeper(CliParams clips)
        {
            var keep = clips.Get("keep", CliPlotHelpers.ParseIntList, null);

            void resample(ref double[][] trajectories, ref int samplePeriod)
            {
                if (keep != null)
                {
                    trajectories = CliPlotHelpers.KeepLeaveNull(trajectories, keep);
                }
            }

            return resample;
        }

        public static TrajectoryResampler DefaultResampler(CliParams clips)
        {
            // tolerates nulls, so we can 'keep' first

            var sampleThreshold = clips.Get("sampleThreshold", double.Parse, double.NaN);
            int movingAverageWindowRadius = clips.Get("maRadius", Misc.ParseInt, 0);
            int meanDownsampleResolution = clips.Get("meanDownsample", Misc.ParseInt, 0);
            int downsampleResolution = clips.Get("downsample", Misc.ParseInt, 0);
            double sampleOffset = clips.Get("sampleOffset", double.Parse, 0);
            double sampleScale = clips.Get("sampleScale", double.Parse, 1);

            void resample(ref double[][] trajectories, ref int samplePeriod)
            {
                if (!double.IsNaN(sampleThreshold))
                {
                    trajectories = Misc.Threshold(trajectories, sampleThreshold, 0, 1);
                }

                if (movingAverageWindowRadius > 0)
                {
                    trajectories = Misc.MovingAverages(trajectories, movingAverageWindowRadius);
                }

                if (meanDownsampleResolution > 0)
                {
                    trajectories = Misc.MeanDownsample(trajectories, meanDownsampleResolution);
                    samplePeriod *= meanDownsampleResolution;
                }

                if (downsampleResolution > 0)
                {
                    trajectories = Misc.Downsample(trajectories, downsampleResolution);
                    samplePeriod *= downsampleResolution;
                }

                if (sampleScale != 0 || sampleScale != 1)
                {
                    trajectories = trajectories.Select(t => t.Select(s => s * sampleScale + sampleOffset).ToArray()).ToArray();
                }
            }

            return resample;
        }

        public static IEnumerable<DataPoint> Reduce(IEnumerable<DataPoint> points, double threshold)
        {
            // this is terrible, but it does the job when I need it
            bool first = true;
            DataPoint last = default(DataPoint);
            DataPoint prev = default(DataPoint);
            double? lastAtan = null;
            bool yieldedPrev = false;

            foreach (var dp in points)
            {
                if (first)
                {
                    yield return dp;
                    last = dp;
                    prev = dp;
                    first = false;
                    continue;
                }

                var dx = dp.X - last.X;
                var dy = dp.Y - last.Y;

                var atan = Math.Atan2(dy, dx);
                if (lastAtan.HasValue)
                {
                    var diff = Math.Abs(atan - lastAtan.Value);
                    if (diff < threshold || diff > Math.PI * 2 - threshold)
                    { // skip
                        prev = dp;
                        yieldedPrev = false;
                        continue;
                    }
                }
                
                if (!yieldedPrev)
                    yield return prev;
                yield return dp;
                last = dp;
                prev = dp;
                lastAtan = atan;
                yieldedPrev = true;
                continue;
            }

            if (!yieldedPrev)
                yield return prev;
        }

        public static ITarget UnwrapTargetOnce(ITarget target)
        {
            if (target is ITargetDecorator decorator)
                target = decorator.Target;

            return target;
        }

        public static ITarget UnwrapTarget(ITarget target)
        {
            ITarget inner = null;
            while (inner != target)
            {
                inner = UnwrapTargetOnce(target);
            }

            return inner;
        }
    }
}
