﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public class CliMultiPlot : ICliProvider
    {
        public string Key => "multiplot";

        public CliPlot CliPlot { get; }

        public CliMultiPlot(CliPlot cliPlot)
        {
            CliPlot = cliPlot;
        }

        public void Run(TextWriter console, CliParams clips)
        {
            var filename = clips.Get("multiplot");
            var title = clips.Get("title", null);
            var outFileName = filename.Contains(";")
                ? clips.Get("out")
                : clips.Get("out", filename);

            PreparePlot(console, clips, filename, title, out var plot);

            CliPlot.PlotPlot(console, clips, plot, outFileName);
        }
        
        public bool PreparePlot(TextWriter console, CliParams clips, string filename, string title, out PlotModel plot)
        {
            var autoTier = clips.Get("autotier", bool.Parse, true);

            plot = new PlotModel() { Title = title };

            var lines = filename.Contains(";")
                ? StringHelpers.Split(filename, new CodePoint[] { ';' }, '"', '\\')
                : File.ReadAllLines(filename);

            foreach (var line in lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                    continue;

                var innerClips = new CliParams();
                innerClips.ConsumeLine(line);
                var innerFile = innerClips.Get("plot");
                
                if (!CliPlot.PreparePlot(console, innerClips, innerFile, null, out var innerPlot))
                {
                    console.WriteLine("Unable to plot innerplot " + innerFile);
                    continue;
                }

                foreach (var a in innerPlot.Axes.ToArray())
                {
                    innerPlot.Axes.Remove(a);

                    var current = plot.Axes.FirstOrDefault(_a => _a.Key == a.Key);
                    if (current != null)
                    {
                        console.WriteLine($"Combining axis with key {current.Key}, Title: {current.Title} ({a.Title})");

                        if (current.IsHorizontal() != a.IsHorizontal())
                        {
                            console.WriteLine(" - Problem: axes have different orientations");
                        }
                    }
                    else
                    {
                        if (autoTier)
                        {
                            int tier = a.PositionTier;
                            while (plot.Axes.Any(_a => _a.PositionTier == tier && _a.Position == a.Position))
                            {
                                tier++;
                            }
                            a.PositionTier = tier;
                        }

                        plot.Axes.Add(a);
                    }
                }
                foreach (var s in innerPlot.Series.ToArray())
                {
                    innerPlot.Series.Remove(s);
                    plot.Series.Add(s);
                }
                foreach (var a in innerPlot.Annotations.ToArray())
                {
                    innerPlot.Annotations.Remove(a);
                    plot.Annotations.Add(a);
                }
            }

            return true;
        }
    }
}
