﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M
{
    public class CliProject : ICliProvider
    {
        public string Key => "project";

        public void Run(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Projects stuff from other stuff");
                console.WriteLine(" - project, the source file to project from (e.g. wholesamples)");
            }

            string source = clips.Get("project");
            string fname = System.IO.Path.GetFileName(source);

            if (fname.StartsWith("wholesamples"))
            {
                ProjectFromWholeSamples(console, clips);
            }
        }

        // will assume the experiment type for the experiment type
        public void ProjectFromWholeSamples(TextWriter console, CliParams clips)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Must provide the following to extract from a wholesamples:");
                console.WriteLine(" - project, the wholesamples source file to project from");
                console.WriteLine(" - expfile, the experiment save file to reconfigure");
                console.WriteLine(" - outdir, the experiment output directory");
                console.WriteLine(" - start, the start epoch (optional)");
                console.WriteLine(" - end, the start epoch (optional)");
                console.WriteLine(" - postfix, the output postfix");
                console.WriteLine(" - noNextExposure, disables the calls to nextExposure (default false)");
            }

            string wholeSamplesSource = clips.Get("project");
            int? start = clips.Get("start", s => (int?)int.Parse(s), null);
            int? end = clips.Get("end", s => (int?)int.Parse(s), null);
            string expFile = clips.Get("expfile");
            string outdir = clips.Get("outdir", System.IO.Path.GetDirectoryName(wholeSamplesSource));
            string postfix = clips.Get("postfix");
            bool noNextExposure = clips.Get("noNextExposure", bool.Parse, false);
            ProjectionMode projectionMode = clips.Get("projectionmode", ParseProjectionMode);

            string outfile = System.IO.Path.Combine(outdir, "wholesamples" + postfix + ".dat");

            // load the experiment - inferring its type - and give us an IExperimentExtractor to work with
            var expExtractor = PopulationExperimentHelpers.LoadUnknownType(expFile, new ExperimentExtractorExperimentReceiver());

            var projectionPreparer = new BasicExtractorWholeSampleProjectorPreparer(projectionMode, noNextExposure);
            expExtractor.ProjectFromWholeSamples(wholeSamplesSource, start, end, outfile, projectionPreparer);
        }

        public static ProjectionMode ParseProjectionMode(string str)
        {
            if (str == null)
            {
                return ProjectionMode.Spin;
            }
            else if (str.Equals("spin", StringComparison.InvariantCultureIgnoreCase))
            {
                return ProjectionMode.Spin;
            }
            else if (str.Equals("evalonly", StringComparison.InvariantCultureIgnoreCase))
            {
                return ProjectionMode.EvalOnly;
            }

            throw new ArgumentException("Unrecognised ProjectionMode: \"" + str + "\"");
        }
    }
}
