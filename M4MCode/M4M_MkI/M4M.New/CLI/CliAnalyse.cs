﻿using M4M.Modular;
using Linear = MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace M4M
{
    public interface ICliAnalyser
    {
        string Prefix { get; }
        string Name { get; }
        void Run(TextWriter console, CliParams clips, string filename);
    }

    public class CliAnalyse : ICliProvider
    {
        public static IEnumerable<ICliAnalyser> CreateDefaultAnalysers()
        {
            var defaultAnalysers = new ICliAnalyser[] {
                new CliRcsAnalyser(),
                new CliExpAnalyser(),
                };

            return defaultAnalysers;
        }

        public string Key => "analyse";

        public ICliAnalyser[] Analysers { get; }

        public CliAnalyse(IEnumerable<ICliAnalyser> analysers)
        {
            Analysers = analysers.ToArray();
        }

        public void Run(TextWriter console, CliParams clips)
        {
            string infname = clips.Get("analyse");

            if (infname != null)
            {
                string outfname = clips.Get("out", infname);

                string ifname = System.IO.Path.GetFileName(infname);
                string ofname = System.IO.Path.GetFileName(outfname);

                string title = clips.Get("title", ofname);

                bool any = false;

                foreach (var analyser in Analysers)
                {
                    if (ifname.StartsWith(analyser.Prefix))
                    {
                        any = true;
                        analyser.Run(console, clips, infname);
                    }
                }

                if (any)
                    return;

                console.WriteLine("Unsure how to analyse " + infname);
            }

            console.WriteLine("Recognised files (by start of file-name) are:");

            foreach (var analyser in Analysers)
            {
                console.WriteLine(analyser.Prefix + " for " + analyser.Name);
            }
        }
    }

    public class CliRcsAnalyser : ICliAnalyser
    {
        public string Prefix => "rcs";
        public string Name => "RcsAnalyser";

        public void Run(TextWriter console, CliParams clips, string filename)
        {
            var trajectories = Analysis.LoadTrajectories(filename, out int samplePeriod);

            int start = clips.Get("start", int.Parse, 0);
            int end = clips.Get("end", int.Parse, trajectories[0].Length * samplePeriod);

            int skip = start / samplePeriod;
            int take = Math.Min((end + samplePeriod - start) / samplePeriod, trajectories[0].Length - skip);
            trajectories = trajectories.Select(traj => traj.Skip(skip).Take(take).ToArray()).ToArray();

            double[] thresholds = clips.Get("thresholds", s => s.Split(';').Select(double.Parse).ToArray());

            var samples = ComputeHuskynessesSamples(console, clips, trajectories, samplePeriod, start, out var modules);

            int hnum = 5;

            //
            console.WriteLine("## Huskyness thresholds");
            console.WriteLine("Threshold\tEpoch");
            foreach (var threshold in thresholds)
            {
                var index = DetectThreshold(samples, threshold, hnum);

                if (index > 0)
                {
                    var epoch = start + index * samplePeriod;
                    console.WriteLine($"{threshold}\t{epoch}");
                }
                else
                {
                    console.WriteLine($"{threshold}\t(not reached)");
                }
            }
            console.WriteLine("");

            //
            console.WriteLine("## Terminal DTM Info");
            var terminalDtm = Analysis.ExtractMatrix(trajectories, trajectories[0].Length - 1, modules.ElementCount, modules.ElementCount);
            SimplePdfPlotExporter.ExportToPdf(CliGenomePlotter.PlotDtm(clips, terminalDtm, "test"), "test.pdf", 200, 200, false, true, null);
            var terminalDtmInfo = new DtmInfo(terminalDtm, DtmClassification.DiscerneTrueModules(terminalDtm, DtmClassification.ComputeAutoThreshold(terminalDtm, 10.0)));
            console.WriteLine(terminalDtmInfo.Summary);
            console.WriteLine("");
        }

        public static double[][] ComputeHuskynessesSamples(TextWriter console, CliParams clips, double[][] trajectories, int samplePeriod, long startTime, out Modular.Modules modules)
        {
            double completeThreshold = clips.Get("huskythreshold", double.Parse, 0.9);
            int N = (int)Math.Round(Math.Sqrt(trajectories.Length));
            int resolution = clips.Get("resolution", int.Parse, 1);

            modules = CliPlotHelpers.GuessModules(console, clips, N);
            var mts = modules.ToArray();

            var dtms = Analysis.EnumerateMatricies(trajectories, N, N, 0, trajectories[0].Length, true);
            var huskynesses = dtms.Select(dtm => Analysis.ComputeModuleHuskyness(dtm, mts)).ToArray();
            bool complete = huskynesses.FirstIndex(hs => hs.All(h => h >= completeThreshold && h <= 1), out int completeIndex, out _);

            return huskynesses;
        }

        public static double[][] ComputeModuleIndependenceSamples(TextWriter console, CliParams clips, double[][] trajectories, int samplePeriod, long startTime, out Modular.Modules modules)
        {
            double completeThreshold = clips.Get("ModuleIndependencethreshold", double.Parse, 0.9);
            int N = (int)Math.Round(Math.Sqrt(trajectories.Length));
            int resolution = clips.Get("resolution", int.Parse, 1);

            modules = CliPlotHelpers.GuessModules(console, clips, N);
            var mts = modules.ToArray();

            var dtms = Analysis.EnumerateMatricies(trajectories, N, N, 0, trajectories[0].Length, true);
            var huskynesses = dtms.Select(dtm => Analysis.ComputeModuleHuskyness(dtm, mts)).ToArray();
            bool complete = huskynesses.FirstIndex(hs => hs.All(h => h >= completeThreshold && h <= 1), out int completeIndex, out _);

            return huskynesses;
        }

        /// <summary>
        /// returns -1 if it never qualified
        /// </summary>
        public static int DetectThreshold(double[][] samples, double threshold, int hnum)
        {
            bool complete = samples.HystericalFirstIndex(hs => hs.All(h => h >= threshold && h <= 1), hnum, out int completeIndex, out _);

            if (complete)
            {
                return completeIndex;
            }
            else
            {
                return -1;
            }
        }
    }

    public class CliExpAnalyser : ICliAnalyser
    {
        public string Prefix => "epoch";

        public string Name => "Experiment";

        public void Run(TextWriter console, CliParams clips, string filename)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Try one of: signal");
            }

            if (clips.IsSet("signal"))
            {
                AnalyseDaveSignal(console, clips, filename);
            }
            else
            {
                console.WriteLine("Try one of: signal");
            }
        }

        public static void AnalyseDistribution(TextWriter console, CliParams clips, string filename)
        {
        }

        public static void AnalyseDaveSignal(TextWriter console, CliParams clips, string filename)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Measures the 'Dave' signal for a given genome in each target of the environment");
                console.WriteLine(" - exposureepoch (default 1)");
                console.WriteLine(" - seed (default 1)");
                console.WriteLine(" - resettargets");
                console.WriteLine(" - variability, modules (default is to vary all traits independently)");
                console.WriteLine(" - closed, modules (default is the variability modules)");
                console.WriteLine(" - deltamag (default is MB pulled from expfile)");
                console.WriteLine(" - postfix (default is none)");
            }

            var exp = PopulationExperiment<DenseIndividual>.Load(filename);
            var config = exp.PopulationConfig.ExperimentConfiguration;

            int exposureEpoch = clips.Get("exposureepoch", int.Parse, 1);
            int seed = clips.Get("seed", int.Parse, 1);
            var resetTargets = clips.IsSet("resettargets");
            var variability = clips.GetOrCreate("variability", MultiModulesStringParser.Instance.Parse, () => Modules.CreateBlockModules(config.Size, 1));
            var closed = clips.Get("closed", MultiModulesStringParser.Instance.Parse, variability);
            console.WriteLine($"Variability Modules: {variability.ToString()}");
            console.WriteLine($"Closed Modules: {closed?.ToString() ?? "(null)"}");

            var rand = new CustomMersenneTwister(seed);
            var context = new ModelExecutionContext(rand);

            var template = exp.Population.ExtractAll()[0].Genome;
            var deltaMag = clips.Get("deltamag", double.Parse, config.ReproductionRules.DevelopmentalTransformationMatrixMutationSize);
            var deltaMode = new CellDeltaMode(); // TODO: make this configurable

            var postfix = clips.Get("postfix", "");

            foreach (var target in config.Targets)
            {
                console.WriteLine($"Target {target.FullName}");

                if (target is IPerfectPhenotypeTarget ppt)
                {
                    if (resetTargets)
                    {
                        ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                        target.NextExposure(rand, config.JudgementRules, exposureEpoch, ref exposureInformation);
                    }

                    target.NextGeneration(rand, config.JudgementRules);

                    var signal = MeasureDaveSignal(context, ppt, config.DevelopmentRules, config.JudgementRules, template, deltaMag, deltaMode, variability, closed, out var bothSignals, out var correctSignals, out var incorrectSignals, out var neitherSignals, out var integral, out var sampleCount);
                    console.WriteLine($" Signal: {signal}    = (c-i)/(c+i+2b)");
                    console.WriteLine($"  Both:      {bothSignals}");
                    console.WriteLine($"  Correct:   {correctSignals}");
                    console.WriteLine($"  Incorrect: {incorrectSignals}");
                    console.WriteLine($"  Neither:   {neitherSignals}");

                    console.WriteLine($" IntegralSignal: {RegularisationHelpers.L1Sum(integral)}    = L1(integral)");

                    var signalGenome = template.Clone(context, null, integral);
                    var signalAverageGenome = template.Clone(context, null, integral / sampleCount);
                    var updatedGenome = template.Clone(context, null, integral + template.TransMat);

                    Analysis.SaveGenome($"genomeSignal{postfix}.dat", signalGenome);
                    Analysis.SaveGenome($"genomeSignalAverage{postfix}.dat", signalAverageGenome);
                    Analysis.SaveGenome($"genomeUpdated{postfix}.dat", updatedGenome);
                }
                else
                {
                    console.WriteLine($" Target {target.FullName} is not a PerfectPhenotypeTarget.");
                }
            }
        }

        public static bool[,] PrepareOpen(int size, Modules closedModules)
        {
            int n = closedModules.ElementCount;

            var open = new bool[size, size];

            // open everything
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    open[r, c] = true;
                }
            }

            if (closedModules != null)
            {
                // close everything within a module
                foreach (var m in closedModules.ModuleAssignments)
                {
                    foreach (var r in m)
                    {
                        foreach (var c in m)
                        {
                            open[r, c] = false;
                        }
                    }
                }
            }

            return open;
        }

        public static double MeasureDaveSignal(ModelExecutionContext context, IPerfectPhenotypeTarget target, DevelopmentRules drules, JudgementRules jrules, DenseGenome template, double deltaMag, IDeltaMode deltaMode, Modules varitationalModules, Modules closedModules, out int bothSignals, out int correctSignals, out int incorrectSignals, out int neitherSignals, out Linear.Matrix<double> integral, out int sampleCount)
        {
            var perfect = target.PreparePerfectP();

            var correctness = perfect.Correlate();

            var n = target.Size;

            var genome = template.Clone(context);

            var open = PrepareOpen(n, closedModules);

            // zero signals
            bothSignals = 0;
            correctSignals = 0;
            incorrectSignals = 0;
            neitherSignals = 0;

            integral = Linear.CreateMatrix.Dense(n, n, 0.0);
            sampleCount = 0;

            // enumerate combinations of modules
            foreach (var gbools in Combinatorics.EnumerateBooleanAssignments(varitationalModules.ModuleCount))
            {
                sampleCount++;

                // fill genome with modules
                for (int mi = 0; mi < varitationalModules.ModuleCount; mi++)
                {
                    var v = gbools[mi] ? 1.0 : -1.0;

                    foreach (var i in varitationalModules.ModuleAssignments[mi])
                    {
                        genome.InitialState[i] = v;
                    }
                }

                // perform DeltaTest no open entries
                var deltaTest = DeltaTest.Test(genome, drules, jrules, target, deltaMag, deltaMode, open);

                // record signals
                for (int r = 0; r < n; r++)
                {
                    for (int c = 0; c < n; c++)
                    {
                        if (!open[r, c])
                            continue;

                        switch (deltaTest.GetBeneficial(r, c))
                        {
                            case DeltaResult.Both:
                                bothSignals++;
                                break;
                            case DeltaResult.Plus:
                                integral[r, c]++;
                                if (correctness[r, c] > 0)
                                    correctSignals++;
                                else if (correctness[r, c] < 0)
                                    incorrectSignals++;
                                break;
                            case DeltaResult.Minus:
                                integral[r, c]--;
                                if (correctness[r, c] > 0)
                                    incorrectSignals++;
                                else if (correctness[r, c] < 0)
                                    correctSignals++;
                                break;
                            case DeltaResult.Neither:
                                neitherSignals++;
                                break;
                        }
                    }
                }
            }

            return (correctSignals - incorrectSignals) / (double)(correctSignals + incorrectSignals + bothSignals * 2);
        }
    }
}