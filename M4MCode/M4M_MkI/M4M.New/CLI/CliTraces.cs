﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M
{   
    public class CliTracee : ICliProvider
    {
        public string Key => "tracee";
        
        public void Run(TextWriter console, CliParams clips)
        {
            string expAddress = clips.Get("tracee");
            string topdir = clips.Get("topdir", System.IO.Path.GetDirectoryName(expAddress));
            if (string.IsNullOrEmpty(topdir))
                topdir = ".";
            string cliPrefix = clips.Get("q", "");
            if (cliPrefix != "")
                cliPrefix += " ";

            RunTracee(console, clips, cliPrefix, expAddress, topdir);
        }

        public void RunTracee(TextWriter console, CliParams clips, string cliPrefix, string expAddress, string topdir)
        {
            if (clips.IsSet("help"))
            {
                console.WriteLine("Tracee parameters include:\n" +
                    "seed\n" +
                    "postfix\n" + 
                    "targets\n" +
                    "samplePeriod\n" +
                    "cycles\n" +
                    "exposureEpoch");
            }

            bool quiet = clips.IsSet("quiet");
            int? seed = clips.Get("seed", s => (int?)int.Parse(s), null);
            string postfix = clips.Get("postfix", "");
            int samplePeriod = clips.Get("samplePeriod", int.Parse, 1);
            int[] targetIndexes = clips.Get("targets", s => s.Replace(" ", "").Split(new [] { ',', ';' }).Select(int.Parse).ToArray(), new int[] { 0 });

            int cycles = clips.Get("cycles", int.Parse, 1);
            targetIndexes = Enumerable.Repeat(targetIndexes, cycles).SelectMany(l => l).ToArray();

            int startEpoch = clips.Get("exposureEpoch", int.Parse, PopulationExperiment<DenseIndividual>.Load(expAddress).Epoch);
            int epochs = clips.Get("epochs", int.Parse, 1);

            RunTracee(console, cliPrefix, expAddress, topdir, postfix, samplePeriod, seed, targetIndexes, quiet, startEpoch, epochs);
        }

        public void RunTracee(TextWriter console, string cliPrefix, string expAddress, string topdir, string postfix, int samplePeriod, int? seed, int[] targetIndexes, bool quiet, int startEpoch, int epochs)
        {
            console.WriteLine(cliPrefix + "Running tracee from prepackaged DenseIndividual experiment");
            console.WriteLine(cliPrefix + "Definition at " + expAddress);
            console.WriteLine(cliPrefix + "Running into  " + topdir);
            console.WriteLine(cliPrefix + "Target sequence is  " + String.Join(", ", targetIndexes));

            var popExperiment = PopulationExperiment<DenseIndividual>.Load(expAddress);
            
            var targets = targetIndexes.Select(ti => popExperiment.PopulationConfig.ExperimentConfiguration.Targets[ti]).ToArray();

            var rand = seed != null
                ? new MathNet.Numerics.Random.MersenneTwister(seed.Value, false)
                : new MathNet.Numerics.Random.MersenneTwister(false);
            var context = new ModelExecutionContext(rand);
            
            var trace = PopulationTrace.RunTrace(console, context, popExperiment.Population, popExperiment.PopulationConfig, targets, samplePeriod, startEpoch, epochs);
            
            Misc.EnsureDirectory(topdir);
            var outfile = System.IO.Path.Combine(topdir, "tracee" + postfix + ".dat");
            if (!quiet)
                console.WriteLine("Writing to " + outfile);
            trace.Save(outfile);
        }
    }

    public class CliTraces : ICliProvider
    {
        public string Key => "traces";
        
        public void Run(TextWriter console, CliParams clips)
        {
            string expAddress = clips.Get("traces");
            string topdir = clips.Get("topdir", System.IO.Path.GetDirectoryName(expAddress));
            if (string.IsNullOrEmpty(topdir))
                topdir = ".";
            string cliPrefix = clips.Get("q", "");
            if (cliPrefix != "")
                cliPrefix += " ";

            RunTraces(console, clips, cliPrefix, expAddress, topdir);
        }

        public void RunTraces(TextWriter console, CliParams clips, string cliPrefix, string expAddress, string topdir)
        {
            bool quiet = clips.IsSet("quiet");
            int? seed = clips.Get("seed", s => (int?)int.Parse(s), null);
            int count = clips.Get("count", int.Parse, 1000);
            string postfix = clips.Get("postfix", "");
            int samplePeriod = clips.Get("samplePeriod", int.Parse, 1);
            int[] targetIndexes = clips.Get("targets", s => s.Replace(" ", "").Split(',', ';').Select(int.Parse).ToArray(), new int[] { 0 });
            
            int cycles = clips.Get("cycles", int.Parse, 1);
            targetIndexes = Enumerable.Repeat(targetIndexes, cycles).SelectMany(l => l).ToArray();

            int startEpoch = clips.Get("exposureEpoch", int.Parse, PopulationExperiment<DenseIndividual>.Load(expAddress).Epoch);
            int epochs = clips.Get("epochs", int.Parse, 1);

            RunTraces(console, cliPrefix, expAddress, topdir, postfix, count, samplePeriod, seed, targetIndexes, quiet, startEpoch, epochs);
        }

        public void RunTraces(TextWriter console, string cliPrefix, string expAddress, string topdir, string postfix, int count, int samplePeriod, int? seed, int[] targetIndexes, bool quiet, int exposureEpoch, int epochs)
        {
            console.WriteLine(cliPrefix + "Running traces from prepackaged DenseIndividual experiment");
            console.WriteLine(cliPrefix + "Definition at " + expAddress);
            console.WriteLine(cliPrefix + "Running into  " + topdir);
            console.WriteLine(cliPrefix + "Target sequence is  " + String.Join(", ", targetIndexes));

            var popExperiment = PopulationExperiment<DenseIndividual>.Load(expAddress);
            
            var targets = targetIndexes.Select(ti => popExperiment.PopulationConfig.ExperimentConfiguration.Targets[ti]).ToArray();

            var rand = seed != null
                ? new MathNet.Numerics.Random.MersenneTwister(seed.Value, false)
                : new MathNet.Numerics.Random.MersenneTwister(false);
            var context = new ModelExecutionContext(rand);
            
            var traces = PopulationTrace.RunTraces(console, context, popExperiment.Population, popExperiment.PopulationConfig, targets, count, samplePeriod, exposureEpoch, epochs);
            
            Misc.EnsureDirectory(topdir);
            var outfile = System.IO.Path.Combine(topdir, "traces" + postfix + ".dat");
            if (!quiet)
                console.WriteLine("Writing to " + outfile);
            traces.Save(outfile);
        }
    }
}
