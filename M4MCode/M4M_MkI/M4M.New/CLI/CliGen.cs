﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace M4M
{
    public class CliGen : ICliProvider
    {
        public string Key => "gen";
        
        public string DefaultTopDir { get; }
        public Dictionary<string, Action<TextWriter, string, CliParams>> GenSets { get; }

        public void AddGenSet(string name, Action<TextWriter, string, CliParams> genSet)
        {
            GenSets.Add(name, genSet);
        }

        public void Run(TextWriter console, CliParams clips)
        {
            string set = clips.Get("gen", null);

            if (set == null || !GenSets.ContainsKey(set))
            {
                console.WriteLine("Available Sets:");

                foreach (var vk in GenSets)
                {
                    console.WriteLine(vk.Key);
                }

                return;
            }
            else
            {
                string topdir = clips.Get("topdir", DefaultTopDir);
                console.WriteLine("Generating " + set + " into " + topdir);

                Action<TextWriter, string, CliParams> genset = GenSets[set];
                genset(console, topdir, clips);
            }
        }

        public static readonly Dictionary<string, Action<TextWriter, string, CliParams>> DefaultGenSets = new Dictionary<string, Action<TextWriter, string, CliParams>>(StringComparer.InvariantCultureIgnoreCase)
        {
            ["composedense"] = M4M.ExperimentComposition.Typical.DefaultDenseExperimentComposer.Gen,
        };

        public CliGen(string defaultTopDir, Dictionary<string, Action<TextWriter, string, CliParams>> genSets)
        {
            DefaultTopDir = defaultTopDir;
            GenSets = genSets;
        }
    }
}
