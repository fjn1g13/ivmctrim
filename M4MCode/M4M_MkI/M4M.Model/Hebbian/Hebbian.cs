﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M.Hebbian
{
    // State Provider registered in GraphSerialisation.cs
    public enum HebbianType : int
    {
        Standard,
        IncreaseOnly,
        DecreaseOnly,
    }

    // State Provider registered in GraphSerialisation.cs
    public enum MatrixNormalisation : int
    {
        None,
        IterativeNormalisation, // Sinkhorn-Knopp
    }

    /// <summary>
    /// Hebbian Spinner.
    /// Hill-climbs a population of 1, before performing a Hebbian update to the TransMat.
    /// Doesn't mess with the rules: you probably want to provide a NullTransMatMutator.
    /// </summary>
    [State.StateClass]
    public class HebbianSpinner : IPopulationSpinner<DenseIndividual>
    {
        [Obsolete]
        protected HebbianSpinner()
        {
        }

        public HebbianSpinner(double fitnessWeightingPower, HebbianType hebbianType, bool signedHebbian, IPopulationSpinner<DenseIndividual> innerSpinner, bool noDiagonal, MatrixNormalisation matrixNormalisation, double fitnessRescale, double fitnessOffset)
        {
            FitnessWeightingPower = fitnessWeightingPower;
            HebbianType = hebbianType;
            SignedHebbian = signedHebbian;
            InnerSpinner = innerSpinner;
            NoDiagonal = noDiagonal;
            MatrixNormalisation = matrixNormalisation;
            FitnessRescale = fitnessRescale;
            FitnessOffset = fitnessOffset;
        }

        public string Name => "HebbianSpinner";

        public string Description => $"{Name} (FitnessWeightingPower = {FitnessWeightingPower}, HebbianType = {HebbianType}, SignedHebbian = {SignedHebbian}, InnerSpinner = {InnerSpinner?.Description ?? "(null)"}, NoDiagonal = {NoDiagonal}, MatrixNormalisation = {MatrixNormalisation}, FitnessRescale = {FitnessRescale}, FitnessOffset = {FitnessOffset})";

        /// <summary>
        /// The power to which to raise the fitness for the fitness factor on update magnitude.
        /// Fitness should be non-negative for non-zero power.
        /// </summary>
        [State.SimpleStateProperty("FitnessWeightingPower")]
        public double FitnessWeightingPower { get; private set; } = 0.0;

        [State.SimpleStateProperty("FitnessRescale")]
        public double FitnessRescale { get; private set; } = 1.0;

        [State.SimpleStateProperty("FitnessOffset")]
        public double FitnessOffset { get; private set; } = 0.0;

        [State.SimpleStateProperty("HebbianType")]
        public HebbianType HebbianType { get; private set; }

        [State.SimpleStateProperty("SignedHebbian")]
        public bool SignedHebbian { get; private set; }

        [State.SimpleStateProperty("InnerSpinner")]
        public IPopulationSpinner<DenseIndividual> InnerSpinner { get; private set; }

        [State.SimpleStateProperty("NoDiagonal")]
        public bool NoDiagonal { get; private set; } = false;

        [State.SimpleStateProperty("MatrixNormalisation")]
        public MatrixNormalisation MatrixNormalisation { get; private set; } = MatrixNormalisation.None;

        public IReadOnlyList<IndividualJudgement<DenseIndividual>> SpinPopulation(Population<DenseIndividual> population, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<DenseIndividual> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<DenseIndividual>>> judgementFeedback, int eliteCount, bool hillclimberMode)
        {
            if (population.Count != 1)
                throw new ArgumentException("Population must have exactly 1 individual to use a HebbianSpinner");

            // no Gen0
            if (InnerSpinner != null)
            {
                InnerSpinner.SpinPopulation(population, context, rrules, drules, jrules, target, selectorPreparer, generations, judgementFeedback, eliteCount, hillclimberMode);
            }
            else
            {
                // spin as normal
                population.SpinPopulation(context, rrules, drules, jrules, target, selectorPreparer, generations, judgementFeedback, eliteCount, hillclimberMode);
            }

            // now extract and perform Hebbian update
            var individual = population.ExtractAll()[0].Clone(context);

            double q = rrules.DevelopmentalTransformationMatrixMutationSize;
            HebbianStepInplace(individual, SignedHebbian, q, HebbianType, context, rrules, drules, jrules, target, FitnessWeightingPower, NoDiagonal, MatrixNormalisation, FitnessRescale, FitnessOffset);
            individual.DevelopInplace(context, drules);

            var cloned = individual.Clone(context);
            var j = cloned.Judge(jrules, target);

            population.Introduce(individual);
            judgementFeedback?.Invoke(generations, new[] { new IndividualJudgement<DenseIndividual>(individual, individual.Judge(jrules, target)) });

            return new[] { new IndividualJudgement<DenseIndividual>(cloned, j) };
        }

        public static void HebbianStepInplace(DenseIndividual individual, bool signed, double mag, HebbianType hebbianType, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, double FitnessWeightingPower, bool noDiagonal, MatrixNormalisation matrixNormalisation, double fitnessRescale, double fitnessOffset)
        {
            var mat = individual.Genome.TransMat;
            var p = individual.Phenotype.Vector;

            var rowSums = matrixNormalisation == MatrixNormalisation.IterativeNormalisation
                ? mat.RowSums()
                : null;
            var colSums = matrixNormalisation == MatrixNormalisation.IterativeNormalisation
                ? mat.ColumnSums()
                : null;

            IEnumerable<MatrixEntryAddress> entries = individual.Genome.TransMatIndexOpenEntries ?? MatrixEntryAddress.CompleteLazy(individual.Genome.Size, individual.Genome.Size);

            var rand = context.Rand;

            double fitnessFactor;

            if (FitnessWeightingPower == 0.0)
            {
                // don't measure
                fitnessFactor = 1.0;
            }
            else
            {
                target.NextGeneration(rand, jrules);
                individual.DevelopInplace(context, drules);
                double w = individual.Judge(jrules, target).CombinedFitness;
                w = w * fitnessRescale + fitnessOffset;
                fitnessFactor = Math.Pow(w, FitnessWeightingPower);
            }

            foreach (var entry in entries)
            {
                if (noDiagonal && entry.Row == entry.Col)
                {
                    continue;
                }

                if (signed)
                {
                    int rs = Math.Sign(p[entry.Row]);
                    int cs = Math.Sign(p[entry.Col]);
                    int sign = Math.Sign(rs * cs);

                    if (sign > 0)
                    {
                        if (hebbianType == HebbianType.DecreaseOnly)
                            continue;

                        mat[entry.Row, entry.Col] = rrules.DevelopmentalTransformationMatrixClamping.Clamp(mat[entry.Row, entry.Col] + mag * fitnessFactor);
                    }
                    else if (sign < 0)
                    {
                        if (hebbianType == HebbianType.IncreaseOnly)
                            continue;

                        mat[entry.Row, entry.Col] = rrules.DevelopmentalTransformationMatrixClamping.Clamp(mat[entry.Row, entry.Col] - mag * fitnessFactor);
                    }
                }
                else
                {
                    var product = p[entry.Row] * p[entry.Col];
                    int sign = Math.Sign(product);

                    if (sign > 0)
                    {
                        if (hebbianType == HebbianType.DecreaseOnly)
                            continue;

                        mat[entry.Row, entry.Col] = rrules.DevelopmentalTransformationMatrixClamping.Clamp(mat[entry.Row, entry.Col] + product * mag * fitnessFactor);
                    }
                    else if (sign < 0)
                    {
                        if (hebbianType == HebbianType.IncreaseOnly)
                            continue;

                        mat[entry.Row, entry.Col] = rrules.DevelopmentalTransformationMatrixClamping.Clamp(mat[entry.Row, entry.Col] - product * mag * fitnessFactor);
                    }
                }
            }

            switch (matrixNormalisation)
            {
                case MatrixNormalisation.IterativeNormalisation:
                    var normalised = Misc.IterativeNormalise(mat, rowSums, colSums);
                    normalised.CopyTo(mat);
                    break;
            }
        }
    }
}
