﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Linear = MathNet.Numerics.LinearAlgebra; // reasonable quality API reference here: https://numerics.mathdotnet.com/api/MathNet.Numerics.LinearAlgebra/ (inline isn't good enough)
using RandomSource = MathNet.Numerics.Random.RandomSource;
using static M4M.Misc;
using M4M.State;

namespace M4M
{
    /// <summary>
    /// The model execution context, which does little more than provide random numbers really, because nobody wants to use matrix pooling.
    /// </summary>
    public class ModelExecutionContext
    {
        public ModelExecutionContext(RandomSource rand)
        {
            TrueRandom = rand;
            Rand = rand;
        }

        /// <summary>
        /// Attempts to disable the Randomness
        /// Returns true if it was not already disabled, passing out the Random object (to be passed to EnableRandom to reenable Random)
        /// </summary>
        public bool TryDisableRandom(out RandomSource rand)
        {
            if (Rand == null)
            {
                rand = null;
                return false;
            }

            Rand = null;
            rand = TrueRandom;
            return true;
        }
        
        /// <summary>
        /// Attempts to disable the Randomness
        /// Throws if Randomness is already disabled
        /// </summary>
        public RandomSource DisableRandom()
        {
            if (Rand == null)
                throw new Exception("Random already disabled; consider using TryDisableRandom");

            Rand = null;
            return TrueRandom;
        }
        
        /// <summary>
        /// Attempts to enable randomness
        /// Throws if Randomness is already enabled, or an inccorect RandomSource is provided
        /// </summary>
        public void EnableRandom(RandomSource rand)
        {
            if (Rand != null)
                throw new Exception("Invalid call to EnableRandom: was not disabled");
            if (rand != TrueRandom)
                throw new Exception("Invalid call to EnableRandom: randomsource was different from that supplied by DisableRandom");
            Rand = TrueRandom;
        }
        
        /// <summary>
        /// Our random source; constant
        /// </summary>
        private RandomSource TrueRandom { get; }

        /// <summary>
        /// The ModelExecutionContext's random source; may be null if the RandomSource is disabled
        /// (sorry about the awful API)
        /// </summary>
        public RandomSource Rand { get; private set; }

        private Dictionary<MatrixDimensions, MatrixPool> MatrixPools = new Dictionary<MatrixDimensions, MatrixPool>(MatrixDimensionsComparer.Instance);
        public MatrixPool GetMatrixPool(int rows, int cols)
        {
            var dim = new MatrixDimensions(rows, cols);

            if (!MatrixPools.TryGetValue(dim, out var pool))
            {
                MatrixPools.Add(dim, pool = new MatrixPool(dim, 100));
            }

            return pool;
        }

        public Linear.Matrix<double> Clone(Linear.Matrix<double> original)
        {
            if (!MatrixPool.EnableMatrixPools)
                return original.Clone();

            return GetMatrixPool(original.RowCount, original.ColumnCount).Clone(original);
        }

        public void Release(Linear.Matrix<double> mat)
        {
            if (!MatrixPool.EnableMatrixPools)
                return;

            GetMatrixPool(mat.RowCount, mat.ColumnCount).Release(mat);
        }
    }

    public class RegularisationHelpers
    {
        /// <summary>
        /// Simple row-major L1 Sum.
        /// </summary>
        /// <param name="mat"></param>
        /// <returns></returns>
        public static double L1Sum(Linear.Matrix<double> mat)
        {
            double acc = 0.0;

            for (int i = 0; i < mat.RowCount; i++)
            {
                for (int j = 0; j < mat.ColumnCount; j++)
                {
                    acc += Math.Abs(mat[i, j]);
                }
            }

            return acc;
        }

        /// <summary>
        /// Row-major L1 Sum.
        /// </summary>
        /// <param name="mat"></param>
        /// <returns></returns>
        public static double RowL1Sum(Linear.Matrix<double> mat)
        {
            if (mat.Storage is Linear.Storage.DenseColumnMajorMatrixStorage<double> dcmms)
            {
                int w = mat.RowCount;
                int h = mat.RowCount;

                double d = 0.0;
                var data = dcmms.Data;
                for (int r = 0; r < w; r++)
                {
                    for (int c = 0; c < h; c++)
                    {
                        d += Math.Abs(data[c * w + r]);
                    }
                }
                return d;
            }
            else if (mat.Storage is Linear.Storage.DiagonalMatrixStorage<double> dms)
            {
                double d = 0.0;
                var data = dms.Data;
                for (int i = 0; i < data.Length; i++)
                {
                    d += Math.Abs(data[i]);
                }
                return d;
            }
            else
            {
                int w = mat.RowCount;
                int h = mat.RowCount;

                double d = 0.0;
                for (int i = 0; i < w; i++)
                {
                    for (int j = 0; j < h; j++)
                    {
                        d += Math.Abs(mat[i, j]);
                    }
                }
                return d;
            }
        }

        /// <summary>
        /// Column-major L1 Sum: faster than <see cref="L1Sum(Linear.Matrix{double})"/>, but not compatible.
        /// </summary>
        /// <param name="mat"></param>
        /// <returns></returns>
        public static double ColL1Sum(Linear.Matrix<double> mat)
        {
            if (mat.Storage is Linear.Storage.DenseColumnMajorMatrixStorage<double> dcmms)
            {
                double d = 0.0;
                var data = dcmms.Data;
                for (int i = 0; i < data.Length; i++)
                {
                    d += Math.Abs(data[i]);
                }
                return d;
            }
            else if (mat.Storage is Linear.Storage.DiagonalMatrixStorage<double> dms)
            {
                double d = 0.0;
                var data = dms.Data;
                for (int i = 0; i < data.Length; i++)
                {
                    d += Math.Abs(data[i]);
                }
                return d;
            }
            else
            {
                int w = mat.RowCount;
                int h = mat.RowCount;

                double d = 0.0;
                for (int j = 0; j < h; j++)
                {
                    for (int i = 0; i < w; i++)
                    {
                        d += Math.Abs(mat[i, j]);
                    }
                }
                return d;
            }
        }

        public static double L2Sum(Linear.Matrix<double> mat)
        {
            double acc = 0.0;

            for (int i = 0; i < mat.RowCount; i++)
            {
                for (int j = 0; j < mat.ColumnCount; j++)
                {
                    double x = mat[i, j];
                    acc += x * x;
                }
            }

            return acc;
        }

        /// <summary>
        /// Row-major L2 Sum.
        /// </summary>
        /// <param name="mat"></param>
        /// <returns></returns>
        public static double RowL2Sum(Linear.Matrix<double> mat)
        {
            if (mat.Storage is Linear.Storage.DenseColumnMajorMatrixStorage<double> dcmms)
            {
                int w = mat.RowCount;
                int h = mat.RowCount;

                double d = 0.0;
                var data = dcmms.Data;
                for (int r = 0; r < w; r++)
                {
                    for (int c = 0; c < h; c++)
                    {
                        var x = data[c * w + r];
                        d += x * x;
                    }
                }
                return d;
            }
            else if (mat.Storage is Linear.Storage.DiagonalMatrixStorage<double> dms)
            {
                double d = 0.0;
                var data = dms.Data;
                for (int i = 0; i < data.Length; i++)
                {
                    var x = data[i];
                    d += x * x;
                }
                return d;
            }
            else
            {
                int w = mat.RowCount;
                int h = mat.RowCount;

                double d = 0.0;
                for (int i = 0; i < w; i++)
                {
                    for (int j = 0; j < h; j++)
                    {
                        var x = mat[i, j];
                        d += x * x;
                    }
                }
                return d;
            }
        }
    }
    
    /// <summary>
    /// Represents a regularisation function, that can compute the cost component of fitness for a given genome.
    /// </summary>
    /// <typeparam name="TG">The type of genome.</typeparam>
    public interface IRegularisationFunction<in TG> where TG : IGenome
    {
        string Name { get; }
        double ComputeCost(TG genome);
    }

    [StateClass]
    public class ConstantEquivalentRegularisationFunction : IRegularisationFunction<IGenome>
    {
        public string Name => "ConstEq";
        public double ComputeCost(IGenome g) => 1.0;
    }

    [StateClass]
    public class L1EquivalentRegularisationFunction : IRegularisationFunction<IGenome>
    {
        public string Name => "L1Eq";
        public double ComputeCost(IGenome g) => RegularisationHelpers.L1Sum(g.TransMat) / ((double)g.Size * g.Size);
    }

    [StateClass]
    public class RowL1EquivalentRegularisationFunction : IRegularisationFunction<IGenome>
    {
        public string Name => "RowL1Eq";
        public double ComputeCost(IGenome g) => RegularisationHelpers.RowL1Sum(g.TransMat) / ((double)g.Size * g.Size);
    }

    [StateClass]
    public class ColL1EquivalentRegularisationFunction : IRegularisationFunction<IGenome>
    {
        public string Name => "ColL1Eq";
        public double ComputeCost(IGenome g) => RegularisationHelpers.ColL1Sum(g.TransMat) / ((double)g.Size * g.Size);
    }

    [StateClass]
    public class L2EquivalentRegularisationFunction : IRegularisationFunction<IGenome>
    {
        public string Name => "L2Eq";
        public double ComputeCost(IGenome g) => RegularisationHelpers.L2Sum(g.TransMat) / ((double)g.Size * g.Size);
    }
    
    [StateClass]
    public class LHalfEquivalentRegularisationFunction : IRegularisationFunction<IGenome>
    {
        public string Name => "LHalfEq";
        public double ComputeCost(IGenome g) => g.TransMat.Enumerate().Sum(d => Math.Sqrt(Math.Abs(d))) / ((double)g.Size * g.Size);
    }
    
    [StateClass]
    public class MMSO1RegularisationFunction : IRegularisationFunction<IGenome>
    {
        public string Name => "MMSO1";
        public double ComputeCost(IGenome g) => g.TransMat.Enumerate().Sum(d => 1.0 * (d < 0 ? -d / (-d + 1) : d / (d + 1))) / ((double)g.Size * g.Size);
    }

    [StateClass]
    public class L1And2 : IRegularisationFunction<IGenome>
    {
        [Obsolete]
        private L1And2()
        { }

        public L1And2(double a, double b)
        {
            this.a = a;
            this.b = b;
        }

        [SimpleStateProperty("a")]
        public double a { get; private set; }

        [SimpleStateProperty("b")]
        public double b { get; private set; }

        public string Name => $"L1And2a{a}b{b}";
        public double ComputeCost(IGenome g) => (a * RegularisationHelpers.RowL1Sum(g.TransMat) + b * RegularisationHelpers.RowL2Sum(g.TransMat)) / ((double)g.Size * g.Size);
    }

    [StateClass]
    public class MMSO : IRegularisationFunction<IGenome>
    {
        [Obsolete]
        private MMSO()
        { }

        public MMSO(double a, double b)
        {
            this.a = a;
            this.b = b;
        }

        [SimpleStateProperty("a")]
        public double a { get; private set; }
        
        [SimpleStateProperty("b")]
        public double b { get; private set; }

        // Michaelis-Menten Symetric... ????
        public string Name => $"MMSOa{a}b{b}";
        public double ComputeCost(IGenome g) => g.TransMat.Enumerate().Sum(d => b * (d < 0 ? -d / (-d + a) : d / (d + a))) / ((double)g.Size * g.Size);
    }

    [StateClass]
    public class MMSOLin : IRegularisationFunction<IGenome>
    {
        [Obsolete]
        private MMSOLin()
        { }

        public MMSOLin(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        [SimpleStateProperty("a")]
        public double a { get; private set; }

        [SimpleStateProperty("b")]
        public double b { get; private set; }

        [SimpleStateProperty("c")]
        public double c { get; private set; }
        
        public string Name => $"MMSOLin(a{a}b{b}c{c})";
        public double ComputeCost(IGenome g) => g.TransMat.Enumerate().Sum(d => c * Math.Abs(d) + b * (d < 0 ? -d / (-d + a) : d / (d + a))) / ((double)g.Size * g.Size);
    }

    [StateClass]
    public class L1and2 : IRegularisationFunction<IGenome>
    {
        [Obsolete]
        private L1and2()
        { }

        /// <summary>
        /// Prepares an L1and2 regularisation function
        /// </summary>
        /// <param name="z">The representaton of the L2 component</param>
        public L1and2(double z)
        {
            this.z = z;
        }
        
        /// <summary>
        /// The representaton of the L2 component
        /// </summary>
        [SimpleStateProperty("z")]
        public double z { get; private set; }
        
        public string Name => $"L1and2(z={z})";
        public double ComputeCost(IGenome g) => ((1-z) * RegularisationHelpers.L1Sum(g.TransMat) + z * RegularisationHelpers.L2Sum(g.TransMat)) / ((double)g.Size * g.Size);
    }

    [StateClass]
    public class JudgementRules
    {
        [Obsolete]
        protected JudgementRules()
        { }

        public static IRegularisationFunction<IGenome> ConstantEquivalent = new ConstantEquivalentRegularisationFunction();
        public static IRegularisationFunction<IGenome> L1Equivalent = new L1EquivalentRegularisationFunction();
        public static IRegularisationFunction<IGenome> RowL1Equivalent = new RowL1EquivalentRegularisationFunction();
        public static IRegularisationFunction<IGenome> ColL1Equivalent = new ColL1EquivalentRegularisationFunction();
        public static IRegularisationFunction<IGenome> L2Equivalent = new L2EquivalentRegularisationFunction();
        public static IRegularisationFunction<IGenome> LHalfEquivalent = new LHalfEquivalentRegularisationFunction();
        public static IRegularisationFunction<IGenome> MMSO1 = new MMSO1RegularisationFunction();

        /// <summary>
        /// a|x| + bx^2
        /// Gradient at x=0 is 
        /// </summary>
        public static IRegularisationFunction<IGenome> L1And2(double a, double b) => new L1And2(a, b);

        /// <summary>
        /// bx/(x+a)
        /// Gradient at x=0 is b/a
        /// </summary>
        public static IRegularisationFunction<IGenome> MMSO(double a, double b) => new MMSO(a, b);

        /// <summary>
        /// bx/(x+a) + cx
        /// Gradient at x=0 is b/a + c
        /// </summary>
        public static IRegularisationFunction<IGenome> MMSOLin(double a, double b, double c) => new MMSOLin(a, b, c);

        /// <summary>
        /// a=a, b=a*(1-q), c=q
        /// bx/(x+a) + cx
        /// Gradient at x=0 is 1
        /// </summary>
        public static IRegularisationFunction<IGenome> MMSOLin(double a, double q) => MMSOLin(a, a * (1 - q), q);
        
        /// <summary>
        /// (1-z)*x + z*x*x
        /// Gradient is (1-z) + z*2*x
        /// </summary>
        public static IRegularisationFunction<IGenome> L1and2(double z) => new L1and2(z);

        /// <summary>
        /// The factor to multiply the regularisation term (c) by when judging a phenotype
        /// λ in the paper
        /// </summary>
        [SimpleStateProperty("RegularisationFactor")]
        public double RegularisationFactor { get; private set; }

        /// <summary>
        /// The function for the regulariser
        /// ϕ
        /// </summary>
        [SimpleStateProperty("RegularisationFunction")]
        public IRegularisationFunction<IGenome> RegularisationFunction { get; private set; }
        
        /// <summary>
        /// The factor to multiply the N(0, 1) term by when adding noise to a target
        /// κ in the paper
        /// </summary>
        [SimpleStateProperty("NoiseFactor")]
        public double NoiseFactor { get; private set; }

        /// <summary>
        /// Assembles the JudgementRules
        /// </summary>
        /// <param name="regularisationFactor">The factor to multiply the regularisation term by (λ)</param>
        /// <param name="regularisationFunction">The function for the regulariser (ϕ)</param>
        /// <param name="noiseFactor">The factor to multiply the N(0, 1) term by when adding noise to a target (κ)</param>
        public JudgementRules(double regularisationFactor, IRegularisationFunction<IGenome> regularisationFunction, double noiseFactor)
        {
            RegularisationFactor = regularisationFactor;
            RegularisationFunction = regularisationFunction;
            NoiseFactor = noiseFactor;
        }
    }

    public interface ISquash
    {
        string Name { get; }
        double Squash(double expression);

        /// <summary>
        /// A one-time delegate (you can pass me around, instead of having to manage your own copy)
        /// </summary>
        Func<double, double> Delegate { get; }
    }

    [StateClass]
    public class NoneSquash : ISquash
    {
        public static readonly NoneSquash Instance = new NoneSquash();

        private NoneSquash()
        {
            Delegate = Squash;
        }

        public string Name => "None";

        public Func<double, double> Delegate { get; }

        public double Squash(double x)
        {
            return x;
        }
    }

    [StateClass]
    public class TanhSquash : ISquash
    {
        private TanhSquash()
        {
            Init();
        }

        public TanhSquash(double innerCoef, string name = null)
            : this()
        {
            InnerCoef = innerCoef;
            Name = name ?? $"Tanh({InnerCoef}*x)";
        }

        private void Init()
        {
            Delegate = Squash;
        }

        [SimpleStateProperty("Name")]
        public string Name { get; private set; }

        [SimpleStateProperty("InnerCoef")]
        public double InnerCoef { get; private set; }

        public Func<double, double> Delegate { get; private set; }

        public double Squash(double x)
        {
            return Math.Tanh(x * InnerCoef);
        }
    }

    [StateClass]
    public class PositiveTanhSquash : ISquash
    {
        private PositiveTanhSquash()
        {
            Init();
        }

        public PositiveTanhSquash(double innerCoef, string name = null)
            : this()
        {
            InnerCoef = innerCoef;
            Name = name ?? $"(Tanh({InnerCoef}*x)+1)/2";
        }

        private void Init()
        {
            Delegate = Squash;
        }

        [SimpleStateProperty("Name")]
        public string Name { get; private set; }

        [SimpleStateProperty("InnerCoef")]
        public double InnerCoef { get; private set; }

        public Func<double, double> Delegate { get; private set; }

        public double Squash(double x)
        {
            return (Math.Tanh(x * InnerCoef) + 1) / 2;
        }
    }

    [StateClass]
    public class MichaelisMenton : ISquash
    {
        public MichaelisMenton()
        {
            Delegate = Squash;
        }

        public string Name => "Michaelis-Menten";
        
        public Func<double, double> Delegate { get; }

        public double Squash(double x)
        {
            return x / (x + 1);
        }
    }

    [StateClass]
    public class MichaelisMentenPaush : ISquash
    {
        public MichaelisMentenPaush()
        {
            Delegate = Squash;
        }

        public string Name => "Michaelis-Menten (Paush)";
        
        public Func<double, double> Delegate { get; }

        public double Squash(double x)
        {
            return x < 0 ? 0 : x/(x+1);
        }
    }

    [StateClass]
    public class MichaelisMentenSymOdd : ISquash
    {
        public MichaelisMentenSymOdd()
        {
            Delegate = Squash;
        }

        public string Name => "Michaelis-Menten (SymOdd)";
        
        public Func<double, double> Delegate { get; }

        public double Squash(double x)
        {
            return x < 0 ? x/(-x+1) : x/(x+1);
        }
    }

    [StateClass]
    public class DevelopmentRules
    {
        [Obsolete]
        private DevelopmentRules()
        { }

        public static ISquash None = NoneSquash.Instance;
        public static ISquash Tanh = new TanhSquash(1.0, "Tanh(x)");
        public static ISquash TanhHalf = new TanhSquash(0.5, "Tanh(x/2)");
        public static ISquash TanhTen = new TanhSquash(10.0, "Tanh(10*x)");

        public static ISquash PositiveTanhHalf = new PositiveTanhSquash(0.5, "(Tanh(x/2)+1)/2");

        public static ISquash MichaelisMenten = new MichaelisMenton();
        public static ISquash MichaelisMentenPaush = new MichaelisMentenPaush();
        public static ISquash MichaelisMentenSymOdd = new MichaelisMentenSymOdd();

        public static ISquash[] CommonSquashes = new ISquash[]
        {
            None,
            Tanh,
            TanhHalf,
            TanhTen,
            PositiveTanhHalf,
            MichaelisMenten,
            MichaelisMentenPaush,
            MichaelisMentenSymOdd,
        };

        /// <summary>
        /// Number of developmental time steps
        /// T in the paper
        /// </summary>
        [SimpleStateProperty("TimeSteps")]
        public int TimeSteps { get; private set; }

        /// <summary>
        /// Trait update rate
        /// τ1 in the paper
        /// </summary>
        [SimpleStateProperty("UpdateRate")]
        public double UpdateRate { get; private set; }

        /// <summary>
        /// Trait decay rate
        /// τ2 in the paper
        /// </summary>
        [SimpleStateProperty("DecayRate")]
        public double DecayRate { get; private set; }

        /// <summary>
        /// Train update squash function
        /// σ in the paper
        /// </summary>
        [SimpleStateProperty("Squash")]
        public ISquash Squash { get; private set; }

        /// <summary>
        /// The scale by which to scale the developed pehnotype.
        /// </summary>
        [SimpleStateProperty("RescaleScale")]
        public double RescaleScale { get; private set; } = 1.0;

        /// <summary>
        /// The amount by which to offset the developed phenotype after applying the <see cref="RescaleScale"/>.
        /// </summary>
        [SimpleStateProperty("RescaleOffset")]
        public double RescaleOffset { get; private set; } = 0.0;

        /// <summary>
        /// Assembles the DevelopmentRules
        /// </summary>
        /// <param name="timeSteps">Number of developmental time steps (T)</param>
        /// <param name="updateRate">Trait update rate (τ1)</param>
        /// <param name="decayRate">Trait decay rate (τ2)</param>
        /// <param name="squash">Train update squash function (σ)</param>
        public DevelopmentRules(int timeSteps, double updateRate, double decayRate, ISquash squash)
        {
            TimeSteps = timeSteps;
            UpdateRate = updateRate;
            DecayRate = decayRate;
            Squash = squash;
        }

        /// <summary>
        /// Assembles the DevelopmentRules
        /// </summary>
        /// <param name="timeSteps">Number of developmental time steps (T)</param>
        /// <param name="updateRate">Trait update rate (τ1)</param>
        /// <param name="decayRate">Trait decay rate (τ2)</param>
        /// <param name="squash">Train update squash function (σ)</param>
        /// <param name="rescaleScale">Trait rescale scale</param>
        /// <param name="rescaleOffset">Trait rescale offset</param>
        public DevelopmentRules(int timeSteps, double updateRate, double decayRate, ISquash squash, double rescaleScale, double rescaleOffset)
        {
            TimeSteps = timeSteps;
            UpdateRate = updateRate;
            DecayRate = decayRate;
            Squash = squash;
            RescaleScale = rescaleScale;
            RescaleOffset = rescaleOffset;
        }
    }

    [StateClass]
    public class ReproductionRules
    {
        [Obsolete]
        private ReproductionRules()
        { }

        public static readonly Range ClampAny = new Range(double.NegativeInfinity, double.PositiveInfinity);
        public static readonly Range ClampAbs1 = new Range(-1, 1);
        public static readonly Range ClampZeroOne = new Range(0, 1);

        /// <summary>
        /// The maximum magnitude of an applied mutation to the initial State (G)
        /// M_G
        /// </summary>
        [SimpleStateProperty("InitialStateMutationSize")]
        public double InitialStateMutationSize { get; private set; }

        /// <summary>
        /// The type of mutation applied to the initial state
        /// </summary>
        [SimpleStateProperty("InitialStateMutationType")]
        //[NetState.SoftState.SoftPropertyAttribute("InitialStateMutationType", null, typeof(NetState.AutoState.AutoEnumStateProvider<NoiseType, int, NetState.Serialisation.IntStateProvider>))]
        public NoiseType InitialStateMutationType { get; private set; }

        /// <summary>
        /// The maximum magnitude of an applied mutation to the developmental transfomration matrix
        /// M_B
        /// </summary>
        [SimpleStateProperty("DevelopmentalTransformationMatrixMutationSize")]
        public double DevelopmentalTransformationMatrixMutationSize { get; private set; }

        /// <summary>
        /// The type of mutation applied to the developmental transfomration matrix
        /// </summary>
        [SimpleStateProperty("DevelopmentalTransformationMatrixMutationType")]
        public NoiseType DevelopmentalTransformationMatrixMutationType { get; private set; }

        /// <summary>
        /// The probability of mutating B every time we mutate consider mutating G
        /// R_B
        /// </summary>
        [SimpleStateProperty("DevelopmentalTransformationMatrixRate")]
        public double DevelopmentalTransformationMatrixRate { get; private set; }
        
        /// <summary>
        /// Whether to not mutate G when mutating B
        /// </summary>
        [SimpleStateProperty("ExclusiveBMutation")]
        public bool ExclusiveBMutation { get; private set; }

        /// <summary>
        /// The number of times to update an element of the initial state vector (e.g. will pick one at random and update it more than once (may pick different traits, or the same one, each draw is uniform)
        /// </summary>
        [SimpleStateProperty("InitialTraitUpdates")]
        public int InitialTraitUpdates { get; private set; }

        /// <summary>
        /// The Range to clamp the initial state elements to
        /// Default is [-1, 1]
        /// </summary>
        [SimpleStateProperty("InitialStateClamping")]
        public Range InitialStateClamping { get; private set; }

        /// <summary>
        /// The Range to clamp the initial state elements to
        /// Default is [NegativeInfinity, PositiveInfinity]
        /// </summary>
        [SimpleStateProperty("DevelopmentalTransformationMatrixClamping")]
        public Range DevelopmentalTransformationMatrixClamping { get; private set; } = ClampAny;

        /// <summary>
        /// Assembles the ReproductionRules
        /// </summary>
        /// <param name="initialStateMutationSize">The maximum magnitude of an applied mutation to the initial State (completely independant of B)</param>
        /// <param name="developmentalTransformationMatrixMutationSize">The maximum magnitude of an applied mutation to the initial  B (completely independant of G)</param>
        /// <param name="developmentalTransformationMatrixMutationRate">The probability of mutating B every time we mutate consider mutating G</param>
        /// <param name="exclusiveBMutation">Whether to not mutate G when mutating B</param>
        /// <param name="initialTraitUpdates">The number of times to update an element of the initial state vector</param>
        /// <param name="initialStateClamping">The Range to clamp the initial state elements to</param>
        public ReproductionRules(double initialStateMutationSize, double developmentalTransformationMatrixMutationSize, double developmentalTransformationMatrixMutationRate, bool exclusiveBMutation = false, int initialTraitUpdates = 1, Range initialStateClamping = null, NoiseType initialStateMutationType = NoiseType.Uniform, NoiseType developmentalTransformationMatrixMutationType = NoiseType.Uniform, Range developmentalTransformationMatrixClamping = null)
        {
            InitialStateMutationSize = initialStateMutationSize;
            InitialStateMutationType = initialStateMutationType;
            DevelopmentalTransformationMatrixMutationSize = developmentalTransformationMatrixMutationSize;
            DevelopmentalTransformationMatrixMutationType = developmentalTransformationMatrixMutationType;
            DevelopmentalTransformationMatrixRate = developmentalTransformationMatrixMutationRate;
            ExclusiveBMutation = exclusiveBMutation;
            InitialTraitUpdates = initialTraitUpdates;
            InitialStateClamping = initialStateClamping ?? ClampAbs1; // use default if null
            DevelopmentalTransformationMatrixClamping = developmentalTransformationMatrixClamping ?? ClampAny; // use default if null
        }
    }

    public interface ITargetPackage
    {
        string Name { get; }
        int TargetSize { get; }
        IReadOnlyList<ITarget> Targets { get; }
    }

    public class TargetPackage : ITargetPackage
    {
        public TargetPackage(string name, IReadOnlyList<ITarget> targets)
        {
            Name = name;
            Targets = targets;
        }

        public string Name { get; }
        public int TargetSize => Targets[0].Size;
        public IReadOnlyList<ITarget> Targets { get; }
    }

    public struct ExposureInformation
    {
        public ExposureInformation(int exposureDuration)
        {
            ExposureDuration = exposureDuration;
        }

        /// <summary>
        /// Indicates the duration (in generations) of the next exposure
        /// </summary>
        public int ExposureDuration { get; set; }
    }

    public static class TargetHelpers
    {
        public static ITarget Unwrap(this ITarget target)
        {
            while (target is ITargetDecorator decorator)
                target = decorator.Target;

            return target;
        }
    }

    public interface ITarget
    {
        /// <summary>
        /// The size of the Target, and so the size of the Phenotypes expected
        /// </summary>
        int Size { get; }

        /// <summary>
        /// A friendly name for the target
        /// </summary>
        string FriendlyName { get; }

        /// <summary>
        /// A full name for the target
        /// </summary>
        string FullName { get; }    

        /// <summary>
        /// A detailed description of the target
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Computes the fitness of the Phenotype given this Target
        /// </summary>
        /// <param name="p">The phenotype</param>
        /// <returns>The fitness of the Phenotype</returns>
        double Judge(Phenotype p);

        /// <summary>
        /// Prepares the Target for the next generation (e.g. by adding noise, etc.)
        /// </summary>
        /// <param name="rand">Random source for the noise</param>
        /// <param name="jrules">Judgement Rules</param>
        /// <returns>True if the objective has changed, thereby invalidating previous evaluations</returns>
        bool NextGeneration(MathNet.Numerics.Random.RandomSource rand, JudgementRules jrules);

        /// <summary>
        /// Prepares the target for the next exposure (e.g. by changing something? I don't know)
        /// </summary>
        /// <param name="rand">Random source for the noise</param>
        /// <param name="jrules">Judgement Rules</param>
        /// <param name="epoch">The current epoch</param>
        /// <param name="exposureInformation">Information about the next exposure, which can be modified by the target</param>
        /// <returns>True if the objective has changed, thereby invalidating previous evaluations</returns>
        bool NextExposure(MathNet.Numerics.Random.RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation);
    }

    /// <summary>
    /// A target where we can find a perfect phenotype vector.
    /// </summary>
    public interface IPerfectPhenotypeTarget : ITarget
    {
        Linear.Vector<double> PreparePerfectP();
    }


    [StateClass]
    public class VectorTarget : IPerfectPhenotypeTarget
    {
        /// <summary>
        /// The vector we are targetting
        /// S in the paper
        /// </summary>
        [VectorStateProperty("Vector")]
        public Linear.Vector<double> Vector { get; private set; }

        /// <summary>
        /// Per-Generation Noisy Vector
        /// Modify inplace
        /// </summary>
        [VectorStateProperty("NoisyVector")]
        private Linear.Vector<double> NoisyVector { get; set; }

        /// <summary>
        /// The size of the Target
        /// </summary>
        public int Size => Vector.Count;
        
        /// <summary>
        /// A friendly name for the target
        /// </summary>
        [SimpleStateProperty("FriendlyName")]
        public string FriendlyName { get; private set; }

        /// <summary>
        /// A full name for the target
        /// </summary>
        [SimpleStateProperty("FullName")]
        public string FullName { get; private set; }
        
        /// <summary>
        /// A detailed description of the target
        /// </summary>
        public virtual string Description => $"Basic Vector Target (Size={Size}, NormaliseCustomJudger={NormaliseCustomJudger}) (CJ: {CustomJudger?.Description})";

        /// <summary>
        /// The Custom Judger to use (default of dot product is used if null)
        /// </summary>
        [SimpleStateProperty("CustomJudger")]
        public IVectorTargetJudger CustomJudger { get; private set; } = null;

        /// <summary>
        /// Whether the output of a CustomJudger should be normalised from [-Size, Size] to [0, 1]
        /// </summary>
        [SimpleStateProperty("NormaliseCustomJudger")]
        public bool NormaliseCustomJudger { get; private set; }

        [Obsolete]
        protected VectorTarget()
        { }

        /// <summary>
        /// Creates a target using the given Vector
        /// </summary>
        /// <param name="targetVector">The vector we are targetting (S)</param>
        /// <param name="friendlyName">A friendly name for the target</param>
        /// <param name="customJudger">A custom judgement function to use (leave null for default)</param>
        public VectorTarget(Linear.Vector<double> targetVector, string friendlyName, IVectorTargetJudger customJudger = null, bool normaliseCustomJudger = true)
        {
            Vector = targetVector;
            FriendlyName = friendlyName;
            FullName = friendlyName + " (CJ: " + (customJudger?.Name ?? "default") +")";
            CustomJudger = customJudger;
            NormaliseCustomJudger = normaliseCustomJudger;
        }

        /// <summary>
        /// Creates a target using a copy of the given array
        /// </summary>
        /// <param name="targetVector">The vector we are targetting (S)</param>
        /// <param name="friendlyName">A friendly name for the target</param>
        /// <param name="customJudger">A custom judgement function to use (leave null for default)</param>
        public VectorTarget(double[] targetVectorArray, string friendlyName, IVectorTargetJudger customJudger = null, bool normaliseCustomJudger = true)
        {
            Vector = Linear.CreateVector.DenseOfArray(targetVectorArray);
            FriendlyName = friendlyName;
            FullName = friendlyName + " (CJ: " + (customJudger?.Name ?? "default") + ")";
            CustomJudger = customJudger;
            NormaliseCustomJudger = normaliseCustomJudger;
        }

        /// <summary>
        /// Creates a target using a copy of the given array
        /// </summary>
        /// <param name="targetVector">The vector we are targetting (S)</param>
        /// <param name="friendlyName">A friendly name for the target</param>
        /// <param name="customJudger">A custom judgement function to use (leave null for default)</param>
        public VectorTarget(int[] targetVectorArray, string friendlyName, IVectorTargetJudger customJudger = null, bool normaliseCustomJudger = true)
        {
            Vector = Linear.CreateVector.DenseOfArray(targetVectorArray.Select(i => (double)i).ToArray());
            FriendlyName = friendlyName;
            FullName = friendlyName + " (CJ: " + (customJudger?.Name ?? "default") + ")";
            CustomJudger = customJudger;
            NormaliseCustomJudger = normaliseCustomJudger;
        }
        
        /// <summary>
        /// Creates a target from the chracters in the string (+ => 1, - => -1, space => 0)
        /// </summary>
        /// <param name="targetVector">The vector we are targetting (S)</param>
        /// <param name="friendlyName">A friendly name for the target</param>
        /// <param name="customJudger">A custom judgement function to use (leave null for default)</param>
        public VectorTarget(string targetVectorString, string friendlyName, IVectorTargetJudger customJudger = null, bool normaliseCustomJudger = true)
        {
            Vector = Misc.ParseExtremesVector(targetVectorString, -1.0, 1.0, 0.0);

            FriendlyName = friendlyName;
            FullName = friendlyName + " (CJ: " + (customJudger?.Name ?? "default") +")";
            CustomJudger = customJudger;
            NormaliseCustomJudger = normaliseCustomJudger;
        }

        /// <summary>
        /// Indicates whether the NoisyVector may be different from the (normal) Vector
        /// (must be true by default, because NoisyVector is serialised)
        /// </summary>
        private bool Noisy = true;

        /// <summary>
        /// Prepares the Target for the next generation by creating new Noise or whatever
        /// </summary>
        /// <param name="rand">Random source for the noise</param>
        /// <param name="jrules">Judgement Rules</param>
        /// <returns>True if the objective has changed, thereby invalidating previous evaluations</returns>
        public virtual bool NextGeneration(MathNet.Numerics.Random.RandomSource rand, JudgementRules jrules)
        {
            bool invalidated = false;

            // reset NoisyVector if necessary
            if (NoisyVector == null)
            {
                NoisyVector = Vector.Clone();
                invalidated = true;
            }
            else if (Noisy)
            {
                Vector.CopyTo(NoisyVector);
                invalidated = true;
            }
            
            // if κ != 0, then add gaussian noise to NoisyVector
            if (jrules.NoiseFactor != 0)
            {
                // cached to avoid unnecessary allocations
                var κ = jrules.NoiseFactor;
                var rnd = rand;

                NoisyVector.MapInplace(d => d + MathNet.Numerics.Distributions.Normal.Sample(rnd, 0, 1) * κ, Linear.Zeros.Include);
                Noisy = true;
                invalidated = true;
            }
            else
            {
                Noisy = false;
            }

            return invalidated;
        }

        /// <summary>
        /// Prepares the target for the next exposure (e.g. by changing something? I don't know)
        /// </summary>
        /// <param name="rand">Random source for the noise</param>
        /// <param name="jrules">Judgement Rules</param>
        /// <param name="epoch">The current epoch</param>
        /// <param name="exposureInformation">Information about the next exposure, which the target can modify</param>
        /// <returns>True if the objective has changed, thereby invalidating previous evaluations</returns>
        public virtual bool NextExposure(MathNet.Numerics.Random.RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            return false;
        }

        /// <summary>
        /// Computes the fitness of the Phenotype given this Target
        /// b in the paper
        /// </summary>
        /// <param name="p">The phenotype (P*)</param>
        /// <returns>The fitness of the Phenotype (b)</returns>
        public double Judge(Phenotype p)
        {
            Debug.Assert(NoisyVector != null, $"Model/VectorTarget.Judge NoisyVector must be set!!! call NextGeneration(rand, jrules) before each generation");
            Debug.Assert(p.Size == Size, $"Model/VectorTarget.Judge (size = {p.Size}) must be the same size as the Target (size = {Size})");

            if (CustomJudger != null)
            {
                if (NormaliseCustomJudger)
                    return 0.5 * (1.0 + CustomJudger.Judge(NoisyVector, p) / Size);
                else
                    return CustomJudger.Judge(NoisyVector, p);
            }
            else
            {
                return JudgeOverride(p);
            }
        }

        protected virtual double JudgeOverride(Phenotype p)
        {
            return 0.5 * (1.0 + NoisyVector.DotProduct(p.Vector) / Size);
        }

        public virtual Linear.Vector<double> PreparePerfectP()
        {
            return Vector.Clone();
        }
    }

    [StateClass]
    public class EuclideanTargetJudger : IVectorTargetJudger
    {
        [Obsolete]
        protected EuclideanTargetJudger()
        {
        }

        public EuclideanTargetJudger(double factor, double bias)
        {
            Factor = factor;
            Bias = bias;
        }

        public string Name => "EuclideanJudger";

        public string Description => $"EuclideanJudger (Factor={Factor}, Bias={Bias})";

        [SimpleStateProperty("Factor")]
        public double Factor { get; private set; }

        [SimpleStateProperty("Bias")]
        public double Bias { get; private set; }

        public double Judge(Linear.Vector<double> vector, Phenotype phenotype)
        {
            return Bias - (vector - phenotype.Vector).L2Norm();
        }
    }

    public interface ITargetDecorator
    {
        ITarget Target { get; }
    }

    [StateClass]
    public class DutyTarget : ITarget, ITargetDecorator
    {
        [Obsolete]
        protected DutyTarget()
        { }

        public DutyTarget(ITarget target, double start, double end)
        {
            if (end < start)
                throw new ArgumentException("Start must be before the end");

            Target = target ?? throw new ArgumentNullException(nameof(target));
            Start = start;
            End = end;
        }

        public int Size => Target.Size;

        public string FriendlyName => $"Duty({Target.FriendlyName})";

        public string FullName => $"Duty({Target.FullName})";

        public string Description => $"Duty({Target.Description}, {Start}, {End})";

        /// <summary>
        /// The underlying target wrapped by this DutyTarget
        /// </summary>
        [SimpleStateProperty("Target")]
        public ITarget Target { get; private set; }

        [SimpleStateProperty("Start")]
        public double Start { get; private set; }

        [SimpleStateProperty("End")]
        public double End { get; private set; }

        public double Judge(Phenotype p)
        {
            return Target.Judge(p);
        }

        public int DetermineDuration(int totalDuration, double start, double end)
        {
            int s = (int)Math.Floor(totalDuration * start);
            int e = (int)Math.Floor(totalDuration * end);
            return e - s;
        }

        public bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            exposureInformation.ExposureDuration = DetermineDuration(exposureInformation.ExposureDuration, Start, End);
            return Target.NextExposure(rand, jrules, epoch, ref exposureInformation);
        }

        public bool NextGeneration(RandomSource rand, JudgementRules jrules)
        {
            return Target.NextGeneration(rand, jrules);
        }
    }

    [StateClass]
    public class SaturationTarget : ITarget, ITargetDecorator
    {
        [Obsolete]
        protected SaturationTarget()
        { }

        public SaturationTarget(ITarget target, double min, double threshold, double max)
        {
            Target = target ?? throw new ArgumentNullException(nameof(target));
            Min = min;
            Threshold = threshold;
            Max = max;
        }

        public int Size => Target.Size;

        public string FriendlyName => $"Sat({Target.FriendlyName})";

        public string FullName => $"Sat({Target.FullName})";

        public string Description => $"Sat({Target.Description})";

        /// <summary>
        /// The underlying target wrapped by this SaturationTarget
        /// </summary>
        [SimpleStateProperty("Target")]
        public ITarget Target { get; private set; }

        [SimpleStateProperty("Min")]
        public double Min { get; private set; }

        [SimpleStateProperty("Threshold")]
        public double Threshold { get; private set; }

        [SimpleStateProperty("Max")]
        public double Max { get; private set; }

        public double Judge(Phenotype p)
        {
            return Target.Judge(p.Saturate(Min, Threshold, Max));
        }

        public bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            return Target.NextExposure(rand, jrules, epoch, ref exposureInformation);
        }

        public bool NextGeneration(RandomSource rand, JudgementRules jrules)
        {
            return Target.NextGeneration(rand, jrules);
        }
    }

    public interface IVectorTargetJudger
    {
        /// <summary>
        /// The name of the IVectorTargetJudger
        /// </summary>
        string Name { get; }

        /// <summary>
        /// A detailed description of the IVectorTargetJudger
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Judges the given phenotype considering the given target vector
        /// </summary>
        /// <param name="vector">The current target vector</param>
        /// <param name="phenotype">The phenotype to judge</param>
        double Judge(Linear.Vector<double> vector, Phenotype phenotype);
    }

    // NOTE: a custom StateProvider is in StateProviders.cs
    public struct MultiMeasureJudgement
    {
        /// <summary>
        /// The benefit afforded the phenotype by the environment
        /// b in the paper
        /// </summary>
        public double Benefit { get; }

        /// <summary>
        /// The cost of the genome
        /// c in the paper
        /// </summary>
        public double Cost { get; }

        /// <summary>
        /// The combined fitness
        /// (b - c * λ)
        /// </summary>
        public double CombinedFitness { get; }

        public MultiMeasureJudgement(double b, double c, double combinedFitness)
        {
            Benefit = b;
            Cost = c;
            CombinedFitness = combinedFitness;
        }

        /// <summary>
        /// Computes the fitness of the Genome/Phenotype combination given this Target and JudgementRules
        /// f_s(G) in the paper
        /// </summary>
        /// <param name="g">The genome (G)</param>
        /// <param name="p">The phenotype (P*)</param>
        /// <param name="jrules">Judgement Rules (regularistion etc.)</param>
        /// <param name="jrules">The current environment (Target)</param>
        /// <returns>The fitness of the Phenotype (w(P*))</returns>
        public static MultiMeasureJudgement Judge(IGenome g, Phenotype p, JudgementRules jrules, ITarget target)
        {
            double b = target.Judge(p);
            
            double λ = jrules.RegularisationFactor;
            double c = jrules.RegularisationFunction.ComputeCost(g);

            return new MultiMeasureJudgement(b, c, b - c * λ);
        }
    }

    [StateClass]
    public class Phenotype
    {
        /// <summary>
        /// The vector representing the phenotype
        /// P* in the paper
        /// </summary>
        [VectorStateProperty("Vector")]
        public Linear.Vector<double> Vector { get; private set; }
        
        /// <summary>
        /// The size of the Phenotype
        /// </summary>
        public int Size => Vector.Count;

        [Obsolete]
        private Phenotype()
        { }

        /// <summary>
        /// Creates a Phenotype
        /// </summary>
        /// <param name="developedPhenotypeVector">The vector representing the phenotype (P*)</param>
        public Phenotype(Linear.Vector<double> developedPhenotypeVector)
        {
            Vector = developedPhenotypeVector;
        }

        /// <summary>
        /// Gets the expression of trait idx
        /// </summary>
        /// <param name="idx">The index of the trait</param>
        public double this[int idx] => Vector[idx];

        /// <summary>
        /// Saturates the phenotype to the given min/max values based on the given threshold
        /// </summary>
        /// <returns></returns>
        public Phenotype Saturate(double min, double threshold, double max)
        {
            return new Phenotype(Linear.CreateVector.Dense<double>(Vector.Count, i => Misc.Saturate(Vector[i], min, threshold, max)));
        }
    }
    
    /// <summary>
    /// IGenome
    /// Something that can develop into a Phenotype
    /// </summary>
    public interface IGenome
    {
        /// <summary>
        /// Initial state before development
        /// G in the paper
        /// </summary>
        Linear.Vector<double> InitialState { get; }

        /// <summary>
        /// Recursively applied developmental transformation matrix
        /// B in the paper
        /// </summary>
        Linear.Matrix<double> TransMat { get; }

        /// <summary>
        /// The size of the Genome
        /// N in the paper
        /// </summary>
        int Size { get; }

        /// <summary>
        /// Develops the Genotype into a Phenotype of the same Size given the DevelopmentalRules
        /// </summary>
        /// <param name="rules">The Rules to apply for development</param>
        /// <returns>The developed phenotype (P*)</returns>
        Phenotype Develop(ModelExecutionContext context, DevelopmentRules rules);

        /// <summary>
        /// Develops the Genotype into a Phenotype of the same Size given the DevelopmentalRules
        /// </summary>
        /// <param name="phenotype">The target Phenotype</param>
        /// <param name="rules">The Rules to apply for development</param>
        void DevelopInto(Phenotype phenotype, ModelExecutionContext context, DevelopmentRules rules);
    }

    /// <summary>
    /// IGenome<typeparamref name="T"/>
    /// An IGenome that can produce a mutant
    /// </summary>
    /// <typeparam name="T">Mutant type</typeparam>
    public interface IGenome<T> : IGenome where T : IGenome<T>
    {
        /// <summary>
        /// Creates a new offspring Genome (of type) with a single mutation
        /// </summary>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        /// <returns>The offspring</returns>
        T Mutate(ModelExecutionContext context, ReproductionRules rules);

        /// <summary>
        /// Mutates this Genome, turning the target Genome into an offspring
        /// </summary>
        /// <param name="target">The target Genome</param>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        void MutateInto(T target, ModelExecutionContext context, ReproductionRules rules);

        /// <summary>
        /// Mutates this Genome inplace
        /// </summary>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        void MutateInplace(ModelExecutionContext context, ReproductionRules rules);

        /// <summary>
        /// Combines this Genome with another, turning the target Genome into an unmutated offspring
        /// </summary>
        /// <param name="other">The other Genome involves in the combination</param>
        /// <param name="target">The target Genome</param>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        void CombineInto(T other, T target, ModelExecutionContext context, ReproductionRules rules);

        /// <summary>
        /// Create an exact copy, duplicating any state
        /// </summary>
        T Clone(ModelExecutionContext context);
    }

    // NOTE: a custom StateProvider is in StateProviders.cs
    public class MatrixEntryAddress
    {
        public int Row { get; private set; }
        public int Col { get; private set; }
        
        public MatrixEntryAddress(int r, int c)
        {
            Row = r;
            Col = c;
        }

        public static MatrixEntryAddress[] Complete(int rows, int cols)
        {
            MatrixEntryAddress[] res = new MatrixEntryAddress[rows * cols];

            int o = 0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    res[o++] = new MatrixEntryAddress(r, c);
                }
            }

            return res;
        }

        public static IEnumerable<MatrixEntryAddress> CompleteLazy(int rows, int cols)
        {
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    yield return new MatrixEntryAddress(r, c);
                }
            }
        }

        public override string ToString()
        {
            return $"({Row},{Col})";
        }

        public override bool Equals(object obj)
        {
            return obj is MatrixEntryAddress address &&
                   Row == address.Row &&
                   Col == address.Col;
        }

        public override int GetHashCode()
        {
            int hashCode = 1084646500;
            hashCode = hashCode * -1521134295 + Row.GetHashCode();
            hashCode = hashCode * -1521134295 + Col.GetHashCode();
            return hashCode;
        }
    }

    [StateClass]
    public class ColumnsTransMatMutator : ITransMatMutator
    {
        public string Name => "Columns" + Size;
        
        [SimpleStateProperty("Size")]
        public int Size { get; private set; }

        [SimpleStateProperty("Modules")]
        public int Modules { get; private set; }

        [SimpleStateProperty("ModuleSize")]
        public int ModuleSize { get; private set; }

        [Obsolete]
        private ColumnsTransMatMutator()
        {}

        [Obsolete]
        [StateMethod]
        private void PostRead(object ctx)
        {
            PrepareRegions();
        }

        private MatrixEntryAddress[][] Regions { get; set; }

        public ColumnsTransMatMutator(int size, int modules)
        {
            Size = size;
            Modules = modules;
            ModuleSize = size / modules;

            PrepareRegions();
        }

        private void PrepareRegions()
        {
            Regions = Misc.CreateEmpty<MatrixEntryAddress>(Size * Modules, ModuleSize);
            for (int c = 0; c < Size; c++)
            {
                for (int r = 0; r < Size; r++)
                {
                    Regions[c * Modules + r / ModuleSize][r % ModuleSize] = new MatrixEntryAddress(r, c);
                }
            }
        }
        
        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            // no clone
            var newTransMat = Linear.CreateMatrix.Dense<double>(Size, Size);
            Mutators.RegionalTransMatMutateInto(Regions, current, newTransMat, transMatIndexOpenEntries, rand, rules);
            
            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            // no copy
            Mutators.RegionalTransMatMutateInto(Regions, current, newTransMat, transMatIndexOpenEntries, rand, rules);
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            throw new NotSupportedException("MutateInPlace is not supported by ColumnsTransMatMutator"); 
        }
    }
    
    [StateClass]
    public class Columns2TransMatMutator : ITransMatMutator
    {
        public string Name => "Columns" + Size + "/" + ModuleSize;

        [SimpleStateProperty("Size")]
        public int Size { get; private set; }

        [SimpleStateProperty("ModuleCount")]
        public int ModuleCount { get; private set; }

        [SimpleStateProperty("ModuleSize")]
        public int ModuleSize { get; private set; }
        
        [Obsolete]
        private Columns2TransMatMutator()
        { }

        public Columns2TransMatMutator(int size, int modules)
        {
            Size = size;
            ModuleCount = modules;

            ModuleSize = size / modules;
        }
        
        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            // no clone
            var newTransMat = Linear.CreateMatrix.Dense<double>(Size, Size);
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
            
            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same vector");

            // no copy
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
        }

        private void MutateIntoInternal(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null)
            {
                // mark all open entries
                foreach (var mea in transMatIndexOpenEntries)
                {
                    int r, c; // row, col

                    r = mea.Row;
                    c = mea.Col;

                    newTransMat[r, c] = 1;
                }
            }

            for (int i = 0; i < newTransMat.RowCount; i += ModuleSize)
            {
                for (int c = 0; c < newTransMat.ColumnCount; c++)
                {
                    // draw delta
                    double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                    for (int r = i; r < i + ModuleSize; r++)
                    {
                        if (transMatIndexOpenEntries != null && newTransMat[r, c] < 1)
                        {
                            // skip unmarked entries if transMatIndexOpenEntries is non-null
                            newTransMat[r, c] = current[r, c];
                        }
                        else
                        {
                            // apply
                            newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]); // clamping
                        }
                    }
                }
            }
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null)
            {
                throw new NotSupportedException("MutateInPlace is not supported by Columns2TransMatMutator with non-null transMatIndexOpenEntries");
            }
            else
            {
                MutateIntoInternal(transMat, transMat, null, rand, rules);
            }
        }
    }
    
    [StateClass]
    public class SingleColumnTransMatMutator : ITransMatMutator
    {
        public string Name => "SingleColumn" + Size + "/" + ModuleSize;
        
        [SimpleStateProperty("Size")]
        public int Size { get; private set; }

        [SimpleStateProperty("Modules")]
        public int Modules { get; private set; }

        [SimpleStateProperty("ModuleSize")]
        public int ModuleSize { get; private set; }
        
        [Obsolete]
        private SingleColumnTransMatMutator()
        { }
        
        public SingleColumnTransMatMutator(int size, int modules)
        {
            Size = size;
            Modules = modules;

            ModuleSize = size / modules;
        }
        
        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            var newTransMat = current.Clone();
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
            
            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same matrix");
            
            current.CopyTo(newTransMat);
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
        }

        private void MutateIntoInternal(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            // pick a random region
            int region = rand.Next(Size * Modules);
            
            // draw delta
            double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

            // update single region
            if (transMatIndexOpenEntries != null)
            {
                foreach (var mea in transMatIndexOpenEntries)
                {
                    int r, c; // row, col

                    r = mea.Row;
                    c = mea.Col;

                    if (c + (r / ModuleSize) * Size == region)
                        newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
                }
            }
            else
            {
                int r = (region / Size) * ModuleSize;
                int c = region % Size;

                for (int ro = 0; ro < ModuleSize; ro++)
                {
                    newTransMat[r + ro, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r + ro, c]);
                }
            }
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            MutateIntoInternal(transMat, transMat, transMatIndexOpenEntries, rand, rules);
        }
    }

    /// <summary>
    /// Updates only a single cell (or does nothing, if there are no open entries)
    /// </summary>
    [StateClass]
    public class SingleCellTransMatMutator : ITransMatMutator
    {
        public string Name => "SingleCell";

        public SingleCellTransMatMutator()
        {
        }

        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null && transMatIndexOpenEntries.Count == 0)
                return current; // don't copy

            var newTransMat = current.Clone();
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);

            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same matrix");

            current.CopyTo(newTransMat);
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
        }

        private void MutateIntoInternal(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null)
            {
                if (transMatIndexOpenEntries.Count == 0)
                    return; // nothing to do

                // pick a random cell
                int cidx = rand.Next(transMatIndexOpenEntries.Count);

                var mea = transMatIndexOpenEntries[cidx];

                int r = mea.Row;
                int c = mea.Col;

                // draw delta
                double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
            }
            else
            {
                int size = current.ColumnCount;

                // pick a random cell
                int r = rand.Next(size);
                int c = rand.Next(size);

                // draw delta
                double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
            }
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            MutateIntoInternal(transMat, transMat, transMatIndexOpenEntries, rand, rules);
        }
    }

    /// <summary>
    /// Updates only a single cell and it's symmetric partner (or does nothing, if there are no open entries)
    /// </summary>
    [StateClass]
    public class SingleCellSymmetricTransMatMutator : ITransMatMutator
    {
        public string Name => "SingleCellSymmetric";

        public SingleCellSymmetricTransMatMutator()
        {
        }

        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null && transMatIndexOpenEntries.Count == 0)
                return current; // don't copy

            var newTransMat = current.Clone();
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);

            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same matrix");

            current.CopyTo(newTransMat);
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
        }

        private void MutateIntoInternal(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null)
            {
                if (transMatIndexOpenEntries.Count == 0)
                    return; // nothing to do

                // pick a random cell
                int cidx = rand.Next(transMatIndexOpenEntries.Count);

                var mea = transMatIndexOpenEntries[cidx];

                int r = mea.Row;
                int c = mea.Col;

                // draw delta
                double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
                newTransMat[c, r] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[c, r]);
            }
            else
            {
                int size = current.ColumnCount;

                // pick a random cell
                int r = rand.Next(size);
                int c = rand.Next(size);

                // draw delta
                double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
                newTransMat[c, r] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[c, r]);
            }
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            MutateIntoInternal(transMat, transMat, transMatIndexOpenEntries, rand, rules);
        }
    }

    /// <summary>
    /// Moves weight between two cells.
    /// </summary>
    [StateClass]
    public class CellTransferTransMatMutator : ITransMatMutator
    {
        public string Name => "CellTransfer";

        public CellTransferTransMatMutator()
        {
        }

        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null && transMatIndexOpenEntries.Count == 0)
                return current; // don't copy

            var newTransMat = current.Clone();
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);

            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same matrix");

            current.CopyTo(newTransMat);
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
        }

        private void MutateIntoInternal(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            int r1, c1, r2, c2;

            if (transMatIndexOpenEntries != null)
            {
                if (transMatIndexOpenEntries.Count <= 1)
                    return; // nothing to do

                // pick two random (distinct) cells
                int c1idx = rand.Next(transMatIndexOpenEntries.Count);
                int c2idx = rand.Next(transMatIndexOpenEntries.Count - 1);
                if (c2idx >= c1idx)
                    c2idx++;

                var mea1 = transMatIndexOpenEntries[c1idx];
                var mea2 = transMatIndexOpenEntries[c2idx];

                r1 = mea1.Row;
                c1 = mea1.Col;
                r2 = mea2.Row;
                c2 = mea2.Col;
            }
            else
            {
                int size = current.ColumnCount;

                // pick two random (distinct) cells
                int c1idx = rand.Next(size * size);
                int c2idx = rand.Next(size * size - 1);
                if (c2idx >= c1idx)
                    c2idx++;

                r1 = c1idx / size;
                c1 = c1idx % size;
                r2 = c2idx / size;
                c2 = c2idx % size;
            }

            // draw delta
            double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

            // find out how much we can transfer without clamping
            var v1 = rules.DevelopmentalTransformationMatrixClamping.Clamp(current[r1, c1] + μ2) - current[r1, c1];
            var v2 = rules.DevelopmentalTransformationMatrixClamping.Clamp(current[r2, c2] - μ2) - current[r2, c2];

            // only draw as much as we can draw
            var v = Math.Min(Math.Abs(v1), Math.Abs(v2));
            μ2 = Math.Sign(μ2) * v;

            // transfer it
            newTransMat[r1, c1] = rules.DevelopmentalTransformationMatrixClamping.Clamp(current[r1, c1] + μ2);
            newTransMat[r2, c2] = rules.DevelopmentalTransformationMatrixClamping.Clamp(current[r2, c2] - μ2);
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            MutateIntoInternal(transMat, transMat, transMatIndexOpenEntries, rand, rules);
        }
    }

    /// <summary>
    /// Updates only a single module or inter-module block
    /// </summary>
    [StateClass]
    public class SingleByModuleTransMatMutator : ITransMatMutator
    {
        public string Name { get; private set; }

        /// <summary>
        /// The modules by which to mutate
        /// </summary>
        [SimpleStateProperty("Modules")]
        public Modular.Modules Modules { get; private set; }

        /// <summary>
        /// Whether to divive weights by the number of cells in the block
        /// </summary>
        [SimpleStateProperty("DivideWeights")]
        public bool DivideWeights { get; private set; }

        [Obsolete]
        protected SingleByModuleTransMatMutator()
        {
        }

        public SingleByModuleTransMatMutator(Modular.Modules modules, bool divideWeights)
        {
            Modules = modules;
            DivideWeights = divideWeights;

            Name = "SingleByModule" + (divideWeights ? "DivideWeights" : "") + modules.ToString();
        }

        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null && transMatIndexOpenEntries.Count == 0)
                return current; // don't copy

            var newTransMat = current.Clone();
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);

            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same matrix");

            current.CopyTo(newTransMat);
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
        }

        // not a very efficient implementation
        private void MutateIntoInternal(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null)
            {
                if (transMatIndexOpenEntries.Count == 0)
                    return; // nothing to do

                // mark open
                foreach (var open in transMatIndexOpenEntries)
                    newTransMat[open.Row, open.Col] = double.NaN;

                // pick two random modules
                int midxr = rand.Next(Modules.ModuleCount);
                int midxc = rand.Next(Modules.ModuleCount);

                // draw delta
                double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                // divide weights if necessary
                if (DivideWeights)
                    μ2 /= (Modules[midxr].Count * Modules[midxc].Count);

                // apply to all
                foreach (var r in Modules[midxr])
                    foreach (var c in Modules[midxc])
                        if (double.IsNaN(newTransMat[r, c]))
                            newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);

                // copy those still open
                foreach (var open in transMatIndexOpenEntries)
                    if (double.IsNaN(newTransMat[open.Row, open.Col]))
                        newTransMat[open.Row, open.Col] = current[open.Row, open.Col];
            }
            else
            {
                // pick two random modules
                int midxr = rand.Next(Modules.ModuleCount);
                int midxc = rand.Next(Modules.ModuleCount);

                // draw delta
                double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                // divide weights if necessary
                if (DivideWeights)
                    μ2 /= (Modules[midxr].Count * Modules[midxc].Count);

                // apply to all
                foreach (var r in Modules[midxr])
                    foreach (var c in Modules[midxc])
                        newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
            }
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            MutateIntoInternal(transMat, transMat, transMatIndexOpenEntries, rand, rules);
        }
    }

    /// <summary>
    /// Doesn't do any mutating at all
    /// </summary>
    [StateClass]
    public class NullTransMatMutator : ITransMatMutator
    {
        public static readonly NullTransMatMutator Instance = new NullTransMatMutator();

        public string Name => "NullTransMatMutator";

        protected NullTransMatMutator()
        {
        }

        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null && transMatIndexOpenEntries.Count == 0)
                return current; // don't copy

            var newTransMat = current.Clone();
            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same matrix");

            current.CopyTo(newTransMat);
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            // nix
        }
    }

    /// <summary>
    /// Throws if you try to use it
    /// </summary>
    [StateClass]
    public class ThrowTransMatMutator : ITransMatMutator
    {
        public static readonly ThrowTransMatMutator Instance = new ThrowTransMatMutator();

        public string Name => "ThrowTransMatMutator";

        protected ThrowTransMatMutator()
        {
        }

        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            throw new InvalidOperationException("Invoked a ThrowTransMatMutator");
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            throw new InvalidOperationException("Invoked a ThrowTransMatMutator");
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            throw new InvalidOperationException("Invoked a ThrowTransMatMutator");
        }
    }

    /// <summary>
    /// Performs multiple single-cell updates (or does nothing, if there are no open entries)
    /// </summary>
    [StateClass]
    public class MutliCellTransMatMutator : ITransMatMutator
    {
        [Obsolete]
        protected MutliCellTransMatMutator()
        {
        }
        
        public string Name => "MultiCell" + UpdateCount;
        
        public MutliCellTransMatMutator(int updateCount)
        {
            UpdateCount = updateCount;
        }

        [SimpleStateProperty("UpdateCount")]
        public int UpdateCount { get; private set; }

        public Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null && transMatIndexOpenEntries.Count == 0)
                return current; // don't copy
            
            var newTransMat = current.Clone();
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
            
            return newTransMat;
        }

        public void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same matrix");

            current.CopyTo(newTransMat);
            MutateIntoInternal(current, newTransMat, transMatIndexOpenEntries, rand, rules);
        }

        private void MutateIntoInternal(Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (transMatIndexOpenEntries != null)
            {
                if (transMatIndexOpenEntries.Count == 0)
                    return; // nothing to do
                
                for (int i = 0; i < UpdateCount; i++)
                {
                    // pick a random cell
                    int cidx = rand.Next(transMatIndexOpenEntries.Count);

                    var mea = transMatIndexOpenEntries[cidx];

                    int r = mea.Row;
                    int c = mea.Col;
                    
                    // draw delta
                    double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);
                    newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
                }
            }
            else
            {
                int size = current.ColumnCount;
                
                for (int i = 0; i < UpdateCount; i++)
                {
                    // pick a random cell
                    int r = rand.Next(size);
                    int c = rand.Next(size);
                
                    // draw delta
                    double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                    newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
                }
            }
        }

        public void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            MutateIntoInternal(transMat, transMat, transMatIndexOpenEntries, rand, rules);
        }
    }

    public static class Mutators
    {
        /// <summary>
        /// General purpose method that probably isn't what you want if you want performance
        /// Applies the same delta to each entry in each region, ignoring entries that arn't in transMatIndexOpenEntries
        /// </summary>
        public static void RegionalTransMatMutateInto(MatrixEntryAddress[][] regions, Linear.Matrix<double> current, Linear.Matrix<double> newTransMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newTransMat)
                throw new Exception("Cannot mutate into the same matrix");

            if (transMatIndexOpenEntries != null)
            {
                // mark all open entries
                foreach (var mea in transMatIndexOpenEntries)
                {
                    int r, c; // row, col

                    r = mea.Row;
                    c = mea.Col;

                    newTransMat[r, c] = 1;
                }
            }

            foreach (var region in regions)
            {
                // draw delta
                double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                foreach (var mea in region)
                {
                    int r, c; // row, col

                    r = mea.Row;
                    c = mea.Col;

                    if (transMatIndexOpenEntries != null && newTransMat[r, c] < 1)
                    {
                        // skip unmarked entries if transMatIndexOpenEntries is non-null
                        newTransMat[r, c] = current[r, c];
                    }
                    else
                    {
                        // apply (unclamped)
                        newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + current[r, c]);
                    }
                }
            }
        }

        public static ITransMatMutator Columns(int size, int modules)
        {
            return new ColumnsTransMatMutator(size, modules);
        }

        public static ITransMatMutator Columns2(int size, int modules)
        {
            return new Columns2TransMatMutator(size, modules);
        }

        public static ITransMatMutator SingleColumn(int size, int modules)
        {
            return new SingleColumnTransMatMutator(size, modules);
        }
    }

    /// <summary>
    /// Not thread safe
    /// </summary>
    [StateClass]
    public class DefaultDevelopmentStep : IDevelopmentStep
    {
        public string Name => $"Default(Size:{Size},Noise:{StateNoise})";

        [SimpleStateProperty("Size")]
        public int Size { get; private set; }
        
        [SimpleStateProperty("StateNoise")]
        public double StateNoise { get; private set; }

        [Obsolete]
        private DefaultDevelopmentStep()
        { }

        [StateMethod]
        private void PostRead(object ctx)
        {
            Init();
        }
        
        private Linear.Vector<double> Update;

        public DefaultDevelopmentStep(int size, double stateNoise)
        {
            Size = size;
            StateNoise = stateNoise;

            Init();
        }

        private void Init()
        {
            Update = Linear.CreateVector.Dense<double>(Size);
        }

        public void Step(Linear.Vector<double> state, Linear.Matrix<double> transMat, RandomSource rand, DevelopmentRules drules, Linear.Vector<double> bias)
        {
            transMat.Multiply(state, Update); // this is where the CPU spends most of its time (N^2)
            if (bias != null)
                Update.Add(bias);

            // no managed provider can help us here
            if (Size < 4096) // if MathNet is just going to do it on one thread... (MathNET uses this constant internally)
            {
                // ... then do it ourselves (probably adds some overhead accessing storage; saves overhead of late fast-pathing in MathNET (which includes allocations))

                double rt = 1 - drules.DecayRate;
                double ut = drules.UpdateRate;

                // fast path dense storage
                if (state.Storage is Linear.Storage.DenseVectorStorage<double> sdvs && Update.Storage is Linear.Storage.DenseVectorStorage<double> udvs)
                {
                    var sdata = sdvs.Data;
                    var udata = udvs.Data;

                    for (int i = 0; i < sdata.Length; i++)
                    {
                        sdata[i] = sdata[i] * rt + drules.Squash.Squash(udata[i]) * ut; // or this, if the matrix is small (N)
                    }
                }
                else
                {
                    for (int i = 0; i < Size; i++)
                    {
                        state[i] = state[i] * rt + drules.Squash.Squash(Update[i]) * ut; // or this, if the matrix is small (N)
                    }
                }
            }
            else
            {
                // let MathNET do it
                Update.MapInplace(drules.Squash.Delegate); // or this, if the matrix is small (N)

                Update.Multiply(drules.UpdateRate, Update);

                state.Multiply(1 - drules.DecayRate, state);
                state.Add(Update, state);
            }

            if (StateNoise > 0.0)
            {
                state.Add(Linear.Double.DenseVector.Build.DenseOfArray(MathNet.Numerics.Distributions.Normal.Samples(rand, 0.0, StateNoise).Take(state.Count).ToArray()), state);
            }
        }

        public void StepOld(Linear.Vector<double> state, Linear.Matrix<double> transMat, RandomSource rand, DevelopmentRules drules)
        {
            transMat.Multiply(state, Update); // this is where the CPU spends most of its time (N^2)

            // no managed provider can help us here
            if (Size < 4096) // if MathNet is just going to do it on one thread... (MathNET uses this constant internally)
            {
                // ... then do it ourselves (probably adds some overhead accessing storage; saves overhead of late fast-pathing in MathNET (which includes allocations))
                for (int i = 0; i < Size; i++)
                    Update[i] = drules.Squash.Squash(Update[i]);
            }
            else
            {
                // let MathNET do it
                Update.MapInplace(drules.Squash.Delegate); // or this, if the matrix is small (N)
            }

            Update.Multiply(drules.UpdateRate, Update);

            state.Multiply(1 - drules.DecayRate, state);
            state.Add(Update, state);

            if (StateNoise > 0.0)
            {
                state.Add(Linear.Double.DenseVector.Build.DenseOfArray(MathNet.Numerics.Distributions.Normal.Samples(rand, 0.0, StateNoise).Take(state.Count).ToArray()), state);
            }
        }
    }

    [StateClass]
    public class SharingDevelopmentStep : IDevelopmentStep
    {
        public string Name => $"Sharing({Size},Noise:{StateNoise})";

        [SimpleStateProperty("Size")]
        public int Size { get; private set; }
        
        [SimpleStateProperty("StateNoise")]
        public double StateNoise { get; private set; }

        [Obsolete]
        private SharingDevelopmentStep()
        { }

        [StateMethod]
        private void PostRead(object ctx)
        {
            Init();
        }
        
        private Linear.Vector<double> Update;

        public SharingDevelopmentStep(int size, double stateNoise)
        {
            Size = size;
            StateNoise = stateNoise;

            Init();
        }

        private void Init()
        {
            Update = Linear.CreateVector.Dense<double>(Size);
        }

        public void Step(Linear.Vector<double> state, Linear.Matrix<double> transMat, RandomSource rand, DevelopmentRules drules, Linear.Vector<double> bias)
        {
            var normed = transMat.PointwiseAbs().NormalizeColumns(1.0);
            var sharingMatrix = transMat.PointwiseMultiply(normed);

            sharingMatrix.Multiply(state, Update);
            if (bias != null)
                Update.Add(bias);
            Update.MapInplace(drules.Squash.Delegate);
            Update.Multiply(drules.UpdateRate, Update);

            state.Multiply(1 - drules.DecayRate, state);
            state.Add(Update, state);

            if (StateNoise > 0.0)
            {
                state.Add(Linear.Double.DenseVector.Build.DenseOfArray(MathNet.Numerics.Distributions.Normal.Samples(rand, 0.0, StateNoise).ToArray()));
            }
        }
    }

    public static class Development
    {
        public static IDevelopmentStep DefaultDevelopmentStep(int size, double stateNoise = 0.0)
        {
            return new DefaultDevelopmentStep(size, stateNoise);
        }
        
        public static IDevelopmentStep SharingDevelopmentStep(int size, double stateNoise = 0.0)
        {
            return new SharingDevelopmentStep(size, stateNoise);
        }
    }

    [StateClass]
    public class UniformCrossOver : IInitialStateCombiner, ITransMatCombiner
    {
        public static readonly UniformCrossOver Instance = new UniformCrossOver();

        public UniformCrossOver()
        {
        }

        public string Name => "UniformCrossover";
        
        public void CombineInto(Linear.Vector<double> a, Linear.Vector<double> b, Linear.Vector<double> target, RandomSource rand, ReproductionRules rules)
        {
            var rb = new RandomBits(rand);

            for (int i = 0; i < target.Count; i++)
            {
                target[i] = rb.NextBit() ? a[i] : b[i];
            }
        }

        public void CombineInto(Linear.Matrix<double> a, Linear.Matrix<double> b, Linear.Matrix<double> target, RandomSource rand, ReproductionRules rules)
        {
            var rb = new RandomBits(rand);

            for (int i = 0; i < target.ColumnCount; i++)
            {
                for (int j = 0; j < target.RowCount; j++)
                {
                    target[i, j] = rb.NextBit() ? a[i, j] : b[i, j];
                }
            }
        }
    }

    public interface IInitialStateCombiner
    {
        string Name { get; }

        /// <summary>
        /// Will not modify any of the parameters
        /// Implies a copy from to target (assumes a, b, and target have the same dimension)
        /// </summary>
        void CombineInto(Linear.Vector<double> a, Linear.Vector<double> b, Linear.Vector<double> target, RandomSource rand, ReproductionRules rules);
    }

    public interface ITransMatCombiner
    {
        string Name { get; }

        /// <summary>
        /// Will not modify any of the parameters
        /// Implies a copy from to target (assumes a, b, and target have the same dimensions)
        /// </summary>
        void CombineInto(Linear.Matrix<double> a, Linear.Matrix<double> b, Linear.Matrix<double> target, RandomSource rand, ReproductionRules rules);
    }

    public interface ITransMatMutator
    {
        string Name { get; }

        /// <summary>
        /// Will not modify any of the parameters
        /// May return current
        /// </summary>
        Linear.Matrix<double> Mutate(Linear.Matrix<double> current, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules);

        /// <summary>
        /// Modifies only target
        /// Implies a copy from current to target (assumes they are the same dimensions)
        /// </summary>
        void MutateInto(Linear.Matrix<double> current, Linear.Matrix<double> target, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules);

        /// <summary>
        /// Modifies only target
        /// </summary>
        void MutateInplace(Linear.Matrix<double> transMat, IReadOnlyList<MatrixEntryAddress> transMatIndexOpenEntries, RandomSource rand, ReproductionRules rules);
    }

    public interface IInitialStateMutator
    {
        string Name { get; }

        /// <summary>
        /// Will not modify any of the parameters
        /// May return current
        /// </summary>
        Linear.Vector<double> Mutate(Linear.Vector<double> current, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules);

        /// <summary>
        /// Modifies only target
        /// Implies a copy from current to target (assumes they are the same dimension)
        /// </summary>
        void MutateInto(Linear.Vector<double> current, Linear.Vector<double> target, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules);

        /// <summary>
        /// Modifies only current
        /// </summary>
        void MutateInplace(Linear.Vector<double> current, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules);
    }

    [StateClass]
    public class ModuleInitialStateMutator : IInitialStateMutator
    {
        public string Name => "Modules" + Size + "/" + ModuleSize;
        
        [SimpleStateProperty("Size")]
        public int Size { get; private set; }

        [SimpleStateProperty("Modules")]
        public int Modules { get; private set; }

        [SimpleStateProperty("ModuleSize")]
        public int ModuleSize { get; private set; }
        
        [Obsolete]
        private ModuleInitialStateMutator()
        { }
        
        public ModuleInitialStateMutator(int size, int modules)
        {
            Size = size;
            Modules = modules;

            ModuleSize = size / modules;
        }
        
        public Linear.Vector<double> Mutate(Linear.Vector<double> current, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            var newInitialState = current.Clone();
            MutateIntoInternal(current, newInitialState, initialStateIndexOpenEntries, rand, rules);
            
            return newInitialState;
        }

        public void MutateInto(Linear.Vector<double> current, Linear.Vector<double> newInitialState, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newInitialState)
                throw new Exception("Cannot mutate into the same vector");
            
            current.CopyTo(newInitialState);
            MutateIntoInternal(current, newInitialState, initialStateIndexOpenEntries, rand, rules);
        }

        private void MutateIntoInternal(Linear.Vector<double> current, Linear.Vector<double> newInitialState, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            for (int u = 0; u < rules.InitialTraitUpdates; u++)
            {
                // pick a module
                int r = rand.Next(Modules);
                
                // draw delta
                double μ1 = rand.NextNoise(rules.InitialStateMutationSize, rules.InitialStateMutationType);

                // apply clamped
                if (initialStateIndexOpenEntries != null)
                {
                    foreach (var i in initialStateIndexOpenEntries)
                        if (i >= r * ModuleSize && i < (r + 1) * ModuleSize)
                            newInitialState[i] = rules.InitialStateClamping.Clamp(μ1 + current[i]);
                }
                else
                {
                    for (int i = r * ModuleSize; i < (r + 1) * ModuleSize; i++)
                        newInitialState[i] = rules.InitialStateClamping.Clamp(μ1 + current[i]);
                }
            }
        }

        public void MutateInplace(Linear.Vector<double> current, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            MutateIntoInternal(current, current, initialStateIndexOpenEntries, rand, rules);
        }
    }

    [StateClass]
    public class InitialStateOneHotModulesMutator : IInitialStateMutator
    {
        public string Name => "OneHotModules" + Modules.ToString();

        [SimpleStateProperty("Size")]
        public Modular.Modules Modules { get; private set; }

        [SimpleStateProperty("Cold")]
        public double Cold { get; private set; }

        [SimpleStateProperty("Hot")]
        public double Hot { get; private set; }

        [Obsolete]
        private InitialStateOneHotModulesMutator()
        { }

        public InitialStateOneHotModulesMutator(Modular.Modules modules, double hot, double cold)
        {
            Modules = modules;
            Hot = hot;
            Cold = cold;
        }

        public Linear.Vector<double> Mutate(Linear.Vector<double> current, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            var newInitialState = current.Clone();
            MutateIntoInternal(current, newInitialState, initialStateIndexOpenEntries, rand, rules);

            return newInitialState;
        }

        public void MutateInto(Linear.Vector<double> current, Linear.Vector<double> newInitialState, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            if (current == newInitialState)
                throw new Exception("Cannot mutate into the same vector");

            current.CopyTo(newInitialState);
            MutateIntoInternal(current, newInitialState, initialStateIndexOpenEntries, rand, rules);
        }

        private void MutateIntoInternal(Linear.Vector<double> current, Linear.Vector<double> newInitialState, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            for (int u = 0; u < rules.InitialTraitUpdates; u++)
            {
                // pick a module
                int mi = rand.Next(Modules.ModuleCount);
                var m = Modules.ModuleAssignments[mi];

                // pick an element
                int ei = rand.Next(m.Count);

                // no delta
                //double μ1 = rand.NextNoise(rules.InitialStateMutationSize, rules.InitialStateMutationType);

                // apply clamped
                if (initialStateIndexOpenEntries != null)
                {
                    throw new InvalidOperationException($"{nameof(InitialStateOneHotModulesMutator)} does not support explicit open entires");
                }
                else
                {
                    for (int i = 0; i < m.Count; i++)
                        newInitialState[m[i]] = Cold;
                    newInitialState[m[ei]] = Hot;
                }
            }
        }

        public void MutateInplace(Linear.Vector<double> current, IReadOnlyList<int> initialStateIndexOpenEntries, RandomSource rand, ReproductionRules rules)
        {
            MutateIntoInternal(current, current, initialStateIndexOpenEntries, rand, rules);
        }
    }

    public interface IDevelopmentStep
    {
        string Name { get; }

        /// <summary>
        /// Modifies state
        /// Will not modify anything else
        /// </summary>
        void Step(Linear.Vector<double> state, Linear.Matrix<double> transMat, MathNet.Numerics.Random.RandomSource rand, DevelopmentRules drules, Linear.Vector<double> bias);
    }

    /// <summary>
    /// Dense Genome (developmental transformation matrix taken to be dense)
    /// </summary>
    [StateClass]
    public class DenseGenome : IGenome<DenseGenome>
    {
        /// <summary>
        /// Initial state before development
        /// G in the paper
        /// </summary>
        [VectorStateProperty("InitialState")]
        public Linear.Vector<double> InitialState { get; private set; }

        /// <summary>
        /// The Developmental Bias Vector
        /// </summary>
        [VectorStateProperty("BiasVector")]
        public Linear.Vector<double> BiasVector { get; private set; }

        /// <summary>
        /// Recursively applied developmental transformation matrix
        /// B in the paper
        /// </summary>
        [MatrixStateProperty("TransMat")]
        public Linear.Matrix<double> TransMat { get; private set; }

        /// <summary>
        /// The size of the Genome
        /// N in the paper
        /// </summary>
        public int Size => InitialState.Count;

        /// <summary>
        /// The list of vector entries that we are allowed to modify
        /// null implies we can modify them all
        /// </summary>
        [SimpleStateProperty("InitialStateIndexOpenEntries")]
        public IReadOnlyList<int> InitialStateIndexOpenEntries { get; private set; }

        /// <summary>
        /// The list of matric entries that we are allowed to modify
        /// null implies we can modify them all
        /// </summary>
        [SimpleStateProperty("TransMatIndexOpenEntries")]
        public IReadOnlyList<MatrixEntryAddress> TransMatIndexOpenEntries { get; private set; }

        /// <summary>
        /// A custom TransMatMutator
        /// If null, the default is used
        /// </summary>
        [SimpleStateProperty("CustomTransMatMutator")]
        public ITransMatMutator CustomTransMatMutator { get; private set; }

        /// <summary>
        /// A custom InitialStateMutator
        /// If null, the default is used
        /// </summary>
        [SimpleStateProperty("CustomInitialStateMutator")]
        public IInitialStateMutator CustomInitialStateMutator { get; private set; }

        /// <summary>
        /// A custom TransMatCombiner
        /// If null, the default is used
        /// </summary>
        [SimpleStateProperty("CustomTransMatCombiner")]
        public ITransMatCombiner CustomTransMatCombiner { get; private set; }

        /// <summary>
        /// A custom InitialStateCombiner
        /// If null, the default is used
        /// </summary>
        [SimpleStateProperty("CustomInitialStateCombiner")]
        public IInitialStateCombiner CustomInitialStateCombiner { get; private set; }

        /// <summary>
        /// A custom DevelopmentStep
        /// If null, the default is used
        /// </summary>
        [SimpleStateProperty("CustomDevelopmentStep")]
        public IDevelopmentStep CustomDevelopmentStep { get; private set; }
        
        [Obsolete]
        private DenseGenome()
        { }

        /// <summary>
        /// Creates a Genome from the initalState (G) vector and developmental transformation matrix (B)
        /// </summary>
        /// <param name="initialState">Initial state before development (G)</param>
        /// <param name="transMat">Recursively applied developmental transformation matrix (B)</param>
        /// <param name="initialStateOpenEntries">Open entries in the InitialState (null means all entries are open)</param>
        /// <param name="transMatOpenEntries">Open entries in the TransMat (null means all entries are open)</param>
        /// <param name="customInitialStateMutator">The custom InitialStateMutator to use</param>
        /// <param name="customTransMatMutator">The custom TransMatMutator to use</param>
        /// <param name="customDevelopmentStep">The custom DevelopmentStep to use</param>
        /// <param name="skipChecks">Where to skip sanity checks: unless you are certain your code works, and performance is a serious concern, just leave this off</param>
        public DenseGenome(Linear.Vector<double> initialState, Linear.Matrix<double> transMat, IReadOnlyList<int> initialStateOpenEntries = null, IReadOnlyList<MatrixEntryAddress> transMatOpenEntries = null, ITransMatMutator customTransMatMutator = null, IInitialStateMutator customInitialStateMutator = null, ITransMatCombiner customTransMatCombiner = null, IInitialStateCombiner customInitialStateCombiner = null, IDevelopmentStep customDevelopmentStep = null, Linear.Vector<double> biasVector = null, bool skipChecks = false)
        {
            if (!skipChecks)
            {
                Debug.Assert(initialState.Count > 0, $"Model/Genome.Ctor1: initialState vector must not be empty (length = {initialState.Count})");
                Debug.Assert(transMat.RowCount == transMat.ColumnCount, $"Model/Genome.Ctor1: transmat must be a square matrix (rows = {transMat.RowCount} cols = {transMat.ColumnCount})");
                Debug.Assert(initialState.Count == transMat.ColumnCount, $"Model/Genome.Ctor1: initialState vector and transMat must have the same dimensions");
                Debug.Assert(biasVector == null || initialState.Count == biasVector.Count, $"Model/Genome.Ctor1: initialState vector and biasVector must have the same dimensions");

                if (initialStateOpenEntries != null)
                {
                    Debug.Assert(initialStateOpenEntries.All(i => i >= 0 && i < initialState.Count), $"Model/Genome.Ctor1: all initialStateOpenEntries must address into the transMat");
                }

                if (transMatOpenEntries != null)
                {
                    Debug.Assert(transMatOpenEntries.All(mea => mea.Row >= 0 && mea.Row < transMat.RowCount && mea.Col >= 0 && mea.Col < transMat.ColumnCount), $"Model/Genome.Ctor1: all transMatOpenEntries must address into the transMat");
                }
            }
            
            InitialState = initialState;
            TransMat = transMat;
            InitialStateIndexOpenEntries = initialStateOpenEntries;
            TransMatIndexOpenEntries = transMatOpenEntries;
            CustomInitialStateMutator = customInitialStateMutator;
            CustomTransMatMutator = customTransMatMutator;
            CustomDevelopmentStep = customDevelopmentStep;
            CustomTransMatCombiner = customTransMatCombiner;
            CustomInitialStateCombiner = customInitialStateCombiner;
            BiasVector = biasVector;
        }

        /// <summary>
        /// Creates a 'zero' genome of the given size
        /// </summary>
        /// <param name="size">The size of the Genome to creates (N)</param>
        /// <param name="customInitialStateMutator">The custom InitialStateMutator to use</param>
        /// <param name="customTransMatMutator">The custom TransMatMutator to use</param>
        /// <param name="customDevelopmentStep">The custom DevelopmentStep to use</param>
        /// <returns>A 'zero' genome of the given size</returns>
        public static DenseGenome CreateDefaultGenome(int size, ITransMatMutator customTransMatMutator = null, IInitialStateMutator customInitialStateMutator = null, IDevelopmentStep customDevelopmentStep = null)
        {
            return new DenseGenome(Linear.CreateVector.Dense<double>(size), Linear.CreateMatrix.Sparse<double>(size, size), customTransMatMutator : customTransMatMutator, customInitialStateMutator : customInitialStateMutator, customDevelopmentStep: customDevelopmentStep);
        }

        /// <summary>
        /// Creates a 'zero' genome of the given size with a genuinely Dense TransMat
        /// </summary>
        /// <param name="size">The size of the Genome to creates (N)</param>
        /// <param name="customInitialStateMutator">The custom InitialStateMutator to use</param>
        /// <param name="customTransMatMutator">The custom TransMatMutator to use</param>
        /// <returns>A 'zero' genome of the given size</returns>
        public static DenseGenome CreateDefaultGenomeDense(int size, ITransMatMutator customTransMatMutator = null, IInitialStateMutator customInitialStateMutator = null, IDevelopmentStep customDevelopmentStep = null)
        {
            return new DenseGenome(Linear.CreateVector.Dense<double>(size), Linear.CreateMatrix.Dense<double>(size, size), customTransMatMutator: customTransMatMutator, customInitialStateMutator : customInitialStateMutator, customDevelopmentStep: customDevelopmentStep);
        }

        /// <summary>
        /// Creates a 'zero' genome of the given size
        /// </summary>
        /// <param name="size">The size of the Genome to create (N)</param>
        /// <param name="coeficientsPerTrait">Number of regulation coeficients that will be use to update each trait (per-row in the transMat)</param>
        /// <param name="rand">The RandomSource to use when creating the DenseGenome (not stored)</param>
        /// <param name="customInitialStateMutator">The custom InitialStateMutator to use</param>
        /// <param name="customTransMatMutator">The custom TransMatMutator to use</param>
        /// <returns>A 'zero' genome of the given size</returns>
        public static DenseGenome CreateDefaultGenomeSparse(int size, int coeficientsPerTrait, RandomSource rand, ITransMatMutator customTransMatMutator = null, IInitialStateMutator customInitialStateMutator = null)
        {
            return new DenseGenome(Linear.CreateVector.Dense<double>(size), Linear.CreateMatrix.Sparse<double>(size, size), null, RandomOpenEntries(size, coeficientsPerTrait, rand), customTransMatMutator, customInitialStateMutator);
        }

        private static MatrixEntryAddress[] RandomOpenEntries(int size, int coeficientsPerTrait, RandomSource rand)
        {
            MatrixEntryAddress[] res = new MatrixEntryAddress[size * coeficientsPerTrait];

            int[] sampler = Create<int>(size, i => i);

            int o = 0;
            for (int i = 0; i < size; i++)
            {
                ShuffleInplace(rand, sampler);
                for (int j = 0; j < coeficientsPerTrait; j++)
                {
                    res[o++] = new MatrixEntryAddress(i, sampler[j]);
                }
            }

            return res;
        }

        /// <summary>
        /// Develops the Genotype into a Phenotype of the same Size given the DevelopmentalRules
        /// </summary>
        /// <param name="phenotypeVector">The target Phenotype</param>
        /// <param name="context">The ModelExecutionContext</param>
        /// <param name="rules">The Rules to apply for development</param>
        /// <returns>The developed phenotype (P*)</returns>
        public void DevelopInto(Phenotype target, ModelExecutionContext context, DevelopmentRules rules)
        {
            var phenotypeVector = target.Vector;
            InitialState.CopyTo(phenotypeVector);
            DevelopPhenotypeInternal(phenotypeVector, context, rules);
        }


        /// <summary>
        /// Develops the given state (assumes it is the initial state, and develops it to a the phenotypic state)
        /// </summary>
        private void DevelopPhenotypeInternal(Linear.Vector<double> state, ModelExecutionContext context, DevelopmentRules rules)
        {
            IDevelopmentStep dstep = CustomDevelopmentStep ?? new DefaultDevelopmentStep(Size, 0.0);

            for (int i = 0; i < rules.TimeSteps; i++)
            {
                dstep.Step(state, TransMat, context.Rand, rules, BiasVector);
            }

            // normalise phenotype via τ1 and τ2
            double normalisationFactor = 1.0 / (rules.UpdateRate / rules.DecayRate);
            state.Multiply(normalisationFactor, state);

            // rescale
            state.Multiply(rules.RescaleScale, state);
            state.Add(rules.RescaleOffset, state);
        }

        /// <summary>
        /// Develops the Genotype into a Phenotype of the same Size given the DevelopmentalRules
        /// </summary>
        /// <param name="rules">The Rules to apply for development</param>
        /// <returns>The developed phenotype (P*)</returns>
        public Phenotype Develop(ModelExecutionContext context, DevelopmentRules rules)
        {
            Linear.Vector<double> state = InitialState.Clone();
            DevelopPhenotypeInternal(state, context, rules);
            return new Phenotype(state);
        }

        /// <summary>
        /// Develops the Genotype into a Phenotype of the same Size given the DevelopmentalRules
        /// </summary>
        /// <param name="rules">The Rules to apply for development</param>
        /// <param name="trajectories">The arrays to place the output; leave null for it to be built for you</param>
        /// <returns>The developed phenotype (P*)</returns>
        public Phenotype DevelopWithTrajectories(MathNet.Numerics.Random.RandomSource rand, DevelopmentRules rules, ref double[][] trajectories)
        {
            if (trajectories == null)
            {
                trajectories = new double[Size][];
                for (int trait = 0; trait < Size; trait++)
                    trajectories[trait] = new double[rules.TimeSteps + 1];
            }

            Linear.Vector<double> state = InitialState.Clone();
            
            IDevelopmentStep dstep = CustomDevelopmentStep ?? new DefaultDevelopmentStep(Size, 0.0);
            
            for (int i = 0; i < rules.TimeSteps; i++)
            {
                // record trajectories
                for (int trait = 0; trait < Size; trait++)
                    trajectories[trait][i] = state[trait];

                dstep.Step(state, TransMat, rand, rules, BiasVector);
            }
            
            // record trajectories (terminals)
            for (int trait = 0; trait < Size; trait++)
                trajectories[trait][rules.TimeSteps] = state[trait];

            // normalise phenotype via τ1 and τ2
            double normalisationFactor = 1.0 / (rules.UpdateRate / rules.DecayRate);
            state.Multiply(normalisationFactor, state);

            // rescale
            state.Multiply(rules.RescaleScale, state);
            state.Add(rules.RescaleOffset, state);

            return new Phenotype(state);
        }

        /// <summary>
        /// Creates an exact replica of the DenseGenome with copies of the state (to mitigate any unintended modification)
        /// </summary>
        /// <returns>The exact replica</returns>
        public DenseGenome Clone(ModelExecutionContext context, Linear.Vector<double> newInitialState = null, Linear.Matrix<double> newTransMat = null, Linear.Vector<double> newBiasVector = null)
        {
            return new DenseGenome(newInitialState ?? InitialState.Clone(), newTransMat ?? context.Clone(TransMat), InitialStateIndexOpenEntries, TransMatIndexOpenEntries, CustomTransMatMutator, CustomInitialStateMutator, CustomTransMatCombiner, CustomInitialStateCombiner, CustomDevelopmentStep, newBiasVector ?? BiasVector?.Clone(), true);
        }

        /// <summary>
        /// Sets the CustomTransmatMutator inplace
        /// </summary>
        public void SetCustomTransmatMutator(ITransMatMutator transMatMutator)
        {
            this.CustomTransMatMutator = transMatMutator;
        }

        /// <summary>
        /// Sets the CustomInitialStateMutator inplace
        /// </summary>
        public void SetCustomInitialStateMutator(IInitialStateMutator initialStateMutator)
        {
            this.CustomInitialStateMutator = initialStateMutator;
        }

        /// <summary>
        /// Creates an exact replice of the DenseGenome with copies of the state (to mitigate any unintended modification)
        /// </summary>
        /// <returns>The exact replica</returns>
        DenseGenome IGenome<DenseGenome>.Clone(ModelExecutionContext context)
        {
            return Clone(context);
        }

        /// <summary>
        /// Mutates the current Genome, stuffing the new Genome into the given DenseGenome
        /// This assumes that the target genome has no shared state: it will reuse existing storage
        /// It also assumes that the dense genomes are the same dimensions; no gaurentees are made if they are not
        /// </summary>
        /// <param name="target">The target DenseGenome, which will become the offspring</param>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        public void MutateInto(DenseGenome target, ModelExecutionContext context, ReproductionRules rules)
        {
            var rand = context.Rand;
            
            bool mutateB = rand.NextDouble() < rules.DevelopmentalTransformationMatrixRate;
            if (mutateB)
            { // mutate trans mat (B)
                if (CustomTransMatMutator != null)
                {
                    CustomTransMatMutator.MutateInto(TransMat, target.TransMat, TransMatIndexOpenEntries, rand, rules);
                }
                else
                {
                    TransMat.CopyTo(target.TransMat);
                    MutateTransMatDefaultInplace(target.TransMat, rand, rules);
                }
            }
            else
            {
                TransMat.CopyTo(target.TransMat);
            }
            
            if (!(mutateB && rules.ExclusiveBMutation))
            { // mutate initial state (G)
                if (CustomInitialStateMutator != null)
                {
                    CustomInitialStateMutator.MutateInto(InitialState, target.InitialState, InitialStateIndexOpenEntries, rand, rules);
                }
                else
                {
                    InitialState.CopyTo(target.InitialState);
                    MutateInitialStateDefaultInplace(target.InitialState, rand, rules);
                }
            }
            else
            {
                InitialState.CopyTo(target.InitialState);
            }
        }
        
        /// <summary>
        /// Mutates the current Genome inplace.
        /// Assumes this genome has no shared state (will not duplicate anything)
        /// </summary>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        public void MutateInplace(ModelExecutionContext context, ReproductionRules rules)
        {
            var rand = context.Rand;

            bool mutateB = rand.NextDouble() < rules.DevelopmentalTransformationMatrixRate;
            if (mutateB)
            { // mutate trans mat (B)
                if (CustomTransMatMutator != null)
                {
                    CustomTransMatMutator.MutateInplace(TransMat, TransMatIndexOpenEntries, rand, rules);
                }
                else
                {
                    MutateTransMatDefaultInplace(TransMat, rand, rules);
                }
            }

            if (!(mutateB && rules.ExclusiveBMutation))
            { // mutate initial state (G)
                if (CustomInitialStateMutator != null)
                {
                    CustomInitialStateMutator.MutateInplace(InitialState, InitialStateIndexOpenEntries, rand, rules);
                }
                else
                {
                    MutateInitialStateDefaultInplace(InitialState, rand, rules);
                }
            }
        }

        /// <summary>
        /// Creates a new offspring DenseGenome with a single mutation
        /// </summary>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        /// <returns>The offspring</returns>
        public DenseGenome Mutate(ModelExecutionContext context, ReproductionRules rules)
        {
            var rand = context.Rand;

            // assume unchanged initially
            var newInitialState = InitialState;
            var newTransMat = TransMat;
            
            bool mutateB = rand.NextDouble() < rules.DevelopmentalTransformationMatrixRate;
            if (mutateB)
            { // mutate trans mat (B)
                if (CustomTransMatMutator != null)
                {
                    newTransMat = CustomTransMatMutator.Mutate(TransMat, TransMatIndexOpenEntries, rand, rules);
                }
                else
                {
                    // new 'pooled' clone
                    newTransMat = context.Clone(TransMat);
                    MutateTransMatDefaultInplace(newTransMat, rand, rules);
                }
            }
            
            if (!(mutateB && rules.ExclusiveBMutation))
            { // mutate initial state (G)
                if (CustomInitialStateMutator != null)
                {
                    newInitialState = CustomInitialStateMutator.Mutate(InitialState, InitialStateIndexOpenEntries, rand, rules);
                }
                else
                {
                    newInitialState = InitialState.Clone();
                    MutateInitialStateDefaultInplace(newInitialState, rand, rules);
                }
            }
            
            return new DenseGenome(newInitialState, newTransMat, InitialStateIndexOpenEntries, TransMatIndexOpenEntries, CustomTransMatMutator, CustomInitialStateMutator, CustomTransMatCombiner, CustomInitialStateCombiner, CustomDevelopmentStep, BiasVector, true);
        }

        /// <summary>
        /// This is the 'traditional' (and deault) initial-state mutation scheme... it's stuffed in this class for not-very-good legacy-related reasons
        /// </summary>
        private void MutateTransMatDefaultInplace(Linear.Matrix<double> newTransMat, RandomSource rand, ReproductionRules rules)
        {
            // mutate all traits a little

            // select trait
            if (TransMatIndexOpenEntries == null)
            { // any
                for (int r = 0; r < Size; r++)
                {
                    for (int c = 0; c < Size; c++)
                    {
                        // draw delta
                        double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                        // apply (unclamped)
                        newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + newTransMat[r, c]);
                    }
                }
            }
            else
            {
                foreach (var mea in TransMatIndexOpenEntries)
                {
                    int r, c; // row, col

                    r = mea.Row;
                    c = mea.Col;
                    
                    // draw delta
                    double μ2 = rand.NextNoise(rules.DevelopmentalTransformationMatrixMutationSize, rules.DevelopmentalTransformationMatrixMutationType);

                    // apply (unclamped)
                    newTransMat[r, c] = rules.DevelopmentalTransformationMatrixClamping.Clamp(μ2 + newTransMat[r, c]);
                }
            }
        }

        /// <summary>
        /// This is the 'traditional' (and deault) initial-state mutation scheme... it's stuffed in this class for not-very-good legacy-related reasons
        /// </summary>
        private void MutateInitialStateDefaultInplace(Linear.Vector<double> newInitialState, RandomSource rand, ReproductionRules rules)
        {
            if (InitialStateIndexOpenEntries == null)
            { // any
                for (int i = 0; i < rules.InitialTraitUpdates; i++)
                {
                    // select trait
                    int r = rand.Next(Size);

                    // draw delta
                    double μ1 = rand.NextNoise(rules.InitialStateMutationSize, rules.InitialStateMutationType);

                    // apply clamped
                    newInitialState[r] = rules.InitialStateClamping.Clamp(μ1 + newInitialState[r]);
                }
            }
            else
            {
                for (int i = 0; i < rules.InitialTraitUpdates; i++)
                {
                    // select trait
                    int r = InitialStateIndexOpenEntries[rand.Next(InitialStateIndexOpenEntries.Count)];

                    // draw delta
                    double μ1 = rand.NextNoise(rules.InitialStateMutationSize, rules.InitialStateMutationType);

                    // apply clamped
                    newInitialState[r] = rules.InitialStateClamping.Clamp(μ1 + newInitialState[r]);
                }
            }
        }

        /// <summary>
        /// Copies the given initial state vector (G') over the internal initial state vector (G)
        /// G = G'
        /// </summary>
        /// <param name="newInitialState">The new initial state vector G'</param>
        public void CopyOverInitialState(Linear.Vector<double> newInitialState)
        {
            newInitialState.CopyTo(InitialState);
        }

        /// <summary>
        /// Copies the given developmental matrix (B') over the internal developmental matrix (B)
        /// B = B'
        /// </summary>
        /// <param name="newInitialState">The new developemntal matrix B'</param>
        public void CopyOverTransMat(Linear.Matrix<double> newTransMat)
        {
            newTransMat.CopyTo(TransMat);
        }

        /// <summary>
        /// Only call this is you are CERTAIN that the TransMat can be recycled
        /// DO NOT PRESUME YOU CAN STUFF THIS BEHIND AN INTERFACE
        /// You can EASILY break your program by using this, because Genome's will not duplicate the TransMat unless they have to
        /// </summary>
        public void Recycle(ModelExecutionContext context)
        {
            context.GetMatrixPool(TransMat.RowCount, TransMat.ColumnCount).Release(TransMat);
        }

        /// <summary>
        /// Combines this genome with another into the given target genome
        /// </summary>
        /// <param name="other">The other Genome involves in the combination</param>
        /// <param name="target">The target Genome</param>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        public void CombineInto(DenseGenome other, DenseGenome target, ModelExecutionContext context, ReproductionRules rules)
        {
            if (CustomInitialStateCombiner != null)
            {
                CustomInitialStateCombiner.CombineInto(InitialState, other.InitialState, target.InitialState, context.Rand, rules);
            }
            else
            {
                UniformCrossOver.Instance.CombineInto(InitialState, other.InitialState, target.InitialState, context.Rand, rules);
            }

            if (CustomTransMatCombiner != null)
            {
                CustomTransMatCombiner.CombineInto(TransMat, other.TransMat, target.TransMat, context.Rand, rules);
            }
            else
            {
                UniformCrossOver.Instance.CombineInto(TransMat, other.TransMat, target.TransMat, context.Rand, rules);
            }
        }
    }
}
