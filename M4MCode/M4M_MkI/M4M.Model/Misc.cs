﻿using M4M.Modular;
using M4M.State;
using MathNet.Numerics.LinearAlgebra.Complex;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.Linq;
using Linear = MathNet.Numerics.LinearAlgebra; // reasonable quality API reference here: https://numerics.mathdotnet.com/api/MathNet.Numerics.LinearAlgebra/ (inline isn't good enogh)

namespace M4M
{
    public interface IEvent<T>
    {
        void Register(T t);
        void Unregister(T t);
    }

    public class Event<T> : IEvent<T>
    {
        HashSet<T> Entries { get; } = new HashSet<T>();

        public Event()
        {
        }

        public void Register(T t)
        {
            Entries.Add(t);
        }

        public void Unregister(T t)
        {
            Entries.Remove(t);
        }

        public void Call(Action<T> act)
        {
            foreach (T t in Entries)
            {
                act(t);
            }
        }

        public bool HasAny => Entries.Count > 0;
    }

    public struct Percentile
    {
        public Percentile(double position, double value)
        {
            Position = position;
            Value = value;
        }

        public double Position { get; }
        public double Value { get; }

        /// <summary>
        /// Assumes that reusableBuffer is null, or that it is the right length; same goes for percentiles
        /// </summary>
        public static void ComputePercentiles(double[] positions, IEnumerable<double> unorderedSamples, ref double[] reusableBuffer, ref Percentile[] percentiles)
        {
            if (reusableBuffer == null)
            {
                reusableBuffer = unorderedSamples.ToArray();
            }
            else
            {
                int i = 0;
                foreach (double d in unorderedSamples)
                {
                    reusableBuffer[i++] = d;
                }

                if (i != reusableBuffer.Length)
                    throw new ArgumentException("Reusable buffer was too big");
            }
            
            Array.Sort(reusableBuffer);

            if (percentiles == null)
                percentiles = new Percentile[positions.Length];
            
            for (int i = 0; i < positions.Length; i++)
            {
                double p = positions[i];

                int idx = (int)(p * (double)(reusableBuffer.Length));
                idx = Misc.Clamp(idx, 0, reusableBuffer.Length - 1);

                percentiles[i] = new Percentile(p, reusableBuffer[idx]);
            }
        }
    }

    public class Indexed<T>
    {
        public Indexed(int index, T value)
        {
            Index = index;
            Value = value;
        }

        public int Index { get; }
        public T Value { get; }

        public static IEnumerable<Indexed<T>> IndexSeq(IEnumerable<T> seq, int startIndex = 0, int step = 1)
        {
            int i = int.MaxValue; // sadly we have to give a value here... and it will always be meaningless
            bool first = true;
            foreach (T t in seq)
            {
                if (first) // we do it this way so that it can't overflow on the last index (totally worth it)
                {
                    i = startIndex;
                    first = false;
                }
                else
                {
                     i += step;
                }

                yield return new Indexed<T>(i, t);
            }
        }
    }

    public class IndexedIndexedComparer<T> : IEqualityComparer<Indexed<T>>
    {
        public static readonly UnindexedIndexedComparer<T> Default = new UnindexedIndexedComparer<T>();

        public IEqualityComparer<T> TComparer { get; }

        public IndexedIndexedComparer(IEqualityComparer<T> tComparer = null)
        {
            TComparer = tComparer ?? EqualityComparer<T>.Default;
        }

        public bool Equals(Indexed<T> x, Indexed<T> y)
        {
            return x.Index == y.Index && TComparer.Equals(x.Value, y.Value);
        }

        public int GetHashCode(Indexed<T> indexed)
        {
            return indexed.Index ^ TComparer.GetHashCode(indexed.Value);
        }
    }

    public class UnindexedIndexedComparer<T> : IEqualityComparer<Indexed<T>>
    {
        public static readonly UnindexedIndexedComparer<T> Default = new UnindexedIndexedComparer<T>();

        public IEqualityComparer<T> TComparer { get; }

        public UnindexedIndexedComparer(IEqualityComparer<T> tComparer = null)
        {
            TComparer = tComparer ?? EqualityComparer<T>.Default;
        }

        public bool Equals(Indexed<T> x, Indexed<T> y)
        {
            return TComparer.Equals(x.Value, y.Value);
        }
        
        public int GetHashCode(Indexed<T> indexed)
        {
            return TComparer.GetHashCode(indexed.Value);
        }
    }

    public static partial class Misc
    {
        public static readonly Dictionary<char, int> Multipliers = new Dictionary<char, int>()
        {
            ['k'] = 3,
            ['M'] = 6,
            ['G'] = 9,
            ['m'] = -3,
            ['u'] = -6,
            ['n'] = -9,
        };

        private static int Pow10(int power)
        {
            return (int)Math.Pow(10, power);
        }

        public static int ParseInt(string str)
        {
            return (int)ParseLong(str);
        }

        public static long ParseLong(string str)
        {
            long i;
            if (long.TryParse(str, out i))
                return i;

            var idx = str.IndexOf('e');
            if (idx == -1)
                idx = str.IndexOf('E');

            if (idx > 0)
            {
                int didx = str.IndexOf('.');
                int sub = didx < 0 ? 0 : idx - didx - 1; // the number of digits after the decimal point but before the e/E
                var front = str.Remove(idx);
                if (didx >= 0)
                    front = front.Remove(didx, 1);
                if (long.TryParse(front, out i) && int.TryParse(str.Substring(idx + 1), out int exp))
                {
                    int mul = exp - sub;
                    i *= Pow10(mul);
                    return i;
                }
            }

            foreach (var vk in Multipliers)
            {
                idx = str.IndexOf(vk.Key);
                if (idx == str.Length - 1)
                {
                    int didx = str.IndexOf('.');

                    if (didx >= 0)
                    {
                        // replace decimal point with multiplier
                        str = str.Substring(0, didx) + vk.Key + str.Substring(didx + 1, idx - didx - 1);
                        idx = didx;
                    }
                }

                if (idx > 0) // don't allow e.g. k3, require 0k3
                {
                    if (long.TryParse(str.Remove(idx, 1), out i))
                    {
                        int sub = str.Length - idx - 1; // the number of digits after the multiplier
                        int mul = vk.Value - sub;
                        i *= Pow10(mul);
                    }
                    return i;
                }
            }

            throw new FormatException($"String does not represent an integer: {str}");
        }

        /// <summary>
        /// Creates a Matrix where each entry on each column has the same value
        /// </summary>
        /// <param name="rowCount">The number of rows to create</param>
        /// <param name="columns">The value in each column</param>
        public static Linear.Matrix<T> Columns<T>(int rowCount, IReadOnlyList<T> columns) where T : struct, IEquatable<T>, IFormattable
        {
            return Linear.CreateMatrix.Dense<T>(rowCount, columns.Count, (i, j) => columns[j]);
        }

        /// <summary>
        /// Creates a Matrix where each entry on each column has the same value
        /// </summary>
        /// <param name="rows">The value in each row</param>
        /// <param name="columnCount">The number of columns to create</param>
        public static Linear.Matrix<T> Rows<T>(IReadOnlyList<T> rows, int columnCount) where T : struct, IEquatable<T>, IFormattable
        {
            return Linear.CreateMatrix.Dense<T>(rows.Count, columnCount, (i, j) => rows[i]);
        }

        /// <summary>
        /// Creates a Matrix where each entry on each column has the same value
        /// </summary>
        /// <param name="rows">The value in each row</param>
        /// <param name="columnCount">The number of columns to create</param>
        public static Linear.Matrix<T> Dense<T>(Modules modules, T intra, T inter) where T : struct, IEquatable<T>, IFormattable
        {
            var mat = Linear.CreateMatrix.Dense<T>(modules.ElementCount, modules.ElementCount, inter);
            var assignments = modules.ModuleAssignments;

            foreach (var module in assignments)
            {
                foreach (var r in module)
                    foreach (var c in module)
                        mat[r, c] = intra;
            }

            return mat;
        }

        /// <summary>
        /// Converts lists of samples to lists of trajectories
        /// </summary>
        public static T[][] SamplesToTrajectories<T>(IEnumerable<IReadOnlyList<T>> samples)
        {
            List<T>[] trajectories = null;

            foreach (var sample in samples)
            {
                if (trajectories == null)
                {
                    trajectories = new List<T>[sample.Count];

                    for (int i = 0; i < trajectories.Length; i++)
                        trajectories[i] = new List<T>();
                }

                for (int i = 0; i < sample.Count; i++)
                    trajectories[i].Add(sample[i]);
            }

            return trajectories.Select(traj => traj.ToArray()).ToArray();
        }

        /// <summary>
        /// Converts lists of trajectories to lists of samples
        /// </summary>
        public static IReadOnlyList<T[]> TrajectoriesToSamples<T>(T[][] trajectories)
        {
            T[][] samples = new T[trajectories[0].Length][];

            for (int t = 0; t < trajectories[0].Length; t++)
            {
                T[] sample = new T[trajectories.Length];

                for (int i = 0; i < sample.Length; i++)
                    sample[i] = trajectories[i][t];

                samples[t] = sample;
            }

            return samples.ToArray();
        }

        public static IEnumerable<Indexed<T>> IndexedHystericalDownSample<T>(IEnumerable<T> sequence, int hnum, IEqualityComparer<T> comparer, int startIndex = 0, int step = 1)
        {
            return HystericalDownSample(Indexed<T>.IndexSeq(sequence, 0, 1), hnum, new UnindexedIndexedComparer<T>(comparer));
        }

        public static IEnumerable<T> HystericalDownSample<T>(IEnumerable<T> sequence, int hnum, IEqualityComparer<T> comparer)
        {
            bool first = true;
            T current = default(T);

            int hcount = 0;

            foreach (T t in sequence)
            {
                if (first)
                {
                    current = t;
                    hcount = 0;
                    continue;
                }
                else if (comparer.Equals(current, t))
                {
                    hcount++;

                    if (hcount == hnum)
                    {
                        yield return current;
                    }
                }
                else
                {
                    current = t;
                    hcount = 0;
                }
            }
        }

        public static double[][] Threshold(IReadOnlyList<IReadOnlyList<double>> trajectories, double threshold, double min, double max)
        {
            return trajectories.Select(t => t == null ? null : t.Select(s => s >= threshold ? max : min).ToArray()).ToArray();
        }

        public static double[][] MovingAverages(IReadOnlyList<IReadOnlyList<double>> trajectories, int windowRadius)
        {
            return trajectories.Select(t => t == null ? null : MovingAverage(t, windowRadius)).ToArray();
        }

        public static double[] MovingAverage(IReadOnlyList<double> trajectory, int windowRadius)
        {
            double[] res = new double[trajectory.Count];

            for (int i = 0; i < res.Length; i++)
            {
                double total = 0.0;
                int c = 0;
                for (int j = i - windowRadius; j <= i + windowRadius; j++)
                {
                    if (j >= 0 && j < trajectory.Count)
                    {
                        total += trajectory[j];
                        c++;
                    }
                }

                res[i] = total / c;
            }

            return res;
        }

        public static double[][] MeanDownsample(IReadOnlyList<IReadOnlyList<double>> trajectories, int resolution)
        {
            return trajectories.Select(t => t == null ? null : MeanDownsample(t, resolution)).ToArray();
        }

        public static double[] MeanDownsample(IReadOnlyList<double> trajectory, int resolution)
        {
            double[] res = new double[trajectory.Count / resolution];

            for (int i = 0; i < res.Length; i++)
            {
                double total = 0.0;
                int c = 0;
                for (int j = i * resolution; j < i * resolution + resolution; j++)
                {
                    if (j >= 0 && j < trajectory.Count)
                    {
                        total += trajectory[j];
                        c++;
                    }
                }

                res[i] = total / c;
            }

            return res;
        }

        public static double[][] Downsample(IReadOnlyList<IReadOnlyList<double>> trajectories, int resolution)
        {
            return trajectories.Select(t => t == null ? null : Downsample(t, resolution)).ToArray();
        }

        public static double[] Downsample(IReadOnlyList<double> trajectory, int resolution)
        {
            double[] res = new double[trajectory.Count / resolution];

            for (int i = 0; i < res.Length; i++)
            {
                res[i] = trajectory[i * resolution];
            }

            return res;
        }

        public static void MeanCentreInplace(this double[] samples)
        {
            double mean = samples.Average();
            for (int i = 0; i < samples.Length; i++)
                samples[i] -= mean;
        }

        /// <summary>
        /// (L1)
        /// </summary>
        public static void NormaliseInplace(this double[] samples)
        {
            double l1s = samples.Sum(x => Math.Abs(x));
            if (l1s > 0)
            {
                double il1s = 1.0 / l1s;
                for (int i = 0; i < samples.Length; i++)
                    samples[i] *= il1s;
            }
        }

        /// <summary>
        /// Attempts to create a directory of the given name is none already exists
        /// Throws if anything goes wrong
        /// </summary>
        public static void EnsureDirectory(string name)
        {
            if (name == "")
                return;

            if (!System.IO.Directory.Exists(name))
            {
                System.IO.Directory.CreateDirectory(name);
            }
        }

        /// <summary>
        /// Approximates the solution to f(x) = target, assuming f is a monotonically non-decreasing function in and on the given bound
        /// Accurate to the given epsillon
        /// </summary>
        /// <param name="f">The function to solve</param>
        /// <param name="start">The start position in the function</param>
        /// <param name="target">Target value</param>
        public static double BinarySearch(Func<double, double> f, double target, Range bound, double eps)
        {
            double binSearch(double l, double fl, double r, double fr)
            {
                var m = (l + r) * 0.5;
                double fm = f(m);

                if (Math.Abs(fm - target) < eps || (l == r))
                    return m;
                else if (m > target)
                    return binSearch(l, fl, m, fm);
                else
                    return binSearch(m, fm, r, fr);
            }

            return binSearch(bound.Min, f(bound.Min), bound.Max, f(bound.Max));
        }

        /// <summary>
        /// Finds the value x that maximise f(x) in the given bounds, assuming f has a negative second differential everywhere
        /// Accurate to the given epsillon
        /// </summary>
        /// <param name="f">The function to solve</param>
        /// <param name="bound">The bounds wherein the search</param>
        /// <param name="eps">The minimum interval to consider</param>
        public static double BinaryArgMax(Func<double, double> f, Range bound, double eps)
        {
            double binSearch(double l, double fl, double r, double fr)
            {
                var ml = l * 0.6 + r * 0.4;
                var mr = l * 0.4 + r * 0.6;
                double fml = f(ml);
                double fmr = f(mr);

                if (Math.Abs(l - r) < eps)
                    return (l + r) * 0.5;
                else if (fml > fmr)
                    return binSearch(l, fl, mr, fmr);
                else
                    return binSearch(ml, fml, r, fr);
            }

            return binSearch(bound.Min, f(bound.Min), bound.Max, f(bound.Max));
        }

        /// <summary>
        /// Finds the value x that maximise f(x) in the given bounds
        /// Accurate to the given epsillon
        /// </summary>
        /// <param name="f">The function to solve</param>
        /// <param name="bound">The bounds wherein the search</param>
        /// <param name="eps">The minimum interval to consider</param>
        public static double ArgMax(Func<double, double> f, Range bound, double eps)
        {
            var bestx = bound.Min;
            var best = f(bound.Min);

            for (double x = bound.Min; x < bound.Max;)
            {
                x = x + eps;
                if (x > bound.Max)
                    x = bound.Max;

                var fx = f(x);
                if (fx > best)
                {
                    bestx = x;
                    best = fx;
                }
            }

            return bestx;
        }

        /// <summary>
        /// Cuts the sequence into contiguous sub-sequences which all have the same key
        /// </summary>
        /// <param name="seq">The sequence to cut</param>
        /// <param name="key">A function which returns the key</param>
        /// <param name="reuseBuffer">Whether the method may reuse the same buffer each time (e.g. return the same list, but with a different contence for each sequence); unless you have a very good reason, don't set this to tru.</param>
        public static IEnumerable<IEnumerable<T>> CutBy<T, K>(this IEnumerable<T> seq, Func<T, K> key, bool reuseBuffer = false) where K : IEquatable<K>
        {
            // this doesn't use CutOn in order to avoid calling key may times

            List<T> current = null;
            K last = default(K);

            foreach (var t in seq)
            {
                K next = key(t);

                if (current == null)
                {
                    current = new List<T>();
                }
                else if (!next.Equals(last))
                {
                    yield return current;
                    current = new List<T>();
                }

                current.Add(t);
                last = next; // this means you can use not-strictly equality things, which you definitely should not
            }

            if (current != null)
                yield return current;
        }

        /// <summary>
        /// Cuts the sequence into contiguous sub-sequences
        /// </summary>
        /// <param name="seq">The sequence to cut</param>
        /// <param name="cutter">A function which return whether to cut between the given elements</param>
        /// <param name="reuseBuffer">Whether the method may reuse the same buffer each time (e.g. return the same list, but with a different contence for each sequence); unless you have a very good reason, don't set this to tru.</param>
        public static IEnumerable<IEnumerable<T>> CutOn<T>(this IEnumerable<T> seq, Func<T, T, bool> cutter, bool reuseBuffer = false)
        {
            List<T> current = null;
            T last = default(T);

            foreach (var t in seq)
            {
                if (current == null)
                {
                    current = new List<T>();
                }
                else if (cutter(last, t))
                {
                    yield return current;
                    current = new List<T>();
                }

                current.Add(t);
                last = t; // this means you can use not-strictly equality things, which you definitely should not
            }

            if (current != null)
                yield return current;
        }

        /// <summary>
        /// Finds the index of the element with the smallest value
        /// Returns the index of the (first) smallest element, throws if the list is empty
        /// </summary>
        public static int IndexMin<T, K>(this IReadOnlyList<T> list, Func<T, K> func) where K : IComparable<K>
        {
            int bestIndex = -1;
            K bestK = default(K);
            bool first = true;

            for (int i = 0; i < list.Count; i++)
            {
                K k = func(list[i]);

                if (first || k.CompareTo(bestK) < 0)
                {
                    bestIndex = i;
                    bestK = k;

                    first = false;
                }
            }

            if (first)
            {
                throw new ArgumentException("Enumeration must not be empty to compute argmin");
            }

            return bestIndex;
        }

        /// <summary>
        /// Finds the index of the first element that matches the predicate
        /// Returns true if a match was found, otherwise false
        /// </summary>
        public static bool FirstIndex<T>(this IEnumerable<T> seq, Predicate<T> predicate, out int index, out T entryAtIndex)
        {
            int i = 0;
            foreach (T t in seq)
            {
                if (predicate(t))
                {
                    entryAtIndex = t;
                    index = i;
                    return true;
                }

                i++;
            }

            // nothing
            entryAtIndex = default(T);
            index = -1;
            return false;
        }

        /// <summary>
        /// Finds the index of the first element that matches the predicate, which is followed by (hnum - 1) entries which also mathc the predicate
        /// Returns true if a match was found, otherwise false
        /// </summary>
        public static bool HystericalFirstIndex<T>(this IEnumerable<T> seq, Predicate<T> predicate, int hnum, out int index, out T entryAtIndex)
        {
            if (hnum < 1)
                throw new ArgumentException("HNum must be greater than or equal to 1", nameof(hnum));

            int hcount = 0;

            // make flow-analysis happy
            entryAtIndex = default(T);
            index = -1;

            int i = 0;
            foreach (T t in seq)
            {
                if (predicate(t))
                {
                    if (hcount == 0)
                    {
                        entryAtIndex = t;
                        index = i;
                        hcount = 1;
                    }
                    else
                    {
                        hcount++;
                    }

                    if (hcount == hnum)
                        return true;
                }
                else
                {
                    hcount = 0;
                }

                i++;
            }

            // nothing
            entryAtIndex = default(T);
            index = -1;
            return false;
        }

        public static IEnumerable<int> IndexWhere<T>(this IEnumerable<T> seq, Predicate<T> predicate)
        {
            int i = 0;
            foreach (T t in seq)
            {
                if (predicate(t))
                    yield return i;

                i++;
            }
        }

        public static int IndexMax<T, K>(this IReadOnlyList<T> list, Func<T, K> func) where K : IComparable<K>
        {
            int bestIndex = -1;
            K bestK = default(K);
            bool first = true;

            for (int i = 0; i < list.Count; i++)
            {
                K k = func(list[i]);

                if (first || k.CompareTo(bestK) > 0)
                {
                    bestIndex = i;
                    bestK = k;

                    first = false;
                }
            }

            if (first)
            {
                throw new ArgumentException("Enumeration must not be empty to compute argmin");
            }

            return bestIndex;
        }

        public static int IndexMin<T, K>(this T[] arr, Func<T, K> func) where K : IComparable<K>
        {
            if (arr.Length == 0)
            {
                throw new ArgumentException("Array must not be empty to compute indexmin");
            }

            int best = 0;
            K bestK = func(arr[0]);

            for (int i = 1; i < arr.Length; i++)
            {
                K k = func(arr[i]);

                if (k.CompareTo(bestK) < 0)
                {
                    best = i;
                    bestK = k;
                }
            }

            return best;
        }

        public static int IndexMax<T, K>(this T[] arr, Func<T, K> func) where K : IComparable<K>
        {
            if (arr.Length == 0)
            {
                throw new ArgumentException("Array must not be empty to compute indexmax");
            }

            int best = 0;
            K bestK = func(arr[0]);

            for (int i = 1; i < arr.Length; i++)
            {
                K k = func(arr[i]);

                if (k.CompareTo(bestK) > 0)
                {
                    best = i;
                    bestK = k;
                }
            }

            return best;
        }

        public static int IndexMax<T>(this ArraySegment<T> arr) where T : IComparable<T>
        {
            if (arr.Count == 0)
            {
                throw new ArgumentException("Array must not be empty to compute indexmax");
            }

            int best = 0;
            T bestT = arr.Array[arr.Offset];

            for (int i = 1; i < arr.Count; i++)
            {
                T t = arr.Array[arr.Offset + i];

                if (t.CompareTo(bestT) > 0)
                {
                    best = i;
                    bestT = t;
                }
            }

            return best;
        }

        public static T ArgMin<T, K>(this IEnumerable<T> enumeration, Func<T, K> func) where K : IComparable<K>
        {
            T bestT = default(T);
            K bestK = default(K);
            bool first = true;

            foreach (T t in enumeration)
            {
                K k = func(t);

                if (first || k.CompareTo(bestK) < 0)
                {
                    bestT = t;
                    bestK = k;

                    first = false;
                }
            }

            if (first)
            {
                throw new ArgumentException("Enumeration must not be empty to compute argmin");
            }

            return bestT;
        }

        public static T ArgMax<T, K>(this IEnumerable<T> enumeration, Func<T, K> func) where K : IComparable<K>
        {
            T bestT = default(T);
            K bestK = default(K);
            bool first = true;

            foreach (T t in enumeration)
            {
                K k = func(t);

                if (first || k.CompareTo(bestK) > 0)
                {
                    bestT = t;
                    bestK = k;

                    first = false;
                }
            }

            if (first)
            {
                throw new ArgumentException("Enumeration must not be empty to compute argmax");
            }

            return bestT;
        }

        public static T ArgMedian<T, K>(this IEnumerable<T> enumeration, Func<T, K> func) where K : IComparable<K>
        {
            var arr = enumeration.OrderBy(func).ToArray();

            if (arr.Length == 0)
                throw new ArgumentException("Enumeration must not be empty to compute argmax");

            return arr[arr.Length / 2];
        }

        public static double Entropy(IEnumerable<double> frequencies, double bitBase = 2)
        {
            // not apparent which base this is in...
            //return MathNet.Numerics.Statistics.StreamingStatistics.Entropy(frequencies) * Math.Log(2) / Math.Log(bitBase);

            double acc = 0;

            foreach (var f in frequencies)
            {
                double logTerm = Math.Log(f, bitBase);

                if (double.IsNaN(logTerm) || double.IsNegativeInfinity(logTerm))
                    continue; // implies f very small

                acc -= f * logTerm;
            }

            return acc;
        }

        /// <summary>
        /// Computes the Χ² error between observed and expected frequencies
        /// </summary>
        /// <param name="observedFrequencies">The frequencies observed</param>
        /// <param name="expectedFrequencies">The expected frequencies</param>
        /// <returns>The Χ² computed as the sum of  (o - e)^2 / e  terms</returns>
        public static double ChiSquaredError<T>(IReadOnlyDictionary<T, double> observedFrequencies, IReadOnlyDictionary<T, double> expectedFrequencies)
        {
            List<double> observed = new List<double>();
            List<double> expected = new List<double>();

            foreach (T t in observedFrequencies.Keys)
            {
                observed.Add(observedFrequencies[t]);
                expected.Add(expectedFrequencies[t]);
            }

            return ChiSquaredError(observed, expected);
        }

        /// <summary>
        /// Computes the Χ² error between observed and expected frequencies
        /// </summary>
        /// <param name="observedFrequencies">The frequencies observed</param>
        /// <param name="expectedFrequencies">The expected frequencies</param>
        /// <returns>The Χ² computed as the sum of  (o - e)^2 / e  terms</returns>
        public static double ChiSquaredError(IReadOnlyList<double> observedFrequencies, IReadOnlyList<double> expectedFrequencies)
        {
            System.Diagnostics.Debug.Assert(observedFrequencies.Count == expectedFrequencies.Count, $"Misc/ChiSquaredError.ctor(string): observedFrequencies and expectedFrequencies must have same count");

            double acc = 0.0;

            int len = observedFrequencies.Count;

            for (int i = 0; i < len; i++)
            {
                double o = observedFrequencies[i];
                double e = expectedFrequencies[i];

                double component = (o - e) * (o - e) / e;

                acc += component;
            }

            return acc;
        }

        public static T[] Create<T>(int length, T v)
        {
            T[] arr = new T[length];

            for (int i = 0; i < length; i++)
            {
                arr[i] = v;
            }

            return arr;
        }

        public static T[] Create<T>(int length, Func<int, T> func)
        {
            T[] arr = new T[length];

            for (int i = 0; i < length; i++)
            {
                arr[i] = func(i);
            }

            return arr;
        }

        public static T[][] CreateEmpty<T>(int length1, int length2)
        {
            T[][] arr = new T[length1][];

            for (int i = 0; i < length1; i++)
            {
                arr[i] = new T[length2];
            }

            return arr;
        }

        public static Linear.Vector<double> Pointwise(Linear.Vector<double> vector, Func<double, double> func)
        {
            return PointwiseInplace(vector.Clone(), func);
        }

        public static Linear.Vector<double> PointwiseInplace(Linear.Vector<double> vector, Func<double, double> func)
        {
            for (int i = 0; i < vector.Count; i++)
            {
                vector[i] = func(vector[i]);
            }
            return vector;
        }

        /// <summary>
        /// An immutable range of doubles, defined by inclusive upper (Max) and lower (Min) bounds
        /// </summary>
        [StateClass]
        public class Range
        {
            [Obsolete]
            private Range()
            { }

            public Range(double min, double max)
            {
                if (max < min)
                    throw new ArgumentException("max must not be less than min");

                Min = min;
                Max = max;
            }

            /// <summary>
            /// The minimum (inclusive) value in the Range
            /// </summary>
            [SimpleStateProperty("Min")]
            public double Min { get; private set; }

            /// <summary>
            /// The maximum (inclusive) value in the Range
            /// </summary>
            [SimpleStateProperty("Max")]
            public double Max { get; private set; }

            /// <summary>
            /// Clamps a value to the range
            /// </summary>
            /// <param name="v">The value to clamp</param>
            /// <returns>Min if v is less than Min, Max if v is greater than Max, else v</returns>
            public double Clamp(double v)
            {
                if (v < Min)
                    return Min;
                else if (v > Max)
                    return Max;
                else
                    return v;
            }

            /// <summary>
            /// Determines whether the given value is within the range (inclusived of Min and Max)
            /// </summary>
            /// <param name="v">The query value</param>
            /// <returns>True if v gte Min and v lte Max, else False</returns>
            public bool Within(double v)
            {
                return v >= Min && v <= Max;
            }

            public override string ToString()
            {
                return $"({Min}, {Max})";
            }

            public double Sample(Random rnd)
            {
                return rnd.NextDouble() * (Max - Min) + Min;
            }
        }

        public static double Saturate(double v, double min, double threshold, double max)
        {
            if (v < threshold)
                return min;
            else
                return max;
        }

        public static Linear.Vector<double> Saturate(this Linear.Vector<double> vector, double min, double threshold, double max)
        {
            return Linear.CreateVector.Dense<double>(vector.Count, i => Misc.Saturate(vector[i], min, threshold, max));
        }

        public static int Clamp(int v, int min, int max)
        {
            if (v < min)
                return min;
            else if (v > max)
                return max;
            else
                return v;
        }

        public static double Clamp(double v, double min, double max)
        {
            if (v < min)
                return min;
            else if (v > max)
                return max;
            else
                return v;
        }

        public static T[][] Unroll<T>(T[] arr, int foldCount)
        {
            T[][] narr = new T[arr.Length / foldCount][];

            int o = 0;
            for (int i = 0; i < narr.Length; i++)
            {
                narr[i] = new T[foldCount];

                for (int j = 0; j < foldCount; j++)
                {
                    narr[i][j] = arr[o];
                    o++;
                }
            }

            return narr;
        }

        public static T[] Roll<T>(T[][] arr)
        {
            T[] narr = new T[arr.Sum(a => a.Length)];

            int o = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    narr[o] = arr[i][j];
                    o++;
                }
            }

            return narr;
        }

        public static void ShuffleInplace<T>(MathNet.Numerics.Random.RandomSource rand, T[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                int j = rand.Next(i, arr.Length);

                if (i != j)
                {
                    T t = arr[i];
                    arr[i] = arr[j];
                    arr[j] = t;
                }
            }
        }

        public static T[,] Unjagged<T>(T[][] jagged)
        {
            T[,] res = new T[jagged.Length, jagged[0].Length];

            for (int i = 0; i < jagged.Length; i++)
            {
                for (int j = 0; j < jagged[0].Length; j++)
                {
                    res[i, j] = jagged[i][j];
                }
            }

            return res;
        }

        public static T[][] Jagged<T>(T[,] unjagged)
        {
            T[][] res = new T[unjagged.GetLength(0)][];

            for (int i = 0; i < res.Length; i++)
            {
                res[i] = new T[unjagged.GetLength(1)];

                for (int j = 0; j < res[i].Length; j++)
                {
                    res[i][j] = unjagged[i, j];
                }
            }

            return res;
        }

        public static T[,] Rotate<T>(T[,] arr)
        {
            T[,] res = new T[arr.GetLength(1), arr.GetLength(0)];

            for (int i = 0; i < arr.GetLength(1); i++)
            {
                for (int j = 0; j < arr.GetLength(0); j++)
                {
                    res[i, j] = arr[j, arr.GetLength(1) - i - 1];
                }
            }

            return res;
        }

        public static IEnumerable<double> DRange(double s, double e, double d)
        {
            double x = s;
            for (; x < e; x += d)
                yield return x;
            if (x < e + d * 0.9)
                yield return e;
        }

        public static double NextSignedUniform(this Random rand, double maxAbs)
        {
            return (rand.NextDouble() * 2.0 - 1.0) * maxAbs;
        }

        public static double NextNoise(this Random rand, double magnitude, NoiseType noiseType)
        {
            switch (noiseType)
            {
                case NoiseType.Uniform:
                    return rand.NextSignedUniform(magnitude);
                case NoiseType.Gaussian:
                    return MathNet.Numerics.Distributions.Normal.Sample(rand, 0.0, magnitude);
                case NoiseType.Binary:
                    return rand.NextDouble() < 0.5 ? magnitude : -magnitude;
                default:
                    throw new Exception("Unrecognised NoiseType: " + noiseType);
            }
        }

        public static string NowTime => DateTime.Now.ToString("yyyyMMdd'T'HHmmss");

        // these three are not efficient
        public static IEnumerable<MatrixEntryAddress> EntriesWhere<T>(this Linear.Matrix<T> mat, Predicate<T> predicate) where T : struct, IEquatable<T>, IFormattable
        {
            return mat.EnumerateIndexed(Linear.Zeros.Include).Where(p => predicate(p.Item3)).Select(p => new MatrixEntryAddress(p.Item1, p.Item2));
        }

        public static MatrixEntryAddress EntryMax<T, K>(this Linear.Matrix<T> mat, Func<T, K> func) where T : struct, IEquatable<T>, IFormattable where K : IComparable<K>
        {
            var argMax = mat.EnumerateIndexed(Linear.Zeros.Include).ArgMax(p => func(p.Item3));
            return new MatrixEntryAddress(argMax.Item1, argMax.Item2);
        }

        public static MatrixEntryAddress EntryMin<T, K>(this Linear.Matrix<T> mat, Func<T, K> func) where T : struct, IEquatable<T>, IFormattable where K : IComparable<K>
        {
            var argMin = mat.EnumerateIndexed(Linear.Zeros.Include).ArgMin(p => func(p.Item3));
            return new MatrixEntryAddress(argMin.Item1, argMin.Item2);
        }

        /// <summary>
        /// Returns a copy of the given matrix, where each element is replaced by zero with probability pruneProbability.
        /// </summary>
        public static Linear.Matrix<double> Prune(RandomSource rand, Linear.Matrix<double> matrix, double pruneProbability)
        {
            var res = matrix.Clone();

            for (int r = 0; r < matrix.RowCount; r++)
            {
                for (int c = 0; c < matrix.ColumnCount; c++)
                {
                    bool prune = rand.NextDouble() < pruneProbability;
                    res[r, c] = prune ? 0.0 : matrix[r, c];
                }
            }

            return res;
        }

        /// <summary>
        /// 3*ab -> ababab
        /// Returns null if invalid
        /// </summary>
        public static string ExpandStar(string maybeStar)
        {
            if (maybeStar.Contains("*"))
            {
                string[] data = maybeStar.Split('*');

                if (data.Length != 2)
                    return null;

                if (!int.TryParse(data[0], out int l))
                    return null;
                string r = data[1];

                return string.Join("", Enumerable.Repeat(r, l));
            }
            else
            {
                return maybeStar;
            }
        }

        public static Linear.Vector<double> ParseExtremesVector(string str, double min, double max, double zero)
        {
            if (str.Contains(";"))
            {
                string[] data = str.Split(';');
                return Linear.CreateVector.DenseOfEnumerable(data.Select(double.Parse));
            }
            else
            {
                str = Misc.ExpandStar(str);

                double extremeTranslator(char c)
                {
                    if (c == '-')
                        return min;
                    if (c == '+')
                        return max;
                    if (c == '0')
                        return zero;
                    throw new Exception($"Unrecognised extreme char: {c}; should be one of -, +, or 0");
                }

                return Linear.CreateVector.DenseOfEnumerable(str.Select(extremeTranslator));
            }
        }

        public static Linear.Matrix<double> Correlate(this Linear.Vector<double> vec)
        {
            return Linear.CreateMatrix.Dense<double>(vec.Count, vec.Count, (r, c) => vec[r] * vec[c]);
        }

        public static double[] ParseDoubleList(string str)
        {
            return str.Split(';').Select(double.Parse).ToArray();
        }

        public static int[] ParseIntList(string str)
        {
            return str.Split(';').Select(ParseInt).ToArray();
        }

        public static Linear.Matrix<double> IterativeNormalise(Linear.Matrix<double> mat, Linear.Vector<double> normalisationRowSums, Linear.Vector<double> normalisationColumnSums, double cutoff = 10E-5)
        {
            var mat2 = mat;

            double l2diff;
            do
            {
                mat = mat2.Clone();

                // row norm

                for (int r = 0; r < mat.RowCount; r++)
                {
                    double rowSum = 0.0;

                    for (int c = 0; c < mat.ColumnCount; c++)
                        rowSum += c != r ? mat[r, c] : 0.0;

                    var irs = normalisationRowSums[r] / rowSum;

                    for (int c = 0; c < mat.ColumnCount; c++)
                        mat2[r, c] *= c != r ? irs : 1.0;
                }

                // col norm

                for (int c = 0; c < mat.ColumnCount; c++)
                {
                    double colSum = 0.0;

                    for (int r = 0; r < mat.RowCount; r++)
                        colSum += c != r ? mat[r, c] : 0.0;

                    var ics = normalisationColumnSums[c] / colSum;

                    for (int r = 0; r < mat.RowCount; r++)
                        mat2[r, c] *= c != r ? ics : 1.0;
                }

                l2diff = (mat - mat2).Enumerate().Sum(x => x * x);
            }
            while (l2diff >= cutoff);

            return mat2;
        }

        public static void FillRandom(Linear.Vector<double> v, Random rand, double magnitude, NoiseType noiseType)
        {
            for (int i = 0; i < v.Count; i++)
                v[i] = Misc.NextNoise(rand, magnitude, noiseType);
        }
    }

    // State Provider registered in GraphSerialisation.cs
    public enum NoiseType : int
    {
        Uniform,
        Gaussian,
        Binary,
    }

    public class Combinatorics
    {
        /// <summary>
        /// Enumerates parameterisations, such that the total of all entryies in any assignment is total
        /// i.e. enumerates all vectors s of length paramCount such that sum_i(s_i) = total, and all s_i >= 0
        /// </summary>
        public static IEnumerable<double[]> EnumerateTotalUnorderedParameterisations(int paramCount, double total, int resolution)
        {
            int volume = resolution * paramCount;
            double fact = total / volume;
            double[] s = new double[paramCount];

            foreach (var p in Combinatorics.EnumerateOrderedAssignments(volume, paramCount))
            {
                // each p is a unique assignment of one unit of weight to a parameter
                for (int i = 0; i < s.Length; i++)
                    s[i] = 0;
                foreach (int a in p)
                    s[a]++;
                for (int i = 0; i < s.Length; i++)
                    s[i] *= fact;
                yield return s;
            }
        }

        /// <summary>
        /// Enumerates parameterisations, such that the magnitude of each entry in the assignment is no more than the previous, and the total of all entryies in any assignment is total
        /// i.e. enumerates all vectors s of length paramCount such that sum_i(s_i) = total, and s_i >= s_j for all i,j were i &lt; j, and all s_i >= 0
        /// </summary>
        public static IEnumerable<double[]> EnumerateOrderedParameterisations(int paramCount, double total, int resolution)
        {
            int volume = resolution * paramCount;
            double fact = total / volume;
            int[] s = new int[paramCount];
            s[0] = volume;
            return EnumerateOrderedParameterisations(s, 1).Select(ir => ir.Select(i => i * fact).ToArray());
        }

        private static IEnumerable<int[]> EnumerateOrderedParameterisations(int paramCount, int total)
        {
            int[] s = new int[paramCount];
            s[0] = total;
            return EnumerateOrderedParameterisations(s, 1);
        }

        private static IEnumerable<int[]> EnumerateOrderedParameterisations(int[] current, int p)
        {
            if (p >= current.Length)
            {
                yield return current;
                yield break;
            }

            int[] c = current.ToArray(); // so we don't have to 'undo' anything
            do
            {
                foreach (var s in EnumerateOrderedParameterisations(c, p + 1))
                    yield return s;

                c[p]++;
                c[0]--;
            }
            while (c[p] <= c[p - 1] && c[1] <= c[0]);
        }

        // NOTE: this method name is terrible; provides the same functionality as EnumerateAssignments
        /// <summary>
        /// Enumerates an array of all posible values as determined by maxes
        /// i.e. enumerates all vectors s, where s_i in [0, maxes_i - 1]
        /// Yields the same (modified) array every iteration; does not modify maxes
        /// </summary>
        /// <param name="maxes">Upbounds of each array element</param>
        public static IEnumerable<int[]> EnumerateCombinations(int[] maxes)
        {
            int[] s = new int[maxes.Length];

            yield return s;

            while (true)
            {
                int p = 0;
                while (p < s.Length && ++s[p] == maxes[p])
                    s[p++] = 0;

                if (p == s.Length)
                    yield break;

                yield return s;
            }
        }
        
        /// <summary>
        /// Enumerates all vectors s where s_i in [0, maxes_i - 1]
        /// Yields the same array every time, and requires that it not be modified by the consumer
        /// Does not modify maximums
        /// </summary>
        /// <param name="maximums">The maximum values for each entry of the assignment</param>
        public static IEnumerable<int[]> EnumerateAssignments(int[] maximums)
        {
            int[] assignment = new int[maximums.Length];
            yield return assignment;

            while (NextAssignment(assignment, maximums))
                yield return assignment;
        }
        
        /// <summary>
        /// Produces the next element in an arbitrarily ordered sequence of all vectors s where s_i in [0, maxes_i - 1]
        /// Returns true if another element can be provided, in which case assignment is updated to contain the next assignemnt
        /// Returns false if the end of the sequence has been reached, in which case the assignment contains only zero values
        /// The initial assignment must be an array of all zeros
        /// No check is performed to verify that assignment and maximums are of the same length, or that the current assignent is valid
        /// </summary>
        /// <param name="assignment">The current assignment</param>
        /// <param name="maximums">The maximum values for each entry of the assignment</param>
        public static bool NextAssignment(int[] assignment, int[] maximums)
        {
            int p = 0;
            while (p < assignment.Length && ++assignment[p] == maximums[p])
                assignment[p++] = 0;

            return p != assignment.Length;
        }
        
        /// <summary>
        /// Enumerates all vectors s of length count where s_i in [0, maxes_i - 1] and s_i &le;= s_j for all i,j where i &lt; j
        /// Yields the same array every time, and requires that it not be modified by the consumer
        /// </summary>
        /// <param name="count">The number of entries per assignment (must be positive)</param>
        /// <param name="maximums">The maximum values for each entry of the assignment (must be non-negative)</param>
        public static IEnumerable<int[]> EnumerateOrderedAssignments(int count, int max)
        {
            if (count <= 0)
                throw new ArgumentException("Count must be positive", nameof(count));
            if (max < 0)
                throw new ArgumentException("Maximum must be non-negative", nameof(max));

            int[] assignment = new int[count];
            yield return assignment;

            while (NextOrderedAssignment(assignment, max))
                yield return assignment;
        }
        
        /// <summary>
        /// Produces the next element in an arbitrarily ordered sequence of all vectors s where s_i in [0, max], and s_i &lt;= s_j for all i,j where i &lt; j
        /// Returns true if another element can be provided, in which case assignment is updated to contain the next assignemnt
        /// Returns false if the end of the sequence has been reached, in which case the assignment contains only zero values
        /// The initial assignment must be an array of all zeros
        /// No check is performed to verify that the current assignent is valid
        /// </summary>
        /// <param name="assignment">The current assignment</param>
        /// <param name="maximum">The maximum value for each entry of the assignment</param>
        public static bool NextOrderedAssignment(int[] assignment, int maximum)
        {
            int p = 0;
            while (p < assignment.Length - 1 && assignment[p] == assignment[p+1])
                assignment[p++] = 0;

            assignment[p]++;

            // handle last element specially
            if (p == assignment.Length - 1)
            {
                if (assignment[p] >= maximum) // this sneakily covers the maximum = 0 case
                {
                    assignment[p] = 0;
                    return false;
                }
            }

            return true;
        }
        
        /// <summary>
        /// Enumerates all vectors s of length count such that sum_i(s_i) = total, and all s_i >= 0
        /// Yields the same array every time, and requires that it not be modified by the consumer
        /// </summary>
        public static IEnumerable<int[]> EnumerateTotalUnorderedAssignment(int count, int total)
        {
            if (count <= 0)
                throw new ArgumentException("Count must be positive", nameof(count));
            if (total < 0)
                throw new ArgumentException("Total must be non-negative", nameof(total));
            
            int[] s = new int[count];

            foreach (var p in Combinatorics.EnumerateOrderedAssignments(total, count))
            {
                // each p is a unique assignment of one unit of weight to a parameter
                for (int i = 0; i < s.Length; i++)
                    s[i] = 0;
                foreach (int a in p)
                    s[a]++;
                yield return s;
            }

            // TODO: implement this without indirecting through EnumerateOrderedAssignments
            //int[] assignment = new int[count];
            //assignment[0] = total;
            //yield return assignment;

            //while (NextTotalUnorderedAssignment(assignment))
            //    yield return assignment;
        }
        
        /// <summary>
        /// Produces the next element in an arbitrarily ordered sequence of all vectors s such that sum_i(s_i) = total, and all s_i >= 0
        /// Returns true if another element can be provided, in which case assignment is updated to contain the next assignemnt
        /// Returns false if the end of the sequence has been reached, in which case the assignment contains only zero values
        /// The initial assignment must be an array with the total in the zeroth position
        /// No check is performed to verify that the current assignent is valid
        /// </summary>
        public static bool NextTotalUnorderedAssignment(int[] assignment)
        {
            throw new NotImplementedException("Not yet implemeneted; use EnumerateTotalUnorderedAssignment instead");
        }

        /// <summary>
        /// Enumerates all boolean assignments of a given length.
        /// Returns the same array every iteration, so clone it if you need to hold onto it.
        /// Returns a single empty array if the length is zero.
        /// </summary>
        /// <param name="length">The number of booleans.</param>
        public static IEnumerable<bool[]> EnumerateBooleanAssignments(int length)
        {
            bool[] assignment = new bool[length];
            yield return assignment;

            int p = 0;
            while (p < length)
            {
                if (assignment[p])
                {
                    assignment[p] = false;
                    p++;
                }
                else
                {
                    assignment[p] = true;
                    yield return assignment;
                    p = 0;
                }
            }
        }
    }

    [StateClass]
    public class FileStuff
    {
        [Obsolete]
        protected FileStuff()
        { }

        public static FileStuff CreateNow(string outDir, string filePrefix, string titlePrefix, bool createDir = true, bool appendTimestamp = true)
        {
            if (appendTimestamp)
                outDir += "_" + Misc.NowTime;

            var stuff = new FileStuff(outDir, filePrefix, titlePrefix);

            if (createDir)
                stuff.CreateDir();

            return stuff;
        }

        public FileStuff(string outDir, string filePrefix, string titlePrefix)
        {
            OutDir = outDir;
            FilePrefix = filePrefix;
            TitlePrefix = titlePrefix;
        }
        
        public void CreateDir()
        {
            if (!System.IO.Directory.Exists(OutDir))
                System.IO.Directory.CreateDirectory(OutDir);
        }

        [SimpleStateProperty("OutDir")]
        public string OutDir { get; private set; }
        
        [SimpleStateProperty("FilePrefix")]
        public string FilePrefix { get; private set; }
        
        [SimpleStateProperty("TitlePrefix")]
        public string TitlePrefix { get; private set; }

        public string Title(string name)
        {
            return TitlePrefix + name;
        }

        public string File(string name)
        {
            return System.IO.Path.Combine(OutDir, FilePrefix + name);
        }

        public System.IO.StreamWriter Open(string name)
        {
            return new System.IO.StreamWriter(File(name));
        }

        public System.IO.FileStream CreateBin(string name)
        {
            return System.IO.File.Open(File(name), System.IO.FileMode.Create);
        }

        public FileStuff CreateSubStuff(string subDir)
        {
            return CreateSubStuff(subDir, FilePrefix, TitlePrefix);
        }

        public FileStuff CreateSubStuff(string subDir, string filePrefix, string titlePrefix)
        {
            string sdir = System.IO.Path.Combine(OutDir, subDir);
            System.IO.Directory.CreateDirectory(sdir);
            return new FileStuff(sdir, filePrefix, titlePrefix);
        }
    }

    /// <summary>
    /// Provides the same observable behaviour and interface (excepting some constructors) as a MathNet.Numerics.Random.MersenneTwister,
    /// but has some overrides to improve memory characteristics
    /// </summary>
    public class CustomMersenneTwister : MathNet.Numerics.Random.MersenneTwister
    {
        public CustomMersenneTwister(int seed) : base(seed, false)
        {
        }

        private readonly byte[] bytes4 = new byte[4];
        private readonly byte[] bytes8 = new byte[8];

        // implementation taken from https://raw.githubusercontent.com/mathnet/mathnet-numerics/master/src/Numerics/Random/RandomSource.cs on 2019-03-10
        // uses a shared byte buffer isntead of creating a new one upon each call
        protected override int DoSampleInt32WithNBits(int bitCount)
        {
            if (bitCount == 0)
            {
                return 0;
            }
            
            DoSampleBytes(bytes4);

            // every bit with independent uniform distribution
            uint uint32 = BitConverter.ToUInt32(bytes4, 0);

            // the least significant N bits with independent uniform distribution and the remaining bits zero
            uint uintN = uint32 >> (32 - bitCount);
            return (int)uintN;
        }
        
        // implementation taken from https://raw.githubusercontent.com/mathnet/mathnet-numerics/master/src/Numerics/Random/RandomSource.cs on 2019-03-10
        // uses a shared byte buffer isntead of creating a new one upon each call
        protected override long DoSampleInt64WithNBits(int bitCount)
        {
			// Fast case: Only 0 is allowed to be returned
            // No random call is needed
            if (bitCount == 0)
            {
                return 0;
            }
            
            DoSampleBytes(bytes8);

            // every bit with independent uniform distribution
            ulong uint64 = BitConverter.ToUInt64(bytes8, 0);

            // the least significant N bits with independent uniform distribution and the remaining bits zero
            ulong uintN = uint64 >> (64 - bitCount);
            return (long)uintN;
        }
    }

    /// <summary>
    /// This is a Struct because we want to void allocations in tight loops.
    /// This is definitely not a good idea, but I did it anyway.
    /// </summary>
    public struct RandomBits
    {
        public RandomBits(Random rand)
        {
            Rand = rand;
            CurrentBits = 0;
            CurrentBit = 0;
        }

        private const int LastBit = 1 << 30;
        private readonly Random Rand;
        private int CurrentBits; // non-negative number: only use the first 31 bits
        private int CurrentBit; // powers of 2, or 0 is we are unseeded

        /// <summary>
        /// Returns the next random boolean
        /// </summary>
        public bool NextBit()
        {
            if (CurrentBit <= 0)
            {
                CurrentBits = Rand.Next();
                CurrentBit = 1;
            }

            bool b = (CurrentBits & CurrentBit) > 0;

            if (CurrentBit < LastBit)
                CurrentBit <<= 1;
            else
                CurrentBit = 0;

            return b;
        }
    }

    public struct MatrixIndexHelper
    {
        public int Width { get; }
        public int Height { get; }

        public MatrixIndexHelper(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public static MatrixIndexHelper FromMatrix<T>(Linear.Matrix<T> mat) where T : struct, IFormattable, IEquatable<T>
        {
            return new MatrixIndexHelper(mat.ColumnCount, mat.RowCount);
        }

        public void ToRowCol(int index, out int row, out int col)
        {
            EnsureInBounds(index);

            row = index / Width;
            col = index % Width;
        }

        public MatrixEntryAddress ToMatrixEntryAddress(int index)
        {
            ToRowCol(index, out int r, out int c);
            return new MatrixEntryAddress(r, c);
        }

        public int ToIndex(int row, int col)
        {
            EnsureInBounds(row, col);

            return row * Width + col;
        }

        public int ToIndex(MatrixEntryAddress mae)
        {
            return ToIndex(mae.Row, mae.Col);
        }

        public void EnsureInBounds(int index)
        {
            if (index < 0 || index >= Width * Height)
                throw new ArgumentException(nameof(index), "Index not in bounds");
        }

        public bool InBounds(int index)
        {
            if (index < 0 || index >= Width * Height)
                return false;
            return true;
        }

        public void EnsureInBounds(int row, int col)
        {
            if (row < 0 || row >= Height)
                throw new ArgumentException(nameof(row), "Row not in bounds");
            if (col < 0 || col >= Width)
                throw new ArgumentException(nameof(col), "Col not in bounds");
        }

        public bool InBounds(int row, int col)
        {
            if (row < 0 || row >= Height)
                return false;
            if (col < 0 || col >= Width)
                return false;
            return true;
        }

        public bool TryOffsetRowCol(int row, int col, int offsetRow, int offsetCol, out int newRow, out int newCol)
        {
            row += offsetRow;
            col += offsetCol;

            if (InBounds(row, col))
            {
                newRow = row;
                newCol = col;
                return true;
            }
            else
            {
                newRow = -1;
                newCol = -1;
                return false;
            }
        }

        public bool TryOffsetIndex(int index, int offsetRow, int offsetCol, out int newIndex)
        {
            ToRowCol(index, out int r, out int c);
            r += offsetRow;
            c += offsetCol;

            if (InBounds(r, c))
            {
                newIndex = ToIndex(r, c);
                return true;
            }
            else
            {
                newIndex = -1;
                return false;
            }
        }
    }
}