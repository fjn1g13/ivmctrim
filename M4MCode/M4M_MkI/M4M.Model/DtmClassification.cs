﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Linear = MathNet.Numerics.LinearAlgebra;
using M4M.DataStructures;

namespace M4M
{
    public class Motivator
    {
        public int Index { get; }

        private readonly int[] _motivatees;
        public IReadOnlyList<int> Motivatees => _motivatees;

        public Motivator(int index, IEnumerable<int> motivatees)
        {
            Index = index;

            _motivatees = motivatees.ToArray();
            Array.Sort(_motivatees);
        }

        public bool Motivates(int other)
        {
            return Array.BinarySearch<int>(_motivatees, other) >= 0;
        }
    }

    public class Module : IComparable<Module>
    {
        public IReadOnlyList<int> Motivators { get; }

        private readonly int[] _motivatees;
        public IReadOnlyList<int> Motivatees => _motivatees;

        public Module(IEnumerable<int> motivators, IEnumerable<int> motivatees)
        {
            Motivators = motivators.OrderBy(i => i).ToArray();
            _motivatees = motivatees.OrderBy(i => i).ToArray();
        }

        public bool Motivates(int other)
        {
            return Array.BinarySearch<int>(_motivatees, other) >= 0;
        }

        // order by motivators, then motivatees (lengths first)
        public int CompareTo(Module other)
        {
            var cmp = Motivators.Count.CompareTo(other.Motivators.Count);

            if (cmp != 0)
                return cmp;

            cmp = Motivatees.Count.CompareTo(other.Motivatees.Count);

            if (cmp != 0)
                return cmp;

            cmp = ComparableList<int>.Compare(Motivators, other.Motivators);

            if (cmp != 0)
                return cmp;

            cmp = ComparableList<int>.Compare(Motivatees, other.Motivatees);
            
            return cmp;
        }

        public string SummaryString
        {
            get => $"{Motivators.Count}/{Motivatees.Count}";
        }

        public static IEnumerable<Module> OrderByTraits(IEnumerable<Module> modules)
        {
            var sep = new[] { int.MinValue }; // use to separate the Motivators and Motivatees
            return modules.OrderBy(m => new LengthIndependentComparableList<int>(m.Motivators.Concat(sep).Concat(m.Motivatees).ToArray()));
        }

        public static IEnumerable<Module> OrderBySize(IEnumerable<Module> modules)
        {
            return modules.OrderByDescending(m => m);
        }

        public static string SummarizeTraitOrder(IEnumerable<Module> modules)
        {
            return SummarizeUnsorted(OrderByTraits(modules));
        }

        public static string Summarize(IEnumerable<Module> modules)
        {
            return SummarizeUnsorted(OrderBySize(modules));
        }

        public static string SummarizeUnsorted(IEnumerable<Module> modules)
        {
            return String.Join(", ", modules.Select(m => m.SummaryString));
        }
        
        public static string DescribeTraitOrder(IEnumerable<Module> modules)
        {
            return DescribeUnsorted(OrderByTraits(modules));
        }
        public static string Describe(IEnumerable<Module> modules)
        {
            return DescribeUnsorted(OrderBySize(modules));
        }

        public static string DescribeUnsorted(IEnumerable<Module> modules)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"{modules.Count()} modules: {Module.SummarizeUnsorted(modules)}");
            sb.AppendLine();

            int mi = 0;
            foreach (var module in modules)
            {
                sb.AppendLine($"#{++mi}: {module.SummaryString}");
                sb.AppendLine(string.Join(", ", module.Motivators));
                sb.AppendLine(string.Join(", ", module.Motivatees));
                sb.AppendLine();
            }

            return sb.ToString();
        }
        
        public static string DescribeUnsortedCompact(IEnumerable<Module> modules)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"{modules.Count()} modules: {Module.SummarizeUnsorted(modules)}");

            int mi = 0;
            foreach (var module in modules)
            {
                sb.Append($"#{++mi}: ");
                sb.Append(string.Join(", ", module.Motivators));
                sb.Append("; ");
                sb.Append(string.Join(", ", module.Motivatees));
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }

    public class DtmInfo
    {
        public Linear.Matrix<double> Matrix { get; }
        public IReadOnlyCollection<Module> Modules { get; }

        public DtmInfo(Linear.Matrix<double> matrix, IReadOnlyCollection<Module> modules)
        {
            Matrix = matrix;
            Modules = modules;
        }

        public string Summary => Module.Summarize(Modules);
        public string Description => Module.Describe(Modules);
    }

    public class DtmGraphNode : IComparable<DtmGraphNode>
    {
        public ExpansionFunction<DtmGraphNode> Forward = dgn => dgn._motivatees.Select(e => new Step<DtmGraphNode>(e, 1));
        public ExpansionFunction<DtmGraphNode> Backward = dgn => dgn._motivators.Select(e => new Step<DtmGraphNode>(e, 1));

        public int Index { get; }

        private List<DtmGraphNode> _motivatees = new List<DtmGraphNode>();
        public IReadOnlyList<DtmGraphNode> Motivatees => _motivatees;

        private List<DtmGraphNode> _motivators = new List<DtmGraphNode>();
        public IReadOnlyList<DtmGraphNode> Motivators => _motivators;

        public DtmGraphNode(int index)
        {
            Index = index;
        }

        public void Motivates(DtmGraphNode other)
        {
            _motivatees.Add(other);
            other._motivators.Add(this);
        }

        public IEnumerable<DtmGraphNode> EnumerateAllMotivators()
        {
            return AStar<DtmGraphNode>.Expand(Motivators, Backward, _ => 0f).Select(p => p.GraphNode);
        }

        public IEnumerable<DtmGraphNode> EnumerateAllMotivatees()
        {
            return AStar<DtmGraphNode>.Expand(_motivatees, Forward, _ => 0f).Select(p => p.GraphNode);
        }

        public int CompareTo(DtmGraphNode other)
        {
            return Index.CompareTo(other.Index);
        }
    }

    public class DtmGraph
    {
        public int Size => Traits.Count;
        public IReadOnlyList<DtmGraphNode> Traits { get; }

        public DtmGraph(Linear.Matrix<double> dtm, double absThreshold)
        {
            var traits  = new List<DtmGraphNode>();
            Traits = traits;

            int s = Math.Max(dtm.ColumnCount, dtm.RowCount);
            for (int i = 0; i < s; i++)
            {
                traits.Add(new DtmGraphNode(i));
            }

            for (int c = 0; c < dtm.ColumnCount; c++)
            {
                for (int r = 0; r < dtm.RowCount; r++)
                {
                    if (Math.Abs(dtm[r, c]) >= absThreshold)
                    {
                        traits[c].Motivates(traits[r]);
                    }
                }
            }
        }
    }

    public class DtmClassification
    {
        public static IReadOnlyCollection<Motivator> ThresholdMotivators(Linear.Matrix<double> dtm, double absThreshold)
        {
            List<Motivator> motivators = new List<Motivator>();

            var motivatees = new List<int>();

            for (int c = 0; c < dtm.ColumnCount; c++)
            {
                motivatees.Clear();

                for (int r = 0; r < dtm.RowCount; r++)
                {
                    if (Math.Abs(dtm[r, c]) >= absThreshold)
                        motivatees.Add(r);
                }

                if (motivatees.Count > 0)
                    motivators.Add(new Motivator(c, motivatees));
            }

            return motivators;
        }

        public static IReadOnlyCollection<IReadOnlyCollection<Motivator>> ClusterMotivators(IEnumerable<Motivator> motivators)
        {
            Dictionary<int, Motivator> table = motivators.ToDictionary(m => m.Index);

            DataStructures.DisjointSets<Motivator> djs = new DataStructures.DisjointSets<Motivator>();

            foreach (var motivator in motivators)
            {
                djs.Add(motivator);
            }

            foreach (var motivator in motivators)
            {
                foreach (var motivatee in motivator.Motivatees)
                {
                    if (table.TryGetValue(motivatee, out var other))
                        djs.Join(motivator, other);
                }
            }

            return djs.EnumerateSets().Select(m => m.ToArray()).ToArray();
        }

        public static IReadOnlyCollection<Module> DiscerneTrueModules(Linear.Matrix<double> dtm, double absThreshold)
        {
            DtmGraph dg = new DtmGraph(dtm, absThreshold);
            return DiscerneTrueModules(dg);
        }
        
        /// <summary>
        /// A 'true module' is a collection of traits driven by the same set of 'true motivators'
        /// A 'true motivator' is a trait which has a feedback loop to itself
        /// </summary>
        /// <param name="dg"></param>
        /// <returns></returns>
        public static IReadOnlyCollection<Module> DiscerneTrueModules(DtmGraph dg)
        {
            // find true motivators
            HashSet<DtmGraphNode> trueMotivators = new HashSet<DtmGraphNode>();

            foreach (DtmGraphNode trait in dg.Traits)
            {
                if (trait.EnumerateAllMotivators().Contains(trait))
                    trueMotivators.Add(trait);
            }

            // group by true motivators
            Dictionary<ComparableList<DtmGraphNode>, List<DtmGraphNode>> groups = new Dictionary<ComparableList<DtmGraphNode>, List<DtmGraphNode>>();
            List<DtmGraphNode> lonelys = new List<DtmGraphNode>();

            foreach (DtmGraphNode trait in dg.Traits)
            {
                var motivators = trait.EnumerateAllMotivators().Where(pm => trueMotivators.Contains(pm)).ToArray();

                if (motivators.Length == 0)
                {
                    lonelys.Add(trait);
                }
                else
                {
                    var cm = new ComparableList<DtmGraphNode>(motivators);

                    if (groups.TryGetValue(cm, out var list))
                    {
                        list.Add(trait);
                    }
                    else
                    {
                        list = new List<DtmGraphNode> { trait };
                        groups.Add(cm, list);
                    }
                }
            }

            // now we start accumulating
            List<Module> modules = new List<Module>();

            // accumulate lonelys
            int[] empty = new int[0];
            foreach (var lonely in lonelys)
            {
                modules.Add(new Module(empty, new[] { lonely.Index }));
            }

            // assemble and accumulate others
            foreach (var grp in groups)
            {
                var motivators = grp.Key;
                var motivatees = grp.Value;

                List<DtmGraphNode> roots = new List<DtmGraphNode>();

                foreach (var motivator in motivators)
                {
                    var allMotivators = new HashSet<DtmGraphNode>(motivator.EnumerateAllMotivators());
                    var allMotivatees = new HashSet<DtmGraphNode>(motivator.EnumerateAllMotivatees());

                    if (allMotivators.IsSubsetOf(allMotivatees))
                        roots.Add(motivator);
                }

                modules.Add(new Module(roots.Select(r => r.Index), motivatees.Select(m => m.Index)));
            }

            return modules.OrderByDescending(m => m).ToArray();
        }

        // this one is iffy
        public static IReadOnlyCollection<Module> DiscerneModulesOld(IEnumerable<Motivator> motivators)
        {
            var clusteredModules = ClusterMotivators(motivators);

            List<Module> modules = new List<Module>();

            foreach (var cm in clusteredModules)
            {
                // build graph
                Dictionary<int, GraphNode<Motivator>> forwardNodes = cm.Select(m => new GraphNode<Motivator>(m)).ToDictionary(n => n.Payload.Index);
                Dictionary<int, GraphNode<Motivator>> backwardNodes = cm.Select(m => new GraphNode<Motivator>(m)).ToDictionary(n => n.Payload.Index);

                foreach (var motivator in cm)
                {
                    var fn = forwardNodes[motivator.Index];
                    var bn = backwardNodes[motivator.Index];

                    foreach (var motivatee in motivator.Motivatees)
                    {
                        if (forwardNodes.TryGetValue(motivatee, out var of))
                            fn.JoinTo(of, 1);
                        if (backwardNodes.TryGetValue(motivatee, out var ob))
                            ob.JoinTo(bn, 1);
                    }
                }

                // find root nodes
                List<Motivator> roots = new List<Motivator>();

                foreach (var motivator in cm)
                {
                    var fn = forwardNodes[motivator.Index];
                    var bn = backwardNodes[motivator.Index];

                    var motivates = new HashSet<Motivator>(fn.Expand().Select(n => n.GraphNode.Payload));
                    var motivatedBy = new HashSet<Motivator>(bn.Expand().Select(n => n.GraphNode.Payload));

                    if (motivatedBy.IsSubsetOf(motivates))
                        roots.Add(motivator);
                }

                // assemble module
                var motivatees = motivators.SelectMany(m => m.Motivatees).Distinct();
                Module module = new Module(roots.Select(r => r.Index), motivatees);

                modules.Add(module);
            }

            return modules;
        }

        public static string Type1Classify(Linear.Matrix<double> dtm, double thresholdFactor = 10.0)
        {
            if (dtm.Enumerate().Max(e => Math.Abs(e)) < 0.1)
                return "type1nothing";

            double threshold = ComputeAutoThreshold(dtm, thresholdFactor);
            var dtmInfo = new DtmInfo(dtm, DtmClassification.DiscerneTrueModules(dtm, threshold));

            return Type1Classify(dtmInfo, threshold);
        }

        public static string Type1Classify(DtmInfo dtmInfo, double threshold)
        {
            var dtm = dtmInfo.Matrix;

            if (dtmInfo.Modules.Count == 1)
            {
                var sole = dtmInfo.Modules.First();

                // need to check it's a 1/4 or a 2/4, and whether it's +ve or -ve
                if (sole.Motivators.Count > 2)
                    return "type1dense";

                int pc = dtm.Enumerate().Count(e => e > threshold);
                int nc = dtm.Enumerate().Count(e => e < -threshold);

                if (pc > nc * 3) // p4 distinction: over 3/4 of correlations must be positive
                {
                    return "type1p4columnpositive";
                }
                else
                {
                    return "type1p4columnnegative";
                }
            }
            else if (dtmInfo.Modules.Count > 1)
            {
                // can't compare summaries... they are good for me to look at as a screening, but they are not reliable enough

                if (dtmInfo.Modules.All(m => m.Motivators.Count == 1) &&
                    dtmInfo.Modules.All(m => m.Motivatees.Count >= 2) &&
                    dtmInfo.Modules.All(m => dtmInfo.Modules.All(o => m == o || !m.Motivators.Intersect(o.Motivators).Any())) &&
                    dtmInfo.Modules.All(m => dtmInfo.Modules.All(o => m == o || !m.Motivatees.Intersect(o.Motivatees).Any()))
                    )
                {
                    // all distinct, and have one leader leading more than one, and more than one of them, so it must be hierarchy
                    return "type1husky" + dtmInfo.Modules.Count;
                }
            }

            return "type1notsure";
        }

        public static double ComputeAutoThreshold(Linear.Matrix<double> dtm, double thresholdFactor)
        {
            return dtm.Enumerate().Max(e => Math.Abs(e)) / thresholdFactor;
        }
    }

    public class DtmTimeClassification
    {
        public DtmTimeClassification(long time, DtmInfo dtmInfo)
        {
            Time = time;
            DtmInfo = dtmInfo;
        }

        public long Time { get; }
        public DtmInfo DtmInfo { get; }
        
        /// <summary>
        /// Notes: returns the same matrix each time (reuse)
        /// </summary>
        /// <param name="trajectories"></param>
        /// <param name="samplePeriod"></param>
        /// <param name="startTime"></param>
        /// <param name="thresholdFactor"></param>
        /// <param name="observationDelay"></param>
        /// <returns></returns>
        public static IEnumerable<DtmTimeClassification> ClassifyTrajectories(double[][] trajectories, int samplePeriod, long startTime, double thresholdFactor = 10.0, int observationDelay = 5)
        {
            long time = startTime;

            int N = (int)Math.Round(Math.Sqrt(trajectories.Length));

            long lastTime = -1;
            int hcount = 0;
            DtmInfo lastDtmAnalysis = null;
            foreach (var dtm in Analysis.EnumerateMatricies(trajectories, N, N, 0, trajectories[0].Length, true))
            {
                // classify
                var dtmInfo = new DtmInfo(dtm, DtmClassification.DiscerneTrueModules(dtm, DtmClassification.ComputeAutoThreshold(dtm, thresholdFactor)));
                
                if (lastDtmAnalysis == null || lastDtmAnalysis.Summary != dtmInfo.Summary)
                {
                    // if we are different, then note it
                    lastTime = time;
                    lastDtmAnalysis = dtmInfo;
                    hcount = 0;
                }
                else if (lastDtmAnalysis.Summary == dtmInfo.Summary)
                {
                    // if we are the same, then note it
                    hcount++;

                    if (hcount == observationDelay)
                    {
                        yield return new DtmTimeClassification(lastTime, lastDtmAnalysis);
                    }
                }
                
                time += samplePeriod;
            }
        }
    }
}
