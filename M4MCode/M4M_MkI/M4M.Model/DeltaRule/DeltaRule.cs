﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M.DeltaRule
{
    /// <summary>
    /// DeltaRule Spinner from 17G01.
    /// Hill-climbs a population of 1, before performing a 'Delta-Rule' update to the TransMat.
    /// Doesn't mess with the rules: you probably want to provide a NullTransMatMutator.
    /// </summary>
    [State.StateClass]
    public class DeltaRuleSpinner : IPopulationSpinner<DenseIndividual>
    {
        [Obsolete]
        protected DeltaRuleSpinner()
        {
        }

        public DeltaRuleSpinner(double fitnessWeightingPower)
        {
            FitnessWeightingPower = fitnessWeightingPower;
        }

        public string Name => "DeltaRuleSpinner";

        public string Description => $"{Name} (FitnessWeightingPower = {FitnessWeightingPower})";

        /// <summary>
        /// The power to which to raise the fitness for the fitness factor on update magnitude.
        /// Fitness should be non-negative for non-zero power.
        /// </summary>
        [State.SimpleStateProperty("FitnessWeightingPower")]
        public double FitnessWeightingPower { get; private set; } = 0.0;

        public IReadOnlyList<IndividualJudgement<DenseIndividual>> SpinPopulation(Population<DenseIndividual> population, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<DenseIndividual> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<DenseIndividual>>> judgementFeedback, int eliteCount, bool hillclimberMode)
        {
            if (population.Count != 1)
                throw new ArgumentException("Population must have exactly 1 individual to use a DeltaRuleSpinner");

            // spin as normal
            population.SpinPopulation(context, rrules, drules, jrules, target, selectorPreparer, generations, judgementFeedback, eliteCount, hillclimberMode);

            // now extrct and perform DeltaRule update
            var individual = population.ExtractAll().Single();

            DeltaRuleStepInplace(individual, context, rrules, drules, jrules, target);

            var cloned = individual.Clone(context);
            var j = cloned.Judge(jrules, target);

            population.Introduce(individual);

            return new[] { new IndividualJudgement<DenseIndividual>(cloned, j) };
        }

        public void DeltaRuleStepInplace(DenseIndividual individual, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target)
        {
            double q = rrules.DevelopmentalTransformationMatrixMutationSize;
            double σ = 0.01 * q;

            IEnumerable<MatrixEntryAddress> entries = individual.Genome.TransMatIndexOpenEntries ?? MatrixEntryAddress.CompleteLazy(individual.Genome.Size, individual.Genome.Size);

            var devTransMat = individual.Genome.TransMat;
            var clonedTransMat = context.Clone(devTransMat);
            var rand = context.Rand;

            double fitnessFactor;

            target.NextGeneration(rand, jrules);

            if (FitnessWeightingPower == 0.0)
            {
                // don't measure
                fitnessFactor = 1.0;
            }
            else
            {
                individual.DevelopInplace(context, drules);
                double w = individual.Judge(jrules, target).CombinedFitness;
                fitnessFactor = Math.Pow(w, FitnessWeightingPower);
            }

            foreach (var entry in entries)
            {
                double old = devTransMat[entry.Row, entry.Col];

                // +
                double dPos = MathNet.Numerics.Distributions.Normal.Sample(rand, +q, σ);
                devTransMat[entry.Row, entry.Col] = rrules.DevelopmentalTransformationMatrixClamping.Clamp(old + dPos);
                individual.DevelopInplace(context, drules);
                double wPos = individual.Judge(jrules, target).CombinedFitness;

                // -
                double dNeg = MathNet.Numerics.Distributions.Normal.Sample(rand, -q, σ);
                devTransMat[entry.Row, entry.Col] = rrules.DevelopmentalTransformationMatrixClamping.Clamp(old + dNeg);
                individual.DevelopInplace(context, drules);
                double wNeg = individual.Judge(jrules, target).CombinedFitness;

                // cache result
                if (wPos > wNeg)
                {
                    clonedTransMat[entry.Row, entry.Col] = rrules.DevelopmentalTransformationMatrixClamping.Clamp(old + dPos * fitnessFactor);
                }
                else
                {
                    clonedTransMat[entry.Row, entry.Col] = rrules.DevelopmentalTransformationMatrixClamping.Clamp(old + dNeg * fitnessFactor);
                }

                // reset original
                devTransMat[entry.Row, entry.Col] = old;
            }

            context.Release(clonedTransMat);
            individual.Genome.CopyOverTransMat(clonedTransMat);
        }
    }
}
