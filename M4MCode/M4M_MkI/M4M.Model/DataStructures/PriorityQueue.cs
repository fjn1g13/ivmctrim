﻿using System;
using System.Collections.Generic;

namespace M4M.DataStructures
{
    /// <summary>
    /// A struct, representing an element and its priority
    /// </summary>
    /// <typeparam name="TElement">The type of element</typeparam>
    /// <typeparam name="TPriority">The type of priority</typeparam>
    public struct PriorityPair<TElement, TPriority>
    {
        /// <summary>
        /// Creates an element/priority pair
        /// </summary>
        /// <param name="element">The element</param>
        /// <param name="priority">The priority</param>
        internal PriorityPair(TElement element, TPriority priority) : this()
        {
            Element = element;
            Priority = priority;
        }

        /// <summary>
        /// The element
        /// </summary>
        public TElement Element { get; }

        /// <summary>
        /// The priority
        /// </summary>
        public TPriority Priority { get; }
    }

    /// <summary>
    /// Represents an ordered queue of elements, allowing ordered removal of elements according to a comparable priority
    /// Elements with the same priority are sorted such that elements added earlier are treated as having an ordinally lower priority (FIFO)
    /// (Updating optionally behaves as removing and re-adding)
    /// </summary>
    /// <typeparam name="TElement">The type of element</typeparam>
    /// <typeparam name="TPriority">The type of priority (must implement IComparable(<typeparamref name="TPriority"/>))</typeparam>
    public class PriorityQueue<TElement, TPriority> where TPriority : IComparable<TPriority>
    {
        private class NodeComparer : IComparer<Node>
        {
            public readonly static NodeComparer Instance = new NodeComparer();

            private NodeComparer()
            {
                // nix
            }

            int IComparer<Node>.Compare(Node x, Node y)
            {
                return x.CompareTo(y);
            }
        }

        private class Node : IComparable<Node>
        {
            public Node(int ElementId, TElement element, TPriority priority)
            {
                this.ElementId = ElementId;
                Element = element;
                Priority = priority;
            }

            public int ElementId { get; }
            public TElement Element { get; }
            public TPriority Priority { get; }

            public int CompareTo(Node other)
            {
                // compare by priority
                int pc = Priority.CompareTo(other.Priority);
                if (pc != 0)
                    return pc;

                // if the priorities are the same, compare by id
                int eic = ElementId.CompareTo(other.ElementId);
                return eic;
            }
        }

        /// <summary>
        /// Initialises an empty PriorityQueue
        /// </summary>
        public PriorityQueue()
        {
            NodeQueue = new SortedSet<Node>(NodeComparer.Instance);
            NodeTable = new Dictionary<TElement, Node>();
        }

        private int _nextId = 0;
        private int NextId => _nextId++;

        private SortedSet<Node> NodeQueue { get; }
        private Dictionary<TElement, Node> NodeTable { get; }

        private void InternalAdd(TElement element, TPriority priority)
        {
            Node node = new Node(NextId, element, priority);
            NodeQueue.Add(node);
            NodeTable.Add(element, node);
        }

        private void InternalUpdate(Node oldNode, TPriority newPriority, bool takeNewId)
        {
            NodeQueue.Remove(oldNode);
            Node newNode = new Node(takeNewId ? NextId : oldNode.ElementId, oldNode.Element, newPriority);
            NodeQueue.Add(newNode);
            NodeTable[oldNode.Element] = newNode;
        }

        private void InternalRemove(Node node)
        {
            NodeQueue.Remove(node);
            NodeTable.Remove(node.Element);
        }

        /// <summary>
        /// Gets the number of elements in the queue
        /// </summary>
        public int Count => NodeQueue.Count;

        /// <summary>
        /// Gets a boolean value indicating whether the queue is empty
        /// </summary>
        public bool Empty => Count == 0;

        /// <summary>
        /// Gets the element with the lowest ordinal priority from the queue
        /// Throws if the queue is empty
        /// </summary>
        public PriorityPair<TElement, TPriority> Min
        {
            get
            {
                if (NodeQueue.Count == 0)
                {
                    throw new InvalidOperationException("Cannot remove the minimum from an empty queue");
                }

                Node minNode = NodeQueue.Min;
                return new PriorityPair<TElement, TPriority>(minNode.Element, minNode.Priority);
            }
        }

        /// <summary>
        /// Removes and returns the element with the lowest ordinal priority from the queue
        /// Throws if the queue is empty
        /// </summary>
        /// <returns>The lowest ordinal priority element and its priority</returns>
        public PriorityPair<TElement, TPriority> RemoveMin()
        {
            if (NodeQueue.Count == 0)
            {
                throw new InvalidOperationException("Cannot remove the minimum from an empty queue");
            }

            Node minNode = NodeQueue.Min;
            InternalRemove(minNode);
            return new PriorityPair<TElement, TPriority>(minNode.Element, minNode.Priority);
        }

        /// <summary>
        /// Attempts to remove and return the element with the lowest ordinal priority from the queue
        /// Returns true if the queue was not empty, and minElement has been set, else false
        /// </summary>
        /// <param name="minElement">The element removed from the queue, or a default value if the queue was empty</param>
        /// <returns>The lowest ordinal priority element and its priority</returns>
        public bool TryRemoveMin(out PriorityPair<TElement, TPriority> minElement)
        {
            if (NodeQueue.Count == 0)
            {
                minElement = default(PriorityPair<TElement, TPriority>);
                return false;
            }

            Node minNode = NodeQueue.Min;
            InternalRemove(minNode);
            minElement = new PriorityPair<TElement, TPriority>(minNode.Element, minNode.Priority);
            return true;
        }

        /// <summary>
        /// Looks up the priority for a given element
        /// Throws if the element was not in the queue
        /// </summary>
        /// <param name="element">The element whose priority should be looked up</param>
        /// <returns>The priority of the given element</returns>
        public TPriority GetPriority(TElement element)
        {
            if (NodeTable.TryGetValue(element, out Node found))
            {
                return found.Priority;
            }
            else
            {
                throw new ArgumentException("Element was not in the queue", nameof(found));
            }
        }

        /// <summary>
        /// Attempts to lookup the priority for a given element
        /// </summary>
        /// <param name="element">The element whose priority should be looked up</param>
        /// <param name="priority">The priority of the element if found (out), otherwise a default value</param>
        /// <returns>True if the element was present and the priority has been returned</returns>
        public bool TryGetPriority(TElement element, out TPriority priority)
        {
            if (NodeTable.TryGetValue(element, out Node found))
            {
                priority = found.Priority;
                return true;
            }
            else
            {
                priority = default(TPriority);
                return false;
            }
        }

        /// <summary>
        /// Update the priority for the given element
        /// Throws if the element was not present
        /// </summary>
        /// <param name="element">The element whose priority should be updated</param>
        /// <param name="newPriority">The new priority of the element</param>
        /// <param name="requeue">Whether or not to requeue the element</param>
        public void Update(TElement element, TPriority newPriority, bool requeue = true)
        {
            if (!NodeTable.TryGetValue(element, out Node oldNode))
            {
                throw new ArgumentException("Element not already present in Queue");
            }

            InternalUpdate(oldNode, newPriority, requeue);
        }

        /// <summary>
        /// Attempts to update the priority for the given element
        /// Returns true if the element was present and has been updated, returns false if the element was not present (does not add the element)
        /// </summary>
        /// <param name="element">The element whose priority should be updated</param>
        /// <param name="newPriority">The new priority of the element</param>
        /// <param name="requeue">Whether or not to requeue the element</param>
        /// <returns>True if the element's priority was updated, otherwise false</returns>
        public bool TryUpdate(TElement element, TPriority newPriority, bool requeue = true)
        {
            if (!NodeTable.TryGetValue(element, out Node oldNode))
            {
                return false;
            }

            InternalUpdate(oldNode, newPriority, requeue);
            return true;
        }

        /// <summary>
        /// Removes the element from the queue
        /// Throws if the element is not present
        /// </summary>
        /// <param name="element">The element to remove from the queue</param>
        public void Remove(TElement element)
        {
            if (!NodeTable.TryGetValue(element, out Node found))
            {
                throw new ArgumentException("Element not already present in Queue");
            }

            InternalRemove(found);
        }

        /// <summary>
        /// Attempts to remove the element from the queue
        /// Returns true if the element was present and was removed, false if the element was not present
        /// </summary>
        /// <param name="element"></param>
        /// <returns>True if the element was removed, otherwise false</returns>
        public bool TryRemove(TElement element)
        {
            if (!NodeTable.TryGetValue(element, out Node found))
            {
                return false;
            }

            InternalRemove(found);
            return true;
        }

        /// <summary>
        /// Attempts to enqueue the element to the queue with the given priority
        /// Returns true if added, false if the element was already present
        /// </summary>
        /// <param name="element">The element to add to the queue</param>
        /// <param name="priority">The priority the element carries</param>
        /// <param name="requeueOnUpdate">If the element is already present, whether to simulated remove/requeue</param>
        /// <returns>True if the element was added, otherwise false</returns>
        public bool EnqueueOrUpdate(TElement element, TPriority priority, bool requeueOnUpdate = true)
        {
            if (NodeTable.TryGetValue(element, out Node oldNode))
            {
                // update
                InternalUpdate(oldNode, priority, requeueOnUpdate);
                return false;
            }
            else
            {
                // add
                InternalAdd(element, priority);
                return true;
            }
        }

        /// <summary>
        /// Attempts to add the element to the queue with the given priority
        /// Returns true if added, false if the element was already present
        /// </summary>
        /// <param name="element">The element to add to the queue</param>
        /// <param name="priority">The priority the element carries</param>
        /// <returns>True if the element was added, otherwise false</returns>
        public bool TryEnqueue(TElement element, TPriority priority)
        {
            if (NodeTable.TryGetValue(element, out Node oldNode))
            {
                // no
                return false;
            }
            else
            {
                // add
                InternalAdd(element, priority);
                return true;
            }
        }

        /// <summary>
        /// Queues the element in the queue with the given priority
        /// Throws if the element is already present in the queue
        /// </summary>
        /// <param name="element">The element to add to the queue</param>
        /// <param name="priority">The priority the element carries</param>
        public void Enqueue(TElement element, TPriority priority)
        {
            if (NodeTable.TryGetValue(element, out Node found))
            {
                throw new ArgumentException("Element already present in Queue");
            }

            InternalAdd(element, priority);
        }

        /// <summary>
        /// Removes all elements from the queue
        /// </summary>
        public void Clear()
        {
            NodeTable.Clear();
            NodeQueue.Clear();
            _nextId = 0;
        }
    }
}
