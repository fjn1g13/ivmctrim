﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M.DataStructures
{
    // Graph

    public class GraphEdge<T>
    {
        public GraphEdge(GraphNode<T> end, float cost)
        {
            End = end;
            Cost = cost;
        }

        public GraphNode<T> End { get; }
        public float Cost { get; }
    }

    public class GraphNode<T>
    {
        public readonly ExpansionFunction<GraphNode<T>> DefaultExpantionFunction = gnode => gnode.Edges.Select(e => new Step<GraphNode<T>>(e.End, e.Cost));
        public readonly HeuristicFunction<GraphNode<T>> NullHeuristicFunction = gnode => 0f;

        public GraphNode(T payload)
        {
            Payload = payload;
        }

        public T Payload { get; }
        public List<GraphEdge<T>> _edges = new List<GraphEdge<T>>();
        public IReadOnlyList<GraphEdge<T>> Edges => _edges.AsReadOnly();

        public IReadOnlyList<GraphNode<T>> RouteTo(GraphNode<T> destination, HeuristicFunction<GraphNode<T>> heuristicFunction = null)
        {
            heuristicFunction = heuristicFunction ?? NullHeuristicFunction;

            PathNode<GraphNode<T>> pathNode = AStar<GraphNode<T>>.Route(this, destination, DefaultExpantionFunction, heuristicFunction);

            if (pathNode == null)
            {
                return null;
            }
            else
            {
                return pathNode.AssembleRoute().ToArray();
            }
        }

        public IEnumerable<PathNode<GraphNode<T>>> Expand(HeuristicFunction<GraphNode<T>> heuristicFunction = null)
        {
            heuristicFunction = heuristicFunction ?? NullHeuristicFunction;

            return AStar<GraphNode<T>>.Expand(new[] { this }, DefaultExpantionFunction, heuristicFunction);
        }

        /// <summary>
        /// One way join
        /// </summary>
        public void JoinTo(GraphNode<T> other, float cost)
        {
            _edges.Add(new GraphEdge<T>(other, cost));
        }

        /// <summary>
        /// Two way join
        /// </summary>
        public void JoinWith(GraphNode<T> other, float cost)
        {
            _edges.Add(new GraphEdge<T>(other, cost));
            other._edges.Add(new GraphEdge<T>(this, cost));
        }
    }

    // A*

    public struct Step<T>
    {
        public Step(T end, float cost) : this()
        {
            End = end;
            Cost = cost;
        }

        public T End { get; }
        public float Cost { get; }
    }

    public class PathNode<T> : IComparable<PathNode<T>>
    {
        /// <summary>
        /// Constructs a Start node
        /// </summary>
        public PathNode(T graphNode, float heurisitcCost)
        {
            Previous = null;
            GraphNode = graphNode;
            TrueCost = 0f;
            HeuristicCost = heurisitcCost;
        }

        /// <summary>
        /// Constructs a non-Start node
        /// </summary>
        public PathNode(PathNode<T> previous, T graphNode, float trueCost, float heuristicCost)
        {
            Previous = previous;
            GraphNode = graphNode;
            TrueCost = trueCost;
            HeuristicCost = heuristicCost;
        }

        public PathNode<T> Previous { get; }
        public T GraphNode { get; }
        public float TrueCost { get; }
        public float HeuristicCost { get; }

        int IComparable<PathNode<T>>.CompareTo(PathNode<T> other)
        {
            return HeuristicCost.CompareTo(other.HeuristicCost);
        }

        public Stack<T> AssembleRoute()
        {
            return AssemblePathInternal(this);
        }

        private static Stack<T> AssemblePathInternal(PathNode<T> top)
        {
            var stack = new Stack<T>();

            var cur = top;

            while (cur.Previous != null)
            {
                stack.Push(cur.GraphNode);
                cur = cur.Previous;
            }

            return stack;
        }

        public PathNode<T> FindStartNode()
        {
            var cur = this;

            while (cur.Previous != null)
            {
                cur = cur.Previous;
            }

            return cur;
        }
    }

    public delegate float HeuristicFunction<T>(T graphNode);
    public delegate IEnumerable<Step<T>> ExpansionFunction<T>(T graphNode);

    public class AStar<T>
    {
        /// <summary>
        /// Returns the PathNode that defines the route from the start to the destination
        /// Returns null if there is no route
        /// </summary>
        public static PathNode<T> Route(T start, T destination, ExpansionFunction<T> expansionFunction, HeuristicFunction<T> heuristicFunction)
        {
            var route = Expand(new[] { start }, expansionFunction, heuristicFunction).FirstOrDefault(pn => pn.GraphNode.Equals(destination));

            return route;
        }

        public static IEnumerable<PathNode<T>> Expand(IEnumerable<T> starts, ExpansionFunction<T> expansionFunction, HeuristicFunction<T> heuristicFunction)
        {
            var due = new PriorityQueue<T, PathNode<T>>();
            foreach (var start in starts)
            due.Enqueue(start, new PathNode<T>(start, heuristicFunction(start)));

            var visited = new HashSet<T>();
            
            while (due.TryRemoveMin(out var cur))
            {
                var curNode = cur.Element;
                var curPath = cur.Priority; // bit confusing...
                yield return curPath;

                visited.Add(curNode);

                foreach (var step in expansionFunction(curNode))
                {
                    if (visited.Contains(step.End))
                    {
                        continue; // already expanded this node: we can't have a better path
                    }

                    // compute cost, assemble PathNode
                    var cost = curPath.TrueCost + step.Cost;
                    var hcost = cost + heuristicFunction(step.End);
                    var provisionalPath = new PathNode<T>(curPath, step.End, cost, hcost);

                    if (due.TryGetPriority(step.End, out var existingPath))
                    {
                        // only use our new path if it is better than the existing path
                        if (provisionalPath.HeuristicCost < existingPath.HeuristicCost)
                        {
                            due.Update(step.End, provisionalPath);
                        }
                    }
                    else
                    {
                        due.Enqueue(step.End, provisionalPath);
                    }
                }
            }
        }
    }
}