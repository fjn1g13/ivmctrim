﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace M4M.DataStructures
{
    class ComparableList<T> : IEnumerable<T>, IComparable<ComparableList<T>> where T : IComparable<T>
    {
        public IReadOnlyList<T> List { get; }
        private int? _hashCode = null;

        public ComparableList(IReadOnlyList<T> list)
        {
            List = list.OrderBy(x => x).ToArray();
        }

        public int CompareTo(ComparableList<T> other)
        {
            return Compare(this.List, other.List);
        }

        public static int Compare(IReadOnlyList<T> l, IReadOnlyList<T> r)
        {
            if (l == r)
                return 0;

            int lengthCmp = l.Count.CompareTo(r.Count);

            if (lengthCmp != 0)
                return lengthCmp;

            var zip = l.Zip(r, (a, b) => a.CompareTo(b));

            foreach (var z in zip)
            {
                if (z < 0)
                    return -1;
                if (z > 0)
                    return 1;
            }

            return 0;
        }

        public override bool Equals(object obj)
        {
            if (obj is ComparableList<T> other)
            {
                return this.CompareTo(other) == 0;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            if (_hashCode.HasValue)
                return _hashCode.Value;

            unchecked
            {
                _hashCode = List.Aggregate(0, (a, b) => a * 7 + b.GetHashCode());
            }

            return _hashCode.Value;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }
    }
    
    class LengthIndependentComparableList<T> : IEnumerable<T>, IComparable<LengthIndependentComparableList<T>> where T : IComparable<T>
    {
        public IReadOnlyList<T> List { get; }
        private int? _hashCode = null;

        public LengthIndependentComparableList(IReadOnlyList<T> list)
        {
            List = list.OrderBy(x => x).ToArray();
        }

        public int CompareTo(LengthIndependentComparableList<T> other)
        {
            return Compare(this.List, other.List);
        }

        public static int Compare(IReadOnlyList<T> l, IReadOnlyList<T> r)
        {
            if (l == r)
                return 0;

            for (int i = 0; ; i++)
            {
                bool endl = i == l.Count;
                bool endr = i == r.Count;

                if (endl && endr)
                    return 0;
                if (endl)
                    return 1;
                if (endr)
                    return -1;

                int cmp = l[i].CompareTo(r[i]);
                if (cmp != 0)
                    return cmp;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is LengthIndependentComparableList<T> other)
            {
                return this.CompareTo(other) == 0;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            if (_hashCode.HasValue)
                return _hashCode.Value;

            unchecked
            {
                _hashCode = List.Aggregate(0, (a, b) => a * 7 + b.GetHashCode());
            }

            return _hashCode.Value;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }
    }
}
