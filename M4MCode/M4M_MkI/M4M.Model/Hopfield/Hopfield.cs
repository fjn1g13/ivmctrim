﻿using M4M.Epistatics;
using M4M.State;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M.Hopfield
{
    // State Provider registered in GraphSerialisation.cs
    public enum BiasType : int
    {
        /// <summary>
        /// No bias is to be added.
        /// </summary>
        None,

        /// <summary>
        /// A constant bias is added.
        /// </summary>
        Constant,

        /// <summary>
        /// A bias proportional to the state is added.
        /// </summary>
        Linear,

        /// <summary>
        /// A bias proportional to the square of the state is added.
        /// </summary>
        Quadratic,
    }

    // State Provider registered in GraphSerialisation.cs
    public enum InteractionType : int
    {
        /// <summary>
        /// No interaction is added.
        /// </summary>
        None,

        /// <summary>
        /// An interaction proportional to the interaction product is added.
        /// </summary>
        Linear,

        /// <summary>
        /// An interaction proportional to the product of interaction product and state is added.
        /// </summary>
        Quadratic,
    }

    // State Provider registered in GraphSerialisation.cs
    public enum InteractionMatrixSource : int
    {
        /// <summary>
        /// The interaction matrix is sourced from the genome's DTM.
        /// </summary>
        Genome,

        /// <summary>
        /// The interaction matrix is sourced from the correlation matrix target's constraint matrix.
        /// </summary>
        Target,

        /// <summary>
        /// The interaction matrix is the sum of the genome's DTM and the correlation matrix target's constraint matrix.
        /// </summary>
        GenomeAndTarget,
    }

    // State Provider registered in GraphSerialisation.cs
    public enum BiasVectorSource : int
    {
        /// <summary>
        /// The interaction matrix is sourced from the genome's bias vector.
        /// </summary>
        Genome,

        /// <summary>
        /// The interaction matrix is sourced from the correlation matrix target's forcing vector.
        /// </summary>
        Target,

        /// <summary>
        /// The interaction matrix is the sum of the genome's bias vector and the correlation matrix target's forcing vector.
        /// </summary>
        GenomeAndTarget,
    }

    /// <summary>
    /// Hopfield Spinner.
    /// Performs lots of relaxation steps, rather than mutating and developing
    /// It's a total cludge, but who cares.
    /// </summary>
    [State.StateClass]
    public class HopfieldSpinner : IPopulationSpinner<DenseIndividual>
    {
        [Obsolete]
        protected HopfieldSpinner()
        { }

        public HopfieldSpinner(ISquash squash, double updateRate, double decayRate, BiasType biasType, InteractionType interactionType, InteractionMatrixSource interactionMatrixSource, BiasVectorSource biasVectorSource)
        {
            Squash = squash;
            UpdateRate = updateRate;
            DecayRate = decayRate;
            BiasType = biasType;
            InteractionType = interactionType;
            InteractionMatrixSource = interactionMatrixSource;
            BiasVectorSource = biasVectorSource;
        }

        public string Name => "HopfieldSpinner";

        public string Description => $"{Name} (Squash={Squash.Name}, UpdateRate={UpdateRate}, DecayRate={DecayRate}, BiasType={BiasType}, InteractionType={InteractionType}, InteractionMatrixSource={InteractionMatrixSource}, BiasVectorSource={BiasVectorSource})";

        [SimpleStateProperty("Squash")]
        public ISquash Squash { get; private set; }

        [SimpleStateProperty("UpdateRate")]
        public double UpdateRate { get; private set; } = 0.1;

        [SimpleStateProperty("DecayRate")]
        public double DecayRate { get; private set; } = 0.1;

        [SimpleStateProperty("BiasType")]
        public BiasType BiasType { get; private set; } = BiasType.Constant;

        [SimpleStateProperty("InteractionType")]
        public InteractionType InteractionType { get; private set; } = InteractionType.Linear;

        [SimpleStateProperty("InteractionMatrixSource")]
        public InteractionMatrixSource InteractionMatrixSource { get; private set; } = InteractionMatrixSource.Genome;

        [SimpleStateProperty("BiasVectorSource")]
        public BiasVectorSource BiasVectorSource { get; private set; } = BiasVectorSource.Genome;

        public IReadOnlyList<IndividualJudgement<DenseIndividual>> SpinPopulation(Population<DenseIndividual> population, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<DenseIndividual> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<DenseIndividual>>> judgementFeedback, int eliteCount, bool hillclimberMode)
        {
            MathNet.Numerics.LinearAlgebra.Matrix<double> mat = null;
            MathNet.Numerics.LinearAlgebra.Vector<double> vec = null;
            MathNet.Numerics.LinearAlgebra.Vector<double> update = null;
            MathNet.Numerics.LinearAlgebra.Vector<double> update2 = null;
            int count = population.Count;

            var individuals = population.ExtractAll();
            var judgements = new IndividualJudgement<DenseIndividual>[individuals.Length];

            for (int generation = 0; generation < generations; generation++)
            {
                target.NextGeneration(context.Rand, jrules);
                var cmt = target as CorrelationMatrixTarget;

                if (generation == 0 && judgementFeedback != null)
                {
                    for (int i = 0; i < individuals.Length; i++)
                    {
                        var individual = individuals[i].Clone(context);
                        individual.DevelopInplace(context, drules);
                        judgements[i] = new IndividualJudgement<DenseIndividual>(individual, individual.Judge(jrules, target));
                    }

                    judgementFeedback.Invoke(0, judgements.ToArray());
                }

                for (int i = 0; i < individuals.Length; i++)
                {
                    Step(cmt, individuals[i].Genome, ref mat, ref vec, ref update, ref update2, Squash.Delegate, UpdateRate, DecayRate, BiasType, InteractionType, InteractionMatrixSource, BiasVectorSource);

                    if (judgementFeedback != null || generation == generations - 1)
                    {
                        individuals[i].DevelopInplace(context, drules);
                        var individual = individuals[i].Clone(context);
                        judgements[i] = new IndividualJudgement<DenseIndividual>(individual, individual.Judge(jrules, target));
                    }
                }

                judgementFeedback?.Invoke(generation + 1, judgements.ToArray());
            }

            population.IntroduceMany(individuals);

            return judgements;
        }

        /// <summary>
        /// Steps the initial state of the given genome.
        /// </summary>
        public static void Step(CorrelationMatrixTarget cmt, DenseGenome genome, ref MathNet.Numerics.LinearAlgebra.Matrix<double> mat2, ref MathNet.Numerics.LinearAlgebra.Vector<double> vec2, ref MathNet.Numerics.LinearAlgebra.Vector<double> update, ref MathNet.Numerics.LinearAlgebra.Vector<double> update2, Func<double, double> squash, double updateRate, double decayRate, BiasType biasType, InteractionType interactionType, InteractionMatrixSource interactionMatrixSource, BiasVectorSource biasVectorSource)
        {
            if (interactionType != InteractionType.None && (update == null || update.Count != genome.Size))
            {
                update = MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(genome.Size);
            }

            var state = genome.InitialState;
            MathNet.Numerics.LinearAlgebra.Matrix<double> mat = null;
            MathNet.Numerics.LinearAlgebra.Vector<double> bias = null;

            if (interactionType != InteractionType.None)
            {
                switch (interactionMatrixSource)
                {
                    case InteractionMatrixSource.Genome:
                        mat = genome.TransMat;
                        break;
                    case InteractionMatrixSource.Target:
                        mat = cmt.CorrelationMatrix;
                        break;
                    case InteractionMatrixSource.GenomeAndTarget:
                        if (mat2 == null || mat2.RowCount != genome.Size || mat2.ColumnCount != genome.Size)
                        {
                            mat2 = MathNet.Numerics.LinearAlgebra.CreateMatrix.Dense<double>(genome.Size, genome.Size);
                        }

                        mat = mat2;
                        genome.TransMat.CopyTo(mat);
                        mat.Add(cmt.CorrelationMatrix, mat);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(interactionMatrixSource));
                }

                mat.Multiply(state, update); // this is where the CPU spends most of its time (N^2)
            }

            if (interactionType == InteractionType.Quadratic)
            {
                update.PointwiseMultiply(state, update);
            }

            if (biasType != BiasType.None)
            {
                switch (biasVectorSource)
                {
                    case BiasVectorSource.Genome:
                        bias = genome.BiasVector;
                        break;
                    case BiasVectorSource.Target:
                        bias = cmt.ForcingVector;
                        break;
                    case BiasVectorSource.GenomeAndTarget:
                        if (vec2 == null || vec2.Count != genome.Size)
                        {
                            vec2 = MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(genome.Size);
                        }

                        bias = vec2;
                        genome.InitialState.CopyTo(bias);
                        bias.Add(cmt.ForcingVector, bias);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(biasVectorSource));
                }
            }

            if (bias != null)
            {
                switch (biasType)
                {
                    case BiasType.None:
                        break;
                    case BiasType.Constant:
                        update.Add(bias, update);
                        break;
                    case BiasType.Linear:
                    case BiasType.Quadratic:
                        if (update2 == null || update2.Count != genome.Size)
                        {
                            update2 = MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(genome.Size);
                        }

                        bias.PointwiseMultiply(state, update2);
                        if (biasType == BiasType.Quadratic)
                        {
                            update2.PointwiseMultiply(state, update2);
                        }

                        update.Add(bias, update2);

                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(biasType));
                }
            }

            update.MapInplace(squash);
            update.Multiply(updateRate, update);

            state.Multiply(1.0 - decayRate, state);

            state.Add(update, state);
        }
    }
}
