﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Linear = MathNet.Numerics.LinearAlgebra; // reasonable quality API reference here: https://numerics.mathdotnet.com/api/MathNet.Numerics.LinearAlgebra/ (inline isn't good enogh)
using RandomSource = MathNet.Numerics.Random.RandomSource;
using static M4M.Misc;
using MathNet.Numerics.Random;
using M4M.State;

namespace M4M
{
    public interface ICycler
    {
        /// <summary>
        /// The Name of the Cycler
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Determines the next element
        /// </summary>
        /// <param name="rand">A random source</param>
        /// <param name="current">The current element's index</param>
        /// <param name="max">The number of elements and exclusive max index</param>
        /// <returns>The index of the next chosen element</returns>
        int Cycle(RandomSource rand, int current, int max);
    }
    
    [StateClass]
    public class LoopCycler : ICycler
    {
        public string Name => "Loop";

        public int Cycle(RandomSource rand, int current, int max)
        {
            return (current + 1) % max;
        }
    }
    
    [StateClass]
    public class RandomCycler : ICycler
    {
        public string Name => "Random";

        public int Cycle(RandomSource rand, int current, int max)
        {
            return rand.Next(max);
        }
    }
    
    [StateClass]
    public class RandomNoRepeatCycle : ICycler
    {
        public string Name => "RandomNoRepeat";

        public int Cycle(RandomSource rand, int current, int max)
        {
            int p = rand.Next(max - 1); // grab a provisional
            if (p >= current) // if the provisional c or above
                p++; // add one
            return p; // return the no-longer provisional
        }
    }

    public class Cyclers
    {
        public static readonly ICycler Loop = new LoopCycler();
        public static readonly ICycler Random = new RandomCycler();
        public static readonly ICycler RandomNoRepeat = new RandomNoRepeatCycle();

        public static ICycler GetStandardCycler(string name)
        {
            if (name.Equals("Loop", StringComparison.InvariantCultureIgnoreCase))
                return Loop;
            else if (name.Equals("Random", StringComparison.InvariantCultureIgnoreCase))
                return Random;
            else if (name.Equals("RandomNoRepeat", StringComparison.InvariantCultureIgnoreCase))
                return RandomNoRepeat;
            else
                throw new ArgumentException("Unrecognised cycler \"" + name + "\"", nameof(name));
        }
    }

    [StateClass]
    public class ExperimentConfiguration
    {
        [Obsolete]
        protected ExperimentConfiguration()
        { }

        public ExperimentConfiguration(int size, IReadOnlyList<ITarget> targets, ICycler targetCycler, int epochs, int generationsPerTargetPerEpoch, double initialStateResetProbability, Range initialStateResetRange, bool shuffleTargets, DevelopmentRules developmentRules, ReproductionRules reproductionRules, JudgementRules judgementRules)
        {
            Size = size;
            Targets = targets;
            TargetCycler = targetCycler;
            Epochs = epochs;
            GenerationsPerTargetPerEpoch = generationsPerTargetPerEpoch;
            InitialStateResetProbability = initialStateResetProbability;
            InitialStateResetRange = initialStateResetRange;
            ShuffleTargets = shuffleTargets;
            DevelopmentRules = developmentRules;
            ReproductionRules = reproductionRules;
            JudgementRules = judgementRules;
        }

        /// <summary>
        /// The size of genomes and targets
        /// </summary>
        [SimpleStateProperty("Size")]
        public int Size { get; private set; }

        /// <summary>
        /// The targets to epose the phenotypes to
        /// </summary>
        [SimpleStateProperty("Targets")]
        public IReadOnlyList<ITarget> Targets { get; private set; }

        /// <summary>
        /// Whether to shuffle the targets each epoch
        /// </summary>
        [SimpleStateProperty("ShuffleTargets")]
        public bool ShuffleTargets { get; private set; }

        /// <summary>
        /// The method by which to cycle targets
        /// (If you can't think of anything better, Cyclers.Loop is probably fine)
        /// </summary>
        [SimpleStateProperty("TargetCycler")]
        public ICycler TargetCycler { get; private set; }

        /// <summary>
        /// The number of epochs to run
        /// </summary>
        [SimpleStateProperty("Epochs")]
        public int Epochs { get; private set; }

        /// <summary>
        /// The probaility of reseting the initial state at the start of each Epoch
        /// (Is this defined for non-population experiments?)
        /// </summary>
        [SimpleStateProperty("InitialStateResetProbability")]
        public double InitialStateResetProbability { get; private set; }

        /// <summary>
        /// The range in which to set a randomised initial state
        /// </summary>
        [SimpleStateProperty("InitialStateResetRange")]
        public Range InitialStateResetRange { get; private set; }

        /// <summary>
        /// Switching interval
        /// K in the paper
        /// </summary>
        [SimpleStateProperty("GenerationsPerTargetPerEpoch")]
        public int GenerationsPerTargetPerEpoch { get; private set; }

        /// <summary>
        /// The Development Rules
        /// </summary>
        [SimpleStateProperty("DevelopmentRules")]
        public DevelopmentRules DevelopmentRules { get; private set; }

        /// <summary>
        /// The Reproduction Rules
        /// </summary>
        [SimpleStateProperty("ReproductionRules")]
        public ReproductionRules ReproductionRules { get; private set; }

        /// <summary>
        /// The Judgement Rules
        /// </summary>
        [SimpleStateProperty("JudgementRules")]
        public JudgementRules JudgementRules { get; private set; }
        
        public void WriteOut(System.IO.StreamWriter cw)
        {
            cw.WriteLine();
            cw.WriteLine($"Size (N) = {Size}");
            cw.WriteLine($"Epochs = {Epochs}");
            cw.WriteLine($"SwitchingRate (K) = {GenerationsPerTargetPerEpoch}");
            cw.WriteLine($"Target Count = {Targets.Count}");
            cw.WriteLine($"Target Cycler = {TargetCycler.Name}");
            cw.WriteLine($"Shuffle Targets = {ShuffleTargets}");
            cw.WriteLine($"Initial State Reset Probability = {InitialStateResetProbability}");
            cw.WriteLine($"Initial State Reset Range = {InitialStateResetRange?.ToString() ?? "(none)"}");

            cw.WriteLine();
            cw.WriteLine($"DRules.{nameof(DevelopmentRules.UpdateRate)} (τ1) = {DevelopmentRules.UpdateRate}");
            cw.WriteLine($"DRules.{nameof(DevelopmentRules.DecayRate)} (τ2) = {DevelopmentRules.DecayRate}");
            cw.WriteLine($"DRules.{nameof(DevelopmentRules.TimeSteps)} (T) = {DevelopmentRules.TimeSteps}");
            cw.WriteLine($"DRules.{nameof(DevelopmentRules.Squash)} (σ) = {DevelopmentRules.Squash.Name}");
            cw.WriteLine($"DRules.{nameof(DevelopmentRules.RescaleScale)} = {DevelopmentRules.RescaleScale}");
            cw.WriteLine($"DRules.{nameof(DevelopmentRules.RescaleOffset)} = {DevelopmentRules.RescaleOffset}");

            cw.WriteLine();
            cw.WriteLine($"RRules.{nameof(ReproductionRules.InitialStateMutationSize)} (M_G) = {ReproductionRules.InitialStateMutationSize}");
            cw.WriteLine($"RRules.{nameof(ReproductionRules.InitialStateMutationType)} (M_G type) = {ReproductionRules.InitialStateMutationType}");
            cw.WriteLine($"RRules.{nameof(ReproductionRules.DevelopmentalTransformationMatrixMutationSize)} (M_B) = {ReproductionRules.DevelopmentalTransformationMatrixMutationSize}");
            cw.WriteLine($"RRules.{nameof(ReproductionRules.DevelopmentalTransformationMatrixMutationType)} (M_B type) = {ReproductionRules.DevelopmentalTransformationMatrixMutationType}");
            cw.WriteLine($"RRules.{nameof(ReproductionRules.DevelopmentalTransformationMatrixRate)} (R_B) = {ReproductionRules.DevelopmentalTransformationMatrixRate}");
            cw.WriteLine($"RRules.{nameof(ReproductionRules.ExclusiveBMutation)} (BEx) = {ReproductionRules.ExclusiveBMutation}");
            cw.WriteLine($"RRules.{nameof(ReproductionRules.InitialTraitUpdates)} (C_G) = {ReproductionRules.InitialTraitUpdates}");
            cw.WriteLine($"RRules.{nameof(ReproductionRules.InitialStateClamping)} = {ReproductionRules.InitialStateClamping}");

            cw.WriteLine();
            cw.WriteLine($"JRules.{nameof(JudgementRules.NoiseFactor)} (κ) = {JudgementRules.NoiseFactor}");
            cw.WriteLine($"JRules.{nameof(JudgementRules.RegularisationFactor)} (λ) = {JudgementRules.RegularisationFactor}");
            cw.WriteLine($"JRules.{nameof(JudgementRules.RegularisationFunction)} (ϕ) = {JudgementRules.RegularisationFunction.Name}");

            cw.WriteLine();
            cw.WriteLine($"Targets");
            for (int i = 0; i < Targets.Count; i++)
            {
                cw.WriteLine($"Target{i}: {Targets[i].FullName} ({Targets[i].FriendlyName})");
                cw.WriteLine($"{Targets[i].Description}");
            }
        }
    }

    /// <summary>
    /// Signals that a new round is about to begin with a new target
    /// </summary>
    /// <param name="genome">The current genome</param>
    /// <param name="target">The new target</param>
    /// <param name="epoch">The current epoch (1-indexed)</param>
    /// <param name="generationCount">The total number of generations seen</param>
    public delegate void EndTargetDelegate(DenseGenome genome, ITarget target, int epoch, int generationCount);
    
    /// <summary>
    /// Signals that a round has just ended
    /// </summary>
    /// <param name="genome">The current genome</param>
    /// <param name="target">The current (about to be swapped out) target</param>
    /// <param name="epoch">The current epoch (1-indexed)</param>
    /// <param name="generationCount">The total number of generations seen</param>
    public delegate void StartTargetDelegate(DenseGenome genome, ITarget target, int epoch, int generationCount);

    /// <summary>
    /// Signals that an epoch has just ended
    /// </summary>
    /// <param name="genome">The current genome</param>
    /// <param name="epochCount">The number of epochs run</param>
    /// <param name="generationCount">The total number of generations seen</param>
    public delegate void EndEpochDelegate(DenseGenome genome, int epochCount, int generationCount);
    
    /// <summary>
    /// Signals that an experiment has just finished
    /// </summary>
    /// <param name="genome">The terminal genome</param>
    /// <param name="epochCount">The number of epochs run</param>
    /// <param name="generationCount">The total number of generations seen</param>
    public delegate void FinishedDelegate(DenseGenome genome, int epochCount, int generationCount);

    public class ExperimentFeedback
    {
        // what is the point of Event<T>?
        public Event<EndTargetDelegate> EndTarget => _endTarget;
        private Event<EndTargetDelegate> _endTarget = new Event<EndTargetDelegate>();

        public Event<StartTargetDelegate> StartTarget => _startTarget;
        private Event<StartTargetDelegate> _startTarget = new Event<StartTargetDelegate>();
        
        public Event<EndEpochDelegate> EndEpoch => _endEpoch;
        private Event<EndEpochDelegate> _endEpoch = new Event<EndEpochDelegate>();
        
        public Event<FinishedDelegate> Finished => _finished;
        private Event<FinishedDelegate> _finished = new Event<FinishedDelegate>();
    }
}
