﻿using System;
using System.Collections.Generic;
using System.Text;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public struct MatrixDimensions
    {
        public readonly int Rows;
        public readonly int Columns;

        public MatrixDimensions(int rows, int columns)
        {
            Rows = rows;
            Columns = columns;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MatrixDimensions))
            {
                return false;
            }

            var dimensions = (MatrixDimensions)obj;
            return Rows == dimensions.Rows &&
                   Columns == dimensions.Columns;
        }

        public override int GetHashCode()
        {
            var hashCode = 1026099028;
            hashCode = hashCode * -1521134295 + Rows.GetHashCode();
            hashCode = hashCode * -1521134295 + Columns.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(MatrixDimensions left, MatrixDimensions right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(MatrixDimensions left, MatrixDimensions right)
        {
            return !(left == right);
        }
    }

    public class MatrixDimensionsComparer : IEqualityComparer<MatrixDimensions>
    {
        public static readonly MatrixDimensionsComparer Instance = new MatrixDimensionsComparer();

        private MatrixDimensionsComparer()
        {
        }

        public bool Equals(MatrixDimensions x, MatrixDimensions y)
        {
            return x.Rows == y.Rows && x.Columns == y.Columns;
        }

        public int GetHashCode(MatrixDimensions matDims)
        {
            return matDims.GetHashCode();
        }
    }

    /// <summary>
    /// Provides means to recycle matrices of specific dimensions
    /// NOT Thread Safe
    /// </summary>
    public class MatrixPool
    {
        /// <summary>
        /// Global switch for MatrixPooling
        /// If you are not careful, matrix pooling can ruin your day: verify that your code works with matrix pooling before depending on it for anything
        /// </summary>
        public static bool EnableMatrixPools = false;
        public long CacheHits { get; private set; }
        public long CacheMisses { get; private set; }
        public long CacheCaps { get; private set; }

        public MatrixDimensions Dim { get; }
        public int MaxCount { get; }
        private readonly Stack<Linear.Matrix<double>> Stack = new Stack<Linear.Matrix<double>>();

        public MatrixPool(MatrixDimensions dim, int maxCount)
        {
            Dim = dim;
            MaxCount = maxCount;
        }

        public void Release(Linear.Matrix<double> unused)
        {
            if (!EnableMatrixPools)
                return;

            if (Stack.Count < MaxCount)
            {
                Stack.Push(unused);
            }
            else
            {
                CacheCaps++;
            }
        }

        public Linear.Matrix<double> Grab()
        {
            if (!EnableMatrixPools)
                return Linear.CreateMatrix.Dense<double>(Dim.Rows, Dim.Columns);

            if (Stack.Count > 0)
            {
                CacheHits++;
                return Stack.Pop();
            }
            else
            {
                CacheMisses++;
                return Linear.CreateMatrix.Dense<double>(Dim.Rows, Dim.Columns);
            }
        }

        public Linear.Matrix<double> Clone(Linear.Matrix<double> original)
        {
            if (!EnableMatrixPools)
                return original.Clone();

            if (Stack.Count > 0)
            {
                CacheHits++;
                var mat = Stack.Pop();
                original.CopyTo(mat);
                return mat;
            }
            else
            {
                CacheMisses++;
                return original.Clone();
            }
        }
    }

}
