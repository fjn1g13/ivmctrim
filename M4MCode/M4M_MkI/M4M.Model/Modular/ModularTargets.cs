﻿using M4M.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M.Modular
{   
    [StateClass]
    public class CoefJudger : IVectorTargetJudger
    {
        [Obsolete]
        protected CoefJudger()
        {
        }

        public CoefJudger(string name, double coef)
        {
            Name = name;
            Coef = coef;
        }

        [SimpleStateProperty("Name")]
        public string Name { get; private set; }

        [SimpleStateProperty("Coef")]
        public double Coef { get; private set; }

        public string Description => $"Coef Judger (Coef={Coef})";

        public double Judge(Linear.Vector<double> vector, Phenotype phenotype)
        {
            return vector.DotProduct(phenotype.Vector) * Coef;
        }
    }

    public class ModularTargetPackage : ITargetPackage
    {
        public static CoefJudger CoefJudgment(string name, double coef)
        {
            return new CoefJudger(name, coef);
        }

        public static readonly VectorTarget[] Targets4x1Standard = new[]
        {
            new VectorTarget("++++", "H0"),
            new VectorTarget("----", "L0"),
        };

        public static readonly VectorTarget[] Targets2x2Standard = new[]
        {
            new VectorTarget("----", "L0R0"),
            new VectorTarget("--++", "L0R1"),
            new VectorTarget("++++", "L1R1"),
            new VectorTarget("++--", "L1R0"),
        };

        public static readonly VectorTarget[] Targets4x4Standard = new[]
        {
            new VectorTarget("----++++++++----", "lhhl"),
            new VectorTarget("++++++++--------", "hhll"),
            new VectorTarget("++++----++++----", "hlhl"),
        };

        public static readonly VectorTarget[] Targets4x4Original = new[]
        {
            new VectorTarget("-+-+++--+--+----", "lhhl"),
            new VectorTarget("+-+-++---++-----", "hhll"),
            new VectorTarget("+-+---+++--+----", "hlhl"),
        };

        public static readonly VectorTarget[] Targets4x4Total = All(new[] { "-+-+", "++--", "-++-", "----" }, s => new VectorTarget(s, "bleh")).ToArray();

        public static readonly VectorTarget[] Targets4x1HighJudge = new[]
        {
            new VectorTarget("++++", "H1.0", CoefJudgment("J1.0", 1.0)),
            new VectorTarget("++++", "H1.1", CoefJudgment("J1.1", 1.1)),
        };

        public static readonly VectorTarget[] Targets3x3Standard = new[]
        {
            new VectorTarget("---++++++", "lhh"),
            new VectorTarget("++++++---", "hhl"),
            new VectorTarget("+++---+++", "hlh"),
        };

        public static readonly VectorTarget[] TargetsSingle4 = new[]
        {
            new VectorTarget("--++", "s4a"),
            new VectorTarget("--++", "s4b"),
        };

        public static readonly VectorTarget[] TargetsSingle8 = new[]
        {
            new VectorTarget("----++++", "s8a"),
            new VectorTarget("----++++", "s8b"),
        };

        public static readonly VectorTarget[] TargetsAnd9 = new[]
        {
            new VectorTarget("---------", "And9_1"),
            new VectorTarget("+++------", "And9_2"),
            new VectorTarget("---+++---", "And9_3"),
            new VectorTarget("+++++++++", "And9_4"),
        };

        public static readonly VectorTarget[] TargetsBool12 = new[]
        {
            new VectorTarget("------------", "Bool12_1"),
            new VectorTarget("+++------+++", "Bool12_2"),
            new VectorTarget("---+++---+++", "Bool12_3"),
            new VectorTarget("+++++++++---", "Bool12_4"),
        };

        public static readonly ModularTargetPackage Standard4x1 = new ModularTargetPackage("Std4x1", Targets4x1Standard, 1);
        public static readonly ModularTargetPackage Standard2x2 = new ModularTargetPackage("Std2x2", Targets2x2Standard, 2);
        public static readonly ModularTargetPackage Standard4x4 = new ModularTargetPackage("Std4x4", Targets4x4Standard, 4);
        public static readonly ModularTargetPackage Original4x4 = new ModularTargetPackage("Org4x4", Targets4x4Original, 4);
        public static readonly ModularTargetPackage Total4x4 = new ModularTargetPackage("Tot4x4", Targets4x4Total, 4);
        public static readonly ModularTargetPackage Standard3x3 = new ModularTargetPackage("Std3x3", Targets3x3Standard, 3);
        public static readonly ModularTargetPackage HighJudge4x1 = new ModularTargetPackage("HiJ4x1", Targets4x1HighJudge, 1);
        public static readonly ModularTargetPackage Single4 = new ModularTargetPackage("Single4", TargetsSingle4, 2);
        public static readonly ModularTargetPackage Single8 = new ModularTargetPackage("Single8", TargetsSingle8, 2);
        public static readonly ModularTargetPackage And9 = new ModularTargetPackage("And9", TargetsAnd9, 3);
        public static readonly ModularTargetPackage Bool12 = new ModularTargetPackage("Bool12", TargetsBool12, 4);

        public static Dictionary<string, ModularTargetPackage> TargetPackages { get; private set; }

        public ModularTargetPackage(string name, VectorTarget[] targets, int moduleCount, bool register = true)
        {
            Name = name;
            Targets = targets;
            ModuleCount = moduleCount;

            if (register)
            { // such terrible programming
                if (TargetPackages == null)
                    TargetPackages = new Dictionary<string, ModularTargetPackage>(StringComparer.InvariantCultureIgnoreCase);

                TargetPackages.Add(name, this);
            }
        }

        public string Name { get; }
        public VectorTarget[] Targets { get; }
        public int ModuleCount { get; }

        public int TargetSize => Targets[0].Size;
        public int ModuleSize => TargetSize / ModuleCount;
        
        IReadOnlyList<ITarget> ITargetPackage.Targets => Targets;

        public static IEnumerable<VectorTarget> All(string[] modules, Func<string, VectorTarget> targetFunc)
        {
            return AllStrings(modules.Select(m => new[] { m, m.Replace('+', '#').Replace('-', '+').Replace('#', '-') })).Select(s => targetFunc(s));
        }

        // why not
        private static IEnumerable<string> AllStrings(IEnumerable<IEnumerable<string>> strs)
        {
            if (strs.Any())
            {
                var head = strs.First();
                var tail = strs.Skip(1);

                foreach (var r in AllStrings(tail))
                    foreach (var s in head)
                        yield return s + r;
            }
            else
            {
                yield return "";
            }
        }

        public Linear.Matrix<double> PrepareFox(double totalWeight)
        {
            int N = TargetSize;
            int k = ModuleSize;

            double elementWeight = totalWeight / (k * N);

            double correl(int i, int j) => Targets[0].Vector[i] * Targets[0].Vector[j];
            var mat = Linear.CreateMatrix.Dense(N, N, (i, j) => i / k == j / k ? elementWeight * correl(i, j) : 0);

            return mat;
        }

        public Linear.Matrix<double> PrepareBlizzard(double totalWeight)
        {
            int N = TargetSize;
            int k = ModuleSize;

            double elementWeight = totalWeight / N;
            
            double correl(int i, int j) => Targets[0].Vector[i] * Targets[0].Vector[j];
            var mat = Linear.CreateMatrix.Dense(N, N, (i, j) => i == j ? elementWeight * correl(i, j) : 0);

            return mat;
        }

        public Linear.Matrix<double> PrepareHusky(double totalWeight)
        {
            int N = TargetSize;
            int k = ModuleSize;

            double elementWeight = totalWeight / N;
            
            double correl(int i, int j) => Targets[0].Vector[i] * Targets[0].Vector[j];
            var mat = Linear.CreateMatrix.Dense(N, N, (i, j) => i / k == j / k && (j % k == 0) ? elementWeight * correl(i, j) : 0);

            return mat;
        }
    }
}
