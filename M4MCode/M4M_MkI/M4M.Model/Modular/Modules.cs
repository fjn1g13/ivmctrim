﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M4M.State;

namespace M4M.Modular
{
    public interface IModulesStringParser
    {
        /// <summary>
        /// The name of Module String Parser
        /// </summary>
        string Name { get; }

        /// <summary>
        /// User-friendly description of the module string format
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Attempts to parse a string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="modules"></param>
        /// <returns></returns>
        bool TryParse(string str, out Modules modules);

        /// <summary>
        /// Throws is the string format is invalid
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        Modules Parse(string str);
    }

    public class ModulesStringParser : IModulesStringParser
    {
        public static readonly ModulesStringParser Instance = new ModulesStringParser();

        public string Name => "ModulesStringParser";

        public string Description => "ModulesStringParser: aaabbca => [0, 1, 2, 6], [3, 4], [5]. Only support unicode single-char letters and digits (behaviour with surrogate pairs is broken and undefined)";

        public Modules Parse(string str)
        {
            return Modules.CreateFromString(str);
        }

        public bool TryParse(string str, out Modules modules)
        {
            if (string.IsNullOrEmpty(str))
            {
                modules = null;
                return false;
            }
            else
            {
                modules = Parse(str);
                return true;
            }
        }
    }

    public class BlockModulesStringParser : IModulesStringParser
    {
        public static readonly BlockModulesStringParser Instance = new BlockModulesStringParser();

        public string Name => "BlockModulesStringParser";

        public string Description => "Creates block-modules 'along the diagonal': 1,2,3 -> [0], [1, 2], [3, 4, 5] or 2*3 -> [0, 1, 2], [3, 4, 5]";

        public Modules Parse(string str)
        {
            return Modules.CreateBlockModules(str.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).SelectMany(ExpandStar).ToArray());
        }

        /// <summary>
        /// 3*2 -> 2,2,2
        /// Returns null if invalid
        /// </summary>
        private static int[] ExpandStar(string maybeStar)
        {
            if (maybeStar.Contains("*"))
            {
                string[] data = maybeStar.Split('*');

                if (data.Length != 2)
                    return null;
                
                if (!int.TryParse(data[0], out int l))
                    return null;
                if (!int.TryParse(data[1], out int r))
                    return null;
                
                return Enumerable.Repeat(r, l).ToArray();
            }
            else
            {
                if (!int.TryParse(maybeStar, out var n))
                    return null;
                return new[] { n };
            }
        }

        public bool TryParse(string str, out Modules modules)
        {
            if (string.IsNullOrEmpty(str))
            {
                modules = null;
                return false;
            }
            else
            {
                var data = str.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).Select(ExpandStar).ToArray();

                if (data.Any(x => x == null))
                {
                    modules = null;
                    return false;
                }

                modules = Modules.CreateBlockModules(data.SelectMany(x => x).ToArray());
                return true;
            }
        }
    }

    /// <summary>
    /// Guesses at the format, but atleast it tells you what it guessed
    /// </summary>
    public class MultiModulesStringParser : IModulesStringParser
    {
        public static readonly MultiModulesStringParser Instance = new MultiModulesStringParser();

        public string Name => throw new NotImplementedException();

        public string Description => throw new NotImplementedException();

        public Modules Parse(string str)
        {
            if (TryParse(str, out var modules))
                return modules;
            else
                throw new Exception($"Unable to parse string '{str}'");
        }

        public bool TryParse(string str, out Modules modules) => TryParse(str, out modules, out _);

        public bool TryParse(string str, out Modules modules, out IModulesStringParser parser)
        {
            parser = InferParser(str);
            return parser.TryParse(str, out modules);
        }

        public IModulesStringParser InferParser(string str)
        {
            if (str.Contains(',') || str.Contains(';') || str.Contains('*'))
            {
                return BlockModulesStringParser.Instance;
            }
            else
            {
                return ModulesStringParser.Instance;
            }
        }
    }

    /// <summary>
    /// Represents a collection of disjoint modules
    /// </summary>
    [StateClass]
    public class Modules
    {
        [Obsolete]
        protected Modules()
        {
        }

        private Modules(int[][] moduleAssignments, bool copy, bool verify)
        {
            if (copy)
                _moduleAssignments = moduleAssignments.Select(inner => inner.ToArray()).ToArray();
            else
                _moduleAssignments = moduleAssignments;
            
            int count = _moduleAssignments.Sum(m => m.Length);
            _elementCount = count;

            if (verify)
            {
                // check that every index is used exactly once
                bool[] table = new bool[count];

                foreach (var m in _moduleAssignments)
                {
                    foreach (var i in m)
                    {
                        if (i < 0 || i > count)
                            throw new Exception("Module assignemnt contains invalid index: " + i);
                        
                        if (table[i])
                            throw new Exception("Module assignemnt contains duplicate index: " + i + ". Modules must be disjoint");
                        
                        table[i] = true;
                    }
                }
            }
        }

        public Modules(IEnumerable<IEnumerable<int>> moduleAssignments)
            : this(moduleAssignments.Select(inner => inner.ToArray()).ToArray(), false, true)
        {
        }

        /// <summary>
        /// The total number of elements
        /// </summary>
        [SimpleStateProperty("ElementCount")]
        private int _elementCount { get; set; }

        /// <summary>
        /// Returns the total number of elements
        /// </summary>
        public int ElementCount
        {
            get
            {
                if (_elementCount == 0)
                {
                    // I messed up, and some of the older instances of this class are serialised with ElementCount=0, so we have to detect that and deal with it
                    // we can't use PostRead, because ModuleAssignments might not have finished reading, so it could crash
                    _elementCount = _moduleAssignments.Sum(m => m.Length);
                }

                return _elementCount;
            }
        }

        [SimpleStateProperty("ModuleAssignments")]
        private int[][] _moduleAssignments { get; set; }

        /// <summary>
        /// Returns the ModuleAssignments directly
        /// </summary>
        public IReadOnlyList<IReadOnlyList<int>> ModuleAssignments => _moduleAssignments;

        /// <summary>
        /// Creates a copy of the modular assignments
        /// </summary>
        /// <returns></returns>
        public int[][] ToArray()
        {
            return _moduleAssignments.Select(inner => inner.ToArray()).ToArray();
        }
        
        /// <summary>
        /// The number of modules
        /// </summary>
        public int ModuleCount => _moduleAssignments.Length;

        /// <summary>
        /// Enumerates the elements associated with the module of the given index
        /// </summary>
        /// <param name="moduleIndex">Module indices are in the range [0, moduleCount-1]</param>
        public IReadOnlyList<int> this[int moduleIndex] => _moduleAssignments[moduleIndex];

        /// <summary>
        /// Creates Block Modules of the same size
        /// </summary>
        /// <param name="moduleCount">The number of modules</param>
        /// <param name="moduleSize">The size of each modules</param>
        public static Modules CreateBlockModules(int moduleCount, int moduleSize)
        {
            return new Modules(Enumerable.Range(0, moduleCount).Select(mi => Enumerable.Range(mi * moduleSize, moduleSize).ToArray()).ToArray(), false, true);
        }

        /// <summary>
        /// Creates Block Modules of the given sizes
        /// </summary>
        /// <param name="moduleSizes">The sizes of each modules</param>
        public static Modules CreateBlockModules(IReadOnlyList<int> modulesSizes)
        {
            //var ass = modulesSizes.Aggregate((acc: 0, l: Enumerable.Empty<int>()), (a, s) => (a.acc + s, a.l.Concat(Enumerable.Range(a.acc, s))), a => a.l.ToArray());

            int[][] assignments = new int[modulesSizes.Count][];

            int t = 0;
            for (int i = 0; i < modulesSizes.Count; i++)
            {
                assignments[i] = Enumerable.Range(t, modulesSizes[i]).ToArray();
                t += modulesSizes[i];
            }
            
            return new Modules(assignments, false, true);
        }
        
        /// <summary>
        /// Creates Modules from a string of symbols indicating the module assignments
        /// </summary>
        /// <param name="str">The string containing the per-element module assignments</param>
        public static Modules CreateFromString(string str)
        {
            // aaabbca -> [0, 1, 2, 6], [3, 4], [5]
            char[] symbols = str.Distinct().OrderBy(c => c).ToArray();
            var assignments = symbols.Select(s => str.IndexWhere(c => c == s));
            return new Modules(assignments);
        }

        /// <summary>
        /// Renders a module string using the default symbol set
        /// </summary>
        public new string ToString()
        {
            return ToString(Enumerable.Range(0, ModuleCount).Select(i => (char)('a' + (i % 26))));
        }
        
        /// <summary>
        /// Renders a module string using the given symbol set
        /// (Does not check that your symbols are distinct)
        /// </summary>
        /// <param name="symbols">A sequence of symbols to use for each module</param>
        public string ToString(IEnumerable<char> symbols)
        {
            char[] c = new char[ElementCount];

            int i = 0;
            foreach (var s in symbols)
            {
                if (i >= ModuleCount)
                    break;

                foreach (var j in this[i])
                {
                    c[j] = s;
                }

                i++;
            }

            return new string(c);
        }

        /// <summary>
        /// Expands the given values from per-module values to per-element values
        /// </summary>
        public T[] Expand<T>(IReadOnlyList<T> values)
        {
            if (values.Count != ModuleAssignments.Count)
                throw new ArgumentException("Incorrect number of values provided; must be exactly one value per module", nameof(values));

            var res = new T[ElementCount];

            for (int mi = 0; mi < ModuleAssignments.Count; mi++)
            {
                foreach (var j in ModuleAssignments[mi])
                {
                    res[j] = values[mi];
                }
            }

            return res;
        }

        /// <summary>
        /// Gets an array where each entry points to a leader. The parent is the 'first' member of the module to which the element is assigned.
        /// For example, aabbbc would produce [0, 0, 2, 2, 2, 5].
        /// </summary>
        public int[] GetDefaultLeaderMap()
        {
            int[] res = new int[ElementCount];

            foreach (var module in ModuleAssignments)
            {
                var defaultParent = module[0];

                foreach (var e in module)
                {
                    res[e] = defaultParent;
                }
            }

            return res;
        }

        /// <summary>
        /// Enumerates all the <see cref="MatrixEntryAddress"/> that correspond to the on-blocks.
        /// </summary>
        public IEnumerable<MatrixEntryAddress> EnumerateOnBlockMatrixEntryAddresses()
        {
            foreach (var m in _moduleAssignments)
            {
                foreach (var i in m)
                {
                    foreach (var j in m)
                    {
                        yield return new MatrixEntryAddress(i, j);
                    }
                }    
            }
        }

        /// <summary>
        /// Enumerates all the <see cref="MatrixEntryAddress"/> that correspond to the off-blocks.
        /// </summary>
        public IEnumerable<MatrixEntryAddress> EnumerateOffBlockMatrixEntryAddresses()
        {
            foreach (var m1 in _moduleAssignments)
            {
                foreach (var m2 in _moduleAssignments)
                {
                    if (m1 == m2)
                        continue;

                    foreach (var i in m1)
                    {
                        foreach (var j in m2)
                        {
                            yield return new MatrixEntryAddress(i, j);
                        }
                    }
                }
            }
        }
    }
}
