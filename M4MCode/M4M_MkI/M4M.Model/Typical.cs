﻿using System.Collections.Generic;
using System.Diagnostics;
using static M4M.Misc;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M
{
    public static class TypicalConfiguration
    {
        public static DevelopmentRules CreateStandardDevelopmentRules(ISquash squash, int timeSteps = 10, double updateRate = 1.0, double decayRate = 0.2, double rescaleScale = 1.0, double rescaleOffset = 0.0)
        {
            DevelopmentRules drules = new DevelopmentRules(timeSteps, updateRate, decayRate, squash, rescaleScale, rescaleOffset);

            return drules;
        }

        public static ReproductionRules CreateStandardReproductionRules(double gMutationMagnitude, double bMutationMagnitude, double bMutationProbability, bool exclusiveBMutation = false, int gUpdates = 1, Range gClamping = null, NoiseType gMutationType = NoiseType.Uniform, NoiseType bMutationType = NoiseType.Uniform, Range bClamping = null)
        {
            ReproductionRules rrules = new ReproductionRules(gMutationMagnitude, bMutationMagnitude, bMutationProbability, exclusiveBMutation, gUpdates, gClamping, gMutationType, bMutationType, bClamping);

            return rrules;
        }

        public static JudgementRules CreateStandardJudgementRules(double regularisationFactor, IRegularisationFunction<IGenome> regularisationFunction, double noiseFactor = 0.0)
        {
            JudgementRules jrules = new JudgementRules(regularisationFactor, regularisationFunction, noiseFactor);

            return jrules;
        }

        public static Population<DenseIndividual> CreatePopulation(ModelExecutionContext context, int populationSize, int N, DevelopmentRules drules, Linear.Vector<double> g0 = null, Linear.Matrix<double> b0 = null, IReadOnlyList<int> gOpenEntries = null, IReadOnlyList<MatrixEntryAddress> bOpenEntries = null, ITransMatMutator transMatMutator = null, IInitialStateMutator initialStateMutator = null, ITransMatCombiner transMatCombiner = null, IInitialStateCombiner initialStateCombiner = null, IDevelopmentStep developmentStep = null, bool epigenetic = false, double stateNoiseFactor = 0.0, Linear.Vector<double> biasVector0 = null)
        {
            if (epigenetic || stateNoiseFactor != 0.0)
            {
                Trace.TraceWarning($"{nameof(stateNoiseFactor)} is non-zero, but {nameof(epigenetic)} is false: {nameof(stateNoiseFactor)} may be ignored");
            }

            g0 = g0 ?? Linear.CreateVector.Dense(N, 0.0);
            b0 = b0 ?? Linear.CreateMatrix.Dense(N, N, 0.0);
            developmentStep = developmentStep ?? Development.DefaultDevelopmentStep(N, stateNoiseFactor);

            DenseIndividual CreateDefaultIndividual(ModelExecutionContext _context)
            {
                var genome = new DenseGenome(g0, b0, gOpenEntries, bOpenEntries, transMatMutator, initialStateMutator, transMatCombiner, initialStateCombiner, developmentStep, biasVector0, false);
                var individual = DenseIndividual.Develop(genome, _context, drules, epigenetic);
                return individual;
            }

            // population stuff
            Population<DenseIndividual> population = new Population<DenseIndividual>(context, _context => CreateDefaultIndividual(_context), populationSize);

            return population;
        }

        public static ExperimentConfiguration CreateConfig(int N, DevelopmentRules drules, ReproductionRules rrules, JudgementRules jrules, IReadOnlyList<ITarget> targets, int epochs, int generationsPerTargetPerEpoch, double gResetProb, Range gResetRange, ICycler targetCycler = null, bool shuffleTargets = false)
        {
            targetCycler = targetCycler ?? Cyclers.Loop;

            ExperimentConfiguration config = new ExperimentConfiguration(N, targets, targetCycler, epochs, generationsPerTargetPerEpoch, gResetProb, gResetRange, shuffleTargets, drules, rrules, jrules);

            return config;
        }

        public static PopulationExperimentConfig<DenseIndividual> CreatePopulationConfig(ExperimentConfiguration config, int eliteCount, bool hillclimberMode, ISelectorPreparer<DenseIndividual> selector = null, IPopulationEndTargetOperation<DenseIndividual> populationEndTargetOperation = null, IPopulationResetOperation<DenseIndividual> resetIndividualInitialStateOperation = null, IPopulationSpinner<DenseIndividual> customPopulationSpinner = null)
        {
            selector = selector ?? SelectorPreparers<DenseIndividual>.Ranked;
            resetIndividualInitialStateOperation = resetIndividualInitialStateOperation ?? PopulationResetOperations.None;

            PopulationExperimentConfig<DenseIndividual> popConfig = new PopulationExperimentConfig<DenseIndividual>(config, selector, eliteCount, hillclimberMode, populationEndTargetOperation, resetIndividualInitialStateOperation, customPopulationSpinner);

            return popConfig;
        }
    }
}
