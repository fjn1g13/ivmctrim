﻿using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static M4M.Misc;
using Linear = MathNet.Numerics.LinearAlgebra;
using static M4M.Analysis;
using System.Diagnostics;

namespace M4M
{
    public class SavedGenome
    {
        public SavedGenome(int epoch, DenseGenome genome, GenomeDistributionInfo gdi)
        {
            Epoch = epoch;
            Genome = genome;
            GenomeDistibutionInfo = gdi;
        }

        public int Epoch { get; }
        public DenseGenome Genome { get; }
        public GenomeDistributionInfo GenomeDistibutionInfo { get; }
    }

    public class ModuleAnalysis
    {
        public Linear.Matrix<double> Region { get; }
        public IReadOnlyList<double> SnowflakeWeights { get; }
        public int BeamIndex { get; }
        public IReadOnlyList<double> BeamWeights { get; }
        public double SnowflakeBeamRatio { get; }
        public double Huskyness { get; }

        public ModuleAnalysis(DenseGenome genome, int startIndex = 0, int count = -1)
        {
            if (count < 0)
                count = genome.Size;

            // cut a square out
            Region = genome.TransMat.SubMatrix(startIndex, count, startIndex, count);

            // compute
            SnowflakeWeights = Region.Diagonal().ToArray();
            BeamIndex = SnowflakeWeights.IndexMax(k => k);
            BeamWeights = Region.Column(BeamIndex).ToArray();
            SnowflakeBeamRatio = BeamWeights[BeamIndex] / BeamWeights.Sum();
            Huskyness = BeamWeights.Sum() / Region.Enumerate().Sum();
        }
    }

    public class GenomeInfo<TGenome> where TGenome : IGenome<TGenome>
    {
        public GenomeInfo(TGenome genome, Phenotype phenotype, MultiMeasureJudgement judgement)
        {
            Genome = genome;
            Phenotype = phenotype;
            Judgement = judgement;
        }

        public TGenome Genome { get; }
        public Phenotype Phenotype { get; }
        public MultiMeasureJudgement Judgement { get; }

        public static GenomeInfo<TGenome> Process(TGenome genome, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target)
        {
            // compute fitness
            Phenotype phenotype = genome.Develop(context, drules);
            MultiMeasureJudgement judgement = MultiMeasureJudgement.Judge(genome, phenotype, jrules, target);

            return new GenomeInfo<TGenome>(genome, phenotype, judgement);
        }
    }

    /// <summary>
    /// Describes the distribution of phenotypes generated from a given genotype compared to a given set of expected frequencies
    /// </summary>
    public class GenomeDistributionInfo
    {
        private GenomeDistributionInfo(DenseGenome genome, string name, IReadOnlyDictionary<VectorTarget, int> counts, IReadOnlyDictionary<VectorTarget, double> frequencies, double error, double entropy)
        {
            Genome = genome;
            Name = name;
            Counts = counts;
            Frequencies = frequencies;
            Error = error;
            Entropy = entropy;
        }

        public static GenomeDistributionInfo Process(ModelExecutionContext context, DenseGenome g, DevelopmentRules drules, IReadOnlyDictionary<VectorTarget, double> expected, string name, int samples = 5000)
        {
            var d = Distribute(RandomPhenotypes(context, g, drules, samples), expected.Keys.ToArray());
            int total = samples;
            Dictionary<VectorTarget, double> frequencies = d.ToDictionary(kv => kv.Key, kv => (double)kv.Value / total);
            double error = ChiSquaredError(frequencies, expected);
            double entropy = Entropy(frequencies.Values);

            return new GenomeDistributionInfo(g, name, d, frequencies, error, entropy);
        }

        public DenseGenome Genome { get; }
        public string Name { get; }
        public IReadOnlyDictionary<VectorTarget, int> Counts { get; }
        public IReadOnlyDictionary<VectorTarget, double> Frequencies { get; }
        public double Error { get; }
        public double Entropy { get; }
    }
    
    public static class Analysis
    {
        public static void ExtractMatrix(double[][] trajectories, int index, Linear.Matrix<double> mat)
        {
            for (int i = 0; i < mat.RowCount; i++)
                for (int j = 0; j < mat.ColumnCount; j++)
                    mat[i, j] = trajectories[i * mat.ColumnCount + j][index];
        }

        public static void ExtractVector(double[][] trajectories, int index, Linear.Vector<double> vec)
        {
            for (int i = 0; i < trajectories.Length; i++)
                vec[i] = trajectories[i][index];
        }

        public static Linear.Matrix<double> ExtractMatrix(double[][] trajectories, int index, int rowCount, int colCount)
        {
            var mat = Linear.CreateMatrix.Dense<double>(rowCount, colCount, (i, j) => trajectories[i * colCount + j][index]);
            return mat;
        }

        public static Linear.Vector<double> ExtractVector(double[][] trajectories, int index)
        {
            var vec = Linear.CreateVector.Dense<double>(trajectories.Length, i => trajectories[i][index]);
            return vec;
        }

        public static double[] ExtractArray(double[][] trajectories, int index)
        {
            var arr = Misc.Create(trajectories.Length, i => trajectories[i][index]);
            return arr;
        }

        // TODO: we need to be able to attach metadata to these things, describing the axes and intervals and such, so that RCS viewer can do something useful with them
        // we could make the first 'length' a version indicator when negative
        // really we want a new pair of methods which take a proper class
        // (RCSViewer can have a dependency on NetState and M4M.Model, that is fine, or we can write a new better version,
        // or write a new program which produces pretty plots (since RCSViewer has special RCS feature we want to keep)
        // (first we need a versioning story in NetState)
        public static void SaveTrajectories(String filename, double[][] trajectories, int samplePeriod)
        {
            using (System.IO.FileStream fs = System.IO.File.Open(filename, System.IO.FileMode.Create))
            {
                NetState.Serialisation.Utils.WriteInt32(fs, trajectories.Length); // length
                NetState.Serialisation.Utils.WriteInt32(fs, trajectories[0].Length); // width
                
                for (int i = 0; i < trajectories.Length; i++)
                {
                    double[] curr = trajectories[i];

                    for (int j = 0; j < curr.Length; j++)
                    {
                        NetState.Serialisation.Utils.WriteFloat64(fs, curr[j]);
                    }
                }

                if (samplePeriod >= 0)
                {
                    NetState.Serialisation.Utils.WriteInt32(fs, -1);
                    NetState.Serialisation.Utils.WriteInt32(fs, samplePeriod);
                }
            }
        }

        public static double[][] LoadTrajectories(String filename, out int samplePeriod)
        {
            using (System.IO.FileStream fs = System.IO.File.Open(filename, System.IO.FileMode.Open))
            {
                int length = NetState.Serialisation.Utils.ReadInt32(fs);
                int width = NetState.Serialisation.Utils.ReadInt32(fs);

                double[][] trajectories = new double[length][];
                
                for (int i = 0; i < trajectories.Length; i++)
                {
                    double[] curr = trajectories[i] = new double[width];

                    for (int j = 0; j < curr.Length; j++)
                    {
                        curr[j] = NetState.Serialisation.Utils.ReadFloat64(fs);
                    }
                }

                if (fs.Position < fs.Length)
                { // there is more
                    int signaller = NetState.Serialisation.Utils.ReadInt32(fs);
                    samplePeriod = NetState.Serialisation.Utils.ReadInt32(fs);
                }
                else
                {
                    samplePeriod = -1;
                }

                return trajectories;
            }
        }

        public static IEnumerable<Linear.Matrix<double>> EnumerateMatricies(double[][] trajectories, int rowCount, int colCount, int start, int end, bool reuseMatrix = false)
        {
            if (reuseMatrix)
            {
                var mat = Linear.CreateMatrix.Dense<double>(rowCount, colCount);
                for (int i = start; i < end; i++)
                {
                    ExtractMatrix(trajectories, i, mat);
                    yield return mat;
                }
            }
            else
            {
                for (int i = start; i < end; i++)
                {
                    yield return ExtractMatrix(trajectories, i, rowCount, colCount);
                }
            }
        }

        /// <summary>
        /// Saves basic information about a genome. Does not save custom mutators/combiners/etc.
        /// </summary>
        public static void SaveGenome(string filename, DenseGenome g)
        {
            using (System.IO.FileStream fs = System.IO.File.Open(filename, System.IO.FileMode.Create))
            {
                NetState.Serialisation.Utils.WriteInt32(fs, g.Size);

                for (int i = 0; i < g.Size; i++)
                    NetState.Serialisation.Utils.WriteFloat64(fs, g.InitialState[i]);

                for (int i = 0; i < g.Size; i++)
                    for (int j = 0; j < g.Size; j++)
                        NetState.Serialisation.Utils.WriteFloat64(fs, g.TransMat[i, j]);

                int entryCount = g.TransMatIndexOpenEntries == null ? -1 : g.TransMatIndexOpenEntries.Count;
                NetState.Serialisation.Utils.WriteInt32(fs, entryCount);

                for (int i = 0; i < entryCount; i++)
                {
                    NetState.Serialisation.Utils.WriteInt32(fs, g.TransMatIndexOpenEntries[i].Row);
                    NetState.Serialisation.Utils.WriteInt32(fs, g.TransMatIndexOpenEntries[i].Col);
                }
            }
        }

        /// <summary>
        /// Loads basic information about a genome. Does not include custom mutators/combiners/etc.
        /// </summary>
        public static DenseGenome LoadGenome(string filename)
        {
            using (System.IO.FileStream fs = System.IO.File.Open(filename, System.IO.FileMode.Open))
            {
                int size = NetState.Serialisation.Utils.ReadInt32(fs);

                Linear.Vector<double> initialState = Linear.CreateVector.Dense<double>(size);
                for (int i = 0; i < size; i++)
                    initialState[i] = NetState.Serialisation.Utils.ReadFloat64(fs);

                Linear.Matrix<double> transMat = Linear.CreateMatrix.Dense<double>(size, size);
                for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                        transMat[i, j] = NetState.Serialisation.Utils.ReadFloat64(fs);

                int entryCount = NetState.Serialisation.Utils.ReadInt32(fs);

                MatrixEntryAddress[] openEntries = entryCount >= 0 ? new MatrixEntryAddress[entryCount] : null;

                if (openEntries != null)
                {
                    for (int i = 0; i < entryCount; i++)
                        openEntries[i] = new MatrixEntryAddress(NetState.Serialisation.Utils.ReadInt32(fs), NetState.Serialisation.Utils.ReadInt32(fs));
                }

                return new DenseGenome(initialState, transMat, null, openEntries);
            }
        }

        /// <summary>
        /// Loads basic information about all genomes in a given directory along with distribution information. Does not include custom mutators/combiners/etc.
        /// </summary>
        public static IEnumerable<SavedGenome> LoadGenomes(string dir, DevelopmentRules drules, IReadOnlyDictionary<VectorTarget, double> expected, int gdiSamples = 50000)
        {
            RandomSource rand = new MersenneTwister();
            ModelExecutionContext context = new ModelExecutionContext(rand);

            var files = System.IO.Directory.GetFiles(dir);
            foreach (var p in files)
            {
                var f = System.IO.Path.GetFileNameWithoutExtension(p);
                if (f.StartsWith("Genome") || f.StartsWith("genome"))
                {
                    int epoch = int.Parse(f.Substring(6));
                    DenseGenome genome = Analysis.LoadGenome(p);

                    yield return new SavedGenome(epoch, genome, GenomeDistributionInfo.Process(context, genome, drules, expected, f, gdiSamples));
                }
            }
        }

        public static double HammingDistance(Linear.Vector<double> a, Linear.Vector<double> b)
        {
            Debug.Assert(a.Count == b.Count, $"Program/HammingDistance.ctor(string): a and b must be the same length");

            double acc = 0.0;

            for (int i = 0; i < a.Count; i++)
            {
                acc += Math.Abs(a[i] - b[i]);
            }

            return acc;
        }

        public static bool Matches(Linear.Vector<double> a, Linear.Vector<double> b)
        {
            Debug.Assert(a.Count == b.Count, $"Program/HammingDistance.ctor(string): a and b must be the same length");
            
            for (int i = 0; i < a.Count; i++)
            {
                if (a[i] * b[i] < 0)
                    return false;
            }

            return true;
        }

        public static VectorTarget Categorize(Phenotype p, IReadOnlyList<VectorTarget> targets)
        {
            // there must be some thresholding going on, let's try to work out what it is (probably the same as implied on p18)
            return targets.FirstOrDefault(t => Matches(p.Vector, t.Vector));

            //return targets.ArgMin(t => HammingDistance(t.Vector, p.Vector));
        }

        public static IReadOnlyDictionary<VectorTarget, int> Distribute(IEnumerable<Phenotype> phenotypes, IReadOnlyList<VectorTarget> targets)
        {
            Dictionary<VectorTarget, int> table = new Dictionary<VectorTarget, int>();

            foreach (VectorTarget t in targets)
            {
                table[t] = 0;
            }

            foreach (Phenotype p in phenotypes)
            {
                VectorTarget placement = Categorize(p, targets);

                if (placement != null)
                {
                    table[placement]++;
                }
            }

            return table;
        }

        public static IEnumerable<Phenotype> RandomPhenotypes(ModelExecutionContext context, DenseGenome template, DevelopmentRules drules, int count)
        {
            for (int i = 0; i < count; i++)
            {
                var initialState = RandomUniformVector(context.Rand, template.Size);
                var g = template.Clone(context);
                g.CopyOverInitialState(initialState);
                yield return g.Develop(context, drules);
            }
        }

        public static Linear.Vector<double> RandomUniformVector(MathNet.Numerics.Random.RandomSource rand, int size)
        {
            double[] arr = Create<double>(size, i => rand.NextDouble() * 2.0 - 1.0);
            return Linear.CreateVector.Dense<double>(arr);
        }

        /// <summary>
        /// Determines the proportion of modules which have a dev-time dominaint trait
        /// (Involves developing the genome)
        /// </summary>
        public static double MeasureDevTimeDominance(RandomSource rand, DevelopmentRules drules, DenseGenome genome, int[][] modules)
        {
            return DetectDevTimeDominance(rand, drules, genome, modules).Count(b => b);
        }

        /// <summary>
        /// Determines which modules have a dev-time dominaint trait
        /// (Involves developing the genome)
        /// </summary>
        public static bool[] DetectDevTimeDominance(RandomSource rand, DevelopmentRules drules, DenseGenome genome, int[][] modules)
        {
            double[][] trajectories = null;
            genome.DevelopWithTrajectories(rand, drules, ref trajectories);

            return DetectDevTimeDominance(trajectories, modules);
        }

        /// <summary>
        /// Determines which modules have a dev-time dominaint trait
        /// </summary>
        public static bool[] DetectDevTimeDominance(double[][] trajectories, int[][] modules)
        {
            var sums = trajectories.Select(t => t.Sum(e => Math.Abs(e))).ToArray();

            bool[] res = new bool[modules.Length];
            for (int mi = 0; mi < modules.Length; mi++)
            {
                var module = modules[mi];
                var dominantTrait = module.ArgMax(i => sums[i]);
                var isDominant = CheckIsDominant(trajectories, dominantTrait, module);
                res[mi] = isDominant;
            }

            return res;
        }

        public static bool CheckIsDominant(double[][] trajectores, int trait, int[] others)
        {
            bool oneWeaker = false;

            var upper = trajectores[trait];
            foreach (var t in others)
            {
                var other = trajectores[t];

                for (int i = 0; i < upper.Length; i++)
                {
                    var u = Math.Abs(upper[i]);
                    var o = Math.Abs(other[i]);

                    if (u > o)
                        oneWeaker = true;
                    else if (u < o)
                        return false; // not dominant
                }
            }

            return oneWeaker;
        }

        /// <summary>
        /// Computes the huskyness of a module containing 2 or more traits
        /// huskyness = (max_col_weight / total_weight), scaled between 0 (dense/nothing) and 1 (ideal husky)
        /// Returns 0 if there are no weights
        /// </summary>
        public static double ComputeModuleHuskyness(Linear.Matrix<double> dtm, IEnumerable<int> moduleTraits)
        {
            double[] columnSums = moduleTraits.Select(j => moduleTraits.Sum(i => Math.Abs(dtm[i, j]))).ToArray();

            int count = columnSums.Length;
            if (count < 2)
            {
                return double.NaN; // too much of a faff having this throw
                //throw new ArgumentException(nameof(moduleTraits), "A module must comprise 2 or more traits");
            }

            double max = columnSums.Max();
            if (max == 0) // avoid div0: this is not a husky
                return 0.0;

            double total = columnSums.Sum();
            double hratio = max / total;

            if (double.IsNaN(hratio) || double.IsInfinity(hratio))
                return double.NaN; // probably ought throw...

            // re-scale hratio from [1/n, 1] to [0, 1]
            int n = count;
            return (hratio - (1.0 / n)) / (1.0 - (1.0 / n));
        }

        /// <summary>
        /// Computes the independence of a module
        /// module_independence = (weight_within_module / weight_in_module_row), on scale between 0 (no control) and 1 (total independence)
        /// Returns NaN if there are no weights
        /// </summary>
        public static double ComputeModuleIndependence(Linear.Matrix<double> dtm, IReadOnlyList<int> moduleTraits)
        {
            double withinSum = 0.0;
            double rowSum = 0.0;

            foreach (var i in moduleTraits)
            {
                foreach (var j in moduleTraits)
                {
                    withinSum += Math.Abs(dtm[i, j]);
                }

                for (int j = 0; j < dtm.ColumnCount; j++)
                {
                    rowSum += Math.Abs(dtm[i, j]);
                }
            }

            var iratio = withinSum / rowSum;

            if (double.IsNaN(iratio) || double.IsInfinity(iratio))
                return double.NaN;

            return iratio;
        }

        /// <summary>
        /// Computes the huskyness of each module in tje given dtm
        /// huskyness = (max_col_weight / total_weight), scaled between 0 (dense/nothing) and 1 (ideal husky)
        /// Returns 0 if there are no weights
        /// </summary>
        public static double[] ComputeModuleHuskyness(Linear.Matrix<double> dtm, IEnumerable<IEnumerable<int>> moduleAssignments)
        {
            return moduleAssignments.Select(m => ComputeModuleHuskyness(dtm, m)).ToArray();
        }

        /// <summary>
        /// Computes the huskyness of each module in tje given dtm
        /// huskyness = (max_col_weight / total_weight), scaled between 0 (dense/nothing) and 1 (ideal husky)
        /// Returns 0 if there are no weights
        /// </summary>
        public static double[] ComputeModuleIndependence(Linear.Matrix<double> dtm, IEnumerable<IEnumerable<int>> moduleAssignments)
        {
            return moduleAssignments.Select(m => ComputeModuleIndependence(dtm, m.ToList())).ToArray();
        }

        /// <summary>
        /// Computes the huskyness of sequencial block modules of the given size
        /// </summary>
        public static IEnumerable<double> ComputeBlockModuleHuskynesses(Linear.Matrix<double> dtm, int moduleSize)
        {
            for (int i = 0; i < dtm.ColumnCount; i += moduleSize)
            {
                yield return ComputeModuleHuskyness(dtm, Enumerable.Range(i, moduleSize));
            }
        }

        /// <summary>
        /// Returns an array of arrays of members of the given number of sequential block modules of the given size
        /// </summary>
        public static int[][] BlockModuleTraits(int moduleSize, int moduleCount)
        {
            int[][] res = new int[moduleCount][];

            for (int i = 0; i < moduleCount; i++)
            {
                res[i] = Enumerable.Range(i * moduleSize, moduleSize).ToArray();
            }

            return res;
        }

        /// <summary>
        /// Computes a simple fitness delta
        /// </summary>
        /// <param name="signed">True to multiply the delta by the sign</param>
        public static TResult ComputeSimpleDelta<TResult>(ModelExecutionContext context, ITarget target, JudgementRules jrules, DevelopmentRules drules, DenseIndividual individual, IReadOnlyList<MatrixEntryAddress> entries, double delta, bool signed, Func<MultiMeasureJudgement, MultiMeasureJudgement, TResult> fitnessComparer)
        {
            var individual2 = individual.Clone(context);
            var dtm2 = individual2.Genome.TransMat;

            if (signed)
            {
                foreach (var mea in entries)
                {
                    dtm2[mea.Row, mea.Col] += delta * Math.Sign(dtm2[mea.Row, mea.Col]);
                }
            }
            else
            {
                foreach (var mea in entries)
                {
                    dtm2[mea.Row, mea.Col] += delta;
                }
            }

            individual2.DevelopInplace(context, drules);

            var f = MultiMeasureJudgement.Judge(individual.Genome, individual.Phenotype, jrules, target);
            var f2 = MultiMeasureJudgement.Judge(individual2.Genome, individual2.Phenotype, jrules, target);

            return fitnessComparer(f2, f);
        }
    }
    
    public class MutantAnalysis<TGenome> where TGenome : IGenome<TGenome>
    {
        public GenomeInfo<TGenome> Source { get; }
        public IReadOnlyList<GenomeInfo<TGenome>> MutantInfos { get; }

        public ReproductionRules ReproductionRules { get; }
        public DevelopmentRules DevelopmentRules { get; }
        public JudgementRules JudgementRules { get; }
        public ITarget Target { get; }

        public double[] FitnessChanges { get; }

        /// <summary>
        /// makes a copy of the genome, so you don't have to worry about it
        /// </summary>
        public MutantAnalysis(TGenome genome, int mutantCount, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target)
        {
            // no defensive copy

            ReproductionRules = rrules;
            DevelopmentRules = drules;
            JudgementRules = jrules;
            Target = target;
            
            Source = GenomeInfo<TGenome>.Process(genome, context, rrules, drules, jrules, target);
            MutantInfos = Enumerable.Range(0, mutantCount).Select(i => GenomeInfo<TGenome>.Process(genome.Mutate(context, rrules), context, rrules, drules, jrules, target)).ToArray();

            FitnessChanges = MutantInfos.Select(mi => mi.Judgement.CombinedFitness - Source.Judgement.CombinedFitness).ToArray();
        }
        
        public void ComputeFitnessChangePercentiles(double[] positions, ref double[] reusableBuffer, ref Percentile[] percentiles)
        {
            Percentile.ComputePercentiles(positions, FitnessChanges, ref reusableBuffer, ref percentiles);
        }
    }
}
