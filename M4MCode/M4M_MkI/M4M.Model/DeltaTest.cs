﻿using System;

namespace M4M
{
    // TODO: this should be an interface like IDeltaMode
    public enum DeltaResultMode
    {
        Best,
        Beneficial,
    }

    public enum DeltaResult
    {
        Neither = 0,
        Minus = 1,
        Plus = 2,
        Both = 3,
    }

    public static class DeltaResultExtensions
    {
        public static string ToSymbol(this DeltaResult deltaResult)
        {
            switch (deltaResult)
            {
                case DeltaResult.Neither:
                    return "/";
                case DeltaResult.Plus:
                    return "+";
                case DeltaResult.Minus:
                    return "-";
                case DeltaResult.Both:
                    return "±";
                default:
                    throw new ArgumentOutOfRangeException(nameof(deltaResult));
            }
        }
    }

    public interface IDeltaMode
    {
        string Name { get; }
        double Judge(ModelExecutionContext context, DenseGenome genome, DevelopmentRules drules, JudgementRules jrules, ITarget target, int r, int c, double delta);
        void Apply(DenseGenome genome, int r, int c, double delta);
    }

    public class CellDeltaMode : IDeltaMode
    {
        public static readonly CellDeltaMode Instance = new CellDeltaMode();

        public string Name => "Cell DeltaMode";

        public void Apply(DenseGenome genome, int r, int c, double delta)
        {
            genome.TransMat[r, c] += delta;
        }

        public double Judge(ModelExecutionContext context, DenseGenome genome, DevelopmentRules drules, JudgementRules jrules, ITarget target, int r, int c, double delta)
        {
            var old = genome.TransMat[r, c];
            genome.TransMat[r, c] += delta;
            var i = DenseIndividual.Develop(genome, context, drules, false);
            var f = i.Judge(jrules, target).CombinedFitness;
            genome.TransMat[r, c] = old;
            return f;
        }
    }

    public class ColumnDeltaMode : IDeltaMode
    {
        public static readonly ColumnDeltaMode Instance = new ColumnDeltaMode();
        
        public string Name => "Column DeltaMode";

        public void Apply(DenseGenome genome, int r, int c, double delta)
        {
            for (int ri = 0; ri < genome.Size; ri++)
                genome.TransMat[ri, c] += delta;
        }

        public double Judge(ModelExecutionContext context, DenseGenome genome, DevelopmentRules drules, JudgementRules jrules, ITarget target, int r, int c, double delta)
        {
            var old = genome.TransMat.Column(c);

            for (int ri = 0; ri < genome.Size; ri++)
                genome.TransMat[ri, c] += delta;

            var i = DenseIndividual.Develop(genome, context, drules, false);
            var f = i.Judge(jrules, target).CombinedFitness;

            genome.TransMat.SetColumn(c, old);
            return f;
        }
    }

    public class CompositeDeltaMode : IDeltaMode
    {
        public CompositeDeltaMode(string name, IDeltaMode applier, IDeltaMode judger)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Applier = applier ?? throw new ArgumentNullException(nameof(applier));
            Judger = judger ?? throw new ArgumentNullException(nameof(judger));
        }

        public string Name { get; }
        public IDeltaMode Applier { get; }
        public IDeltaMode Judger { get; }

        public void Apply(DenseGenome genome, int r, int c, double delta)
        {
            Applier.Apply(genome, r, c, delta);
        }

        public double Judge(ModelExecutionContext context, DenseGenome genome, DevelopmentRules drules, JudgementRules jrules, ITarget target, int r, int c, double delta)
        {
            return Judger.Judge(context, genome, drules, jrules, target, r, c, delta);
        }
    }

    public class DeltaTest
    {
        public double F0 { get; }
        public MathNet.Numerics.LinearAlgebra.Matrix<double> DeltaPlus { get; }
        public MathNet.Numerics.LinearAlgebra.Matrix<double> DeltaMinus { get; }
        private bool[,] Open { get; }
        
        public bool IsOpen(int r, int c)
        {
            return Open == null || Open[r, c];
        }

        public DeltaTest(double f0, MathNet.Numerics.LinearAlgebra.Matrix<double> deltaPlus, MathNet.Numerics.LinearAlgebra.Matrix<double> deltaMinus, bool[,] open)
        {
            F0 = f0;
            DeltaPlus = deltaPlus;
            DeltaMinus = deltaMinus;
            Open = open;
        }

        private void EnsureOpen(int r, int c)
        {
            if (!IsOpen(r, c))
                throw new InvalidOperationException($"Entry {r}, {c} is not open.");
        }

        public DeltaResult GetBest(int r, int c)
        {
            EnsureOpen(r, c);

            var p = DeltaPlus[r, c];
            var m = DeltaMinus[r, c];

            var cmp = p.CompareTo(m);

            if (cmp < 0)
                return m > F0 ? DeltaResult.Minus : DeltaResult.Neither;
            else if (cmp > 0)
                return p > F0 ? DeltaResult.Plus : DeltaResult.Neither;
            else
                return m > F0 ? DeltaResult.Both : DeltaResult.Neither;
        }

        public DeltaResult GetBeneficial(int r, int c)
        {
            EnsureOpen(r, c);

            var p = DeltaPlus[r, c];
            var m = DeltaMinus[r, c];

            var pp = p > F0;
            var mp = m > F0;

            if (pp && mp)
                return DeltaResult.Both;
            else if (pp)
                return DeltaResult.Plus;
            else if (mp)
                return DeltaResult.Minus;
            else
                return DeltaResult.Neither;
        }

        public static DeltaTest Test(DenseGenome genome, DevelopmentRules drules, JudgementRules jrules, ITarget target, double deltaMag, IDeltaMode deltaMode, bool[,] open = null)
        {
            var context = new ModelExecutionContext(new CustomMersenneTwister(0));
            
            var f0 = deltaMode.Judge(context, genome, drules, jrules, target, 0, 0, 0);

            var deltaPlus = MathNet.Numerics.LinearAlgebra.CreateMatrix.Dense(genome.Size, genome.Size, (i, j) => open == null || open[i, j] ? deltaMode.Judge(context, genome, drules, jrules, target, i, j, +deltaMag) : double.NaN);
            var deltaMinus = MathNet.Numerics.LinearAlgebra.CreateMatrix.Dense(genome.Size, genome.Size, (i, j) => open == null || open[i, j] ? deltaMode.Judge(context, genome, drules, jrules, target, i, j, -deltaMag) : double.NaN);

            return new DeltaTest(f0, deltaPlus, deltaMinus, open);
        }
    }
}
