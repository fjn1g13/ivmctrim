﻿using System;
using System.Collections.Generic;

namespace M4M
{
    public interface ISelector<T>
    {
        T SelectRandom();
    }
    public interface IRerandomableSelector<T> : ISelector<T>
    {
        void SetRandomSource(Random rnd);
    }

    public class ParetoSelector<T> : IRerandomableSelector<T>
    {
        public int Count => Options.Count;
        private List<T> Options { get; }
        private IList<double[]> Measures { get; }
        private Random Random;

        public ParetoSelector(Random rnd)
        {
            Random = rnd;
            Options = new List<T>();
            Measures = new List<double[]>();
        }

        public void Add(T option, double[] measures)
        {
            Options.Add(option);
            Measures.Add(measures);
        }

        public T SelectRandom()
        {
            int a = Random.Next(Count);
            int b = Random.Next(Count - 1);

            if (b >= a)
                b++;

            int comparison = Compare(Measures[a], Measures[b]);

            // a dominates
            if (comparison > 0)
                return Options[a];

            // b dominates
            if (comparison < 0)
                return Options[b];

            // random
            if (Random.NextDouble() >= 0.5)
                return Options[a];
            else
                return Options[b];
        }

        private int Compare(double[] a, double[] b)
        {
            bool aDominates = false;
            bool bDominates = false;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] > b[i])
                    aDominates = true;
                else if (b[i] > a[i])
                    bDominates = true;
            }

            if (aDominates && !bDominates)
                return 1;

            if (bDominates && !aDominates)
                return -1;

            return 0;
        }

        public void SetRandomSource(Random rnd)
        {
            Random = rnd;
        }
    }
    
    public class UniformSelector<T> : IRerandomableSelector<T>
    {
        public int Count => Options.Count;
        private readonly List<T> Options = new List<T>();
        private Random Random;

        public UniformSelector(Random rnd) : this(rnd, 256)
        {
        }

        public UniformSelector(Random rnd, int initialCapacity)
        {
            if (initialCapacity < 1)
                throw new ArgumentException("Must be greater than 0", "initialCapactity");

            Options = new List<T>(initialCapacity);
            Random = rnd;
        }

        public void Add(T option)
        {
            Options.Add(option);
        }

        public T SelectRandom()
        {
            if (Count == 0)
                throw new Exception("No items!");

            return Options[Random.Next(Count)];
        }

        public void SetRandomSource(Random rnd)
        {
            Random = rnd;
        }
    }

    /// <summary>
    /// Pretty fast and maybe reliable
    /// </summary>
    public class WeightedSelectorMk2<T> : IRerandomableSelector<T>
	{
		public int Count { get; private set; }
		private T[] Options;
		private double[] BaseWeights;
		private double TotalWeight;
		private Random Random;
        
		public WeightedSelectorMk2(Random rnd) : this(rnd, 256)
		{
		}

		public WeightedSelectorMk2(Random rnd, int initialCapacity)
		{
			if (initialCapacity < 1)
				throw new ArgumentException("Must be greater than 0", "initialCapactity");
			
			Count = 0;
			Options = new T[initialCapacity];
			BaseWeights = new double[initialCapacity];
			TotalWeight = 0.0;
			Random = rnd;
		}

		public void Add(T option, double weight)
		{
			if (Count + 1 >= BaseWeights.Length)
			{
				// resize
				T[] noarr = new T[Options.Length * 2];
				Array.Copy(Options, noarr, Count);
				Options = noarr;
				
				double[] nbwarr = new double[BaseWeights.Length * 2];
				Array.Copy(BaseWeights, nbwarr, Count);
				BaseWeights = nbwarr;
			}
			
			Options[Count] = option;
			TotalWeight += weight;
			BaseWeights[Count] = TotalWeight;
			Count++;
		}

		public T SelectRandom()
		{
			if (Count == 0)
				throw new Exception("No items!");

			double v = Random.NextDouble() * TotalWeight;
			int k = Array.BinarySearch(BaseWeights, 0, Count, v);
			
			if (k < 0)
				k = ~k;
			
			return Options[k];
		}
		
        public void SetRandomSource(Random rnd)
        {
            Random = rnd;
        }
    }
}
