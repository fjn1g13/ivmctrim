﻿using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M4M
{
    /// <summary>
    /// Something that configures a DensePopulationExperimentFeedback
    /// </summary>
    /// <param name="sensePopulationExperimentFeedback">The DensePopulationExperimentFeedback to configure</param>
    public delegate void DensePopulationExperimentFeedbackConfigurator(IDensePopulationExperimentFeedback densePopulationExperimentFeedback);
    
    /// <summary>
    /// Something that creates a DensePopulationExperimentFeedback
    /// </summary>
    /// <param name="rand">A random source</param>
    /// <param name="populationExperimentConfig">The experiment configuration being run</param>
    /// <param name="feedback">Optional: null means 'provide your own'</param>
    public delegate IDensePopulationExperimentFeedback DensePopulationExperimentFeedbackFactory(RandomSource rand, PopulationExperimentConfig<DenseIndividual> populationExperimentConfig, PopulationExperimentFeedback<DenseIndividual> feedback);

    public interface IDensePopulationExperimentFeedback
    {
        PopulationExperimentConfig<DenseIndividual> PopulationConfig { get; }

        /// <summary>
        /// The PopulationExperimentFeedback this instance uses
        /// </summary>
        PopulationExperimentFeedback<DenseIndividual> Feedback { get; }
        
        IReadOnlyList<IndividualJudgement<DenseIndividual>> BestSamples { get; }
        IReadOnlyList<int> SampleEpochs { get; }

        int SamplePeriod { get; }
    }
}
