﻿using M4M.State;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M.LotkaVolterra
{
    /// <summary>
    /// LotkaVolterra Spinner.
    /// Performs lots of relaxation steps, rather than mutating and developing
    /// It's a total cludge, but who cares.
    /// </summary>
    [State.StateClass]
    public class LotkaVolterraSpinner : IPopulationSpinner<DenseIndividual>
    {
        [Obsolete]
        protected LotkaVolterraSpinner()
        { }

        public LotkaVolterraSpinner(double growthRate)
        {
            GrowthRate = growthRate;
        }

        public string Name => "LotkaVolterraSpinner";

        public string Description => $"{Name} (GrowthReate={GrowthRate})";

        [SimpleStateProperty("GrowthReate")]
        public double GrowthRate { get; private set; }

        public IReadOnlyList<IndividualJudgement<DenseIndividual>> SpinPopulation(Population<DenseIndividual> population, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<DenseIndividual> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<DenseIndividual>>> judgementFeedback, int eliteCount, bool hillclimberMode)
        {
            MathNet.Numerics.LinearAlgebra.Vector<double> update = null;
            MathNet.Numerics.LinearAlgebra.Vector<double> update2 = null;
            int count = population.Count;

            var individuals = population.ExtractAll();
            var judgements = new IndividualJudgement<DenseIndividual>[individuals.Length];

            for (int generation = 0; generation < generations; generation++)
            {
                target.NextGeneration(context.Rand, jrules);

                if (generation == 0 && judgementFeedback != null)
                {
                    for (int i = 0; i < individuals.Length; i++)
                    {
                        var individual = individuals[i].Clone(context);
                        individual.DevelopInplace(context, drules);
                        judgements[i] = new IndividualJudgement<DenseIndividual>(individual, individual.Judge(jrules, target));
                    }

                    judgementFeedback.Invoke(0, judgements.ToArray());
                }

                for (int i = 0; i < individuals.Length; i++)
                {
                    Step(individuals[i].Genome, ref update, ref update2, GrowthRate);

                    if (judgementFeedback != null || generation == generations - 1)
                    {
                        individuals[i].DevelopInplace(context, drules);
                        var individual = individuals[i].Clone(context);
                        judgements[i] = new IndividualJudgement<DenseIndividual>(individual, individual.Judge(jrules, target));
                    }
                }

                judgementFeedback?.Invoke(generation + 1, judgements.ToArray());
            }

            population.IntroduceMany(individuals);

            return judgements;
        }

        /// <summary>
        /// Steps the initial state of the given genome.
        /// </summary>
        public static void Step(DenseGenome genome, ref MathNet.Numerics.LinearAlgebra.Vector<double> updateLeft, ref MathNet.Numerics.LinearAlgebra.Vector<double> updateRight, double growthRate)
        {
            if (updateLeft == null || updateLeft.Count != genome.Size)
            {
                updateLeft = MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(genome.Size);
            }

            if (updateRight == null || updateRight.Count != genome.Size)
            {
                updateRight = MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(genome.Size);
            }

            var state = genome.InitialState;
            var transMat = genome.TransMat;
            var bias = genome.BiasVector;

            transMat.Multiply(state, updateRight); // r = Wx
            updateRight.Add(bias, updateRight); // r = k+Wx

            state.Multiply(growthRate, updateLeft); // l = mx
            updateLeft.PointwiseDivide(bias, updateLeft); // l = mx/k

            updateLeft.PointwiseMultiply(updateRight, updateRight); // lr = (mx/k)(k+Wx)
            state.Add(updateRight, state);
        }
    }
}
