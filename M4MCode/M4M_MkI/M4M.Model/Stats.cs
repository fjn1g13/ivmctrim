﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M4M
{
    public class Label
    {
        public Label(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public string Name { get; set; }
    }

    /// <summary>
    /// Counts the number of elements in an enumerable while enumerating it.
    /// </summary>
    public class Counter
    {
        private int _total = 0;
        public int Total => _total;

        public IEnumerable<T> Count<T>(IEnumerable<T> enumerable)
        {
            foreach (var t in enumerable)
            {
                // why on earth is this interlocked? enumerables are not meant to be thread safe
                System.Threading.Interlocked.Increment(ref _total);
                yield return t;
            }
        }
    }
    
    public class BasicStats
    {
        public BasicStats(int count, double mean, double stdDev)
        {
            Count = count;
            Mean = mean;
            StdDev = stdDev;
        }

        public int Count;
        public double Mean { get; }
        public double StdDev { get; }
            
        public static BasicStats Measure(IEnumerable<double> samples)
        {
            var counter = new Counter();
                
            var stats = MathNet.Numerics.Statistics.Statistics.MeanStandardDeviation(counter.Count(samples));
            int count = counter.Total;
            var μ = stats.Item1;
            var σ = stats.Item2;

            return new BasicStats(count, μ, σ);
        }
    }

    // consider making these structs
    public class Labelled<TItem, TLabel> : IComparable<Labelled<TItem, TLabel>> where TItem : IComparable<TItem>
    {
        public Labelled(TItem item, TLabel label)
        {
            Item = item;
            Label = label;
        }

        public TItem Item { get; }
        public TLabel Label { get; }

        public int CompareTo(Labelled<TItem, TLabel> other)
        {
            return Item.CompareTo(other.Item);
        }
    }

    public class Ranked<T> where T : IComparable<T>
    {
        public Ranked(T item, double rank)
        {
            Item = item;
            Rank = rank;
        }

        public T Item { get; }
        public double Rank { get; }
    }

    public enum TestType
    {
        LowerTailed,
        TwoTailed,
        UpperTailed,
    }

    public static class Stats
    {
        public static double UnpairedTScore(BasicStats sa, BasicStats sb)
        {
            var diff = sa.Mean - sb.Mean;

            var sea = (sa.StdDev * sa.StdDev) / sa.Count;
            var seb = (sb.StdDev * sb.StdDev) / sb.Count;
            var se = Math.Sqrt(sea + seb);

            return diff / se;
        }

        public static double Sqr(double x) => x * x;
        public static double Sqr(int x) => x * x;
        public static double Cube(double x) => x * x * x;
        public static double Cube(int x) => x * x * x;

        public static double UnpairedDegreesFreedom(BasicStats sa, BasicStats sb)
        {
            double dfn = Sqr(Sqr(sa.StdDev) / sa.Count + Sqr(sb.StdDev) / sb.Count);
            double dfda = Sqr(Sqr(sa.StdDev) / sa.Count) / (sa.Count - 1);
            double dfdb = Sqr(Sqr(sb.StdDev) / sb.Count) / (sb.Count - 1);
            double dfd = dfda + dfdb;

            double df = dfn / dfd;

            return df;
        }

        public static double UnpairedPValue(BasicStats sa, BasicStats sb)
        {
            double tscore = UnpairedTScore(sa, sb);
            double df = UnpairedDegreesFreedom(sa, sb);

            double p = MathNet.Numerics.Distributions.StudentT.CDF(0, 1.0, df, tscore);

            return p;
        }

        public static double UnpairedPValue(IEnumerable<double> a, IEnumerable<double> b)
        {
            // should really to check that a and b are normally distributed

            var sa = BasicStats.Measure(a);
            var sb = BasicStats.Measure(b);

            return UnpairedPValue(sa, sb);
        }

        public static IEnumerable<Ranked<T>> Rank<T>(IEnumerable<T> seq) where T : IComparable<T>
        {
            var sorted = seq.OrderBy(x => x);

            bool first = true;
            int position = 0;

            List<T> currents = new List<T>();

            foreach (var s in sorted)
            {
                if (first)
                {
                    currents.Add(s);
                    first = false;
                }
                else
                {
                    if (s.CompareTo(currents.Last()) > 0)
                    {
                        // yield the currents (with rank)
                        double rank = position - (currents.Count - 1) / 2.0;
                        foreach (var c in currents)
                        {
                            yield return new Ranked<T>(c, rank);
                        }
                        currents.Clear();
                    }

                    currents.Add(s);
                }

                position++;
            }

            // end
            {
                // yield the currents (with rank)
                double rank = position - (currents.Count - 1) / 2.0;
                foreach (var c in currents)
                {
                    yield return new Ranked<T>(c, rank);
                }
            }
        }

        public static IEnumerable<Labelled<TItem, TLabel>> Label<TItem, TLabel>(IEnumerable<TItem> items, TLabel label) where TItem : IComparable<TItem>
        {
            return items.Select(item => new Labelled<TItem, TLabel>(item, label));
        }

        public static IEnumerable<Labelled<TItem, TLabel>> Label<TItem, TLabel>(IEnumerable<TItem> items, Func<TItem, TLabel> labeller) where TItem : IComparable<TItem>
        {
            return items.Select(item => new Labelled<TItem, TLabel>(item, labeller(item)));
        }

        public static double ZTestStatisticToPValue(double z, TestType testType)
        {
            var normal = new MathNet.Numerics.Distributions.Normal();

            switch (testType)
            {
                case TestType.LowerTailed:
                    return normal.CumulativeDistribution(z);
                case TestType.TwoTailed:
                    return 2.0 * normal.CumulativeDistribution(-Math.Abs(z));
                case TestType.UpperTailed:
                    return normal.CumulativeDistribution(-z);
                default:
                    throw new ArgumentException(nameof(testType), $"Unrecognised TestType {testType}");
            }
        }

        /// <summary>
        /// Computes the Z-statistic continuity correction component.
        /// </summary>
        /// <param name="offset">The different between the sample and the distribution's mean.</param>
        /// <param name="testType">The type of Z-Test</param>
        public static double MannWhitneyUCorrection(double offset, TestType testType)
        {
            // In each case, we return something of magnitude 0.5 which pushes the Z values toward zero.
            // This has the effect of increasing the P-value.
            // In the two-tailed case, we ensure the Z values doesn't pass 0.

            switch (testType)
            {
                case TestType.LowerTailed:
                    return 0.5;
                case TestType.TwoTailed:
                    return -Math.Sign(offset) * Math.Min(0.5, Math.Abs(offset)); // don't pass 0: beyond zero it represents a nonsensical reduction in P-value
                case TestType.UpperTailed:
                    return -0.5;
                default:
                    throw new ArgumentException(nameof(testType), "Unrecognised TestType");
            }
        }

        /// <summary>
        /// Computes the Mann-Whitney U test statistic for the two independent distributions.
        /// The Z and U values are those of the first distribution, <paramref name="a"/>.
        /// </summary>
        /// <typeparam name="T">The type of elements in the distributions. Must provide a total ordering.</typeparam>
        /// <param name="a">The first distribution.</param>
        /// <param name="b">The second distribution.</param>
        /// <param name="testType">The type of Z-Test to perform.</param>
        /// <param name="u">The U-statistic.</param>
        /// <returns>The continuity corrected Z-statistic.</returns>
        public static double MannWhitneyU<T>(IEnumerable<T> a, IEnumerable<T> b, TestType testType, out double u) where T : IComparable<T>
        {
            // Uses normal approximation; not found a nice source to explain exact yet.

            var alabel = new Label("a");
            var blanel = new Label("b");

            var n1counter = new Counter();
            var n2counter = new Counter();

            var al = Label(n1counter.Count(a), alabel);
            var bl = Label(n2counter.Count(b), blanel);

            var ranked = Rank(al.Concat(bl)).ToArray();
            var grouped = ranked.GroupBy(r => r.Rank);

            var r1 = grouped.SelectMany(g => g).Where(e => e.Item.Label == alabel).Sum(e => e.Rank);

            var n1 = n1counter.Total;
            var n2 = n2counter.Total;
            var n = n1 + n2;

            if (n1 == 0 || n2 == 0)
                throw new ArgumentException("One or both sequences contained no elements.");

            var u1 = r1 - n1 * (n1 + 1) / 2.0;
            // don't bother computing u2 (or r2)
            u = u1;

            var m = (n1 * n2) / 2.0;
            var c = MannWhitneyUCorrection(u - m, testType);

            var tieAdjustment = grouped.Sum(g => Cube(g.Count()) - g.Count()) / (n * (n - 1));
            var σ = Math.Sqrt((n1 * n2 / 12.0) * ((n + 1) - tieAdjustment));

            var z = (u - m + c) / σ;

            return z;
        }

        /// <summary>
        /// Computes the Mann-Whitney U test results for the two independent distributions using a normal-distribution approximation for the p value.
        /// The results are those for the first distribution, <paramref name="a"/>.
        /// </summary>
        /// <typeparam name="T">The type of elements in the distributions. Must provide a total ordering.</typeparam>
        /// <param name="a">The first distribution.</param>
        /// <param name="b">The second distribution.</param>
        /// <param name="testType">The type of Z-Test to perform.</param>
        /// <returns>The test results.</returns>
        public static MannWhitneyUTestResult MannWhitneyU<T>(IEnumerable<T> a, IEnumerable<T> b, TestType testType) where T : IComparable<T>
        {
            var z = MannWhitneyU<T>(a, b, testType, out var u);
            var p = ZTestStatisticToPValue(z, testType);
            return new MannWhitneyUTestResult(testType, z, u, p);
        }
    }

    public class MannWhitneyUTestResult
    {
        public MannWhitneyUTestResult(TestType testType, double z, double u, double p)
        {
            TestType = testType;
            Z = z;
            U = u;
            P = p;
        }

        /// <summary>
        /// The type of Z-Test performed.
        /// </summary>
        public TestType TestType { get; }

        /// <summary>
        /// The Z test-statistic.
        /// </summary>
        public double Z { get; }

        /// <summary>
        /// The U test-statistic.
        /// </summary>
        public double U { get; }

        /// <summary>
        /// The P-value.
        /// </summary>
        public double P { get; }

        public override string ToString()
        {
            return $"Mann-Whitney-U {this.TestType}: Z={this.Z}, U={this.U}, P={this.P}";
        }
    }
}