﻿using M4M.State;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace M4M
{
    public interface IUnknownWholeSamplesTypeReceiver<TResult>
    {
        TResult Receive<T>(List<WholeSample<T>> wholeSamples) where T : IIndividual<T>;
    }

    public static class WholeSamplesHelpers
    {
        public static TResult LoadUnknownType<TResult>(string fileName, IUnknownWholeSamplesTypeReceiver<TResult> receiver)
        {
            using (var fs = System.IO.File.Open(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
            {
                object untyped = GraphSerialisation.Read<object>(fs); // we assume this is a List<WholeSamples<T>>
                
                 // find out T
                Type individualType = untyped.GetType().GetGenericArguments()[0].GetGenericArguments()[0];

                // type gap
                return NetState.AutoState.AutoSerialisationHelpers.CallNamedStatic<TResult>(typeof(WholeSamplesHelpers), null, nameof(CallbackReceiver), new[] { individualType, typeof(TResult) }, untyped, receiver);
            }
        }

        private static TResult CallbackReceiver<T, TResult>(List<WholeSample<T>> WholeSamples, IUnknownWholeSamplesTypeReceiver<TResult> receiver) where T : IIndividual<T>
        {
            return receiver.Receive(WholeSamples);
        }
    }

    public class WholeSamplesManipulatorReceiver : IUnknownWholeSamplesTypeReceiver<IWholeSamplesManipulator>
    {
        public IWholeSamplesManipulator Receive<T>(List<WholeSample<T>> wholeSamples) where T : IIndividual<T>
        {
            return new WholeSamplesExtractor<T>(wholeSamples);
        }
    }

    public interface IWholeSamplesManipulator
    {
        /// <summary>
        /// Appends samples loaded from a file
        /// </summary>
        void AppendSamples(string filename, int epochOffset, long generationOffset, bool skipFirst);

        /// <summary>
        /// Appends samples loaded from multiple files (in order)
        /// </summary>
        void AppendSamples(IEnumerable<string> filenames, bool resequence, bool skipFirst);

        /// <summary>
        /// Saves the samples to a file
        /// </summary>
        void SaveSamples(string outFilename);

        /// <summary>
        /// The number of samples
        /// </summary>
        int Count { get; }
    }

    // this is a pretty tiresome type
    public class WholeSamplesExtractor<T> : IWholeSamplesManipulator where T : IIndividual<T>
    {
        public WholeSamplesExtractor(List<WholeSample<T>> wholeSamples)
        {
            WholeSamples = wholeSamples;
        }

        private List<WholeSample<T>> WholeSamples;
        
        public int Count => WholeSamples.Count;

        public void AppendSamples(string filename, int epochOffset, long generationOffset, bool skipFirst)
        {
            var moreSamples = WholeSample<T>.LoadWholeSamples(filename);

            if (skipFirst)
            {
                moreSamples.RemoveAt(0);
            }

            foreach (var ws in moreSamples)
            {
                ws.Retime(ws.Epoch + epochOffset, ws.Generations + generationOffset);
            }

            WholeSamples.AddRange(moreSamples);
        }

        public void AppendSamples(IEnumerable<string> filenames, bool resequence, bool skipFirst)
        {
            foreach (var filename in filenames)
            {
                int epochOffset = resequence ? WholeSamples.Last().Epoch : 0;
                long generationOffset = resequence ? WholeSamples.Last().Generations : 0;
                AppendSamples(filename, epochOffset, generationOffset, skipFirst);
            }
        }

        public void SaveSamples(string outFilename)
        {
            WholeSample<T>.SaveWholeSamples(outFilename, WholeSamples);
        }
    }

    [StateClass]
    public class WholeSample<T> where T : IIndividual<T>
    {
        [Obsolete]
        protected WholeSample()
        {
        }

        public WholeSample(long generations, int epoch, ITarget target, IReadOnlyList<IndividualJudgement<T>> judgements)
        {
            Generations = generations;
            Epoch = epoch;
            Target = target;
            Judgements = judgements;
        }

        // provided for backward compatibility
        [Obsolete]
        [DeprecatedSimpleStateProperty("Generations")]
        private int _generationsShort
        {
            get
            {
                throw new InvalidOperationException("Property _generationsShort is deprecated, and cannot be used for serialisation");
            }
            set
            {
                Generations = value;
            }
        }

        [SimpleStateProperty("GenerationsLong")]
        public long Generations { get; private set; }

        [SimpleStateProperty("Epoch")]
        public int Epoch { get; private set; }
        
        [SimpleStateProperty("Target")]
        public ITarget Target { get; private set; }
        
        [SimpleStateProperty("Judgements")]
        public IReadOnlyList<IndividualJudgement<T>> Judgements { get; private set; }

        public void Retime(int epoch, long generations)
        {
            Epoch = epoch;
            Generations = generations;
        }
        
        public static void SaveWholeSamples(string filename, List<WholeSample<T>> wholeSamples)
        {
            Misc.EnsureDirectory(System.IO.Path.GetDirectoryName(filename));
            using (var fs = System.IO.File.OpenWrite(filename))
            {
                GraphSerialisation.Write(wholeSamples, fs);
            }
        }

        public static List<WholeSample<T>> LoadWholeSamples(string filename)
        {
            using (var fs = System.IO.File.Open(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
            {
                var wholeSamples = GraphSerialisation.Read<List<WholeSample<T>>>(fs);
                return wholeSamples;
            }
        }

        public static IEnumerable<List<WholeSample<T>>> EnumerateWholeSamplesSeries(IReadOnlyList<string> filenames)
        {
            foreach (var filename in filenames)
            {
                var wholeSamples = LoadWholeSamples(filename);
                yield return wholeSamples;
            }
        }

        public static IEnumerable<WholeSample<T>> EnumerateWholeSamplesSeries2(IReadOnlyList<string> filenames)
        {
            return EnumerateWholeSamplesSeries(filenames).SelectMany(lws => lws);
        }
    }

    [StateClass]
    public class TraceInfo<T> where T : IIndividual<T>
    {
        [Obsolete]
        protected TraceInfo()
        {
        }

        public TraceInfo(List<WholeSample<T>> traceWholeSamples)
        {
            TraceWholeSamples = traceWholeSamples;
        }

        [SimpleStateProperty("TraceWholeSamples")]
        public List<WholeSample<T>> TraceWholeSamples { get; private set; }

        public TraceInfo<T> ExtractGenerations(long start, long end)
        {
            return Filter(ws => ws.Generations >= start && ws.Generations <= end);
        }

        public TraceInfo<T> ExtractEpochs(int start, int end)
        {
            return Filter(ws => ws.Epoch >= start && ws.Epoch <= end);
        }

        public TraceInfo<T> Filter(Func<WholeSample<T>, bool> filter)
        {
            var wholeSamples = TraceWholeSamples.Where(filter).ToList();
            return new TraceInfo<T>(wholeSamples);
        }

        /// <summary>
        /// Drops epoch boundaries (i.e. samples which share a generation, but not an epoch
        /// Useful for rough downsamplings
        /// Default of dropFirst is equivalent to the old behaviour of not bothering to record the first generation of each epoch
        /// </summary>
        public TraceInfo<T> DropEpochBoundaries(bool dropFirst = true)
        {
            List<WholeSample<T>> wholeSamples = new List<WholeSample<T>>();

            WholeSample<T> last = null;

            for (int i = 0; i < TraceWholeSamples.Count; i++)
            {
                var cur = TraceWholeSamples[i];

                if (last == null)
                {
                    last = cur;
                }
                else if (last.Epoch != cur.Epoch && last.Generations == cur.Generations)
                {
                    if (dropFirst)
                    {
                        wholeSamples.Add(last);
                        last = null;
                    }
                    else
                    {
                        last = cur;
                    }
                }
                else
                {
                    wholeSamples.Add(last);
                    last = cur;
                }
            }

            return new TraceInfo<T>(wholeSamples);
        }

        /// <summary>
        /// Performs a trivial downsampling
        /// </summary>
        public TraceInfo<T> DownSample(int samplePeriod, bool keepLast = true)
        {
            List<WholeSample<T>> wholeSamples = new List<WholeSample<T>>();

            int end = keepLast
                ? TraceWholeSamples.Count - 1 // ignore last in loop (we add it explictly later)
                : TraceWholeSamples.Count;

            for (int i = 0; i < end; i += samplePeriod)
            {
                wholeSamples.Add(TraceWholeSamples[i]);
            }

            if (keepLast)
            {
                wholeSamples.Add(TraceWholeSamples.Last());
            }

            return new TraceInfo<T>(wholeSamples);
        }

        public void RetimeInplace(int startEpoch, long startGeneration)
        {
            var ws1 = TraceWholeSamples[0];

            int epochOffset = startEpoch - ws1.Epoch;
            long generationOffset = startGeneration - ws1.Generations;

            OffsetInplace(epochOffset, generationOffset);
        }
        
        public void OffsetInplace(int epochOffset, long generationOffset)
        {
            foreach (var ws in TraceWholeSamples)
            {
                ws.Retime(ws.Epoch + epochOffset, ws.Generations + generationOffset);
            }
        }

        public void Save(string filename)
        {
            using (var fs = System.IO.File.Open(filename, System.IO.FileMode.Create))
            {
                GraphSerialisation.Write(this, fs);
            }
        }

        public static TraceInfo<T> Load(string filename)
        {
            using (var fs = System.IO.File.Open(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
            {
                return GraphSerialisation.Read<TraceInfo<T>>(fs);
            }
        }
    }

    public class PopulationTraceRecorder<T> where T : IIndividual<T>
    {
        public List<WholeSample<T>> TraceWholeSamples = new List<WholeSample<T>>();

        public PopulationJudgementFeedbackDelegate<T> Recorder { get; }

        public int SamplePeriod { get; }

        public PopulationTraceRecorder(int samplePeriod)
        {
            Recorder = Record;
            SamplePeriod = samplePeriod;
        }

        private void Record(FileStuff stuff, IReadOnlyList<IndividualJudgement<T>> populationJudgements, int epochCount, long generationCount, ITarget target)
        {
            if (generationCount % SamplePeriod == 0)
            {
                TraceWholeSamples.Add(new WholeSample<T>(generationCount, epochCount, target, populationJudgements));
            }
        }
    }

    public interface ITraceWatcher<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        void StartTrace();
        void EndTrace();
        void StartTarget(long generation, ITarget target, Population<TIndividual> population);
        void Judged(long generation, ITarget target, IReadOnlyList<IndividualJudgement<TIndividual>> individualJudgement);
    }

    public class TrajectoryTraceWatcher<TIndividual> : ITraceWatcher<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        public TrajectoryTraceWatcher(Func<ITarget, IReadOnlyList<IndividualJudgement<TIndividual>>, double>[] samplers)
        {
            Samplers = samplers;
            AllTrajectories = samplers.Select(s => new List<double[]>()).ToArray();
        }

        public Func<ITarget, IReadOnlyList<IndividualJudgement<TIndividual>>, double>[] Samplers { get; }
        private List<double[]>[] AllTrajectories { get; }
        private List<double>[] Current;

        public double[][] GetTrajectories(int samplerIndex)
        {
            return AllTrajectories[samplerIndex].ToArray();
        }

        public void EndTrace()
        {
            for (int i = 0; i < Samplers.Length; i++)
            {
                AllTrajectories[i].Add(Current[i].ToArray());
            }
            Current = null;
        }

        public void Judged(long generation, ITarget target, IReadOnlyList<IndividualJudgement<TIndividual>> individualJudgements)
        {
            for (int i = 0; i < Samplers.Length; i++)
            {
                Current[i].Add(Samplers[i](target, individualJudgements));
            }
        }

        public void StartTarget(long generation, ITarget target, Population<TIndividual> population)
        {
            // nix
        }

        public void StartTrace()
        {
            Current = Misc.Create(Samplers.Length, i => new List<double>());
        }
    }

    public class PopulationTrace
    {
        /// <summary>
        /// Runs a tracee over a population; you probably want the other overload which return a <see cref="TraceInfo{T}"/>.
        /// Basically, it does the same thing as RunEpochs, but without a PopulationExperiment (indeed, should produce exactly the same results; if not, something is wrong).
        /// </summary>
        public static void RunTrace<T>(TextWriter console, ModelExecutionContext context, FileStuff fileStuff, Population<T> population, PopulationExperimentConfig<T> popConfig, ITarget[] targets, PopulationJudgementFeedbackDelegate<T> feedback, EndPopulationTargetDelegate<T> endTargetFeedback, int startEpoch, int epochs) where T : IIndividual<T>
        {
            var populationSpinner = popConfig.CustomPopulationSpinner ?? DefaultPopulationSpinner<T>.Instance;

            int generation = 0;
            for (int i = 0; i < epochs; i++)
            {
                int epoch = startEpoch + i;

                foreach (var target in targets)
                {
                    ExposureInformation exposureInformation = new ExposureInformation(popConfig.ExperimentConfiguration.GenerationsPerTargetPerEpoch);
                    target.NextExposure(context.Rand, popConfig.ExperimentConfiguration.JudgementRules, epoch, ref exposureInformation);

                    if (context.Rand.NextDouble() < popConfig.ExperimentConfiguration.InitialStateResetProbability)
                        popConfig.PopulationResetOperation?.Reset(context, population, popConfig.ExperimentConfiguration.InitialStateResetRange, popConfig.ExperimentConfiguration.DevelopmentRules, popConfig.ExperimentConfiguration.ReproductionRules, target);

                    if (exposureInformation.ExposureDuration < 0)
                        continue; // allow zero through as a special case; < 0 means skip altogether

                    Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementFeedback = (g, ijs) =>
                    {
                        feedback?.Invoke(fileStuff, ijs, i, generation + g, target);
                    };

                    var terminalIjs = populationSpinner.SpinPopulation(population, context, popConfig.ExperimentConfiguration.ReproductionRules, popConfig.ExperimentConfiguration.DevelopmentRules, popConfig.ExperimentConfiguration.JudgementRules, target, popConfig.SelectorPreparer, exposureInformation.ExposureDuration, judgementFeedback, popConfig.EliteCount, popConfig.HillclimberMode);

                    popConfig.PopulationEndTargetOperation?.Process(context, population, popConfig.ExperimentConfiguration.DevelopmentRules, popConfig.ExperimentConfiguration.JudgementRules, target);

                    generation += exposureInformation.ExposureDuration;
                    
                    endTargetFeedback?.Invoke(fileStuff, population, target, epoch, generation, terminalIjs);
                }
            }
        }

        /// <summary>
        /// Runs a tracee over a population.
        /// Basically, it does the same thing as RunEpochs, but without a PopulationExperiment (indeed, should produce exactly the same results; if not, something is wrong).
        /// </summary>
        public static TraceInfo<T> RunTrace<T>(TextWriter console, ModelExecutionContext context, Population<T> population, PopulationExperimentConfig<T> popConfig, ITarget[] targets, int samplePeriod, int startEpoch, int epochs) where T : IIndividual<T>
        {
            PopulationTraceRecorder<T> recorder = new PopulationTraceRecorder<T>(samplePeriod);
            RunTrace(console, context, null, population, popConfig, targets, recorder.Recorder, null, startEpoch, epochs);
            return new TraceInfo<T>(recorder.TraceWholeSamples);
        }

        /// <summary>
        /// Runs multiples traces.
        /// </summary>
        public static TracesInfo RunTraces(TextWriter console, ModelExecutionContext context, Population<DenseIndividual> population, PopulationExperimentConfig<DenseIndividual> popConfig, ITarget[] targets, int count, int samplePeriod, int startEpoch, int epochs)
        {
            var rand = context.Rand;
            var pop = population;

            TracesRecorder tracesRecorder = TracesRecorder.FromFeedback(popConfig.ExperimentConfiguration.Size, samplePeriod, null);
            var populationSpinner = popConfig.CustomPopulationSpinner ?? DefaultPopulationSpinner<DenseIndividual>.Instance;

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            for (int i = 0; i < count; i++)
            {
                int generation = 0;
                for (int j = 0; j < epochs; j++)
                {
                    int epoch = startEpoch + j;

                    Action<int, IReadOnlyList<IndividualJudgement<DenseIndividual>>> judgementFeedback = (g, ijs) => tracesRecorder.Judged(g + generation, ijs);

                    population = pop.Clone();

                    if (sw.ElapsedMilliseconds > 30000)
                    {
                        console.WriteLine($"{i} / {count}");
                        sw.Restart();
                    }

                    tracesRecorder.StartTrace();
                    foreach (var target in targets)
                    {
                        ExposureInformation exposureInformation = new ExposureInformation(popConfig.ExperimentConfiguration.GenerationsPerTargetPerEpoch);
                        target.NextExposure(rand, popConfig.ExperimentConfiguration.JudgementRules, epoch, ref exposureInformation);

                        tracesRecorder.StartTarget(generation, target);

                        if (exposureInformation.ExposureDuration < 0)
                            continue; // allow zero through as a special case; < 0 means skip altogether

                        popConfig.PopulationResetOperation?.Reset(context, population, popConfig.ExperimentConfiguration.InitialStateResetRange, popConfig.ExperimentConfiguration.DevelopmentRules, popConfig.ExperimentConfiguration.ReproductionRules, target);
                        populationSpinner.SpinPopulation(population, context, popConfig.ExperimentConfiguration.ReproductionRules, popConfig.ExperimentConfiguration.DevelopmentRules, popConfig.ExperimentConfiguration.JudgementRules, target, popConfig.SelectorPreparer, exposureInformation.ExposureDuration, judgementFeedback, popConfig.EliteCount, popConfig.HillclimberMode);
                        popConfig.PopulationEndTargetOperation?.Process(context, population, popConfig.ExperimentConfiguration.DevelopmentRules, popConfig.ExperimentConfiguration.JudgementRules, target);

                        generation += exposureInformation.ExposureDuration;
                    }
                    tracesRecorder.EndTrace();
                }
            }

            return new TracesInfo(0, targets, samplePeriod, tracesRecorder.BestFitnesses, tracesRecorder.MeanFitnesses, tracesRecorder.BestInitialStates, tracesRecorder.BestTerminalStates, tracesRecorder.TargetSwitches);
        }

        /// <summary>
        /// Runs multiple traces, and returns the results as sampled trajectories.
        /// (Used by M4M_DED).
        /// </summary>
        public static double[][] TracesToTrajectories<TIndividual>(ModelExecutionContext context, PopulationExperiment<TIndividual> templateExp, IEnumerable<Population<TIndividual>> populations, Func<ITarget, IReadOnlyList<IndividualJudgement<TIndividual>>, double> sampler, int epochCount) where TIndividual : IIndividual<TIndividual>
        {
            var watcher = new TrajectoryTraceWatcher<TIndividual>(new [] { sampler });
            RunTraces(context, templateExp, populations, watcher, epochCount);
            return watcher.GetTrajectories(0);
        }

        /// <summary>
        /// Runs multiples traces.
        /// </summary>
        public static void RunTraces<TIndividual>(ModelExecutionContext context, PopulationExperiment<TIndividual> templateExp, IEnumerable<Population<TIndividual>> populations, ITraceWatcher<TIndividual> traceWatcher, int epochCount) where TIndividual : IIndividual<TIndividual>
        {
            foreach (var population in populations)
            {
                var traj = new List<double>();

                var exp = new PopulationExperiment<TIndividual>(population.Clone(context), templateExp.PopulationConfig, null, templateExp.Epoch, templateExp.TotalGenerationCount);

                void judged(FileStuff stuff, IReadOnlyList<IndividualJudgement<TIndividual>> ijs, int _epochCount, long generationCount, ITarget target)
                {
                    traceWatcher.Judged(generationCount, target, ijs);
                }

                void startTarget(FileStuff stuff, Population<TIndividual> _population, ITarget target, int epoch, long generationCount)
                {
                    traceWatcher.StartTarget(generationCount, target, _population);
                }

                var feedback = new PopulationExperimentFeedback<TIndividual>();
                feedback.StartTarget.Register(startTarget);
                feedback.Judged.Register(judged);

                // run trace
                traceWatcher.StartTrace();
                for (int i = 0; i < epochCount; i++)
                    exp.RunEpoch(context, feedback);
                traceWatcher.EndTrace();
            }
        }
    }

    [StateClass]
    public class TraceTargetSwitch
    {
        public TraceTargetSwitch(long generation, ITarget target)
        {
            Generation = generation;
            Target = target;
        }

        [Obsolete]
        protected TraceTargetSwitch()
        {
        }
        
        // provided for backward compatibility
        [Obsolete]
        [DeprecatedSimpleStateProperty("Generation")]
        private int _generationShort
        {
            get
            {
                throw new InvalidOperationException("Property _generationShort is deprecated, and cannot be used for serialisation");
            }
            set
            {
                Generation = value;
            }
        }

        [SimpleStateProperty("GenerationLong")]
        public long Generation { get; private set; }

        [SimpleStateProperty("Target")]
        public ITarget Target { get; private set; }
    }

    [StateClass]
    public class TracesInfo
    {
        [Obsolete]
        protected TracesInfo()
        {
        }

        public TracesInfo(long startGeneration, IReadOnlyList<ITarget> targets, int samplePeriod, IReadOnlyList<double[]> bestFitnesses, IReadOnlyList<double[]> meanFitnesses, IReadOnlyList<double[][]> bestInitialStates, IReadOnlyList<double[][]> bestTerminalStates, TraceTargetSwitch[] targetSwitches)
        {
            StartGeneration = startGeneration;
            Targets = targets;
            SamplePeriod = samplePeriod;
            BestFitnesses = bestFitnesses;
            MeanFitnesses = meanFitnesses;
            BestInitialStates = bestInitialStates;
            BestTerminalStates = bestTerminalStates;
            TargetSwitches = targetSwitches;
        }
        
        // provided for backward compatibility
        [Obsolete]
        [DeprecatedSimpleStateProperty("StartGeneration")]
        private int _startGeneration
        {
            get
            {
                throw new InvalidOperationException("Property _startGeneration is deprecated, and cannot be used for serialisation");
            }
            set
            {
                StartGeneration = value;
            }
        }

        [SimpleStateProperty("StartGenerationLong")]
        public long StartGeneration { get; private set; }
        
        public long EndGeneration => StartGeneration + BestFitnesses[0].Length * SamplePeriod;

        [SimpleStateProperty("Targets")]
        public IReadOnlyList<ITarget> Targets { get; private set; }

        [SimpleStateProperty("SamplePeriod")]
        public int SamplePeriod { get; private set; }

        [SimpleStateProperty("BestFitnesses")]
        public IReadOnlyList<double[]> BestFitnesses { get; private set; }

        [SimpleStateProperty("MeanFitnesses")]
        public IReadOnlyList<double[]> MeanFitnesses { get; private set; }

        [SimpleStateProperty("BestInitialStates")]
        public IReadOnlyList<double[][]> BestInitialStates { get; private set; }

        [SimpleStateProperty("BestTerminalStates")]
        public IReadOnlyList<double[][]> BestTerminalStates { get; private set; }

        [SimpleStateProperty("TargetSwitches")]
        public TraceTargetSwitch[] TargetSwitches { get; private set; }

        public void Save(string filename)
        {
            using (var fs = System.IO.File.Open(filename, System.IO.FileMode.Create))
            {
                GraphSerialisation.Write(this, fs);
            }
        }

        public static TracesInfo Load(string filename)
        {
            using (var fs = System.IO.File.Open(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
            {
                return GraphSerialisation.Read<TracesInfo>(fs);
            }
        }

        public TracesInfo Extract(long start, long end)
        {
            var tses = TargetSwitches.Where(ts => ts.Generation >= start && ts.Generation < end).ToArray();

            start -= StartGeneration;
            end -= StartGeneration;
            
            int skip = (int)(start / SamplePeriod);
            int take = (int)Math.Min((end + 1 - start) / SamplePeriod, BestFitnesses[0].Length - skip);

            return new TracesInfo(skip * SamplePeriod + StartGeneration, Targets, SamplePeriod,
                BestFitnesses.Select(s => s.Skip(skip).Take(take).ToArray()).ToArray(),
                MeanFitnesses.Select(s => s.Skip(skip).Take(take).ToArray()).ToArray(),
                BestInitialStates.Select(s => s.Select(t => t.Skip(skip).Take(take).ToArray()).ToArray()).ToArray(),
                BestTerminalStates.Select(s => s.Select(t => t.Skip(skip).Take(take).ToArray()).ToArray()).ToArray(),
                tses
                );
        }

        public double[] ExtractMeanSamples(long generation)
        {
            int skip = (int)(generation / SamplePeriod);
            return MeanFitnesses.Select(s => s.Skip(skip).First()).ToArray();
        }

        public double[] ExtractBestSamples(long generation)
        {
            int skip = (int)(generation / SamplePeriod);
            return BestFitnesses.Select(s => s.Skip(skip).First()).ToArray();
        }
    }

    public class TracesRecorder : ITraceWatcher<DenseIndividual>
    {
        // uses feedback
        private TracesRecorder(int size, int samplePeriod, PopulationExperimentFeedback<DenseIndividual> feedback)
        {
            Size = size;
            SamplePeriod = samplePeriod;
            Feedback = feedback ?? new PopulationExperimentFeedback<DenseIndividual>();

            Feedback.Judged.Register(Judged);
            Feedback.Started.Register(Started);
            Feedback.Finished.Register(Finished);
            Feedback.StartTarget.Register(TargetStarted);
        }

        // expects ITraceWatcher
        private TracesRecorder(int size, int samplePeriod)
        {
            Size = size;
            SamplePeriod = samplePeriod;
            Feedback = null;
        }

        public static TracesRecorder FromFeedback(int size, int samplePeriod, PopulationExperimentFeedback<DenseIndividual> feedback)
        {
            return new TracesRecorder(size, samplePeriod, feedback);
        }

        public static TracesRecorder AsTraceWatcher(int size, int samplePeriod)
        {
            return new TracesRecorder(size, samplePeriod);
        }

        public List<double[]> BestFitnesses { get; } = new List<double[]>();
        public List<double[]> MeanFitnesses { get; } = new List<double[]>();
        public List<double[][]> BestInitialStates { get; } = new List<double[][]>();
        public List<double[][]> BestTerminalStates { get; } = new List<double[][]>();
        public TraceTargetSwitch[] TargetSwitches { get; private set; } = null;

        private List<double> BestFitness;
        private List<double> MeanFitness;
        private List<double>[] BestInitialState;
        private List<double>[] BestTerminalState;
        private List<TraceTargetSwitch> TargetSwitchList;

        public int SamplePeriod { get; }
        public int Size { get; }
        public PopulationExperimentFeedback<DenseIndividual> Feedback { get; }

        private void Started(FileStuff stuff, Population<DenseIndividual> population)
        {
            StartTrace();
        }

        public void StartTrace()
        {
            BestFitness = new List<double>();
            MeanFitness = new List<double>();

            BestInitialState = Misc.Create(Size, i => new List<double>());
            BestTerminalState = Misc.Create(Size, i => new List<double>());

            if (TargetSwitches == null)
                TargetSwitchList = new List<TraceTargetSwitch>();
        }

        private void Finished(FileStuff stuff, Population<DenseIndividual> population, int epochCount, long generationCount)
        {
            EndTrace();
        }

        public void EndTrace()
        {
            BestFitnesses.Add(BestFitness.ToArray());
            MeanFitnesses.Add(MeanFitness.ToArray());
            
            BestInitialStates.Add(BestInitialState.Select(r => r.ToArray()).ToArray());
            BestTerminalStates.Add(BestTerminalState.Select(r => r.ToArray()).ToArray());

            if (TargetSwitches == null)
            {
                TargetSwitches = TargetSwitchList.ToArray();
                TargetSwitchList = null;
            }
        }

        private void TargetStarted(FileStuff stuff, Population<DenseIndividual> population, ITarget target, int epoch, long generationCount)
        {
            StartTarget(generationCount, target);
        }

        public void StartTarget(long generation, ITarget target, Population<DenseIndividual> population)
        {
            StartTarget(generation, target);
        }

        public void StartTarget(long generation, ITarget target)
        {
            if (TargetSwitchList != null)
                TargetSwitchList.Add(new TraceTargetSwitch(generation, target));
        }

        private void Judged(FileStuff stuff, IReadOnlyList<IndividualJudgement<DenseIndividual>> populationJudgements, int epochCount, long generationCount, ITarget target)
        {
            Judged(generationCount, populationJudgements);
        }

        public void Judged(long generation, ITarget target, IReadOnlyList<IndividualJudgement<DenseIndividual>> populationJudgements)
        {
            Judged(generation, populationJudgements);
        }

        public void Judged(long generation, IReadOnlyList<IndividualJudgement<DenseIndividual>> populationJudgements)
        {
            if (generation % SamplePeriod != 0)
                return;

            var best = populationJudgements.ArgMax(ij => ij.Judgement.CombinedFitness);

            BestFitness.Add(best.Judgement.CombinedFitness);
            MeanFitness.Add(populationJudgements.Average(ij => ij.Judgement.CombinedFitness));
            
            for (int i = 0; i < BestInitialState.Length; i++)
                BestInitialState[i].Add(best.Individual.Genome.InitialState[i]);
            for (int i = 0; i < BestTerminalState.Length; i++)
                BestTerminalState[i].Add(best.Individual.Phenotype[i]);
        }
    }
}
