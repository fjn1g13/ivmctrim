﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M4M.State;
using MathNet.Numerics.Random;

namespace M4M.Epistatics
{
    [StateClass]
    public class Type1DHTOPTarget : ITarget
    {
        [Obsolete]
        protected Type1DHTOPTarget()
        {
            // nix
        }

        public Type1DHTOPTarget(int size)
        {
            if ((int)Math.Round(Math.Pow(2, Math.Log(size, 2))) != size)
                throw new Exception("Invalid Type1DHTOP Target Size");

            Size = size;
        }

        [SimpleStateProperty("Size")]
        public int Size { get; private set; }

        public string FriendlyName => "Type1DHTOP" + Size;

        public string FullName => "Type1DHTOP" + Size;
        
        public string Description => $"Type1 DHTOP Target, Size={Size}";

        public double Judge(Phenotype p)
        {
            double f = 0.0;

            int l = Size;
            int max = 0;
            while (l >= 4)
            {
                max = 1 + max * 2;
                ReduceInplace(p.Vector.Select(d => d > 0 ? 1 : -1).ToArray(), ref l, ref f); // not quite Sign (we don't want any zeros)
            }

            return (f / max) * (p.Vector.Sum(x => Math.Abs(x)) / Size);
        }

        private static void ReduceInplace(int[] arr, ref int l, ref double f)
        {
            for (int i = 0; i < l; i += 4)
            {
                bool nozeros = arr.Skip(i).Take(4).Count(x => x == 0) == 0;
                bool oneone = arr.Skip(i).Take(4).Count(x => x > 0) == 1;
                bool ok = nozeros && oneone;

                if (ok)
                {
                    f++;
                    arr[i/2+0] = arr[i+2] + arr[i+3];
                    arr[i/2+1] = arr[i+1] + arr[i+1];
                }
                else
                {
                    f--;
                    arr[i/2+0] = 0;
                    arr[i/2+1] = 0;
                }
            }

            l /= 2;
        }

        public bool NextGeneration(RandomSource rand, JudgementRules jrules)
        {
            // no change
            return false;
        }

        public bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            return false;
        }
    }
}
