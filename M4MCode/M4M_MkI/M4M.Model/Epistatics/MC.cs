﻿using M4M.State;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M.Epistatics
{
    public class MCTargetPackages
    {
        public static EpistaticTargetPackage MCPlainOnPlain(int n, int k, double p) => new EpistaticTargetPackage($"MC{n},{k},{p}_2", new[] { new MCTargetPlainOnPlain(n, k, p), new MCTargetPlainOnPlain(n, k, p) });
        public static EpistaticTargetPackage MCCheckOnPlain(int n, int k, double p) => new EpistaticTargetPackage($"MC{n},{k},{p}_2", new[] { new MCTargetCheckOnPlain(n, k, p), new MCTargetCheckOnPlain(n, k, p) });
        public static EpistaticTargetPackage MCCheckOnCheck(int n, int k, double p) => new EpistaticTargetPackage($"MC{n},{k},{p}_2", new[] { new MCTargetCheckOnCheck(n, k, p), new MCTargetCheckOnCheck(n, k, p) });
    }

    [StateClass]
    public class MCTargetPlainOnPlain : CorrelationMatrixTarget, IPerfectPhenotypeTarget
    {
        [SimpleStateProperty("N")]
        public int N { get; private set; }

        [SimpleStateProperty("K")]
        public int K { get; private set; }

        [SimpleStateProperty("P")]
        public double P { get; private set; }

        [Obsolete]
        protected MCTargetPlainOnPlain()
        {
        }

        public MCTargetPlainOnPlain(int n, int k, double p) : base(PrepareMCMatrixPlainPlain(n, k, p), $"Plain{n}Plain{k}_{p}", true)
        {
            N = n;
            K = k;
            P = p;
        }

        public static Linear.Vector<double> PrepareMCIdealVectorPlainPlain(int n, int k)
        {
            Linear.Vector<double> vec = Linear.CreateVector.Dense(n * k, i => 1.0);
            return vec;
        }

        public static Linear.Matrix<double> PrepareMCMatrixPlainPlain(int n, int k, double p)
        {
            Linear.Matrix<double> mat = Linear.CreateMatrix.Dense(n * k, n * k, (i, j) =>
            {
                int mi = i / k;
                int mj = j / k;

                if (mi == mj)
                { // same module
                    return 1.0;
                }
                else
                { // different modules
                    return p;
                }
            });
            return mat;
        }

        public Vector<double> PreparePerfectP()
        {
            return PrepareMCIdealVectorPlainPlain(N, K);
        }
    }

    [StateClass]
    public class MCTargetCheckOnPlain : CorrelationMatrixTarget, IPerfectPhenotypeTarget
    {
        [SimpleStateProperty("N")]
        public int N { get; private set; }

        [SimpleStateProperty("K")]
        public int K { get; private set; }

        [SimpleStateProperty("P")]
        public double P { get; private set; }

        [Obsolete]
        protected MCTargetCheckOnPlain()
        {
        }

        public MCTargetCheckOnPlain(int n, int k, double p) : base(PrepareMCMatrixCheckPlain(n, k, p), $"Check{n}Plain{k}_{p}", true)
        {
            N = n;
            K = k;
            P = p;
        }

        public static Linear.Vector<double> PrepareMCIdealVectorCheckPlain(int n, int k)
        {
            Linear.Vector<double> vec = Linear.CreateVector.Dense(n * k, i =>
            {
                int mi = i / k;

                double mc = (mi) % 2 == 0 ? 1 : -1;

                return mc;
            });
            return vec;
        }

        public static Linear.Matrix<double> PrepareMCMatrixCheckPlain(int n, int k, double p)
        {
            Linear.Matrix<double> mat = Linear.CreateMatrix.Dense(n * k, n * k, (i, j) =>
            {
                int mi = i / k;
                int mj = j / k;

                double mc = (mi + mj) % 2 == 0 ? 1 : -1;

                if (mi == mj)
                { // same module
                    return 1.0;
                }
                else
                { // different modules
                    return mc * p;
                }
            });
            return mat;
        }

        public Vector<double> PreparePerfectP()
        {
            return PrepareMCIdealVectorCheckPlain(N, K);
        }
    }

    [StateClass]
    public class MCTargetCheckOnCheck : CorrelationMatrixTarget, IPerfectPhenotypeTarget
    {
        [SimpleStateProperty("N")]
        public int N { get; private set; }

        [SimpleStateProperty("K")]
        public int K { get; private set; }

        [SimpleStateProperty("P")]
        public double P { get; private set; }

        [Obsolete]
        protected MCTargetCheckOnCheck()
        {
        }

        public MCTargetCheckOnCheck(int n, int k, double p) : base(PrepareMCMatrixCheckCheck(n, k, p), $"Check{n}Check{k}_{p}", true)
        {
            N = n;
            K = k;
            P = p;
        }

        public static Linear.Vector<double> PrepareMCIdealVectorCheckCheck(int n, int k)
        {
            Linear.Vector<double> vec = Linear.CreateVector.Dense(n * k, i =>
            {
                int mi = i / k;
                int ti = i % k;

                double mc = (mi) % 2 == 0 ? 1 : -1;
                double tc = (ti) % 2 == 0 ? 1 : -1;

                return mc * tc;
            });
            return vec;
        }

        public static Linear.Matrix<double> PrepareMCMatrixCheckCheck(int n, int k, double p)
        {
            Linear.Matrix<double> mat = Linear.CreateMatrix.Dense(n * k, n * k, (i, j) =>
                {
                    int mi = i / k;
                    int mj = j / k;
                    
                    int ti = i % k;
                    int tj = j % k;

                    double mc = (mi + mj) % 2 == 0 ? 1 : -1;
                    double tc = (ti + tj) % 2 == 0 ? 1 : -1;

                    if (mi == mj)
                    { // same module
                        return tc;
                    }
                    else
                    { // different modules
                        return tc * mc * p;
                    }
                });
            return mat;
        }

        public Vector<double> PreparePerfectP()
        {
            return PrepareMCIdealVectorCheckCheck(N, K);
        }
    }
}
