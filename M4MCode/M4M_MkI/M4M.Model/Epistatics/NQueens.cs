﻿using System;
using System.Collections.Generic;
using System.Linq;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M.Epistatics
{
    public static class NQueens
    {
        /// <summary>
        /// -1 (ascii /) corresponds to blank
        /// </summary>
        public static int[] ParsePartialSolution(string str)
        {
            int[] ints;

            if (str.Contains(";") || str.Contains(","))
            {
                ints = str.Split(new[] { ',', ';' }).Select(int.Parse).ToArray();
            }
            else
            {
                ints = str.Select(c => c - '0').ToArray(); // '/' -> -1
            }

            return ints;
        }

        public static Linear.Vector<double> PrepareForcingVector(int[] partialSolution, bool full)
        {
            int n = (int)partialSolution.Length;

            var forcingVector = Linear.CreateVector.Dense<double>(n * n);

            for (int i = 0; i < partialSolution.Length; i++)
            {
                if (partialSolution[i] < 0)
                    continue;

                if (full)
                {
                    for (int j = 0; j < n; j++)
                    {
                        forcingVector[i + j] = -1;
                    }
                }

                forcingVector[i * n + partialSolution[i]] = 1;
            }

            return forcingVector;
        }

        public static Linear.Matrix<double> GeneralNQueens(int n, double diagonalCoef, double constraintCoef, bool proportionalConstraints)
        {
            int dim = n * n;

            Linear.Matrix<double> cmat = Linear.CreateMatrix.Dense<double>(dim, dim);

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    if (i == j)
                    {
                        cmat[i, j] = diagonalCoef;
                        continue;
                    }

                    double constraintSum = 0;

                    int ci = i % n;
                    int cj = j % n;

                    if (ci == cj) // same col
                        constraintSum += proportionalConstraints ? (double)n / n : 1.0;

                    int ri = i / n;
                    int rj = j / n;

                    if (ri == rj) // same row
                        constraintSum += proportionalConstraints ? (double)n / n : 1.0;

                    int sei = ri - ci;
                    int sej = rj - cj;

                    if (sei == sej) // same diff-diagonal
                        constraintSum += proportionalConstraints ? (double)n / (n - Math.Abs(sei)) : 1.0;

                    int kei = ri + ci;
                    int kej = rj + cj;

                    if (kei == kej) // same sum-diagonal
                        constraintSum += proportionalConstraints ? (double)n / (n - Math.Abs(kei - (n - 1))) : 1.0;

                    cmat[i, j] = constraintSum * constraintCoef;
                }
            }

            return cmat;
        }

        public static int[,] RenderSolution(Linear.Vector<double> densities, double threshold, out int n)
        {
            int dim = densities.Count;

            n = (int)Math.Round(Math.Sqrt(dim));

            if (n * n != dim)
                throw new ArgumentException("densities must be a square", nameof(densities));

            var solution = new int[n, n];

            for (int r = 0; r < n; r++)
            {
                for (int c = 0; c < n; c++)
                {
                    solution[r, c] = densities[r * n + c] > threshold ? 1 : 0;
                }
            }

            return solution;
        }

        public static List<NQueensConflict> LocateConflicts(int[,] solution)
        {
            int n = solution.GetLength(0);

            List<MatrixEntryAddress>[] seen;

            void reset(int count)
            {
                seen = Misc.Create<List<MatrixEntryAddress>>(count, i => new List<MatrixEntryAddress>());

                for (int i = 0; i < seen.Length; i++)
                {
                    seen[i].Clear();
                }
            }

            void set(int i, MatrixEntryAddress mea)
            {
                if (i < 0)
                    return;

                var l = seen[i];

                l.Add(mea);
            }

            var conflicts = new List<NQueensConflict>();

            void accumulate(NQueensConflictType type, string prefix)
            {
                for (int i = 0; i < seen.Length; i++)
                {
                    if (seen[i].Count > 1)
                    {
                        conflicts.Add(new NQueensConflict($"{prefix}{i}", type, seen[i].ToArray()));
                    }
                }
            }

            // for each row
            reset(n);
            for (int r = 0; r < n; r++)
            {
                for (int c = 0; c < n; c++)
                {
                    if (solution[r, c] > 0)
                        set(r, new MatrixEntryAddress(r, c));
                }
            }
            accumulate(NQueensConflictType.Row, $"Row");

            // for each column
            reset(n);
            for (int c = 0; c < n; c++)
            {
                for (int r = 0; r < n; r++)
                {
                    if (solution[r, c] > 0)
                        set(c, new MatrixEntryAddress(r, c));
                }
            }
            accumulate(NQueensConflictType.Column, $"Col");

            // for each diff-diagonal
            reset(n * 2 - 1);
            for (int c = 0; c < n; c++)
            {
                for (int r = 0; r < n; r++)
                {
                    if (solution[r, c] > 0)
                        set(r - c + n - 1, new MatrixEntryAddress(r, c));
                }
            }
            accumulate(NQueensConflictType.DiffDiagonal, $"DiffDiagonal");

            // for each sum-diagonal
            reset(n * 2 - 1);
            for (int c = 0; c < n; c++)
            {
                for (int r = 0; r < n; r++)
                {
                    if (solution[r, c] > 0)
                        set(r + c, new MatrixEntryAddress(r, c));
                }
            }
            accumulate(NQueensConflictType.SumDiagonal, $"SumDiagonal");

            return conflicts;
        }
    }

    public enum NQueensConflictType
    {
        Row,
        Column,
        DiffDiagonal,
        SumDiagonal,
    }

    public class NQueensConflict
    {
        public NQueensConflict(string description, NQueensConflictType type, IReadOnlyList<MatrixEntryAddress> entries)
        {
            Description = description ?? throw new ArgumentNullException(nameof(description));
            Type = type;
            Entries = entries ?? throw new ArgumentNullException(nameof(entries));
        }

        public string Description { get; }
        public NQueensConflictType Type { get; }
        public IReadOnlyList<MatrixEntryAddress> Entries { get; }
    }

    public class NQueensFeedback
    {
        public NQueensFeedback(int resolution, int plotPeriod, double threshold)
        {
            Resolution = resolution;
            PlotPeriod = plotPeriod;
            Threshold = threshold;

            NQueensScoreSamples = new List<int>();
            NQueensConflictCountSamples = new List<int>();
        }

        public int Resolution { get; }
        public int PlotPeriod { get; }
        public double Threshold { get; }

        private List<int> NQueensScoreSamples { get; }
        private List<int> NQueensConflictCountSamples { get; }

        public void Attach(IDensePopulationExperimentFeedback densePopulationExperimentFeedback)
        {
            var feedback = densePopulationExperimentFeedback.Feedback;

            feedback.EndTarget.Register(EndTarget);
            feedback.Finished.Register(Finished);
            feedback.EndEpoch.Register(EndEpoch);
        }

        private void EndTarget(FileStuff stuff, Population<DenseIndividual> population, ITarget target, int epoch, long generationCount, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
        {
            // record samples
            if (epoch % Resolution == 0)
            {
                var p = oldPopulationJudgements[0].Individual.Phenotype;
                var sol = NQueens.RenderSolution(p.Vector, Threshold, out int n);
                var conflictCount = NQueens.LocateConflicts(sol).Count;
                NQueensConflictCountSamples.Add(conflictCount);

                var NQueensScore = n - conflictCount;
                NQueensScoreSamples.Add(NQueensScore);
            }
        }

        private void Finished(FileStuff stuff, Population<DenseIndividual> population, int epochCount, long generationCount)
        {
            // write out
            SaveNQueensSamples(stuff, "_t");
        }

        private void EndEpoch(FileStuff stuff, Population<DenseIndividual> population, int epochCount, long generationCount, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
        {
            if (epochCount % PlotPeriod == 0)
            {
                SaveNQueensSamples(stuff, "" + epochCount);
            }
        }

        private void SaveNQueensSamples(FileStuff stuff, string name)
        {
            double[][] NQueensSamples = new double[][] {
                NQueensScoreSamples.Select(x => (double)x).ToArray(),
                NQueensConflictCountSamples.Select(x => (double)x).ToArray()
            };
            Analysis.SaveTrajectories(stuff.File("nqueenssamples" + name + ".dat"), NQueensSamples, Resolution);
        }
    }
}
