﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linear = MathNet.Numerics.LinearAlgebra;
using RandomSource = MathNet.Numerics.Random.RandomSource;
using System.Diagnostics;
using M4M.State;
using MathNet.Numerics.Random;

namespace M4M.Epistatics
{
    public class CbbnkKindVectorTarget : VectorTarget
    {
        public CbbnkKindVectorTarget(int N, int K, double ω, double ψ, Linear.Vector<double> target, string friendlyName)
            : base(target, friendlyName, new CbbnkKindVectorTargetJudger(N, K, ω, ψ), false)
        {
            Debug.Assert(N * K == target.Count, $"Epistatics/CbbnkKindVectorTarget.ctor(Vector<double>) target vector must have length N*K={N * K}");
            Debug.Assert(ω != 1.0, $"Epistatics/CbbnkKindVectorTarget.ctor(Vector<double>) ω must not be 1");
        }

        public CbbnkKindVectorTarget(int N, int K, double ω, double ψ, double[] target, string friendlyName)
            : base(target, friendlyName, new CbbnkKindVectorTargetJudger(N, K, ω, ψ), false)
        {
            Debug.Assert(N * K == target.Length, $"Epistatics/CbbnkKindVectorTarget.ctor(double[]) target array must have length N*K={N * K}");
            Debug.Assert(ω != 1.0, $"Epistatics/CbbnkKindVectorTarget.ctor(double[]) ω must not be 1");
        }

        public CbbnkKindVectorTarget(int N, int K, double ω, double ψ, string target, string friendlyName)
            : base(target, friendlyName, new CbbnkKindVectorTargetJudger(N, K, ω, ψ), false)
        {
            Debug.Assert(N * K == target.Length, $"Epistatics/CbbnkKindVectorTarget.ctor(string) target string '{target}' must have length N*K={N * K}");
            Debug.Assert(ω != 1.0, $"Epistatics/CbbnkKindVectorTarget.ctor(string) ω must not be 1");
        }
    }

    [StateClass]
    public class CbbnkKindVectorTargetJudger : IVectorTargetJudger
    {
        [Obsolete]
        protected CbbnkKindVectorTargetJudger()
        { }

        [SimpleStateProperty("Name")]
        public string Name { get; private set; }
        
        public string Description => $"CbbnkKindVectorTargetJudger, Module Count={N}, Module Size={K}, Slow cut-off ω={ω}, Slow gradient ψ={ψ}";

        public CbbnkKindVectorTargetJudger(int N, int K, double ω, double ψ)
        {
            this.N = N;
            this.K = K;
            this.ω = ω;
            this.ψ = ψ;

            Name = $"CbbnkKindVectorTargetJudger(N={N}, K={K}, ω={ω}, ψ={ψ})";
        }

        [SimpleStateProperty("N")]
        public int N { get; private set; }

        [SimpleStateProperty("K")]
        public int K { get; private set; }

        [SimpleStateProperty("ω")]
        public double ω { get; private set; }

        [SimpleStateProperty("ψ")]
        public double ψ { get; private set; }

        public double Judge(Linear.Vector<Double> target, Phenotype phenotype)
        {
            double fitnessAcc = 0;

            for (int mi = 0; mi < N * K; mi += K)
            {
                double acc = 0;
                for (int i = mi; i < mi + K; i++)
                    acc += phenotype[i] * target[i];

                acc /= K;

                double moduleFitness;
                if (acc > ω) // ideal
                {
                    moduleFitness = ω * ψ + (Math.Abs(acc) - ω) * (1 - ω * ψ) / (1 - ω);
                }
                else
                {
                    moduleFitness = Math.Abs(acc) * ψ;
                }

                fitnessAcc += moduleFitness;
            }

            return fitnessAcc / N;
        }
    }

    [StateClass]
    public class CbbnkKindTarget : ITarget
    {
        [Obsolete]
        private CbbnkKindTarget()
        { }

        [SimpleStateProperty("FriendlyName")]
        public string FriendlyName { get; private set; }

        [SimpleStateProperty("FullName")]
        public string FullName { get; private set; }
        
        /// <summary>
        /// A detailed description of the target
        /// </summary>
        public virtual string Description => $"Cbbnk Kind Target, Size={Size}, Number of Modules={N}, Module Size={K}, Slow cut-off ω={ω}, Slow gradient ψ={ψ}";

        [SimpleStateProperty("Size")]
        public int Size { get; private set; }

        /// <summary>
        /// Number of modules (NOT Size)
        /// </summary>
        [SimpleStateProperty("N")]
        public int N { get; private set; }

        /// <summary>
        /// Module dimension
        /// </summary>
        [SimpleStateProperty("K")]
        public int K { get; private set; }

        /// <summary>
        /// Slow cut-off
        /// </summary>
        [SimpleStateProperty("ω")]
        public double ω { get; private set; }

        /// <summary>
        /// Slow gradient
        /// </summary>
        [SimpleStateProperty("ψ")]
        public double ψ { get; private set; }

        public CbbnkKindTarget(int N, int K, double ω, double ψ, string friendlyName)
        {
            this.N = N;
            this.K = K;
            this.ω = ω;
            this.ψ = ψ;

            Size = N * K;
            FriendlyName = friendlyName;
            FullName = $"{nameof(CbbnkKindTarget)} {friendlyName}, N={N}, K={K}, ω={ω}, ψ={ψ}";
        }

        public double Judge(Phenotype phenotype)
        {
            double fitnessAcc = 0;

            for (int mi = 0; mi < N * K; mi += K)
            {
                double acc = 0;
                for (int i = mi; i < mi + K; i++)
                    acc += phenotype[i];

                acc /= K;

                double moduleFitness;
                if (acc > ω) // ideal
                {
                    moduleFitness = ω * ψ + (Math.Abs(acc) - ω) * (1 - ω * ψ) / (1 - ω);
                }
                else
                {
                    moduleFitness = Math.Abs(acc) * ψ;
                }

                fitnessAcc += moduleFitness;
            }

            return fitnessAcc / N;
        }

        public bool NextGeneration(RandomSource rand, JudgementRules jrules)
        {
            return false;
        }

        public bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            return false;
        }
    }

    [StateClass]
    public class CbbnkCruelVectorTarget : VectorTarget
    {
        [Obsolete]
        protected CbbnkCruelVectorTarget()
        { }

        public CbbnkCruelVectorTarget(int N, int K, double ω, double ψ, Linear.Vector<double> target, string friendlyName)
            : base(target, friendlyName, new CbbnkCruelVectorTargetJudger(N, K, ω, ψ), false)
        {
            Debug.Assert(N * K == target.Count, $"Epistatics/CbbnkCruelVectorTarget.ctor(Vector<double>) target vector must have length N*K={N * K}");
        }

        public CbbnkCruelVectorTarget(int N, int K, double ω, double ψ, double[] target, string friendlyName)
            : base(target, friendlyName, new CbbnkCruelVectorTargetJudger(N, K, ω, ψ), false)
        {
            Debug.Assert(N * K == target.Length, $"Epistatics/CbbnkCruelVectorTarget.ctor(double[]) target array must have length N*K={N * K}");
        }

        public CbbnkCruelVectorTarget(int N, int K, double ω, double ψ, string target, string friendlyName)
            : base(target, friendlyName, new CbbnkCruelVectorTargetJudger(N, K, ω, ψ), false)
        {
            Debug.Assert(N * K == target.Length, $"Epistatics/CbbnkCruelVectorTarget.ctor(string) target string '{target}' must have length N*K={N * K}");
        }
    }

    [StateClass]
    public class CbbnkCruelVectorTargetJudger : IVectorTargetJudger
    {
        [Obsolete]
        protected CbbnkCruelVectorTargetJudger()
        { }

        [SimpleStateProperty("Name")]
        public string Name { get; private set; }
        
        public string Description => $"CbbnkCruelVectorTargetJudger, Module Count={N}, Module Size={K}, Slow cut-off ω={ω}, Slow gradient ψ={ψ}";

        public CbbnkCruelVectorTargetJudger(int N, int K, double ω, double ψ)
        {
            this.N = N;
            this.K = K;
            this.ω = ω;
            this.ψ = ψ;

            Name = $"CbbnkCruelVectorTargetJudger(N={N}, K={K}, ω={ω}, ψ={ψ})";
        }

        [SimpleStateProperty("N")]
        public int N { get; private set; }

        [SimpleStateProperty("K")]
        public int K { get; private set; }

        [SimpleStateProperty("ω")]
        public double ω { get; private set; }

        [SimpleStateProperty("ψ")]
        public double ψ { get; private set; }

        public double Judge(Linear.Vector<Double> target, Phenotype phenotype)
        {
            double fitnessAcc = 0;

            for (int mi = 0; mi < N * K; mi += K)
            {
                double acc = 0;
                for (int i = mi; i < mi + K; i++)
                    acc += phenotype[i] * target[i];

                acc /= K;

                double moduleFitness;
                if (acc > ω) // ideal
                {
                    moduleFitness = 1.0;
                }
                else
                {
                    moduleFitness = Math.Abs(acc) * ψ;
                }

                fitnessAcc += moduleFitness;
            }

            return fitnessAcc / N;
        }
    }

    [StateClass]
    public class CbbnkCruelTarget : ITarget
    {
        [Obsolete]
        protected CbbnkCruelTarget()
        { }
        
        [SimpleStateProperty("FriendlyName")]
        public string FriendlyName { get; private set; }
        
        [SimpleStateProperty("FullName")]
        public string FullName { get; private set; }
        
        /// <summary>
        /// A detailed description of the target
        /// </summary>
        public virtual string Description => $"Cbbnk Cruel Target, Size={Size}, Number of Modules={N}, Module Size={K}, Slow cut-off ω={ω}, Slow gradient ψ={ψ}";

        [SimpleStateProperty("Size")]
        public int Size { get; private set; }

        /// <summary>
        /// Number of modules (NOT Size)
        /// </summary>
        [SimpleStateProperty("N")]
        public int N { get; private set; }

        /// <summary>
        /// Module dimension
        /// </summary>
        [SimpleStateProperty("K")]
        public int K { get; private set; }

        /// <summary>
        /// Slow cut-off
        /// </summary>
        [SimpleStateProperty("ω")]
        public double ω { get; private set; }

        /// <summary>
        /// Slow gradient
        /// </summary>
        [SimpleStateProperty("ψ")]
        public double ψ { get; private set; }

        public CbbnkCruelTarget(int N, int K, double ω, double ψ, string friendlyName)
        {
            this.N = N;
            this.K = K;
            this.ω = ω;
            this.ψ = ψ;

            Size = N * K;
            FriendlyName = friendlyName;
            FullName = $"{nameof(CbbnkCruelTarget)} {friendlyName}, N={N}, K={K}, ω={ω}, ψ={ψ}";
        }

        public double Judge(Phenotype phenotype)
        {
            double fitnessAcc = 0;

            for (int mi = 0; mi < N * K; mi += K)
            {
                double acc = 0;
                for (int i = mi; i < mi + K; i++)
                    acc += phenotype[i];

                acc /= K;

                double moduleFitness;
                if (acc > ω) // ideal
                {
                    moduleFitness = 1.0;
                }
                else
                {
                    moduleFitness = Math.Abs(acc) * ψ;
                }

                fitnessAcc += moduleFitness;
            }

            return fitnessAcc / N;
        }

        public bool NextGeneration(RandomSource rand, JudgementRules jrules)
        {
            return false;
        }

        public bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            return false;
        }
    }

    [StateClass]
    public class CbbnkStepVectorTarget : VectorTarget
    {
        [Obsolete]
        protected CbbnkStepVectorTarget() : base()
        { }

        public CbbnkStepVectorTarget(int N, int K, double ω, double ψ, double ξ, Linear.Vector<double> target, string friendlyName)
            : base(target, friendlyName, new CbbnkStepVectorTargetJudger(N, K, ω, ψ, ξ), false)
        {
            Debug.Assert(N * K == target.Count, $"Epistatics/CbbnkStepVectorTarget.ctor(Vector<double>) target vector must have length N*K={N * K}");
        }

        public CbbnkStepVectorTarget(int N, int K, double ω, double ψ, double ξ, double[] target, string friendlyName)
            : base(target, friendlyName, new CbbnkStepVectorTargetJudger(N, K, ω, ψ, ξ), false)
        {
            Debug.Assert(N * K == target.Length, $"Epistatics/CbbnkStepVectorTarget.ctor(double[]) target array must have length N*K={N * K}");
        }

        public CbbnkStepVectorTarget(int N, int K, double ω, double ψ, double ξ, string target, string friendlyName)
            : base(target, friendlyName, new CbbnkStepVectorTargetJudger(N, K, ω, ψ, ξ), false)
        {
            Debug.Assert(N * K == target.Length, $"Epistatics/CbbnkStepVectorTarget.ctor(string) target string '{target}' must have length N*K={N * K}");
        }
    }
    
    [StateClass]
    public class CbbnkStepVectorTargetJudger : IVectorTargetJudger
    {
        [Obsolete]
        protected CbbnkStepVectorTargetJudger()
        { }

        [SimpleStateProperty("Name")]
        public string Name { get; private set; }

        public string Description => $"CbbnkStepVectorTargetJudger, Module Count={N}, Module Size={K}, Slow cut-off ω={ω}, Slow gradient ψ={ψ}, Step ξ={ξ}";

        public CbbnkStepVectorTargetJudger(int N, int K, double ω, double ψ, double ξ)
        {
            this.N = N;
            this.K = K;
            this.ω = ω;
            this.ψ = ψ;
            this.ξ = ξ;

            Name = $"CbbnkStepVectorTargetJudger(N={N}, K={K}, ω={ω}, ψ={ψ}, ξ={ξ})";
        }
        
        [SimpleStateProperty("N")]
        public int N { get; private set; }

        [SimpleStateProperty("K")]
        public int K { get; private set; }

        /// <summary>
        /// Step Offset
        /// </summary>
        [SimpleStateProperty("ω")]
        public double ω { get; private set; }

        /// <summary>
        /// Slow Gradient
        /// </summary>
        [SimpleStateProperty("ψ")]
        public double ψ { get; private set; }

        /// <summary>
        /// Step size
        /// </summary>
        [SimpleStateProperty("ξ")]
        public double ξ { get; private set; }

        public double Judge(Linear.Vector<Double> target, Phenotype phenotype)
        {
            double fitnessAcc = 0;

            for (int mi = 0; mi < N * K; mi += K)
            {
                double acc = 0;
                for (int i = mi; i < mi + K; i++)
                    acc += phenotype[i] * target[i];

                acc /= K;

                double moduleFitness;
                if (acc > ω) // ideal
                {
                    moduleFitness = Math.Abs(acc) * ψ + ξ;
                }
                else
                {
                    moduleFitness = Math.Abs(acc) * ψ;
                }

                fitnessAcc += moduleFitness;
            }

            return fitnessAcc / N;
        }
    }
    
    public class BBNKTargetPackages
    {
        // Step
        public static EpistaticTargetPackage CbbnkStep4x4Notvariable(double ω, double ψ, double ξ) => new EpistaticTargetPackage($"CbbnkStep4x4P{ω}_{ψ}_{ξ}_2", new[] { new CbbnkStepVectorTarget(4, 4, ω, ψ, ξ, "----++++++++----", "LHHL"), new CbbnkStepVectorTarget(4, 4, ω, ψ, ξ, "----++++++++----", "LHHL") });
        public static EpistaticTargetPackage CbbnkStep2x2Variable(double ω, double ψ, double ξ) => new EpistaticTargetPackage($"CbbnkCruel2x2P{ω}_{ψ}_{ξ}_4MVG", new[] { new CbbnkStepVectorTarget(2, 2, ω, ψ, ξ, "++++", "HH"), new CbbnkStepVectorTarget(2, 2, ω, ψ, ξ, "++--", "HL"), new CbbnkStepVectorTarget(2, 2, ω, ψ, ξ, "----", "LL"), new CbbnkStepVectorTarget(2, 2, ω, ψ, ξ, "--++", "LH") });
        public static EpistaticTargetPackage CbbnkStep2x3Variable(double ω, double ψ, double ξ) => new EpistaticTargetPackage($"CbbnkCruel2x3P{ω}_{ψ}_{ξ}_4MVG", new[] { new CbbnkStepVectorTarget(2, 3, ω, ψ, ξ, "++++++", "HH"), new CbbnkStepVectorTarget(2, 3, ω, ψ, ξ, "+++---", "HL"), new CbbnkStepVectorTarget(2, 3, ω, ψ, ξ, "------", "LL"), new CbbnkStepVectorTarget(2, 3, ω, ψ, ξ, "---+++", "LH") });
        public static EpistaticTargetPackage CbbnkStep3x2Variable(double ω, double ψ, double ξ) => new EpistaticTargetPackage($"CbbnkStep3x2P{ω}_{ψ}_{ξ}_3MVG", new[] { new CbbnkStepVectorTarget(3, 2, ω, ψ, ξ, "++++--", "HHL"), new CbbnkStepVectorTarget(3, 2, ω, ψ, ξ, "++--++", "HLH"), new CbbnkStepVectorTarget(3, 2, ω, ψ, ξ, "--++++", "LHH") });
        public static EpistaticTargetPackage CbbnkStep3x3Variable(double ω, double ψ, double ξ) => new EpistaticTargetPackage($"CbbnkStep3x3P{ω}_{ψ}_{ξ}_3MVG", new[] { new CbbnkStepVectorTarget(3, 3, ω, ψ, ξ, "++++++---", "HHL"), new CbbnkStepVectorTarget(3, 3, ω, ψ, ξ, "+++---+++", "HLH"), new CbbnkStepVectorTarget(3, 3, ω, ψ, ξ, "---++++++", "LHH") });
        public static EpistaticTargetPackage CbbnkStep4x4Variable(double ω, double ψ, double ξ) => new EpistaticTargetPackage($"CbbnkStep4x4P{ω}_{ψ}_{ξ}_3MVG", new[] { new CbbnkStepVectorTarget(4, 4, ω, ψ, ξ, "----++++++++----", "LHHL"), new CbbnkStepVectorTarget(4, 4, ω, ψ, ξ, "++++----++++----", "HLHL"), new CbbnkStepVectorTarget(4, 4, ω, ψ, ξ, "++++++++--------", "HHLL") });
        public static EpistaticTargetPackage CbbnkStep4x4SemiVariable(double ω, double ψ, double ξ) => new EpistaticTargetPackage($"CbbnkStep4x4P{ω}_{ψ}_{ξ}_2SemiMVG", new[] { new CbbnkStepVectorTarget(4, 4, ω, ψ, ξ, "++++++++--------", "HHLL"), new CbbnkStepVectorTarget(4, 4, ω, ψ, ξ, "++++++++++++++++", "HHHH") });
        public static EpistaticTargetPackage CbbnkStep5x4Variable(double ω, double ψ, double ξ) => new EpistaticTargetPackage($"CbbnkStep5x4P{ω}_{ψ}_{ξ}_3MVG", new[] { new CbbnkStepVectorTarget(4, 5, ω, ψ, ξ, "-----++++++++++-----", "LHHL"), new CbbnkStepVectorTarget(4, 5, ω, ψ, ξ, "+++++-----+++++-----", "HLHL"), new CbbnkStepVectorTarget(4, 5, ω, ψ, ξ, "++++++++++----------", "HHLL") });

        // Kind
        public static EpistaticTargetPackage CbbnkKind3x3Variable(double ω, double ψ) => new EpistaticTargetPackage("CbbnkKind3x3P" + ω + "_" + ψ + "_3MVG", new[] { new CbbnkKindVectorTarget(3, 3, ω, ψ, "---++++++", "LHH"), new CbbnkKindVectorTarget(3, 3, ω, ψ, "+++---+++", "HLH"), new CbbnkKindVectorTarget(3, 3, ω, ψ, "++++++---", "HHL") });
        public static EpistaticTargetPackage CbbnkKind2x2Variable(double ω, double ψ) => new EpistaticTargetPackage("CbbnkKind2x2P" + ω + "_" + ψ + "_4MVG", new[] { new CbbnkKindVectorTarget(2, 2, ω, ψ, "++++", "HH"), new CbbnkKindVectorTarget(2, 2, ω, ψ, "++--", "HL"), new CbbnkKindVectorTarget(2, 2, ω, ψ, "----", "LL"), new CbbnkKindVectorTarget(2, 2, ω, ψ, "--++", "LH") });

        // Cruel
        public static EpistaticTargetPackage CbbnkCruel4x4(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel4x4P" + ω + "_" + ψ + "_2", new[] { new CbbnkCruelTarget(4, 4, ω, ψ, "Cruel2x2"), new CbbnkCruelTarget(4, 4, ω, ψ, "Cruel2x2") });
        public static EpistaticTargetPackage CbbnkCruel3x3(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel3x3P" + ω + "_" + ψ + "_2", new[] { new CbbnkCruelTarget(3, 3, ω, ψ, "Cruel2x2"), new CbbnkCruelTarget(3, 3, ω, ψ, "Cruel2x2") });
        public static EpistaticTargetPackage CbbnkCruel2x2(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel2x2P" + ω + "_" + ψ + "_2", new[] { new CbbnkCruelTarget(2, 2, ω, ψ, "Cruel2x2"), new CbbnkCruelTarget(2, 2, ω, ψ, "Cruel2x2") });

        public static EpistaticTargetPackage CbbnkCruel2x2NonVariable(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel2x2P" + ω + "_" + ψ + "_2NVG", new[] { new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++++", "HH"),  new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++++", "HH") });
        
        public static EpistaticTargetPackage CbbnkCruel2x2Variable(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel2x2P" + ω + "_" + ψ + "_4MVG", new[] { new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++++", "HH"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++--", "HL"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "----", "LL"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "--++", "LH") });

        public static EpistaticTargetPackage CbbnkCruel2x2VariablePositiveBias(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel2x2P" + ω + "_" + ψ + "_6MVGPB", new[] { new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++++", "HH"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++--", "HL"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "----", "LL"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++++", "HH"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "--++", "LH"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "----", "LL") });
        public static EpistaticTargetPackage CbbnkCruel2x2VariableNegativeBias(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel2x2P" + ω + "_" + ψ + "_6MVGNB", new[] { new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++++", "HH"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++--", "HL"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "--++", "LH"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "----", "LL"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "--++", "LH"), new CbbnkCruelVectorTarget(2, 2, ω, ψ, "++--", "HL") });

        public static EpistaticTargetPackage CbbnkCruel3x2Variable(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel3x2P" + ω + "_" + ψ + "_3MVG", new[] { new CbbnkCruelVectorTarget(3, 2, ω, ψ, "++++--", "HHL"), new CbbnkCruelVectorTarget(3, 2, ω, ψ, "++--++", "HLH"), new CbbnkCruelVectorTarget(3, 2, ω, ψ, "--++++", "LHH") });

        public static EpistaticTargetPackage CbbnkCruel2x3Variable(double ω, double ψ) => new EpistaticTargetPackage("CbbnkCruel2x3P" + ω + "_" + ψ + "_4MVG", new[] { new CbbnkCruelVectorTarget(2, 3, ω, ψ, "++++++", "HH"), new CbbnkCruelVectorTarget(2, 3, ω, ψ, "+++---", "HL"), new CbbnkCruelVectorTarget(2, 3, ω, ψ, "------", "LL"), new CbbnkCruelVectorTarget(2, 3, ω, ψ, "---+++", "LH") });
    }
}
