﻿using M4M.State;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M.Epistatics
{
    [StateClass]
    public class IvmcStackedVectorTargetJudger : IVectorTargetJudger
    {
        [Obsolete]
        protected IvmcStackedVectorTargetJudger()
        {
        }

        public IvmcStackedVectorTargetJudger(IModuleBenefitFunction moduleBenefitFunction, IvmcProper proper)
            : this(moduleBenefitFunction, proper, 1.0, true, 2)
        {
        }

        public IvmcStackedVectorTargetJudger(IModuleBenefitFunction moduleBenefitFunction, IvmcProper proper, double decayFactor, bool rescaleFitness, int scaleFactor)
        {
            ModuleBenefitFunction = moduleBenefitFunction;
            Proper = proper;
            Name = "IvmcStackedVectorTargetJudger";
            DecayFactor = decayFactor;
            RescaleFitness = rescaleFitness;
            ScaleFactor = scaleFactor;
        }

        [SimpleStateProperty("StackedModules")]
        private int[][] StackedModules { get; set; } // not used

        [SimpleStateProperty("ModuleBenefitFunction")]
        public IModuleBenefitFunction ModuleBenefitFunction { get; private set; }

        [SimpleStateProperty("Proper")]
        public IvmcProper Proper { get; private set; }

        [SimpleStateProperty("Name")]
        public string Name { get; private set; }

        [SimpleStateProperty("DecayFactor")]
        public double DecayFactor { get; private set; }

        [SimpleStateProperty("RescaleFitness")]
        public bool RescaleFitness { get; private set; } = true;

        [SimpleStateProperty("ScaleFactor")]
        public int ScaleFactor { get; private set; } = 2;

        public string Description => $"IvmcStackedVectorTargetJudger (ModuleBenefitFunction={ModuleBenefitFunction.Name}, DecayFactor={DecayFactor}, RescaleFitness={RescaleFitness}, ScaleFactor={ScaleFactor})";

        public double Judge(Vector<double> target, Phenotype phenotype)
        {
            double[] moduleAvgs;

            if (Proper.ModulesType == IvmcProperModulesType.BlockModules)
            {
                Proper.DecomposeBlockModules(out int M, out int K);
                moduleAvgs = new double[M];

                for (int i = 0; i < M * K; i += K)
                {
                    double acc = 0;
                    for (int j = i; j < i + K; j++)
                        acc += phenotype[j] * target[j];

                    acc /= K; // average

                    moduleAvgs[i / K] = acc;
                }
            }
            else if (Proper.ModulesType == IvmcProperModulesType.FreeModules)
            {
                Proper.DecomposeFreeModules(out var modules);
                moduleAvgs = new double[modules.ModuleCount];
                
                for (int i = 0; i < modules.ModuleCount; i++)
                {
                    var m = modules[i];

                    double acc = 0;
                    foreach (int j in m)
                        acc += phenotype[j] * target[j];

                    acc /= m.Count; // average

                    moduleAvgs[i] = acc;
                }
            }
            else
            {
                throw new Exception("Unrecognised IvmcProperModulesType: " + Proper.ModulesType);
            }
            
            double benefitAcc = 0;
            int smCount = 0;

            double decay = 1.0;
            double idealAcc = 0.0;
            for (int s = 1; s <= moduleAvgs.Length; s *= ScaleFactor)
            {
                for (int i = 0; i < moduleAvgs.Length; i += s)
                {
                    if (s > 1)
                    {
                        double tot = 0.0;
                        int stride = s / ScaleFactor;
                        for (int j = 0; j < ScaleFactor; j++)
                        {
                            tot += moduleAvgs[i + stride * j];
                        }
                        moduleAvgs[i] = tot / ScaleFactor;
                    }

                    var acc = moduleAvgs[i];
                    
                    // this will reward being correlated, without biasing the system
                    double moduleBenefit = ModuleBenefitFunction.ComputeModuleBenefit(acc, 0.5, 0.5);

                    benefitAcc += moduleBenefit * decay;
                    idealAcc += decay;
                    smCount++;
                }

                decay *= DecayFactor;
            }

            if (RescaleFitness)
                return benefitAcc / idealAcc;
            else
                return benefitAcc;
        }
    }

    [StateClass]
    public class SimpleStackedTarget : VectorTarget
    {
        [Obsolete]
        protected SimpleStackedTarget()
        {
        }

        public SimpleStackedTarget(string targetString, int moduleSize, int moduleCount, double decayFactor, int scaleFactor, IModuleBenefitFunction moduleBenefitFunction, double plusCoef, double minusCoef, bool rescaleFitness)
            : base(targetString, "SimpleStackedTarget")
        {
            ModuleSize = moduleSize;
            ModuleCount = moduleCount;
            DecayFactor = decayFactor;
            ScaleFactor = scaleFactor;
            ModuleBenefitFunction = moduleBenefitFunction ?? throw new ArgumentNullException(nameof(moduleBenefitFunction));
            PlusCoef = plusCoef;
            MinusCoef = minusCoef;
            RescaleFitness = rescaleFitness;
        }

        public override string Description => $"SimpleStackedTargetPackageM{ModuleCount}K{ModuleSize}cP{PlusCoef}cM{MinusCoef}SF{ScaleFactor}DF{DecayFactor}RF{RescaleFitness}{ModuleBenefitFunction.Name}";

        [SimpleStateProperty("ModuleSize")]
        public int ModuleSize { get; private set; }

        [SimpleStateProperty("ModuleCount")]
        public int ModuleCount { get; private set; }

        [SimpleStateProperty("DecayFactor")]
        public double DecayFactor { get; private set; }

        [SimpleStateProperty("ScaleFactor")]
        public int ScaleFactor { get; private set; } = 2;

        [SimpleStateProperty("ModuleBenefitFunction")]
        public IModuleBenefitFunction ModuleBenefitFunction { get; private set; }

        [SimpleStateProperty("PlusCoef")]
        public double PlusCoef { get; private set; }

        [SimpleStateProperty("MinusCoef")]
        public double MinusCoef { get; private set; }

        [SimpleStateProperty("RescaleFitness")]
        public bool RescaleFitness { get; private set; }

        protected override double JudgeOverride(Phenotype p)
        {
            var target = Vector;
            var phenotype = p.Vector;

            int M = ModuleCount;
            int K = ModuleSize;

            double[] moduleAvgs = new double[M];

            for (int i = 0; i < M * K; i += K)
            {
                double acc = 0;
                for (int j = i; j < i + K; j++)
                    acc += phenotype[j] * target[j];

                acc /= K; // average

                moduleAvgs[i / K] = acc;
            }

            double benefitAcc = 0;
            int smCount = 0;

            double decay = 1.0;
            double idealAcc = 0.0;
            for (int s = 1; s <= moduleAvgs.Length; s *= ScaleFactor)
            {
                for (int i = 0; i < moduleAvgs.Length; i += s)
                {
                    if (s > 1)
                    {
                        double tot = 0.0;
                        int stride = s / ScaleFactor;
                        for (int j = 0; j < ScaleFactor; j++)
                        {
                            tot += moduleAvgs[i + stride * j];
                        }
                        moduleAvgs[i] = tot / ScaleFactor;
                    }

                    var acc = moduleAvgs[i];

                    double moduleBenefit = ModuleBenefitFunction.ComputeModuleBenefit(acc, MinusCoef, PlusCoef);

                    benefitAcc += moduleBenefit * decay;
                    idealAcc += decay;
                    smCount++;
                }

                decay *= DecayFactor;
            }

            if (RescaleFitness)
                return benefitAcc / idealAcc;
            else
                return benefitAcc;
        }
    }
}
