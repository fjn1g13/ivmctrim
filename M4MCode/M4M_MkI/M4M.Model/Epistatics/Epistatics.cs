﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linear = MathNet.Numerics.LinearAlgebra;
using RandomSource = MathNet.Numerics.Random.RandomSource;
using static M4M.Misc;
using System.Diagnostics;
using M4M.State;

namespace M4M.Epistatics
{
    public interface IModuleBenefitFunction
    {
        string Name { get; }
        double ComputeModuleBenefit(double moduleAvg, double cminus, double cplus);
    }

    public static class ModuleBenefitFunctions
    {
        public static IModuleBenefitFunction ParseModuleBenefitFunction(string name)
        {
            if (name.Equals("proper", StringComparison.InvariantCultureIgnoreCase))
            {
                return new ProperModuleBenefitFunction();
            }
            else if (name.Equals("split", StringComparison.InvariantCultureIgnoreCase))
            {
                return new SplitModuleBenefitFunction();
            }
            if (name.Equals("adjustedProper", StringComparison.InvariantCultureIgnoreCase))
            {
                return new AdjustedProperModuleBenefitFunction();
            }

            throw new ArgumentException("Unrecognised ModuleBenefitFunction: " + name);
        }
    }

    [StateClass]
    public class ProperModuleBenefitFunction : IModuleBenefitFunction
    {
        public ProperModuleBenefitFunction()
        {
        }

        public string Name => "ProperModuleBenefitFunction";

        public double ComputeModuleBenefit(double acc, double cminus, double cplus)
        {
            double modulesBenefit = 0.0;

            double positiveness = 0.5 + acc * 0.5;
            modulesBenefit += positiveness * positiveness * cplus;

            double negativeness = 0.5 - acc * 0.5;
            modulesBenefit += negativeness * negativeness * cminus;

            return modulesBenefit;
        }
    }

    [StateClass]
    public class AdjustedProperModuleBenefitFunction : IModuleBenefitFunction
    {
        public AdjustedProperModuleBenefitFunction()
        {
        }

        public string Name => "AdjustedProperModuleBenefitFunction";

        public double ComputeModuleBenefit(double acc, double cminus, double cplus)
        {
            double modulesBenefit = 0.0;

            double positiveness = 0.5 + acc * 0.5;
            modulesBenefit += positiveness * positiveness * cplus;

            double negativeness = 0.5 - acc * 0.5;
            modulesBenefit += negativeness * negativeness * cminus;

            return (modulesBenefit - 0.5) * 2;
        }
    }

    [StateClass]
    public class SplitModuleBenefitFunction : IModuleBenefitFunction
    {
        public SplitModuleBenefitFunction()
        {
        }

        public string Name => "SplitModuleBenefitFunction";

        public double ComputeModuleBenefit(double acc, double cminus, double cplus)
        {
            double moduleBenefit = acc > 0
                ? acc * cplus
                : -acc * cminus;

            return moduleBenefit;
        }
    }

    public class EpistaticTargetPackage : ITargetPackage
    {
        public EpistaticTargetPackage(string name, ITarget[] targets)
        {
            Debug.Assert(targets != null, $"Epistatics/EpistaticTargetPackage.ctor targets must be set");

            int size = targets[0].Size;
            Debug.Assert(targets.All(t => t.Size == size), $"Epistatics/EpistaticTargetPackage.ctor targets must all be the same size");

            Name = name;
            Targets = targets;
        }

        /// <summary>
        /// The name of the target package
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The targets in the target package
        /// </summary>
        public ITarget[] Targets { get; }

        /// <summary>
        /// The Size of the targets
        /// </summary>
        public int TargetSize => Targets[0].Size;

        IReadOnlyList<ITarget> ITargetPackage.Targets => Targets;
    }
}
