﻿using System;
using System.Collections.Generic;
using System.Text;
using MathNet.Numerics.Random;

namespace M4M.Epistatics
{
    [State.StateClass]
    public class BinaryPuzzleTarget : ITarget
    {
        [Obsolete]
        protected BinaryPuzzleTarget()
        { }

        public BinaryPuzzleTarget(double[] targetVector, int width, int height, double relativeConstraintCost, double relativeLineConstraintCost)
        {
            if (targetVector.Length != width * height)
                throw new ArgumentException($"Target vector must have legnth width*height = {width * height}");

            TargetVector = targetVector;
            Width = width;
            Height = height;
            RelativeConstraintCost = relativeConstraintCost;
            RelativeLineConstraintCost = relativeLineConstraintCost;
        }

        public int Size => Width * Height;
        public string FriendlyName => "BinaryPuzzleTarget" + Size;
        public string FullName => "BinaryPuzzleTarget" + Size;
        public string Description => $"BinaryPuzzleTarget (W={Width}, H={Height}, Target={String.Join(";", TargetVector)}, RCC={RelativeConstraintCost}, RLCC={RelativeLineConstraintCost})";

        [State.SimpleStateProperty("Width")]
        public int Width { get; private set; }

        [State.SimpleStateProperty("Height")]
        public int Height { get; private set; }

        [State.SimpleStateProperty("TargetVector")]
        public double[] TargetVector { get; private set; }

        [State.SimpleStateProperty("RelativeConstraintCost")]
        public double RelativeConstraintCost { get; private set; }

        [State.SimpleStateProperty("RelativeLineConstraintCost")]
        public double RelativeLineConstraintCost { get; private set; }

        public double Judge(Phenotype phenotype)
        {
            var w = Width;
            var h = Height;
            double p(int r, int c) => phenotype[r * w + c];

            // target
            double fitness = 0.0;
            for (int i = 0; i < phenotype.Size; i++)
                fitness += phenotype[i] * TargetVector[i];

            // no-threes in a row
            for (int r = 0; r < h; r++)
            {
                for (int c = 0; c < w; c++)
                {
                    if (r > 0 && r < h - 1)
                    {
                        var bad = Math.Sign(p(r - 1, c)) == Math.Sign(p(r, c)) && Math.Sign(p(r + 1, c)) == Math.Sign(p(r, c));
                        var diff = RelativeConstraintCost * (Math.Abs(p(r - 1, c)) + Math.Abs(p(r, c)) + Math.Abs(p(r + 1, c)));
                        if (bad)
                            fitness -= diff;
                        else
                            fitness += diff;
                    }

                    if (c > 0 && c < w - 1)
                    {
                        var bad = Math.Sign(p(r, c - 1)) == Math.Sign(p(r, c)) && Math.Sign(p(r, c + 1)) == Math.Sign(p(r, c));
                        var diff = RelativeConstraintCost * (Math.Abs(p(r, c - 1)) + Math.Abs(p(r, c)) + Math.Abs(p(r, c + 1)));
                        if (bad)
                            fitness -= diff;
                        else
                            fitness += diff;
                    }
                }
            }

            // parity rows
            int rs1 = h % 2;
            for (int r = 0; r < h; r++)
            {
                int n = 0;
                double sum = 0.0;

                for (int c = 0; c < w; c++)
                {
                    var v = p(r, c);
                    sum += Math.Abs(v);
                    if (v > 0)
                        n++;
                    else
                        n--;
                }

                fitness -= sum * (Math.Abs(n) - rs1);
            }

            // parity columns
            int cs1 = w % 2;
            for (int c = 0; c < w; c++)
            {
                int n = 0;
                double sum = 0.0;

                for (int r = 0; r < h; r++)
                {
                    var v = p(r, c);
                    sum += Math.Abs(v);
                    if (v > 0)
                        n++;
                    else
                        n--;
                }

                fitness -= sum * (Math.Abs(n) - cs1);
            }

            return fitness;
        }

        public bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            return false;
        }

        public bool NextGeneration(RandomSource rand, JudgementRules jrules)
        {
            return false;
        }
    }
}
