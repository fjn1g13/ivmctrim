﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linear = MathNet.Numerics.LinearAlgebra;
using RandomSource = MathNet.Numerics.Random.RandomSource;
using System.Diagnostics;
using M4M.State;

namespace M4M.Epistatics
{
    public enum NeighbourType
    {
        // combinations
        One = 1,
        Four = 2,
        Five = 3,
        Eight = 6,
        Nine = 7,

        // flags
        Self = 1,
        Square = 2,
        Cross = 4,
    }

    public delegate bool CorrelationFilter(int rows, int cols, MatrixEntryAddress e0, MatrixEntryAddress e1);
    public delegate void DrDcFunc(int rows, int cols, MatrixEntryAddress e0, MatrixEntryAddress e1, out int dr, out int dc);
    public delegate bool DrDcFilter(int dr, int dc);

    public static class Neighbours
    {
        public static bool SquareDrDcFilter(int dr, int dc) => dr + dc == 1;
        public static bool CrossDrDcFilter(int dr, int dc) => dr == 1 && dc == 1;
        public static bool SelfDrDcFilter(int dr, int dc) => dr == 0 && dc == 0;

        public static void NonTorusDrDc(int rows, int cols, MatrixEntryAddress e0, MatrixEntryAddress e1, out int dr, out int dc)
        {
            dr = Math.Abs(e0.Row - e1.Row);
            dc = Math.Abs(e0.Col - e1.Col);
        }

        public static void TorusDrDc(int rows, int cols, MatrixEntryAddress e0, MatrixEntryAddress e1, out int dr, out int dc)
        {
            dr = Math.Abs(e0.Row - e1.Row);
            dc = Math.Abs(e0.Col - e1.Col);

            if (dr * 2 > rows)
                dr = rows - dr;
            if (dc * 2 > cols)
                dc = cols - dc;
        }

        /// <summary>
        /// Filters pairs of MatrixEntryAddresses to those which are the same entry, or are 4-neighbours of each other
        /// Provided for convience
        /// </summary>
        public static bool FiveNeighbourFilter(int rows, int cols, MatrixEntryAddress e0, MatrixEntryAddress e1)
        {
            int dr = Math.Abs(e0.Row - e1.Row);
            int dc = Math.Abs(e0.Col - e1.Col);

            return dr + dc <= 1; // only self-connections and direct (4) neighbours
        }

        public static NeighbourType ParseNeightbourType(string str)
        {
            switch (str.ToLowerInvariant())
            {
                case "four":
                    return NeighbourType.Four;
                case "five":
                    return NeighbourType.Five;
                case "eight":
                    return NeighbourType.Eight;
                case "nine":
                    return NeighbourType.Nine;
            }

            if (int.TryParse(str, out var i))
                return (NeighbourType)i;

            throw new ArgumentException($"Unrecognised neighbour type \"{str}\"");
        }

        public static CorrelationFilter GetNeightbourFilter(NeighbourType neighbourType, bool torus)
        {
            DrDcFunc drDcFunc = torus ? (DrDcFunc)TorusDrDc: NonTorusDrDc;

            List<DrDcFilter> filters = new List<DrDcFilter>();
            if ((neighbourType & NeighbourType.Self) > 0)
                filters.Add(SelfDrDcFilter);
            if ((neighbourType & NeighbourType.Square) > 0)
                filters.Add(SquareDrDcFilter);
            if ((neighbourType & NeighbourType.Cross) > 0)
                filters.Add(CrossDrDcFilter);

            return (rows, cols, e0, e1) =>
            {
                drDcFunc(rows, cols, e0, e1, out var dr, out var dc);
                foreach (var f in filters)
                    if (f(dr, dc))
                        return true;

                return false;
            };
        }
    }

    public interface IMatrixPattern
    {
        string Name { get; }
        Linear.Matrix<double> Generate(int rows, int cols);
    }

    public class FlatPattern : IMatrixPattern
    {
        public static readonly FlatPattern Instance = new FlatPattern();

        public string Name => "Flat";

        public Linear.Matrix<double> Generate(int rows, int cols)
        {
            return Linear.CreateMatrix.Dense<double>(rows, cols, (i, j) => 1.0);
        }
    }

    public class CheckPattern : IMatrixPattern
    {
        public static readonly CheckPattern Instance = new CheckPattern();

        public string Name => "Check";

        public Linear.Matrix<double> Generate(int rows, int cols)
        {
            return Linear.CreateMatrix.Dense<double>(rows, cols, (i, j) => (i + j) % 2 == 0 ? 1.0 : -1.0);
        }
    }

    public class ConcentricSquaresPattern : IMatrixPattern
    {
        public static readonly ConcentricSquaresPattern Instance = new ConcentricSquaresPattern();

        public string Name => "ConcentricSquares";

        public Linear.Matrix<double> Generate(int rows, int cols)
        {
            int halfv = rows / 2;
            int halfh = cols / 2;

            double p(int r, int c)
            {
                int v = r >= halfv ? rows - r - 1 : r;
                int h = c >= halfh ? cols - c - 1 : c;

                int m = Math.Min(v, h);
                return m % 2 == 0 ? +1.0 : -1.0;
            }

            return Linear.CreateMatrix.Dense<double>(rows, cols, p);
        }
    }

    public static class MatrixPatterns
    {
        public static Dictionary<string, IMatrixPattern> Patterns = new Dictionary<string, IMatrixPattern>(StringComparer.InvariantCultureIgnoreCase);

        static MatrixPatterns()
        {
            RegisterPattern(new FlatPattern());
            RegisterPattern(new CheckPattern());
            RegisterPattern(new ConcentricSquaresPattern());
        }

        public static void RegisterPattern(IMatrixPattern pattern)
        {
            Patterns.Add(pattern.Name, pattern);
        }

        public static IMatrixPattern GetPattern(string name)
        {
            if (Patterns.TryGetValue(name, out var found))
                return found;
            else
                throw new ArgumentException($"No pattern with name \"{name}\"", nameof(name));
        }
    }

    [StateClass]
    public class CorrelationMatrixTarget : ITarget
    {
        [MatrixStateProperty("CorrelationMatrix")]
        public Linear.Matrix<double> CorrelationMatrix { get; private set; }

        [MatrixStateProperty("NoisyMatrix")]
        private Linear.Matrix<double> NoisyMatrix { get; set; }


        [VectorStateProperty("ForcingVector")]
        public Linear.Vector<double> ForcingVector { get; private set; }

        public int Size => CorrelationMatrix.RowCount;

        [SimpleStateProperty("NormaliseFitness")]
        public bool NormaliseFitness { get; private set; } = true; // default (legacy)

        [SimpleStateProperty("FriendlyName")]
        public string FriendlyName { get; private set; }
        [SimpleStateProperty("FullName")]
        public string FullName { get; private set; }
        
        public string Description => $"CorrelationMatrixTarget, Size={Size}";

        [Obsolete]
        protected CorrelationMatrixTarget()
        {
        }

        public CorrelationMatrixTarget(Linear.Matrix<double> correlationMatrix, string friendlyName, bool normaliseFitness, Linear.Vector<double> forcingVector = null)
        {
            Debug.Assert(correlationMatrix.RowCount == correlationMatrix.ColumnCount, $"Epistatics/CorrelationMatrixTarget.ctor: correlationMatrix must be square");
            if (forcingVector != null)
                Debug.Assert(correlationMatrix.RowCount == forcingVector.Count, $"Epistatics/CorrelationMatrixTarget.ctor: forcingVector must have the same dimension as the correlationMatrix");

            CorrelationMatrix = correlationMatrix;
            ForcingVector = forcingVector;

            FriendlyName = friendlyName;
            NormaliseFitness = normaliseFitness;
            FullName = friendlyName;
        }

        public double Judge(Phenotype p)
        {
            var pv = p.Vector;

            Debug.Assert(NoisyMatrix != null, $"Epistatics/CorrelationMatrixTarget.Judge NoisyMatrix must be set!!! call NextGeneration(rand, jrules) before each generation");
            Debug.Assert(p.Size == Size, $"Epistatics/CorrelationMatrixTarget.Judge (size = {p.Size}) must be the same size as the Target (size = {Size})");

            double forcingBenefit = 0.0;
            if (ForcingVector != null)
            {
                forcingBenefit = pv.DotProduct(ForcingVector);
            }

            double provisional = NoisyMatrix.LeftMultiply(pv).DotProduct(pv);
            if (NormaliseFitness)
                return 0.5 + (provisional / (Size * Size)) / 2.0 + forcingBenefit;
            else
                return provisional + forcingBenefit;
        }

        public bool NextGeneration(RandomSource rand, JudgementRules jrules)
        {
            // reset NoisyMatrix
            if (NoisyMatrix == null)
            {
                NoisyMatrix = CorrelationMatrix.Clone();
            }
            else
            {
                CorrelationMatrix.CopyTo(NoisyMatrix);
            }

            // if κ != 0, then add gaussian noise to NoisyVector
            if (jrules.NoiseFactor != 0)
            {
                NoisyMatrix.MapInplace(d => d + MathNet.Numerics.Distributions.Normal.Sample(rand, 0, 1) * jrules.NoiseFactor, Linear.Zeros.Include);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            return false;
        }
    }

    public struct Pair<T>
    {
        public Pair(T a, T b)
        {
            A = a;
            B = b;
        }

        public T A { get; }
        public T B { get; }
    }

    public static class StandardCorrelationMatrixTargets
    {
        public static IEnumerable<MatrixEntryAddress> EnumerateUnhappyCorrelations(Linear.Matrix<double> constraintMatrix, Linear.Vector<double> vector)
        {
            foreach (var e in MatrixEntryAddress.CompleteLazy(constraintMatrix.RowCount, constraintMatrix.ColumnCount))
            {
                var correlation = constraintMatrix[e.Row, e.Col];
                if (correlation != 0.0)
                {
                    var vr = vector[e.Row];
                    var vc = vector[e.Col];

                    if (Math.Sign(vr * vc) != Math.Sign(correlation))
                        yield return e;
                }
            }
        }

        /// <summary>
        /// Creates a LinearLocalConstraints correlation matrix target.
        /// </summary>
        /// <param name="width">One dimension.</param>
        /// <param name="height">The other dimension.</param>
        /// <param name="normaliseFitness">Whether to normalise fitness.</param>
        public static CorrelationMatrixTarget LinearLocalConstraints(int width, int height, IMatrixPattern matrixPattern, NeighbourType neighbourType, bool torus, bool normaliseFitness)
        {
            Linear.Matrix<double> pattern = matrixPattern.Generate(height, width);

            var correlationMatrix = CreateCorrelationMatrix(pattern, Neighbours.GetNeightbourFilter(neighbourType, torus));
            return new CorrelationMatrixTarget(correlationMatrix, $"Linear{matrixPattern.Name}Local{neighbourType}Constraints{width}x{height}{(torus ? "Torus" : "")}{(normaliseFitness ? "Nrm" : "")}", normaliseFitness);
        }

        /// <summary>
        /// Creates a ConcentricSquares correlation matrix target.
        /// </summary>
        /// <param name="size">The dimension of the (square) pattern matrix.</param>
        /// <param name="normaliseFitness">Whether to normalise fitness.</param>
        public static CorrelationMatrixTarget ConcentricSquares(int size, bool normaliseFitness)
        {
            int half = size / 2;

            double p(int r, int c)
            {
                int v = r >= half ? size - r - 1 : r;
                int h = c >= half ? size - c - 1 : c;

                int m = Math.Min(v, h);
                return m % 2 == 0 ? +1.0 : -1.0;
            }

            Linear.Matrix<double> pattern = Linear.CreateMatrix.Dense<double>(size, size, p);

            var correlationMatrix = CreateCorrelationMatrix(pattern, Neighbours.FiveNeighbourFilter);
            return new CorrelationMatrixTarget(correlationMatrix, "ConcentricSquares" + size + (normaliseFitness ? "Nrm" : ""), normaliseFitness);
        }

        /// <summary>
        /// Creates a PartialConcentricSquares correlation matrix target, where each constraint is removed with probability pruneProbability.
        /// </summary>
        /// <param name="size">The dimension of the (square) pattern matrix.</param>
        /// <param name="normaliseFitness">Whether to normalise fitness.</param>
        /// <param name="pruneProbability">The probability of pruning each entry in the problem matrix.</param>
        /// <param name="rand">The random source to use.</param>
        public static CorrelationMatrixTarget PartialConcentricSquares(int size, bool normaliseFitness, double pruneProbability, RandomSource rand)
        {
            var cs = ConcentricSquares(size, normaliseFitness);
            var prunedMatrix = Misc.Prune(rand, cs.CorrelationMatrix, pruneProbability);
            return new CorrelationMatrixTarget(prunedMatrix, "ConcentricSquares" + size + (normaliseFitness ? "Nrm" : "") + "Partial" + pruneProbability, normaliseFitness);
        }

        /// <summary>
        /// Creates a random contraint correlation matrix target.
        /// </summary>
        /// <param name="size">The dimension of the correlation matrix.</param>
        /// <param name="offDiagonalMag">The maximum magnitude of the off-diagonal matrix elements</param>
        /// <param name="noiseType">The type of noise on the off-diagonal matrix elements</param>
        /// <param name="normaliseFitness">Whether to normalise fitness.</param>
        public static CorrelationMatrixTarget RandomInconsistentConstraintTarget(RandomSource rand, int size, double offDiagonalMag, NoiseType noiseType, bool normaliseFitness)
        {
            double corr(int r, int c)
            {
                if (r == c)
                    return 1.0; // 1 on the diagonal

                return Misc.NextNoise(rand, offDiagonalMag, noiseType);
            }

            var constraintMatrix = Linear.CreateMatrix.Dense<double>(size, size, corr);
            return new CorrelationMatrixTarget(constraintMatrix, "RandomInconsistent" + size + (normaliseFitness ? "Nrm" : ""), normaliseFitness);
        }

        /// <summary>
        /// Creates a 'consistent' correlation matrix from the given matrix pattern.
        /// Only correlations which pass the filter will be included in the (n*m)*(n*m) correlation matrix.
        /// </summary>
        /// <param name="pattern">The n*m pattern matrix to correlate.</param>
        /// <param name="filter">The correlation filter.</param>
        /// <returns></returns>
        public static Linear.Matrix<double> CreateCorrelationMatrix(Linear.Matrix<double> pattern, CorrelationFilter filter)
        {
            int width = pattern.ColumnCount;
            int size = pattern.ColumnCount * pattern.RowCount;

            double corr(int r, int c)
            {
                var e0 = new MatrixEntryAddress(r / width, r % width);
                var e1 = new MatrixEntryAddress(c / width, c % width);

                if (filter(pattern.RowCount, pattern.ColumnCount, e0, e1))
                {
                    return pattern[e0.Row, e0.Col] * pattern[e1.Row, e1.Col];
                }
                else
                {
                    return 0.0;
                }
            }

            var mat = Linear.CreateMatrix.Dense<double>(size, size, corr);
            return mat;
        }

        /// <summary>
        /// Creates a ContinousHiff correlation matrix target.
        /// </summary>
        /// <param name="levels">The number of levels.</param>
        /// <param name="normaliseFitness">Whether to normalise fitness.</param>
        /// <param name="downFactor">The factor by which constraints are multiplied each level.</param>
        /// <param name="chaff">Whether to 'checkerboard' the matrix.</param>
        public static CorrelationMatrixTarget ContinuousHiff(int levels, bool normaliseFitness, double downFactor, bool chaff)
        {
            int size = 1 << (levels);

            Linear.Matrix<double> chiff = Linear.CreateMatrix.Dense<double>(size, size, 0.0);

            double p = 1.0;
            int k = 1;

            for (int l = 0; l < levels + 1; l++)
            {
                for (int i = 0; i < size; i += k)
                {
                    for (int r = i; r < i + k; r++)
                    {
                        for (int c = i; c < i + k; c++)
                        {
                            if (chiff[r, c] == 0.0)
                                chiff[r, c] = p;
                        }
                    }
                }

                if (l != 0)
                    p *= downFactor;
                k *= 2;
            }

            if (chaff)
            {
                for (int r = 0; r < size; r++)
                {
                    for (int c = 0; c < size; c++)
                    {
                        if ((r + c) % 2 == 1)
                            chiff[r, c] = -chiff[r, c];
                    }
                }
            }

            return new CorrelationMatrixTarget(chiff, "ContinuousHiff" + size + "DF" + downFactor, normaliseFitness);
        }

        /// <summary>
        /// Creates a Flat correlation matrix target.
        /// Diagonal has all ones. Everything else has value agreement.
        /// </summary>
        /// <param name="size">The width and height of the matrix.</param>
        /// <param name="normaliseFitness">Whether to normalise fitness.</param>
        /// <param name="agreement">The non-diagonal entry value.</param>
        public static CorrelationMatrixTarget Flat(int size, bool normaliseFitness, double agreement)
        {
            Linear.Matrix<double> flat = Linear.CreateMatrix.Dense<double>(size, size, (i, j) => i == j ? 1.0 : agreement);
            return new CorrelationMatrixTarget(flat, "Flat" + size + "Ag" + agreement, normaliseFitness);
        }

        /// <summary>
        /// Creates a Funky MC correlation matrix target.
        /// </summary>
        /// <param name="ascendingModuleCount">The number of 'ascending' modules.</param>
        /// <param name="normaliseFitness">Whether to normalise fitness.</param>
        /// <param name="agreement">The inter-module interaction strength.</param>
        public static CorrelationMatrixTarget FunkyMcAscending(int ascendingModuleCount, bool normaliseFitness, double agreement)
        {
            var assignments = new List<List<int>>();
            int o = 0;
            for (int i = 1; i <= ascendingModuleCount; i++)
            {
                var module = new List<int>();
                for (int j = 0; j < i ; j++)
                {
                    module.Add(o++);
                }
                assignments.Add(module);
            }
            var modules = new Modular.Modules(assignments);
            return FunkyMc(modules, normaliseFitness, agreement);
        }

        /// <summary>
        /// Creates a Funky MC correlation matrix target.
        /// </summary>
        /// <param name="modules">The modules.</param>
        /// <param name="normaliseFitness">Whether to normalise fitness.</param>
        /// <param name="agreement">The inter-module interaction strength.</param>
        public static CorrelationMatrixTarget FunkyMc(Modular.Modules modules, bool normaliseFitness, double agreement)
        {
            int size = modules.ElementCount;
            Linear.Matrix<double> funkymc = Linear.CreateMatrix.Dense<double>(size, size, agreement);

            foreach (var module in modules.ModuleAssignments)
            {
                for (int i = 0; i < module.Count; i++)
                {
                    int r = module[i];
                    for (int j = 0; j < module.Count; j++)
                    {
                        int c = module[j];
                        funkymc[r,c] = 1.0;
                    }
                }
            }

            return new CorrelationMatrixTarget(funkymc, "FunkyMC" + modules?.ToString() + "Ag" + agreement, normaliseFitness);
        }
    }
}
