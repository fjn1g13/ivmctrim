﻿using System;
using System.Collections.Generic;
using System.Linq;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M.Epistatics
{
    public static class Sudoku
    {
        /// <summary>
        /// takes 0+, maps to -1+ (i.e. 0 (blank) -> -1, 1 -> 0, n -> n-1)
        /// -1 corresponds to blank
        /// </summary>
        public static int[] ParsePartialSolution(string str)
        {
            int[] ints;

            if (str.Contains(";") || str.Contains(","))
            {
                ints = str.Split(new[] { ',', ';' }).Select(int.Parse).ToArray();
            }
            else
            {
                ints = str.Select(c => c - '0').ToArray();
            }

            int n = (int)Math.Round(Math.Sqrt(ints.Length));

            int d = (int)Math.Sqrt(n);

            if (d * d != n)
                throw new ArgumentException("N must be a square number", nameof(n));

            return ints.Select(i => i - 1).ToArray();
        }

        public static Linear.Vector<double> PrepareForcingVector(int[] partialSolution, bool full)
        {
            int n = (int)Math.Round(Math.Sqrt(partialSolution.Length));

            int d = (int)Math.Sqrt(n);

            if (d * d != n)
                throw new ArgumentException("N must be a square number", nameof(n));

            int dim = n * n * n;

            var forcingVector = Linear.CreateVector.Dense<double>(dim);

            for (int i = 0; i < partialSolution.Length; i++)
            {
                if (partialSolution[i] < 0)
                    continue;

                if (full)
                {
                    for (int j = 0; j < n; j++)
                    {
                        forcingVector[i + j] = -1;
                    }
                }

                forcingVector[i * n + partialSolution[i]] = 1;
            }

            return forcingVector;
        }

        public static Linear.Matrix<double> GeneralSudoku(int n, double diagonalCoef, double constraintCoef, bool additiveConstaints)
        {
            int d = (int)Math.Sqrt(n);

            if (d * d != n)
                throw new ArgumentException("N must be a square number", nameof(n));

            int dim = n * n * n;

            Linear.Matrix<double> cmat = Linear.CreateMatrix.Dense<double>(dim, dim);

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    if (i == j)
                    {
                        cmat[i, j] = diagonalCoef;
                        continue;
                    }

                    int ci = i / n;
                    int cj = j / n;

                    int constraintCount = 0;
                    if (ci == cj) // same cell
                    {
                        constraintCount++;

                        // don't accumulate
                        goto constrain;
                    }

                    int m = i % n;
                    int o = j % n;

                    if (m != o) // different number
                        goto constrain;

                    int cxi = ci % n;
                    int cxj = cj % n;

                    if (cxi == cxj) // same col
                        constraintCount++;

                    int cyi = ci / n;
                    int cyj = cj / n;

                    if (cyi == cyj) // same row
                        constraintCount++;

                    int bxi = cxi / d;
                    int bxj = cxj / d;

                    int byi = cyi / d;
                    int byj = cyj / d;

                    if (bxi == bxj && byi == byj) // same block
                        constraintCount++;

                    constrain:
                    cmat[i, j] = additiveConstaints
                        ? constraintCount * constraintCoef
                        : Math.Sign(constraintCount) * constraintCoef;
                }
            }

            return cmat;
        }

        public static int[,] RenderSolution(Linear.Vector<double> densities, out int n)
        {
            var arr = densities.ToArray();

            int dim = densities.Count;

            n = (int)Math.Round(Math.Pow(dim, 1 / 3.0));

            int d = (int)Math.Round(Math.Sqrt(n));

            if (d * d != n)
                throw new ArgumentException("N must be a square number", nameof(n));

            var solution = new int[n, n];

            for (int r = 0; r < n; r++)
            {
                for (int c = 0; c < n; c++)
                {
                    if (new ArraySegment<double>(arr, r * n * n + c * n, n).All(x => x == 0))
                        solution[r, c] = -1;
                    else
                        solution[r, c] = Misc.IndexMax(new ArraySegment<double>(arr, r * n * n + c * n, n));
                }
            }

            return solution;
        }

        public static List<SudokuConflict> LocateConflicts(int[,] solution)
        {
            int n = solution.GetLength(0);

            int d = (int)Math.Round(Math.Sqrt(n));

            if (d * d != n)
                throw new ArgumentException("N must be a square number", nameof(n));

            var seen = Misc.Create<List<MatrixEntryAddress>>(n, i => new List<MatrixEntryAddress>());
            bool conflict = false;

            void reset()
            {
                conflict = false;

                for (int i = 0; i < seen.Length; i++)
                {
                    seen[i].Clear();
                }
            }

            void set(int i, MatrixEntryAddress mea)
            {
                if (i < 0)
                    return;

                var l = seen[i];

                l.Add(mea);

                if (l.Count > 1)
                {
                    conflict = true;
                }
            }

            var conflicts = new List<SudokuConflict>();

            void accumulate(SudokuConflictType type, string prefix)
            {
                if (!conflict)
                    return;

                for (int i = 0; i < seen.Length; i++)
                {
                    if (seen[i].Count > 1)
                    {
                        conflicts.Add(new SudokuConflict($"{prefix}{i}", type, seen[i].ToArray()));
                    }
                }
            }

            // for each row
            for (int r = 0; r < n; r++)
            {
                reset();

                for (int c = 0; c < n; c++)
                {
                    set(solution[r, c], new MatrixEntryAddress(r, c));
                }

                if (conflict)
                {
                    accumulate(SudokuConflictType.Row, $"Row{r}: ");
                }
            }

            // for each column
            for (int c = 0; c < n; c++)
            {
                reset();

                for (int r = 0; r < n; r++)
                {
                    set(solution[r, c], new MatrixEntryAddress(r, c));
                }

                if (conflict)
                {
                    accumulate(SudokuConflictType.Column, $"Col{c}: ");
                }
            }

            // for each block
            for (int ro = 0; ro < d; ro++)
            {
                for (int co = 0; co < d; co++)
                {
                    reset();

                    for (int ri = 0; ri < d; ri++)
                    {
                        for (int ci = 0; ci < d; ci++)
                        {
                            var r = ro * d + ri;
                            var c = co * d + ci;
                            set(solution[r, c], new MatrixEntryAddress(r, c));
                        }
                    }

                    if (conflict)
                    {
                        accumulate(SudokuConflictType.Block, $"Block{ro},{co}: ");
                    }
                }
            }

            // for each entry
            for (int c = 0; c < n; c++)
            {
                for (int r = 0; r < n; r++)
                {
                    if (solution[r, c] < 0)
                    {
                        conflicts.Add(new SudokuConflict($"Unassigned{solution[r, c]}", SudokuConflictType.UnassignedCell, new[] { new MatrixEntryAddress(r, c) }));
                    }
                }
            }

            return conflicts;
        }
    }

    public enum SudokuConflictType
    {
        Block,
        Row,
        Column,
        UnassignedCell,
    }

    public class SudokuConflict
    {
        public SudokuConflict(string description, SudokuConflictType type, IReadOnlyList<MatrixEntryAddress> entries)
        {
            Description = description ?? throw new ArgumentNullException(nameof(description));
            Type = type;
            Entries = entries ?? throw new ArgumentNullException(nameof(entries));
        }

        public string Description { get; }
        public SudokuConflictType Type { get; }
        public IReadOnlyList<MatrixEntryAddress> Entries { get; }
    }

    public class SudokuFeedback
    {
        public SudokuFeedback(int resolution, int plotPeriod)
        {
            Resolution = resolution;
            PlotPeriod = plotPeriod;

            SudokuScoreSamples = new List<int>();
            SudokuConflictCountSamples = new List<int>();
        }

        public int Resolution { get; }
        public int PlotPeriod { get; }

        private List<int> SudokuScoreSamples { get; }
        private List<int> SudokuConflictCountSamples { get; }

        public void Attach(IDensePopulationExperimentFeedback densePopulationExperimentFeedback)
        {
            var feedback = densePopulationExperimentFeedback.Feedback;

            feedback.EndTarget.Register(EndTarget);
            feedback.Finished.Register(Finished);
            feedback.EndEpoch.Register(EndEpoch);
        }

        private void EndTarget(FileStuff stuff, Population<DenseIndividual> population, ITarget target, int epoch, long generationCount, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
        {
            // record samples
            if (epoch % Resolution == 0)
            {
                var sudokuScore = -1;
                SudokuScoreSamples.Add(sudokuScore);

                var p = oldPopulationJudgements[0].Individual.Phenotype;
                var sol = Sudoku.RenderSolution(p.Vector, out int n);
                SudokuConflictCountSamples.Add(Sudoku.LocateConflicts(sol).Count);
            }
        }

        private void Finished(FileStuff stuff, Population<DenseIndividual> population, int epochCount, long generationCount)
        {
            // write out
            SaveSudokuSamples(stuff, "_t");
        }

        private void EndEpoch(FileStuff stuff, Population<DenseIndividual> population, int epochCount, long generationCount, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
        {
            if (epochCount % PlotPeriod == 0)
            {
                SaveSudokuSamples(stuff, "" + epochCount);
            }
        }

        private void SaveSudokuSamples(FileStuff stuff, string name)
        {
            double[][] sudokuSamples = new double[][] {
                SudokuScoreSamples.Select(x => (double)x).ToArray(),
                SudokuConflictCountSamples.Select(x => (double)x).ToArray()
            };
            Analysis.SaveTrajectories(stuff.File("sudokusamples" + name + ".dat"), sudokuSamples, Resolution);
        }
    }
}
