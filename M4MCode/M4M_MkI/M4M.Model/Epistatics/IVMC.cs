﻿using M4M.State;
using MathNet.Numerics.Random;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M.Epistatics
{
    [StateClass]
    public class ConstantIvmc : VectorTarget
    {
        [Obsolete]
        protected ConstantIvmc() : base()
        { }

        public ConstantIvmc(int N, int K, double V, Linear.Vector<double> target, bool[] lowModules, bool[] highModules, string friendlyName)
            : base(target, friendlyName, new ConstantIvmcVectorTargetJudger(N, K, V, lowModules, highModules), false)
        {
            Debug.Assert(N * K == target.Count, $"Epistatics/ConstantIvmc.ctor(Vector<double>) target vector must have length N*K={N * K}");
            Debug.Assert(K == lowModules.Length, $"Epistatics/ConstantIvmc.ctor(Vector<double>) low-modules vector must have length K={K}");
            Debug.Assert(K == highModules.Length, $"Epistatics/ConstantIvmc.ctor(Vector<double>) high-modules vector must have length K={K}");
        }

        public ConstantIvmc(int N, int K, double V, string target, string lowModules, string highModules, string friendlyName)
            : base(target, friendlyName, new ConstantIvmcVectorTargetJudger(N, K, V, lowModules, highModules), false)
        {
            Debug.Assert(N * K == target.Length, $"Epistatics/ConstantIvmc.ctor(string) target string must have length N*K={N * K}");
            Debug.Assert(K == lowModules.Length, $"Epistatics/ConstantIvmc.ctor(string) low-modules string must have length K={K}");
            Debug.Assert(K == highModules.Length, $"Epistatics/ConstantIvmc.ctor(string) high-modules string must have length K={K}");
        }
    }

    [StateClass]
    public class ConstantIvmcVectorTargetJudger : IVectorTargetJudger
    {
        public ConstantIvmcVectorTargetJudger(int n, int k, double v, string lowModules, string highModules)
            : this(n, k, v,
                  lowModules.Select(c => c == 'T' || c == 't' || c == '1').ToArray(),
                  highModules.Select(c => c == 'T' || c == 't' || c == '1').ToArray())
        {
        }

        public ConstantIvmcVectorTargetJudger(int n, int k, double v, bool[] lowModules, bool[] highModules)
        {
            N = n;
            K = k;
            V = v;

            LowModules = lowModules;
            HighModules = highModules;

            string lms = string.Join("", LowModules.Select(enabled => enabled ? "1" : "0"));
            string hms = string.Join("", HighModules.Select(enabled => enabled ? "1" : "0"));

            Name = $"ConstantIvmcVectorTargetJudger(N={N}, K={K}, V={V}, Low={lms}, High={hms})";
        }

        [Obsolete]
        protected ConstantIvmcVectorTargetJudger()
        { }

        [SimpleStateProperty("Name")]
        public string Name { get; private set; }

        public string Description => $"ConstantIvmc VectorTargetJudger (Number of modules={N}, Modules size={K}, Highness bias={V}, LowModules={string.Join("", LowModules.Select(b => b ? "1" : "0"))}, HighModules={string.Join("", HighModules.Select(b => b ? "1" : "0"))})";

        /// <summary>
        /// Number of modules (NOT number of traits)
        /// </summary>
        [SimpleStateProperty("N")]
        public int N { get; private set; }

        /// <summary>
        /// Module size
        /// </summary>
        [SimpleStateProperty("K")]
        public int K { get; private set; }

        /// <summary>
        /// High-module fitness bias
        /// </summary>
        [SimpleStateProperty("V")]
        public double V { get; private set; }

        /// <summary>
        /// Whether the low (by trait0 -) modules are enabled
        /// </summary>
        [SimpleStateProperty("LowModules")]
        public IReadOnlyList<bool> LowModules { get; private set; }
        
        /// <summary>
        /// Whether the high (by trait0 +) modules are enabled
        /// </summary>
        [SimpleStateProperty("HighModules")]
        public IReadOnlyList<bool> HighModules { get; private set; }
        
        public double Judge(Linear.Vector<double> target, Phenotype phenotype)
        {
            double fitnessAcc = 0;

            for (int mi = 0; mi < N * K; mi += K)
            {
                double acc = 0;
                for (int i = mi; i < mi + K; i++)
                    acc += phenotype[i] * target[i];

                acc /= K; // average
                acc *= target[mi];
                
                double moduleFitness = 0.0;

                if (HighModules[mi / K])
                {
                    double highbonus = target[mi] > 0 ? V : 1.0;
                    double highness = 0.5 + acc * 0.5;
                    moduleFitness += highness * highness * highbonus;
                }
                
                if (LowModules[mi / K])
                {
                    double lowbonus = target[mi] < 0 ? V : 1.0;
                    double lowness = 0.5 - acc * 0.5;
                    moduleFitness += lowness * lowness * lowbonus;
                }
                
                fitnessAcc += moduleFitness;
            }

            return fitnessAcc / N;
        }
    }
    
    // This class is badly named now.
    // Sorry.
    [StateClass]
    public class ExpIvmc : VectorTarget
    {
        [Obsolete]
        protected ExpIvmc() : base()
        { }
        
        /// <summary>
        /// Number of modules (NOT number of traits)
        /// </summary>
        [SimpleStateProperty("N")]
        public int N { get; private set; }

        /// <summary>
        /// Module size
        /// </summary>
        [SimpleStateProperty("K")]
        public int K { get; private set; }

        /// <summary>
        /// High-module fitness bias
        /// </summary>
        [SimpleStateProperty("V")]
        public double V { get; private set; }
        
        /// <summary>
        /// Dual Resource Probability
        /// </summary>
        [SimpleStateProperty("Z")]
        public double Z { get; private set; }

        /// <summary>
        /// Whether the low (by trait0 -) modules are enabled
        /// </summary>
        [SimpleStateProperty("LowModules")]
        public bool[] LowModules { get; private set; }
        
        /// <summary>
        /// Whether the high (by trait0 +) modules are enabled
        /// </summary>
        [SimpleStateProperty("HighModules")]
        public bool[] HighModules { get; private set; }

        /// <summary>
        /// Whether the high modules are inverted
        /// </summary>
        [SimpleStateProperty("HighModuleInversions")]
        public bool[] HighModuleInversions { get; private set; }
        
        /// <summary>
        /// The probability with which to change high modules invertions with each exposure
        /// </summary>
        [SimpleStateProperty("ChangeInversionProbability")]
        public double ChangeInversionProbability { get; private set; }

        public ExpIvmc(int N, int K, double V, double Z, double F, Linear.Vector<double> target, bool[] lowModules, bool[] highModules, string friendlyName, double changeInversionProbability)
            : base(target, friendlyName, new ExpIvmcVectorTargetJudger(), false)
        {
            Debug.Assert(N * K == target.Count, $"Epistatics/ExpIvmc.ctor(Vector<double>) target vector must have length N*K={N * K}");
            Debug.Assert(K == lowModules.Length, $"Epistatics/ExpIvmc.ctor(Vector<double>) low-modules vector must have length K={K}");
            Debug.Assert(K == highModules.Length, $"Epistatics/ExpIvmc.ctor(Vector<double>) high-modules vector must have length K={K}");

            ((ExpIvmcVectorTargetJudger)CustomJudger).Ivmc = this; // horrid

            this.N = N;
            this.K = K;
            this.V = V;
            this.Z = Z;
            LowModules = lowModules;
            HighModules = highModules;

            ChangeInversionProbability = changeInversionProbability;
            HighModuleInversions = new bool[N]; // (false)
        }
        
        public ExpIvmc(int N, int K, double V, double Z, string target, string lowModules, string highModules, string friendlyName, double changeInversionProbability)
            : base(target, friendlyName, new ExpIvmcVectorTargetJudger(), false)
        {
            Debug.Assert(N * K == target.Length, $"Epistatics/ExpIvmc.ctor(string) target string must have length N*K={N * K}");
            Debug.Assert(K == lowModules.Length, $"Epistatics/ExpIvmc.ctor(string) low-modules string must have length K={K}");
            Debug.Assert(K == highModules.Length, $"Epistatics/ExpIvmc.ctor(string) high-modules string must have length K={K}");

            ((ExpIvmcVectorTargetJudger)CustomJudger).Ivmc = this; // horrid

            this.N = N;
            this.K = K;
            this.V = V;
            this.Z = Z;
            LowModules = lowModules.Select(c => c == 'T' || c == 't' || c == '1').ToArray();
            HighModules = highModules.Select(c => c == 'T' || c == 't' || c == '1').ToArray();

            ChangeInversionProbability = changeInversionProbability;
            HighModuleInversions = new bool[N]; // (false)
        }
        
        public override bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            base.NextExposure(rand, jrules, epoch, ref exposureInformation);
            
            for (int i = 0; i < HighModules.Length; i++)
            {
                bool dual = rand.NextDouble() < Z;
                bool high = rand.NextBoolean();

                HighModules[i] = dual || high;
                LowModules[i] = dual || (!high);
            }

            if (rand.NextDouble() < ChangeInversionProbability)
            {
                for (int i = 0; i < HighModuleInversions.Length; i++)
                    HighModuleInversions[i] = rand.NextBoolean();
            }

            return true;
        }
    }
    
    [StateClass]
    public class ExpIvmcVectorTargetJudger : IVectorTargetJudger
    {
        public ExpIvmcVectorTargetJudger(ExpIvmc expIvmc)
        {
            Ivmc = expIvmc;
            Name = "ExpIvmcVectorTargetJudger";
        }

        public ExpIvmcVectorTargetJudger()
        {
            Ivmc = null;
            Name = "ExpIvmcVectorTargetJudger";
        }

        [SimpleStateProperty("ExpIvmc")]
        public ExpIvmc Ivmc { get; set; } // this is awful
        
        [SimpleStateProperty("Name")]
        public string Name { get; private set; }
        
        public string Description => "ExpIvmcVectorTargetJudger";

        public double Judge(Linear.Vector<double> target, Phenotype phenotype)
        {
            double benefitAcc = 0;

            for (int mi = 0; mi < Ivmc.N * Ivmc.K; mi += Ivmc.K)
            {
                double acc = 0;
                for (int i = mi; i < mi + Ivmc.K; i++)
                    acc += phenotype[i] * target[i];

                acc /= Ivmc.K; // average
                acc *= target[mi]; // TODO: FIXME: this is wrong... thankfully we've never varied... is it wrong?????? it doesn't look wrong...
                
                double moduleFitness = 0.0;

                bool highIsV = Ivmc.HighModuleInversions == null // backwards compatibility
                    ? target[mi / Ivmc.K] > 0
                    : (target[mi / Ivmc.K] > 0) != Ivmc.HighModuleInversions[mi / Ivmc.K];
                
                if (Ivmc.HighModules[mi / Ivmc.K])
                {
                    double highbonus = highIsV ? Ivmc.V : 1.0;
                    double highness = 0.5 + acc * 0.5;
                    moduleFitness += highness * highness * highbonus;
                }
                
                if (Ivmc.LowModules[mi / Ivmc.K])
                {
                    double lowbonus = highIsV ? 1.0 : Ivmc.V;
                    double lowness = 0.5 - acc * 0.5;
                    moduleFitness += lowness * lowness * lowbonus;
                }
                
                benefitAcc += moduleFitness;
            }

            return benefitAcc / Ivmc.N;
        }
    }
    
    // ExpImvc is a nightmare in every way
    // -> let's do this properly...

    [StateClass]
    public class IvmcProper
    {
        [Obsolete]
        protected IvmcProper() : base()
        { }

        public IvmcProper(int moduleCount, int moduleSize) : this (moduleCount, moduleSize, new double[moduleCount], new double[moduleCount])
        {
        }

        public IvmcProper(int moduleCount, int moduleSize, double[] cminus, double[] cplus)
        {
            Debug.Assert(moduleCount == cminus.Length, $"Epistatics/IvmcProper.ctor(int, int, double[], double[]) cminus must have length M={moduleCount}");
            Debug.Assert(moduleCount == cplus.Length, $"Epistatics/IvmcProper.ctor(int, int, double[], double[]) cplus and cmonus must have length M={moduleCount}");

            ModuleCount = moduleCount;
            ModuleSize = moduleSize;
            
            Cminus = cminus;
            Cplus = cplus;

            Modules = null;
        }

        public IvmcProper(Modular.Modules modules) : this(modules, new double[modules.ModuleCount], new double[modules.ModuleCount])
        {
        }

        public IvmcProper(Modular.Modules modules, double[] cminus, double[] cplus)
        {
            Debug.Assert(modules.ModuleCount == cminus.Length, $"Epistatics/IvmcProper.ctor(Modular.Modules, double[], double[]) cminus must have length M={ModuleCount}");
            Debug.Assert(modules.ModuleCount == cplus.Length, $"Epistatics/IvmcProper.ctor(Modular.Modules, double[], double[]) cplus and cmonus must have length M={ModuleCount}");
            
            ModuleCount = modules.ModuleCount;
            ModuleSize = -1;
            
            Cminus = cminus;
            Cplus = cplus;

            Modules = modules;
        }

        /// <summary>
        /// Per-module Negative match fitnesses
        /// </summary>
        [SimpleStateProperty("Cminus")]
        public double[] Cminus { get; private set; }

        /// <summary>
        /// Per-module Positive match fitnesses
        /// </summary>
        [SimpleStateProperty("Cplus")]
        public double[] Cplus { get; private set; }
        
        /// <summary>
        /// Module Count
        /// defined for both BlockModules and FreeModules configuration
        /// </summary>
        [SimpleStateProperty("ModuleCount")]
        public int ModuleCount { get; private set; }
        
        /// <summary>
        /// Module Size
        /// undefined if not BlockModules configuration
        /// </summary>
        [SimpleStateProperty("ModuleSize")]
        private int ModuleSize { get; set; }

        /// <summary>
        /// Describes the Free modules in the FreeModules configuration
        /// null if not FreeModules configuration
        /// </summary>
        [SimpleStateProperty("Modules")]
        private Modular.Modules Modules { get; set; }

        /// <summary>
        /// Decomposes a BlockModules type IvmcProper
        /// Throws if of the wrong ModuleType
        /// </summary>
        /// <param name="moduleCount">The number of modules</param>
        /// <param name="moduleSize">The size of each module</param>
        public void DecomposeBlockModules(out int moduleCount, out int moduleSize)
        {
            if (ModulesType != IvmcProperModulesType.BlockModules)
                throw new Exception("Attempted to decompose an IvmcProper as BlockModules with differet ModuleType");

            moduleCount = ModuleCount;
            moduleSize = ModuleSize;
        }

        /// <summary>
        /// Decomposes a FreeModules type IvmcProper
        /// Throws if of the wrong ModuleType
        /// </summary
        /// <param name="modules">The Modules</param>
        public void DecomposeFreeModules(out Modular.Modules modules)
        {
            if (ModulesType != IvmcProperModulesType.FreeModules)
                throw new Exception("Attempted to decompose an IvmcProper as BlockModules with differet ModuleType");

            modules = Modules;
        }

        /// <summary>
        /// Returns the type of modules used by this IvmcProper
        /// Use this information to call the appropriate Decompose method
        /// </summary>
        public IvmcProperModulesType ModulesType
        {
            get
            {
                // !!! switching on ModuleType
                return Modules == null ? IvmcProperModulesType.BlockModules : IvmcProperModulesType.FreeModules;
            }
        }

        /// <summary>
        /// Retrieves the total number of elements
        /// </summary>
        public int ElementCount
        {
            get
            {
                // !!! switching on ModuleType
                if (Modules != null)
                    return Modules.ElementCount;
                else
                    return ModuleCount * ModuleSize;
            }
        }

        /// <summary>
        /// no-save
        /// We use this to cache the result of a call to AsModules
        /// </summary>
        private Modular.Modules _generatedModulesCache;

        /// <summary>
        /// Retrieves a Modular.Modules representation of the Module Assignment
        /// Supported for BlockModules and FreeModules
        /// Result is cached
        /// </summary>
        public Modular.Modules ModulesAsFreeModules()
        {
            if (_generatedModulesCache == null)
            {
                // !!! switching on ModuleType
                if (Modules != null)
                {
                    _generatedModulesCache = Modules;
                }
                else
                { // BlockModules
                    _generatedModulesCache = Modular.Modules.CreateBlockModules(ModuleCount, ModuleSize);
                }
            }

            return _generatedModulesCache;
        }
    }

    public enum IvmcProperModulesType
    {
        BlockModules = 0,
        FreeModules = 1,
    }

    // TODO: obsolve this wherever possible
    public enum IvmcProperJudgementMode
    {
        Proper = 0,
        Split = 1,
        StackedSplit = 3,
        Stacked = 4,
    }

    public interface IIvmcProperJudgementPreparer
    {
        string Name { get; }
        string Description { get; }
        IVectorTargetJudger PrepareJudger(IvmcProper proper);
    }

    public class IvmcStackedProperJudgementPreparer : IIvmcProperJudgementPreparer
    {
        public IvmcStackedProperJudgementPreparer(IModuleBenefitFunction moduleBenefitFunction, double decayFactor, bool rescaleFitness, int scaleFactor)
        {
            ModuleBenefitFunction = moduleBenefitFunction ?? throw new ArgumentNullException(nameof(moduleBenefitFunction));
            DecayFactor = decayFactor;
            RescaleFitness = rescaleFitness;
            ScaleFactor = scaleFactor;
        }

        public string Name => "Stacked" + ModuleBenefitFunction.Name;

        public string Description => Name + "Df" + DecayFactor + "Rf" + RescaleFitness;

        public IVectorTargetJudger PrepareJudger(IvmcProper proper)
        {
            return new IvmcStackedVectorTargetJudger(ModuleBenefitFunction, proper, DecayFactor, RescaleFitness, ScaleFactor);
        }

        public IModuleBenefitFunction ModuleBenefitFunction { get; }
        public double DecayFactor { get; }
        public bool RescaleFitness { get; }
        public int ScaleFactor { get; }
    }

    public class TraditionalIvmcProperJudgementPreparer : IIvmcProperJudgementPreparer
    {
        public TraditionalIvmcProperJudgementPreparer(IvmcProperJudgementMode properJudgementMode)
        {
            ProperJudgementMode = properJudgementMode;
        }

        public IvmcProperJudgementMode ProperJudgementMode { get; }

        public string Name => ProperJudgementMode.ToString();

        public string Description => Name;

        public IVectorTargetJudger PrepareJudger(IvmcProper proper)
        {
            switch (ProperJudgementMode)
            {
                case IvmcProperJudgementMode.Proper:
                    return new IvmcProperVectorTargetJudger(proper);
                case IvmcProperJudgementMode.Split:
                    return new IvmcSplitVectorTargetJudger(proper);
                case IvmcProperJudgementMode.StackedSplit:
                    return new IvmcStackedVectorTargetJudger(new SplitModuleBenefitFunction(), proper);
                default:
                    throw new ArgumentException(nameof(ProperJudgementMode), "Illegal ProperJudgementMode (or non-Traditional)");
            }
        }

        public static IvmcProperJudgementMode ParseTraditionalIvmcProperJudgementMode(string name)
        {
            if (string.Equals(name, "Proper", StringComparison.InvariantCultureIgnoreCase))
            {
                return IvmcProperJudgementMode.Proper;
            }
            else if (string.Equals(name, "Split", StringComparison.InvariantCultureIgnoreCase))
            {
                return IvmcProperJudgementMode.Split;
            }
            else if (string.Equals(name, "StackedSplit", StringComparison.InvariantCultureIgnoreCase))
            {
                return IvmcProperJudgementMode.StackedSplit;
            }
            else if (string.Equals(name, "Stacked", StringComparison.InvariantCultureIgnoreCase))
            {
                return IvmcProperJudgementMode.Stacked;
            }
            else if (int.TryParse(name, out int n))
            {
                // for backwards compatability
                return (IvmcProperJudgementMode)n;
            }
            else
            {
                throw new ArgumentException($"Unrecognised Traditional ImvcProperJudgementMode \"{name}\"");
            }
        }
    }

    public interface IIvmcProperTarget
    {
        IvmcProper Proper { get; }
    }

    [StateClass]
    public class IvmcProperSinusoid : VectorTarget, IIvmcProperTarget
    {
        private static string _ds(double[] darr) => string.Join(";", darr);

        public override string Description => $"IvmcProperSinusoid (Size={Size}, NormaliseCustomJudger={NormaliseCustomJudger}, Proper.Modules={Proper.ModulesAsFreeModules().ToString()}, PlusPhases={_ds(PlusPhases)}, MinusPhases={_ds(MinusPhases)}, PlusAmps={_ds(PlusAmps)}, MinusAmps={_ds(MinusAmps)}, PlusFreqs={_ds(PlusFreqs)}, MinusFreqs={_ds(MinusFreqs)}, PlusMeans={_ds(PlusMeans)}, MinusMeans={_ds(MinusMeans)}), (CJ: {CustomJudger?.Description})";

        [Obsolete]
        protected IvmcProperSinusoid() : base()
        { }

        /// <summary>
        /// My IvmcProper instance (seen by the custom judger)
        /// </summary>
        [SimpleStateProperty("Proper")]
        public IvmcProper Proper { get; private set; }

        [SimpleStateProperty("PlusPhases")]
        public double[] PlusPhases { get; private set; }

        [SimpleStateProperty("MinusPhases")]
        public double[] MinusPhases { get; private set; }

        [SimpleStateProperty("PlusAmps")]
        public double[] PlusAmps { get; private set; }

        [SimpleStateProperty("MinusAmps")]
        public double[] MinusAmps { get; private set; }

        [SimpleStateProperty("PlusFreqs")]
        public double[] PlusFreqs { get; private set; }

        [SimpleStateProperty("MinusFreqs")]
        public double[] MinusFreqs { get; private set; }

        [SimpleStateProperty("PlusMeans")]
        public double[] PlusMeans { get; private set; }

        [SimpleStateProperty("MinusMeans")]
        public double[] MinusMeans { get; private set; }

        private IvmcProperSinusoid(IIvmcProperJudgementPreparer properJudgementPreparer, Linear.Vector<double> target, string friendlyName, IvmcProper proper)
            : base(target, friendlyName, properJudgementPreparer.PrepareJudger(proper), false)
        {
            Proper = proper;
        }
        
        
        public IvmcProperSinusoid(IIvmcProperJudgementPreparer properJudgementPreparer, Modular.Modules modules, double[] plusPhases, double[] minusPhases, double[] plusAmps, double[] minusAmps, double[] plusFreqs, double[] minusFreqs, double[] plusMeans, double[] minusMeans, Linear.Vector<double> target, string friendlyName)
            : this(properJudgementPreparer, target, friendlyName, new IvmcProper(modules))
        {
            PlusPhases = plusPhases;
            MinusPhases = minusPhases;
            PlusAmps = plusAmps;
            MinusAmps = minusAmps;
            PlusFreqs = plusFreqs;
            MinusFreqs = minusFreqs;
            PlusMeans = plusMeans;
            MinusMeans = minusMeans;
        }

        public override bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            base.NextExposure(rand, jrules, epoch, ref exposureInformation);

            double twoPi = Math.PI * 2.0;

            for (int mi = 0; mi < Proper.ModuleCount; mi++)
            {
                Proper.Cplus[mi] = PlusMeans[mi] + Math.Sin((epoch + PlusPhases[mi]) * PlusFreqs[mi] * twoPi) * PlusAmps[mi];
                Proper.Cminus[mi] = MinusMeans[mi] + Math.Sin((epoch + MinusPhases[mi]) * MinusFreqs[mi] * twoPi) * MinusAmps[mi];
            }

            return true;
        }
        
        public override Linear.Vector<double> PreparePerfectP()
        {
            if (Proper.ModulesType == IvmcProperModulesType.BlockModules)
            {
                Proper.DecomposeBlockModules(out int M, out int K);

                // clone target vector
                Linear.Vector<double> p = Vector.Clone();

                // flip according to module conditions
                for (int i = 0; i < M * K; i++)
                {
                    if (Proper.Cminus[i / K] > Proper.Cplus[i / K])
                        p[i] = -p[i];
                }

                return p;
            }
            else if (Proper.ModulesType == IvmcProperModulesType.FreeModules)
            {
                Proper.DecomposeFreeModules(out var modules);

                Linear.Vector<double> p = Vector.Clone();
                
                for (int i = 0; i < modules.ModuleCount; i++)
                {
                    if (Proper.Cminus[i] > Proper.Cplus[i])
                    {
                        foreach (var j in modules[i])
                            p[j] = -p[j];
                    }
                }

                return p;
            }
            else
            {
                throw new Exception("Unrecognised IvmcProperModulesType: " + Proper.ModulesType);
            }
        }
    }

    // the only 'problem' with this is that it is over-parameterised
    [StateClass]
    public class IvmcProperChanging : VectorTarget, IIvmcProperTarget
    {
        public override string Description => $"IvmcProper Changing Vector Target (Size={Size}, NormaliseCustomJudger={NormaliseCustomJudger}, CH={CH}, CL={CL}, C0={C0}, Z={Z}, Proper.Modules={Proper.ModulesAsFreeModules().ToString()}, VariationModules={VariationModules?.ToString()}, VariationModulePlusOverrides={VariationModuleOverridesString(VariationModulePlusOverrides)}, VariationModuleMinusOverrides={VariationModuleOverridesString(VariationModuleMinusOverrides)}) (CJ: {CustomJudger?.Description})";

        private static string VariationModuleOverridesString(double[] vmos)
        {
            if (vmos == null)
                return "";
            return string.Join(";", vmos.Select(d => double.IsNaN(d) ? "_" : d.ToString()));
        }

        [Obsolete]
        protected IvmcProperChanging() : base()
        { }

        /// <summary>
        /// Number of modules
        /// </summary>
        [Obsolete]
        [DeprecatedSimpleStateProperty("M")]
        private int M
        {
            get
            {
                throw new InvalidOperationException("Property M is deprecated, and cannot be used for serialisation");
            }
            set
            {
                // use Proper instead
            }
        }

        /// <summary>
        /// Module size
        /// </summary>
        [Obsolete]
        [DeprecatedSimpleStateProperty("K")]
        private int K
        {
            get
            {
                throw new InvalidOperationException("Property K is deprecated, and cannot be used for serialisation");
            }
            set
            {
                // use Proper instead
            }
        }

        /// <summary>
        /// High-module fitness
        /// </summary>
        [SimpleStateProperty("CH")]
        public double CH { get; private set; }

        /// <summary>
        /// Low-module fitness
        /// </summary>
        [SimpleStateProperty("CL")]
        public double CL { get; private set; }

        /// <summary>
        /// No-module fitness
        /// </summary>
        [SimpleStateProperty("C0")]
        public double C0 { get; private set; }

        /// <summary>
        /// Dual Resource Probability
        /// </summary>
        [SimpleStateProperty("Z")]
        public double Z { get; private set; }

        /// <summary>
        /// My IvmcProper instance (seen by the custom judger)
        /// </summary>
        [SimpleStateProperty("Proper")]
        public IvmcProper Proper { get; private set; }
        
        /// <summary>
        /// The modules of modules which vary together
        /// Null impies they all change independently
        /// </summary>
        [SimpleStateProperty("VariationModules")]
        public Modular.Modules VariationModules { get; private set; }
        
        /// <summary>
        /// Unchanging (overriden) variation module plus coefficiencients
        /// </summary>
        [SimpleStateProperty("VariationModulePlusOverrides")]
        public double[] VariationModulePlusOverrides { get; private set; }
        
        /// <summary>
        /// Unchanging (overriden) variation module minus coefficiencients
        /// </summary>
        [SimpleStateProperty("VariationModuleMinusOverrides")]
        public double[] VariationModuleMinusOverrides { get; private set; }

        private IvmcProperChanging(IIvmcProperJudgementPreparer properJudgementPreparer, Linear.Vector<double> target, string friendlyName, IvmcProper proper)
            : base(target, friendlyName, properJudgementPreparer.PrepareJudger(proper), false)
        {
            Proper = proper;
        }
        
        private IvmcProperChanging(IIvmcProperJudgementPreparer properJudgementPreparer, string target, string friendlyName, IvmcProper proper)
            : base(target, friendlyName, properJudgementPreparer.PrepareJudger(proper), false)
        {
            Proper = proper;
        }
        
        public IvmcProperChanging(IIvmcProperJudgementPreparer properJudgementPreparer, int M, int K, double CH, double CL, double C0, double Z, Linear.Vector<double> target, string friendlyName)
            : this(properJudgementPreparer, target, friendlyName, new IvmcProper(M, K))
        {
            Debug.Assert(M * K == target.Count, $"Epistatics/IvmcProperChanging.ctor(Vector<double>, int, int) target vector must have length M*K={M * K}");
            
            this.CH = CH;
            this.CL = CL;
            this.C0 = C0;
            this.Z = Z;

            this.VariationModules = null;
            this.VariationModulePlusOverrides = null;
            this.VariationModuleMinusOverrides = null;
        }
        
        public IvmcProperChanging(IIvmcProperJudgementPreparer properJudgementPreparer, Modular.Modules modules, Modular.Modules variationModules, double[] variationModulePlusOverrides, double[] variationModuleMinusOverrides, double CH, double CL, double C0, double Z, Linear.Vector<double> target, string friendlyName)
            : this(properJudgementPreparer, target, friendlyName, new IvmcProper(modules))
        {
            Debug.Assert(variationModules != null && variationModules.ElementCount == modules.ModuleCount, $"Epistatics/IvmcProperChanging.ctor(Vector<double>, Modular.Modules) variationModules should have the same number of elements as modules has modules: {modules.ModuleCount}");
            Debug.Assert(modules.ElementCount == target.Count, $"Epistatics/IvmcProperChanging.ctor(Vector<double>, Modular.Modules) target vector must have length {modules.ElementCount}");
            
            this.CH = CH;
            this.CL = CL;
            this.C0 = C0;
            this.Z = Z;

            this.VariationModules = variationModules;
            this.VariationModulePlusOverrides = variationModulePlusOverrides;
            this.VariationModuleMinusOverrides = variationModuleMinusOverrides;
        }
        
        public IvmcProperChanging(IIvmcProperJudgementPreparer properJudgementPreparer, int M, int K, double CH, double CL, double C0, double Z, string target, string friendlyName)
            : this(properJudgementPreparer, target, friendlyName, new IvmcProper(M, K))
        {
            Debug.Assert(M * K == target.Length, $"Epistatics/IvmcProperChanging.ctor(string, int, int) target string must have length M*K={M * K}");
            
            this.CH = CH;
            this.CL = CL;
            this.C0 = C0;
            this.Z = Z;

            this.VariationModules = null;
            this.VariationModulePlusOverrides = null;
            this.VariationModuleMinusOverrides = null;
        }
        
        public IvmcProperChanging(IIvmcProperJudgementPreparer properJudgementPreparer, Modular.Modules modules, Modular.Modules variationModules, double[] variationModulePlusOverrides, double[] variationModuleMinusOverrides, double CH, double CL, double C0, double Z, string target, string friendlyName)
            : this(properJudgementPreparer, target, friendlyName, new IvmcProper(modules))
        {
            Debug.Assert(variationModules == null || variationModules.ElementCount == modules.ModuleCount, $"Epistatics/IvmcProperChanging.ctor(string, Modular.Modules) variationModules should have the same number of elements as modules has modules: {modules.ModuleCount}");
            
            this.CH = CH;
            this.CL = CL;
            this.C0 = C0;
            this.Z = Z;

            this.VariationModules = variationModules;
            this.VariationModulePlusOverrides = variationModulePlusOverrides;
            this.VariationModuleMinusOverrides = variationModuleMinusOverrides;
        }

        public override bool NextExposure(RandomSource rand, JudgementRules jrules, int epoch, ref ExposureInformation exposureInformation)
        {
            base.NextExposure(rand, jrules, epoch, ref exposureInformation);

            if (VariationModules == null)
            {
                for (int i = 0; i < Proper.ModuleCount; i++)
                {
                    // use overrides by default
                    int vmi = i;
                    double cplus = VariationModulePlusOverrides != null ? VariationModulePlusOverrides[vmi] : double.NaN;
                    double cminus = VariationModuleMinusOverrides != null ? VariationModuleMinusOverrides[vmi] : double.NaN;

                    // don't generate randoms if we can help it
                    if (double.IsNaN(cplus) || double.IsNaN(cminus))
                    {
                        bool dual = rand.NextDouble() < Z;
                        bool neg = rand.NextBoolean();

                        // replace non-existant overrides
                        if (double.IsNaN(cplus))
                            cplus = !neg ? CH : dual ? CL : C0;
                        if (double.IsNaN(cminus))
                            cminus = neg ? CH : dual ? CL : C0;
                    }

                    // insert
                    Proper.Cplus[i] = cplus;
                    Proper.Cminus[i] = cminus;
                }
            }
            else
            {
                int vmi = 0;
                foreach (var vm in VariationModules.ModuleAssignments)
                {
                    double cplus = VariationModulePlusOverrides != null ? VariationModulePlusOverrides[vmi] : double.NaN;
                    double cminus = VariationModuleMinusOverrides != null ? VariationModuleMinusOverrides[vmi] : double.NaN;

                    // don't generate randoms if we can help it
                    if (double.IsNaN(cplus) || double.IsNaN(cminus))
                    {
                        bool dual = rand.NextDouble() < Z;
                        bool neg = rand.NextBoolean();

                        // replace non-existant overrides
                        if (double.IsNaN(cplus))
                            cplus = !neg ? CH : dual ? CL : C0;
                        if (double.IsNaN(cminus))
                            cminus = neg ? CH : dual ? CL : C0;
                    }

                    // insert
                    foreach (var i in vm)
                    {
                        Proper.Cplus[i] = cplus;
                        Proper.Cminus[i] = cminus;
                    }

                    vmi++;
                }
            }

            return true;
        }
        
        public override Linear.Vector<double> PreparePerfectP()
        {
            if (Proper.ModulesType == IvmcProperModulesType.BlockModules)
            {
                Proper.DecomposeBlockModules(out int M, out int K);

                // clone target vector
                Linear.Vector<double> p = Vector.Clone();

                // flip according to module conditions
                for (int i = 0; i < M * K; i++)
                {
                    if (Proper.Cminus[i / K] > Proper.Cplus[i / K])
                        p[i] = -p[i];
                }

                return p;
            }
            else if (Proper.ModulesType == IvmcProperModulesType.FreeModules)
            {
                Proper.DecomposeFreeModules(out var modules);

                Linear.Vector<double> p = Vector.Clone();
                
                for (int i = 0; i < modules.ModuleCount; i++)
                {
                    if (Proper.Cminus[i] > Proper.Cplus[i])
                    {
                        foreach (var j in modules[i])
                            p[j] = -p[j];
                    }
                }

                return p;
            }
            else
            {
                throw new Exception("Unrecognised IvmcProperModulesType: " + Proper.ModulesType);
            }
        }
    }

    [StateClass]
    public class IvmcProperVectorTargetJudger : IVectorTargetJudger
    {
        [Obsolete]
        protected IvmcProperVectorTargetJudger()
        {
        }

        public IvmcProperVectorTargetJudger(IvmcProper proper)
        {
            Proper = proper;
            Name = "IvmcProperVectorTargetJudger";
        }

        [SimpleStateProperty("Proper")]
        public IvmcProper Proper { get; private set; }
        
        [SimpleStateProperty("Name")]
        public string Name { get; private set; }

        public string Description => "IvmcProperVectorTargetJudger";
        
        public double Judge(Linear.Vector<double> target, Phenotype phenotype)
        {
            if (Proper.ModulesType == IvmcProperModulesType.BlockModules)
            {
                Proper.DecomposeBlockModules(out int M, out int K);
                
                double fitnessAcc = 0;

                for (int i = 0; i < M * K; i += K)
                {
                    double acc = 0;
                    for (int j = i; j < i + K; j++)
                        acc += phenotype[j] * target[j];

                    acc /= K; // average

                    double moduleFitness = 0.0;

                    double positiveness = 0.5 + acc * 0.5;
                    moduleFitness += positiveness * positiveness * Proper.Cplus[i / K];

                    double negativeness = 0.5 - acc * 0.5;
                    moduleFitness += negativeness * negativeness * Proper.Cminus[i / K];

                    fitnessAcc += moduleFitness;
                }

                return fitnessAcc / M; // mean fitness of all modules
            }
            else if (Proper.ModulesType == IvmcProperModulesType.FreeModules)
            {
                Proper.DecomposeFreeModules(out var modules);
                
                double fitnessAcc = 0;

                for (int i = 0; i < modules.ModuleCount; i++)
                {
                    var m = modules[i];

                    double acc = 0;
                    foreach (var j in m)
                        acc += phenotype[j] * target[j];

                    acc /= m.Count; // average

                    double moduleFitness = 0.0;

                    double positiveness = 0.5 + acc * 0.5;
                    moduleFitness += positiveness * positiveness * Proper.Cplus[i];

                    double negativeness = 0.5 - acc * 0.5;
                    moduleFitness += negativeness * negativeness * Proper.Cminus[i];

                    fitnessAcc += moduleFitness;
                }

                return fitnessAcc / modules.ModuleCount; // mean fitness of all modules
            }
            else
            {
                throw new Exception("Unrecognised IvmcProperModulesType: " + Proper.ModulesType);
            }
        }
    }
    
    [StateClass]
    public class IvmcSplitVectorTargetJudger : IVectorTargetJudger
    {
        [Obsolete]
        protected IvmcSplitVectorTargetJudger()
        {
        }

        public IvmcSplitVectorTargetJudger(IvmcProper proper)
        {
            Proper = proper;
            Name = "IvmcSplitVectorTargetJudger";
        }

        [SimpleStateProperty("Proper")]
        public IvmcProper Proper { get; private set; }
        
        [SimpleStateProperty("Name")]
        public string Name { get; private set; }
        
        public string Description => "IvmcSplitVectorTargetJudger";

        public double Judge(Linear.Vector<double> target, Phenotype phenotype)
        {
            if (Proper.ModulesType == IvmcProperModulesType.BlockModules)
            {
                Proper.DecomposeBlockModules(out int M, out int K);

                double benefitAcc = 0;

                for (int i = 0; i < M * K; i += K)
                {
                    double acc = 0;
                    for (int j = i; j < i + K; j++)
                        acc += phenotype[j] * target[j];

                    acc /= K; // average

                    double moduleBenefit = acc > 0
                        ? acc * Proper.Cplus[i / K]
                        : -acc * Proper.Cminus[i / K];

                    benefitAcc += moduleBenefit;
                }

                return benefitAcc / M; // mean fitness of all modules
            }
            else if (Proper.ModulesType == IvmcProperModulesType.FreeModules)
            {
                Proper.DecomposeFreeModules(out var modules);
                
                double benefitAcc = 0;

                for (int i = 0; i < modules.ModuleCount; i++)
                {
                    var m = modules[i];

                    double acc = 0;
                    foreach (int j in m)
                        acc += phenotype[j] * target[j];

                    acc /= m.Count; // average

                    double moduleBenefit = acc > 0
                        ? acc * Proper.Cplus[i]
                        : -acc * Proper.Cminus[i];

                    benefitAcc += moduleBenefit;
                }

                return benefitAcc / modules.ModuleCount; // mean fitness of all modules
            }
            else
            {
                throw new Exception("Unrecognised IvmcProperModulesType: " + Proper.ModulesType);
            }
        }
    }

    public class IvmcProperFeedback
    {
        public IvmcProperFeedback(int resolution, int plotPeriod, bool recordPropers, int moduleCount)
        {
            Resolution = resolution;
            PlotPeriod = plotPeriod;
            RecordPropers = recordPropers;

            ModuleCount = moduleCount;

            SwitchCountSamples = new List<int>();
            SolveCountSamples = new List<int>();
            PerModuleSwitchCountSamples = Misc.Create<List<int>>(moduleCount, i => new List<int>());
            PerModuleSolveCountSamples = Misc.Create<List<int>>(moduleCount, i => new List<int>());

            IvmcProperCMinusSamples = Misc.Create<List<double>>(moduleCount, i => new List<double>());
            IvmcProperCPlusSamples = Misc.Create<List<double>>(moduleCount, i => new List<double>());

            CurrentSwitchCount = 0;
            CurrentPerModuleSwitchCount = new int[moduleCount];
            CurrentSolveCount = 0;
            CurrentPerModuleSolveCount = new int[moduleCount];
        }

        public int Resolution { get; }
        public int PlotPeriod { get; }
        public bool RecordPropers { get; }

        private int ModuleCount { get; }

        private List<int> SwitchCountSamples { get; }
        private List<int> SolveCountSamples { get; }
        private List<int>[] PerModuleSwitchCountSamples { get; }
        private List<int>[] PerModuleSolveCountSamples { get; }
        private List<double>[] IvmcProperCPlusSamples { get; }
        private List<double>[] IvmcProperCMinusSamples { get; }

        public void Attach(IDensePopulationExperimentFeedback densePopulationExperimentFeedback)
        {
            var feedback = densePopulationExperimentFeedback.Feedback;

            feedback.EndTarget.Register(EndTarget);
            feedback.Finished.Register(Finished);
            feedback.EndEpoch.Register(EndEpoch);
        }

        private int CurrentSwitchCount = 0;
        private int[] CurrentPerModuleSwitchCount;
        private int CurrentSolveCount = 0;
        private int[] CurrentPerModuleSolveCount;
        private Phenotype OldPhenotype = null;
        private void EndTarget(FileStuff stuff, Population<DenseIndividual> population, ITarget target, int epoch, long generationCount, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
        {
            Sample(target, epoch, oldPopulationJudgements);

            // epoch resolution, sub-epoch detail

            // record sample
            if (epoch % Resolution == 0)
            {
                // switches & solves
                SwitchCountSamples.Add(CurrentSwitchCount);
                CurrentSwitchCount = 0;

                SolveCountSamples.Add(CurrentSolveCount);
                CurrentSolveCount = 0;

                for (int i = 0; i < CurrentPerModuleSwitchCount.Length; i++)
                {
                    PerModuleSwitchCountSamples[i].Add(CurrentPerModuleSwitchCount[i]);
                    CurrentPerModuleSwitchCount[i] = 0;

                    PerModuleSolveCountSamples[i].Add(CurrentPerModuleSolveCount[i]);
                    CurrentPerModuleSolveCount[i] = 0;
                }

                // target (IvmcProper)

                if (RecordPropers)
                {
                    if (target is Epistatics.IIvmcProperTarget ivmcProperTarget)
                    {
                        var proper = ivmcProperTarget.Proper;

                        for (int i = 0; i < ModuleCount; i++)
                        {
                            IvmcProperCMinusSamples[i].Add(proper.Cminus[i]);
                            IvmcProperCPlusSamples[i].Add(proper.Cplus[i]);
                        }
                    }
                }
            }
        }

        private void Finished(FileStuff stuff, Population<DenseIndividual> population, int epochCount, long generationCount)
        {
            // write out
            SaveSwitchCounts(stuff, "_t");
            SaveSolveCounts(stuff, "_t");
            if (RecordPropers)
                SaveIvmcProper(stuff, "_t");
        }

        private void EndEpoch(FileStuff stuff, Population<DenseIndividual> population, int epochCount, long generationCount, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
        {
            if (epochCount % PlotPeriod == 0)
            {
                SaveSwitchCounts(stuff, "" + epochCount);
                SaveSolveCounts(stuff, "" + epochCount);
                if (RecordPropers)
                    SaveIvmcProper(stuff, "" + epochCount);
            }
        }

        private void SaveSwitchCounts(FileStuff stuff, string name)
        {
            double[][] switchCounts = PerModuleSwitchCountSamples.Select(l => l.Select(x => (double)x).ToArray()).Concat(new[] { SwitchCountSamples.Select(x => (double)x).ToArray() }).ToArray();
            Analysis.SaveTrajectories(stuff.File("ivmcswitches" + name + ".dat"), switchCounts, Resolution);
        }

        private void SaveSolveCounts(FileStuff stuff, string name)
        {
            double[][] solveCounts = PerModuleSolveCountSamples.Select(l => l.Select(x => (double)x).ToArray()).Concat(new[] { SolveCountSamples.Select(x => (double)x).ToArray() }).ToArray();
            Analysis.SaveTrajectories(stuff.File("ivmcsolves" + name + ".dat"), solveCounts, Resolution);
        }

        private void SaveIvmcProper(FileStuff stuff, string name)
        {
            var cminus = IvmcProperCMinusSamples.Select(l => l.ToArray());
            var cplus = IvmcProperCPlusSamples.Select(l => l.ToArray());
            double[][] both = cminus.Concat(cplus).ToArray();
            Analysis.SaveTrajectories(stuff.File("ivmcproper" + name + ".dat"), both, Resolution);
        }

        private void Sample(ITarget target, int epoch, IReadOnlyList<IndividualJudgement<DenseIndividual>> oldPopulationJudgements)
        {
            var newPhenotype = oldPopulationJudgements[0].Individual.Phenotype;
            bool allSolved = true;

            // TODO: need a general purpose version of this which works for other IIvmcProperTarget types
            if (OldPhenotype != null && target is IvmcProperChanging ivmcProperChanging)
            {
                var proper = ivmcProperChanging.Proper;
                bool isHard(int moduleIndex)
                {
                    // it's hard if both C+ and C- are >= CL
                    return proper.Cminus[moduleIndex] >= ivmcProperChanging.CL && proper.Cplus[moduleIndex] >= ivmcProperChanging.CL;
                }

                var modules = proper.ModulesAsFreeModules();
                var targetVector = ivmcProperChanging.Vector;

                bool switched(int moduleIndex)
                {
                    // it's switched if the module sum has changed sign
                    double oldSum = modules[moduleIndex].Sum(i => OldPhenotype[i] * targetVector[i]);
                    double newSum = modules[moduleIndex].Sum(i => newPhenotype[i] * targetVector[i]);
                    return Math.Sign(oldSum) != Math.Sign(newSum);
                }

                bool solved(int moduleIndex)
                {
                    var problemSign = Math.Sign(proper.Cplus[moduleIndex] - proper.Cminus[moduleIndex]);
                    var solSign = Math.Sign(modules[moduleIndex].Sum(i => newPhenotype[i] * targetVector[i]));

                    return problemSign == solSign;
                }

                // foreach module, check if it is 'hard', and then check if we switched
                for (int i = 0; i < proper.ModuleCount; i++)
                {
                    if (isHard(i) && switched(i))
                    {
                        CurrentPerModuleSwitchCount[i]++;
                        CurrentSwitchCount++;
                    }

                    if (solved(i))
                    {
                        CurrentPerModuleSolveCount[i]++;
                    }
                    else
                    {
                        allSolved = false;
                    }
                }
            }

            if (allSolved)
            {
                CurrentSolveCount++;
            }

            OldPhenotype = newPhenotype;
        }
    }

    public class IvmcTargetPackages
    {
        // Example1_Tiny
        public static EpistaticTargetPackage IvmcExample1_1(double v, double z, double changeInversionProbability) => new EpistaticTargetPackage($"IvmcExample1_4_V{v}_2", new[]
            {
                new Epistatics.ExpIvmc(1, 4, v, z, "++++", "t", "t", $"Example1TinyV{v}Z{z}ciP{changeInversionProbability}", changeInversionProbability)
            });

        // Example1
        public static EpistaticTargetPackage IvmcExample1_4(double v, double z, double changeInversionProbability) => new EpistaticTargetPackage($"IvmcExample1_4_V{v}_2", new[]
            {
                new Epistatics.ExpIvmc(4, 4, v, z, "++++++++++++++++", "tttt", "tttt", $"Example1V{v}Z{z}ciP{changeInversionProbability}", changeInversionProbability)
            });

        // Example1_Big
        public static EpistaticTargetPackage IvmcExample1_8(double v, double z, double changeInversionProbability) => new EpistaticTargetPackage($"IvmcExample1_8_V{v}_2", new[]
            {
                new Epistatics.ExpIvmc(8, 4, v, z, "++++++++++++++++++++++++++++++++", "tttttttt", "tttttttt", $"Example1BigV{v}Z{z}ciP{changeInversionProbability}", changeInversionProbability)
            });

        // Proper1
        public static EpistaticTargetPackage IvmcProper1_4x4(IvmcProperJudgementMode properJudgementMode, double ch, double cl, double c0, double z) => new EpistaticTargetPackage($"IvmcProper1_4x4CH{ch}CL{cl}Z{z}", new[]
            {
                IvmcProper1_4x4target(properJudgementMode, ch, cl, c0, z)
            });

        public static Epistatics.IvmcProperChanging IvmcProper1_4x4target(IvmcProperJudgementMode properJudgementMode, double ch, double cl, double c0, double z)
        {
            return new Epistatics.IvmcProperChanging(new TraditionalIvmcProperJudgementPreparer(properJudgementMode), 4, 4, ch, cl, c0, z, "++++++++++++++++", $"IvmcProper{properJudgementMode.ToString()}4x4CH{ch}CL{cl}C0{c0}Z{z}");
        }
        
        // Proper1Custom
        public static EpistaticTargetPackage IvmcProper1_Custom(string modulesString, string variationModulesString, string variationModulePlusOverridesString, string variationModuleMinusOverridesString, string targetString, IIvmcProperJudgementPreparer properJudgementPreparer, double ch, double cl, double c0, double z, string name) => new EpistaticTargetPackage($"IvmcProper1_CustomCH{ch}CL{cl}CZ{c0}Z{z}", new[]
            {
                IvmcProper1_CustomTarget(modulesString, variationModulesString, variationModulePlusOverridesString, variationModuleMinusOverridesString, targetString, properJudgementPreparer, ch, cl, c0, z, name)
            });
        
        //public static Epistatics.IvmcProperChanging IvmcProper1_CustomTarget(string modulesString, string variationModulesString, string variationModulePlusOverridesString, string variationModuleMinusOverridesString, string targetString, IvmcProperJudgementMode properJudgementMode, double ch, double cl, double c0, double z)
        public static Epistatics.IvmcProperChanging IvmcProper1_CustomTarget(string modulesString, string variationModulesString, string variationModulePlusOverridesString, string variationModuleMinusOverridesString, string targetString, IIvmcProperJudgementPreparer properJudgementPreparer, double ch, double cl, double c0, double z, string name)
        {
            return new Epistatics.IvmcProperChanging(
                properJudgementPreparer,
                Modular.MultiModulesStringParser.Instance.Parse(modulesString),
                variationModulesString != null ? Modular.MultiModulesStringParser.Instance.Parse(variationModulesString) : null,
                variationModulePlusOverridesString != null ? ParseOverrides(variationModulePlusOverridesString, ch, cl, c0) : null,
                variationModuleMinusOverridesString != null ? ParseOverrides(variationModuleMinusOverridesString, ch, cl, c0) : null,
                ch, cl, c0, z,
                targetString,
                name ?? $"IvmcProper{properJudgementPreparer.Name}CH{ch}CL{cl}C0{c0}Z{z}_{modulesString}_{variationModulesString}_{targetString}");
        }

        public static void GenerateRandomIvmcHL0Overrides(RandomSource rand, int moduleCount, double Z, int targetCount, out string ivmcvmmos, out string ivmcvmpos)
        {
            var m = new StringBuilder();
            var p = new StringBuilder();

            for (int i = 0; i < targetCount; i++)
            {
                if (i > 0)
                {
                    m.Append('|');
                    p.Append('|');
                }

                GenerateRandomIvmcHL0Overrides(rand, moduleCount, Z, out ivmcvmmos, out ivmcvmpos);

                m.Append(ivmcvmmos);
                p.Append(ivmcvmpos);
            }

            ivmcvmmos = m.ToString();
            ivmcvmpos = p.ToString();
        }

        public static void GenerateRandomIvmcHL0Overrides(RandomSource rand, int moduleCount, double Z, out string ivmcvmmos, out string ivmcvmpos)
        {
            char[] m = new char[moduleCount];
            char[] p = new char[moduleCount];

            for (int mi = 0; mi < moduleCount; mi++)
            {
                bool phigh = rand.NextBoolean(); // ¬mhigh
                bool mtype = rand.NextDouble() < Z; // ¬stype

                var high = phigh ? p : m;
                var low = phigh ? m : p;

                high[mi] = 'H';
                low[mi] = mtype ? 'L' : '0';
            }

            ivmcvmmos = new string(m);
            ivmcvmpos = new string(p);
        }

        public static double[] ParseOverrides(string str, double ch, double cl, double c0)
        {
            if (string.IsNullOrEmpty(str))
                return null; // bleh

            if (str.Contains(";"))
            {
                return str.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s == "_" ? double.NaN : double.Parse(s)).ToArray();
            }

            double[] res = new double[str.Length];
            for (int i = 0; i < res.Length; i++)
            {
                char c = str[i];
                
                switch (c)
                {
                    case 'H':
                    case 'h':
                        res[i] = ch;
                        break;
                    case 'L':
                    case 'l':
                        res[i] = cl;
                        break;
                    case '0':
                        res[i] = c0;
                        break;
                    case '_':
                        res[i] = double.NaN;
                        break;
                    default:
                        throw new ArgumentException("Invalid IvmcOverride String: \"" + str + "\"", nameof(str));
                }
            }

            return res;
        }
    }
}
