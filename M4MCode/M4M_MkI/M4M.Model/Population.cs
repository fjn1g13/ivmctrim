﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RandomSource = MathNet.Numerics.Random.RandomSource;
using static M4M.Misc;
using MathNet.Numerics.Random;
using M4M.State;
using System.IO;
using M4M.Epistatics;
using M4M.Modular;
using MathNet.Numerics.Providers.LinearAlgebra;

namespace M4M
{
    /// <summary>
    /// Represents an individual with a phenotype that can be judged by a target.
    /// </summary>
    public interface IIndividual
    {
        Phenotype Phenotype { get; }
        MultiMeasureJudgement Judge(JudgementRules jrules, ITarget target);
        void WriteOut(System.IO.StreamWriter cw);
    }

    /// <summary>
    /// Represents an individual within a population that can be mutated and combined to produce new individuals.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IIndividual<T> : IIndividual where T : IIndividual<T>
    {
        /// <summary>
        /// Creates a new individual with mutations
        /// </summary>
        /// <param name="context">The Model Execution Context</param>
        /// <param name="drules">Development Rules</param>
        /// <param name="rrules">Reproduction Rules</param>
        T Mutate(ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules);

        /// <summary>
        /// Mutates this individual into the given target individual
        /// </summary>
        /// <param name="target">The target Individual</param>
        /// <param name="context">The Model Execution Context</param>
        /// <param name="drules">Development Rules</param>
        /// <param name="rrules">Reproduction Rules</param>
        void MutateInto(T target, ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules);

        /// <summary>
        /// Combines this Individual with another
        /// </summary>
        /// <param name="other">The other Genome involves in the combination</param>
        /// <param name="target">The target Genome</param>
        /// <param name="context">Model Execution Context</param>
        /// <param name="rules">Reproduction Rules</param>
        void CombineAndMutateInto(T other, T target, ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules);

        /// <summary>
        /// Creates an exact copy of the individual, cloning any internal state
        /// (Must not modify the state of the Random number generator)
        /// </summary>
        T Clone(ModelExecutionContext context);

        /// <summary>
        /// Inidicates that the individual should be recycled if possible.
        /// </summary>
        /// <param name="context">Model Execution Context</param>
        void Recycle(ModelExecutionContext context);

        /// <summary>
        /// Indicates that the individual has escaped the population, and can no longer be recycled under any circumstances
        /// </summary>
        void Escaped();
    }

    [StateClass]
    public class SimpleIndividual<G> : IIndividual<SimpleIndividual<G>> where G : IGenome<G>
    {
        [Obsolete]
        protected SimpleIndividual()
        { }

        [SimpleStateProperty("Genome")]
        public G Genome { get; private set; }

        [SimpleStateProperty("Phenotype")]
        public Phenotype Phenotype { get; private set; }

        protected SimpleIndividual(G genome, Phenotype phenotype)
        {
            Genome = genome;
            Phenotype = phenotype;
        }

        public SimpleIndividual<G> Clone(ModelExecutionContext context)
        {
            return new SimpleIndividual<G>(Genome.Clone(context), new Phenotype(Phenotype.Vector));
        }

        public static SimpleIndividual<G> Develop(G genome, ModelExecutionContext context, DevelopmentRules drules)
        {
            return new SimpleIndividual<G>(genome, genome.Develop(context, drules));
        }

        public SimpleIndividual<G> Mutate(ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules)
        {
            return Develop(Genome.Mutate(context, rrules), context, drules);
        }

        public void DevelopInplace(ModelExecutionContext context, DevelopmentRules drules)
        {
            Genome.DevelopInto(Phenotype, context, drules);
        }

        public void MutateInto(SimpleIndividual<G> target, ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules)
        {
            Genome.MutateInto(target.Genome, context, rrules);
            target.DevelopInplace(context, drules);
        }

        public void MutateInplace(ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules)
        {
            Genome.MutateInplace(context, rrules);
            DevelopInplace(context, drules);
        }

        public void CombineAndMutateInto(SimpleIndividual<G> other, SimpleIndividual<G> target, ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules)
        {
            Genome.CombineInto(other.Genome, target.Genome, context, rrules);
            target.Genome.MutateInplace(context, rrules);
            target.DevelopInplace(context, drules);
        }

        public MultiMeasureJudgement Judge(JudgementRules jrules, ITarget target)
        {
            return MultiMeasureJudgement.Judge(Genome, Phenotype, jrules, target);
        }

        public void Recycle(ModelExecutionContext context)
        {
            // nix
        }

        public void Escaped()
        {
            // nix
        }

        public void WriteOut(System.IO.StreamWriter cw)
        {
            cw.WriteLine($"Individual is SimpleIndividual of generic IGenome");

            cw.WriteLine($"Size = {Genome.Size}");
        }
    }

    [StateClass]
    public class DenseIndividual : IIndividual<DenseIndividual>
    {
        [SimpleStateProperty("Genome")]
        public DenseGenome Genome { get; private set; }

        [SimpleStateProperty("Phenotype")]
        public Phenotype Phenotype { get; private set; }

        [SimpleStateProperty("Epigenetic")]
        public bool Epigenetic { get; private set; }

        /// <summary>
        /// Indicates that this DenseIndividual knows that the DenseGenome can be recycled at this time
        /// This should also be false on load, because we don't know who is loading it (don't persist)
        /// Never set this to true, except in the constructor
        /// </summary>
        private bool CanRecycle = false;

        [Obsolete]
        protected DenseIndividual()
        { }

        private DenseIndividual(DenseGenome genome, Phenotype phenotype, bool epigenetic, bool canRecycle = false)
        {
            Genome = genome;
            Phenotype = phenotype;
            Epigenetic = epigenetic;

            CanRecycle = canRecycle;
        }

        public DenseIndividual Clone(ModelExecutionContext context)
        {
            return new DenseIndividual(Genome.Clone(context), new Phenotype(Phenotype.Vector.Clone()), Epigenetic, true);
        }

        public static DenseIndividual Develop(DenseGenome genome, ModelExecutionContext context, DevelopmentRules drules, bool epigenetic)
        {
            // we have no idea where the Genome came from, so we can't recycle
            return DevelopInternal(genome, context, drules, epigenetic, false);
        }

        private static DenseIndividual DevelopInternal(DenseGenome genome, ModelExecutionContext context, DevelopmentRules drules, bool epigenetic, bool canRecycle)
        {
            return new DenseIndividual(genome, genome.Develop(context, drules), epigenetic, canRecycle);
        }

        public void DevelopInplace(ModelExecutionContext context, DevelopmentRules drules)
        {
            Genome.DevelopInto(Phenotype, context, drules);
        }

        public DenseIndividual Mutate(ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules)
        {
            if (Epigenetic)
            {
                var temp = Genome.Clone(context); // clone me
                temp.CopyOverInitialState(Phenotype.Vector); // copy phenotype into state

                // because our Genome is cloned
                //  - we know it doesn't violate the ability for us to be recycled (don't clear CanRecycle)
                //  - for the moment the new guy can also be recycled (so tell DevelopInternal)
                return DevelopInternal(temp.Mutate(context, rrules), context, drules, Epigenetic, true);
            }
            else
            {
                // simple
                var newGenome = Genome.Mutate(context, rrules);
                bool canRecycle = newGenome.TransMat != Genome.TransMat
                    && newGenome.InitialState != Genome.InitialState; // detect whether an interdependency exists

                CanRecycle &= canRecycle; // clear CanRecycle if we can no longer be recycled

                return DevelopInternal(newGenome, context, drules, Epigenetic, canRecycle);
            }
        }

        public void MutateInto(DenseIndividual target, ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules)
        {
            if (!target.CanRecycle)
                throw new Exception("Cannot mutate into a potencially escaped target individual");

            if (Epigenetic)
            {
                // create a new genome, with our phenotype as it's initial state
                var temp = Genome.Clone(context); // clone me
                temp.CopyOverInitialState(Phenotype.Vector); // copy phenotype into state

                // mutate and develop
                temp.MutateInto(target.Genome, context, rrules);
                target.DevelopInplace(context, drules);
            }
            else
            {
                // mutate and develop
                Genome.MutateInto(target.Genome, context, rrules);
                target.DevelopInplace(context, drules);
            }
        }

        public void MutateInplace(ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules)
        {
            if (!CanRecycle)
                throw new Exception("Cannot mutate a potencially escaped target individual");

            if (Epigenetic)
                Genome.CopyOverInitialState(Phenotype.Vector);

            Genome.MutateInplace(context, rrules);
            DevelopInplace(context, drules);
        }

        public void CombineAndMutateInto(DenseIndividual other, DenseIndividual target, ModelExecutionContext context, DevelopmentRules drules, ReproductionRules rrules)
        {
            if (!target.CanRecycle)
                throw new Exception("Cannot combine into a potencially escaped target individual");

            if (Epigenetic)
            {
                var temp1 = Genome.Clone(context); // clone me
                temp1.CopyOverInitialState(Phenotype.Vector);

                var temp2 = other.Genome.Clone(context); // clone other
                temp2.CopyOverInitialState(other.Phenotype.Vector);

                Genome.CombineInto(temp1, temp2, context, rrules);
                target.Genome.MutateInplace(context, rrules);
                target.DevelopInplace(context, drules);
            }
            else
            {
                Genome.CombineInto(other.Genome, target.Genome, context, rrules);
                target.Genome.MutateInplace(context, rrules);
                target.DevelopInplace(context, drules);
            }
        }

        public MultiMeasureJudgement Judge(JudgementRules jrules, ITarget target)
        {
            return MultiMeasureJudgement.Judge(Genome, Phenotype, jrules, target);
        }

        public void Recycle(ModelExecutionContext context)
        {
            if (CanRecycle)
            {
                Genome.Recycle(context);
            }
        }

        public void Escaped()
        {
            CanRecycle = false;
        }

        public void WriteOut(System.IO.StreamWriter cw)
        {
            string d = "(Default)";

            cw.WriteLine($"Individual is DenseIndividual:");
            cw.WriteLine($"{nameof(Epigenetic)} = {Epigenetic}");

            cw.WriteLine();
            cw.WriteLine($"Genome Info:");
            cw.WriteLine($"{nameof(Genome.Size)} = {Genome.Size}");
            cw.WriteLine($"{nameof(Genome.InitialStateIndexOpenEntries)} = {(Genome.InitialStateIndexOpenEntries == null ? d : String.Join(" ", Genome.InitialStateIndexOpenEntries))}");
            cw.WriteLine($"{nameof(Genome.TransMatIndexOpenEntries)} = {(Genome.TransMatIndexOpenEntries == null ? d : String.Join(" ", Genome.TransMatIndexOpenEntries))}");
            cw.WriteLine($"{nameof(Genome.CustomInitialStateMutator)} = {Genome.CustomInitialStateMutator?.Name ?? d}");
            cw.WriteLine($"{nameof(Genome.CustomTransMatMutator)} = {Genome.CustomTransMatMutator?.Name ?? d}");
            cw.WriteLine($"{nameof(Genome.CustomInitialStateCombiner)} = {Genome.CustomInitialStateCombiner?.Name ?? d}");
            cw.WriteLine($"{nameof(Genome.CustomTransMatCombiner)} = {Genome.CustomTransMatCombiner?.Name ?? d}");
            cw.WriteLine($"{nameof(Genome.CustomDevelopmentStep)} = {Genome.CustomDevelopmentStep?.Name ?? d}");
            cw.WriteLine($"{nameof(Genome.BiasVector)} non null = {Genome.BiasVector != null}");
        }
    }

    public delegate T IndividualGenerator<T>(ModelExecutionContext context) where T : IIndividual<T>;

    /// <summary>
    /// Represents an individual and its judgement.
    /// </summary>
    [StateClass]
    public class IndividualJudgement<T> where T : IIndividual<T>
    {
        [Obsolete]
        protected IndividualJudgement()
        { }

        public IndividualJudgement(T individual, MultiMeasureJudgement judgement)
        {
            Individual = individual;
            Judgement = judgement;
        }

        [SimpleStateProperty("Individual")]
        public T Individual { get; private set; }

        [SimpleStateProperty("Judgement")]
        public MultiMeasureJudgement Judgement { get; private set; }
    }

    /// <summary>
    /// Providers a method to prepare a selector for the given type of individual.
    /// </summary>
    /// <typeparam name="T">The type of individual.</typeparam>
    public interface ISelectorPreparer<T> where T : IIndividual<T>
    {
        string Name { get; }

        /// <summary>
        /// Prepares a selector from a <see cref="RandomSource"/> and collection of <see cref="IndividualJudgement{T}"/>, which can be used to sample individuals.
        /// </summary>
        ISelector<T> Prepare(IReadOnlyList<IndividualJudgement<T>> individualJudgements, RandomSource rand);
    }

    public interface IPopulationSpinner<T> where T : IIndividual<T>
    {
        string Name { get; }
        string Description { get; }

        /// <summary>
        /// Spins the population for the given number of generations.
        /// This method will call <see cref="ITarget.NextGeneration(RandomSource, JudgementRules)"/> as necessary, but will not call <see cref="ITarget.NextExposure(RandomSource, JudgementRules, int, ref ExposureInformation)"/>.
        /// Parameters generally come directly from a <see cref="PopulationExperiment{T}"/> and <see cref="ExperimentConfiguration"/>.
        /// </summary>
        IReadOnlyList<IndividualJudgement<T>> SpinPopulation(Population<T> population, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<T> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementFeedback, int eliteCount, bool hillclimberMode);
    }

    /// <summary>
    /// Just wraps a call to <see cref="Population{T}.SpinPopulation(ModelExecutionContext, ReproductionRules, DevelopmentRules, JudgementRules, ITarget, ISelectorPreparer{T}, int, Action{int, IReadOnlyList{IndividualJudgement{T}}}, int, bool)"/>.
    /// </summary>
    [StateClass]
    public class DefaultPopulationSpinner<T> : IPopulationSpinner<T> where T : IIndividual<T>
    {
        public static readonly DefaultPopulationSpinner<T> Instance = new DefaultPopulationSpinner<T>();

        public string Name => "DefaultPopulationSpinner";
        public string Description => "DefaultPopulationSpinner";

        public IReadOnlyList<IndividualJudgement<T>> SpinPopulation(Population<T> population, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<T> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementFeedback, int eliteCount, bool hillclimberMode)
        {
            return population.SpinPopulation(context, rrules, drules, jrules, target, selectorPreparer, generations, judgementFeedback, eliteCount, hillclimberMode);
        }
    }

    [StateClass]
    public class PairedPopulationSpinner<T> : IPopulationSpinner<T> where T : IIndividual<T>
    {
        public static readonly PairedPopulationSpinner<T> Instance = new PairedPopulationSpinner<T>();

        public string Name => "PairedPopulationSpinner";
        public string Description => "PairedPopulationSpinner";

        public IReadOnlyList<IndividualJudgement<T>> SpinPopulation(Population<T> population, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<T> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementFeedback, int eliteCount, bool hillclimberMode)
        {
            return population.SpinPopulationPaired(context, rrules, drules, jrules, target, generations, judgementFeedback);
        }
    }

    [StateClass]
    public class RecombinantTriosPopulationSpinner<T> : IPopulationSpinner<T> where T : IIndividual<T>
    {
        public static readonly RecombinantTriosPopulationSpinner<T> Instance = new RecombinantTriosPopulationSpinner<T>();

        public string Name => "RecombinantTriosPopulationSpinner";
        public string Description => "RecombinantTriosPopulationSpinner";

        public IReadOnlyList<IndividualJudgement<T>> SpinPopulation(Population<T> population, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<T> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementFeedback, int eliteCount, bool hillclimberMode)
        {
            return population.SpinPopulationRecombinantTrios(context, rrules, drules, jrules, target, generations, judgementFeedback);
        }
    }

    [StateClass]
    public class Population<T> where T : IIndividual<T>
    {
        /// <summary>
        /// Controls the number of threads that may be used when processing a population. It hasn't been tested for months, so don't touch it.
        /// Note: <see cref="ModelExecutionContext"/> is NOT thread safe, so all threads will be given their own context.
        /// </summary>
        public int PopulationParallelism { get; set; } = 1;

        [SimpleStateProperty("Individuals")]
        private List<T> Individuals { get; set; } = new List<T>();

        public int Count => Individuals.Count;

        /// <summary>
        /// Initializes a population of random individuals
        /// </summary>
        public Population(ModelExecutionContext context, IndividualGenerator<T> randomIndividualGenerator, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Individuals.Add(randomIndividualGenerator(context));
            }
        }

        /// <summary>
        /// Initializes a population of identifcal individuals (reuses same reference, no attempt to clone)
        /// </summary>
        public Population(T template, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Individuals.Add(template);
            }
        }

        /// <summary>
        /// Initializes an empty population
        /// </summary>
        public Population()
        {
        }

        /// <summary>
        /// Initializes a population with the given individuals
        /// </summary>
        public Population(IEnumerable<T> individuals)
        {
            Individuals.AddRange(individuals);
        }

        /// <summary>
        /// Creates a new population, containing the same individuals
        /// </summary>
        /// <returns></returns>
        public Population<T> Clone()
        {
            foreach (var i in Individuals)
                i.Escaped();

            return new Population<T>(Individuals);
        }

        /// <summary>
        /// Creates a new population, containing clones of the current individuals
        /// </summary>
        public Population<T> Clone(ModelExecutionContext context)
        {
            bool doRenable = context.TryDisableRandom(out var rand);

            var newPop = new Population<T>(Individuals.Select(i => i.Clone(context)));
            
            if (doRenable)
                context.EnableRandom(rand);
            
            return newPop;
        }

        public void Clear()
        {
            Individuals.Clear();
        }

        private void RecycleAndClear(ModelExecutionContext context)
        {
            foreach (var i in Individuals)
                i.Recycle(context);

            Clear();
        }

        public T[] ExtractAll()
        {
            var res = Individuals.ToArray();

            foreach (var i in res)
                i.Escaped();

            Clear();
            return res;
        }

        public T[] PeekAll()
        {
            var res = Individuals.ToArray();
            foreach (var i in res)
                i.Escaped();

            return res;
        }

        public IndividualJudgement<T>[] PeekAndJudgeAll(JudgementRules jrules, ITarget target)
        {
            var peeked = PeekAll();

            return JudgeAll(peeked, jrules, target, false);
        }

        public IndividualJudgement<T>[] ExtractAndJudgeAll(JudgementRules jrules, ITarget target)
        {
            var extracted = ExtractAll();

            return JudgeAll(extracted, jrules, target, false);
        }

        private IndividualJudgement<T>[] ExtractAndJudgeAllNoEscape(JudgementRules jrules, ITarget target)
        {
            var extracted = ExtractAll(); // does actually escape... but this whole design is a mess, and it's not worth fixing it now

            return JudgeAll(extracted, jrules, target, true);
        }

        private static IndividualJudgement<T>[] JudgeAll(T[] individuals, JudgementRules jrules, ITarget target, bool noEscape)
        {
            IndividualJudgement<T>[] res = new IndividualJudgement<T>[individuals.Length];

            for (int i = 0; i < individuals.Length; i++)
            {
                T t = individuals[i];
                if (!noEscape)
                    t.Escaped();

                res[i] = new IndividualJudgement<T>(t, t.Judge(jrules, target));
            }

            return res;

            // NOTE: reckon the Linq was adding ~20% overhead
            //return individuals.Select(t => new IndividualJudgement<T>(t, t.Judge(jrules, target))).ToArray();
        }

        /// <summary>
        /// Experimental
        /// Spins the population (repeatedly cycles it) with paired displacement replacement competition selection, whatever it should be calle
        /// </summary>
        public IReadOnlyList<IndividualJudgement<T>> SpinPopulationPaired(ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, int generations, Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementFeedback)
        {
            if (Count < 2)
                throw new Exception("Misuse of SpinPopulationPaired; population must have atleast 2 individuals");

            // this doesn't actually recycle anything...
            T[] arr = ExtractAll().Select(i => { var r = i.Clone(context); i.Recycle(context); return r;}).ToArray(); // start by cloning them (assumes many generations)

            var rand = context.Rand;

            IReadOnlyList<IndividualJudgement<T>> judgements = null;

            for (int generation = 0; generation < generations; generation++)
            {
                target.NextGeneration(rand, jrules);

                if (generation == 0 && judgementFeedback != null)
                {
                    // cloning here shouldn't add much overhead over creating new things directly, and is more efficient if there is no IndividualJudgement (usual case)
                    judgements = JudgeAll(arr.Select(i => i.Clone(context)).ToArray(), jrules, target, false);
                    judgementFeedback.Invoke(0, judgements);
                }

                bool lastGeneration = generation == generations - 1;

                int a = rand.Next(arr.Length);
                int b = rand.Next(arr.Length - 1);
                if (b >= a)
                    b++;

                var aj = arr[a].Judge(jrules, target);
                var bj = arr[b].Judge(jrules, target);

                if (bj.CombinedFitness > aj.CombinedFitness)
                { // arbitrary tie-breaking
                    var t = a;
                    a = b;
                    b = t;

                    var tj = aj;
                    aj = bj;
                    bj = tj;
                }

                // replace b with a mutant of a
                arr[a].MutateInto(arr[b], context, drules, rrules);

                if (judgementFeedback != null || lastGeneration)
                {
                    // cloning here shouldn't add much overhead over creating new things directly, and is more efficient if there is no IndividualJudgement (usual case)
                    judgements = JudgeAll(arr.Select(i => i.Clone(context)).ToArray(), jrules, target, false);
                    judgementFeedback?.Invoke(generation + 1, judgements);
                }
            }

            Individuals.AddRange(arr);
            return judgements;
        }

        /// <summary>
        /// Experimental
        /// Spins the population (repeatedly cycles it) with trio competition and recombination
        /// Foreach Generation
        ///   Select 3 individuals at random
        ///   Kill the weakest
        ///   Cross the other 2 (recombine) with mutation to produce a replacement
        /// </summary>
        public IReadOnlyList<IndividualJudgement<T>> SpinPopulationRecombinantTrios(ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, int generations, Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementFeedback)
        {
            if (Count < 3)
                throw new Exception("Misuse of SpinPopulationRecombinantTrios; population must have atleast 3 individuals");

            // this doesn't actually recycle anything
            T[] arr = ExtractAll().Select(i => { var r = i.Clone(context); i.Recycle(context); return r; }).ToArray(); // start by cloning them (assumes many generations)

            var rand = context.Rand;

            IReadOnlyList<IndividualJudgement<T>> judgements = null;

            for (int generation = 0; generation < generations; generation++)
            {
                target.NextGeneration(rand, jrules);

                if (generation == 0 && judgementFeedback != null)
                {
                    // cloning here shouldn't add much overhead over creating new things directly, and is more efficient if there is no IndividualJudgement (usual case)
                    judgements = JudgeAll(arr.Select(i => i.Clone(context)).ToArray(), jrules, target, false);
                    judgementFeedback.Invoke(0, judgements);
                }

                bool lastGeneration = generation == generations - 1;

                int a = rand.Next(arr.Length);
                int b = rand.Next(arr.Length - 1);
                if (b >= a)
                    b++;
                int c = rand.Next(arr.Length - 2);
                if (c >= a)
                    c++;
                if (c >= b)
                    c++;

                var aj = arr[a].Judge(jrules, target);
                var bj = arr[b].Judge(jrules, target);
                var cj = arr[c].Judge(jrules, target);

                if (bj.CombinedFitness > aj.CombinedFitness)
                { // arbitrary tie-breaking
                    var t = a;
                    a = b;
                    b = t;

                    var tj = aj;
                    aj = bj;
                    bj = tj;
                }

                if (cj.CombinedFitness > bj.CombinedFitness)
                { // arbitrary tie-breaking
                    var t = b;
                    b = c;
                    c = t;

                    var tj = bj;
                    bj = cj;
                    cj = tj;
                }
                
                // replace c with a recombined mutant of a and b
                arr[a].CombineAndMutateInto(arr[b], arr[c], context, drules, rrules);

                if (judgementFeedback != null || lastGeneration)
                {
                    // cloning here shouldn't add much overhead over creating new things directly, and is more efficient if there is no IndividualJudgement (usual case)
                    judgements = JudgeAll(arr.Select(i => i.Clone(context)).ToArray(), jrules, target, false);
                    judgementFeedback?.Invoke(generation + 1, judgements);
                }
            }

            Individuals.AddRange(arr);
            return judgements;
        }

        /// <summary>
        /// Spins the population (repeatedly cycles it)
        /// Using this method enables extra recycling
        /// </summary>
        public IReadOnlyList<IndividualJudgement<T>> SpinPopulation(ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, JudgementRules jrules, ITarget target, ISelectorPreparer<T> selectorPreparer, int generations, Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementFeedback, int eliteCount, bool hillclimberMode)
        {
            bool noEscape = judgementFeedback == null;

            // hill-climber spin fast path (good if generations are high and bmutate rate is also high)
            bool hcSpinFastPath1 = noEscape && hillclimberMode && Count == 1 && eliteCount == 1;
            if (hcSpinFastPath1)
            {
                return new[] { SpinHillclimb1FastPathNoEscape(generations, context, jrules, target, rrules, drules) };
            }

            // (no fast path)
            var rand = context.Rand;
            IReadOnlyList<IndividualJudgement<T>> judgements = null;

            for (int generation = 0; generation < generations; generation++)
            {
                target.NextGeneration(rand, jrules);

                judgements = CyclePopulationInternal(context, jrules, target, rrules, drules, selectorPreparer, eliteCount, hillclimberMode, noEscape, generation == 0 ? judgementFeedback : null);
                judgementFeedback?.Invoke(generation + 1, judgements);

                if (noEscape)
                {
                    bool isLast = generation == generations - 1;

                    if (isLast)
                    {
                        // CyclePopulationInternal didn't escape them (because they havn't escaped yet), so we need to escape them now
                        foreach (var ij in judgements)
                            ij.Individual.Escaped();
                    }
                    else
                    {
                        // we can recycle them!
                        foreach (var ij in judgements)
                            ij.Individual.Recycle(context);
                    }
                }
            }

            return judgements;
        }

        /// <summary>
        /// Judges everyone, and then produces mutatant offspring by weighted selection
        /// Returns the previous members if the population, and their judgements
        /// </summary>
        public IReadOnlyList<IndividualJudgement<T>> CyclePopulation(ModelExecutionContext context, JudgementRules jrules, ITarget target, ReproductionRules rrules, DevelopmentRules drules, ISelectorPreparer<T> selectorPreparer, int eliteCount, bool hillclimberMode, Action<int, IReadOnlyList<IndividualJudgement<T>>> genZeroJudgementFeedback)
        {
            return CyclePopulationInternal(context, jrules, target, rrules, drules, selectorPreparer, eliteCount, hillclimberMode, false, genZeroJudgementFeedback);
        }

        /// <summary>
        /// Judges everyone, and then produces mutatant offspring by weighted selection
        /// Returns the previous members of the population, and their judgements
        /// </summary>
        private IReadOnlyList<IndividualJudgement<T>> CyclePopulationInternal(ModelExecutionContext context, JudgementRules jrules, ITarget target, ReproductionRules rrules, DevelopmentRules drules, ISelectorPreparer<T> selectorPreparer, int eliteCount, bool hillclimberMode, bool noEscape, Action<int, IReadOnlyList<IndividualJudgement<T>>> genZeroJudgementFeedback)
        {
            int count = Count;
            var judgements = noEscape && (genZeroJudgementFeedback == null)
                ? ExtractAndJudgeAllNoEscape(jrules, target)
                : ExtractAndJudgeAll(jrules, target);

            // this should only be non-null in the first generation
            genZeroJudgementFeedback?.Invoke(0, judgements);

            if (hillclimberMode)
            {
                if (count == 1 && eliteCount == 1)
                {
                    // fast path
                    Hillclimb1FastPath(context, jrules, target, rrules, drules, judgements[0]);
                }
                else
                {
                    var sel = selectorPreparer.Prepare(judgements, context.Rand);
                    var elites = judgements.OrderByDescending(j => j.Judgement.CombinedFitness).Take(eliteCount);
                    HillclimbFrom(sel, count, context, jrules, target, rrules, drules, elites);
                }
            }
            else
            {
                var sel = selectorPreparer.Prepare(judgements, context.Rand);
                var elites = judgements.OrderByDescending(j => j.Judgement.CombinedFitness).Take(eliteCount);
                PopulateFrom(sel, count - eliteCount, context, rrules, drules, elites.Select(j => j.Individual));
            }

            return judgements;
        }

        private void PopulateFrom(ISelector<T> selector, int count, ModelExecutionContext context, ReproductionRules rrules, DevelopmentRules drules, IEnumerable<T> elites)
        {
            for (int i = 0; i < count; i++)
            {
                Individuals.Add(selector.SelectRandom().Mutate(context, drules, rrules));
            }

            foreach (var elite in elites)
                Individuals.Add(elite);
        }

        /// <summary>
        /// Runs an efficient Hill-Climbing (assuming large number of generations)
        /// </summary>
        private IndividualJudgement<T> SpinHillclimb1FastPathNoEscape(int generations, ModelExecutionContext context, JudgementRules jrules, ITarget target, ReproductionRules rrules, DevelopmentRules drules)
        {
            if (Count != 1)
                throw new Exception("Misuse of SpinHillclimb1FastPathNoEscape; population should have exactly one individual");

            var rand = context.DisableRandom(); // don't try: if this fails, we require random later on
            var currentIndividual = ExtractAll()[0].Clone(context); // start with a clone: that means we know it hasn't escaped (contract says it won't change the context state)
            var candidateIndividual = currentIndividual.Clone(context);
            context.EnableRandom(rand);

            IndividualJudgement<T> result = null;

            for (int generation = 0; generation < generations; generation++)
            {
                bool lastGeneration = generation == generations - 1;

                target.NextGeneration(rand, jrules);

                var currentJudgement = currentIndividual.Judge(jrules, target);

                if (lastGeneration)
                {
                    currentIndividual.Escaped();
                    result = new IndividualJudgement<T>(currentIndividual, currentJudgement);
                }

                currentIndividual.MutateInto(candidateIndividual, context, drules, rrules);
                var candidateJudgement = candidateIndividual.Judge(jrules, target);

                if (candidateJudgement.CombinedFitness > currentJudgement.CombinedFitness)
                {
                    // keep new (offspring)

                    var t = currentIndividual;
                    currentIndividual = candidateIndividual;
                    candidateIndividual = t;

                    currentJudgement = candidateJudgement;
                }
            }

            // don't need this guy any more
            candidateIndividual.Recycle(context);

            // keep current
            Individuals.Add(currentIndividual);

            // return current judgement
            return result;
        }

        public void Introduce(T individual)
        {
            Individuals.Add(individual);
        }

        public void IntroduceMany(IEnumerable<T> individuals)
        {
            Individuals.AddRange(individuals);
        }

        private void Hillclimb1FastPath(ModelExecutionContext context, JudgementRules jrules, ITarget target, ReproductionRules rrules, DevelopmentRules drules, IndividualJudgement<T> current)
        {
            T next = current.Individual.Mutate(context, drules, rrules);
            var j = next.Judge(jrules, target);

            if (j.CombinedFitness > current.Judgement.CombinedFitness)
            {
                // keep new (offspring)
                Individuals.Add(next);
            }
            else
            {
                // keep old (elite)
                Individuals.Add(current.Individual);

                // recycle new
                next.Recycle(context);
            }
        }

        private void HillclimbFrom(ISelector<T> selector, int count, ModelExecutionContext context, JudgementRules jrules, ITarget target, ReproductionRules rrules, DevelopmentRules drules, IEnumerable<IndividualJudgement<T>> elites)
        {
            // put the elites in a list
            List<IndividualJudgement<T>> provisionals = new List<IndividualJudgement<T>>(elites);

            if (count == 1 && provisionals.Count == 1)
            {
                // fast path
                Hillclimb1FastPath(context, jrules, target, rrules, drules, provisionals[0]);
            }
            else
            {
                int threads = PopulationParallelism;

                if (threads == 1)
                {
                    // create lots of offspring, and add them to the list
                    for (int i = 0; i < count; i++)
                    {
                        T next = selector.SelectRandom().Mutate(context, drules, rrules);
                        var j = next.Judge(jrules, target);
                        provisionals.Add(new IndividualJudgement<T>(next, j));
                    }
                }
                else
                {
                    // NOTE: really need to pull CyclePopulation out into an interface, make these implementations a struct, and pass them as a generic parameter to RunEpochs (OSLT)
                    // for the time being, we have to make new random sources every time, which is really bad (really need a type with a 'CyclePopulation' method which holds randoms internally (can generate them once)
                    ModelExecutionContext[] contexts = new ModelExecutionContext[threads];
                    for (int i = 0; i < threads; i++)
                        contexts[i] = new ModelExecutionContext(new MathNet.Numerics.Random.MersenneTwister(context.Rand.Next(0, int.MaxValue), false));

                    // trivial lock based implementation to start with
                    // max savings of ~10% with 4 threads with population of 16
                    // conclusion: not worth the effort at this time
                    T[] parents = new T[count];
                    IndividualJudgement<T>[] cache = new IndividualJudgement<T>[count];

                    for (int i = 0; i < count; i++)
                        parents[i] = selector.SelectRandom();

                    System.Threading.CountdownEvent signaller = new System.Threading.CountdownEvent(threads);

                    Action<int> work = p =>
                    {
                        for (int i = p; i < count; i += threads) // probably not what the cache wants... but unless the population is huge it won't make a difference
                        {
                            T next = parents[i].Mutate(contexts[p], drules, rrules);
                            var j = next.Judge(jrules, target);
                            cache[i] = new IndividualJudgement<T>(next, j);
                        }

                        signaller.Signal();
                    };

                    for (int p = 0; p < threads; p++)
                    {
                        if (p == threads - 1)
                        {
                            work(p);
                        }
                        else
                        {
                            work.BeginInvoke(p, null, null);
                        }
                    }

                    signaller.Wait();

                    provisionals.AddRange(cache);
                }

                // add only the best to the population

                // no LINQ (and no allocations)
                provisionals.Sort(Descending);
                for (int i = 0; i < count; i++)
                {
                    Individuals.Add(provisionals[i].Individual);
                }

                for (int i = count; i < provisionals.Count; i++)
                {
                    provisionals[i].Individual.Recycle(context);
                }

                // LINQ (various allocations)
                //foreach (var ij in provisionals.OrderByDescending(j => j.Judgement.CombinedFitness).Take(count))
                //{
                //    Individuals.Add(ij.Individual);
                //}
            }
        }

        private readonly Comparison<IndividualJudgement<T>> Descending = (a, b) => b.Judgement.CombinedFitness.CompareTo(a.Judgement.CombinedFitness);

        /// <summary>
        /// Returns a member of the population at random, without remove it from the population
        /// Do NOT hold onto the individual, it is liable to be corrupted
        /// </summary>
        public T PeekRandom(RandomSource rand)
        {
            var sample = Individuals[rand.Next(Count)];
            sample.Escaped();
            return sample;
        }

        /// <summary>
        /// Performs the given operation on each individual in the population without removing them from the population.
        /// </summary>
        /// <param name="processor"></param>
        public void Process(Func<T, T> processor)
        {
            for (int i = 0; i < Individuals.Count; i++)
                Individuals[i] = processor(Individuals[i]);
        }
    }

    [StateClass]
    public class PopulationExperimentConfig<T> where T : IIndividual<T>
    {
        [Obsolete]
        protected PopulationExperimentConfig()
        { }

        public PopulationExperimentConfig(ExperimentConfiguration experimentConfiguration, ISelectorPreparer<T> selectorPreparer, int eliteCount, bool hillclimberMode, IPopulationEndTargetOperation<T> populationEndTargetOperation, IPopulationResetOperation<T> populationResetOperation, IPopulationSpinner<T> customPopulationSpinner)
        {
            ExperimentConfiguration = experimentConfiguration;
            SelectorPreparer = selectorPreparer;
            EliteCount = eliteCount;
            HillclimberMode = hillclimberMode;
            PopulationEndTargetOperation = populationEndTargetOperation;
            PopulationResetOperation = populationResetOperation;
            CustomPopulationSpinner = customPopulationSpinner;
        }

        /// <summary>
        /// The base configuration for the experiment
        /// </summary>
        [SimpleStateProperty("ExperimentConfiguration")]
        public ExperimentConfiguration ExperimentConfiguration { get; private set; }

        /// <summary>
        /// The selector preparer
        /// </summary>
        [SimpleStateProperty("SelectorPreparer")]
        public ISelectorPreparer<T> SelectorPreparer { get; private set; }

        /// <summary>
        /// The number of elites to keep each generation
        /// </summary>
        [SimpleStateProperty("EliteCount")]
        public int EliteCount { get; private set; }

        /// <summary>
        /// Whether to employ elites in a hill-climber fashion
        /// </summary>
        [SimpleStateProperty("HillclimberMode")]
        public bool HillclimberMode { get; private set; }
        
        /// <summary>
        /// The Custom Population Spinner
        /// (null means use the default)
        /// </summary>
        [SimpleStateProperty("CustomPopulationSpinner")]
        public IPopulationSpinner<T> CustomPopulationSpinner { get; private set; }

        /// <summary>
        /// The Population end-of-target operation
        /// </summary>
        [SimpleStateProperty("PopulationEndTargetOperation")]
        public IPopulationEndTargetOperation<T> PopulationEndTargetOperation { get; private set; }

        /// <summary>
        /// The Population reset operation
        /// </summary>
        [SimpleStateProperty("PopulationResetOperation")]
        public IPopulationResetOperation<T> PopulationResetOperation { get; private set; }

        public void WriteOut(System.IO.StreamWriter cw)
        {
            cw.WriteLine();
            cw.WriteLine();
            cw.WriteLine($"Population+SelectorPreparer: {SelectorPreparer.Name}");
            cw.WriteLine($"Population+EliteCount: {EliteCount}");
            cw.WriteLine($"Population+HillclimberMode: {HillclimberMode}");
            cw.WriteLine($"Population+CustomPopulationSpinner: {CustomPopulationSpinner?.Description ?? "(null)"}");
            cw.WriteLine($"Population+PopulationEndTargetOperation: {PopulationEndTargetOperation?.Name ?? "(null)"}");
            cw.WriteLine($"Population+PopulationResetOperation: {PopulationResetOperation?.Name ?? "(null)"}");

            ExperimentConfiguration.WriteOut(cw);
        }
    }

    /// <summary>
    /// Signals that a new round is about to begin with a new target
    /// </summary>
    /// <param name="stuff">Stuff</param>
    /// <param name="population">The population (transient)</param>
    /// <param name="target">The new target</param>
    /// <param name="epoch">The current epoch (1-indexed)</param>
    /// <param name="generationCount">The total number of generations seen</param>
    /// <param name="oldPopulationJudgements">The population from which the current population was produced, along with the judgement of each individual</param>
    public delegate void EndPopulationTargetDelegate<T>(FileStuff stuff, Population<T> population, ITarget target, int epoch, long generationCount, IReadOnlyList<IndividualJudgement<T>> oldPopulationJudgements) where T : IIndividual<T>;

    /// <summary>
    /// Signals that a round has just ended
    /// </summary>
    /// <param name="stuff">Stuff</param>
    /// <param name="population">The population (transient)</param>
    /// <param name="target">The current (about to be swapped out) target</param>
    /// <param name="epoch">The current epoch (1-indexed)</param>
    /// <param name="generationCount">The total number of generations seen</param>
    public delegate void StartPopulationTargetDelegate<T>(FileStuff stuff, Population<T> population, ITarget target, int epoch, long generationCount) where T : IIndividual<T>;

    /// <summary>
    /// Signals that an epoch has just ended
    /// </summary>
    /// <param name="stuff">Stuff</param>
    /// <param name="population">The population (transient)</param>
    /// <param name="epochCount">The number of epochs run</param>
    /// <param name="generationCount">The total number of generations seen</param>
    /// <param name="oldPopulationJudgements">The population from which the current population was produced, along with the judgement of each individual</param>
    public delegate void EndPopulationEpochDelegate<T>(FileStuff stuff, Population<T> population, int epochCount, long generationCount, IReadOnlyList<IndividualJudgement<T>> oldPopulationJudgements) where T : IIndividual<T>;

    /// <summary>
    /// Signals that an experiment has just finished
    /// </summary>
    /// <param name="stuff">Stuff</param>
    /// <param name="population">The population (transient)</param>
    /// <param name="epochCount">The number of epochs run</param>
    /// <param name="generationCount">The total number of generations seen</param>
    public delegate void PopulationFinishedDelegate<T>(FileStuff stuff, Population<T> population, int epochCount, long generationCount) where T : IIndividual<T>;

    /// <summary>
    /// Signals that an experiment has just started
    /// </summary>
    /// <param name="stuff">Stuff</param>
    /// <param name="population">The population (transient)</param>
    public delegate void PopulationStartedDelegate<T>(FileStuff stuff, Population<T> population) where T : IIndividual<T>;

    /// <summary>
    /// Signals the end of the population judgement for the given generation
    /// </summary>
    /// <param name="stuff">Stuff</param>
    /// <param name="populationJudgements">The population judgements</param>
    /// <param name="generationCount">The total number of generations seen</param>
    public delegate void PopulationJudgementFeedbackDelegate<T>(FileStuff stuff, IReadOnlyList<IndividualJudgement<T>> populationJudgements, int epochCount, long generationCount, ITarget target) where T : IIndividual<T>;

    /// <summary>
    /// Something that configures a PopulationExperimentFeedback
    /// </summary>
    /// <param name="populationExperimentFeedback">The PopulationExperimentFeedback</param>
    public delegate void PopulationExperimentFeedbackConfigurator<T>(PopulationExperimentFeedback<T> populationExperimentFeedback) where T : IIndividual<T>;

    public class PopulationExperimentFeedback<T> where T : IIndividual<T>
    {
        public Event<EndPopulationTargetDelegate<T>> EndTarget => _endTarget;
        private Event<EndPopulationTargetDelegate<T>> _endTarget = new Event<EndPopulationTargetDelegate<T>>();

        public Event<StartPopulationTargetDelegate<T>> StartTarget => _startTarget;
        private Event<StartPopulationTargetDelegate<T>> _startTarget = new Event<StartPopulationTargetDelegate<T>>();

        public Event<EndPopulationEpochDelegate<T>> EndEpoch => _endEpoch;
        private Event<EndPopulationEpochDelegate<T>> _endEpoch = new Event<EndPopulationEpochDelegate<T>>();

        public Event<PopulationStartedDelegate<T>> Started => _started;
        private Event<PopulationStartedDelegate<T>> _started = new Event<PopulationStartedDelegate<T>>();

        public Event<PopulationFinishedDelegate<T>> Finished => _finished;
        private Event<PopulationFinishedDelegate<T>> _finished = new Event<PopulationFinishedDelegate<T>>();

        public Event<PopulationJudgementFeedbackDelegate<T>> Judged => _judged;
        private Event<PopulationJudgementFeedbackDelegate<T>> _judged = new Event<PopulationJudgementFeedbackDelegate<T>>();
    }

    public interface IPopulationEndTargetOperation<T> where T : IIndividual<T>
    {
        string Name { get; }
        void Process(ModelExecutionContext context, Population<T> population, DevelopmentRules drules, JudgementRules jrules, ITarget target);
    }

    public interface IPopulationResetOperation<T> where T : IIndividual<T>
    {
        string Name { get; }
        void Reset(ModelExecutionContext context, Population<T> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target);
    }

    [StateClass]
    public class NeutralMutationPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        [Obsolete]
        protected NeutralMutationPopulationResetOperation()
        {
        }

        public NeutralMutationPopulationResetOperation(int neutralMutationCount)
        {
            NeutralMutationCount = neutralMutationCount;
        }

        public string Name => "NeutralMutation" + NeutralMutationCount;

        [SimpleStateProperty("NeutralMutationCount")]
        public int NeutralMutationCount { get; private set; }

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            DenseIndividual neutralise(DenseIndividual individual)
            {
                individual = individual.Clone(context);

                for (int i = 0; i < NeutralMutationCount; i++)
                    individual.Genome.MutateInplace(context, rrules);

                individual.DevelopInplace(context, drules);

                return individual;
            }

            population.Process(neutralise);
        }
    }

    [StateClass]
    public class RandomIndependantPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public string Name => "RandomIndependant";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context: context, newInitialState: MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(di.Genome.Size, bleh => gResetRange.Sample(context.Rand))), context, drules, di.Epigenetic));
        }
    }

    [StateClass]
    public class RandomBinaryIndependantPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public string Name => "RandomBinaryIndependant";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context: context, newInitialState: MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(di.Genome.Size, bleh => context.Rand.NextBoolean() ? gResetRange.Min : gResetRange.Max)), context, drules, di.Epigenetic));
        }
    }

    [StateClass]
    public class RandomModulesPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        [Obsolete]
        protected RandomModulesPopulationResetOperation()
        {
        }

        public RandomModulesPopulationResetOperation(Modules modules, NoiseType noiseType, double mag)
        {
            Modules = modules ?? throw new ArgumentNullException(nameof(modules));
            NoiseType = noiseType;
            Magnitude = mag;
        }

        public string Name => $"RandomModules (NoiseType: {NoiseType}, Magnitude: {Magnitude}, Modules: {Modules.ToString()})";

        [SimpleStateProperty("Modules")]
        public Modules Modules { get; private set; }

        [SimpleStateProperty("NoiseType")]
        public NoiseType NoiseType { get; private set; }

        [SimpleStateProperty("Magnitude")]
        public double Magnitude { get; private set; }

        public MathNet.Numerics.LinearAlgebra.Vector<double> Sample(RandomSource rand)
        {
            var v = MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(Modules.ElementCount);

            foreach (var m in Modules.ModuleAssignments)
            {
                var r = Misc.NextNoise(rand, Magnitude, NoiseType);

                foreach (var i in m)
                {
                    v[i] = r;
                }
            }

            return v;
        }

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context: context, newInitialState: Sample(context.Rand)), context, drules, di.Epigenetic));
        }
    }

    [StateClass]
    public class MatchTargetVectorPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public string Name => "MatchTargetVector";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context: context, newInitialState: ((VectorTarget)target.Unwrap()).Vector), context, drules, di.Epigenetic));
        }
    }

    [StateClass]
    public class MatchTargetVectorPerfectPhenotypePopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public string Name => "MatchTargetVectorPerfectPhenotype";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context: context, newInitialState: ((VectorTarget)target.Unwrap()).PreparePerfectP()), context, drules, di.Epigenetic));
        }
    }

    [StateClass]
    public class MatchTargetForcingPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public MatchTargetForcingPopulationResetOperation()
        {
        }

        public string Name => "MatchTargetForcing";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context: context, newInitialState: ((CorrelationMatrixTarget)target.Unwrap()).ForcingVector.Clone()), context, drules, di.Epigenetic));
        }
    }

    [StateClass]
    public class AdditiveNoisePopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        [Obsolete]
        protected AdditiveNoisePopulationResetOperation()
        {
        }

        public AdditiveNoisePopulationResetOperation(NoiseType noiseType, double noiseMagnitude, bool clamp)
        {
            NoiseType = noiseType;
            NoiseMagnitude = noiseMagnitude;
            Clamp = clamp;
        }

        public string Name => "AdditiveNoise" + NoiseType + NoiseMagnitude + (Clamp ? "Clamp" : "");

        [SimpleStateProperty("NoiseType")]
        public NoiseType NoiseType { get; private set; }

        [SimpleStateProperty("NoiseMagnitude")]
        public double NoiseMagnitude { get; private set; }
        
        [SimpleStateProperty("Clamp")]
        public bool Clamp { get; private set; }

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            DenseIndividual addNoise(DenseIndividual individual)
            {
                individual = individual.Clone(context);

                var initialState = individual.Genome.InitialState;

                for (int i = 0; i < initialState.Count; i++)
                {
                    var μ = Misc.NextNoise(context.Rand, NoiseMagnitude, NoiseType);

                    if (Clamp)
                        initialState[i] = gResetRange.Clamp(initialState[i] + μ);
                    else
                        initialState[i] += μ;
                }

                individual.DevelopInplace(context, drules);

                return individual;
            }

            population.Process(addNoise);
        }
    }

    [StateClass]
    public class ZeroPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public string Name => "Zero";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context: context, newInitialState: MathNet.Numerics.LinearAlgebra.CreateVector.Dense<double>(di.Genome.Size, 0.0)), context, drules, di.Epigenetic));
        }
    }

    [StateClass]
    public class NoPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public string Name => "None";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context), context, drules, di.Epigenetic)); // clone directly
        }
    }

    [StateClass]
    public class DenseNoPopulationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public string Name => "None";

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            // TODO: do we need to clone?
            population.Process(di => di.Clone(context));
        }
    }

    [StateClass]
    public class GenericNoPopulationResetOperation<TIndividual> : IPopulationResetOperation<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        public string Name => "None";

        public void Reset(ModelExecutionContext context, Population<TIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            // TODO: do we need to clone?
            population.Process(i => i.Clone(context));
        }
    }

    [StateClass]
    public class IterativeNormalisationResetOperation : IPopulationResetOperation<DenseIndividual>
    {
        public IterativeNormalisationResetOperation(double rowSum)
        {
            RowSum = rowSum;
        }

        // TODO: verify this is a Sinkhorn-Knopp with mul
        public string Name => "IterativeNormalisation" + RowSum;

        [SimpleStateProperty("RowSum")]
        public double RowSum { get; private set; }

        public void Reset(ModelExecutionContext context, Population<DenseIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            population.Process(di => DenseIndividual.Develop(di.Genome.Clone(context: context, newTransMat: Misc.IterativeNormalise(di.Genome.TransMat, MathNet.Numerics.LinearAlgebra.CreateVector.Dense(di.Genome.Size, RowSum), MathNet.Numerics.LinearAlgebra.CreateVector.Dense(di.Genome.Size, RowSum))), context, drules, di.Epigenetic));
        }
    }

    [StateClass]
    public class CombinedPopulationResetOperation<TIndividual> : IPopulationResetOperation<TIndividual> where TIndividual : IIndividual<TIndividual>
    {
        [Obsolete]
        protected CombinedPopulationResetOperation()
        { }

        public CombinedPopulationResetOperation(IReadOnlyList<IPopulationResetOperation<TIndividual>> subOps)
        {
            SubOps = subOps ?? throw new ArgumentNullException(nameof(subOps));
        }

        public string Name => $"CombinedPopulationResetOperation({string.Join("||", SubOps.Select(so => so.Name))})";

        [SimpleStateProperty("SubOps")]
        public IReadOnlyList<IPopulationResetOperation<TIndividual>> SubOps { get; private set; }

        public void Reset(ModelExecutionContext context, Population<TIndividual> population, Range gResetRange, DevelopmentRules drules, ReproductionRules rrules, ITarget target)
        {
            foreach (var subOp in SubOps)
                subOp.Reset(context, population, gResetRange, drules, rrules, target);
        }
    }

    public static class PopulationResetOperations
    {
        public static readonly IPopulationResetOperation<DenseIndividual> RandomIndependent = new RandomIndependantPopulationResetOperation();
        public static readonly IPopulationResetOperation<DenseIndividual> RandomBinaryIndependent = new RandomBinaryIndependantPopulationResetOperation();
        public static readonly IPopulationResetOperation<DenseIndividual> MatchTargetVector = new MatchTargetVectorPopulationResetOperation();
        public static readonly IPopulationResetOperation<DenseIndividual> MatchTargetVectorPerfectPhenotype = new MatchTargetVectorPerfectPhenotypePopulationResetOperation();
        public static readonly IPopulationResetOperation<DenseIndividual> MatchForcingVector = new MatchTargetForcingPopulationResetOperation();
        public static readonly IPopulationResetOperation<DenseIndividual> Zero = new ZeroPopulationResetOperation();
        public static readonly IPopulationResetOperation<DenseIndividual> None = new NoPopulationResetOperation();
        public static IPopulationResetOperation<TIndividual> GenericNone<TIndividual>() where TIndividual : IIndividual<TIndividual> => new GenericNoPopulationResetOperation<TIndividual>();
    }

    [StateClass]
    public class ProportionalSelectorPreparer<T> : ISelectorPreparer<T> where T : IIndividual<T>
    {
        public string Name => "Proportional";

        public ISelector<T> Prepare(IReadOnlyList<IndividualJudgement<T>> judgements, RandomSource rand)
        {
            var wsel = new WeightedSelectorMk2<T>(rand, judgements.Count);

            foreach (var ij in judgements)
            {
                wsel.Add(ij.Individual, ij.Judgement.CombinedFitness);
            }

            return wsel;
        }
    }

    [StateClass]
    public class RankedSelectorPreparer<T> : ISelectorPreparer<T> where T : IIndividual<T>
    {
        public string Name => "Ranked";

        public ISelector<T> Prepare(IReadOnlyList<IndividualJudgement<T>> judgements, RandomSource rand)
        {
            var wsel = new WeightedSelectorMk2<T>(rand, judgements.Count);

            int rank = 1;
            foreach (var ij in judgements.OrderBy(_ij => _ij.Judgement.CombinedFitness))
            {
                wsel.Add(ij.Individual, rank);
                rank++;
            }

            return wsel;
        }
    }

    [StateClass]
    public class ParetoSelectorPreparer<T> : ISelectorPreparer<T> where T : IIndividual<T>
    {
        public string Name => "Pareto";

        public ISelector<T> Prepare(IReadOnlyList<IndividualJudgement<T>> judgements, RandomSource rand)
        {
            var psel = new ParetoSelector<T>(rand);

            foreach (var ij in judgements)
            {
                psel.Add(ij.Individual, new[] { ij.Judgement.Benefit, ij.Judgement.Cost });
            }

            return psel;
        }
    }

    public static class SelectorPreparers<T> where T : IIndividual<T>
    {
        public static readonly ISelectorPreparer<T> Proportional = new ProportionalSelectorPreparer<T>();
        public static readonly ISelectorPreparer<T> Ranked = new RankedSelectorPreparer<T>();
        public static readonly ISelectorPreparer<T> Pareto = new ParetoSelectorPreparer<T>();
    }

    // really want to be able to save/load these as a single package... need to think about some stuff first...

    public interface IUnknownPopulationExperimentTypeReceiver<TResult>
    {
        TResult Receive<T>(PopulationExperiment<T> populationExperiment) where T : IIndividual<T>;
    }

    public static class PopulationExperimentHelpers
    {
        public static TResult LoadUnknownType<TResult>(string fileName, IUnknownPopulationExperimentTypeReceiver<TResult> receiver)
        {
            using (var fs = System.IO.File.Open(fileName, System.IO.FileMode.Open))
            {
                object untyped = GraphSerialisation.Read<object>(fs); // we assume this is a PopulationExperiment<T>

                // find out T
                Type individualType = untyped.GetType().GetGenericArguments()[0];

                // type gap
                return NetState.AutoState.AutoSerialisationHelpers.CallNamedStatic<TResult>(typeof(PopulationExperimentHelpers), null, nameof(CallbackReceiver), new[] { individualType, typeof(TResult) }, untyped, receiver);
            }
        }

        private static TResult CallbackReceiver<T, TResult>(PopulationExperiment<T> populationExperiment, IUnknownPopulationExperimentTypeReceiver<TResult> receiver) where T : IIndividual<T>
        {
            return receiver.Receive(populationExperiment);
        }
    }

    [StateClass]
    public class PopulationExperiment<T> where T : IIndividual<T>
    {
        public PopulationExperiment(Population<T> population, PopulationExperimentConfig<T> populationConfig, FileStuff fileStuff)
        {
            Population = population;
            PopulationConfig = populationConfig;
            FileStuff = fileStuff;
            Epoch = 0;
            TotalGenerationCount = 0;
        }

        public PopulationExperiment(Population<T> population, PopulationExperimentConfig<T> populationConfig, FileStuff fileStuff, int startingEpoch, long startingGenerationCount)
        {
            Population = population;
            PopulationConfig = populationConfig;
            FileStuff = fileStuff;
            Epoch = startingEpoch;
            TotalGenerationCount = startingGenerationCount;
        }

        [Obsolete]
        protected PopulationExperiment()
        { }

        /// <summary>
        /// The current Population
        /// </summary>
        [SimpleStateProperty("Population")]
        public Population<T> Population { get; private set; }

        /// <summary>
        /// The Population Configuration
        /// </summary>
        [SimpleStateProperty("PopulationConfig")]
        public PopulationExperimentConfig<T> PopulationConfig { get; private set; }

        /// <summary>
        /// The file stuff we are spitting stuff into
        /// </summary>
        [SimpleStateProperty("FileStuff")]
        public FileStuff FileStuff { get; private set; }

        /// <summary>
        /// The current epoch
        /// </summary>
        [SimpleStateProperty("Epoch")]
        public int Epoch { get; private set; }

        /// <summary>
        /// The current generations
        /// </summary>
        [SimpleStateProperty("TotalGenerationCountLong")]
        public long TotalGenerationCount { get; private set; }

        [DeprecatedSimpleStateProperty("TotalGenerationCount")]
        private int _totalGenerationCountShort
        {
            get
            {
                throw new InvalidOperationException("Attempted to access deprecated property '_totalGenerationCountShort'");
            }
            set
            {
                TotalGenerationCount = value;
            }
        }

        /// <summary>
        /// The current target index
        /// </summary>
        [SimpleStateProperty("CurrentTargetIndex")]
        public int CurrentTargetIndex { get; private set; }

        public void Save(string postfix, bool appendTimestamp)
        {
            string fname = "epoch" + Epoch + "save" + postfix;

            if (appendTimestamp)
                fname += Misc.NowTime;

            fname += ".dat";
            string tempfname = "~" + fname;

            using (var fs = FileStuff.CreateBin(tempfname))
            {
                SaveTo(fs);
            }

            System.IO.File.Copy(FileStuff.File(tempfname), FileStuff.File(fname), true);
            System.IO.File.Delete(FileStuff.File(tempfname));
        }

        public void SaveTo(string filename)
        {
            using (var fs = System.IO.File.Open(filename, System.IO.FileMode.Create))
            {
                SaveTo(fs);
            }
        }

        public void SaveTo(System.IO.FileStream fs)
        {
            GraphSerialisation.Write(this, fs);
        }

        public PopulationExperiment<T> Clone(FileStuff fileStuff)
        {
            return new PopulationExperiment<T>(Population.Clone(), PopulationConfig, fileStuff, Epoch, TotalGenerationCount);
        }

        public static PopulationExperiment<T> Load(string fileName)
        {
            using (var fs = System.IO.File.OpenRead(fileName))
            {
                return GraphSerialisation.Read<PopulationExperiment<T>>(fs);
            }
        }

        /// <summary>
        /// Runs a single epoch of the experiment.
        /// Feedback may be null.
        /// If you don't want to use this (e.g. you have a population but not a population experiment), then be sure to understand the order in which things need to happen:
        ///  shuffle the targets if necessary
        ///  for as many times as we have targets
        ///    cycle to the next target
        ///    call NextExposure on the target with appropriate ExposureInformation
        ///    reset the population with probability config.InitialStateResetProbability
        ///    give up on the exposure at this point if exposureInformation.ExposureDuration is less than zero (proceed if it is exactly zero, e.g. to allow delta/hebbian spinner to do its thing)
        ///    spin the population with the given exposureInformation.ExposureDuration
        ///    perform the PopulationEndTargetOperation
        /// </summary>
        public IReadOnlyList<IndividualJudgement<T>> RunEpoch(ModelExecutionContext context, PopulationExperimentFeedback<T> feedback)
        {
            var config = PopulationConfig.ExperimentConfiguration;
            var populationConfig = PopulationConfig;
            var population = Population;

            int size = config.Size;
            DevelopmentRules drules = config.DevelopmentRules;
            ReproductionRules rrules = config.ReproductionRules;
            JudgementRules jrules = config.JudgementRules;

            int[] targetShuffling = config.ShuffleTargets
                ? Misc.Create<int>(config.Targets.Count, i => i)
                : null;

            IReadOnlyList <IndividualJudgement<T>> judgements = null;

            var populationSpinner = populationConfig.CustomPopulationSpinner ?? DefaultPopulationSpinner<T>.Instance;

            Epoch++;

            // shuffle targets, if we must
            if (config.ShuffleTargets)
            {
                Misc.ShuffleInplace(context.Rand, targetShuffling);
            }
            int currentTargetShuffleIndex = config.ShuffleTargets
                ? 0
                : CurrentTargetIndex;

            for (int ti = 0; ti < config.Targets.Count; ti++)
            {
                // NOTE: by doing this here, we are skipping the 0th target and moving onto the 1th: important when running mirror tracees and when assessing wholesamples
                // (this behaviour is preserved for backwards compatability, but is not generally considered desirable, though it doesn't affect single-target experiments)
                // select next currentTarget
                currentTargetShuffleIndex = config.TargetCycler.Cycle(context.Rand, currentTargetShuffleIndex, config.Targets.Count);
                CurrentTargetIndex = config.ShuffleTargets
                    ? targetShuffling[currentTargetShuffleIndex]
                    : currentTargetShuffleIndex;

                ITarget currentTarget = config.Targets[CurrentTargetIndex];

                ExposureInformation exposureInformation = new ExposureInformation(config.GenerationsPerTargetPerEpoch);
                currentTarget.NextExposure(context.Rand, jrules, Epoch, ref exposureInformation);

                // reset if we should
                if (context.Rand.NextDouble() < config.InitialStateResetProbability)
                    populationConfig.PopulationResetOperation?.Reset(context, population, config.InitialStateResetRange, drules, rrules, currentTarget);
 
                if (exposureInformation.ExposureDuration < 0)
                    continue; // allow zero through as a special case; < 0 means skip altogether

                feedback?.StartTarget.Call(st => st(FileStuff, population, currentTarget, Epoch, TotalGenerationCount));

                // mutate, judge, select cycle
                Action<int, IReadOnlyList<IndividualJudgement<T>>> judgementCallback = (feedback?.Judged.HasAny ?? false) ? // do a special check, so that we only incur the cost in the loop if it is going somewhere
                    new Action<int, IReadOnlyList<IndividualJudgement<T>>>((g, pjs) => feedback?.Judged.Call(act => act(FileStuff, pjs, Epoch, TotalGenerationCount + g, currentTarget))) : null;
                judgements = populationSpinner.SpinPopulation(population, context, rrules, drules, jrules, currentTarget, populationConfig.SelectorPreparer, exposureInformation.ExposureDuration, judgementCallback, populationConfig.EliteCount, populationConfig.HillclimberMode);
                populationConfig.PopulationEndTargetOperation?.Process(context, population, populationConfig.ExperimentConfiguration.DevelopmentRules, populationConfig.ExperimentConfiguration.JudgementRules, currentTarget);

                TotalGenerationCount += exposureInformation.ExposureDuration;

                feedback?.EndTarget.Call(et => et(FileStuff, population, currentTarget, Epoch, TotalGenerationCount, judgements));
            }

            feedback?.EndEpoch.Call(ee => ee(FileStuff, population, Epoch, TotalGenerationCount, judgements));

            return judgements;
        }

        public void WriteOutConfig(System.IO.StreamWriter cw, RandomSource rand)
        {
            cw.WriteLine("# Config for " + FileStuff.OutDir);

            PopulationConfig.WriteOut(cw);

            cw.WriteLine();
            cw.WriteLine();
            cw.WriteLine("# Population Info");

            cw.WriteLine($"Size = {Population.Count}");

            cw.WriteLine();
            cw.WriteLine();
            cw.WriteLine("# Example Inidividual Info");

            T sample = Population.PeekRandom(rand);
            sample.WriteOut(cw);
        }
    }

    public class PopulationExperimentRunners
    {
        public static PopulationExperiment<T> PrepareExperiment<T>(Population<T> population, PopulationExperimentConfig<T> populationConfig, string outDir, string filePrefix = "", string titlePrefix = "", bool disableAllIO = false, bool appendTimestamp = true) where T : IIndividual<T>
        {
            FileStuff stuff = FileStuff.CreateNow(outDir, filePrefix, titlePrefix, !disableAllIO, appendTimestamp);
            var populationExperiment = new PopulationExperiment<T>(population, populationConfig, stuff);
            return populationExperiment;
        }

        public static PopulationExperiment<T> PrepareExperiment<T>(Population<T> population, PopulationExperimentConfig<T> populationConfig, string outDir, int startingEpoch, long startingGenerationCount, string filePrefix = "", string titlePrefix = "", bool disableAllIO = false, bool appendTimestamp = true) where T : IIndividual<T>
        {
            FileStuff stuff = FileStuff.CreateNow(outDir, filePrefix, titlePrefix, !disableAllIO, appendTimestamp);
            var populationExperiment = new PopulationExperiment<T>(population, populationConfig, stuff, startingEpoch, startingGenerationCount);
            return populationExperiment;
        }

        public static void WriteOutInitials<T>(RandomSource rand, PopulationExperiment<T> populationExperiment) where T : IIndividual<T>
        {
            var stuff = populationExperiment.FileStuff;
            var populationConfig = populationExperiment.PopulationConfig;
            var population = populationExperiment.Population;

            // save config
            using (var cfs = stuff.CreateBin("config.dat"))
            {
                GraphSerialisation.Write(populationConfig, cfs);
            }

            // save population
            using (var cfs = stuff.CreateBin("initialpopulation.dat"))
            {
                GraphSerialisation.Write(population, cfs);
            }

            // print out config
            using (var cw = stuff.Open("config.txt"))
            {
                populationExperiment.WriteOutConfig(cw, rand);
            }
        }

        /// <summary>
        /// Runs many epochs.
        /// </summary>
        /// <param name="console">The <see cref="TextWriter"/> to spam.</param>
        /// <param name="cliPrefix">The prefix to use when writing to <paramref name="console"/>.</param>
        /// <param name="epochCount">The number of epochs to run.</param>
        /// <param name="context">The execution context.</param>
        /// <param name="populationExperiment">The experiment.</param>
        /// <param name="feedback">The feedback.</param>
        /// <param name="savePeriodEpochs">The number of epochs between saving the experiment.</param>
        /// <param name="savePeriodSeconds">The number sections between saving the experiment if <paramref name="savePeriodEpochs"/> is exceeded.</param>
        /// <param name="nosave">Whether to not save outputs as it goes.</param>
        public static void RunEpochs<T>(TextWriter console, string cliPrefix, int epochCount, ModelExecutionContext context, PopulationExperiment<T> populationExperiment, PopulationExperimentFeedback<T> feedback, int savePeriodEpochs = 500, int savePeriodSeconds = 60 * 60, bool nosave = false) where T : IIndividual<T>
        {
            var config = populationExperiment.PopulationConfig.ExperimentConfiguration;
            var stuff = populationExperiment.FileStuff;

            int progressReportingPeriod = Math.Max(1, epochCount / 500);

            feedback?.Started.Call(_f => _f(stuff, populationExperiment.Population));

            Stopwatch sw = new Stopwatch();
            sw.Reset();
            sw.Start();

            Stopwatch saveSw = new Stopwatch();
            saveSw.Reset();
            saveSw.Start();

            for (int epoch = 1; epoch <= epochCount; epoch++)
            {
                var judgements = populationExperiment.RunEpoch(context, feedback);

                if (epoch % progressReportingPeriod == 0)
                {
                    double remEstMilliseconds = sw.ElapsedMilliseconds * ((double)(epochCount - epoch) / epoch);
                    TimeSpan remEst = TimeSpan.FromMilliseconds(remEstMilliseconds);

                    double percentProgress = (epoch / (double)config.Epochs) * 100.0;
                    double meanFitness = judgements.Average(ij => ij.Judgement.CombinedFitness);
                    double meanBenefit = judgements.Average(ij => ij.Judgement.Benefit);
                    double meanCost = judgements.Average(ij => ij.Judgement.Cost);
                    console.WriteLine(cliPrefix + $"Epoch{epoch} ({percentProgress:00.0}%):\t f̅={meanFitness:0.000}, b̅={meanBenefit:0.000}, c̅={meanCost:0.000}\tre={remEst.ToString(@"d\:hh\:mm\:ss")}");
                }

                if (!nosave)
                {
                    if ((savePeriodEpochs != -1 && (populationExperiment.Epoch % savePeriodEpochs == 0))
                    || (savePeriodSeconds != -1 && savePeriodSeconds * 1000 < saveSw.ElapsedMilliseconds))
                    {
                        populationExperiment.Save("", false);
                        saveSw.Restart();
                    }
                }
            }

            feedback?.Finished.Call(_f => _f(stuff, populationExperiment.Population, populationExperiment.Epoch, populationExperiment.TotalGenerationCount));

            if (!nosave)
                populationExperiment.Save("terminal", false);
        }

        public static void RunExperiment<T>(TextWriter console, string cliPrefix, ModelExecutionContext context, Population<T> population, PopulationExperimentConfig<T> populationConfig, string outDir, bool disableTransientIO, bool disableAllIO = false, PopulationExperimentFeedback<T> feedback = null, string filePrefix = "", string titlePrefix = "") where T : IIndividual<T>
        {
            FileStuff stuff = FileStuff.CreateNow(outDir, filePrefix, titlePrefix, !disableAllIO);
            RunExperiment<T>(console, cliPrefix, context, population, populationConfig, stuff, disableTransientIO, disableAllIO, feedback);
        }

        public static PopulationExperiment<T> RunExperiment<T>(TextWriter console, string cliPrefix, ModelExecutionContext context, Population<T> population, PopulationExperimentConfig<T> populationConfig, FileStuff stuff, bool disableTransientIO, bool disableAllIO = false, PopulationExperimentFeedback<T> feedback = null) where T : IIndividual<T>
        {
            var rand = context.Rand;

            var populationExperiment = new PopulationExperiment<T>(population, populationConfig, stuff);
            populationExperiment.Save("start", false);

            WriteOutInitials(rand, populationExperiment);
            console.WriteLine(populationExperiment.FileStuff.OutDir);

            RunEpochs(console, cliPrefix, populationConfig.ExperimentConfiguration.Epochs, context, populationExperiment, feedback);

            return populationExperiment;
        }
    }
}