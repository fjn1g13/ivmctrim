﻿using NetState.AutoState;
using NetState.Serialisation;
using System;
using System.Collections.Generic;
using System.Text;
using Linear = MathNet.Numerics.LinearAlgebra;

namespace M4M.State
{
    //
    // Vector
    //

    public class AutoDenseVectorStateProvider<T, TElementProvider> where TElementProvider : IStateProvider<T, IStreamContext, IStreamContext> where T : struct, IFormattable, IEquatable<T>
    {
        public static readonly DenseVectorStateProvider<T, IStreamContext, IStreamContext> Instance = new DenseVectorStateProvider<T, IStreamContext, IStreamContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, IStreamContext, IStreamContext>>(typeof(TElementProvider)));
    }

    public class AutoDenseVectorStateProvider<T, TElementProvider, TWriteContext, TReadContext> where TElementProvider : IStateProvider<T, TWriteContext, TReadContext> where TWriteContext : IStreamContext where TReadContext : IStreamContext where T : struct, IFormattable, IEquatable<T>
    {
        public static readonly DenseVectorStateProvider<T, TWriteContext, TReadContext> Instance = new DenseVectorStateProvider<T, TWriteContext, TReadContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, TWriteContext, TReadContext>>(typeof(TElementProvider)));
    }

    public class DenseVectorStateProvider<T, TWriteContext, TReadContext> : IClassStateProvider<Linear.Vector<T>, TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<Linear.Vector<T>, TWriteContext, TReadContext, object, object> where TWriteContext : IStreamContext where TReadContext : IStreamContext where T : struct, IFormattable, IEquatable<T>
    {
        public DenseVectorStateProvider(IStateProvider<T, TWriteContext, TReadContext> elementStateProvider)
        {
            ElementStateProvider = elementStateProvider;
        }

        public IStateProvider<T, TWriteContext, TReadContext> ElementStateProvider { get; }

        public IClassStateProvider<Linear.Vector<T>, TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this;
        }

        public void WriteProviderConfig(object context)
        {
            // nix
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public Linear.Vector<T> ReadCreate(TReadContext context)
        {
            int count = context.StreamRw.ReadInt32();
            
            T[] arr = new T[count];

            return Linear.CreateVector.DenseOfArray<T>(arr);
        }

        public void ReadState(TReadContext context, Linear.Vector<T> state)
        {
            for (int i = 0; i < state.Count; i++)
            {
                state[i] = ElementStateProvider.Read(context);
            }
        }
        
        public void WriteCreate(TWriteContext context, Linear.Vector<T> state)
        {
            int count = state.Count;
            context.StreamRw.WriteInt32(count);
        }

        public void WriteState(TWriteContext context, Linear.Vector<T> state)
        {
            for (int i = 0; i < state.Count; i++)
            {
                ElementStateProvider.Write(context, state[i]);
            }
        }
        
        public Linear.Vector<T> Read(TReadContext context)
        {
            int count = context.StreamRw.ReadInt32();

            if (count < 0)
                return null;

            T[] arr = new T[count];

            for (int i = 0; i < count; i++)
            {
                arr[i] = ElementStateProvider.Read(context);
            }

            return Linear.CreateVector.DenseOfArray(arr);
        }

        public void Write(TWriteContext context, Linear.Vector<T> state)
        {
            if (state == null)
            {
                context.StreamRw.WriteInt32(-1);
                return;
            }

            int count = state.Count;
            context.StreamRw.WriteInt32(count);
            
            for (int i = 0; i < count; i++)
            {
                ElementStateProvider.Write(context, state[i]);
            }
        }
    }

    //
    // Matrix
    //
    
    public class AutoDenseMatrixStateProvider<T, TElementProvider> where TElementProvider : IStateProvider<T, IStreamContext, IStreamContext> where T : struct, IFormattable, IEquatable<T>
    {
        public static readonly DenseMatrixStateProvider<T, IStreamContext, IStreamContext> Instance = new DenseMatrixStateProvider<T, IStreamContext, IStreamContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, IStreamContext, IStreamContext>>(typeof(TElementProvider)));
    }

    public class AutoDenseMatrixStateProvider<T, TElementProvider, TWriteContext, TReadContext> where TElementProvider : IStateProvider<T, TWriteContext, TReadContext> where TWriteContext : IStreamContext where TReadContext : IStreamContext where T : struct, IFormattable, IEquatable<T>
    {
        public static readonly DenseMatrixStateProvider<T, TWriteContext, TReadContext> Instance = new DenseMatrixStateProvider<T, TWriteContext, TReadContext>(AutoSerialisationHelpers.AcquireInstance<IStateProvider<T, TWriteContext, TReadContext>>(typeof(TElementProvider)));
    }

    public class DenseMatrixStateProvider<T, TWriteContext, TReadContext> : IClassStateProvider<Linear.Matrix<T>, TWriteContext, TReadContext, object, object>, IClassStateProviderFactory<Linear.Matrix<T>, TWriteContext, TReadContext, object, object> where TWriteContext : IStreamContext where TReadContext : IStreamContext where T : struct, IFormattable, IEquatable<T>
    {
        public DenseMatrixStateProvider(IStateProvider<T, TWriteContext, TReadContext> elementStateProvider)
        {
            ElementStateProvider = elementStateProvider;
        }

        public IStateProvider<T, TWriteContext, TReadContext> ElementStateProvider { get; }
        
        public IClassStateProvider<Linear.Matrix<T>, TWriteContext, TReadContext, object, object> CreateNew()
        {
            return this;
        }

        public void WriteProviderConfig(object context)
        {
            // nix
        }

        public void ReadProviderConfig(object context)
        {
            // nix
        }

        public Linear.Matrix<T> ReadCreate(TReadContext context)
        {
            int rows = context.StreamRw.ReadInt32();
            int cols = context.StreamRw.ReadInt32();
            
            T[,] arr = new T[rows, cols];

            return Linear.CreateMatrix.DenseOfArray<T>(arr);
        }

        public void ReadState(TReadContext context, Linear.Matrix<T> state)
        {
            for (int i = 0; i < state.RowCount; i++)
            {
                for (int j = 0; j < state.ColumnCount; j++)
                {
                    state[i, j] = ElementStateProvider.Read(context);
                }
            }
        }
        
        public void WriteCreate(TWriteContext context, Linear.Matrix<T> state)
        {
            int rows = state.RowCount;
            int cols = state.ColumnCount;
            context.StreamRw.WriteInt32(rows);
            context.StreamRw.WriteInt32(cols);
        }

        public void WriteState(TWriteContext context, Linear.Matrix<T> state)
        {
            for (int i = 0; i < state.RowCount; i++)
            {
                for (int j = 0; j < state.ColumnCount; j++)
                {
                    ElementStateProvider.Write(context, state[i, j]);
                }
            }
        }
        
        public Linear.Matrix<T> Read(TReadContext context)
        {
            int rows = context.StreamRw.ReadInt32();

            if (rows == -1)
                return null;

            int cols = context.StreamRw.ReadInt32();
            
            T[,] arr = new T[rows, cols];

            var res = Linear.CreateMatrix.DenseOfArray<T>(arr);

            ReadState(context, res);

            return res;
        }

        public void Write(TWriteContext context, Linear.Matrix<T> state)
        {
            if (state == null)
            {
                context.StreamRw.WriteInt32(-1);
                return;
            }

            WriteCreate(context, state);
            WriteState(context, state);
        }
    }

    //
    // Structs
    //

    public class MatrixEntryAddressStateProvider : IStateProvider<MatrixEntryAddress, IStreamContext, IStreamContext>
    {
        public static readonly MatrixEntryAddressStateProvider Instance = new MatrixEntryAddressStateProvider();

        public MatrixEntryAddress Read(IStreamContext context)
        {
            int r = context.StreamRw.ReadInt32();
            int c = context.StreamRw.ReadInt32();
            return new MatrixEntryAddress(r, c);
        }

        public void Write(IStreamContext context, MatrixEntryAddress state)
        {
            context.StreamRw.WriteInt32(state.Row);
            context.StreamRw.WriteInt32(state.Col);
        }
    }

    public class MultiMeasureJudgementStateProvider : IStateProvider<MultiMeasureJudgement, IStreamContext, IStreamContext>
    {
        public static readonly MultiMeasureJudgementStateProvider Instance = new MultiMeasureJudgementStateProvider();

        public MultiMeasureJudgement Read(IStreamContext context)
        {
            double b = DoubleStateProvider.Instance.Read(context);
            double c = DoubleStateProvider.Instance.Read(context);
            double cf = DoubleStateProvider.Instance.Read(context);
            return new MultiMeasureJudgement(b, c, cf);
        }

        public void Write(IStreamContext context, MultiMeasureJudgement state)
        {
            DoubleStateProvider.Instance.Write(context, state.Benefit);
            DoubleStateProvider.Instance.Write(context, state.Cost);
            DoubleStateProvider.Instance.Write(context, state.CombinedFitness);
        }
    }
}
