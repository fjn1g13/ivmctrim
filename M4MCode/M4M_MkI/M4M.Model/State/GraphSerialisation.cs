﻿using NetState.AutoState;
using NetState.Serialisation;
using NetState.SoftState;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace M4M.State
{
    /// <summary>
    /// Marks the class for automatric (soft) serialisation
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class StateClassAttribute : SoftClassAttribute
    {
    }

    /// <summary>
    /// Marks the methods as available for use by the automatic (soft) serialiser
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class StateMethodAttribute : SoftStateMethodAttribute
    {
    }

    /// <summary>
    /// Automatically deals with simple primitives and classes
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class SimpleStatePropertyAttribute : SoftPropertyAttributeAttribute
    {
        public SimpleStatePropertyAttribute(string name) : base(name, null, false, typeof(M4MPrimitiveTable))
        {
            // nix
        }
    }

    /// <summary>
    /// Automatically deals with simple primitives and classes
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DeprecatedSimpleStatePropertyAttribute : SoftPropertyAttributeAttribute
    {
        public DeprecatedSimpleStatePropertyAttribute(string name) : base(name, null, true, typeof(M4MPrimitiveTable))
        {
            // nix
        }
    }

    public class MatrixStatePropertyAttribute : SoftPropertyAttributeAttribute
    {
        public MatrixStatePropertyAttribute(string name) : base(name, null, typeof(AutoDenseMatrixStateProvider<double, DoubleStateProvider>))
        {
            // nix
        }
    }

    public class VectorStatePropertyAttribute : SoftPropertyAttributeAttribute
    {
        public VectorStatePropertyAttribute(string name) : base(name, null, typeof(AutoDenseVectorStateProvider<double, DoubleStateProvider>))
        {
            // nix
        }
    }

    public static class GraphSerialisation
    {
        public static void Write<T>(T root, string filename) where T : class
        {
            using (var fs = System.IO.File.OpenRead(filename))
            {
                Write(root, fs);
            }
        }

        public static void Write<T>(T root, Stream stream) where T : class
        {
            var gsi = PrepareGraphSerialisationInfo();
            var writer = AutoGraphSerialisation.CreateWriter<object>(new StreamReaderWriter(stream), gsi);

            writer.WriteRoot(root);
        }

        public static T Read<T>(string filename) where T : class
        {
            using (var fs = System.IO.File.OpenRead(filename))
            {
                return Read<T>(fs);
            }
        }

        public static T Read<T>(Stream stream) where T : class
        {
            var gsi = PrepareGraphSerialisationInfo();
            var reader = AutoGraphSerialisation.CreateReader<object>(new StreamReaderWriter(stream), gsi);

            return reader.ReadRoot<T>();
        }

        public static GraphStreamSerialisationInfo<object> PrepareGraphSerialisationInfo()
        {
            // adds our structs and such to an AutoGraphStateProviderTable
            var primitiveTable = new M4MPrimitiveTable();

            // init gsi
            var gsi = new GraphStreamSerialisationInfo<object>(
                new CustomClassStateProviderTable<object, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>>(),
                new CustomStateProviderProviders<object, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>>(),
                false
                );

            // automatic handling for collections etc.
            gsi.CustomClassStateProviderProviders.AddProvider(new AutoGraphClassStateProviderTable(primitiveTable));

            // automatic soft-class serialisation
            gsi.CustomClassStateProviderProviders.AddProvider(AutoSoftStateProviders<IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>, IWriteGraphStreamContext<object>, IReadGraphStreamContext<object>>.Instance); // this is a good thing
            
            return gsi;
        }
    }

    public class M4MPrimitiveTable : AutoGraphStateProviderTable
    {
        public M4MPrimitiveTable() : base(true)
        {
            // Mathnet
            AddProvider(typeof(MathNet.Numerics.LinearAlgebra.Vector<double>), typeof(AutoDenseVectorStateProvider<double, DoubleStateProvider>));
            AddProvider(typeof(MathNet.Numerics.LinearAlgebra.Matrix<double>), typeof(AutoDenseMatrixStateProvider<double, DoubleStateProvider>));

            // Structs
            AddProvider(typeof(MatrixEntryAddress), typeof(MatrixEntryAddressStateProvider));
            AddProvider(typeof(MultiMeasureJudgement), typeof(MultiMeasureJudgementStateProvider));

            // Enums
            AddProvider(typeof(NoiseType), typeof(AutoEnumStateProvider<NoiseType, int, IntStateProvider>));
            AddProvider(typeof(Hebbian.HebbianType), typeof(AutoEnumStateProvider<Hebbian.HebbianType, int, IntStateProvider>));
            AddProvider(typeof(Hebbian.MatrixNormalisation), typeof(AutoEnumStateProvider<Hebbian.MatrixNormalisation, int, IntStateProvider>));
            AddProvider(typeof(Hopfield.BiasType), typeof(AutoEnumStateProvider<Hopfield.BiasType, int, IntStateProvider>));
            AddProvider(typeof(Hopfield.InteractionType), typeof(AutoEnumStateProvider<Hopfield.InteractionType, int, IntStateProvider>));
            AddProvider(typeof(Hopfield.InteractionMatrixSource), typeof(AutoEnumStateProvider<Hopfield.InteractionMatrixSource, int, IntStateProvider>));
            AddProvider(typeof(Hopfield.BiasVectorSource), typeof(AutoEnumStateProvider<Hopfield.BiasVectorSource, int, IntStateProvider>));
        }
    }
}
