﻿using System;
using System.Linq;

namespace M4M.CoreRunner
{
    public class Program
    {
        private static void ConfigureCulture()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
        }

        private static void ConfigureMathNet(bool quiet, bool parallelMath)
        {
            if (parallelMath)
                MathNet.Numerics.Control.UseMultiThreading();
            else
                MathNet.Numerics.Control.UseSingleThread();

            if (MathNet.Numerics.Control.TryUseNativeOpenBLAS() && !quiet)
                Console.WriteLine("OpenBLAS");
        }

        static void Main(string[] args)
        {
            bool quiet = args.Contains("quiet");
            bool parallelMath = args.Contains("parallelMath");

            if (!quiet)
                Console.WriteLine("~~ M4M Core Running ~~");
            ConfigureCulture();
            ConfigureMathNet(quiet, parallelMath);
            
            Cli cli = Cli.PrepareDefaultCli();
            CliPrompt cliPrompt = new CliPrompt(cli);
            cliPrompt.Run(System.Console.Out, args);
        }
    }
}
