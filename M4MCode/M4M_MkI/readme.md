# What is this file

This file is full of information about M4M. It includes information about building M4M, running M4M, using M4M, a little information about how M4M works internally, and a couple of examples.

The other thing this file is, is totally incomplete: M4M has always been a rapidly developed program, with little features and additional modes bolted on all the time. This file provides some pointers into the code so that you can hopefully find the most relevant bits quickly.

# M4M

M4M provides a command-line interface for generating, running, plotting, and analysing stuff.

It has 5 assemblies, though only only 4 matter, with different .NET targets.

 - M4M.Model (.NET Standard 2.0): includes the underlying model (`DenseGenome`, `PopulationExperiment`, Targets, `ExperimentFeedback`, etc.), general purpose data structures (`PriorityQueue`, `DisjointSets`, etc.), `Misc` rubbish, and serialisation stuff. Linear Algebra and Random Numbers are handled through the MathNet.Numerics library. Serialisation (all binary) uses the nightmarish NetState. All this stuff is in the `M4M` namespace, not `M4M.Model`.

 - M4M.Old (.NET Framework 4.6.1): old stuff which depends on the .NET Framework which I haven't looked at in years. Ignore it. Actually, I probably deleted it at some point, so don't be shocked if you can't find it.

 - M4M.New (.NET Standard 2.0): new stuff which works. Much of this, too, should be abandoned. Includes the `Epistatics.Ivmc*` stuff, `Cli.*` stuff, experiment setup stuff, and plotting stuff (plotting provided by OxyPlot). Basically, anything which has any interface to the user.

 - M4M.CoreRunner (.NET Core 2.1): the .NET Core runner. (Works with newer versions of .NET Core also, but you'll need to update the scripts accordingly; NOTE: .NET Core 2.1 is EOL in 2021/10; not switched to a new one yet by default due to lazyness and poor CI support)

 - M4M (.NET Framework 4.6.1): the .NET Framework runner. And lots of stuff which consumes M4M.Old but we don't care about.

## Getting up and running

If you can stuff all this stuff in a directory and download a copy of the .NET Framework or [.NET Core](https://dotnet.microsoft.com/download), then you are off to a good start:

Life will be easiest if:

 - You are on windows, have Visual Studio installed, and can load up the developer command prompt
 - You have the .NET Core SDK 2.1 installed
 - You have a recent version of powershell (pwsh) installed

Note that while M4M can be run on linux without powershell, the scripts that generate the experiments are powershell scripts.

### M4M Stuff:

For .NET Core (>= 2.1), .NET 5 (hereon lumped in with .NET Core)

 - M4M.CoreRunner.dll
 - M4M.Model.dll
 - M4M.New.dll

For .NET Framework (>= 4.5?)

 - M4M.exe
 - M4M.Old.dll
 - M4M.Model.dll
 - M4M.New.dll

### Dependencies:

Dependencies are listed for reference: you shouldn't need to take any action to obtain then.

Excepting NetState.dll, which should have been supplied with M4M, these packages should be obtained automatically by nuget upon build (requires an internet connection).

 - NetState.dll - provides serialisation (essential)
 - [MathNet.Numerics.dll](https://numerics.mathdotnet.com/) - provides mathsy stuff (essential)
 - [OxyPlot.dll](https://github.com/oxyplot/oxyplot/) 2.0 - provides Pdf plotting capabilities (essential): newer versions have replaced the old PDF provision, so don't use those
 - System.Runtime.CompilerServices.Unsafe.dll - something important (essential)
 - SixLabors.Core.dll - [SixLabors](https://sixlabors.com/) stuff is only used for bitmap rendering (e.g. the bestiary); there is some licencing weirdness going on, but using an old version (e.g. 1.0.0-beta0007) for non-commercial work should be fine
 - SixLabors.Fonts.dll
 - SixLabors.ImageSharp.dll
 - SixLabors.ImageSharp.Drawing.dll
 - SixLabors.Shapes.dll

.NET Framework stuff also requires:

 - [OxyPlot.Pdf.dll](https://github.com/oxyplot/oxyplot/) - provides the nice Unicode plotting (essential: there is no fall-back to the inferior Pdf exporter)
 - [PdfSharp.dll](http://www.pdfsharp.net/) - provides the nice Unicode plotting (essential: there is no fall-back to the inferior Pdf exporter)

You can use the MathNet.Numerics OpenBLAS provider if it is available.

Contact the maintainer if you have any difficulties.

The PDF plotting should be replaced with the shiny new Skia based PDF exporter at some point.

#### .NET Core

.NET Core can be compiled for various platforms (M4M has been employed in practise on Linux (RHEL7) and Windows (7, 10)), and supports 'download and unzip' deployment. If you want to do build from source or otherwise do development, then you need the SDK, otherwise you can get away with just the runtime. You can find the installers for 2.1 (which is what everything M4M targets) here: https://dotnet.microsoft.com/download/dotnet-core/2.1. Iridis unfortunately doesn't have 2.1 (at the time of writing), so you'll have get it manually if you are working there or re/dual-target everything to 3.0 (it has been tested against 3.0, 3.1, and .NET 5.0, so don't be afraid of doing that.)

I can't remember what installing does, but if you are using a compute cluster or an otherwise locked down system, life is no harder than downloading and unpacking an archive and added the path to `dotnet` executable to your PATH or creating an alias. If you intend to do C# development, then it is strongly advisable that you add `dotnet` to your PATH.

#### .NET Framework

The .NET framework ships with Windows, so is probably already installed. If not, you will need to install it. If you can't install things or are using Linux, you'd better look at using .NET Core instead.

#### Powershell

If you are using Windows, you probably have powershell installed already.

If you don't have powershell but do have a recent version of .NET Core, you can install it by running

    dotnet tool install --global PowerShell

Then you can use `pwsh` to run it (works on both linux and windows).

### Building everything

Building is easy with .NET Core; it's a bit more painful with .NET Framework because msbuild and friends usually isn't in PATH.

If you have .NET Core and powershell, run `. m4minit.ps1` from powershell (pwsh) then you need only run this command:

    m4mbuild

If you run it from the Visual Studio Developer Powershell, then it should build all the windows specific utilities also (I'm too lazy to port them to .NET Core, so they need .NET Framework). You can pass `m4minit.ps1` an alternative .NET version to use, e.g. `. m4minit.ps1 net5` (one of `netcoreapp2.1` (the default for legacy reasons), `netcoreapp3.1`, `net5`).

If you don't want all the utility stuff, you can run:

    netstateb
    m4mb

If you are on linux, you can run `. m4minit.sh` and hopefully it will do the same sort of thing (again, you can specify a .NET version as a parameter). Again, run

    netstateb
    m4mb

To build the things you need. These scripts will also add `m4m` as an alias. If running `m4m` after running the build scripts doesn't error, then you are good to go.

If you don't want to use the build scripts, or they don't work, or you want to use the non-core utilites, then read on...

For reasons not worth going into, M4M ships with its own serialisation framework (netstate), and you need to build that before anything else (e.g. with visual studio or msbuild or a `dotnet` command as described below). Netstate starts .NET Standard, so it doesn't matter how you build it, and you only need to build it once.

the easiest thing to do is to open the solutions in (an up-to-date) Visual Studio and build everything from within the Batch Build menu, or call `msbuild /p:Configuration=Release` on the runner and it should work everything out. If you don't have MSBuild (e.g. you are on linux) or don't know what msbuild is, then you can just as well use `dotnet build --configuration Release`. If you want the Core-runner, for example, open a terminal in the directory `M4M.CoreRunner` (the one containing `M4M.CoreRunner.csproj`) and run `dotnet build`: it will build the M4M dependencies for you.

Things that may go wrong include:
 - Some warning about info files or `AutoVersion` or something: check that `AutoVersion.dll` and its runtime config are in the build tools directory. If they are not, it isn't a big problem, but you could always ask me about it.
 - Some complaint about not being able to reference net standard 2.0: delete all (ALL) the bin and obj directories, and try again.
 - Some complaint about netstate/state attributes: you need to build netstate before anything else

The utilities are all written for .NET Framework for Windows, and need msbuild to be built, e.g. `msbuild M4MPlotting.sln /p:Configuration=Release`. This should work if you run it from a Visual Studio Developer Prompty (windows search for 'Developer Prompt' will find it if it is available). (If you want them to run on .NET Core under windows, shout at me, and I'll sort it out. If you want M4MPlot to run under .NET Core on linux, shout at me and maybe I'll sort it out; the others I will not be re-writing.)

### Running it

You run the .NET Core runner from the command line with the command `dotnet M4M.CoreRunner.dll`.

    ~~ M4M Core Running ~~
    OpenBLAS
    Available providers:
    // long list

You run the .NET Framework runner from the command line as `m4m.exe`.

    ~~ M4M Framework Running ~~
    OpenBLAS
    Available providers:
    // long list

The 'OpenBLAS' line only appears if open-BLAS is detected (whereafter it will be used).

If you don't want the nice intro text, use the 'quiet' commandline parameter.

#### Command-line aliases

In order To make your life easier, it is suggested that you alias `m4m.exe` or `dotnet M4M.CoreRunner.dll` to just `m4m`. In any help documentation, `m4m` is used in this sense. Aliasing works differently in different shells, for example:

    // cmd
    doskey m4m = path\to\m4m.exe $*

    // PowerShell
    function m4m() { dotnet "path\to\M4M.CoreRunner.dll" $args }

    // bash
    alias m4m='~/path/to/dotnet/dotnet path/to/M4M.CoreRunner.dll'

Recall that forward-slashes work as separates on both modern versions of Windows and Linux. There is no reason this software shouldn't work on OSX with .NET Core, but I've never tried. m4m.exe might also work under mono, but again, I haven't tried.

You can make life easy by using powershell and loading the `m4minit.ps1` script, which provides a handful of useful aliases.

#### VS Code and Windows Terminal

If you happen to be using VS Code with powershell as the terminal, you can configure the terminal by adding the following to your settings.json (e.g. just for the workspace where you want it):

    "terminal.integrated.shellArgs.windows": ["-NoExit", "-command" ,".", "\"./m4minit.ps1\""],

This tells it to run m4minit.ps1 on startup, and then to keep that instance alive. You'll need to update the path to m4minit.ps1 (in my case, I have it enabled only in the M4M_MKI directory, so m4minit.ps1 is in the same directly).

Much the same can be done for the windows terminal. Create a new config block in settings.json, and swap out the startup command:

    "commandline": "powershell -NoExit -command \". C:/Users/Murdo/Documents/raw/Code/Other/M4M_MkI/m4minit.ps1\"",

Change the font to something sensible as well, because the default font in Windows Terminal is not good.

    "fontFace": "Consolas",

Another convenience I use is to add an environment variable for the address of m4minit.ps1, so that I can run it from anywhere in a hurry. This is especially useful on the compute server, when I need to swap between pwsh and bash at short notice.

### Cli Parameters

The M4M command line is controlled by various command-line parameters. There are no positional parameters.

There are 2 types of parameters:

 - Flags, which are just case-insensitive keywords that are left on their own. Examples include `quiet` (less pointless output), `diag` (diagnostic output), `help` (print help text sometimes), and `green` (plots `rcs` files in green mode instead of rainbow mode).

 - The other type, which are key-value pairs. You can set these either with "key=value" (no spaces), or "-key value" (one space). Examples include `plot=rcs_t(best).dat`, `legend=to`, `targetpackage=custom`, `dtminfo=true`, and many others. Keys are all case-insensitive, and most (non-filename) values are case-insensitive. In code, flags are just key-value pairs with a null value.

Generally, if a parameter requires a list of things, then this list is semi-colon delimitated (`;`), because it turns out that commas (`,`) are a bad choice. Unfortunately, many places which accept semi-colon also recognise comma. If you have difficulties inputing numbers containing commas, then you may consider the `forceinvarientculture` (or `fic`) flag to use US-style numerals. One issue with semi-colons is that some shells (Powershell) recognise these as end-of-lines, so you have to wrap lists in quotes, or escape the semi-colons with a back-tick.

You can append to already assigned parameters with "+key value" or "key+=value". If the parameter is unassigned or null, then it will be assigned the given value.

Some parameters are keyed, such that they can be set for individual items, where the name has the format `param:key`. These occur regularly in plotting, where customising individual axes is useful (e.g. `title:x=Generations`). In Code, these are no different from other parameters, and there is no real support for them, but it's a nice convention.

As a simple piece of assistance, you will be informed of any Cli Parameters which are not observed at the end of execution. If a parameter is not observed, it may warrant investigation: for example, it might indicate a typo. This utility is disabled by the `quiet` and `donotlistunobserved` flags.

After spending any time doing electronics, numerical formats like `3k3` become irresistible, so there is basic support for this (and exponential) format on many integer parameters. E.g. all of these are equivalent:

    epochs=1500
    epochs=1K5
    epochs=1.5
    epochs=1.5E3

#### Special Parameters

There are two types of special parameters, which allow you to 'include' parameters from another source. This is useful when you need to share parameters between commands, or just want to logically group them.

 - `params`: inline parameters
 - `paramfile`: file parameters

##### params

All parameters with key `params` will be expanded immediately into more params: the following two commands are equivalent:

    m4m -exp epoch0savestart.dat plotperiod=1000 saveperiod=1000 saveperiodepochs=1000
    m4m -exp epoch0savestart.dat params="plotperiod=1000 saveperiod=1000 saveperiodepochs=1000"

This is performed as the parameters are consumed (in code, by the `CliParams.Consume*` methods): it can be used anywhere, and the usual overwriting rules apply. Importantly, you can use as many `params` commands as you want, so you can include many sources.

##### paramfile

After consuming all parameters from some source, _some_ tools will look up the `paramfile` parameter and, if it exists, consume every line in the give file (or semi-colon delimited files; back-tick for escape). This operation is recursive, but will ignore any paramfile that is observed twice. Parameters from a paramfile are ignored if they are set by whatever source reference the paramfile. Because this is really just a normal parameter (unlike `params`) but with important behaviour, you cannot use it more than once in a command.

Any line in a paramfile that starts with `//` is ignored. Lines later in a file will replace parameters set by lines earlier in the same paramfile.

In code, this is provided by the `M4M.Cli.LoadParamFiles` static method.

### Differences between .NET Core and .NET Framework Versions

The only real different is that .NET Core one is used for grunt work, and the .NET Framework is used alongside M4MPlotting.exe, and the Pdf engine that has unicode support (which the .NET Core version does not). They should otherwise provide the same command-line experience.

## Files

Data files all end in `.dat`. Anything which is not `.dat` is not a data file. Most important of all, `config.txt` is not a datafile, so don't bother editing it ever (it provides a summary of what is in an experiment, so that you can check it is running what you expect it to be running: don't edit it). `config.dat` is a data file, but you can't edit it. Look at the `reconfig` provider if you want to know how to 'edit' things (hint: you can't).

Let me repeat: you cannot edit data-files. The serialisation format is not designed to permit such editing, and any attempts to do so are liable to corrupt data files, or otherwise leave you with invalid state. Also, it might just be dishonest, but let's not worry about that. Do not edit data files.

### File Prefixes

In order to make life worse, I mean better, I mean weird, file-names all have a meaningful prefix. Do not change the prefix of your data-files, as the program won't know what to do with them. For example

    // this is an experiment definition, as indicated by the `epoch` prefix
    epoch1000save.dat

    // this file contains Regulatory Connection Strength trajectories (prefix `rcs`)
    rcs1000000(best).dat

    // this file contains end-of-epoch Whole Samples (prefix `wholesamplese`)
    wholesamplese2000000.dat

    // this file contains a single Dense Genome (prefix `genome`)
    genome5000.dat

### File Postfixes

Some files end in "_t": this means they are 'terminals', generated at the end of an experiment, which - in the case of trajectory files - contain all samples recorded through the whole experiment. Partial results will have an end-of-epoch number instead. Wholesamplese files are special, because they tend not to start from epoch 0 every time.

### File Formats

There are 2 main file formats, both exclusively Big-Endian with UTF-8 string encoding:

 - NetState Graph: this is an automatically serialised version of some actual .NET Classes. Don't even think about editing these. In code, these are loaded with the static `M4M.State.GraphSerialisation.Read<T>(Stream)` method, whre `T` is the expected type of object being read. The exact nature of these files depends on the runtime, but some sickening code in `NetState.dll` performs some automatic mapping which means they should be completely interopable.

    Though in theory these could be loaded from another software package, the effort would be considerable, and if that were necessary it would probably be easier to load them in .NET first, and then use some dodgy JSON serialiser or something to re-export them.
    
    The benefits of this format are in rapid prototyping and (reasonably) efficient storage. Take a look at the State directory under M4M.Model if you want to understand how it is used. This is the `M4M.State` namespace, and the layer that interfaces with the netstate library. Unless you are working with structures, the `StateClass` and `SimpleStateProperty` annotations are all you need to to know.

 - Trajectories: this is just a number of long lists of double-precision floating point numbers. Unless you are very unlucky, the file will also tell you the sample-period with which the data was sampled. This is used as an efficient way to pack in lots of data samples that are taken during the course of an experiment. In theory you could edit these... but why would you want to mess with your output data? Examples include `rcs` files, `fitness` files, `ist` files, `pst` files, `ivmcswitches` files, and `ivmcproper` files. In code, these are loaded with the static `M4M.Analysis.LoadTrajectories` method, and give you a `double[][]`, with the structure `trajectories[line][time]`

    The format is very simple, so it should be trivial to load them from another software package if necessary.

Some files are just weird. `genome` files are one such example, as I recall. The number of weird files should not increase over time.

## Key Structures

### Experiment

In code, an experiment is a `PopulationExperiment<TIndividual>`, and is serialised as such to an `epoch` file, such as `epoch0savestarter.dat`. An experiment contains all the information to run an experiment, except for the PRNG seed, and information concerning 'feedback'. Population Experiment information includes:

 - the current epoch and generation counts
 - the current target index
 - the current population, a `Population<TIndividual>`
 - the population experiment configuration, a `PopulationExperimentConfig<TIndividual>`
 - some information concerning where the experiment was created/might be run, which makes no sense but is there anyway

### Population

A population (`Population<TIndividual>`) just contains a list of individuals of type `TIndividual`, and provides various methods to mess with them.

Populations and Individuals share the strange idea of 'escaped' individuals: for not-very-good reasons, individuals remember whether they have escaped the control of the population so that they know they can't be 'reycled'. This 'recycling' in theory allows object reuse, but in practise is a nightmare you should never enable. Instead, consider writing/requesting a fast-path which uses mutable individuals where performance is a problem.

### Population Experiment Configuration

A `PopulationExperimentConfig<TIndividual>` contains information about an experiment on a population. This information includes:

 - the experiment configuration 'config', an `ExperimentConfiguration` (note this isn't generic)
 - the population spinner, which manages the cycling of the population each generation
 - the "PopulationResetOperation", which is run at the start of each target exposure
 - the "PopulationEndTargetOperation", which processes the population at the end of each target exposure

Some population spinners (e.g. the default spinner and `DeltaRuleSpinner`) recognise the following information in the population experiment configuration:

 - the elite count, the number of elites to keep each generation
 - hill-climber mode flag, which enables a 'hill-climber' rather than weighted population replacement
 - the selector preparer, an object which prepares a selector for fitness-dependent selection

### Experiment Configuration

An `ExperimentConfiguration` contains lots of information about an experiment. Crucially, it includes:

 - the number of epochs to run
 - the number of generations per target per epoch `K`
 - the development rules `drules`
 - the reproduction rules `rrules`
 - the judgement rules `jrules`
 - the list of targets
 - the target-cycler

It also includes:

 - the 'InitialStateResetProbability', the probabiliy of reseting stuff at the start of each target exposure (not necessarily just initial states)
 - the 'InitialStateResetRange', which determines the range in which initial states should be reset when applicable

### Individuals

These include a genome and a phenotype. The genome is generic; the phenotype is not. `DenseIndividual` is the only type of individual you should ever care about, and comprises a `DenseGenome`, the developed `Phenotype`, and some other stuff you probably don't want to (and shouldn't have to) think about.

### Whole Sample

A `WholeSample<TIndividual>` holds a substancial amount of information about a single generation or epoch:

 - the epoch and generation it represents
 - a list of individual judgements (i.e. individual and judgement pairs)
 - the current target (this is not a clone, so it will become 'out of date' if the target is ever reset (e.g. noise is added, or it's a Variable IVMC Target))

The `wholesamplese` and `tracee` files are literally a `List<WholeSample<TIndividual>>`, which is only somewhat regrettable. `wholesamplese` files usually represent end-of-epoch samples, while `tracee` files usually represent within-epoch samples. They are usually stuffed into a `TraceInfo` as soon as possible.

## Providers

Here is a run-down on the different provides. Most provides are also used as parameters, e.g. to use the `plot` provider you provide a `plot=something` parameter, where `something` is a data-file or the word 'misc'. The cli just uses whichever provider it happens spot has a parameter associated with it first.

The most essential ones are:

 - `gen`, which generates experiments
 - `exp` and `run`, which run experiments
 - `plot`, which plots data-files

### `exp`

Runs a `PopulationExperiment` packaged in an  `epoch` file. It will provide an indication of progress every `0.1%`, indicating the current epoch and an estimate of the remaining runtime. 

 - `topdir=directory` specifies the directory to run in (note: topdir is a highly context-dependent parameter)
 - `continue`: attempts to continue an 'unfinished' experiment, simply subtracts the observed epoch count from the total epoch count to run

Basic Examples:

    // runs the indicated experiment in the same directory as the experiment file, using the given random seed
    m4m exp=ImvcExperiment1/epoch0savestarter.dat seed=42

    // runs the indicated experiment in the indicated directory (note: topdir is a highly context-dependent parameter)
    m4m exp=ImvcExperiment1/epoch0savestarter.dat topdir=ImvcExperiment1runOne

    // continues the indicated experiment (not that random seeds are not serialised, so a new seed is generated/must be supplied when you 'continue'
    m4m exp=ImvcExperiment1/epoch500save.dat continue

#### Automatic Saving

Because `m4m` is _really slow_, part-way experiment 'save' files are regularly written out. Note that these do not contain random seed information, so 'continuing' an experiment is a bit of a lie, because the seed will be different.

 - `saveperiodepochs=epochs`: changes how often an `epoch` save file is produced (period in epochs)
 - `saveperiodseconds=seconds`: changes how often an `epoch` save file is produced in seconds (e.g. even if it ask it to wait for 1billion epochs, it will still spit out a file wherever it has got to if the number of seconds since the last file has exceeded this parameter)

#### Default Feedback

Vitally important to running experiments is to get something out from them. By default, the default 'feedback' provider will produce an endless supply data files. Indeed, the amount of data-files is a linear function of the duration... and the data-files are mostly cumulative... so the amount of space consumed is quadratic in the duration. Basic controls of the amount of data files include:

 - `plotPeriod=epochs`: controls how often preliminary/preview data files are produced (mostly trajectory files such as `rcs`, `fitness`, etc.)
 - `savePeriod=epochs`: controls how often a `genome` file is produced (period in epochs)
 - `sampleMax=count`: controls the maximum number of samples that will appear in the default trajectory files, default is 2000
 - `tracePeriod=epochs`: controls the interval between whole-epoch traces
 - `traceDuration=epochs`: controls how long each trace is in epochs
 - `wholeSamplePeriod=epochs`: controls how often a 'WholeSample' is taken
 - `wholeSampleWritePeriod=epochs`: controls how often wholesamples are written out (these file can be big, so do not accumulate)

Other than the regular preliminary data-files (which has a numerical postfix indicating the epoch whence they come), there are terminal data-files produced when the experiment ends with postfix `_t`. Usually you will want to delete everything except the `_t` files, since they contain all of the same information, and disk-space is at a premium when you are running a hundred of these at a time.

#### Feedback Configurators

Other than the default feedback - from which you can't escape without messing with the code - there is support for independent 'feedback configurators'.

You can specify configurators as a semi-colon (`;`) delimited list, for example:

    m4m exp=epoch0savestarter.dat configurators="ivmcdensefeedback;satfitnessfeedback"

Remember to put quotes around your semi-colon list if using PowerShell.

##### IvmcDenseFeedback

The `ivmcdensefeedback` configurator produces trajectory files counting ivmc-switch events, ivmc-solves, and (optionally) the actual ivmc-benefit environmental factors during each epoch.

The `ivmcdensefeedback` configurator has the following parameters:

 - `ivmcSwitchResolution=epochs`: controls the interval between IvmcSwitch samples (note: every switch in every is counted, but with `
 - `ivmcSwitchResolution>1`, the counts accumulate resulting in a lower-resolution trajectory)
 - `ivmcSwitchPlotperiod=epochs`: controls how often `ivmcswitches` files are written out

If the `ivmcdensefeedback` configurator is successfully attached, you should see the message `IvmcProperFeedback attached`.

##### SatFitnessFeedback

The `satfitnessfeedback` configurator produces trajectory files that contain the fitness of a saturated phenotype. This is useful, for example, if you want to know whether the system is able to find high-fitness patterns, without all the noise of directional selection.

The `satfitnessfeedback` configurator has the following parameters:

 - `SatFitnessSampleRate`: the sample period (defualt is 1, which produces very large files)
 - `SatFitnessPlotPeriod`: the plot period, defualts to whatever `plotperiod` is
 - `SatThreshold`: the threshold that determines whether values are high or low (default 0)
 - `SatMin`: the value to assign to low values (default -1)
 - `SatMax`: the value to assign to high values (default +1)

If the `ivmcdensefeedback` configurator is successfully attached, you should see the message `SatFitnessFeedback attached`.

Internally, this uses a `ProjectedFitnessFeedback` which it passes a `SaturationTarget` that wraps the existing targets (except those that are already instances of `SaturationTarget`, in which case it does not wrap them; don't ask me why).

### `run`

Runs a directory full of directories containing experiments. Most stuff that works with `exp` also works with `run`, though possibility in different ways (e.g. `seed` has a different job, and `topdir` becomes meaningless). Important parameters are:

 - `run=directory`: specifies the directory to run
 - `repeats=count`: the number of repeats to run of each experiment (each is run in its own directory with an `r{repeat}` postfix), default is 1
 - `seed=seed`: specifies the seed of the first experiment: it is incremented for each successive experiment (in file-name order), randomised by default
 - `expfilename=epochfilename`: the exact `epoch` filename to look for in sub-sub-directories.
 - `postfix=postfix`: the 'runs' postfix
 
 #### Example

A 'Real world' derived example, demonstrating many `run` and `exp` parameters.

    m4m run=$1 postfix=$2 traceperiod=0 wholesampleperiod=100 wholesamplewriteperiod=2000000 plotperiod=500000 saveperiod=500000 saveperiodepochs=100000 saveperiodseconds=7200 seed=$3 expfilename=$4 configurators=ivmcdensefeedback ivmcswitchplotperiod=1000000 ivmcswitchresolution=1 ivmcrecordpropers=false > logs/$1runs$2log.txt

It has the following features:
 - takes the run-directory postfix, seed, and experiement file name as a command-line parameter
 - sets a low-ish wholesampleperiod, high wholesamplewrite period (so you get one big data-file at the end instead of having to concat lots of small data files, but you have to wait for it)
 - disables traces (traceperiod=0)
 - sets a large but not too-large plotperiod and saveperiod, so that part-way results can be interrogated
 - sets a smallish saveperiodepochs, so that there are many checkpoints (which are small and can always be put to good use)
 - sets the saveperiodseconds to 7200seconds = 2hours (the whole run took ~30hours)
 - engages the ivmcdensefeedback configurator, with the lowest possible ivmcswitchresolution (corresponding to no down-sample), but disabling ivmcrecordpropers
 - pipes the output to a log file

### `gen`

Generates experiment files, based on a number of 'preparers', each of which has its own set of parameters, and may produce one or many experiment files. The prepare of choice is taken as the argument of the `gen` parameter. If no parameter is provided, a list of providers is printed. At some point I delete all the providers other than `composedense`, but in theory you could add more for e.g. non-dense stuff.

Many providers have the following common parameters:

 - `topdir=directory`: the 'top' directory; this default to some unhelpful place in `C:` under Windows or `~` under Linux, so it's usually worth qualifying
 - `targetpackage=packagenameortype`: specifies the target package, or type of target package. The specifics depends on each preparer.

 Most providers have a somewhat helpful `help` text outlining the parameters, but that's about it. Always check the `config.txt` file to verify everything is as you expect.

 #### Generating Experiments

 The `composedense` preparer produces a single 'Dense' experiment; it is intended to be a half-way extensible means, the backing for which should be the basis of any future providers. One day I might delete all the old provides, because they are just generally a nightmare. Unlike other providers - which are a monolithic nightmare - this provider is a semi-decoupled nightmare spread over multiple files (see the `ExperimentComposition` namespace). The functionality is provided by multiple 'composers', each of which deals with some part of the hierarchy of misery that is a `PopulationExperiment<DenseIndividual>` (they are generic where applicable). The error handling is not so great at the moment, because I made some bad decisions concerning where to put error handling logic.

Sub-components and their parameters:

##### `DefaultExperimentComposer`

Composes an experiment, defering most of the work to other composers passed as a parameter.

 - `seed=int`: the seed to use when generating stuff
 - `name=string`: the name of the experiment; the inner-most directory wherein it will appear
 - `appendTimestamp`: (flag) whether to append a timestamp to the experiment

##### BasicTargetPackageComposer

Redirects the work of building the target package (`ITargetPackage`) to one of many `IGenericPrefixedComposer<ITargetPackage>`.

 - `targetpackage=prefixsuffix`: determines the composer to use (by prefix) passing it the suffix

By default, the following prefxied target package composers are available:

 - `file:`: (recursive) produces a single target package from multiple targets as indicated by each line of a file (each line must contain a `targetpackage=` clause, or you might end up with an infinite loop)
 - `exp:`: uses targets from an existing experiment file
 - `ivmc:`: prepares an Ivmc TargetPackage; see commentary in section _Ivmc Target Packages_
 - `mc:`: MC-problem target packages, based on a correlation matrix

##### BasicExperimentConfigurationComposer

Puts together an `ExperimentConfiguration`, defering the generation of drules, rrules, and jrules to prefixed composers.

 - `epochs`: the number of epochs to run
 - `K`: the number of generations per epoch
 - `gResetProb=[0,1]`: the probability of reseting the initial state vector at the start of each epoch
 - `drules=prefixsuffix`: determines the composer to use for `DevelopmentRules`
 - `rrules=prefixsuffix`: determines the composer to use for `ReproductionRules`
 - `jrules=prefixsuffix`: determines the composer to use for `JudgementRules`

##### DefaultDevelopmentRulesComposer

The default drules composer (usually available to `BasicExperimentConfigurationComposer` as prefix "default").

 - `T=int`: number of developmental time-steps to use, default 10
 - `tau=[0.0, 1.0]`: the decay rate, default 0.2
 - `squash=name`: the squash composer to use, defaults to tahnhalf

##### DefaultReproductionRulesComposer

The default rrules composer (usually available to `BasicExperimentConfigurationComposer` as prefix "default").

 - `MG=magnitude`: G mutation magnitude, default is 2 (along with BGtype=binary and [-1, +1] clamping gives 'binary' G mutation)
 - `MGtype=noisetype`: type of G mutation, default is binary
 - `MB=magnitude`: B mutation magnitude, default is 2E-5
 - `MBtype=noisetype`: type of B mutation, default is uniform
 - `bprob=[0.0, 1.0]`: the probability of performing a mutation of B, default is 1.0
 - `BEx=bool`: controls whether mutations on B should be performed at the exclusion of G, default is false (no)
 - `CG=int`: the number of initial-state traits to update, default is 1

##### DefaultJudgementRulesComposer

The default jrules composer (usually available to `BasicExperimentConfigurationComposer` as prefix "default").

 - `lambda`: the regularisation coeficient
 - `kappa`: the noise-factor to apply to targets
 - `regfunc=prefixsuffic`: the cost function composer and suffix (see `BasicRegularisationFunctionComposer`)

##### BasicRegularisationFunctionComposer

Composes typical regularisation function, and provides for custom prefixed composers. Supported regularisation functions include:

 - `const`: a constant-equivalent (i.e. no regularisation)
 - `l1`: an l1-equivalent (linear), as used in most experiments
 - `rowl1`: an l1-equivalent (linear), which should be identical to `l1`, but is faster
 - `coll1`: an l1-equivalent (inear), which is not identical to `l1`, but is faster still
 - `l2`: an l2-equivalent (quadratic)
 - `mmsoAB{a;b}`: michaelis-menton style regularisor given by `cost = b|x|/(|x|+a)`
 - `mmsoABC{a;b;c}`: michaelis-menton style regularisor given by `cost = b|x|/(|x|+a) + c|x|`
 - `mmsoAQ{a;q}`: michaelis-menton style regularisor given by `cost = b|x|/(|x|+a) + c|x|` with `a=a, b=a*(1-q), c=q`
 - `l1And2ab{a;b}`: sum of linear and quadratic, `cost = a|x| + bx^2`

##### BasicDensePopulationExperimentConfigurationComposer

Puts together a `PopulationExperimentConfig<DenseIndividual>`, defering the generation of a custom population spinner to a prefix composer. Supported parameters include:

 - `resetoperation`: the population reset operation to used; one of: uniform, binary; default depends on the G mutation type
 - `spinner=prefixsuffix`: determines the prefixed composer and suffix of a custom population spinner (which may or may not observe other `PopulationExperimentConfig<T>` parameters), e.g. `dense:hebbian`, `dense:hopfield`, or `dense:deltarule`
 - `hc=bool`: whether to use hill-climber mode where applicable
 - `elitecount=int`: the number of elites to keep each generation where applicable
 - `selectorpreparer`: the selector prepare to use, one of: `ranked`, `pareto`, `proportional` (default is `ranked`)

Presently supported but ill-advised parameters include:

 - `gResetBinary`: controls whether g resets should be binary (values sampled from {-1, 1}) or uniform (values sampled uniformally from [-1, +1]); prefer `resetoperation` instead which overrides this

##### DefaultPopulationSpinnerComposer

Puts together a population spinner (usually available to `BasicDensePopulationExperimentConfigurationComposer` as prefix "default").

 - `default`: uses the `DefaultPopulationSpinner<TIndividual>`, which simply calls into `Population.SpinPopulation`. Provides a simple population-cycle based on the population-configuration hill-climber mode (on/off), elite-count, and selector-preparer. Implements a fast-path of hill-climbers; otherwise, pretty inefficient. No parallelism is employed for a variety of reasons.
 - `paired`: uses the `PairedPopulationSpinner`, which simply calls into `Population.SpinPopulationPaired`. Ignores hill-climber mode, elite-count, and selector-preparer. Selects 2 individuals at random (no weighting), and replaces the less-fit individual with a mutant of the more-fit individual.
 - `trios`: uses the `RecombinantTriosPopulationSpinner`, which simply calls into `Population.SpinPopulationRecombinantTrios`. Ignores hill-climber mode, elite-count, and selector-preparer. Selects 3 individuals at random (no weighting), and replaces the less-fit individual with a combination of the more-fit individuals. _I'm pretty sure this has never been run, let alone tested._

##### DensePopulationSpinnerComposer

Puts together a population spinner for a dense individuals (usually available to `BasicDensePopulationExperimentConfigurationComposer` as prefix "dense").

 - `delta`: uses the `DeltaRuleSpinner`, which calls into `Population.SpinPopulation` before performing a delta-rule update step once the usual stuff has ended. Generally you want to set `bprob=0` so that the dtm is only modified by the delta-rule update step. An additional parameter is available:
   - `deltarulefitnesspowerfactor=double`: a factor to which the fitness of the individual generated by the default spinner will be raised to create a factor by which the delta-nudges are multiplied after evalution (a crude form of fitness informed update)
 - `hebbian`: wraps an 'inner spinner', running a hebbian update step at the end, updating `B` a little according to the auto-correlations in `G`. It has many parameters, including:
   - `hebbiantype=hebbiantype`: controls the types of updates performed by the hebbian update. One of `Standard`, `IncreaseOnly`, and `DecreaseOnly` (default is `Standard`)
   - `innerSpinner`: another `dense` type spinner (e.g. combine with `lotkavolterra` to get close to _What can ecosystems learn? Expanding evolutionary ecology with learning theory_ by Power et al.)
 - `hopfield`: modifies `G` is a hopfield network... so it's a bit like splating the developmental process over many 'generations'. It has many parameters, including
 - `lotkavolterra`: treat `G` as population densities with interacts according to `B` and the Lotka-Volterra competition dynamics. Additional parameter:
   - `growthrate` the growth rate

##### BasicDensePopulationComposer

Puts together a `Population<DenseIndividual>`

 - `g0string`: extreme value or semi-colon delimited double list of initial state expressions for the starting genome; default is all-zeros
 - `b0string`: a matrix string; currently only supports the `col:c0;..` format, whereby the value of whole columns is provided in a semi-colon delimited double list; default is all-zeros
 - `transmatmutator=prefixsuffix`: determines the developmental transformation matrix mutator, one of `default`, `singlecell`, `columns{moduleCount}`, `singlecolumn{moduleCount}`
 - `popSize=int`: the number of clones in the population, default `1`

#### Generating Ivmc Experiments (Legacy)

 The `properivmc` preparer produces a single 'Ivmc' experiment. It has the following important parameters (some of which are congruent with other generators), many of which do not have defaults:

 - `epochs`: the number of epochs to run
 - `K`: the number of generations per epoch
 - `lambda`: the regularisation coeficient
 - `targetpackage`: the type of target package to use, either `4` (the default, a 4x4 ChangingProeprIvmc) or `custom`
 - `regfunc`: the cost function, such as `L1`, `L2`, `MmsoLinAQ1,1` (alterntaively, specify `q` and `a` as parameters of an MMSOLin cost function without qualify `regfunc`)
 - `MG=magnitude`: G mutation magnitude, default is 2 (along with BGtype=binary and [-1, +1] clamping gives 'binary' G mutation)
 - `MGtype=noisetype`: type of G mutation, default is binary
 - `MB=magnitude`: B mutation magnitude, default is 2E-5
 - `MBtype=noisetype`: type of B mutation, default is uniform
 - `bprob=[0.0, 1.0]`: the probability of performing a mutation of B, default is 1.0
 - `BEx=bool`: controls whether mutations on B should be performed at the exclusion of G, default is false (no)
 - `gResetProb=[0,1]`: the probability of reseting the initial state vector at the start of each epoch
 - `gResetBinary`: controls whether g resets should be binary (values sampled from {-1, 1}) or uniform (values sampled uniformally from [-1, +1])

##### Ivmc Target Packages

The target package usually contains a single `IvmcProperChanging` Vector Target, which has the following parameters:

 - `CH`: the 'high' benefit coefficient, default 1.0
 - `CL`: the 'low' benefit coefficient, default 0.7
 - `C0`: the 'zero' benefit coefficient, default 0.0 (recomment -1.0 for Split)
 - `Z`: the multi-peak module condition probability
 - `judgemode`: the judgement mode (aka. eta), either 0 (quadratic `Proper`, or piecewise-linear `Split`, or `Stacked` with a `modulebenefitfunction` (e.g. `split`)

Custom target packages have the following parameters, with the equivalent of the `4` values indicated in brackets

 - `targetstring`: the target string as a sequence of + and - symbols (`4`: `++++++++++++++++`)
 - `modulesstring`: a string describing the modules in the environment, one of two formats (`4`: `4*4` or `aaaabbbbccccdddd`)
 - `variationmodulesstring`: a string describing the modules of variation (in terms of modules; `4`: `null` or `4*1` or `abcd`)
 - `variationModulePlusOverridesString (ivmcvmpos)`: a string describing 'positive' benefit coefficients
 - `variationModuleMinusOverridesString (ivmcvmmos)`: a string describing 'negative' benefit coefficients

You can produce multiple targets by using pipe (`|`) delimited variation override strings. If you do so, then you need to folly qualify both plus and minus overrides for every target. The format of these can be either a mix of `h`, `l`, `0`, and `_` (corresponding to CH, CL, C0, and unoverrided), or a semi-colon (`;`) delimited list of doubles and `_` (corresponds to `NaN`).

### `plot`

There isn't much point in having data if you can't visualise it. Because all file-names are prefixed with the type of file they are, plotting is mostly a case of `plot=filename`: the `CliPlot` provider maintains a list of `ICliPlotter`s which associate with a particular prefix, and directs input directly to them (though common post-plot processing is performed by `CliPlot` itself, e.g. legend placement).

Another vital parameter is `out=filename`, which specifies the output filename (otherwise the input filename is used). The output file name has `.pdf` appended, so do not include it yourself.

See the section on utility tools to find out how you can use this same functionality without touching a command line.
 
The handling of some files is common, so the next section is broken down by type of file rather, with additional commentary per-prefix where necessary.

#### Trajectory-type files

In code, these are all handled by a `CliTrajectoryPlotter` configured with different colours and other boring chart information (default title, axes labels, etc.). Colours are not generally user-configurable, because that would be a nightmare.

File-prefixes which employ a `CliTrajectoryPlotter` are:

 - `rcs`, Regulatory Connection Strength trajectories (rainbow colours or `green`)
 - `ist`, Initial State Trajectories (purple)
 - `pst`, Phenotypic State Trajectories (blue)
 - `fitness`, End-of-epoch fitness trajectories (f/b/c terms) (red/orange/yellow)
 - `ivmcswitches`, Ivmc Modules Switchcounts (pink). The last trajectory is the sum of the others
 - `ivmcproper`, Ivmc Proper-Module Configurations (brown)

Common parameters include:

 - `start=epoch`: the epoch from which to start processing & plotting
 - `end=epoch`: the epoch from which to start processing & plotting
 - `resolution=interval`: the post-processed plotting resolution (`resolution=1` is the default which plots everything)
 - `maRadius=windowradius` computes a moving average over each trajectory
 - `meanDownsample=width`: performs a downsampling of each trajectory, replacing many data-points with a single data-point that is the mean (performed after a moving average)
 - `keep=intlist`: an intlist delimited list of indicies into the trajectory file to keep, discarding all others (by default, all are kept)
 - `plottype=type`: chooses between `line` and `scatter` as the plot type
 - `ymin`: the min value to show on the y-axis
 - `ymax`: the max value to show on the y-axis

You can also specify a `samplePeriod` if the data-files you are using happen to lack this information; there is no other good reason for doing this.

##### `rcs`

`rcs` files have some special features. Rather than plotting trajectories, you can plot the genome Developmental Transformation Matrix (a.k.a. `DTM` a.k.a. `transMat`) for a particular epoch by adding the `epoch=epoch` parameter.

Also supported are 2 `rcs` plot annotators:

 - `dtmInfo=bool`, which adds fall-lines onto the plot indicated the types of modules detected during periods of stability
 - `huskyInfo=bool`, which plots the huskyness, and takes a `completeThreshold=[0,1]` parameter which it uses to provide a fall-line indicated when complete huskyness has been achieved (all modules have huskyness > completeThreshold; the exact epoch will also be printed to the console). This method will guess at the environmental module configuration, but it can otherwise be specified via `modulesize` or `modulesstring` parameters.

For example:

    // plots the dtm at epoch 1000
    m4m plot=rcs_t(best).dat epoch=1000 forceaspect out=g1000

    // plots lots of information on an rcs plot
    m4m plot=rsc_t(best).dat dtminfo=true huskyinfo=true completethresdhold=0.9 modulesize=4 out=rscinfo

#### `epoch`

Population Experiments (`epoch` files) have 3 plot modes:

 - `genome`: plots as a genome (see below)
 - `ma`: performs a Mutant Analysis, plotting a histogram of the frequency of different changes in fitness.
 - `target=targetname`: plots a target vector, qualified by name. `moduleCount` can be qualified to change the number of rows in the resulting plot.

The following parameters apply to the Mutant Analysis `ma` mode:

 - `mutantcount=count`: the sample size
 - `target=targetname`: the target to judge the mutants by
 - `seed=seed`: the seed
 - `bincount=count`: the number of bins on the histogram
 - `resetTarget=bool`: whether to reset the target (calling `NextExposure`) before performing the Mutant Analysis
 - `min`: the minimum fitness change to bin
 - `max`: the maximum fitness change to bin

#### Genome

Genomes can be plotted from trace-info files (if a `generation` is specified), `epoch` (if `genome` is passed), and `genome` files. By default, the dtm will be plotted (which can also be achieved with an `rcs` by specifying an `epoch`), but there are four genome render modes in total:

 - `dtm`: plots the Developmental Transformation matrix
 - `is`: plots the Initial State vector
 - `ps`: plots the Phenotypic State vector (requires an experiment config)
 - `dev`: plots developmental trajectories (requires an experiment config), can select which traits are drawn with `keep=intlist`

`epoch` files contain an experiment configuration already, which is used by default if needed, but an alternative can be specified with the `expfile` or `configfile` parameter as it must be to use the `ds` or `dev` modes with files which do not provide an experiment configuration already.

#### Trace Info

_Not to be confused with traces_

Such files includes `wholesamplese` and `tracee` prefix files, and are basically just a list of whole-samples. Such files may either operate on an `epoch`s timescale (e.g. `wholesamplese`) or a `generations` timescale (e.g. `tracee`).

Because these files contain so much information, they allow you to easily plot multiple things on the same plot, each controlled by a `PlotOptions` parameter, which indicates the vertical axis (left/right) and line stype (solid/dashed).

 - `fitness=plotoptions`: Fitness (red), default is right-axis (enabled)
 - `regcoefs=plotoptions`: Regulatory Connection Strenghts (dtm matrix, green), default is off
 - `gtraits=plotoptions`: Initial Trait expressions (purple), default is off
 - `ptraits=plotoptions`: Phenotypic Trait expressions (blue), default is off
 - `huskyness=plotoptions`: Huskyness (grey), default is off
 - `devtimedominance`: Developmental-time Dominance, default is off

Other parameters include:

 - `start=epoch/generation`: the epoch/generation from which to start processing & plotting
 - `end=epoch/generation`: the epoch/generation from which to start processing & plotting
 - `retime=epoch/generation`: shifts the temporal dimensions so that the data starts at the given epoch/generation
 - `resolution=width`: which performs mean-downsampling (what is normally `meandownsample`)
 - `downsample=width`: which performs skip-downsample (what is normally `resolution`)
 - `targets=list`: the seim-colon (';') delimitated list of targets to show on keep (by default it keeps only the 0th target)
 - `targetlines=something`: adds fall-lines indicating target changes, one of `line`, `labels`, or null. Note that this does not show changes in exposure, only changes in the actual target object, so it's pretty useless with `IvmcProperChanging`, sadly.
 - `diff`: enables plotting of the change in parameters from the start of the processed data rather than the true values
 - `fitnessmin`: the minimum on the fitness axis (if any)
 - `fitnessmax`: the maximum on the fitness axis (if any)
 - `ymin`: the minimum on non-fitness y-axes (if any)
 - `ymax`: the maximum on non-fitness y-axes (if any)

If `huskyness` is enables, the 'true' modules will be guessed unless a `modulesize` or `modulesstring` are provided.

If `devtimedominance` is enables, an experiment config must be provided through one of the `expfile` or `configfile` parameters, which may be either a `config` file containg suitable developmental rues, or an `epoch` file containing a suitable experimental configation. A `seed` can be provided as a parameter.

You can specify a `generation` to plot the genome from that generation instead of the trajectories (see section `Genome` for further information). If an experiment config is required, it should be supplied as described above, via the `configfile` or `expfile` parameter.

##### Tracee Plotting Providers

As an alternative to the aforemented `PlotOption` based series, you can use various whole-sample providers. This is a more flexible system, which gives you greater control (useful for multi-plots) and is less of a total nightmare in Code: if you want to add more ploting options for wholesamples, then add more providers. This code doesn't support anything but an 'average' metric and downsampling resolution, though it wouldn't be hard to extend it to do so.

The basic usage is to specify the `providers` parameter with a list of provider/name pairs, e.g.

    providers=fitness:f;gtraits:g;gsum:gsum

The providers can then be customised by name. In code, most go through the `Tracee1DProvider<T>.PlotStuffs` (even the 2D and 0D ones) method, and so support the following keyed parmeters:

 - `plottype:name`: the plot type, either `line` or `scatter`, or `rectangles` for 1D-things
 - `xaxiskey:name`: the x-axis key
 - `yaxiskey:name`: the y-axis key
 - `colour:name`: the base colour
 - `markercolour:name`: the base marker colour
 - `strokethickness:name`: the stroke thickness and marker size
 - `markerType:name`: the type of marker, any of `cross`, `square`, `triangle`, `diamong`, `circle`, `none` (default)
 - `resolution:name`: the stride between samples (default is 1)

Some of these also work without a key, in which case they will apply to all providers that support this usage.

The available providers are:

 - `fitness`: b - lambda*c
 - `benefit`: b
 - `cost`: c
 - `ptraits`: phenotypic trait expression
 - `gtraits`: genotypic trait expression
 - `regcoef`: regulation coefficients
 - `gsum`: sum of genotypic trait expressions
 - `huskyness`: degree of hierarhcy
 - `sudoku`: sudoku score
 - `moduleindependence`
 - `deltab` (slow, should probably be renamed dbdb)

##### Tracee Epoch Boundaries

Note that mutli-epoch `tracee` files may have duplicated generations on 'epoch boundaries', where one sample is provided for each epoch, but they report a single generation. This means that they include information about reset and fitness changes before the system is able to respond (which other trajectory formats do not contain). This is not applicable to `wholesample` files, as they do not contian sub-epoch samples.

These multi-sampled boundaries can create issues with plotting, for example when down-sampling, where the extra samples will mess up the sample period. Consider setting the `dropMode` to remove boundary samples; it may take any of 3 values:
 
 - `none`: don't drop any samples (default)
 - `dropFirst`: drop the first sample of the _next_ epoch on any boundaries
 - `dropLast`: drop the last sample of the _previous_ epoch on any boundaries

These boundaries can also produce misleading line-graphs. Consider setting `injectBreaks=true` to inject a break in any lines on epoch boundaries. You can further set the `brokenLineStyle` to change the style of the lines used for breaks, which detaults to `dot`. Note that broken lines may add a significant rendering: they can be commited completely by setting `brokenLineStyle=none`.

#### `traces`

_Not to be confused with Trace Info/tracee_

These files (prefix `traces`) are metrics (fitness) collated from many tracees, and the `traces` parameter takes a semi-colon (`;`) delimitated list of them. There are two render modes:

 - `areas`: plots areas representing percentiles for each traces
 - not `areas`: plots individual traces for each traces

Other parameters:

 - `names`: a semi-colon (`;`) delimited list of names to give to each of the traces (one per file)
 - `colors`: a semi-colon (`;`) delimited list of colors to give to each of the traces (one per file)
 - `mean`: measures `mean` metrics rather than 'best' metrics within a population
 - `start=generation`: sets the generation from which to start processing & plotting
 - `end=generation`: sets the generation from which to start processing & plotting
 - `retimeStart=generation`: offsets the generations (i.e. x-axis) so that they start at the given generation
 - `resolution=interval`: the post-processed plotting resolution (`resolution=1` is the default which plots everything)
 - `ymin`: the min value to show on the y-axis
 - `ymax`: the max value to show on the y-axis

#### `misc`

If the word `misc` is supplied instead of a file-name as the `plot` parameter, then a table of miscellanous plot providers is consulted according to the `misc=provder`. These includes:

 - `ivmcproperthing`: plots the 'proper' cost function
 - `ivmcsplitthing`: the the 'split' cost function
 - `transition`: plots transition plots of dubious utility
 - `McPhenotypeTransition`: plots phenotype transitions (jelly molds) for mc problems (need to supply lots of parameters) or a given expfile (in which case it has sensible defaults)
    example: `m4m -plot misc misc=McPhenotypeTransition n=32 k=1 a=0 b=32 -expfile epoch0savestart.dat terpmode=one`
    - `expfile`, is specified, is the expfile to use for fitness computation and all that
    - `n` is the number of traits
    - `k` is the module size
    - `a` is the initial number of 'set' modules (left of the plot)
    - `b` is the terminal number of set modules (right of the plot)
    - `terpMode` is the interpolation mode, one of `one` or `all`
    - `x`, if specified, has it plot the phenotype with the given progress between a and b
    - `target`, if specified, plots a target from the expfile if it is an `Epistatics.CorrelationMatrixTarget` (this is a tad out of place...)

// TODO: need more info about things.

#### Plotting Post-Processing

There are a number 'post-postprocessing' parameters which control how the figure looks, having nothing to with the data itself. Most of these are implemented in `PreProcessPlot`. Those that only apply to exports are implemented in `PostProcessPlot`.

 - `title=text`: sets the plot-title 
 - `legenttitle=text`: sets the legent-title
 - `legend=pos`: positions the legend (inside/outside, top/bottom, left/right, and none)
 - `size=size`: sets the plot size relative to A4 landscape (note: this is 'paper' size, not the size of the axes)
 - `width=width`: overrides the size, setting the plot width in 1/72" (note: this is 'paper' size, not the width of the axes)
 - `height=height`: overrides the size, setting the plot height in 1/72" (note: this is 'paper' size, not the height of the axes)
 - `border=thickness` sets the border thickness
 - `padding=margins`: sets the plot margins manually, either `left;top;right;bottom` or `all`
 - `intervalsize`: scales the interval size for all axes. A larger interval size means fewer ticks on each axis
 - `intervalsizeh`: scales the interval size for horizontal axes
 - `intervalsizez`: scales the interval size for vertical axes
 - `forceaspect`: messes with the width and height to force the axes to be square
 - `allgrey`: changes all line-plots to be a variety of greys, regardless of their original colours (note british spelling)
 - `repeatlines=interval`: adds a vertical line at regular intervals
 - `defaultcolors=colorlist`: takes a semi-colon seperated list of colors to use as the default series colors

Some parameters operate on individual axes. You can't always know what the axes are called, but the names of every axis will be printed if `quiet` is not set. In most cases, omiting the `:key` part will mean it applies to all axes.

 - `tier:axis=int`: sets the position tier of the axis 
 - `label:axis=text`: sets the axis-label of the axis (e.g. `label:x=Epochs`)
 - `format:axis=format`: sets the format mode of the axis (e.g. `0000.000` or `0E+4` or `auto`)
 - `visible:axis=bool`: sets the visibility of the axis
 - `minpad:axis=padding`: sets the minimum padding of the axis
 - `maxpad:axis=padding`: sets the maximum padding of the axis
 - `min:axis=value`: sets the minimum value of the axis (overrides axis padding)
 - `max:axis=value`: sets the maximum value of the axis (overrides axis padding)
 - `pos:axis=position`: sets the axis position (left/right/top/bottom)
 - `zerocrossing:axis=bool`: sets whether the axis should be positioned at the zero-crossing
 - `axislinestyle:axis=linestyle`: sets the line style of the axis
 - `axislinecolour:axis=linestyle`: sets the line color of the axis
 - `axislinethickness:axis=linestyle`: sets the line thickness of the axis
 - `startposition:axis=value`: sets the start position of the axis
 - `endposition:axis=value`: sets the end position of the axis
 - `axislinestyle:axis=linestyle`: sets the line style of the axis
 - `majorticksize:axis`
 - `majorgridlinestyle:axis`
 - `majorgridlinecolour:axis`
 - `majorgridlinethickness:axis`
 - `minorticksize:axis`
 - `minorgridlinestyle:axis`
 - `minorgridlinecolour:axis`
 - `minorgridlinethickness:axis`

Some parameters operate on individual series. In most cases, omiting the `:key` part will mean it applies to all series.

 - `reduceGeometry:series=bool`: enables or disables geometry reduction, which may reduce the accuracy of line and area series in favour of smaller output (default false)

### `spit`

Writes out a `config.txt` for the given population experiment.

 - `split=filename`: the filename for the population experiment
 - `out=filename`: the output filename, defaults to `config.txt`

### `tracee`

Runs a trace, producing a `tracee` file. Parameters include:

 - `postfix`: the postfix of the `tracee` file
 - `topdir`: the directory wherein to run
 - `seed=int`: the random seed to use
 - `targets=intlist`: an int-list of target indexes to loop through; uses target0 by default
 - `cycles=int`: the number of times to cycle through the targets per epoch
 - `sampleperiod=generations`: the sampling period in generations, default is 1 (take all samples)
 - `exposureEpoch=int`: the starting epoch
 - `epochs=int`: the number of epochs to run

Basic Examples:

    // runs a tracee, looping through targets 0 and 1 ten times, producing the file tracees/tracee2M0.dat
    m4m tracee=epoch2000000saveterminal targets="0;1" cycles=10 seed=1 topdir=tracees postfix=2M0

### `traces`

Runs many traces, producing a `traces` file.

 - `traces=expfile`: the population experiment from which to trace
 - `sedd=int`: the random seed
 - `count=int`: the number of traces to run, default is 1000
 - `postfix`: the `traces` file postfix, default is ""
 - `targets=intlist`: the list of targets to cycle through
 - `cycles=int`: the number of times to cycle through the targets per epoch
 - `sampleperiod=generations`: the sampling period in generations, default is 1 (take all samples)
 - `exposureEpoch=int`: the starting epoch
 - `epochs=int`: the number of epochs to run
 - `q`: the cli-prefix (no idea why this is configurable)

### `concat`

Concatenates data files together.

 - `concat`: a semi-colon delimited list of data-files
 - `out`: output filenamee
 - `mode`: the concatenation mode, one of `traj` (trajectories) or `ws` (wholesamples)

#### Trajectories mode

The trajectories mode, `traj`, concatenates trajectory files together (e.g. `rcs`, `ist`, `ivmcswitchness`, etc.). If your files do not have a sample period, you will have to provide one.

 - `trimends`: set to trim the last entry in all but the last trajectories (similar to `skipfirst`, but the other way round)
 - `sampleperiod`: the trajectory sampleperiod

#### Wholesamplse mode

The trajectories mode, `ws` or `wholesamples`, concatenates `wholesample` files together.

 - `skipfirst`: skip the first entry in successive files (similar to `trimends`, but the other way round)
 - `resequence`: resequences wholesamples in the order supplied

### `extract`

Extracts a population experiment from a `wholsamples` or `tracee`.

 - `extract`: the `wholesamples` or `tracee` source file to extract from
 - `expfile`: the experiment save file to reconfigure
 - `epoch`: the epoch to extract (`wholesamples` only)
 - `generation`: the generation to extract (`tracee` only)
 - `outdir`: the experiment output directory
 - `postfix`: the experiment savefile postfix
 - `zeroStart`: whether to zero the start epochs and generations (default is false: keep)
 - `firstOfEpoch`: whether to take the first entry for the given epoch (default is false: takes the last)

### `analyse`

Defers to `ICliAnalysers` based on the prefix of the file parameter. Only an `rcs` analyser is currently provided.

### `hebbian`

Generates individuals from an experiment by running a number (usually 1) of epochs, and then generates a genotype with a DTM that is the average of the autocorrelations of the individual genotype or phenotype or something.

 - `hebbian=expfile`: the experiment file
 - `epochs=int`: the number of epochs to run (default 1)
 - `startEpoch=int`: the start epoch (default 0)
 - `generations=int`: overrides the per-target generation count if set
 - `count=int`: the number of samples to use
 - `targets=intlist`: the sequence of targets (by index): by default, the first target it used alone
 - `initialreset`: if set, overrides the reset operation
 - `randomvector`: if set, overrides the reset operation (ignored if `initialreset` if set), one of `binary` or `uniform`
 - `extractor`: the vector extractor to use, either `g` (the initial state vector) or `p` (the phenotypic state vector)
 - `out`: output file name, ish: a `genome` file and PDF plot will be generated into the working directory
 - `title`: the title of the PDF output
 - `seed=int`: the random seed (default 1)
 - `weighted=bool`: whether the averaging should be weighted by fitness

### `reconfig`

Reconfigures a population experiment, enabling you to make specific adjustments to the configuration, modify the population, and change the targets. Parameters are grouped logically.

#### Boring Parameters

 - `outdir`: the directory wherein to place the reconfiged population experiment
 - `postfix`: the postfix to append to the population experiment filename

#### Population Experiment Config

Available parameters include:

 - `spinner`: the population spinner
 - `hillclimberMode=bool`: hill-climber mode
 - `elitecount=int`: the elite-count
 - `selectorpreparer`: the selector-preparer

#### Experiment Config

Available parameters include:

 - `epochs=int`: the number of epochs to run
 - `K`: the number of generations per target per epoch
 - `initialStateResetProbability=[0,1]`: the probability of reseting the population at the start of each target exposure
 - `targets=epistaticTargetPackage`: parses targets (e.g. `exp:`, `file:`, `ivmc:`, `mc:`); behaviour likely to change at some point to match composers
 - `targetCycler`: the target cycler

#### Population

You can change various things about the popuation, including swapping out completely. The following parameters are applied in order, and compound upon each other:

 - `population=expfile`: pull in a population from another different population experiemnt
 - `genome=genomefile`: pull in a new genome from a file
 - `g0`: the initial state vector
 - `b0`: the developmental transformation matrix
 - `pop.size`: the new population size. The populatoin is shuffled, before looping over the population and taking as many individuals as requested

#### `jrules`

JRules refers to Judgement Rules. Available parameters include:

 - `jrules.lambda=double`: the regularisation factor
 - `jrules.kappa=double`: the vector-target noise size

#### `drules`

DRules refers to Developmental Rules. Available parameters include:

 - `drules.T=int`: the number of developmental timesteps
 - `drules.tau1=[0,1]`: the update rate
 - `drules.tau2=[0,1]`: the decay rate

#### `rrules`

RRules refers to Reproduction Rules. Available parameters include:

 - `rrules.RB=double`: the probability of mutating the dtm (usually known as `rprob`)
 - `rrules.MG=double`: the magniute of initial state mutations
 - `rrules.MB=double`: the magniute of dtm mutations
 - `rrules.MGtype=double`: the type of mutations applied to the initial state vector
 - `rrules.MBtype=double`: the type of mutations applied to the dtm
 - `rrules.BEX=bool`: controls whether dtm mutations exclude initial state mutations
 - `rrules.CG=int`: the number of updates applied to the initial state vector per mutation

### `bestiary`

The `bestiary` provider has two functions: tabulating dtms, and tabulating mutant phenotypes. These have some common parameters:

 - `out=file.png`: controls the output file-name. If not extension is provided, '.png' will be appended
 - `min=double`: the value that is mapped to the coldest colour
 - `bhot`: sets black to be the hot colour
 - `whot`: sets white to be the hot colour, which is the default and overrides `bhot` if provided

#### Dtms

If `dir=directory` is provided as a parameter (with not value provided to `bestiary`), then table (bitmap) is produced from `genome` files within the sub-sub-directories of the given directory. Parameters include:

 - `gname=genomefilename`: specifies the file-name to look for in sub-sub-directories, default is `terminalgenome.dat` (for historical reasons, this file doesn't start with `genome`, but is otherwise equivalent to `genome_t.dat`)
 - `labels=bool`: controls whether labels should be rendered on the table, default is true (if the system crashes complaining about fonts, you probably want to disable these)
 - `font=fontname`: specifies the font to use if labels are to be added

#### Mutants

If `bestiary=epochfile` is provided as a parameter, a number of mutatnts are generated for every member of the population in the `epoch` file based on a given `gmode`, and their phenotypic state vectors drawn on a bitmpa image (one-row per module).

 - `gmode=mutationmode`: controls how mutants are generated from the given genome, details below
 - `seed=seed`: sets the random seed to be used
 - `count`: the number of mutants per individual in the population, default is 100

There are 3 mutation modes:

 - `gmode=binary`: each mutant has a single element set to either -1 or +1 randomly
 - `gmode=uniform`: each mutants has a single element set uniformally in the G-clamping range randomly
 - `gmode=mutate`: uses reproduction rules based on the population experiment configuration supplies as a parameter to `bestiary`

### `sparseswitchers`

Given an experiment file and the indices of the traits in a module, finds sparse network configurations which allow switching by flipping the first trait in the module

 - `sparseswitchers=filename`: the config to use
 - `module=intlist`: the list of traits in the module, default is to use all traits
 - `target=int`: the index of the target to use, default 0
 - `epoch=int`: target reset exposure epoch, implies reset target if set
 - `resettarget`: resets the target
 - `seed=int`: random seed
 - `proper`: enables/disables cplus and cminus
 - `cplus=doublelist`: list of c_plus values for target modules
 - `cminus=doublelist`: list of c_minus values for target modules
 - `T=int`: alternative developmental step count

## Simple Example

Before doing anything else, run `m4m` on its own to check you have everything set up and working. It should provide a list of providers if it is working. We will use or mention the `gen`, `exp`, `run`, `spit`, `plot`, `bestiary`, and `hebbian` providers in this example.

### Scenario

This is a simple example of how you might use the `m4m` tool. Perhaps you want to replicate the results found in a paper called _"How evolution learns to generalise"_ by Kouvaris et al. with the L1 (linear) regularistaion function. In particular, we want to plot the trajectories of regulatory connection strenghts over time, and investigate the distribution of phenotypes generated by the terminal genotype.

### Generating an experiment

There just happens to be a target package to describe the environment we need: `classic:orig4x4`. We could describe it with a heavily customised Ivmc target package, but that would detract from the point of a simple example. Here is how we will generate the experiment:

    m4m gen=composedense topdir=Experiments name=Test1 lambda=0.1 epochs=200 targetpackage=classic:orig4x4 regfunc=l1 epochs=200 K=20000 mg=0.1 mgtype=uniform mb=1e-4 mbtype=uniform bprob=1 bex=false decayrate=0.2 T=10 typo=true

There is a _lot_ of information here, and it is all model parameters. If anyone claims to have a simple model, but it has this many parameters (there are probably a few more which are defaults which I've forgotten to mention) then they are lying. Let's go through everything to reassure you that nothing scary is happening, and to give you an idea of how you can start to change parameters to run new and exciting experiments that nobody else will care about:

 - `gen=composedense`: tells m4m we need the `gen` provider, and tells the provider to compose something dense (never use anything else: they are all legacy and unmaintained)
 - `topdir=Experiments`: create our experiment within a directory called `Experiments`
 - `name=Test`: Create the experiment within a sub-directory called `Test`
 - `regfunc=l1`: use the L1 regularisation function (cost of connections)
 - `lambda=0.1`: the cost factor
 - `targetpackage=classic:orig4x4`: use the classic `orig4x4` target package, which contains 3 targets
 - `epochs=200`: 200 epochs please
 - `K=20000`: 20000 generations per target per epoch
 - `bex=false`: mutate B and G at the same time
 - `bprob=1`: always mutate B
 - `mgtype=uniform`: uniform mutation in G
 - `mg=0.1`: small mutations in G
 - `mbtype=uniform`: uniform mutation in B
 - `mb=1e-3`: no idea what this number is: you'll need to read the paper to calculate it; I am too lazy to do so now
 - `typo=true`: doesn't do anything apart from illustrate the unused parameter warning, which you should always check
 - `T=10`: the number of developmental timesteps is 10, which happens to be the default, but default are liable to mean you default on your morgage payments, so specify it anyway
 - `decayrate=0.2`: also a default (tau_2 in the paper)

Running this `gen` command should create an `Experiments` directory containing another `Test` directory, which contains the following files:

 - config.dat: a configuration data file (not every useful)
 - config.txt: a text-summary of the experiment, which is very useful for checking the experiment is as expected: don't modify it, as it doesn't change the experiment, and will only create confusion. If you do modify or delete the file by accident, you can create a new one by running `m4m -spit epoch0savestarter.dat` (it works with any `epoch` experiment file)
 - epoch0savestarter.dat: The experiment file itself, which is explained in detail somewhere in this document. This is the file we will 'run' in order to perform the experiment.
 - initialpopulation.dat: The initial population. I don't think I've ever used one of these for anything, so feel free to delete it (the same information in contained in the experiment file)

It should also inform you that the "typo" parameter was not consumed. If you'd typo'd `decayrate` (or some other important thing) this would be your first warning of this fact. Your second warning should be when you look at the `config.txt` file to check everything looks correct. Your third warning is when you get angry because nothing makes sense, so it's best to make the most of the first 2.

### Running an experiment

Now we can run the experiment. We can use either the `exp` or `run` providers. The `exp` provider is good for running just one experiment. The `run` provider is good for running a whole block at once. Both are easy to use, and are generally configurable in the same way.

This is how to run an experiment 4 times with a given seed (the repeats will have incrementally increasing seeds):

    m4m -exp Experiments/Test1/epoch0savestarter.dat seed=1 repeats=4

Because we have specified a non-trivial number of repeats, the program will produce a separate output directory for each repeat, called `r0`, `r1`, `r2`, etc. Had we not specified a number of repeats, the outputs would be outputed to the same directory as experiment file.

A similar result can be achieved with `run` as followed:

    m4m -run Experiments seed=1 repeats=4 postfix=One

This will create a directory called `TestrunsOne` containing the outputs.

If we had any more experiments in directors in `Experiments` (e.g. created with `name=Test2` or `name=Test3`, etc.), then these would have also been run.

We could run a second set of runs (e.g. with a different base seed) into a new directory by changing the postfix.

The output from `exp` and `run` is very similar, and usually worth holding onto (e.g. by piping to a log file) so that you can look up seeds and run-time in the future. While the experiments are running themselves, it prints incremental reports on its progress, and basic population metrics (mean benefit, cost, and fitness).

As the simulation runs, output files will start to appear. The last set to be produced will all end in `_t`, indicating the terminal copy. Usually we end up deleting all the numbered files which also have an `_t` varient, because they contain a subset of the same data (or are genome files we don't really care about). In our case, we asked for only 200 epochs, so there are not many 'partial' results.

 - config.dat: as discussed
 - config.txt: as discussed
 - epoch0savestart.dat: a copy of epoch0savestarter.dat
 - epoch200saveterminal.dat: the terminal experiment file, which contains all the information to run the experiment, but contains the terminal population and internally knows it has reached 200 epochs. These are extremely useful for analyses, and can be used to 'continue' an experiment if we need to
 - fitness_t(best).dat: fitness trajectories
 - genome0.dat: the starting genome
 - genome200.dat: genome at end of epoch 200
 - genome_t.dat: terminal genome
 - initialpopulation.dat: as discussed
 - ist_t(best).dat: initial state trajectories
 - pst_t(best).dat: phenotypic state trajectorties
 - rcs_t(best).dat: regulatory connection strength trajectories
 - terminalgenome.dat: terminal genome again, but with a less-useful filename
 - wholesamplese200.dat: wholesamplese from epoch 0 to 200

The `m4mclean` powershell function in `m4minit.ps1` will recursively clean away any numbered files in directories that contain a `genome_t.dat` file.

### Plotting results

Drop into one of the output directories (e.g. `Experiments/Test1/r0` ir you used `exp` or `ExperimentsrunsOne/Test1r0` if you used `run`) to make life easy:

    cd Experiments/Test1/r0
    cd ExperimentsrunsOne/Test1r0

First things first, we should look at the terminal genome. We can plot it with the following command.

    m4m -plot genome_t.dat thing=dtm out=terminal forceaspect title="Terminal Developmental Matrix" min=-1 max=+1

With a bit of luck, it should look like this:

![Terminal Genome](ReadmeFigures/terminal.pdf.png)

The parmeters we have given to `plot` (which is the provider we are using here) are as follows:

 - `plot=genome_t.dat`: the file we want to plot, which for `genome` files plots the developmental transformation matrix by default
 - `thing=dtm`: asks to plot the dtm (which is also the default). We could alternative have asked for `is` or `ps`, the initial and phenotypic states respectively
 - `out=terminal`: asks it to output any file to `terminal.pdf` (the extension is implied: in future it might support more export types). If you don't specify `out`, it will be the same as the input file name
 - `forceaspect`: a flag parameter which forces a genome plot like this to be square ish: you'll generally not use it for anything else
 - `title=whatever`: specifies the title of the plot
 - `min=-1`: specifies the minimum (black) value: by default it is the minimum value in the plot, which is great for comparing within one genome, but less so when comparing between plots
  - `max=+1`: specifies the maximum (white) value: by default it is the maximum value in the plot

Plotting an `epoch` experiment file will, by default, behave like plotting a `genome` file, though there are other things you can plot, and the `thing=dev` option shows the trait expression trajectories from initial state to phenotypic state.

Since our genome looks about right, let's see what actually happened during the experiment:

    m4m -plot rcs_t(best).dat

This is a bit hideous, so add the `green` flag to make it tidier looking. It is also clear that not a whole lot happens after epoch `20`, so set `end=20` so we can focus on the detail. We also don't need the legend, so get rid of it by nulling it with `legend=`:

    m4m -plot "rcs_t(best).dat" green end=20 legend= title="Regulatory Connections Trajectories" out=rcs

Hopefully it looks like this:

![Regulatory Connection Trajectories](ReadmeFigures/rcs.pdf.png)

### Phenotype Distributions

Though the DTM is extremly informative by itself, it can help to look at the sorts of phenotypes it produces when random initial state vectors are developed with it.

    m4m -bestiary epoch200saveterminal.dat out=nb.png gmode=uniform seed=1 count=100

This will give you something like this (one pixel per trait; row-major):

![Sample Phenotypes](ReadmeFigures/nb.png)

 - `bestiary=epoch200saveterminal.dat`: the experiment from which to sample
 - `out=nb.png`: the output filename (this time it needs an extention... maybe .jpg would work, I don't know, it's delegated to ImageSharp)
 - `seed=1`: the random seed
 - `gmode=uniform`: the initial state randomisation mode, uniform random values
 - `count=100`: the number of phenotype samples to generate

You can add a `generations=int` parameter to cause each sample to go through that many generations before being rendered.

If looking at phenotypes individually isn't your thing, the `hebbian` provider will average the correlations between them for you:

    m4m -hebbian epoch200saveterminal.dat out=hebb generations=0 seed=1 initialreset=uniform extractor=p

This should give you:

![Sample Phenotypes](ReadmeFigures/hebb.pdf.png)

 - `hebbian=epoch200saveterminal.dat`: you guessed, the experiment from which to sample
 - `out=hebb`: the output name (you get a PDF and a genome file)
 - `generations=0`: don't run any geneations (unlike with `bestiary`, this defaults to `K` from the experiment)
 - `initialreset=binary`: the initial state randomisation mode, binary random values
 - `extractor=p`: take the phenotype (default is the genotype, which with `generations=0` won't tell you much)
 - `seed=1`: the random seed

### Running a Tracee

We can run a Tracee, to investiage the within- and between-epoch behaviour of the system. A tracee is run from an `epoch` experiment file: often we may want to compare the behaviour at the start or end of a simulation (note that you can configure `exp` and `run` to record `tracees` at intervals (see section _Default Feedback_), which is a better solution if you want to show the change in behaviour of a specific 'run' of the whole system).

Let's get two post-hoc tracees, for the start and terminal `epoch` experiment files:

    m4m -tracee .\epoch0savestart.dat epochs=5 targets="1;2;0" sampleperiod=10 postfix=0
    m4m -tracee .\epoch200savestart.dat epochs=5 targets="1;2;0" sampleperiod=10 postfix=200

Note the slightly weird ordering of the `targets` parameter, which relects the behaviour of the default target cycler (i.e. starts with the 'second' target, not the first). Note also that we have reduced the sample period to keep the file size small, and provided a different postfix for each file, that corresponds to the epoch number of the experiment.

These tracees are of no use without being about to interrogate them. Let's plot part of both of them, so we can compare:

    m4m -plot .\tracee0.dat start=100k end=140k providers=fitness:f markertype:f=diamond downsample=100
    m4m -plot .\tracee200.dat start=100k end=140k providers=fitness:f markertype:f=diamond downsample=100

Here, we use the traceplotting provider `fitness`, because it allows use to configure the marker type, and we only plot the values between 100k and 140k epochs.

If you happen to have the `FigForm` utility handy, you can place the resulting pdfs side-by-side (or over-and-under, if you change the arrangment to `0&1`) as so:

    figform -pages ".\tracee0.dat.pdf;.\tracee200.dat.pdf" -arrangement "0;1" -out both.pdf

![Two Tracees, side by side.](ReadmeFigures/traceesSideBySide.png)

### Running Traces

Traces have very little in common with Tracees. Instead of running one simulation over potentially many epochs (recording alot of information along the way), traces runs many simulations, and only record basic metrics (e.g. the best fitness in the population).

    m4m -traces epoch200saveterminal.dat count=100 postfix=200
    m4m -plot traces200.dat areas wide=0,50

The `areas` parameter means we want to plot the 'percentile bounds' over all simulations, and the `wide` parameter determines which percentile ranges are plotting: e.g. `0` refers to the range `0-100` (i.e. shows the whole range of values), `25` would refer to the inner quartiles, and `50` is special, refering to the mean, which is plotted as a solid line rather than an area.

Although they record less information - and in a more efficient format - it is still advisible to reduce the `sampleperiod` when running `traces`. In this particular set of traces, we can clearly see two s-curves (corresponding to the changing of two dense modules).

![Traces.](ReadmeFigures/traces.png)

Intuitively, this makes no sense... and when I take a while to think about it, it still makes no sense. Maybe it's because we are comparing mutations in `G` to mutations in `B`?

### Reconfiguring an experiment

Let's turn on exclusive `B` mutation, set the probability of `B` mutation to one half, and see if the traces change.

    m4m -reconfig .\epoch200saveterminal.dat outdir=reconfigs rrules.bex=true rrules.rb=0.5 postfix=BEx
    cd .\reconfigsBEx\
    m4m -traces .\epoch0savestarter.dat count=100 targets=1

![Reconfiged Traces.](ReadmeFigures/reconfigedTraces.png)

Nope. That isn't it, and the fact that the curves are almost exactly twice the size suggests the `B` has no influence on `G` anyway. The reality is that this is just an artefact of the percitile ranging: by 1200 generations, most runs have changed one module, and by 1400, most have changed the other: there is no systematic pattern to the runs themselves (i.e. the distribution of swap times for modules is independent and not bi-modal).

Maybe the area plotting mode is misleading us?

That concludes the basic example. The best way to work out how to use the various tools is to look at the code, but hopefully this readme is of some utility.

## M4M Utility Tools

There are 3 additional (windows only) tools that may be of some great benefit. They are all (fairly simple) UI's built on top of the M4M.Model and M4M.New libraries, and are built with the m4mbuild powershell command from the Visual Studio Developer Powershell. Direct DLL referenes are used in-code (rather than project reference) due to issues with older versions of msbuild and Visual Studio. They are .NET Framework applications (though I intend to make them also compile to .NET Core 3.1 at some point) using the Windows Forms GUI framework.

### M4MPlotting

A small program that allows interaction with plots preduced by `m4m plot` commands. There was a simplified cross-platform version of this once, but it was slow. If you need it, give me a shout and I'll dig it out (or more likely re-write it).

![Interrogating an rcs file with M4MPlotting](ReadmeFigures/m4mplotting.png)

Though it mostly just shows plots and allows you modify the 'command-line' arguments for the m4m plot command, there is some additional functionality included for dealing with specific files that is mostly easy to discover:

 - click on `rcs` trajectories to see the trans mat at that point in time
 - click on `wholesmaples` or `tracee` trajectories to see the trans mat/initial states/whatever at the point
 - the `CliQ` misc plotter happens to be interactive
 - you can add draggable cursors with the `cursors` flat, which can be customised per axis (e.g. `cursors:x=blue`)

I use this tool endlessly, and have .dat files open with it by default, so I can just double click on a data-file to see what is inside. You can slap Ctrl-C to copy a bitmap of the image, or export it as a PDF or PNG from the File > Export menu (the extension name determines the output format). The File > View Logs menu shows you the same output you would get from running the command at the command line.

The `m4mplot` function in `m4minit.ps1` opens this and copies the other command-line parameters into the parameter text box. This is handy for opening e.g. large files where you have to specify a large downsample in order for it to render in good time.

Don't overlook the `File` > `ViewLogs` option, which is handy for checking for errors, unused flags, and picking up axis keys.

### M4MDenseDev

A not-so small program that allows viewing of wholesamples or rcs/ist pairs, and messing about with dense genomes. This is used in the usual demo, where a hierarchy produing rcs is loaded, the switch genes are identified, and then G is set so that the switch genes are high and everything else is low. Sweeping through the rcs then shows up how hierarchy enables the switch genes to take over. To load stuff, you must drag the files onto the main window, which also allows you to quickly compose an experiment for testing/demonstration purposes (though usually you will want to load actual data files).

![The classic evolution of hierarchy demo](ReadmeFigures/m4mdensedev.png)

Where applicable, M4MDenseDev is a better tool than M4MPlotting, because it is optimised to work with what it expects, rather than just being a thin layer over the command-line plotter. It is very useful for demonstrating what different regulatory matricies do, and getting a feel for the signal felt by the B matrix.

### M4MDensemod

A small program that allows you to step one epoch at a time and modify the initial state vector. It is really only useful for investigaing static landscape problems, with a nice visualiasation for 4-neighbour constraint matricies (e.g. the concentric squares problem).

![The concentric squares problem](ReadmeFigures/m4mdensemod.png)

## Design Notes 

There are many bad things about M4M. Here are some of the more important mistakes in its design:

 - There is no strong seperation between things in a population, and things outside of a population: this makes it hard (impossible) to provide low-overhead and 'ideal' reset operations, because they have to perform a development step themselves, because they can't assume nothing has escaped the population
    - This basically means the epistatic mode can't support reset operations properly
    - It also means that there is a hideous system for tracking 'escaped' individuals: we tried to provide a seperation with the `Process' method, but isn't good enough and I can't assume it works
 - There is no clarity on when individuals are reported in feedback
 - The developmental interface is too restrictive
 - A number of key components use caching to reduce overhead, which makes parallelism impossible
 - The parallism mechanism that exists (but should not) makes a mockery of deterministic execution: don't use it
 - The individuals that are reported by the `SpinPopulation` methods are not those that are in the population after each generation: they are those in the population before each generation. This is because not all (population) modules actually require the offspring to be evaluated, but what it means is that all the feedback stuff is slightly misleading.

Some other things that could be better:

 - Expfiles should not have a directory associated with them
 - Experiment duration should be easier to change
 - Experiment configs should be eaiser to edit, or should know how to 're-generate' themselves: they should have a known seed
 - Experiment seeds are not persisted: you can't continue an experiment with the same seed
 - There shouldn't be a hard dependency on MathNET: it would be inefficient to inject AD stuff, because conversions are be necessary; it would be nicer if (at run-time) any math-backing could be used
 - `savestart.dat` autocompletes before `savestarter.dat`, so it's very easy to run the wrong thing when you are reusing directories
 - Lots of components do not depend on the type of individual; however, there isn't the infrastructure to support such genericism, so they mostly fail if used with anything else