# run netstateb and m4mb (alias defined below) if you need to build m4m

m4mframework='netcoreapp2.1' # change this if you don't have net core 2.1
m4mdir=$(pwd)

echo "M4M Dir is $m4mdir"

# alias
alias netstateb='dotnet build "$m4mdir/../NetState/NetState/NetState.csproj" --configuration Release'
alias m4mb='dotnet build "$m4mdir/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Release --framework $m4mframework /p:M4M_DUAL_TARGET=true'
alias m4m='dotnet $m4mdir/M4M.CoreRunner/bin/Release/$m4mframework/M4M.CoreRunner.dll'