﻿using System.IO;
using System.Text;

namespace M4M.Tests
{
    public class DummyConsole : TextWriter
    {
        public static DummyConsole Instance { get; } = new DummyConsole();

        public override Encoding Encoding => System.Text.Encoding.Unicode;
    }
}
