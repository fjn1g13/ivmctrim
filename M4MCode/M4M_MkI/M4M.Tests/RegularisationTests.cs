﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Linq;

namespace M4M.Tests
{
    [TestClass]
    public class RegularisationTests
    {
        [TestMethod]
        public void TestTypicals2x2()
        {
            var g0 = CreateVector.Dense<double>(2);
            var b0 = CreateMatrix.DenseOfArray(new[,] { { 1.0, -0.5 }, { 2.0, 0.0 } });
            var genome = new DenseGenome(g0, b0);

            var lno = JudgementRules.ConstantEquivalent.ComputeCost(genome);
            var l1 = JudgementRules.L1Equivalent.ComputeCost(genome);
            var l2 = JudgementRules.L2Equivalent.ComputeCost(genome);

            Assert.AreEqual(1.0, lno); // 1
            Assert.AreEqual(3.5 / 4, l1); // sum / n^2
            Assert.AreEqual(5.25 / 4, l2); // sqr-sum / n^2
        }

        [TestMethod]
        public void TestTypicals4x4()
        {
            var g0 = CreateVector.Dense<double>(4);
            var b0 = CreateMatrix.DenseOfArray(new[,] { { 1.0, -0.5, 0.0, 0.0 }, { 0.0, 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0, 0.0 }, { 0.0, 0.0, 2.0, 0.0 } });
            var genome = new DenseGenome(g0, b0);

            var lno = JudgementRules.ConstantEquivalent.ComputeCost(genome);
            var l1 = JudgementRules.L1Equivalent.ComputeCost(genome);
            var l1row = JudgementRules.RowL1Equivalent.ComputeCost(genome);
            var l1col = JudgementRules.RowL1Equivalent.ComputeCost(genome);
            var l2 = JudgementRules.L2Equivalent.ComputeCost(genome);

            Assert.AreEqual(1.0, lno); // 1
            Assert.AreEqual(3.5 / 16, l1); // sum / n^2
            Assert.AreEqual(3.5 / 16, l1row); // sum / n^2
            Assert.AreEqual(3.5 / 16, l1col); // sum / n^2
            Assert.AreEqual(5.25 / 16, l2); // sqr-sum / n^2
        }

        [TestMethod]
        public void TestDiagonal()
        {
            var g0 = CreateVector.Dense<double>(4);
            var b0 = CreateMatrix.Diagonal<double>(new[] { 1.0, -0.5, 0.0, 2.0 });
            var genome = new DenseGenome(g0, b0);

            var lno = JudgementRules.ConstantEquivalent.ComputeCost(genome);
            var l1 = JudgementRules.L1Equivalent.ComputeCost(genome);
            var l1row = JudgementRules.RowL1Equivalent.ComputeCost(genome);
            var l1col = JudgementRules.RowL1Equivalent.ComputeCost(genome);
            var l2 = JudgementRules.L2Equivalent.ComputeCost(genome);

            Assert.AreEqual(1.0, lno); // 1
            Assert.AreEqual(3.5 / 16, l1); // sum / n^2
            Assert.AreEqual(3.5 / 16, l1row); // sum / n^2
            Assert.AreEqual(3.5 / 16, l1col); // sum / n^2
            Assert.AreEqual(5.25 / 16, l2); // sqr-sum / n^2
        }

        [TestMethod]
        public void TestCandidatesLarge()
        {
            var n = 256;

            var ones = CreateVector.Dense(n, 1.0);

            for (int i = 0; i < 10; i++)
            {
                var rnd = new Random(i);
                var b = CreateMatrix.Dense<double>(n, n, (_i, _j) => rnd.NextDouble() * 20 - 10);

                var rh1 = RegularisationHelpers.L1Sum(b);
                var rh1row = RegularisationHelpers.RowL1Sum(b);
                var rh1col = RegularisationHelpers.ColL1Sum(b);
                var l1 = b.Enumerate().Sum(Math.Abs);
                var mp1 = b.PointwiseAbs().LeftMultiply(ones).DotProduct(ones);

                MathAssert.AreClose(l1, rh1);
                MathAssert.AreClose(l1, rh1row);
                MathAssert.AreClose(l1, rh1col);
                Assert.AreEqual(rh1, rh1row);
                MathAssert.AreClose(l1, rh1col);

                var rh2 = RegularisationHelpers.L2Sum(b);
                var l2 = b.Enumerate().Sum(x => x * x);
                var mp2 = b.PointwiseMultiply(b).LeftMultiply(ones).DotProduct(ones);

                MathAssert.AreClose(l2, rh2);
                MathAssert.AreClose(l2, mp2);
            }
        }
    }
}
