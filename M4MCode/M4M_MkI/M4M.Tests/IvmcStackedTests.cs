﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathNet.Numerics.LinearAlgebra;
using System;
using M4M.Epistatics;

namespace M4M.Tests
{
    [TestClass]
    public class IvmcStackedTests
    {
        public readonly Phenotype PM1 = new Phenotype(CreateVector.Dense<double>(new[] { -1.0, -1, -1, -1, -1, -1, -1, -1 }));
        public readonly Phenotype PZ0 = new Phenotype(CreateVector.Dense<double>(new[] { 0.0, 0, 0, 0, 0, 0, 0, 0 }));
        public readonly Phenotype PP1 = new Phenotype(CreateVector.Dense<double>(new[] { +1.0, +1, +1, +1, +1, +1, +1, +1 }));
        public readonly Phenotype PMixA = new Phenotype(CreateVector.Dense<double>(new[] { 1.0, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 0.0 }));

        [TestMethod]
        public void TestSplitModuleBenefitFunction()
        {
            var eta = new SplitModuleBenefitFunction();

            MathAssert.AreClose(1.0, eta.ComputeModuleBenefit(-1.0, 1.0, 1.0));
            MathAssert.AreClose(-1.0, eta.ComputeModuleBenefit(-1.0, -1.0, 1.0));
            MathAssert.AreClose(-0.5, eta.ComputeModuleBenefit(-1.0, -0.5, -0.5));

            MathAssert.AreClose(0.2, eta.ComputeModuleBenefit(-0.2, 1.0, 1.0));
            MathAssert.AreClose(-0.2, eta.ComputeModuleBenefit(-0.2, -1.0, 1.0));
            MathAssert.AreClose(-0.1, eta.ComputeModuleBenefit(-0.2, -0.5, -0.5));

            MathAssert.AreClose(0.0, eta.ComputeModuleBenefit(0.0, 1.0, 1.0));
            MathAssert.AreClose(0.0, eta.ComputeModuleBenefit(0.0, -1.0, 1.0));
            MathAssert.AreClose(0.0, eta.ComputeModuleBenefit(0.0, -0.5, -0.5));

            MathAssert.AreClose(0.4, eta.ComputeModuleBenefit(0.4, 1.0, 1.0));
            MathAssert.AreClose(0.4, eta.ComputeModuleBenefit(0.4, -1.0, 1.0));
            MathAssert.AreClose(-0.2, eta.ComputeModuleBenefit(0.4, -0.5, -0.5));

            MathAssert.AreClose(1.0, eta.ComputeModuleBenefit(1.0, 1.0, 1.0));
            MathAssert.AreClose(1.0, eta.ComputeModuleBenefit(1.0, -1.0, 1.0));
            MathAssert.AreClose(-0.5, eta.ComputeModuleBenefit(1.0, -0.5, -0.5));
        }

        [TestMethod]
        public void TestProperModuleBenefitFunction()
        {
            var eta = new ProperModuleBenefitFunction();

            // negative
            // -1  -> 1
            // -0.2 -> 0.36
            // 0.0 -> 0.25
            // 0.4 -> 0.09
            // +1  -> 0

            // positive
            // -1  -> 0
            // -0.2 -> 0.16
            // 0.0 -> 0.25
            // 0.4 -> 0.49
            // +1  -> 1

            MathAssert.AreClose(1.0, eta.ComputeModuleBenefit(-1.0, 1.0, 1.0));
            MathAssert.AreClose(-1.0, eta.ComputeModuleBenefit(-1.0, -1.0, 1.0));
            MathAssert.AreClose(-0.5, eta.ComputeModuleBenefit(-1.0, -0.5, -0.5));

            MathAssert.AreClose(0.52, eta.ComputeModuleBenefit(-0.2, 1.0, 1.0));
            MathAssert.AreClose(-0.2, eta.ComputeModuleBenefit(-0.2, -1.0, 1.0));
            MathAssert.AreClose(-0.26, eta.ComputeModuleBenefit(-0.2, -0.5, -0.5));

            MathAssert.AreClose(0.5, eta.ComputeModuleBenefit(0.0, 1.0, 1.0));
            MathAssert.AreClose(0.0, eta.ComputeModuleBenefit(0.0, -1.0, 1.0));
            MathAssert.AreClose(-0.25, eta.ComputeModuleBenefit(0.0, -0.5, -0.5));

            MathAssert.AreClose(0.58, eta.ComputeModuleBenefit(0.4, 1.0, 1.0));
            MathAssert.AreClose(0.4, eta.ComputeModuleBenefit(0.4, -1.0, 1.0));
            MathAssert.AreClose(-0.29, eta.ComputeModuleBenefit(0.4, -0.5, -0.5));

            MathAssert.AreClose(1.0, eta.ComputeModuleBenefit(1.0, 1.0, 1.0));
            MathAssert.AreClose(1.0, eta.ComputeModuleBenefit(1.0, -1.0, 1.0));
            MathAssert.AreClose(-0.5, eta.ComputeModuleBenefit(1.0, -0.5, -0.5));
        }

        [TestMethod]
        public void TestSplitMCGen()
        {
            var clips = new CliParams();
            clips.ConsumeLine("targetstring=++++++++ modulesstring=aabbccdd judgemode=stacked modulebenefitfunction=split decayfactor=0.75 rescalefitness=true scalefactor=4 ch=1 cl=0.9 c0=-1 z=1");
            var targetPackage = ExperimentParsing.ParseIvmcTargetPackage(DummyConsole.Instance, clips, "custom");

            Assert.AreEqual(1, targetPackage.Targets.Length);
            Assert.AreEqual(8, targetPackage.TargetSize);

            void AssertAll(IvmcProperChanging target)
            {
                Assert.IsNotNull(target);

                Assert.AreEqual(1.0, target.CH);
                Assert.AreEqual(0.9, target.CL);
                Assert.AreEqual(-1, target.C0);

                var proper = target.Proper;

                Assert.AreEqual(4, proper.ModuleCount);
                Assert.AreEqual(8, proper.ElementCount);
                Assert.AreEqual(IvmcProperModulesType.FreeModules, proper.ModulesType);

                var judger = target.CustomJudger as IvmcStackedVectorTargetJudger;
                Assert.IsNotNull(judger);

                Assert.AreEqual(0.75, judger.DecayFactor);
                Assert.AreEqual(proper, judger.Proper);
                Assert.AreEqual(true, judger.RescaleFitness);

                var eta = judger.ModuleBenefitFunction as SplitModuleBenefitFunction;
                Assert.IsNotNull(eta);
            }

            var target0 = targetPackage.Targets[0] as IvmcProperChanging;

            AssertAll(target0);
            AssertAll(StateExtensions.Cycle(target0));
        }

        [TestMethod]
        public void TestImvStackAsMC()
        {
            var clips = new CliParams();
            clips.ConsumeLine("targetstring=++++++++ modulesstring=aabbccdd judgemode=stacked modulebenefitfunction=split decayfactor=0.75 rescalefitness=true scalefactor=4 ch=1 cl=1 c0=-1 z=1");
            var target = ExperimentParsing.ParseIvmcTargetPackage(DummyConsole.Instance, clips, "custom").Targets[0] as IvmcProperChanging;

            var context = new ModelExecutionContext(new CustomMersenneTwister(1));

            var jrules = TypicalConfiguration.CreateStandardJudgementRules(0.0, JudgementRules.ConstantEquivalent, 0);

            var m4p = new[] { 1.0, 1.0, 1.0, 1.0 };

            // check cminus/cplus
            for (int i = 0; i < 100; i++)
            {
                var exposureInformation = new ExposureInformation(1000);
                target.NextExposure(context.Rand, jrules, 0, ref exposureInformation);
                Assert.AreEqual(1000, exposureInformation.ExposureDuration); // shouldn't touch this

                target.NextGeneration(context.Rand, jrules);

                // should always be +1
                CollectionAssert.AreEqual(m4p, target.Proper.Cplus);
                CollectionAssert.AreEqual(m4p, target.Proper.Cminus);

                // check choice phenotypes
                MathAssert.AreClose(0.5, target.Judge(PM1)); // (max)
                MathAssert.AreClose(0.0, target.Judge(PZ0)); // (min)
                MathAssert.AreClose(0.5, target.Judge(PP1)); // (max)
                MathAssert.AreClose(((0.5 + 1 + 0.5 + 0) * 0.5 + (0.5 + 1 + -0.5 + 0) / 4 * 0.75 * 0.5) / (4.75), target.Judge(PMixA)); // (0.5, 1, -0.5, 0)
            }
        }
    }
}
