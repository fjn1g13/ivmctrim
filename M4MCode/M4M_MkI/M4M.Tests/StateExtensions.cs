﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace M4M.Tests
{
    public class StateExtensions
    {
        public static T Cycle<T>(T state) where T : class
        {
            using (var ms = new MemoryStream())
            {
                State.GraphSerialisation.Write<T>(state, ms);
                ms.Position = 0;
                return State.GraphSerialisation.Read<T>(ms);
            }
        }
    }
}
