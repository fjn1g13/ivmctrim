using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathNet.Numerics.LinearAlgebra;
using System;
using MathNet.Numerics;
using System.Linq;

namespace M4M.Tests
{
    [TestClass]
    public class DenseGenomeTests
    {
        public static ModelExecutionContext CreateContextNoRandom()
        {
            return new ModelExecutionContext(null);
        }

        [TestMethod]
        public void TestDrulesDefaults()
        {
            var drules = TypicalConfiguration.CreateStandardDevelopmentRules(DevelopmentRules.TanhHalf);
            Assert.AreEqual(10, drules.TimeSteps);
            Assert.AreEqual(1.0, drules.UpdateRate);
            Assert.AreEqual(0.2, drules.DecayRate);
            Assert.AreEqual(1.0, drules.RescaleScale);
            Assert.AreEqual(0.0, drules.RescaleOffset);
            Assert.AreEqual(DevelopmentRules.TanhHalf, drules.Squash);
        }

        [TestMethod]
        public void TestTypicalDrules()
        {
            var drules = TypicalConfiguration.CreateStandardDevelopmentRules(DevelopmentRules.TanhHalf, 5, 0.1, 0.2, 0.3, 0.4);
            Assert.AreEqual(5, drules.TimeSteps);
            Assert.AreEqual(0.1, drules.UpdateRate);
            Assert.AreEqual(0.2, drules.DecayRate);
            Assert.AreEqual(0.3, drules.RescaleScale);
            Assert.AreEqual(0.4, drules.RescaleOffset);
            Assert.AreEqual(DevelopmentRules.TanhHalf, drules.Squash);
        }

        [TestMethod]
        public void TestDevelopmentNoSquash()
        {
            var rnd = new Random(1);
            for (int _r = 0; _r < 10; _r++)
            {
                var T = rnd.Next(0, 20);
                var dr = rnd.NextDouble();
                var ur = rnd.NextDouble();
                var rs = rnd.NextDouble();
                var ro = rnd.NextDouble();

                int n = 16;

                var g0 = CreateVector.Dense(n, i => (double)i);
                var b0 = CreateMatrix.DenseDiagonal(n, 1.0); // independent

                var drules = TypicalConfiguration.CreateStandardDevelopmentRules(DevelopmentRules.None, T, ur, dr, rs, ro);
                var genome = new DenseGenome(g0, b0); // default dev-step

                var context = CreateContextNoRandom();

                var p = genome.Develop(context, drules).Vector;

                var prod =
                    Math.Pow(1.0 - drules.DecayRate + drules.UpdateRate, drules.TimeSteps) // T steps
                    * (drules.DecayRate / drules.UpdateRate); // tau2/tau1 product

                for (int i = 0; i < p.Count; i++)
                {
                    MathAssert.AreClose(i * prod * drules.RescaleScale + drules.RescaleOffset, p[i]);
                }
            }
        }

        [TestMethod]
        public void TestDefaultStateCycle()
        {
            var rrules = TypicalConfiguration.CreateStandardReproductionRules(0.1, 1e-3, 0.5, true);
            var context = new ModelExecutionContext(new CustomMersenneTwister(1));

            var genome = DenseGenome.CreateDefaultGenomeSparse(16, 8, context.Rand);

            for (int i = 0; i < 100; i++)
                genome.Mutate(context, rrules);

            // Assert there is noise: if there is no noise, it's a rubbish test
            Assert.AreNotSame(0.0, genome.InitialState.Sum());
            Assert.AreNotSame(0.0, genome.TransMat.L1Norm());

            var cycled = StateExtensions.Cycle(genome);

            CollectionAssert.AreEqual(genome.TransMatIndexOpenEntries.ToList(), cycled.TransMatIndexOpenEntries.ToList());
            CollectionAssert.AreEqual(genome.InitialState, cycled.InitialState);
            CollectionAssert.AreEqual(genome.TransMat.ToRowMajorArray(), cycled.TransMat.ToRowMajorArray());
        }
    }
}
