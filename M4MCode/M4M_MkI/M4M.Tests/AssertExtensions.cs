﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace M4M.Tests
{
    public static class MathAssert
    {
        public static void AreClose(double expected, double actual, double eps = 1e-10)
        {
            if (expected == actual)
                return; // covers degenerate cases like 0==0

            var diff = Math.Abs(expected - actual);
            var sum = Math.Abs(expected) + Math.Abs(actual);
            Assert.IsTrue(diff < sum * eps, $"Expected {expected}, Actual {actual}");
        }
    }
}
