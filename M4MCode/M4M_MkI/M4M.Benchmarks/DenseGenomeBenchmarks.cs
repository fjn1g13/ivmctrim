﻿using BenchmarkDotNet.Attributes;
using MathNet.Numerics.LinearAlgebra;
using System;

namespace M4M.Benchmarks
{
    [MemoryDiagnoser]
    public class DenseGenomeBenchmarks
    {
        [Params(/*4,*/ 16, 64, 256)]
        public int Size;

        [Params(0, 1, 10)]
        public int T;

        [Params(false)]
        public bool ParallelMath;

        public Matrix<double> BMatrix { get; set; }
        public DenseGenome Genome { get; set; }
        public ModelExecutionContext Context { get; set; }
        public DevelopmentRules DRules { get; set; }

        [GlobalSetup]
        public void Initialize()
        {
            var rnd = new Random(1);
            BMatrix = CreateMatrix.Dense<double>(Size, Size, (i, j) => rnd.NextDouble() * 2.0 - 1.0);
            Genome = DenseGenome.CreateDefaultGenome(Size, customDevelopmentStep: new DefaultDevelopmentStep(Size, 0.0));
            Genome.CopyOverTransMat(BMatrix);

            Context = new ModelExecutionContext(null);
            DRules = TypicalConfiguration.CreateStandardDevelopmentRules(DevelopmentRules.TanhHalf, T);

            if (ParallelMath)
                MathNet.Numerics.Control.UseMultiThreading();
            else
                MathNet.Numerics.Control.UseSingleThread();
        }

        [Benchmark]
        public void Dev()
        {
            Genome.Develop(Context, DRules);
        }
    }
}
