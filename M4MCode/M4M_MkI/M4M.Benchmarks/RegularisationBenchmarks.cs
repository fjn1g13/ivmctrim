﻿using BenchmarkDotNet.Attributes;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Linq;

namespace M4M.Benchmarks
{
    [MemoryDiagnoser]
    public class RegularisationBenchmarks
    {
        [Params(4, 16, 64, 256)]
        public int Size;

        public Matrix<double> BMatrix { get; set; }
        public DenseGenome Genome { get; set; }
        private Vector<double> Ones { get; set; }
        public double R;

        [GlobalSetup]
        public void Initialize()
        {
            var rnd = new Random(1);
            BMatrix = CreateMatrix.Dense<double>(Size, Size, (i, j) => rnd.NextDouble() * 2.0 - 1.0);
            Genome = DenseGenome.CreateDefaultGenome(Size);
            Genome.CopyOverTransMat(BMatrix);
            Ones = CreateVector.Dense(Size, 1.0);
        }

        [Benchmark]
        public void L1_RegHelpers()
        {
            R = RegularisationHelpers.L1Sum(BMatrix);
        }

        [Benchmark]
        public void L1_FPRowCandidate()
        {
            R = RegularisationHelpers.RowL1Sum(BMatrix);
        }

        [Benchmark]
        public void L1_FPColCandidate()
        {
            R = RegularisationHelpers.ColL1Sum(BMatrix);
        }

        //[Benchmark]
        //public void L1_Linq()
        //{
        //    R = BMatrix.Enumerate().Sum(Math.Abs);
        //}

        [Benchmark]
        public void L1_UncachedMul()
        {
            R = BMatrix.PointwiseAbs().LeftMultiply(Ones).DotProduct(Ones);
        }

        //[Benchmark]
        //public void L2_RegHelpers()
        //{
        //    R = RegularisationHelpers.L2Sum(BMatrix);
        //}

        //[Benchmark]
        //public void L2_FPCandidate()
        //{
        //    double d = 0.0;

        //    // fast path arrays from dense storage
        //    if (BMatrix.Storage is MathNet.Numerics.LinearAlgebra.Storage.DenseColumnMajorMatrixStorage<double> dcmms)
        //    {
        //        var data = dcmms.Data;
        //        for (int i = 0; i < data.Length; i++)
        //        {
        //            var v = data[i];
        //            d += v * v;
        //        }
        //    }

        //    R = d;
        //}


        //[Benchmark]
        //public void L2_Linq()
        //{
        //    R = BMatrix.Enumerate().Sum(x => x * x);
        //}

        //[Benchmark]
        //public void L2_UncachedMul()
        //{
        //    R = BMatrix.PointwiseMultiply(BMatrix).LeftMultiply(Ones).DotProduct(Ones);
        //}
    }
}
