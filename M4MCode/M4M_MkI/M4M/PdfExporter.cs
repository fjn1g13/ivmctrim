﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4M
{
    public class PdfPlotExporter : IPlotExporter
    {
        public static PlotExportReport Translate(PdfExportReport report)
        {
            return new PlotExportReport(report.Filename, report.ExportedWidth, report.ExportedHeight);
        }

        public PlotExportReport ExportPlot(string filename, PlotModel model, double size, bool forceAspect, PostProcessPlot postProcessor = null)
        {
            return Translate(PdfExporter.ExportToPdf(model, filename + ".pdf", size, true, forceAspect, postProcessor));
        }

        public PlotExportReport ExportPlot(string filename, PlotModel model, double width, double height, bool forceAspect, PostProcessPlot postProcessor = null)
        {
            return Translate(PdfExporter.ExportToPdf(model, filename + ".pdf", width, height, true, forceAspect, postProcessor));
        }
    }

    public class PdfExportReport
    {
        public PdfExportReport(string filename, double exportedWidth, double exportedHeight)
        {
            Filename = filename;
            ExportedWidth = exportedWidth;
            ExportedHeight = exportedHeight;
        }

        public string Filename { get; }
        public double ExportedWidth { get; }
        public double ExportedHeight { get; }
    }

    public class PdfExporter
    {
        public static System.Diagnostics.Process ViewPdf(string name)
        {
            if (!name.EndsWith(".pdf"))
                name += ".pdf";

            try
            {
                return System.Diagnostics.Process.Start(name);
            }
            catch
            {
                return null; // high quality code
            }
        }

        public static void ExportToPdf(OxyPlot.PlotModel model, string ofname, bool bigger = true, bool noHalf = false, bool forceAspect = false, PostProcessPlot postProcessor = null)
        {
            ExportToPdf(model, ofname, 1.0, bigger, forceAspect, postProcessor);
            if (!noHalf)
                ExportToPdf(model, ofname + "_half", 0.5, bigger, forceAspect, postProcessor);
        }

        public static void DummyExport(OxyPlot.PlotModel model)
        {
            OxyPlot.Pdf.PdfExporter exporter = new OxyPlot.Pdf.PdfExporter();
            double Width = 297 / 25.4 * 72 * 1.0; // A4
            double Height = 210 / 25.4 * 72 * 1.0;

            exporter.Width = Width;
            exporter.Height = Height;

            model.InvalidatePlot(true);

            using (var fs = new System.IO.MemoryStream())
            {
                exporter.Export(model, fs);
            }
        }

        public static PdfExportReport ExportToPdf(OxyPlot.PlotModel model, string ofname, double size, bool bigger, bool forceAspect, PostProcessPlot postProcessor)
        {
            double width = 297 / 25.4 * 72 * size; // A4
            double height = 210 / 25.4 * 72 * size;

            return ExportToPdf(model, ofname, width, height, bigger, forceAspect, postProcessor);
        }

        // this is a C&P job of the ExportToPdf which uses the PdfSharp based PdfExporter
        // a copy appear in M4M.Old: this was introduce to M4M (Framework) so we can finally abandon M4M.Old (but we still want it to compile so we can drag old code out when we need it)
        public static PdfExportReport ExportToPdf(OxyPlot.PlotModel model, string ofname, double width, double height, bool bigger, bool forceAspect, PostProcessPlot postProcessor)
        {
            double fc = 1.2;
            double prc = 2.0;
            if (model.Axes.Count > 2)
                prc = 0.0;

            model.Padding = new OxyPlot.OxyThickness(0.0, 0.0, model.Padding.Right * prc, 0.0); // Yan says there shouldn't be any whitespace
            model.TitleFontSize *= fc;
            model.DefaultFontSize *= fc;

            if (bigger)
            {
                foreach (var s in model.Series)
                {
                    if (s is OxyPlot.Series.LineSeries)
                        ((OxyPlot.Series.LineSeries)s).StrokeThickness *= 2.0;
                }
                model.LegendFontSize *= fc;
            }

            ofname = ofname.EndsWith(".pdf") ? ofname : ofname + ".pdf";

            OxyPlot.Pdf.PdfExporter exporter = new OxyPlot.Pdf.PdfExporter();

            exporter.Width = width;
            exporter.Height = height;

            model.InvalidatePlot(true);

            void doPlot()
            {
                if (System.IO.File.Exists(ofname))
                    System.IO.File.Delete(ofname); // something doesn't like writing onto plots apparently

                using (var fs = new System.IO.FileStream(ofname, System.IO.FileMode.OpenOrCreate))
                {
                    exporter.Export(model, fs);
                }
            }

            void doForceAspect()
            {
                if (model.PlotArea.Width > model.PlotArea.Height)
                {
                    width = width - model.PlotArea.Width + model.PlotArea.Height;
                }
                else
                {
                    height = height + model.PlotArea.Width - model.PlotArea.Height;
                }

                exporter.Width = width;
                exporter.Height = height;
            }

            doPlot();

            if (forceAspect)
            {
                doForceAspect();
                doPlot();
            }

            if (postProcessor != null)
            {
                postProcessor(model);
                doPlot();

                if (forceAspect)
                {
                    doForceAspect();
                    doPlot();
                }
            }

            return new PdfExportReport(ofname, width, height);
        }
    }
}
