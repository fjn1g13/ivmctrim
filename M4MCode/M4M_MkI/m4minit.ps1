param($m4mframework='netcoreapp2.1') # change this if you don't have net core 2.1 (e.g. netcoreapp3.1 or net5)

# run netstateb and m4mb (alias defined below) if you need to build m4m

# $PSScriptRoot = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition # PS2 compatibility
$m4mdir = $PSScriptRoot
$outerdir = ($m4mdir -replace "(.+)(\\|/)M4M_MkI", '$1')
$rawdir = ($m4mdir -replace "(.+)(\\|/)Code(\\|/)Other(\\|/)M4M_MkI", '$1')
Write-Output "RAW Directory: $rawdir"

function m4m()
{
    dotnet "$outerdir/M4M_MkI/M4M.CoreRunner/bin/Release/$m4mframework/M4M.CoreRunner.dll" $args
}

function m4mdbg()
{
    dotnet "$outerdir/M4M_MkI/M4M.CoreRunner/bin/Debug/$m4mframework/M4M.CoreRunner.dll" $args
}

function m4m21()
{
    dotnet "$outerdir/M4M_MkI/M4M.CoreRunner/bin/Release/netcoreapp2.1/M4M.CoreRunner.dll" $args
}

function m4m31()
{
    dotnet "$outerdir/M4M_MkI/M4M.CoreRunner/bin/Release/netcoreapp3.1/M4M.CoreRunner.dll" $args
}

function m4m5()
{
    dotnet "$outerdir/M4M_MkI/M4M.CoreRunner/bin/Release/net5/M4M.CoreRunner.dll" $args
}

function m4mded()
{
    dotnet "$outerdir/M4M_Ded/M4M_Ded.CoreRunner/bin/Release/netcoreapp2.1/M4M_Ded.CoreRunner.dll" $args
}

function m4mexe()
{
    . "$outerdir/M4M_MkI/M4M/bin/x64/Release/M4M.exe" $args
}

function netstateb()
{
    dotnet build "$outerdir/NetState/NetState/NetState.csproj" --configuration Release
    dotnet build "$outerdir/NetState/NetState/NetState.csproj" --configuration Debug
}

function m4mb()
{
    # remember to build netstate before you try to build M4M
    dotnet build "$outerdir/M4M_MkI/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Release --framework $m4mframework /p:M4M_DUAL_TARGET=true
    dotnet build "$outerdir/M4M_MkI/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Debug --framework $m4mframework /p:M4M_DUAL_TARGET=true
}

function m4mb21()
{
    # remember to build netstate before you try to build M4M
    dotnet build "$outerdir/M4M_MkI/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Release --framework netcoreapp2.1 /p:M4M_DUAL_TARGET=true
    dotnet build "$outerdir/M4M_MkI/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Debug --framework netcoreapp2.1 /p:M4M_DUAL_TARGET=true
}

function m4mb31()
{
    dotnet build "$outerdir/M4M_MkI/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Release --framework netcoreapp3.1 /p:M4M_DUAL_TARGET=true
    dotnet build "$outerdir/M4M_MkI/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Debug --framework netcoreapp3.1 /p:M4M_DUAL_TARGET=true
}

function m4mb5()
{
    dotnet build "$outerdir/M4M_MkI/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Release --framework net5 /p:M4M_DUAL_TARGET=true
    dotnet build "$outerdir/M4M_MkI/M4M.CoreRunner/M4M.CoreRunner.csproj" --configuration Debug --framework net5 /p:M4M_DUAL_TARGET=true
}

function m4mclean()
{
    $dirs = (Get-ChildItem -r "genome_t.dat" | ForEach-Object{$_.Directory})

    foreach ($d in $dirs) {
        Write-Output $d
        Get-ChildItem $d | Where-Object {$_.Name -match ".*[a-z]+[0-9]+\(best\).dat"} | Remove-Item
        Get-ChildItem $d | Where-Object {$_.Name -match "sudokusamples[0-9]+.dat"} | Remove-Item
        Get-ChildItem $d | Where-Object {$_.Name -match "ivmcswitches[0-9]+.dat"} | Remove-Item
        Get-ChildItem $d | Where-Object {$_.Name -match ".*epoch[0-9]0*[1-9][0-9]*save.dat"} | Remove-Item
    }
}

$m4mfilepattern = "(.*[a-z]+)([0-9]+)(\(best\)|[a-z]*).dat"
$m4mbestpattern = "(sudokusamples)([0-9]+).dat|(ivmcswitches)([0-9]+).dat|(.*[a-z]+)([0-9]+)\(best\).dat"

class M4MFileInfo {
    M4MFileInfo([string] $prefix, [int] $epoch, [string] $filename) {
        $this.Prefix = $prefix
        $this.Epoch = $epoch
        $this.Filename = $filename
    }
    [string] $Prefix = $null
    [int] $Epoch = -1
    [string] $Filename = $null
}

function m4minfo($filename)
{
    $match = $filename -match $m4mfilepattern;
    if ($match) {
        $prefix = $matches[1]
        $epoch = [Int]$matches[2]
        [M4MFileInfo]::new($prefix, $epoch, $filename)
    } else {
        throw "Unable to match file"
    }
}

function m4mdeepclean()
{
    $dirs = (Get-ChildItem -r | Where-Object {$_.Name -match $m4mbestpattern} | ForEach-Object{$_.Directory.FullName} | Get-unique)

    foreach ($d in $dirs) {
        Write-Output $d

        $groups = (Get-ChildItem $d | Where-Object {$_.Name -match $m4mbestpattern} | ForEach-Object {m4minfo $_} | Group-Object -property "Prefix")

        foreach ($g in $groups) {
            $g.Group | Sort-Object -property Epoch | Select-Object -skiplast 1 | ForEach-Object {"$d/$($_.Filename)"} | Remove-Item
        }
    }
}

function m4mbuild()
{
    # doesn't build M4M (Framework) at the moment becuase M4M.Old is a liability

    netstateb
    m4mb
    m4mb31
    m4mb5
    dotnet build "$outerdir/M4M_Ded/M4M_Ded.CoreRunner/M4M_Ded.CoreRunner.csproj" --configuration Release
    vs # works on my machine, won't work on yours (run from developer powershell instead)
    $projs = ("$outerdir/M4MPlotting/M4MPlotting.sln", "$outerdir/M4MDenseDev/M4MDenseDev.sln", "$outerdir/M4MDenseMod/M4MDenseMod.sln")
    foreach ($proj in $projs) {
        msbuild $proj /p:Configuration=Debug
        msbuild $proj /p:Configuration=Release
        msbuild $proj /p:Configuration=Debug /p:platform=x64
        msbuild $proj /p:Configuration=Release /p:platform=x64
    }
}

function m4mplot()
{
    . "$outerdir/M4MPlotting/M4MPlotting/bin/Debug/M4MPlotting.exe" $args
}

function m4mdensedev()
{
    . "$outerdir/M4MDenseDev/M4MDenseDev/bin/Debug/M4MDenseDev.exe" $args
}

function m4mdensemod()
{
    . "$outerdir/M4MDenseMod/M4MDenseMod/bin/Debug/M4MDenseMod.exe" $args
}

Try
{
  # fails on linux remote systems
  [System.Console]::Title = [System.Console]::Title + " - M4M"
}
Catch
{
}