﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M_Ded
{
    public class MiscPlotting
    {
        public static BoxPlotItem Box(double x, IEnumerable<double> samples)
        {
            var sorted = samples.OrderBy(s => s).ToArray();

            var lq = MathNet.Numerics.Statistics.ArrayStatistics.LowerQuartileInplace(sorted);
            var uq = MathNet.Numerics.Statistics.ArrayStatistics.UpperQuartileInplace(sorted);
            var median = MathNet.Numerics.Statistics.ArrayStatistics.MedianInplace(sorted);

            var lt = lq - (median - lq) * 1.5;
            var ut = uq + (uq - median) * 1.5;

            var low = Math.Min(samples.Where(s => s >= lt).Min(), lq);
            var high = Math.Max(samples.Where(s => s <= ut).Max(), uq);

            var bi = new BoxPlotItem(x, low, lq, median, uq, high);

            foreach (var o in sorted.Where(s => s < low || s > high))
                bi.Outliers.Add(o);

            return bi;
        }

        public static double Median(IReadOnlyList<double> samples)
        {
            int c = samples.Count;
            if (c % 2 == 1)
                return samples[c / 2];
            else
                return (samples[c / 2 - 1] + samples[c / 2]) / 2.0;
        }
    }
}
