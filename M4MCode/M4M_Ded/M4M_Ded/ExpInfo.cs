﻿using M4M;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace M4M_Ded
{
    public class ExpSamples
    {
        public ExpSamples(ExpInfo expInfo, IReadOnlyList<WholeSample<DenseIndividual>> samples, string comment)
        {
            ExpInfo = expInfo ?? throw new ArgumentNullException(nameof(expInfo));
            Samples = samples ?? throw new ArgumentNullException(nameof(samples));
            Comment = comment;
        }

        public ExpInfo ExpInfo { get; }
        public IReadOnlyList<M4M.WholeSample<M4M.DenseIndividual>> Samples { get; }
        public string Comment { get; }
    }

    public class ExpInfo
    {
        public ExpInfo(string dir, string block, string run, int repeat)
        {
            Dir = dir ?? throw new ArgumentNullException(nameof(dir));
            Block = block;
            Run = run;
            Repeat = repeat;
        }

        public string Dir { get; }
        public string Block { get; }
        public string Run { get; }
        public int Repeat { get; }

        public static IEnumerable<ExpInfo> EnumerateExps(string dir, string name, string filePattern)
        {
            var files = System.IO.Directory.GetFiles(dir, filePattern, System.IO.SearchOption.AllDirectories);
            foreach (var f in files)
            {
                var block = Regex.Match(f, $@"(?<={name})[^\\/]+(?=runs)").Groups[0].Value;
                var run = Regex.Match(f, $@"(?<={block}runs)[^\\/]+[\\/]").Groups[0].Value;
                var repeat = int.Parse(Regex.Match(f, @"(?<=[\\/]r)\d+(?=[\\/])").Groups[0].Value);

                yield return new ExpInfo(System.IO.Path.GetDirectoryName(f), block, run, repeat);
            }
        }
    }
}
