﻿using M4M;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M4M_Ded
{
    public class RunonExperimentData
    {
        public RunonExperimentData(string dir, string groupName, string runonExpFile, string expFileName, string postfix)
        {
            Dir = dir;
            GroupName = groupName;

            RunonExp = M4M.PopulationExperiment<M4M.DenseIndividual>.Load(runonExpFile);
            Exps = ExpInfo.EnumerateExps(dir, groupName, expFileName).ToList();
            Runons = EnumerateRunon(Exps, RunonExp, expFileName, postfix).ToArray();
        }

        public string Dir { get; }
        public string GroupName { get; }

        public PopulationExperiment<DenseIndividual> RunonExp { get; }
        public IReadOnlyList<ExpInfo> Exps { get; }
        public ExpSamples[] Runons { get; }

        public static IEnumerable<ExpSamples> EnumerateRunon(IEnumerable<ExpInfo> exps, M4M.PopulationExperiment<M4M.DenseIndividual> runonExp, string expFileName, string postfix)
        {
            var rand = new M4M.CustomMersenneTwister(1);
            var context = new M4M.ModelExecutionContext(rand);

            int i = 0;
            foreach (var e in exps)
            {
                var runonDir = System.IO.Path.Combine(e.Dir, "runon" + postfix);
                if (!System.IO.Directory.Exists(runonDir))
                    System.IO.Directory.CreateDirectory(runonDir);

                var runonWholesampleFile = System.IO.Path.Combine(runonDir, "wholesampleseRunon.dat");
                if (System.IO.File.Exists(runonWholesampleFile))
                {
                    var wsSat = M4M.WholeSample<M4M.DenseIndividual>.LoadWholeSamples(runonWholesampleFile);
                    yield return new ExpSamples(e, wsSat, "runon");
                }
                else
                {
                    var unRunonExpFile = System.IO.Path.Combine(e.Dir, expFileName);
                    var unRunonExp = PopulationExperiment<DenseIndividual>.Load(unRunonExpFile);

                    var exp = PopulationExperimentRunners.PrepareExperiment(unRunonExp.Population, runonExp.PopulationConfig, runonDir, disableAllIO: true, appendTimestamp: false);
                    var feedback = new WholeSampleFeedback<DenseIndividual>(new PopulationExperimentFeedback<DenseIndividual>(), 1, false);
                    var cliPrefix = $"{i}: ";
                    i++;

                    PopulationExperimentRunners.RunEpochs(System.Console.Out, cliPrefix, runonExp.PopulationConfig.ExperimentConfiguration.Epochs, context, exp, feedback.Feedback, -1, -1, nosave: true);
                    var wsRunon = feedback.WholeSamples;

                    exp.Save("runon", false);
                    WholeSample<M4M.DenseIndividual>.SaveWholeSamples(runonWholesampleFile, wsRunon);
                    yield return new ExpSamples(e, wsRunon, "runon");
                }
            }
        }
    }
}
