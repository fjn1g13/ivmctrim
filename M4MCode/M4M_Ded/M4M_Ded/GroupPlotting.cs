﻿using M4M;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M_Ded
{

    public class GroupPlotting
    {
        public static PlotModel PlotBockTracees(IEnumerable<ExpSamples> exps)
        {
            var cts = MakeBlockTracees(exps);
            var plot = M4M.TrajectoryPlotting.PrepareTrajectoriesPlot("Fitness Trajectories", "Epoch", "Fitness", null, 0, 500, "x", "y", null, false, false, false);
            var multiPlot = new PlotModel();
            foreach (var ct in cts.OrderBy(ct => ct.Key))
                M4M.EvolvabilityTraces.PlotArea(plot, ct.Value, 1);
            return plot;
        }

        public static PlotModel PlotTerminalFitnessDistributions(IEnumerable<ExpSamples> exps, string blockTitle, Func<string, string> blockFormatter)
        {
            blockFormatter = blockFormatter ?? (x => x);

            var plot = new PlotModel() { Title = "Terminal Fitness Distributions" };
            var caxis = new CategoryAxis() { Title = blockTitle, Position = AxisPosition.Bottom };
            plot.Axes.Add(caxis);
            plot.Axes.Add(new LinearAxis() { Title = "Terminal Fitness", Position = AxisPosition.Left });

            var bp = new BoxPlotSeries();
            int i = 0;
            foreach (var exp in exps.GroupBy(e => e.ExpInfo.Block).OrderBy(ct => ct.Key))
            {
                caxis.Labels.Add(blockFormatter(exp.Key));
                var bi = MiscPlotting.Box(i++, exp.Select(ws => ws.Samples.Last().Judgements[0].Judgement.Benefit));
                bp.Items.Add(bi);
            }
            //bp.BoxWidth = 0.8;
            plot.Series.Add(bp);

            return plot;
        }

        public static PlotModel PlotMeanFitnessDistributions(IEnumerable<ExpSamples> exps, string blockTitle, Func<string, string> blockFormatter)
        {
            blockFormatter = blockFormatter ?? (x => x);

            var plot = new PlotModel() { Title = "Mean Fitness Distributions" };
            var caxis = new CategoryAxis() { Title = blockTitle, Position = AxisPosition.Bottom };
            plot.Axes.Add(caxis);
            plot.Axes.Add(new LinearAxis() { Title = "Mean Fitness", Position = AxisPosition.Left });

            var bp = new BoxPlotSeries();
            int i = 0;
            foreach (var exp in exps.GroupBy(e => e.ExpInfo.Block).OrderBy(ct => ct.Key))
            {
                caxis.Labels.Add(blockFormatter(exp.Key));
                var bi = MiscPlotting.Box(i++, exp.Select(ws => ws.Samples.Average(s => s.Judgements[0].Judgement.Benefit)));
                bp.Items.Add(bi);
            }
            //bp.BoxWidth = 0.8;
            plot.Series.Add(bp);

            return plot;
        }

        static Dictionary<string, M4M.ColourfulTraces> MakeBlockTracees(IEnumerable<ExpSamples> exps)
        {
            var grps = exps.GroupBy(e => e.ExpInfo.Block).ToList();
            var dict = new Dictionary<string, M4M.ColourfulTraces>();
            var colors = new M4M.TrajectoryColours1D(OxyColors.LightBlue, OxyColors.Red).Colours(grps.Count);

            int ci = 0;
            foreach (var grp in grps)
            {
                int sr = grp.First().Samples[1].Epoch - grp.First().Samples[0].Epoch;
                var samples = grp.Select(r => r.Samples.Select(ws => ws.Judgements[0].Judgement.Benefit).ToArray()).ToList();
                var ct = new M4M.ColourfulTraces($"Q={grp.Key}", colors[ci++], samples, sr, 0);
                dict.Add(grp.Key, ct);
            }

            return dict;
        }
    }
}
