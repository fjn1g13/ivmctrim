﻿using M4M;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace M4M_Ded
{
    public class SaturatedExperimentData
    {
        public SaturatedExperimentData(string dir, string groupName, string satExpFile, string wholesampleFilename, ProjectionMode projectionMode, bool saturateExistingTarget)
        {
            Dir = dir;
            GroupName = groupName;
            ProjectionMode = projectionMode;

            SatExp = M4M.PopulationExperiment<M4M.DenseIndividual>.Load(satExpFile);
            Exps = ExpInfo.EnumerateExps(dir, groupName, wholesampleFilename + ".dat").ToList();
            Sats = EnumerateSaturated(Exps, SatExp, wholesampleFilename, projectionMode, saturateExistingTarget).ToArray();
        }

        public string Dir { get; }
        public string GroupName { get; }
        public ProjectionMode ProjectionMode { get; }

        public PopulationExperiment<DenseIndividual> SatExp { get; }
        public IReadOnlyList<ExpInfo> Exps { get; }
        public ExpSamples[] Sats { get; }

        public static PopulationExperiment<T> ReplaceTargets<T>(PopulationExperiment<T> exp, ITarget[] targets, FileStuff fileStuff) where T : IIndividual<T>
        {
            var popConfig = exp.PopulationConfig;
            var config = exp.PopulationConfig.ExperimentConfiguration;
            var newConfig = new ExperimentConfiguration(config.Size, targets, config.TargetCycler, config.Epochs, config.GenerationsPerTargetPerEpoch, config.InitialStateResetProbability, config.InitialStateResetRange, config.ShuffleTargets, config.DevelopmentRules, config.ReproductionRules, config.JudgementRules);
            var newPopConfig = new PopulationExperimentConfig<T>(newConfig, popConfig.SelectorPreparer, popConfig.EliteCount, popConfig.HillclimberMode, popConfig.PopulationEndTargetOperation, popConfig.PopulationResetOperation, popConfig.CustomPopulationSpinner);
            var newExp = new PopulationExperiment<T>(exp.Population, newPopConfig, fileStuff);
            return newExp;
        }

        public static PopulationExperiment<T> Saturate<T>(PopulationExperiment<T> exp, ITarget[] targets,  double min, double threshold, double max, FileStuff fileStuff) where T : IIndividual<T>
        {
            var satTargets = targets.Select(t => new SaturationTarget(t, min, threshold, max)).ToArray();
            return ReplaceTargets(exp, satTargets, fileStuff);
        }

        public static IEnumerable<ExpSamples> EnumerateSaturated(IEnumerable<ExpInfo> exps, M4M.PopulationExperiment<M4M.DenseIndividual> satExp, string wholesampleFilename, ProjectionMode projectionMode, bool saturateExistingTarget)
        {
            var rand = new M4M.CustomMersenneTwister(1);
            var context = new M4M.ModelExecutionContext(rand);
            IWholeSampleProjector<DenseIndividual> projector = saturateExistingTarget
                ? null 
                : new M4M.BasicExtractorWholeSampleProjectorPreparer(projectionMode, false).PrepareProjector(satExp);

            foreach (var e in exps)
            {
                var satPath = System.IO.Path.Combine(e.Dir, wholesampleFilename + "sat.dat");
                if (System.IO.File.Exists(satPath))
                {
                    var wsSat = M4M.WholeSample<M4M.DenseIndividual>.LoadWholeSamples(satPath);
                    yield return new ExpSamples(e, wsSat, "saturated");
                }
                else
                {
                    var unSatPath = System.IO.Path.Combine(e.Dir, wholesampleFilename + ".dat");
                    var ws = M4M.WholeSample<M4M.DenseIndividual>.LoadWholeSamples(unSatPath);

                    if (saturateExistingTarget)
                    {
                        var targets = new[] { ws.Last().Target };
                        var satTargetTemplate = (SaturationTarget)satExp.PopulationConfig.ExperimentConfiguration.Targets[0];
                        var fileStuff = FileStuff.CreateNow(e.Dir, "", "", false, false);
                        var exp = Saturate(satExp, targets, satTargetTemplate.Min, satTargetTemplate.Threshold, satTargetTemplate.Max, fileStuff);
                        projector = new M4M.BasicExtractorWholeSampleProjectorPreparer(projectionMode, false).PrepareProjector(exp);
                        exp.Save("sat", false);
                        Console.WriteLine("SaturateExistingTarget: " + exp.FileStuff.OutDir);
                    }

                    var wsSat = M4M.WholeSampleProjectionHelpers.Project(System.Console.Out, context, ws, projector, false, false);
                    M4M.WholeSample<M4M.DenseIndividual>.SaveWholeSamples(satPath, wsSat);
                    yield return new ExpSamples(e, wsSat, "saturated");
                }
            }
        }

        public static void ProcessWholeSamples(IEnumerable<ExpSamples> sats, M4M.PopulationExperiment<M4M.DenseIndividual> satExp, Func<string, string> blockFormatter)
        {
            blockFormatter = blockFormatter ?? (x => x);

            var satTarget = (M4M.SaturationTarget)satExp.PopulationConfig.ExperimentConfiguration.Targets[0];

            if (satTarget.Target is M4M.Epistatics.CorrelationMatrixTarget cmTarget)
            {
                var bMax = cmTarget.CorrelationMatrix.Enumerate().Count(x => x != 0.0);

                foreach (var e in sats)
                {
                    var last = e.Samples.Last();
                    var bt = last.Judgements[0].Judgement.Benefit;
                    var flatLine = e.Samples.AsEnumerable().Reverse().TakeWhile(s => s.Judgements[0].Judgement.Benefit == bt);
                    Console.WriteLine($"{blockFormatter(e.ExpInfo.Block)}/runs{e.ExpInfo.Run}/r{e.ExpInfo.Repeat}: {last.Epoch - flatLine.Last().Epoch} epochs at {bt}/{bMax}");
                }
            }
            else if (satTarget.Target is M4M.Epistatics.IIvmcProperTarget ivmcProperTarget)
            {
                // forget it
            }
        }
    }
}