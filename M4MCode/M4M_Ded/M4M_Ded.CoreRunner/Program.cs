﻿using M4M;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M4M_Ded.CoreRunner
{
    class Program
    {
        static void Main(string[] args)
        {
//args = @"m4mded ResetFitnessTraces -expfile C:\M4MExperiments\Cs\CsRiVaryQNoDelta\CsRiVaryQNoDelta\CsRiVaryQNoDeltaQ10k\r0\epoch2000save.dat count=10 out=fitnessTraces.dat".Split(' ');

            var clips = new CliParams();
            clips.Consume(args);

            if (!clips.IsSet("quiet"))
            {
                Console.WriteLine("~~ M4M Ded Core Running ~~");
            }

            if (clips.IsSet("AreaTrajectoryTraces"))
            {
                AreaTrajectoryTraces(clips);
            }

            if (clips.IsSet("ResetFitnessTraces"))
            {
                ResetFitnessTraces(clips);
            }

            if (clips.IsSet("SatBlock"))
            {
                SatBlock(clips);
            }

            if (clips.IsSet("RunonBlock"))
            {
                RunonBlock(clips);
            }

            bool doNotListUnobserved = clips.IsSet("quiet") || clips.IsSet("donotlistunobserved");
            if (!doNotListUnobserved)
            {
                var unobserved = clips.EnumerateUnobsered().ToArray();
                if (unobserved.Length > 0)
                    Console.WriteLine("Unobserved CliParams: " + string.Join(", ", unobserved));
            }
        }

        public static void AreaTrajectoryTraces(CliParams clips)
        {
            var tracesFiles = clips.Get("AreaTrajectoryTraces", s => s.Split(';'));
            var labels = clips.Get("label", s => s.Split(';'));
            var colors = clips.Get("color", s => s.Split(';').Select(CliPlotHelpers.ParseColour).ToArray());
            var title = clips.Get("title");

            double? ymin = clips.Get("ymin", s => (double?)double.Parse(s), null);
            double? ymax = clips.Get("ymax", s => (double?)double.Parse(s), null);
            
            string xAxisKey = clips.Get("xaxiskey", "x");
            string yAxisKey = clips.Get("yaxiskey", "y");

            bool logX = clips.Get("logX", bool.Parse, false);
            bool logY = clips.Get("logY", bool.Parse, false);

            var plot = TrajectoryPlotting.PrepareTrajectoriesPlot(title, "Generation", $"(Qualify title:{yAxisKey})", null, ymin, ymax, xAxisKey, yAxisKey, null, logX, logY, false);

            var cts = new ColourfulTraces[tracesFiles.Length];
            for (int i = 0; i < tracesFiles.Length; i++)
            {
                var traj = Analysis.LoadTrajectories(tracesFiles[i], out var samplePeriod);
                cts[i] = new ColourfulTraces(labels[i], colors[i], traj, samplePeriod, 0);
                EvolvabilityTraces.PlotArea(plot, cts[i], 1);
            }

            SimplePdfPlotExporter.ExportToPdf(plot, "plot.pdf", 300, 300, false, false, null);
        }

        public static void ResetFitnessTraces(CliParams clips)
        {
            var expFile = clips.Get("ExpFile");
            var count = clips.Get("Count", int.Parse, 1000);
            var epochCount = clips.Get("EpochCount", int.Parse, 1);
            var seed = clips.Get("seed", int.Parse, 1);
            var outFile = clips.Get("out");
            var saturate = clips.Get("saturate", bool.Parse, false);
            var saturateMin = clips.Get("saturatemin", double.Parse, -1.0);
            var saturateThreshold = clips.Get("saturatethreshold", double.Parse, 0.0);
            var saturateMax = clips.Get("saturatemax", double.Parse, 1.0);

            var rand = new CustomMersenneTwister(seed);
            var context = new ModelExecutionContext(rand);

            var trajectories = PopulationExperimentHelpers.LoadUnknownType(expFile, new TracesRunnner(count, epochCount, context, saturate, saturateMin, saturateThreshold, saturateMax));
            Analysis.SaveTrajectories(outFile, trajectories, 1);
        }

        class TracesRunnner : IUnknownPopulationExperimentTypeReceiver<double[][]>
        {
            public TracesRunnner(int resetCount, int epochCount, ModelExecutionContext context, bool saturate, double saturationMin, double saturationThreshold, double saturationMax)
            {
                ResetCount = resetCount;
                EpochCount = epochCount;
                Context = context;
                Saturate = saturate;
                SaturationMin = saturationMin;
                SaturationThreshold = saturationThreshold;
                SaturationMax = saturationMax;
            }

            public int ResetCount { get; }
            public int EpochCount { get; }
            public ModelExecutionContext Context { get; }
            public bool Saturate { get; }
            public double SaturationMin { get; }
            public double SaturationThreshold { get; }
            public double SaturationMax { get; }

            public double[][] Receive<TIndividual>(PopulationExperiment<TIndividual> populationExperiment) where TIndividual : IIndividual<TIndividual>
            {
                var config = populationExperiment.PopulationConfig.ExperimentConfiguration;

                IEnumerable<Population<TIndividual>> populations()
                {
                    for (int i = 0; i < ResetCount; i++)
                    {
                        var pop = populationExperiment.Population.Clone();
                        populationExperiment.PopulationConfig.PopulationResetOperation.Reset(Context, pop, config.InitialStateResetRange, config.DevelopmentRules, config.ReproductionRules, config.Targets[0]);
                        yield return pop;
                    }
                }

                ITarget currentTarget = null;
                SaturationTarget satTarget = null;
                
                double judge(ITarget target, IReadOnlyList<IndividualJudgement<TIndividual>> individualJudgements)
                {
                    if (Saturate)
                    {
                        if (currentTarget != target)
                        {
                            currentTarget = target;
                            satTarget = new SaturationTarget(target, SaturationMin, SaturationThreshold, SaturationMax);
                        }

                        return individualJudgements.Max(ij => ij.Individual.Judge(config.JudgementRules, satTarget).CombinedFitness);
                    }
                    else
                    {
                        return individualJudgements.Max(ij => ij.Judgement.CombinedFitness);
                    }
                }

                return PopulationTrace.TracesToTrajectories(Context, populationExperiment.Clone(null), populations(), judge, EpochCount);
            }
        }

        public static void SatBlock(CliParams clips)
        {
            var dir = clips.Get("SatBlock");
            var groupName = clips.Get("GroupName");
            var satExpFile = clips.Get("SatExpFile");
            var wholesampleFilename = clips.Get("WholesampleFilename");
            var blockTitle = clips.Get("BlockTitle");
            var projectionMode = clips.Get("ProjectionMode", CliProject.ParseProjectionMode, ProjectionMode.EvalOnly);
            var saturateExistingTarget = clips.Get("SaturateExistingTarget", bool.Parse, false);

            SatBlock(dir.Split(';'), groupName.Split(';'), satExpFile, wholesampleFilename, blockTitle, projectionMode, saturateExistingTarget);
        }

        // TODO: this shuold really be broken up into one task that produces the saturated wholesamples, and the other tasks which do the work
        // These could appear as a CliBlockAnalysis class or something (err... 2 of them are plotters...)
        public static void SatBlock(IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string satExpFile, string wholesampleFilename, string blockTitle, ProjectionMode projectionMode, bool saturateExistingTarget)
        {
            if (dirs.Count == 0)
                throw new ArgumentException("Must provide atleast one Dir and GroupName");
            if (dirs.Count != groupNames.Count)
                throw new ArgumentException("Must provide the same number of Dirs and groupNames");

            var sats = new List<ExpSamples>();

            for (int i = 0; i < dirs.Count; i++)
            {
                var sed = new SaturatedExperimentData(dirs[i], groupNames[i], satExpFile, wholesampleFilename, projectionMode, saturateExistingTarget);
                sats.AddRange(sed.Sats.OrderBy(e => e.ExpInfo.Block));
                SaturatedExperimentData.ProcessWholeSamples(sats, sed.SatExp, null);
            }

            var traceesplot = GroupPlotting.PlotBockTracees(sats);
            SimplePdfPlotExporter.ExportToPdf(traceesplot, "SatTracees.pdf", 300, 300, false, false, null);

            var boxplot = GroupPlotting.PlotTerminalFitnessDistributions(sats, blockTitle, null);
            SimplePdfPlotExporter.ExportToPdf(boxplot, "SatBoxPlots.pdf", 300, 300, false, false, null);
        }

        public static void RunonBlock(CliParams clips)
        {
            var dir = clips.Get("RunonBlock");
            var groupName = clips.Get("GroupName");
            var runonExpFile = clips.Get("RunonExpFile");
            var expFilename = clips.Get("ExpFilename");
            var blockTitle = clips.Get("BlockTitle");
            var postfix = clips.Get("Postfix");

            var width = clips.Get("width", double.Parse, 600);
            var height = clips.Get("height", double.Parse, 300);

            double[] thresholds = clips.Get("threshold", s => s.Split(';').Select(double.Parse).ToArray(), new double[0]);

            //
            var runons = RunonBlock(dir.Split(';'), groupName.Split(';'), runonExpFile, expFilename, postfix);

            var boxplot = GroupPlotting.PlotMeanFitnessDistributions(runons, blockTitle, null);
            foreach (var threshold in thresholds)
            {
                boxplot.Annotations.Add(new OxyPlot.Annotations.LineAnnotation() { Type = OxyPlot.Annotations.LineAnnotationType.Horizontal, Y = threshold, Color = OxyPlot.OxyColors.Gray });
            }
            SimplePdfPlotExporter.ExportToPdf(boxplot, "RunonBoxPlots.pdf", width, height, false, false, null);

            foreach (var threshold in thresholds)
            {
                Console.WriteLine($"# Threshold {threshold}");

                foreach (var exp in runons.GroupBy(e => e.ExpInfo.Block).OrderBy(ct => ct.Key))
                {
                    var passRate = (double)exp.Count(ws => ws.Samples.Average(s => s.Judgements[0].Judgement.Benefit) >= threshold) / exp.Count();
                    Console.WriteLine(exp.Key + ": " + passRate);
                }

                Console.WriteLine();
            }
        }

        public static List<ExpSamples> RunonBlock(IReadOnlyList<string> dirs, IReadOnlyList<string> groupNames, string runonExpFile, string expFilename, string postfix)
        {
            if (dirs.Count == 0)
                throw new ArgumentException("Must provide atleast one Dir and GroupName");
            if (dirs.Count != groupNames.Count)
                throw new ArgumentException("Must provide the same number of Dirs and groupNames");

            var runons = new List<ExpSamples>();

            for (int i = 0; i < dirs.Count; i++)
            {
                var runon = new RunonExperimentData(dirs[i], groupNames[i], runonExpFile, expFilename, postfix);
                runons.AddRange(runon.Runons.OrderBy(e => e.ExpInfo.Block));
            }

            return runons;
        }
    }
}
