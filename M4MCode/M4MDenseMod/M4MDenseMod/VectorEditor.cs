﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using M4M;

namespace M4MDenseMod
{
    public delegate void FieldChanged<T>(T oldValue, T newValue);

    public partial class VectorEditor : DoubleBufferedPanel
    {
        private ObservableVector<double> _Vector;
        public ObservableVector<double> Vector
        {
            get => _Vector;
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value));

                if (_Vector != value)
                {
                    var old = _Vector;
                    if (old != null)
                        old.Modified -= _Vector_Modified;

                    _Vector = value;
                    _Vector.Modified += _Vector_Modified;
                    VectorChanged?.Invoke(old, _Vector);
                    Invalidate();
                }
            }
        }

        private void _Vector_Modified(IReadOnlyList<int> changed)
        {
            Invalidate();
        }

        public event ObservableVectorChanged<double> VectorChanged;
        public event FieldChanged<int> DisplayWidthChanged;

        private int _DisplayWidth = 1;
        public int DisplayWidth
        {
            get => _DisplayWidth;
            set
            {
                if (_DisplayWidth != value)
                {
                    var oldWidth = _DisplayWidth;
                    _DisplayWidth = value;
                    DisplayWidthChanged(oldWidth, value);
                    Invalidate();
                }
            }
        }

        public IColours _Colours = null;
        public IColours Colours
        {
            get => _Colours;
            set
            {
                if (_Colours != value)
                {
                    _Colours = Colours;
                    Invalidate();
                }
            }
        }

        public int DisplayHeight => (int)(Math.Ceiling((double)Vector.Length / DisplayWidth));

        private HashSet<IVectorAnnotator> Annotators { get; } = new HashSet<IVectorAnnotator>();

        public IEnumerable<IVectorAnnotator> EnumerateAnnotators()
        {
            return Annotators;
        }

        public void AddAnnotator(IVectorAnnotator annotator)
        {
            Annotators.Add(annotator);
            Invalidate();
        }

        public void RemoveAnnotator(IVectorAnnotator annotator)
        {
            Annotators.Remove(annotator);
            Invalidate();
        }

        public void ClearAnnotators()
        {
            Annotators.Clear();
            Invalidate();
        }

        public VectorEditor()
        {
            // pointless defaults
            _Vector = new ObservableVector<double>(16, 0.0);
            _DisplayWidth = 4;
            _Colours = new GreyColours();

            InitializeComponent();
        }

        public VectorEditor(ObservableVector<double> vector, int displayWidth, IColours colours)
        {
            _Vector = vector;
            _DisplayWidth = displayWidth;
            _Colours = colours;

            InitializeComponent();
        }

        private void VectorEditor_Load(object sender, EventArgs e)
        {
            Invalidate();
        }

        private int CellDim;
        private Point TopLeft;

        protected virtual void DoLayout(Rectangle clipRectangle)
        {
            CellDim = Math.Min(clipRectangle.Width / DisplayWidth, clipRectangle.Height / DisplayHeight);
            TopLeft = new Point(
                (clipRectangle.Width - CellDim * DisplayWidth) / 2,
                (clipRectangle.Height - CellDim * DisplayHeight) / 2);
        }

        public Rectangle GetRectangle(int i)
        {
            int x = i % DisplayWidth;
            int y = i / DisplayWidth;

            return new Rectangle(TopLeft.X + x * CellDim, TopLeft.Y + y * CellDim, CellDim, CellDim);
        }

        public Point GetCenter(int i)
        {
            int x = i % DisplayWidth;
            int y = i / DisplayWidth;

            return new Point(TopLeft.X + x * CellDim + CellDim / 2, TopLeft.Y + y * CellDim + CellDim / 2);
        }

        public bool HitTest(Point p, out int index)
        {
            for (int i = 0; i < Vector.Length; i++)
            {
                if (GetRectangle(i).Contains(p))
                {
                    index = i;
                    return true;
                }
            }

            index = -1;
            return false;
        }

        protected virtual void VectorEditor_Paint(object sender, PaintEventArgs e)
        {
            DoLayout(e.ClipRectangle);

            var g = e.Graphics;

            for (int i = 0; i < Vector.Length; i++)
            {
                using (var b = new SolidBrush(Colours.GetColor(Vector[i])))
                {
                    g.FillRectangle(b, GetRectangle(i));
                }
            }

            foreach (var a in Annotators)
                a.Annotate(g, e.ClipRectangle, this);
        }

        private void VectorEditor_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}
