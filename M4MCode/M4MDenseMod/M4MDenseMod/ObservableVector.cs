﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4MDenseMod
{
    public delegate void ObservableVectorModified(IReadOnlyList<int> changed);
    public delegate void ObservableVectorChanged<T>(ObservableVector<T> previous, ObservableVector<T> current);

    public class ObservableVector<T>
    {
        public ObservableVector(T[] data)
        {
            Data = data?.ToArray() ?? throw new ArgumentNullException(nameof(data));
        }

        public ObservableVector(int length, T value)
        {
            Data = new T[length];
            for (int i = 0; i < length; i++)
                Data[i] = value;
        }

        private T[] Data { get; }

        public event ObservableVectorModified Modified;

        public T this[int i]
        {
            get => Data[i];
            set
            {
                if (i < 0 || i >= Length)
                    throw new ArgumentOutOfRangeException(nameof(i));

                if (!Data[i].Equals(value))
                {
                    Data[i] = value;
                    Modified?.Invoke(new[] { i });
                }
            }
        }

        public T[] ToArray() => Data.ToArray();
        public int Length => Data.Length;
    }

}
