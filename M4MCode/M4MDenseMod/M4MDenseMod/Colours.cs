﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4MDenseMod
{
    public interface IColours
    {
        Color GetColor(double value);
    }

    public class GreyColours : IColours
    {
        public GreyColours()
        {
            // defaults
        }

        public GreyColours(double max, double min)
        {
            Max = max;
            Min = min;
        }

        public GreyColours(bool whiteHot, Color highColour, Color lowColour, double max, double min)
        {
            WhiteHot = whiteHot;
            HighColour = highColour;
            LowColour = lowColour;
            Max = max;
            Min = min;
        }

        public bool WhiteHot { get; } = true;

        public Color HighColour { get; } = Color.Red;
        public Color LowColour { get; } = Color.Blue;

        public double Max { get; } = 1;
        public double Min { get; } = -1;

        public Color GetColor(double value)
        {
            double v = (value - Min) / (Max - Min);
            if (v > 1.0)
                return HighColour;
            if (v < 0.0)
                return LowColour;

            byte b = (byte)Math.Round(v * 255);
            return Color.FromArgb(b, b, b);
        }
    }
}
