﻿using M4M;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M4MDenseMod
{
    public interface IVectorAnnotator
    {
        void Annotate(Graphics g, Rectangle clipRectangle, VectorEditor editor);
    }

    public class LocalMatrixBoundryAnnotator : IVectorAnnotator
    {
        public bool Enabled { get; set; } = true;

        public LocalMatrixBoundryAnnotator(Matrix<double> constraintMatrix)
        {
            ConstraintMatrix = constraintMatrix ?? throw new ArgumentNullException(nameof(constraintMatrix));
        }

        public MathNet.Numerics.LinearAlgebra.Matrix<double> ConstraintMatrix { get; }

        public void Annotate(Graphics g, Rectangle clipRectangle, VectorEditor editor)
        {
            if (!Enabled)
                return;

            var mat = ConstraintMatrix;
            var mih = new MatrixIndexHelper(editor.DisplayWidth, editor.DisplayHeight); // assume square

            using (var pen = new Pen(Color.Red, 2))
            {
                foreach (var unhappy in M4M.Epistatics.StandardCorrelationMatrixTargets.EnumerateUnhappyCorrelations(mat, MathNet.Numerics.LinearAlgebra.CreateVector.DenseOfArray(editor.Vector.ToArray())))
                {
                    int i = unhappy.Row;
                    int j = unhappy.Col;

                    var ci = editor.GetCenter(i);
                    var cj = editor.GetCenter(j);

                    int r = ci.Y;
                    int c = ci.X;
                    int r2 = cj.Y;
                    int c2 = cj.X;
                    int dr = r2 - r;
                    int dc = c2 - c;

                    int x0 = c + dc / 2 + Math.Abs(dr) / -2;
                    int x1 = c + dc / 2 + Math.Abs(dr) / 2;
                    int y0 = r + dr / 2 + Math.Abs(dc) / -2;
                    int y1 = r + dr / 2 + Math.Abs(dc) / 2;

                    g.DrawLine(pen, x0, y0, x1, y1);
                }
            }
        }
    }
}
