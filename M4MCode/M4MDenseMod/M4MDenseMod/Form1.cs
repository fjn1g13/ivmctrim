﻿using M4M;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M4MDenseMod
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            // VERY important
            MathNet.Numerics.Control.UseSingleThread();

            InitializeComponent();

            Editor = new VectorEditor();
            Editor.AllowDrop = true;
            Editor.Dock = DockStyle.Fill;
            Editor.DragDrop += Editor_DragDrop;
            Editor.DragEnter += Editor_DragEnter;
            Editor.MouseDown += Editor_MouseDown;
            this.Controls.Add(Editor);
            Editor.BringToFront();
            Editor.DisplayWidthChanged += (o, n) => DisplayWidthNumeric.Value = n;
        }

        private PopulationExperiment<DenseIndividual> _Experiment;
        public PopulationExperiment<DenseIndividual> Experiment
        {
            get => _Experiment;
            set
            {
                _Experiment = value;
                var individual = value.Population.PeekAll()[0];
                Editor.Vector = new ObservableVector<double>(individual.Genome.InitialState.ToArray());
            }
        }

        public int Seed
        {
            get => (int)SeedNumeric.Value;
            set
            {
                this.Invoke(new Action(() => { SeedNumeric.Value = value; }));
            }
        }

        public bool FixSeed
        {
            get => FixSeedCheck.Checked;
            set
            {
                FixSeedCheck.Checked = value;
            }
        }

        private bool _LocalViolationBoundries = false;
        public bool LocalViolationBoundries
        {
            get => _LocalViolationBoundries;
            set
            {
                if (value != LocalViolationBoundries)
                {
                    bool invalidate = false;

                    _LocalViolationBoundries = value;
                    LocalViolationBoundriesCheck.Checked = value;
                    if (LocalMatrixBoundryAnnotator != null)
                    {
                        LocalMatrixBoundryAnnotator.Enabled = value;
                        invalidate = true;
                    }

                    Editor.Invalidate();
                }
            }
        }

        // annotators
        private LocalMatrixBoundryAnnotator LocalMatrixBoundryAnnotator = null; // may be null

        public int NextSeed()
        {
            int res = Seed;
            if (!FixSeed)
                Seed++;
            return res;
        }

        private void Editor_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files)
            {
                try
                {
                    if (System.IO.File.Exists(file))
                    {
                        var fileName = System.IO.Path.GetFileName(file);

                        if (fileName.StartsWith("epoch"))
                        {
                            Editor.ClearAnnotators();
                            Experiment = PopulationExperiment<DenseIndividual>.Load(file);
                            if (!FixWidthCheck.Checked)
                                Editor.DisplayWidth = (int)Math.Ceiling(Math.Sqrt(Editor.Vector.Length));
                            var target0 = M4M.CliPlotHelpers.UnwrapTarget(Experiment.PopulationConfig.ExperimentConfiguration.Targets[0]);
                            if (target0 is M4M.Epistatics.CorrelationMatrixTarget cmt)
                            {
                                Editor.AddAnnotator(LocalMatrixBoundryAnnotator = new LocalMatrixBoundryAnnotator(cmt.CorrelationMatrix));
                                LocalMatrixBoundryAnnotator.Enabled = LocalViolationBoundries;
                            }
                            else
                            {
                                LocalMatrixBoundryAnnotator = null;
                            }
                        }
                    }
                }
                catch
                {
                    // TODO: do something
                }
            }
        }

        private void Editor_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        // we'll be lazy and do everything manually for the moment
        private void Editor_MouseDown(object sender, MouseEventArgs e)
        {
            if (Editor.HitTest(e.Location, out int i))
            {
                Editor.Vector[i] = -Editor.Vector[i];
            }
        }

        public PopulationExperiment<DenseIndividual> PrepareModifiedExperiment()
        {
            int seed = NextSeed();
            var newIs = MathNet.Numerics.LinearAlgebra.CreateVector.DenseOfArray(Editor.Vector.ToArray());
            var drules = Experiment.PopulationConfig.ExperimentConfiguration.DevelopmentRules;
            var context = new ModelExecutionContext(new CustomMersenneTwister(seed));
            var individuals = Experiment.Population.PeekAll().Select(ij => DenseIndividual.Develop(ij.Genome.Clone(context, newInitialState: newIs), context, drules, ij.Epigenetic));
            var pop = new Population<DenseIndividual>(individuals);
            var exp = new PopulationExperiment<DenseIndividual>(pop, Experiment.PopulationConfig, Experiment.FileStuff, Experiment.Epoch, Experiment.TotalGenerationCount);
            return exp;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            var res = SaveFileDialog.ShowDialog();
            if (res == DialogResult.OK)
            {
                var exp = PrepareModifiedExperiment();
                var filename = SaveFileDialog.FileName;
                exp.SaveTo(filename);
            }
        }

        private async void StepEpochButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

            void start()
            {
                StepEpochButton.Enabled = false;
                sw.Restart();
            }

            void go()
            {
                void judged(FileStuff stuff, IReadOnlyList<IndividualJudgement<DenseIndividual>> populationJudgements, int epochCount, long generationCount, ITarget target)
                {
                    if (sw.ElapsedMilliseconds > 100)
                    {
                        this.BeginInvoke(new Action(() =>
                        {
                            Editor.Vector = new ObservableVector<double>(populationJudgements[0].Individual.Genome.InitialState.ToArray());
                            StepEpochButton.Text = "" + generationCount;
                            sw.Restart();
                        }));
                        sw.Restart();
                    }
                }

                var exp = PrepareModifiedExperiment();

                var context = new ModelExecutionContext(new CustomMersenneTwister(NextSeed()));
                var feedback = new PopulationExperimentFeedback<DenseIndividual>();
                feedback.Judged.Register(judged);
                exp.RunEpoch(context, feedback);

                Experiment = exp;
            }

            void finish()
            {
                sw.Stop();
                StepEpochButton.Enabled = true;
                StepEpochButton.Text = "Step Epoch";
            }

            start();
            await Task.Run(new Action(go));
            finish();
        }

        private void LocalViolationBoundriesCheck_CheckedChanged(object sender, EventArgs e)
        {
            LocalViolationBoundries = LocalViolationBoundriesCheck.Checked;
        }

        private void DisplayHeightNumeric_ValueChanged(object sender, EventArgs e)
        {
            Editor.DisplayWidth = (int)DisplayWidthNumeric.Value;
        }
    }
}
