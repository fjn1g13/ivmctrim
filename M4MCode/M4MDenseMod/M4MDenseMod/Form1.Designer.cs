﻿namespace M4MDenseMod
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.FixWidthCheck = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DisplayWidthNumeric = new System.Windows.Forms.NumericUpDown();
            this.LocalViolationBoundriesCheck = new System.Windows.Forms.CheckBox();
            this.FixSeedCheck = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SeedNumeric = new System.Windows.Forms.NumericUpDown();
            this.StepEpochButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.ControlPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayWidthNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeedNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // ControlPanel
            // 
            this.ControlPanel.Controls.Add(this.FixWidthCheck);
            this.ControlPanel.Controls.Add(this.label2);
            this.ControlPanel.Controls.Add(this.DisplayWidthNumeric);
            this.ControlPanel.Controls.Add(this.LocalViolationBoundriesCheck);
            this.ControlPanel.Controls.Add(this.FixSeedCheck);
            this.ControlPanel.Controls.Add(this.label1);
            this.ControlPanel.Controls.Add(this.SeedNumeric);
            this.ControlPanel.Controls.Add(this.StepEpochButton);
            this.ControlPanel.Controls.Add(this.SaveButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ControlPanel.Location = new System.Drawing.Point(0, 0);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(228, 386);
            this.ControlPanel.TabIndex = 0;
            // 
            // FixWidthCheck
            // 
            this.FixWidthCheck.AutoSize = true;
            this.FixWidthCheck.Location = new System.Drawing.Point(176, 122);
            this.FixWidthCheck.Name = "FixWidthCheck";
            this.FixWidthCheck.Size = new System.Drawing.Size(36, 17);
            this.FixWidthCheck.TabIndex = 6;
            this.FixWidthCheck.Text = "fix";
            this.FixWidthCheck.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Width:";
            // 
            // DisplayWidthNumeric
            // 
            this.DisplayWidthNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DisplayWidthNumeric.Location = new System.Drawing.Point(54, 121);
            this.DisplayWidthNumeric.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.DisplayWidthNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DisplayWidthNumeric.Name = "DisplayWidthNumeric";
            this.DisplayWidthNumeric.Size = new System.Drawing.Size(116, 20);
            this.DisplayWidthNumeric.TabIndex = 4;
            this.DisplayWidthNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DisplayWidthNumeric.ValueChanged += new System.EventHandler(this.DisplayHeightNumeric_ValueChanged);
            // 
            // LocalViolationBoundriesCheck
            // 
            this.LocalViolationBoundriesCheck.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LocalViolationBoundriesCheck.Location = new System.Drawing.Point(14, 96);
            this.LocalViolationBoundriesCheck.Name = "LocalViolationBoundriesCheck";
            this.LocalViolationBoundriesCheck.Size = new System.Drawing.Size(198, 19);
            this.LocalViolationBoundriesCheck.TabIndex = 3;
            this.LocalViolationBoundriesCheck.Text = "Local Violation Boundaries";
            this.LocalViolationBoundriesCheck.UseVisualStyleBackColor = true;
            this.LocalViolationBoundriesCheck.CheckedChanged += new System.EventHandler(this.LocalViolationBoundriesCheck_CheckedChanged);
            // 
            // FixSeedCheck
            // 
            this.FixSeedCheck.AutoSize = true;
            this.FixSeedCheck.Location = new System.Drawing.Point(176, 71);
            this.FixSeedCheck.Name = "FixSeedCheck";
            this.FixSeedCheck.Size = new System.Drawing.Size(36, 17);
            this.FixSeedCheck.TabIndex = 1;
            this.FixSeedCheck.Text = "fix";
            this.FixSeedCheck.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Seed:";
            // 
            // SeedNumeric
            // 
            this.SeedNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SeedNumeric.Location = new System.Drawing.Point(54, 70);
            this.SeedNumeric.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.SeedNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SeedNumeric.Name = "SeedNumeric";
            this.SeedNumeric.Size = new System.Drawing.Size(116, 20);
            this.SeedNumeric.TabIndex = 2;
            this.SeedNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // StepEpochButton
            // 
            this.StepEpochButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StepEpochButton.Location = new System.Drawing.Point(12, 41);
            this.StepEpochButton.Name = "StepEpochButton";
            this.StepEpochButton.Size = new System.Drawing.Size(200, 23);
            this.StepEpochButton.TabIndex = 2;
            this.StepEpochButton.Text = "Step Epoch";
            this.StepEpochButton.UseVisualStyleBackColor = true;
            this.StepEpochButton.Click += new System.EventHandler(this.StepEpochButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveButton.Location = new System.Drawing.Point(12, 12);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(200, 23);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 386);
            this.Controls.Add(this.ControlPanel);
            this.Name = "MainForm";
            this.Text = "M4M DenseMod";
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayWidthNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeedNumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private VectorEditor Editor;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.SaveFileDialog SaveFileDialog;
        private System.Windows.Forms.Button StepEpochButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown SeedNumeric;
        private System.Windows.Forms.CheckBox FixSeedCheck;
        private System.Windows.Forms.CheckBox LocalViolationBoundriesCheck;
        private System.Windows.Forms.CheckBox FixWidthCheck;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown DisplayWidthNumeric;
    }
}

